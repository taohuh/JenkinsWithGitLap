/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.n2nconnect.iTradeCIMBSG_uat.stock.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.n2nconnect.iTradeCIMBSG_uat.stock.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 18;
  public static final String VERSION_NAME = "2.3.5";
}
