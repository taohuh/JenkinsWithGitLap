package com.n2nconnect.android.stock.util;

//based from the project https://github.com/scottyab/rootbeer

import com.n2nconnect.android.stock.DefinitionConstants;

public class RootBeerNative {
    static boolean libraryLoaded = false;

    /**
     * Loads the C/C++ libraries statically
     */
    static {
        try {
            System.loadLibrary("tool-checker");
            libraryLoaded = true;
        }
        catch (UnsatisfiedLinkError e){
            //e.printStackTrace();
            DefinitionConstants.Debug("unable to load tool-checker");
        }
    }

    public boolean wasNativeLibraryLoaded(){
        return libraryLoaded;
    }

    public native int checkForRoot(Object[] pathArray);
    public native int setLogDebugMessages(boolean logDebugMessages);
}