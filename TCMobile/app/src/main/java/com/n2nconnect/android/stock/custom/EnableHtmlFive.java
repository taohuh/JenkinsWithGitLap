package com.n2nconnect.android.stock.custom;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.webkit.WebSettings;

public  class EnableHtmlFive {
	
	private Context context;
	private String TAG ="ENABLE HTML5";
	
	public EnableHtmlFive(Context context) {
		this.context = context;
	}
	
	public void EnableHtmlFive(WebSettings ws){

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
			try {
				Log.d(TAG, "Enabling HTML5-Features");
				Method m1 = WebSettings.class.getMethod(
						"setDomStorageEnabled",
						new Class[] { Boolean.TYPE });
				m1.invoke(ws, Boolean.TRUE);

				Method m2 = WebSettings.class.getMethod(
						"setDatabaseEnabled",
						new Class[] { Boolean.TYPE });
				m2.invoke(ws, Boolean.TRUE);

				Method m3 = WebSettings.class
						.getMethod("setDatabasePath",
								new Class[] { String.class });
				m3.invoke(ws, "/data/data/" + context.getPackageName()
						+ "/databases/");

				Method m4 = WebSettings.class
						.getMethod("setAppCacheMaxSize",
								new Class[] { Long.TYPE });
				m3.invoke(ws, "/data/data/" + context.getPackageName()
						+ "/databases/");
				Method m5 = WebSettings.class
						.getMethod("setAppCachePath",
								new Class[] { String.class });
				m5.invoke(ws, "/data/data/" + context.getPackageName()
						+ "/cache/");

				Method m6 = WebSettings.class.getMethod(
						"setAppCacheEnabled",
						new Class[] { Boolean.TYPE });
				m6.invoke(ws, Boolean.TRUE);

				Log.d(TAG, "Enabled HTML5-Features");
			} catch (NoSuchMethodException e) {
				Log.e(TAG, "Reflection fail", e);
			} catch (InvocationTargetException e) {
				Log.e(TAG, "Reflection fail", e);
			} catch (IllegalAccessException e) {
				Log.e(TAG, "Reflection fail", e);
			}
		}
	}

}
