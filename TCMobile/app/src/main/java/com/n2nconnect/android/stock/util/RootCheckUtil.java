package com.n2nconnect.android.stock.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

import android.content.Context;
import android.content.pm.PackageManager;

import com.n2nconnect.android.stock.DefinitionConstants;

//Note: code based on https://github.com/scottyab/rootbeer
//also read: https://blog.netspi.com/android-root-detection-techniques/

/**
 * 1001: su binary exists.
 * 1002: dangerous props
 * 1003: path is mounted with rw permissions!
 * 1004: detectTestKeys
 * 1005: su exists
 * 1006: busybox binary exists.
 */

public class RootCheckUtil {
    private final Context mContext;
    private boolean loggingEnabled = true;
    private String errorMsg;

    public RootCheckUtil(Context context) {
        mContext = context;
    }

    public boolean isRooted() {

        /*return detectRootManagementApps() || detectPotentiallyDangerousApps() || checkForBinary("su")
                || checkForBinary("busybox") || checkForDangerousProps() || checkForRWPaths()
                || detectTestKeys() || checkSuExists() || checkForRootNative();*/

        //not going to check root management apps and potentially dangerous apps
        return checkForBinary("su")
                || checkForBinary("busybox") || checkForDangerousProps() || checkForRWPaths()
                || detectTestKeys() || checkSuExists() || checkForRootNative();
    }

    public boolean isRootedWithoutBusyBoxCheck() {

        return checkForBinary("su")
                || checkForDangerousProps() || checkForRWPaths()
                || detectTestKeys() || checkSuExists() || checkForRootNative();

        /*return detectRootManagementApps() || detectPotentiallyDangerousApps() || checkForBinary("su")
                || checkForDangerousProps() || checkForRWPaths()
                || detectTestKeys() || checkSuExists() || checkForRootNative();*/
    }

    public boolean detectTestKeys() {
        String buildTags = android.os.Build.TAGS;

        if(buildTags != null && buildTags.contains("test-keys"))
            this.setErrorMsg("1004");
            //this.setErrorMsg("test-keys detected!");

        return buildTags != null && buildTags.contains("test-keys");
    }

    public boolean detectRootManagementApps() {
        return detectRootManagementApps(null);
    }

    public boolean detectRootManagementApps(String[] additionalRootManagementApps) {

        ArrayList<String> packages = new ArrayList<String>();
        packages.addAll(Arrays.asList(knownRootAppsPackages));
        if (additionalRootManagementApps!=null && additionalRootManagementApps.length>0){
            packages.addAll(Arrays.asList(additionalRootManagementApps));
        }

        return isAnyPackageFromListInstalled(packages);
    }

    public boolean detectPotentiallyDangerousApps() {
        return detectPotentiallyDangerousApps(null);
    }

    public boolean detectPotentiallyDangerousApps(String[] additionalDangerousApps) {

        ArrayList<String> packages = new ArrayList<String>();
        packages.addAll(Arrays.asList(knownDangerousAppsPackages));
        if (additionalDangerousApps!=null && additionalDangerousApps.length>0){
            packages.addAll(Arrays.asList(additionalDangerousApps));
        }

        return isAnyPackageFromListInstalled(packages);
    }

    public boolean detectRootCloakingApps() {
        return detectRootCloakingApps(null) || canLoadNativeLibrary() && !checkForNativeLibraryReadAccess();
    }

    public boolean detectRootCloakingApps(String[] additionalRootCloakingApps) {

        ArrayList<String> packages = new ArrayList<String>();
        packages.addAll(Arrays.asList(knownRootCloakingPackages));
        if (additionalRootCloakingApps!=null && additionalRootCloakingApps.length>0){
            packages.addAll(Arrays.asList(additionalRootCloakingApps));
        }
        return isAnyPackageFromListInstalled(packages);
    }

    public boolean checkForSuBinary(){
        return checkForBinary("su");
    }

    public boolean checkForBusyBoxBinary(){
        return checkForBinary("busybox");
    }

    public boolean checkForBinary(String filename) {

        String[] pathsArray = suPaths;

        boolean result = false;

        for (String path : pathsArray) {
            String completePath = path + filename;
            File f = new File(completePath);
            boolean fileExists = f.exists();
            if (fileExists) {
                //this.setErrorMsg(filename + " binary exists.");
                if(filename.equals("su"))
                    this.setErrorMsg("1001");
                else
                    this.setErrorMsg("1006");
                result = true;
            }
        }

        return result;
    }

    private String[] propsReader() {
        Process process = null;
        InputStream inputstream = null;

        try {
            process = Runtime.getRuntime().exec("getprop");
            inputstream = process.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String propval = "";
        try {
            propval = new Scanner(inputstream).useDelimiter("\\A").next();

        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }finally{
            if(process != null)
                process.destroy();

            try {
                inputstream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return propval.split("\n");
    }

    private String[] mountReader() {
        Process process = null;
        InputStream inputstream = null;

        try {
            process = Runtime.getRuntime().exec("mount");
            inputstream = process.getInputStream();

        } catch (IOException e) {
            e.printStackTrace();
        }

        // If input steam is null, we can't read the file, so return null
        if (inputstream == null) return null;

        String propval = "";
        try {
            propval = new Scanner(inputstream).useDelimiter("\\A").next();
        } catch (NoSuchElementException e) {
            DefinitionConstants.Debug("mountReader failed.");
        }finally{
            if(process != null)
                process.destroy();

            try {
                inputstream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return propval.split("\n");
    }

    private boolean isAnyPackageFromListInstalled(List<String> packages){
        boolean result = false;

        PackageManager pm = mContext.getPackageManager();

        for (String packageName : packages) {
            try {
                pm.getPackageInfo(packageName, 0);

                this.setErrorMsg(packageName + " ROOT management app detected!");
                result = true;
            } catch (PackageManager.NameNotFoundException e) {
                // Exception thrown, package is not installed into the system
                DefinitionConstants.Debug(packageName + " not installed.");
            }
        }

        return result;
    }

    public boolean checkForDangerousProps() {
        DefinitionConstants.Debug("checkForDangerousProps");
        final Map<String, String> dangerousProps = new HashMap<String, String>();
        dangerousProps.put("ro.debuggable", "1");
        dangerousProps.put("ro.secure", "0");

        boolean result = false;

        String[] lines = propsReader();
        for (String line : lines) {
            for (String key : dangerousProps.keySet()) {
                if (line.contains(key)) {
                    String badValue = dangerousProps.get(key);
                    badValue = "[" + badValue + "]";
                    DefinitionConstants.Debug(key + "=" + badValue);

                    if (line.contains(badValue)) {
                        //this.setErrorMsg(key + " = " + badValue + " detected!");
                        this.setErrorMsg("1002");
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public boolean checkForRWPaths() {
        DefinitionConstants.Debug("checkForRWPaths");

        boolean result = false;

        String[] lines = mountReader();
        for (String line : lines) {

            // Split lines into parts
            String[] args = line.split(" ");

            if (args.length < 4){
                // If we don't have enough options per line, skip this and log an error
                DefinitionConstants.Debug("Error formatting mount line: "+line);
                continue;
            }

            String mountPoint = args[1];
            String mountOptions = args[3];

            for(String pathToCheck: pathsThatShouldNotBeWrtiable) {
                if (mountPoint.equalsIgnoreCase(pathToCheck)) {
                    DefinitionConstants.Debug(pathToCheck + "," + mountPoint + "," + mountOptions);

                    // Split options out and compare against "rw" to avoid false positives
                    for (String option : mountOptions.split(",")){

                        if (option.equalsIgnoreCase("rw")){
                            //this.setErrorMsg(pathToCheck + " path is mounted with rw permissions! " + line);
                            this.setErrorMsg("1003, " + pathToCheck);
                            result = true;
                            break;
                        }
                    }
                }
            }
        }

        return result;
    }

    public boolean checkSuExists() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[] { "which", "su" });
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            this.setErrorMsg("1005");
            return in.readLine() != null;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    public boolean checkForNativeLibraryReadAccess() {
        RootBeerNative rootBeerNative = new RootBeerNative();
        try {
            rootBeerNative.setLogDebugMessages(loggingEnabled);
            return true;
        } catch (UnsatisfiedLinkError e) {
            return false;
        }
    }

    public boolean canLoadNativeLibrary(){
        return new RootBeerNative().wasNativeLibraryLoaded();
    }

    public boolean checkForRootNative() {

        if (!canLoadNativeLibrary()){
            DefinitionConstants.Debug("We could not load the native library to test for root");
            return false;
        }

        String binaryName = "su";
        String[] paths = new String[suPaths.length];
        for (int i = 0; i < paths.length; i++) {
            paths[i] = suPaths[i]+binaryName;
        }

        RootBeerNative rootBeerNative = new RootBeerNative();
        try {
            rootBeerNative.setLogDebugMessages(loggingEnabled);
            return rootBeerNative.checkForRoot(paths) > 0;
        } catch (UnsatisfiedLinkError e) {
            return false;
        }
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    private final String[] knownRootAppsPackages = {
            "com.noshufou.android.su",
            "com.noshufou.android.su.elite",
            "eu.chainfire.supersu",
            "com.koushikdutta.superuser",
            "com.thirdparty.superuser",
            "com.yellowes.su"
    };

    private final String[] knownDangerousAppsPackages = {
            "com.koushikdutta.rommanager",
            "com.koushikdutta.rommanager.license",
            "com.dimonvideo.luckypatcher",
            "com.chelpus.lackypatch",
            "com.ramdroid.appquarantine",
            "com.ramdroid.appquarantinepro"
    };

    private final String[] knownRootCloakingPackages = {
            "com.devadvance.rootcloak",
            "com.devadvance.rootcloakplus",
            "de.robv.android.xposed.installer",
            "com.saurik.substrate",
            "com.zachspong.temprootremovejb",
            "com.amphoras.hidemyroot",
            "com.amphoras.hidemyrootadfree",
            "com.formyhm.hiderootPremium",
            "com.formyhm.hideroot"
    };

    private final String[] suPaths ={
            "/data/local/",
            "/data/local/bin/",
            "/data/local/xbin/",
            "/sbin/",
            "/su/bin/",
            "/system/bin/",
            "/system/bin/.ext/",
            "/system/bin/failsafe/",
            "/system/sd/xbin/",
            "/system/usr/we-need-root/",
            "/system/xbin/"
    };

    private final String[] pathsThatShouldNotBeWrtiable = {
            "/system",
            "/system/bin",
            "/system/sbin",
            "/system/xbin",
            "/vendor/bin",
            //"/sys",
            "/sbin",
            "/etc",
            //"/proc",
            //"/dev"
    };
}