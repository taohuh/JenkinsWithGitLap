package com.n2nconnect.android.stock.model;

import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.util.SystemUtil;

public class DervTradePrtfDetailRpt {
	private int sequenceNo;
	private String side;
	private int matchedQty;
	private float matchedPrice;
	private float multiplier;
	private String homeCurrency;
	private float forexExchangeRate;
	private String currencySymbol;
	
	private float unrealised;
	
	public int getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(int sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
	public int getMatchedQty() {
		return matchedQty;
	}
	public void setMatchedQty(int matchedQty) {
		this.matchedQty = matchedQty;
	}
	public float getMatchedPrice() {
		return matchedPrice;
	}
	public void setMatchedPrice(float matchedPrice) {
		this.matchedPrice = matchedPrice;
	}
	public float getMultiplier() {
		return multiplier;
	}
	public void setMultiplier(float multiplier) {
		this.multiplier = multiplier;
	}
	public String getHomeCurrency() {
		return homeCurrency;
	}
	public void setHomeCurrency(String homeCurrency) {
		this.homeCurrency = homeCurrency;
	}
	public float getForexExchangeRate() {
		return forexExchangeRate;
	}
	public void setForexExchangeRate(float forexExchangeRate) {
		this.forexExchangeRate = forexExchangeRate;
	}
	public String getCurrencySymbol() {
		return currencySymbol;
	}
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	
	public void calculateUnrealised(DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt){
		float result = 0;
		String derivativeStockType = dervTradePrtfSubDetailRpt.getDerivativeStockType();
		float averagePrice = matchedPrice;
		float lastDone = dervTradePrtfSubDetailRpt.getLastDone();
		String stockName = dervTradePrtfSubDetailRpt.getStockName();
				
		int nettPosition = 0;
		if(side.equalsIgnoreCase(ParamConstants.SELL_SIDE)) nettPosition = matchedQty * -1;
		else if(side.equalsIgnoreCase(ParamConstants.BUY_SIDE)) nettPosition = matchedQty;
		
		if(derivativeStockType != null){
			if(derivativeStockType.equalsIgnoreCase("F")){//FUTURE
				result = (lastDone- averagePrice)* nettPosition* multiplier;
			}else if(derivativeStockType.equalsIgnoreCase("O")){//OPTION
				
				if(SystemUtil.isCall(stockName)){//CALL
					if (nettPosition >=0){ //CALL BUY
			          if (lastDone- averagePrice <=0){ result = 0; }
			          else{
			        	  result =( lastDone - averagePrice)* nettPosition * multiplier;
			          }
					}
					else{ //CALL SELL
			          if (averagePrice - lastDone >= averagePrice){
			        	  result = averagePrice * Math.abs(nettPosition) * multiplier;
			          }
			          else{
			        	  //result = (lastDone- averagePrice)* nettPosition* multiplier //Kam Fai 01/08/2013
			        	  result = (averagePrice - lastDone) * Math.abs(nettPosition) * multiplier;
			          } 
					}
				}else{//PUT
					if (nettPosition >=0){ //PUT BUY
			          if (lastDone - averagePrice >=0) { result = 0; }
			          else { 
			        	  //result = (lastDone- averagePrice)* nettPosition* multiplier  //Kam Fai 01/08/2013
			        	  result = (averagePrice - lastDone) * nettPosition * multiplier ;
			          }
					}
			      else{//PUT SELL
			          if (lastDone - averagePrice >= averagePrice){
			            result = averagePrice * Math.abs(nettPosition)* multiplier;
			          }
			          else{
			        	  //result = (lastDone - averagePrice)* nettPosition * multiplier;
			        	  result = (lastDone - averagePrice) * Math.abs(nettPosition) * multiplier; //Kam Fai 01/08/2013
			          }
			      }
				}
			}
			this.unrealised = result;
		}
		
	}
	
	public float getUnrealised() {
		return unrealised;
	}
}
