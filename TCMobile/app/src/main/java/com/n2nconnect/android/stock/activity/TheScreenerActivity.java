package com.n2nconnect.android.stock.activity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.StockInfo;
import com.n2nconnect.android.stock.model.Watchlist;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class TheScreenerActivity extends CustomWindow implements OnDoubleLoginListener {
	
	private final String TAG	= "TheScreenerActivity";
	private WebView webview_the_screener;
	private ProgressDialog	dialog;
	private String url	= "";
	private String senderCode	= "";
	private String screenerType;
	private String exchange;
	private String symbolCode;
	private String token;
	private String strSymbolCode;
	private int lotSize;
    private String instrument;
    private boolean isPayableWithCPF;
	
	//Note:this AESSecretKey and bankName are provided by theScreener team
	private static final byte[] theScreenerSecretKey = new byte[] { 'A', 'r', 'L', 'l', 'g', 'M', '6', 'S', 'a', '1', 'Q', 'm', '4', 'a', 'H', 'M' };
	private final String bankName	= "BANK";
	
	//params for add to watchlist
	private List<Watchlist> watchlists;
	private boolean isExpired	 		= false;
	private int intRetryCtr	= 0;
	private AlertDialog alertException;
	
	private DoubleLoginThread loginThread;
	
	private final String strTheScreenerGlobalURL		= AppConstants.brokerConfigBean.getTheScreenerUrl() + "GlobalIndices.html?";
	private final String strTheScreenIndividualURL		= AppConstants.brokerConfigBean.getTheScreenerUrl() + "IndividualStockDetail.html?";
	
	private String stockCodeFromUrl;
	private String exchangeCodeFromUrl;
	private String qcExchangeCode;

	private CharSequence pnotfound = "Webpage not available";
	
	private final int TRADE_TASK_ID		= 9001;
	private final int WATCHLIST_TASK_ID	= 9002;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.title.setText(getResources().getString(R.string.screener_module_title));
		this.icon.setImageResource(R.drawable.menuicon_thescreener);	
		
		setContentView(R.layout.the_screener_activity);
		
		Bundle extras	= this.getIntent().getExtras();
		if(extras != null){
			screenerType		= extras.getString(ParamConstants.INFO_TYPE_TAG);
			
			if(screenerType.equals(ParamConstants.SCREENER_TYPE_INDIVIDUAL)){
				exchange			= extras.getString(ParamConstants.EXCHANGECODE_TAG);
				symbolCode			= extras.getString(ParamConstants.SYMBOLCODE_TAG);
				lotSize				= extras.getInt(ParamConstants.LOT_SIZE_TAG);
				instrument			= extras.getString(ParamConstants.INSTRUMENT_TAG);
				isPayableWithCPF	= extras.getBoolean(ParamConstants.IS_PAYABLE_WITH_CPF);
				
				strSymbolCode		= symbolCode.substring(0, symbolCode.lastIndexOf(".") );
			}
		}
		
		senderCode 		= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

		alertException 	= new AlertDialog.Builder(TheScreenerActivity.this)
				//.setIcon(android.R.attr.alertDialogStyle)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
						return;
					}
				}).create();
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
		if( idleThread != null && idleThread.isPaused() )
			idleThread.resumeRequest(false);
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
		
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		loginThread = ((StockApplication) TheScreenerActivity.this.getApplication()).getDoubleLoginThread();
		if( loginThread != null && loginThread.isPaused() ){
			loginThread.setRegisteredListener(TheScreenerActivity.this);
			loginThread.resumeRequest();
		} 
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
		
		token	= generateToken();
		url		= generateTheScreenerURL();
		
		dialog					= new ProgressDialog(TheScreenerActivity.this);
		webview_the_screener	= (WebView) findViewById(R.id.webview_the_screener);
		//WebSettings ws 			= webview_the_screener.getSettings();
		
		//webview_the_screener = (WebView) findViewById(R.id.webview_the_screener);
		webview_the_screener.getSettings().setJavaScriptEnabled(true);
		
		//to enhance the performance with hardware acceleration, android 19 has Chromium engine for WebView
		if (Build.VERSION.SDK_INT >= 19) {
			webview_the_screener.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		}       
		else {
			webview_the_screener.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}
		
		webview_the_screener.loadUrl(url);
		
		webview_the_screener.setWebViewClient(new WebViewController());
			
	}

	public class WebViewController extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			
        	if(url.startsWith("itradecimbtradebuttonclicked:")){
        		String symbolCodeFromUrl	= getSymbolCodeFromUrl(url);
        		
        		if(AppConstants.noTradeClient){
        			showAlertDialog("You do not have a trading account", false);
        		} else if(qcExchangeCode == null || qcExchangeCode.isEmpty()){
        			showAlertDialog(getResources().getString(R.string.stock_not_found_screener_trade), false);
        			
        		}else{
	        		Log.d(TAG, "TradeButton= " + url);			//itradecimbwatchlistbuttonclicked:/stockcode:Z74/exchange:SG
	        		
	        		if(symbolCode != null && symbolCode.equalsIgnoreCase(symbolCodeFromUrl)){
	        			startTradeActivity(symbolCodeFromUrl, Integer.toString(lotSize), instrument, isPayableWithCPF);
	        		} else{
	        			//request coming from global Indices, fetch Stock Info
	        			new FetchStockInfo().execute(symbolCodeFromUrl, Integer.toString(TRADE_TASK_ID));
	        		}
        		} 
        		
        		return true;
        	} else if(url.startsWith("itradecimbwatchlistbuttonclicked:")){
        		//to reset ATP expire time
        		if(idleThread != null && ! isShowingIdleAlert)
    				idleThread.resetExpiredTime();
        		
        		String symbolCodeFromUrl	= getSymbolCodeFromUrl(url);
        		
        		Log.d(TAG, "WatchlistButton= " + url);
        		if(qcExchangeCode == null || qcExchangeCode.isEmpty()){
        			showAlertDialog(getResources().getString(R.string.stock_not_found_screener_watch), false);
        			
        		}else{
	        		if(symbolCodeFromUrl != null ){
	        			new FetchStockInfo().execute(symbolCodeFromUrl, Integer.toString(WATCHLIST_TASK_ID));
	        		}
        		}
        		return true;
        	} 
        	//Added based on suggestion from iScreener team
        	else if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
                view.reload();
                
                return true;
            }
        	else {
        		view.loadUrl(url);
        		
        		return false;
        	}
        }
		
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			showAlertDialog(getResources().getString(R.string.screener_not_available), true);
	    }
	}
	
	private void showAlertDialog(String alertMsg, final boolean isExit){
		alertException 	= new AlertDialog.Builder(TheScreenerActivity.this)
		//.setIcon(android.R.attr.alertDialogStyle)
		.setMessage(alertMsg)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				if(isExit){
					TheScreenerActivity.this.finish();
				}
				return;
			}
		}).create();

		if(!TheScreenerActivity.this.isFinishing()) //Fixes for Crashlytics
			alertException.show();
	}

	private void startTradeActivity(String mSymbolCode, String mLotSize, String mInstrument, boolean mIsPayableWithCPF) {

		Intent intent = new Intent();
		intent.putExtra(ParamConstants.SYMBOLCODE_TAG, mSymbolCode);
		intent.putExtra(ParamConstants.LOT_SIZE_TAG, mLotSize);
		intent.putExtra(ParamConstants.INSTRUMENT_TAG, mInstrument);
		intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, mIsPayableWithCPF);
		intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
		intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
		intent.putExtra(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG, ParamConstants.PARAM_ORDER_SOURCE_IS);	//Change_Request_20170119, ReqNo.4
		
		intent.setClass(TheScreenerActivity.this, TradeActivity.class);

		startActivity(intent);

	}
	
	private String getSymbolCodeFromUrl(String url){
		String[] temp	= url.split("/");
		
		String[] symbolCodeChunk	= temp[1].split("\\:");
		stockCodeFromUrl			= symbolCodeChunk[1];
		
		String[] exchangeChunk		= temp[2].split("\\:");
		exchangeCodeFromUrl			= exchangeChunk[1];
		
		qcExchangeCode		= SystemUtil.getQcExchangeCode(exchangeCodeFromUrl);	//to support delay feed
		
		if(qcExchangeCode != null && !qcExchangeCode.isEmpty()){
			return stockCodeFromUrl + "." + qcExchangeCode;
		} else
			return stockCodeFromUrl + "." + exchangeCodeFromUrl;
	}
	
	private String generateToken(){
		String token	= null;
		String currentTimeStamp	= FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss");
		
		try {
			token	= Base64.encodeToString(encrypt(theScreenerSecretKey, bankName + "|" + currentTimeStamp), Base64.NO_WRAP); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return token;
	}
	
	private String generateTheScreenerURL(){
		/* Sample URLs
		 * http://103.253.10.206/m-cimb/GlobalIndices.html?id=abc21441&lang=en&token=OFRIFC9r7wnRJKL%2FlinuKUhToELoC2GOGFwgPVfMWQM%3D
		 * http://103.253.10.206/m-cimb/IndividualStockDetail.html?market=SG&symbol=D05&id=abc21441&lang=en&token=OFRIFC9r7wnRJKL%2FlinuKUhToELoC2GOGFwgPVfMWQM%3D
		 */
		String url		= "";
		try {
			if(screenerType.equals(ParamConstants.SCREENER_TYPE_INDIVIDUAL)){
				
				url	= this.strTheScreenIndividualURL + "region=" + AppConstants.screenerRegionId.get(getApplicationContext().getPackageName())
						+ "&market=" + exchange + "&symbol=" + strSymbolCode + "&id=" + senderCode
						+ "&lang=en&token=" + URLEncoder.encode(token, AppConstants.STRING_ENCODING_FORMAT);
				
			}else{
				
				url	= this.strTheScreenerGlobalURL + "region=" + AppConstants.screenerRegionId.get(getApplicationContext().getPackageName()) 
						+ "&id=" + senderCode 
						+ "&lang=en&token=" + URLEncoder.encode(token, AppConstants.STRING_ENCODING_FORMAT);
			}
			
			DefinitionConstants.Debug("theScreenerURL: " + url);
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return url;
	}
	
	//AES encryption, no padding
	public static byte[] encrypt(byte[] key, String plainText) throws Exception {

		SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		byte[] encrypted = cipher.doFinal(plainText.getBytes());

		return encrypted;
	}
	
	//to decrypt data sent by iBillionaire
	public static String decrypt(byte[] key, String encryptedString) throws Exception{
		
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        
        byte[] encrypedBytes = Base64.decode(encryptedString, Base64.NO_WRAP);		//new Base64().decode(text);
        byte[] decrypedValueBytes = (cipher.doFinal(encrypedBytes));
        String decrypedString = new String(decrypedValueBytes);
        
        return decrypedString;
	}
	
	private class FetchWatchlistTask extends AsyncTask<String, Void, Exception>{
		
		String atpUserParam 								= null;
		String senderCode									= null;
		private boolean isDialogOwner						= false;
		private AsyncTask<Void, Void, Exception> updateTask = null;
		private Exception exception							= null;
		private String mStockCode							= null;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			try{
				atpUserParam 	= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
				senderCode		= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
				watchlists 		= new ArrayList<Watchlist>();
				
				if( ! TheScreenerActivity.this.dialog.isShowing() ){
					TheScreenerActivity.this.dialog.setMessage("Retrieving Watchlist...");
					
					if( ! TheScreenerActivity.this.isFinishing() ){
						TheScreenerActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		@Override
		protected Exception doInBackground(String... params) {
			mStockCode	= params[0];
			
			Map<String, Object> parameters 	= new HashMap<String, Object>();
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			try {
				String response = AtpConnectUtil.getFavListInfo(parameters);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = AtpConnectUtil.getFavListInfo(parameters);
				}
				intRetryCtr = 0;
	
				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_WATCHLIST);
				
				watchlists = AtpMessageParser.parseFavListInfo(response);
				
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				return e;
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Void... params) {
			try{
				if( ! TheScreenerActivity.this.dialog.isShowing() ){
					TheScreenerActivity.this.dialog.setMessage("Retrieving Watchlist...");
					
					if( ! TheScreenerActivity.this.isFinishing() ){
						TheScreenerActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected void onPostExecute(Exception exception) {
			super.onPostExecute(exception);

			try{
				if (exception != null) {
					if( TheScreenerActivity.this.dialog != null && TheScreenerActivity.this.dialog.isShowing() && isDialogOwner){
						TheScreenerActivity.this.dialog.dismiss();
						isDialogOwner = false;
					}
					
					if(exception instanceof FailedAuthenicationException){
						
						if (loginThread != null)
							loginThread.stopRequest();

						if (LoginActivity.QCAlive != null)
							LoginActivity.QCAlive.stopRequest();

						TheScreenerActivity.this.sessionExpired();
					}else{
						alertException.setMessage(exception.getMessage());
						alertException.setButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								finish();
							}
						});
						
						if( ! TheScreenerActivity.this.isFinishing() )
							alertException.show();
					}
					
					exception = null;
					return;
				}
				
				if( TheScreenerActivity.this.dialog != null && TheScreenerActivity.this.dialog.isShowing() && isDialogOwner){
					TheScreenerActivity.this.dialog.dismiss();
					isDialogOwner = false;
				}
				
				//check whether the watchlist exists.
				Watchlist theScreenerWatchlist	= null;
				for(Watchlist w: watchlists){
					if(w.getName().equalsIgnoreCase(getResources().getString(R.string.screener_module_title))){
						
						theScreenerWatchlist	= w;
						break;
					}
				}
				
				String tempMaxId	= "1";
				
				if (watchlists.isEmpty() || theScreenerWatchlist == null) {
					if(!watchlists.isEmpty()){
						
						int maxId	= 0;
						for (Iterator<Watchlist> itr = watchlists.iterator(); itr.hasNext(); ) {
							Watchlist watchlist	= itr.next();
							int id 				= Integer.parseInt(watchlist.getId());
							if(id > maxId)
								maxId = id;
						}
						
						tempMaxId	= String.valueOf(maxId + 1);
					}
					
					//create new watchlist and add to that watchlist
					
					new ExecuteWLCreate().execute(tempMaxId, getResources().getString(R.string.screener_module_title));
					
				} else{
					Log.i(TAG, "Adding " + mStockCode + " to watchlist id=" + theScreenerWatchlist.getId() + " name=" + theScreenerWatchlist.getName());
					//new AddWatchlistStock().execute(theScreenerWatchlist.getId());
					new CheckWatchlist().execute(theScreenerWatchlist.getId());
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}	
	
	private class CheckWatchlist extends AsyncTask<String, Void, Boolean> {
		private Exception exception;
		private String watchlistId;
		boolean isSuccess = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(String... items) {
			watchlistId = items[0];

			String atpUserParam = sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			String senderCode = sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
			
			try {
				isSuccess = QuoteActivity.checkWatchlistSymbols(qcExchangeCode, stockCodeFromUrl, watchlistId, atpUserParam, senderCode, TheScreenerActivity.this);

			} catch (FailedAuthenicationException e) {
				isExpired = true;
				/*
				 * TODO: check later if(refreshThread != null)
				 * refreshThread.stopRequest();
				 */
				if (loginThread != null)
					loginThread.stopRequest();

				if (LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();

				exception = e;

			} catch (Exception e) {
				exception = e;
			}
			return isSuccess;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			if (exception != null) {

				showAlertDialog(exception.getMessage(), false);
			} else {

				new AddWatchlistStock().execute(watchlistId);
			}
		}
	}
	
	private class AddWatchlistStock extends AsyncTask<String, Void, String> {
		private Exception exception = null;
		String strWatchlistID;
		
		@Override
		protected String doInBackground(String... item) {
			strWatchlistID		= item[0];
			
			try {
				String atpUserParam				= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
				String senderCode 				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

				Map<String, Object> parameters	= new HashMap<String, Object>();
				
				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);

				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
				parameters.put(ParamConstants.FAVORATEID_TAG, strWatchlistID);
				parameters.put(ParamConstants.SYMBOLCODE_TAG, stockCodeFromUrl);
				
				exchangeCodeFromUrl		= (exchangeCodeFromUrl.length() > 1 && exchangeCodeFromUrl.toUpperCase().endsWith("D") ) ? exchangeCodeFromUrl.substring(0, (exchangeCodeFromUrl.length() - 1) )  : exchangeCodeFromUrl;
				parameters.put(ParamConstants.EXCHANGECODE_TAG, exchangeCodeFromUrl);
				
				try {
					boolean isSuccess = AtpConnectUtil.addFavListStock(parameters);
					while (intRetryCtr < AppConstants.intMaxRetry && !isSuccess) {
						intRetryCtr++;
						isSuccess = AtpConnectUtil.addFavListStock(parameters);
					}
					intRetryCtr = 0;

					if (!isSuccess)
						throw new Exception(ErrorCodeConstants.FAIL_ADD_STOCK_INTO_WATCHLIST);
				} catch (FailedAuthenicationException e) {
					isExpired = true;
				
					if(loginThread != null)
						loginThread.stopRequest();
			
					if(LoginActivity.QCAlive != null)
						LoginActivity.QCAlive.stopRequest();
					throw e;
				} catch (Exception e) {
					throw e;
				}
			} catch (Exception e) {
				exception = e;
			}
			
			return stockCodeFromUrl;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			try{
				if (isExpired) {
					TheScreenerActivity.this.sessionExpired();
					return;
				}
	
				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					
					if( ! TheScreenerActivity.this.isFinishing() )
						alertException.show();
					
					return;
				}
				
				Toast.makeText(getApplicationContext(), "Added to " + getResources().getString(R.string.screener_module_title) + " watchlist: " + result, Toast.LENGTH_SHORT).show();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private class ExecuteWLCreate extends AsyncTask<String, Void, String>{
		Exception exception		= null;
		String strWatchlistId;
		String strWatchlistName;
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();

			try{
				if( ! dialog.isShowing() ){
					dialog.setMessage("creating watchlist...");
					if( ! TheScreenerActivity.this.isFinishing() )
						dialog.show();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			strWatchlistId					= params[0];
			strWatchlistName 				= params[1];

			String atpUserParam				= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			String senderCode 				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
			String paramString 				= sharedPreferences.getString(PreferenceConstants.ATP_PARAM_STRING, null);

			Map<String, Object> parameters 	= new HashMap<String, Object>();
			parameters.put(ParamConstants.FAVORATEID_TAG, strWatchlistId);
			
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			parameters.put(ParamConstants.FAVORATENAME_TAG, strWatchlistName);
			parameters.put(ParamConstants.PARAMSTRING_TAG, paramString);

			try {
				boolean isSuccess	= AtpConnectUtil.updateFavListName(parameters);
				while( intRetryCtr < AppConstants.intMaxRetry && ! isSuccess ){
					intRetryCtr++;
					isSuccess	= AtpConnectUtil.updateFavListName(parameters);
				}
				intRetryCtr			= 0;
				
				if( ! isSuccess )
					throw new Exception(ErrorCodeConstants.FAIL_UPDATE_WATCHLIST);
			}catch(Exception e){
				e.printStackTrace();
				exception = e;
				return null;
			}
			return strWatchlistName;
		}

		@Override
		protected void onPostExecute(String strWatchlistName){
			
			try{
			if( dialog != null && dialog.isShowing() )
				dialog.dismiss();
				
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
			
					if( ! TheScreenerActivity.this.isFinishing() )
						alertException.show();
					return;
				}
	
				new AddWatchlistStock().execute(strWatchlistId);
			
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private StockInfo stockInfo;
	
	private class FetchStockInfo extends AsyncTask<String, Void, String[]> {
		private Exception exception 						= null;
		private int nextTaskId;
		
		@Override
		protected void onPreExecute() {
			Log.i(TAG, "FetchStockInfo Task");
			super.onPreExecute();
			
			if(!TheScreenerActivity.this.isFinishing()){
				dialog.setMessage("fetching StockInfo");
				dialog.show();
			}
		}

		@Override
		protected String[] doInBackground(String... params) {
			
			String[] symbolCodes 	= { params[0] };
			nextTaskId				= Integer.parseInt(params[1]);
			
			String response = null;
			try {
				intRetryCtr = 0;
				
				response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
				}
				intRetryCtr = 0;

				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);

			} catch (Exception e) {
				exception = e;
			}

			if(response != null)
				stockInfo = QcMessageParser.parseStockDetail(response);

			return symbolCodes;
		}

		@Override
		protected void onPostExecute(String[] result) {
			super.onPostExecute(result);

			if (dialog != null && dialog.isShowing())
				dialog.dismiss();

			if (exception != null) {
				alertException.setMessage(exception.getMessage());

				if (!TheScreenerActivity.this.isFinishing())
					alertException.show();
				return;
			} else{
				String strSymbolCode	= stockInfo.getSymbolCode();
				switch(nextTaskId){
				
				case TRADE_TASK_ID:
					if(strSymbolCode == null || strSymbolCode.isEmpty())
						showAlertDialog(getResources().getString(R.string.stock_not_found_screener_trade), false);
					else
						startTradeActivity(stockInfo.getSymbolCode(), Integer.toString(stockInfo.getSharePerLot()), stockInfo.getInstrument(), stockInfo.isPayableWithCPF());
					break;
					
				case WATCHLIST_TASK_ID:
					if(strSymbolCode == null || strSymbolCode.isEmpty())
						showAlertDialog(getResources().getString(R.string.stock_not_found_screener_watch), false);
					else
						new FetchWatchlistTask().execute(stockInfo.getSymbolCode());
					break;
					
				}
			}
		}
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message	= handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				
				if(loginThread != null)
					loginThread.stopRequest();
				
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				if( TheScreenerActivity.this.mainMenu.isShowing() )
					TheScreenerActivity.this.mainMenu.hide();
					
				if( !isFinishing() )
					TheScreenerActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (webview_the_screener.canGoBack() && (keyCode == KeyEvent.KEYCODE_BACK)) {
	    	// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if(this.mainMenu.isShowing())
	    		this.mainMenu.hide();
			
			webview_the_screener.goBack();
	    	finish();
	    	
	        return true;
	    }else if(keyCode==KeyEvent.KEYCODE_BACK){
	    	// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
    @Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
    // Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
}