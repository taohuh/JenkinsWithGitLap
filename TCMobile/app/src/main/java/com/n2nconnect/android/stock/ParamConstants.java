package com.n2nconnect.android.stock;


public class ParamConstants {

	public static final String USERNAME_TAG = "USERNAME";
	public static final String PASSWORD_TAG = "PASSWORD";
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
	public static final String RSA_PASSWORD_TAG = "RSAPASSWORD";
	public static final String E2EE_RANDOM_NUM_TAG = "E2EERANDOMNUM";
	public static final String E2EE_SESSION_ID_TAG = "E2EESESSIONID";
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
	public static final String USERPARAM_TAG = "USERPARAM";
	public static final String USERPARAM_INBYTE_TAG = "USERPARAMINBYTE";
	public static final String SENDERCODE_TAG = "SENDER_CODE";
	public static final String BROKERCODE_TAG = "BROKER_CODE";
	public static final String SECTORCODE_TAG = "SECTOR_CODE";
	public static final String SYMBOLCODE_TAG = "SYMBOL_CODE";
	public static final String LOT_SIZE_TAG = "LOT_SIZE";
	public static final String CURRENCY_TAG = "CURRENCY";
	public static final String NEWSID_TAG = "NEWS_ID";
	public static final String ADMINKEY_TAG = "ADMIN_KEY";
	public static final String CHART_DURATION_TAG = "CHART_DURATION";
	public static final String ORDER_DETAILS_TAG = "ORDER_DETAILS";
	public static final String ORDER_DISPLAYSEQ_TAG = "ORDER_DISPLAY_SEQ";
	public static final String ORDER_DETAILTYPE_TAG = "ORDER_DETAIL_TYPE";
	public static final String FROM_LOGIN_TAG = "FROM_LOGIN";
	public static final String TIMESTAMP_TAG = "TIME_STAMP";
	public static final String BID_ASK_PRICE_TAG = "BID_ASK_PRICE";
	// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12
	public static final String BID_ASK_QTY_TAG = "BID_ASK_QTY";
	public static final String INSTRUMENT_TAG = "INSTRUMENT";
	// Added by Mary@20130121 - Change_Request-20130104, ReqNo.3 - START
	public static final String RDS_VERSIONNO_TAG			= "RDS_VERSIONNO";
	public static final String RDS_AGREE_OR_DISAGREE_TAG	= "RDS_AGREE_OR_DISAGREE";
	// Added by Mary@20130121 - Change_Request-20130104, ReqNo.3 - END
	// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - START
	public static final String RDS_URL_TAG					= "RDS_URL_TAG";
	public static final String IS_BLOCK_BY_RDS_TAG			= "IS_BLOCK_BY_RDS_TAG";
	// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - END
	
	public static final String GO_TO_DATE_TAG = "GO_TO_DATE";
	public static final String ACCOUNTNO_TAG = "ACCOUNT_NO";
	public static final String CLIENTCODE_TAG = "CLIENT_CODE";
	public static final String BRANCHCODE_TAG = "BRANCH_CODE";
	public static final String EXCHANGECODE_TAG = "EXCHANGE_CODE";
	public static final String TRADINGPIN_TAG = "TRADING_PIN";
	public static final String TICKETNO_TAG = "TICKET_NO";
	public static final String ACTIONCODE_TAG = "ACTION_CODE";
	public static final String TRADEFORGET_NEWPWD_TAG = "TRADEFORGET_NEWPWD";
	public static final String FILTER_BY_EXCHG = "FILTER_BY_EXCHG";	
	public static final String PAYMENT_TYPE_TAG = "PAYMENT_TYPE";
	public static final String ORDER_TIMESTAMP_TAG = "ORDER_TIMESTAMP";
	public static final String ORDER_QUANTITY_TAG = "ORDER_QUANTITY";
	// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1 - START
	public static final String DISCLOSED_QUANTITY_TAG = "DISCLOSED_QUANTITY";
	public static final String MINIMUM_QUANTITY_TAG = "MINIMUM_QUANTITY";
	// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1 - END
	public static final String ORDER_MATCHEDQTY_TAG = "ORDER_MATCHED_QTY";
	// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.5
	public static final String ORDER_UNMATCHQTY_TAG = "ORDER_UNMATCH_QTY";
	public static final String ORDER_PRICE_TAG = "ORDER_PRICE";
	public static final String ORDER_SOURCE_TAG = "ORDER_SOURCE";
	public static final String ORDER_TYPE_TAG = "ORDER_TYPE";
	public static final String ORDER_STOPLIMIT_TAG = "ORDER_STOPLIMIT";
	public static final String ORDER_VALIDITY_TAG = "ORDER_VALIDITY";
	public static final String ORDERPAD_TYPE_TAG = "ORDER_PAD_TYPE";
	public static final String ORDER_NO_TAG = "ORDER_NO";
	public static final String ORDER_FIXEDNO_TAG = "ORDER_FIXED_NO";
	public static final String ORDER_EXPIRYDATE_TAG = "ORDER_EXPIRY_DATE";
	public static final String ORDER_STATUS_TAG = "ORDER_STATUS";
	public static final String PAYMENT_CURRENCY_TAG = "PAYMENT_CURRENCY";
	public static final String SEARCH_TEXT_TAG = "SEARCH_TEXT";
	public static final String FAVORATEID_TAG = "FAVORATE_ID";
	public static final String FAVORATENAME_TAG = "FAVORATE_NAME";
	public static final String PARAMSTRING_TAG = "PARAM_STRING";
	public static final String PASSWORD_TYPE_TAG = "PASSWORD_TYPE";
	public static final String INFO_TYPE_TAG = "INFO_TYPE";
	public static final String OLD_PASSWORD_TAG = "OLD_PASSWORD";
	public static final String NEW_PASSWORD_TAG = "NEW_PASSWORD";
	public static final String HINT_QUESTION_TAG = "HINT_QUESTION";
	public static final String HINT_ANSWER_TAG = "HINT_ANSWER";
	public static final String HINT_EMAIL_TAG = "HINT_EMAIL";
	public static final String PIN_NUMBER_TAG = "PIN_NUMBER";
	// Added by Mary@20120921 - Fixes_Request-20120815, ReqNo.3
	public static final String STOCK_CODE_TAG = "STOCK_CODE";
	// Added by Mary@20130108 - Fixes_Request-20121102, ReqNo.17
	public static final String ORI_ACTION_TYPE	= "ORI_ACTION_TYPE";
	
	//ADDED by Diyana@20160614  - ReqNo.1
	public static final String STOCK_NAME_TAG = "STOCK_NAME";
	
	// Added by Melissa
	public static final String MODE_TAG	= "MODE";
	public static final String BUY_SIDE = "B";
	public static final String SELL_SIDE = "S";
	
	// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4
	public static final String IS_PAYABLE_WITH_CPF = "IS_PAYABLE_WITH_CPF";
	//Kw@20130404 - Fixes_Request-20130314, ReqNo.11
	public static final String EXCEED_5_BID_IS_PROCEED  = "EXCEED_5_BID_IS_PROCEED";
	
	public static final String REVERT_LOGIN_TAG = "REVERT_LOGIN";
	
	public static final String CATEGORY_NAME_TAG = "CATEGORY_NAME";
//	public static final String NEWS_ITEMS_TAG = "NEWS_ITEMS";

	public static final String USERPARAM_KEY_TAG 					= "USERPARAM_KEY";
	public static final String USERPARAM_KEY_INBYTE_TAG 			= "USERPARAM_KEY";
	public static final String SYMBOLCODES_TAG 						= "SYMBOL_CODES";
	
	// Added by Mary@20120717 - to confirm is Short Sell Activity proceed
	public static final String SHORT_SELL_IS_PROCEED				= "SHORT_SELL_IS_PROCEED";
	
	// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7
	public static final String SHORT_SELL_IS_AWARE					= "SHORT_SELL_IS_AWARE";
	
	// Added by Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - START
	public static final String PARAM_FC_SOURCE_CODE					= "PARAM_FC_SRC_CD";
	public static final String PARAM_FC_SELECTED_VIEW				= "PARAM_FC_SELECTED_VIEW";
	public static final String PARAM_FC_REPORT_TYPE					= "PARAM_FC_RPT_TYPE";
	public static final String PARAM_FC_REPORT_PERIOD				= "PARAM_FC_RPT_PERIOD";
	public static final String PARAM_FC_CATEGORY					= "PARAM_FC_CATEGORY";
	
	public static final String PARAM_STR_FCIQ_FI_ANNOUNCEMENT		= "Announcement";
	public static final String PARAM_STR_FC_FI_SYNOPSIS				= "Synopsis";
	public static final String PARAM_STR_FC_FI_COMPANY_INFO			= "Info";
	public static final String PARAM_STR_FC_FI_KEY_PERSON			= "KeyPerson";
	public static final String PARAM_STR_FC_FI_SHAREHOLDER			= "ShareHolders";
	
	//Added by Kw@20120114 - Change Request -20120104, ReqNo.2 -START
	public static final String DEVICE_ID_TAG = "DEVICEID";
	public static final String OTP_VALUE_TAG = "OTPVALUE";
	public static final String OTP_IC_NUMBER = "ICNUMBER";
	
	// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14
	public static final String PARAM_1FA_VALUE						= "PARAM_1FA_VALUE";
	
	
	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
	public static int PAGE_NUMBER = 0;
	/* Mary@20130704 - Fixes_Request-20130523, ReqNo.27
	public static int PORTFOLIOTYPE_TAG =1;
	*/
	public static String PARAM_PORTFOLIOTYPE_TAG 					= "PARAM_PORTFOLIOTYPE_TAG";
	// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27 - START
	public static final int INT_PORTFOLIOTYPE_REALIZED				= 0;
	public static final int INT_PORTFOLIOTYPE_UNREALIZED			= 1;
	// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27 - END
	public static int IMAGESORT=0;//request 10
	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
	
	public static final int INT_PAGE_UNREALIZED						= 0;
	public static final int INT_PAGE_REALIZED						= 1;
	
	public static final int INT_PAGE_VIRTUALPL						= 0;
	public static final int INT_PAGE_SUMMARY						= 1;
	
	public static final int INT_PAGE_DAY							= 0;
	public static final int INT_PAGE_OVERNIGHT						= 1;
	
	public static final int PARAM_INT_VIEW_FC_FUNDAMENTAL_INFO		= 1;
	public static final int PARAM_INT_VIEW_FC_FINANCIAL_REPORT		= 2;
	// Added by Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - END
	
	// Added by Mary@20121112 - Fixes_Request-20121023, ReqNo.4
	public static final String PARAM_STR_RESTART_ACTIVITY			= "RESTART_ACTIVITY";
	
	// Added by Mary@20121113 - Change_Request-20120719, ReqNo.12
	public static final String PARAM_STR_TRD_EXCHG_CODE				= "PARAM_STR_TRD_EXCHG_CODE";
	public static final String PARAM_INT_EXCHG_IMAGE_ID				= "PARAM_INT_EXCHG_IMAGE_ID";
	public static final String PARAM_STR_EXCHG_NAME					= "PARAM_STR_EXCHG_NAME";
	
	// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - START
	public static final String PARAM_STR_DEVICE_OS_TYPE				= "PARAM_STR_DEVICE_OS_TYPE";
	public static final String PARAM_STR_DEVICE_OS_VERSION			= "PARAM_STR_DEVICE_OS_VERSION";
	public static final String PARAM_STR_DEVICE_MODEL				= "PARAM_STR_DEVICE_MODEL";
	public static final String PARAM_STR_DEVICE_CONNECTIVITY		= "PARAM_STR_DEVICE_CONNECTIVITY";
	public static final String PARAM_STR_SIM_OPERATOR_NAME			= "PARAM_STR_SIM_OPERATOR_NAME";
	public static final String PARAM_STR_EXTRA_INFO					= "PARAM_STR_EXTRA_INFO";
	public static final String PARAM_STR_SCREEN_SIZE				= "PARAM_STR_SCREEN_SIZE";
	// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - END
	
	// Added by Mary@20130829 - ChangeRequest-20120719, ReqNo.4 - START
	public static final String PARAM_BLN_HAS_ATP_CHANGE				= "PARAM_BLN_HAS_ATP_CHANGE";
	// Added by Mary@20130829 - ChangeRequest-20120719, ReqNo.4 - END
	
	//Added by Thinzar@20140707: Change_Request-20140701,ReqNo.1
	public static final String PARAM_PACKAGE_NAME 					= "PARAM_PACKAGE_NAME";
	
	//Added by Thinzar@20140815: Change_Request-20140801,ReqNo.4
	public static final String TRIGGER_PRICE_TYPE_TAG 			= "TRIGGER_PRICE_TYPE";
	public static final String TRIGGER_PRICE_DIRECTION_TAG 		= "TRIGGER_PRICE_DIRECTION";
	
	//Added by Thinzar@20140917, Change_Request-20140901,Reqno.6
	public static final String PUBLIC_KEY_TAG					= "public_key";
	public static final String PUBLIC_IP_TAG					= "public_ip";
	public static final String PRIVATE_IP_TAG					= "private_ip";
	public static final String SECONDARY_PORT_TAG				= "secondary_port";
	
	//Added by thinzar@20141027, Change_Request-20140901, ReqNo.12
	public static final String 	MARKETCODE_TAG					= "MARKET_CODE";
	
	//Added by Thinzar@20141106, Change_Request-20141101, ReqNo. 1
	public static final String SPLASH_ACTIVITY_TAG				= "SplashScreenActivity";
	public static final String LOGIN_ACTIVITY_TAG				= "LoginActivity";
	
	//Added by Thinzar@20141110, Change_Request-20140901, ReqNo. 9
	public static final String MULTI_LOGIN_TAG					= "MULTI_LOGIN";
	public static final String SINGLE_LOGIN_TAG					= "SINGLE_LOGIN";
	public static final String MIN_VERSION_NOT_MET_TAG			= "MIN_VER_NOT_MET";
	
	//Added by Thinzar@20141111, Change_Request-20141101, ReqNo. 3
	public static final String BROKER_ID_TAG					= "BROKER_ID";
	public static final String BROKER_NAME_TAG					= "BROKER_NAME";
	
	//Added by Thinzar, Change_Request-20160101, ReqNo. 6
	public static final String DATE_FROM_TAG					= "DATE_FROM";
	public static final String DATE_TO_TAG						= "DATE_TO";
	public static final String STOCK_PAGER_ID_TAG				= "STOCK_PAGER_ID";
	
	//Change_Request-20160722, ReqNo.3
	public class CHARTTYPE{
		public static final String chartTypeImage		= "IMAGE";
		public static final String chartTypeModulus		= "MODULUS";
		public static final String chartTypeTeleTrader	= "TELETRADER";
	}
	
	//Change_Request-20160722, ReqNo.2
	public static final String SCREENER_TYPE_INDIVIDUAL			= "INDIVIDUAL";
	public static final String SCREENER_TYPE_GLOBAL				= "GLOBAL";
	public static final String LOGIN_TYPE_TAG					= "LOGIN_TYPE";
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.8
	public static final String IS_REDIRECT_IBILLIONAIRE_TAG		= "IS_REDIRECT_IBILLIONAIRE";
	public static final String STOCK_CODE_IBILLIONAIRE_TAG		= "STOCK_CODE_IBILLIONAIRE";
	public static final String EXCHG_CODE_IBILLIONAIRE_TAG		= "EXCHG_CODE_IBILLIONAIRE";
	
	//Fixes_Requst-20161101, ReqNo.1
	public static final String FROM_EXCHANGE_TAG				= "FROM_EXCHANGE";
	
	//Change_Request-20160722, ReqNo.11
	public static final String PARAM_STR_LOGIN_TYPE				= "STR_LOGIN_TYPE";
	
	//Change_Request-20161101, ReqNo.2
	public static final String IS_PRIVATE_ORDER					= "IS_PRIVATE_ORDER";
	
	//Fixes_Request-20161101, ReqNo.14
	public static final String STATUS_CODE_TAG					= "STATUS_CODE";
	public static final String STATUS_MSG_TAG					= "STATUS_MSG";
	
	//Change_Request-20170119, ReqNo.3
	public static final String PACKAGE_ID_TAG					= "PACKAGE_ID";
	
	//Change_Request-20170119, ReqNo.4
	public static final String EXTERNAL_ORDER_SOURCE_TAG		= "EXTERNAL_ORDER_SOURCE";
	public static final String PARAM_ORDER_SOURCE_IB			= "IB";
	public static final String PARAM_ORDER_SOURCE_IS			= "IS";
	public static final String PARAM_ORDER_SOURCE_DTI			= "DTI";
	public static final String PARAM_ORDER_SOURCE_SGX			= "SGX";
	
	//Change_Request-20170119, ReqNo.5
	public static final String DTI_PRI_TAG						= "pri";
	public static final String DTI_STATUS_TAG					= "status";
	public static final String DTI_LIST_TAG						= "list";
	public static final String DTI_CLASS_TAG					= "class";
	public static final String DTI_EVENT_TYPE_NAME_TAG			= "event_type_name";
	public static final String DTI_EXP_PERCENT_MOVE_TAG			= "expected_percentage_move";
	public static final String DTI_ID_TAG						= "id";
	public static final String DTI_INSTRUMENT_TAG				= "instrument";
	public static final String DTI_INSTRUMENT_EXCHG_TAG			= "exchange_symbol";
	public static final String DTI_INSTRUMENT_CURRENCY_TAG		= "currency_symbol";
	public static final String DTI_INSTRUMENT_ID_TAG			= "id";
	public static final String DTI_INSTRUMENT_ISIN_TAG			= "isin";
	public static final String DTI_INSTRUMENT_NAME_TAG			= "name";
	public static final String DTI_INSTRUMENT_SYM_SOMMON_TAG	= "symbol_COMMON";
	public static final String DTI_PRICING_TAG					= "pricing";
	public static final String DTI_PRICING_CLOSE_TAG			= "close";
	public static final String DTI_PRICING_HIGH_TAG				= "high";
	public static final String DTI_PRICING_VOL_TAG				= "volume";
	public static final String DTI_TRADE_TYPE_TAG				= "trade_type";
	public static final String DTI_TRADE_HORIZON				= "trading_horizon";
	
	//to use for hashMap
	public static final String DTI_COUNTRY_TAG					= "DTI_COUNTRY";
	public static final String DTI_BG_RESID_TAG					= "DTI_BG_RESID";
	public static final String MAPPED_EXCHG_TAG					= "MAPPED_EXCHG";
	public static final String DTI_RESULT_ARRAYLIST_TAG			= "DTI_RESULT_ARRAYLIST";
	public static final String IS_FROM_DTI_TAG					= "IS_FROM_DTI";
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.8 - for touchID
	public static final String IS_PROCEED_TOUCHID_TAG			= "IS_PROCEED_TOUCHID";
	public static final String MOBILE_TOKEN_TAG					= "MOBILE_TOKEN";
	public static final String PRE_LOGIN_TOKEN_TAG				= "PRE_LOGIN_TOKEN";
	
	//Change_Request-20170601, ReqNo.2
	public static final String IS_EXPAND_FORM_TAG				= "IS_EXPAND_FORM";
	public static final String NEWS_SOURCE_TAG					= "NEWS_SOURCE";
	public static final String NEWS_CATEGORY_TAG				= "NEWS_CATEGORY";
	public static final String NEWS_CATEGORY_NAME_TAG			= "NEWS_CATEGORY_NAME";
	public static final String STOCK_NEWS_OBJ_TAG				= "STOCK_NEWS_OBJ";
	public static final String NEWS_KEYWORD_TAG					= "NEWS_KEYWORD";
	public static final String NEWS_TARGET_TAG					= "NEWS_TARGET";
	public static final String NEWS_FROM_DATE_TAG				= "NEWS_FROM_DATE";
	public static final String NEWS_TO_DATE_TAG					= "NEWS_TO_DATE";
	public static final String NEWS_IS_SEARCH_TAG				= "NEWS_IS_SEARCH";

	//Change_Request-20170601, ReqNo.10
	public static final String IS_TRIGGER_SMS_TAG				= "IS_TRIGGER_SMS";
	public static final String DEVICE_TYPE_2FA_TAG				= "DEVICE_TYPE_2FA";
	public static final String CHALLENGE_CODE_2FA_TAG			= "CHALLENGE_CODE_2FA";

	//Change_Request-20170601, ReqNo.14
	public static final String PK_MODULUS_TAG					= "PK_MODULUS";
	public static final String PK_EXPONENT_TAG 					= "PK_EXPONENT";
	public static final String PRIMARY_PORT_TAG 				= "PRIMARY_PORT";

	//Change_Request-20170601, ReqNo.18
	public static final String IB_TYPE_INDIVIDUAL				= "INDIVIDUAL";
	public static final String IB_TYPE_GLOBAL					= "GLOBAL";

	public static final String FINGERPRINT_DIALOG_TITLE_TAG		= "FINGERPRINT_DIALOG_TITLE";
}