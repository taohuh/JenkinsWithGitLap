package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.ExchangeListView;
import com.n2nconnect.android.stock.custom.ExchangeListView.OnExchangListEventListener;
import com.n2nconnect.android.stock.model.Sector;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class TradePreferenceActivity extends CustomWindow 
	implements OnDoubleLoginListener, OnExchangListEventListener {

	private ProgressDialog dialog;
	private DoubleLoginThread loginThread;
	private RadioGroup rgTradeQtyMeasure;
	private RadioGroup rgAutoFillTradeQty;
	private EditText etCustomTradeQty;
	private TextView tvTradeQtyMeasureDesc;
	private TextView tvTradeQtyDesc;
	private TextView tvAutoFillTradePriceDesc;
	private RadioButton rbTradeQtyInLot;
	private RadioButton rbTradeQtyInUnit;
	private RadioButton rbAutoFillTradeQty;
	private RadioButton rbCustomTradeQty;
	private ToggleButton tbAutoFillTradePrice;
	
	private String strBeforeEdit	= null;
	
	// Added by Mary@20121112 - Change_Request-20120719, ReqNo.12 - START
	private ExchangeListView exchgListView;
	private Button btnExchangeType;
	private Map<String, Object> selectedExchangeMap;
	// Added by Mary@20121112 - Change_Request-20120719, ReqNo.12 - END

	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.trade_preferences);
	
			this.title.setText(getResources().getString(R.string.trade_pref_module_title));
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntAccSettingMenuIconBgImg() );
			
			loginThread				= ((StockApplication) TradePreferenceActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) TradePreferenceActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(TradePreferenceActivity.this);
					
			dialog 					= new ProgressDialog(TradePreferenceActivity.this);
			
			tvTradeQtyMeasureDesc	= (TextView) findViewById(R.id.tvTradeQtyMeasurementDesc);
			tvTradeQtyDesc			= (TextView) findViewById(R.id.tvTradeQtyDesc);
			tvAutoFillTradePriceDesc= (TextView) findViewById(R.id.tvAutoFillTradePriceDesc);
			
			etCustomTradeQty		= (EditText) findViewById(R.id.etCustomTradeQty);
			rbTradeQtyInLot			= (RadioButton) findViewById(R.id.rbTradeQtyInLot);
			rbTradeQtyInUnit		= (RadioButton) findViewById(R.id.rbTradeQtyInUnit);
			rbAutoFillTradeQty		= (RadioButton) findViewById(R.id.rbAutoFillTradeQty);
			rbCustomTradeQty		= (RadioButton) findViewById(R.id.rbCustomTradeQty);
			tbAutoFillTradePrice	= (ToggleButton) findViewById(R.id.tbAutoFillTradePrice);
			
			rgTradeQtyMeasure		= (RadioGroup) findViewById(R.id.rgTradeQtyMeasurement);
			rgTradeQtyMeasure.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
	            public void onCheckedChanged(RadioGroup group, int checkedId) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					AppConstants.brokerConfigBean.setIntTradeQtyMeasurement(checkedId == R.id.rbTradeQtyInLot ? AppConstants.INT_TRADE_QTY_IN_LOT : AppConstants.INT_TRADE_QTY_IN_UNIT);
					
					toggleTradeQtySection();
				}
			});
			
			rgAutoFillTradeQty	= (RadioGroup) findViewById(R.id.rgTradeQty);
			rgAutoFillTradeQty.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
	            public void onCheckedChanged(RadioGroup group, int checkedId) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					AppConstants.brokerConfigBean.isAutoFillTradeQuantity( checkedId == R.id.rbAutoFillTradeQty ? true : false);
					
					toggleTradeQtySection();
				}
			});
			
			tbAutoFillTradePrice.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v){
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					AppConstants.brokerConfigBean.isAutoFillTradePrice( tbAutoFillTradePrice.isChecked() );
					
					toggleTradePriceSection();
				}
			});
			
			etCustomTradeQty.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable editable) {
					int intText	= 0;
					
					if( editable.length() > 0 && !editable.toString().equals(strBeforeEdit) ){
						intText	= Integer.parseInt( editable.toString() );
						
						editable.replace(0, editable.length(), String.valueOf(intText) );
						
						AppConstants.brokerConfigBean.setIntCustomTradeQuantity(intText);
					}
				}
	
				public void beforeTextChanged(CharSequence arg0, int start, int count, int after) {
					String strTemp	= arg0.toString();
					if(strBeforeEdit == null || ( strBeforeEdit != null && !strBeforeEdit.equals(strTemp) ) )
						strBeforeEdit	= strTemp;
					
					strTemp			= null;
				}
	
				public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {			
				}
	
			});
			
			// Added by Mary@20121106 - Change_Request-20120719, ReqNo.12 - START
			btnExchangeType 		= (Button)findViewById(R.id.btnExchangeType);
			// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - END
			
			/* comment out for TCMobile 2.0 enhancements
			if(! AppConstants.hasMultiLogin ){
				( (RelativeLayout)findViewById(R.id.rl_TradeExchange) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBgImg() );
				btnExchangeType.setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );
			}
			*/
			
			btnExchangeType.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					TradePreferenceActivity.this.doExchangeList();				
				}        	
			});
			
			exchgListView = new ExchangeListView(TradePreferenceActivity.this, TradePreferenceActivity.this, getLayoutInflater(), null);
			exchgListView.setHideOnSelect(true);
			// Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
			
			//Added by Thinzar@20140904, Fixes_Request-20140820, ReqNo.14 - START
			if(!AppConstants.hasEquityExchange){	
				TableRow trTradeQtyMeasurement 		= (TableRow)findViewById(R.id.trTradeQtyMeasurement);
				TableRow trTradeQtyMeasurementDesc 	= (TableRow)findViewById(R.id.trTradeQtyMeasurementDesc);
				trTradeQtyMeasurement.setVisibility(View.GONE);
				trTradeQtyMeasurementDesc.setVisibility(View.GONE);
			}
			//Added by Thinzar@20140904, Fixes_Request-20140820, ReqNo.14 - END
			
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}
	
	public void onResume(){
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			super.onResume();
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= TradePreferenceActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(TradePreferenceActivity.this);
				loginThread.resumeRequest();
			}
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
			
			if( dialog != null && dialog.isShowing() )
				dialog.dismiss();
			
			dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
			/* Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
			*/
			if( (idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) ) && ( ! TradePreferenceActivity.this.isFinishing() ) )
				dialog.show();
			
			List<Map<String, Object>> listExchangeMap		= new ArrayList<Map<String, Object>>();
			try{
				Map<String, Integer> mapExchgImages = DefinitionConstants.getExchangeImageMapping();
				List<String> listQCExchgCode		= DefinitionConstants.getValidQCExchangeCodes();
	
				for(int i=0; i<listQCExchgCode.size(); i++){
					String strExchgCode		= DefinitionConstants.getTrdExchangeCode().get(i);
					String strQCExchgCode	= listQCExchgCode.get(i);
					String strExchgName		= DefinitionConstants.getExchangeNameMapping().get(strQCExchgCode);
					
					Map<String, Object> exchgMap			= new HashMap<String, Object>();
					exchgMap.put(ParamConstants.PARAM_STR_TRD_EXCHG_CODE, strExchgCode);
					exchgMap.put(ParamConstants.PARAM_STR_EXCHG_NAME, strExchgName);
					exchgMap.put(ParamConstants.PARAM_INT_EXCHG_IMAGE_ID, mapExchgImages.get(strQCExchgCode) == null ? null : ( mapExchgImages.get(strQCExchgCode) ).intValue() );
					
					listExchangeMap.add(exchgMap);
				}
				
				exchgListView.setListExchangeMap(listExchangeMap);
			} catch (Exception e) {			
				e.printStackTrace();
			}
			
			reinitPage();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			if(dialog != null && dialog.isShowing() )
				dialog.dismiss();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	
	public void onPause(){
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onPause();
			
			cacheTradePreferenceSetting();
			
			// Added by Mary@20121112 - Change_Request-20120719, ReqNo.4
			AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, null);
		
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}	
	
	
	public void exchangeListSelectedEvent(Map<String, Object> exchangeMap, Sector sector) {
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
		TradePreferenceActivity.this.cacheTradePreferenceSetting();
		
		selectedExchangeMap = exchangeMap;

		btnExchangeType.setText( String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );
		
		// Added by Mary@20121112 - Change_Request-20121112, ReqNo.12 - START
		Map<String, String> mapTradePreference	= new HashMap<String, String>();
		mapTradePreference.put( PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) ) );
		AppConstants.cacheBrokerUserDetails(this, sharedPreferences, mapTradePreference);
		mapTradePreference.clear();
		mapTradePreference		= null;
		// Added by Mary@20121112 - Change_Request-20121112, ReqNo.12 - END

		TradePreferenceActivity.this.reinitPage();
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
			else
				finish();
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	// Added by Mary@20121106 - Change_Request-20120719, ReqNo.12 - START
	private void doExchangeList() {
		if( exchgListView.isShowing() ){
			exchgListView.hide();
		}else{
			if( mainMenu.isShowing() )
				mainMenu.hide();
			
			exchgListView.show(findViewById(R.id.tl_tradeQty), selectedExchangeMap);
		}
	}
	// Added by Mary@20121106 - Change_Request_20120719, ReqNo.12 - END
	
	public void reinitPage(){
		SharedPreferences sharedPreferences	= null;
		/* Mary@20130604 - comment unused variable
		String strTemp						= null;
		*/
		try{
			sharedPreferences			= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
			
			// Mary@20121112 - Change_Request-20120719, ReqNo.12 - START
			String strTradePreferenceExchgCode	= AppConstants.getCachedBrokerUserDetail(this, sharedPreferences, PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE);
			if(strTradePreferenceExchgCode == null){
				String strTradeExchgCode				= AppConstants.getCachedBrokerUserDetail(this, sharedPreferences, PreferenceConstants.TRADE_EXCHANGE_CODE);
				strTradePreferenceExchgCode				= strTradeExchgCode == null ? AppConstants.DEFAULT_EXCHANGE_CODE : strTradeExchgCode;
				Map<String, String> mapTradePreference	= new HashMap<String, String>();
				mapTradePreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, strTradePreferenceExchgCode);
				AppConstants.cacheBrokerUserDetails(this, sharedPreferences, mapTradePreference);
				
				mapTradePreference.clear();
				mapTradePreference				= null;
				strTradeExchgCode				= null;
			}
			
			if (selectedExchangeMap == null) {
				for( Iterator<Map<String, Object>> itr=exchgListView.getListExchangeMap().iterator(); itr.hasNext(); ){
					Map<String, Object> mapExchange		= itr.next();
					String strExchgCode	= String.valueOf( mapExchange.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
					if( strTradePreferenceExchgCode.equals(strExchgCode) ){
						selectedExchangeMap = mapExchange;
						break;
					}
				}
			}
			btnExchangeType.setText( String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );
			// Mary@20121112 - Change_Request-20120719, ReqNo.12 - END
			
			// Added by Mary@20121114 - Fixes_Request-20120719, ReqNo.12
			AppConstants.updateTradePreferenceAccordanceToCurrentExchange(this, sharedPreferences);			
			
			toggleTradeQtySection();
			
			toggleTradePriceSection();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			sharedPreferences	= null;
			/* Mary@20130604 - comment unused variable
			strTemp				= null;
			*/
		}
	}
	
	public void toggleTradePriceSection(){
		if( AppConstants.brokerConfigBean.autoFillTradePrice() )
			tvAutoFillTradePriceDesc.setText(R.string.autofill_trade_price_desc);
		else
			tvAutoFillTradePriceDesc.setText(R.string.manual_trade_price_desc);
		
		tbAutoFillTradePrice.setChecked( AppConstants.brokerConfigBean.autoFillTradePrice() );
	}
	
	public void toggleTradeQtySection(){
		/* a) Qty Measurement */
		if(AppConstants.hasEquityExchange){	//If filter added by Thinzar, Fixes_Request-20140820, ReqNo.14
			if(AppConstants.brokerConfigBean.getIntTradeQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT){
				tvTradeQtyMeasureDesc.setText(R.string.trade_qty_in_lot_desc);
				rbTradeQtyInLot.setChecked(true);
			}else{
				tvTradeQtyMeasureDesc.setText(R.string.trade_qty_in_unit_desc);
				rbTradeQtyInUnit.setChecked(true);
			}
		}

		/* b) Auto Fill / Custom Qty */
		if( AppConstants.brokerConfigBean.autoFillTradeQuantity() ){
			rbAutoFillTradeQty.setChecked(true);
			etCustomTradeQty.setEnabled(false);
			etCustomTradeQty.setVisibility(View.GONE);
			tvTradeQtyDesc.setText(R.string.autofill_trade_qty_desc);
		}else{
			rbCustomTradeQty.setChecked(true);
			etCustomTradeQty.setEnabled(true);
			etCustomTradeQty.setVisibility(View.VISIBLE);
			if(AppConstants.brokerConfigBean.getIntTradeQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT)
				tvTradeQtyDesc.setText(R.string.custom_trade_qty_in_lot_desc);
			else
				tvTradeQtyDesc.setText(R.string.custom_trade_qty_in_unit_desc);	
		}
		etCustomTradeQty.setText( String.valueOf( AppConstants.brokerConfigBean.getIntCustomTradeQuantity() ) );

	}
	
	public void cacheTradePreferenceSetting(){
		SharedPreferences sharedPreferences			= null;
		Map<String, String> mapBrokerUserPreference	= new HashMap<String, String>();
		try{
			sharedPreferences					= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
			
			AppConstants.brokerConfigBean.setIntCustomTradeQuantity( Integer.parseInt( etCustomTradeQty.getText().toString() ) );
			AppConstants.brokerConfigBean.isAutoFillTradePrice( tbAutoFillTradePrice.isChecked() );
			if(AppConstants.hasEquityExchange){	//If filter added by Thinzar, Fixes_Request-20140820, ReqNo.14
				AppConstants.brokerConfigBean.setIntTradeQtyMeasurement( ( (RadioButton)findViewById(R.id.rbTradeQtyInLot) ).isChecked() ? AppConstants.INT_TRADE_QTY_IN_LOT : AppConstants.INT_TRADE_QTY_IN_UNIT);
			}
			AppConstants.brokerConfigBean.isAutoFillTradeQuantity( ( (RadioButton)findViewById(R.id.rbAutoFillTradeQty) ).isChecked() ? true : false);
			
			mapBrokerUserPreference.put(PreferenceConstants.IS_AUTOFILL_TRADE_PRICE, String.valueOf( AppConstants.brokerConfigBean.autoFillTradePrice() ) );
			mapBrokerUserPreference.put(PreferenceConstants.IS_AUTOFILL_TRADE_QUANTITY, String.valueOf( AppConstants.brokerConfigBean.autoFillTradeQuantity() ) );
			mapBrokerUserPreference.put(PreferenceConstants.CUSTOMED_TRADE_QUANTITY, String.valueOf( AppConstants.brokerConfigBean.getIntCustomTradeQuantity() ) );
			mapBrokerUserPreference.put(PreferenceConstants.TRADE_QUANTITY_MEASUREMENT, String.valueOf( AppConstants.brokerConfigBean.getIntTradeQtyMeasurement() ) );
			
			AppConstants.cacheBrokerUserTradePreference(TradePreferenceActivity.this, sharedPreferences, mapBrokerUserPreference);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			sharedPreferences		= null;
			mapBrokerUserPreference.clear();
			mapBrokerUserPreference	= null;
		}
	}
	
	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(TradePreferenceActivity.this.mainMenu.isShowing()){
					TradePreferenceActivity.this.mainMenu.hide();
				}
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				
				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					TradePreferenceActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};
}
