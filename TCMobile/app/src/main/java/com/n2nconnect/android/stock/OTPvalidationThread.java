package com.n2nconnect.android.stock;

//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 START
public class OTPvalidationThread {
	
	private OtpValidationListener registeredListener;
	
	
	public OtpValidationListener getRegisteredListener() {
		return registeredListener;
	}
	
	public void setRegisteredListener(OtpValidationListener registeredListener) {
		this.registeredListener = registeredListener;
	}
	
	public interface OtpValidationListener {
		public void OtpValidationEvent(boolean isValid,String message);

	
	}
}
