package com.n2nconnect.android.stock.model;

import com.n2nconnect.android.stock.AppConstants;

public class BusinessDone {

	private float price;
	private long sVolume;
	private long bVolume;
	private long totalVolume;
	private long totalValue;
	private String stockCode;
	private String stockShortName;
	private float LACP;
	private long totalBTranVol;
	private long totalSTranVol;
	private long totalTradeVol;
	private long totalTradeVal;
	
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	public long getSVolume() {
		return sVolume;
	}
	public void setSVolume(long sVolume) {
		this.sVolume = sVolume;
	}
	
	public long getBVolume() {
		return bVolume;
	}
	public void setBVolume(long bVolume) {
		this.bVolume = bVolume;
	}
	
	public long getTotalVolume() {
		return totalVolume;
	}
	public void setTotalVolume(long totalVolume) {
		this.totalVolume = totalVolume;
	}
	
	public long getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(long totalValue) {
		this.totalValue = totalValue;
	}
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	
	public String getStockShortName() {
		return stockShortName;
	}
	public void setStockShortName(String stockShortName) {
		this.stockShortName = stockShortName;
	}
	
	public float getLACP(){
		return LACP;
	}
	public void setLACP(float LACP){
		this.LACP = LACP;
	}
	
	public long getTotalBuyTransVolume() {
		return totalBTranVol;
	}
	public void setTotalBuyTransVolume(long totalBTranVol) {
		this.totalBTranVol = totalBTranVol;
	}
	
	public long getTotalSellTransVolume() {
		return totalSTranVol;
	}
	public void setTotalSellTransVolume(long totalSTranVol) {
		this.totalSTranVol = totalSTranVol;
	}
	
	public long getTotalTradeVolume() {
		return totalTradeVol;
	}
	public void setTotalTradeVolume(long totalTradeVol) {
		this.totalTradeVol = totalTradeVol;
	}
	
	public long getTotalTradeValue() {
		return totalTradeVal;
	}
	public void setTotalTradeValue(long totalTradeVal) {
		this.totalTradeVal = totalTradeVal;
	}
	
	//Added by Thinzar@20140402, Change_Request-20150402, ReqNo. 2 - START
	//@int viewPreference: 0=unit, 1=lot
	public long getSVolume(int viewPreference, int sharePerLot) {
		if(viewPreference == AppConstants.INT_TRADE_QTY_IN_UNIT)
			return sVolume;
		else
			return sVolume/sharePerLot;
	}
	
	public long getBVolume(int viewPreference, int sharePerLot) {
		if(viewPreference == AppConstants.INT_TRADE_QTY_IN_UNIT)
			return bVolume;
		else
			return bVolume/sharePerLot;
	}
	
	public long getTotalVolume(int viewPreference, int sharePerLot) {
		if(viewPreference == AppConstants.INT_TRADE_QTY_IN_UNIT)
			return totalVolume;
		else
			return totalVolume/sharePerLot;	
	}
	//Added by Thinzar@20140402, Change_Request-20150402, ReqNo. 2 - END
	
}
