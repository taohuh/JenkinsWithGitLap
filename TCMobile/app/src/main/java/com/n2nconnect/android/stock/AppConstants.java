package com.n2nconnect.android.stock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.n2nconnect.android.stock.model.BrokerPlistConfigBean;
import com.n2nconnect.android.stock.model.CurrencyExchangeInfo;
import com.n2nconnect.android.stock.model.DervTradePrtfSubDetailRpt;
import com.n2nconnect.android.stock.model.RDSInfo;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.content.Context;
import android.content.SharedPreferences;

public class AppConstants {
	public static final int CHANGE_PASSWORD_TYPE		= 0;
	public static final int CHANGE_PIN_TYPE 			= 1;

	public static final int INFO_ABOUT_TYPE 			= 0;
	public static final int INFO_TERM_TYPE 				= 1;
	public static final int INFO_PRIVACY_TYPE 			= 2;
	public static final int INFO_WARRANTY_TYPE 			= 3;
	// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8
	public static final int INFO_SERVICE_TYPE			= 4;
	// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - START
	public static final int INFO_SUBSCRIBE_SERVICE_TYPE	= 5;
	public static final int INFO_SUBSCRIBE_STATUS_TYPE	= 6;
	// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - END

	public static final int ORDER_DETAIL_REVISE 		= 0;
	public static final int ORDER_DETAIL_SUBMIT 		= 1;
	public static final int PORTFOLIO_DETAIL_TRADE 		= 2;

	public static final int ORDER_PAD_TRADE 			= 0;
	public static final int ORDER_PAD_REVISE 			= 1;

	public static final int TRADE_BUY_ACTION 			= 0;
	public static final int TRADE_SELL_ACTION 			= 1;
	public static final int TRADE_REVISE_ACTION 		= 2;
	public static final int TRADE_CANCEL_ACTION 		= 3;

	public static final int ORDER_DETAILS_REQUEST 		= 0;
	public static final int SELECT_ACCOUNT_REQUEST 		= 1;
	public static final int DERIVATIVE_DETAIL_REQUEST	= 2;
	// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3
	public static final int RDS_REQUEST 				= 2;
	// Added by Mary@20130829 - ChangeRequest-20120719, ReqNo.4
	public static final int SELECT_BROKER_HOUSE_REQUEST	= 3;
	public static final int SELECT_ORDER_HISTORY_REQUEST= 4;

	public static final int CHART_DURATION_1_DAY 		= 0;
	public static final int CHART_DURATION_1_MONTH 		= 1;
	public static final int CHART_DURATION_3_MONTHS		= 2;
	public static final int CHART_DURATION_6_MONTHS 	= 3;
	public static final int CHART_DURATION_1_YEAR 		= 4;
	public static final int CHART_DURATION_2_YEARS 		= 5;
	public static final int CHART_DURATION_MKT_SUMM		= 999;	//added by thinzar, Change_Request-20160722, ReqNo. 5

	public final static int TRADE_INPUT_PRICE 			= 0;
	public final static int TRADE_INPUT_QUANTITY 		= 1;
	public final static int TRADE_INPUT_STOPLIMIT 		= 2;
	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
	public final static int TRADE_INPUT_MINIMUM_QUANTITY= 3;
	public final static int TRADE_INPUT_DISCLOSED_QTY	= 4;
	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END

	public final static int TRADE_LIST_ORDER 			= 0;
	public final static int TRADE_LIST_VALIDITY 		= 1;
	public final static int TRADE_LIST_PAYMENT			= 2;
	public final static int TRADE_LIST_CURRENCY 		= 3;

	// Added by Mary@20120827 - ChangeRequest-20120719, ReqNo.12 - START
	public static final int INT_TRADE_QTY_IN_UNIT		= 0;
	public static final int INT_TRADE_QTY_IN_LOT		= 1;
	public static int DEFAULT_TRADE_QTY_TYPE			= 1;
	// Added by Mary@20120827 - ChangeRequest-20120719, ReqNo.12 - END

	public static final String ERROR_STRING 			= "ERROR";

	public static final String ATP_SENDER_CODE 			= "[SenderCode]";
	public static final String ATP_BROKER_CODE 			= "[BrokerCode]";
	public static final String USER_PARAM_CODE 			= "[UserParam]";

	public static final String STRING_ENCODING_FORMAT 	= "ISO-8859-1";
	public static final String DATA_STORE 				= "APP_DATA";

	public static final String DEFAULT_MARKET_CODE 		= "020000000.KL";
	public static final String DEFAULT_SECTOR_CODE 		= "10";

	public static final String MY_EXCHANGE_CODE = "MY";
	public static final String YM_EXCHANGE_CODE = "YM";
	public static final String ES_EXCHANGE_CODE = "ES";
	public static final String ZS_EXCHANGE_CODE = "ZS";
	public static final String UC_EXCHANGE_CODE = "UC";
	// Added by Mary@20130312 - Fixes_Request-20130108, ReqNo.19
	public static final List<String> LIST_DERIVATIVES_EXCHG_CODE = new ArrayList<String>();
	public static final String ADMIN_KEY 				= "0108";
	public static final int DEFAULT_REFRESH_RATE 		= 5;

	public static final int TRADE_KEEP_ALIVE_RATE 		= 1200;

	public static final int CHECK_DOUBLE_LOGIN_RATE 	= 5;
	// Added by Mary@20130939 - Fixes_Request-20130711, ReqNo.27
	public static final int DEFAULT_QC_SERVER_PULL_PORT	= 80; 

	public static int QC_KEEP_ALIVE_RATE 				= 1500;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	public static int intMaxRetry						= 3;

	public static boolean noTradeClient 				= false;
	public static boolean FLAG_DMA 						= false;
	public static boolean SSLEnable 					= false;

	public static boolean debug 						= true;
	public static boolean showMenu 						= true;

	public static boolean atpCompress 					= false;
	public static boolean qcCompress 					= false;		

	public static String senderCode 					= null;
	public static String userParam 						= null;
	public static String encryptedKey 					= null;
	public static String QCUserParamKey 				= null;
	public static String QC_SERVER_IP 					= null;
	// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3
	public static int QC_SERVER_PORT					= 80;
	public static String MAP_QC_IP						= null;
	public static String TNCInfo 						= null;

	public static String DEFAULT_EXCHANGE_CODE 			= "KL";

	// Added by Sonia@20130823 - Change_Request-20130225, ReqNo.5
	public static String EXCHANGECODE_PH				= "PH";

	// Added by Mary@20121227 - Fixes_Request-20121102, ReqNo.9
	private static String DEFAULT_CURRENCY_CODE			= null;

	public static byte[] userParamInByte;

	public static Map<String,String>News;
	public static Map<String, String> mapQCIp			= new HashMap<String, String>();
	// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3
	public static Map<String, Integer> mapQCPort		= new HashMap<String, Integer>();

	public static TradeAccount selectedAcc 				= null;

	// Added by Mary@20130620 - Change_Request-20130424,ReqNo.9
	public static Map<String, String>mapFCBitMode		= new HashMap<String, String>();
	// Added by Kw@20130222 - Change_Request20130104, ReqNo.6
	public static Map<String,Integer>marketDepth		= new HashMap<String,Integer>();
	// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
	public static Map<String,Integer>clientLimitOption	 = new HashMap<String,Integer>();

	public static QCBean QCdata							= new QCBean(); // {<response_string>, <userparam>, <aliveTimeOut>}

	// Added by Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - START
	public static String strDefaultSourceCode			= "CPIQ";
	public static String strDefaultReportType			= "Annual";
	public static String strDefaultReportPeriod			= "INC";
	// Added by Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - END

	// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.10 - START
	public static boolean hasDisclaimerShown			= false;
	// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.10 - END

	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
	//public static BrokerConfigBean brokerConfigBean		= null; // NOTE : if NO multi login then set it to your white labeling broker config class e.g. ApexBrokerConfigBean()
	//public static final boolean hasMultiLogin			= brokerConfigBean == null ? true : false;
	//Thinzar@20140702: comment out above two lines and replace with below
	public static BrokerPlistConfigBean brokerConfigBean = null;
	public static boolean hasMultiLogin = false;	//this value will be set from config file and save to sharedPref
	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END

	// Added by kw@2012@20120817 - Change Request-20120719, ReqNo.9
	public static final ArrayList<String> alAllSectorCode= new ArrayList<String>();

	// Added by Mary@20121025 - Fixes_Request-20121023, ReqNo.11
	public static final String PARAM_DATE_FORMAT		= "dd-MM-yyyy";

	// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - START
	private static boolean hasResetPwd					= false;
	private static boolean hasResetPin					= false;
	// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - END

	// Added by Kw@20130116 - Change_Request20130104, ReqNo.2 - START
	public static Info2FA info2FA = new Info2FA();
	public static String selectedDeviceID ="";
	public static String otpValue="";
	// Added by Kw@20130116 - Change_Request20130104, ReqNo.2 - END

	// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3
	private static RDSInfo rdsInfo;

	// Added by Sonia@20130823 - Change_Request-20130225, ReqNo.5 - START
	public static int MAXRECORDNO = 0;
	public static int MINRECORDNO = 0;
	public static int FINAL_LASTRECORDNO = 0;
	// Added by Sonia@20130823 - Change_Request-20130225, ReqNo.5 - END

	// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
	public static boolean hasLogout	= false;

	// Added by Mary@20130830 - ChangeRequest-20120719, ReqNo.4 - START	
	public final static ENUM_BROKER_HOUSE_LIST_ORDER enumBrokerHouseListOrder	
	= ENUM_BROKER_HOUSE_LIST_ORDER.ASCENDING;
	public static enum ENUM_BROKER_HOUSE_LIST_ORDER 	{ RANDOM, ASCENDING, DESCENDING }
	// Added by Mary@20130830 - ChangeRequest-20120719, ReqNo.4 - END

	//Added by Thinzar@20140903 - Fixes_Request-20140820, ReqNo.14
	public static boolean hasEquityExchange				= false;

	public static Map<String, String> map = new HashMap<String, String>();

	// Added by Mary@20130312 - Fixes_Request-20130108, ReqNo.19 - START
	public static List<String> getListDerivativesExchgCode(){
		if(LIST_DERIVATIVES_EXCHG_CODE.size() == 0){
			LIST_DERIVATIVES_EXCHG_CODE.add(AppConstants.MY_EXCHANGE_CODE);
			LIST_DERIVATIVES_EXCHG_CODE.add(AppConstants.YM_EXCHANGE_CODE);
			LIST_DERIVATIVES_EXCHG_CODE.add(AppConstants.ES_EXCHANGE_CODE);
			LIST_DERIVATIVES_EXCHG_CODE.add(AppConstants.ZS_EXCHANGE_CODE);
			LIST_DERIVATIVES_EXCHG_CODE.add(AppConstants.UC_EXCHANGE_CODE);
		}

		return LIST_DERIVATIVES_EXCHG_CODE;
	}
	// Added by Mary@20130312 - Fixes_Request-20130108, ReqNo.19 - END

	// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3 - START	
	public static RDSInfo getRDSInfo(){
		return rdsInfo;
	}
	public static void setRDSInfo(RDSInfo rdsInfo){
		AppConstants.rdsInfo = rdsInfo;
	}
	// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3 - END

	public static void setDefaultExchangeCode(String exchgCode) {
		DEFAULT_EXCHANGE_CODE = exchgCode;
	}

	// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - START
	public static boolean hasResetPwd() {
		return hasResetPwd;
	}

	public static void setHasResetPwd(boolean hasResetPwd) {
		AppConstants.hasResetPwd = hasResetPwd;
	}

	public static boolean hasResetPin() {
		return hasResetPin;
	}

	public static void setHasResetPin(boolean hasResetPin) {
		AppConstants.hasResetPin = hasResetPin;
	}
	// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - END

	public static void setEncryptedKey(String key) {
		encryptedKey = key;
	}

	public static void setQCIP(String ExchgCode, String QCIP) {

		// Added by Mary@20130711 - Fixes_Request-20130711, ReqNo.1
		try{
			mapQCIp.put( ExchgCode, QCIP.split(":")[1] );
			// Added by Mary@20130711 - Fixes_Request-20130711, ReqNo.1 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130711 - Fixes_Request-20130711, ReqNo.1 - END
	}

	// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3 - START
	public static void setQCPort(String strExchgCode, String strQC){
		//e.g.,strQC = "0:49.236.201.87:20000:80:"

		try{
			/* Mary@20130939 - Fixes_Request-20130711, ReqNo.27 - START
				mapQCPort.put( strExchgCode, Integer.parseInt( strQC.split(":")[3].trim() ) );
			 */
			int intPort;
			int primary_port	= 0;
			int secondary_port	= 0;
			String[] strQCArr = strQC.split(":");

			//Added by Thinzar, Change_Request-20150901, ReqNo3 - START
			if(strQCArr.length > 3 && strQCArr[3].trim().length() > 0) 
				primary_port	= Integer.parseInt( strQCArr[3].trim() );
			if(strQCArr.length > 2 && strQCArr[2].trim().length() > 0)
				secondary_port	= Integer.parseInt( strQCArr[2].trim() );

			if(strExchgCode.equalsIgnoreCase("PH") || strExchgCode.equalsIgnoreCase("PHD")){
				//special case for Philippines exchange because PSE UAT port is 8001
				if(primary_port > 0)			intPort	= primary_port;
				else if(secondary_port > 0)		intPort	= secondary_port;
				else							intPort	= DEFAULT_QC_SERVER_PULL_PORT;

				QC_SERVER_PORT = intPort;
			} else {
				intPort	= (primary_port > 0)? primary_port: DEFAULT_QC_SERVER_PULL_PORT;
			}
			//Added by Thinzar, Change_Request-20150901, ReqNo3 - END

			/* Commented out by Thinzar, Change_Request-20150901, ReqNo3
				int intPort	= ( strQC.split(":") != null 
									&& strQC.split(":").length > 3  
									&& strQC.split(":")[3] != null 
									&& strQC.split(":")[3].trim().length() > 0 
								) ? Integer.parseInt( strQC.split(":")[3].trim() ) : DEFAULT_QC_SERVER_PULL_PORT;
			 */
			
			mapQCPort.put(strExchgCode, intPort);
			// Mary@20130939 - Fixes_Request-20130711, ReqNo.27 - END
		}catch(Exception e){
			e.printStackTrace();
			mapQCPort.put(strExchgCode, 80);
		}
	}
	// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3 - END

	// Added by Mary@20130620 - Change_Request-20130424,ReqNo.9 - START
	public static void addMapFCBitMode(String strExchgCode, String strFCBitMode) {	
		mapFCBitMode.put(strExchgCode, strFCBitMode);
	}
	public static String getMapFCBitMode(String strExchgCode){
		return mapFCBitMode.get(strExchgCode) == null ? "" : mapFCBitMode.get(strExchgCode);
	}
	// Added by Mary@20130620 - Change_Request-20130424,ReqNo.9 - END

	// Added by Kw@20130222 - Change_Request20130104, ReqNo.6
	public static void addMarketDepthLvl(String ExchgCode, int marketDepthLvl) {	
		marketDepth.put(ExchgCode, marketDepthLvl);
	}

	public static int getMarketDepthLvl(String ExchgCode){
		return marketDepth.get(ExchgCode) == null?0:marketDepth.get(ExchgCode);
	}

	// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
	public static void addClientLimitOption(String ExchgCode, int flagCode) {	
		clientLimitOption.put(ExchgCode, flagCode);
	}

	public static int getClientLimitOption(String ExchgCode){
		return clientLimitOption.get(ExchgCode) == null?0:clientLimitOption.get(ExchgCode);
	}
	public static Map<String, String> getExchangeServerMapping() {
		return mapQCIp;
	}

	// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3 - START
	public static Map<String, Integer> getExchangeServerPortMapping(){
		return mapQCPort;
	}
	// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3 - END

	public static String getEncryptedKey() {
		return encryptedKey;
	}

	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
	public static boolean cacheBrokerInit(Context context, Map<String, String> mapParam){
		String strPreferencesName						= null;
		SharedPreferences.Editor editoBrokerPreferences	= null;
		try{
			strPreferencesName		= PreferenceConstants.BROKER_INIT;
			editoBrokerPreferences 	= context.getSharedPreferences(strPreferencesName, Context.MODE_PRIVATE).edit();

			for(Iterator<String> i=mapParam.keySet().iterator(); i.hasNext();){
				String strParamKey	= i.next();
				String strParamValue= mapParam.get(strParamKey);
				if(strParamKey != null){
					editoBrokerPreferences.remove(strParamKey);

					if(strParamValue != null)
						editoBrokerPreferences.putString(strParamKey, strParamValue);
				}
			}
			editoBrokerPreferences.commit();

			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			strPreferencesName	= null;
			editoBrokerPreferences	= null;
		}
	}

	public static String getCachedBrokerInitParamValue(Context context, String strParamKey){
		String strPreferencesName			= null;
		SharedPreferences brokerPreferences	= null;
		String strParamValue				= null;
		try{
			strPreferencesName	= PreferenceConstants.BROKER_INIT;
			brokerPreferences 	= context.getSharedPreferences(strPreferencesName, Context.MODE_PRIVATE);
			strParamValue		= brokerPreferences.getString(strParamKey, null);

			return strParamValue;
		}catch(Exception e){
			e.printStackTrace();
			return strParamValue;
		}finally{
			strPreferencesName	= null;
			strParamValue		= null;
			brokerPreferences	= null;
		}
	}

	public static boolean cacheBrokerUserDetails(Context context, SharedPreferences sharedPreferences, Map<String, String> mapParam){
		String strPreferencesName						= null;
		SharedPreferences.Editor editoBrokerPreferences	= null;
		try{
			strPreferencesName		= PreferenceConstants.BROKER_PREFERENCE + "_" + sharedPreferences.getString(PreferenceConstants.BROKER_CODE, null) + "_" + sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
			editoBrokerPreferences 	= context.getSharedPreferences(strPreferencesName, Context.MODE_PRIVATE).edit();

			for(Iterator<String> i=mapParam.keySet().iterator(); i.hasNext();){
				String strParamKey	= i.next();
				String strParamValue= mapParam.get(strParamKey);
				if(strParamKey != null){
					editoBrokerPreferences.remove(strParamKey);

					if(strParamValue != null)
						editoBrokerPreferences.putString(strParamKey, strParamValue);
				}
			}
			editoBrokerPreferences.commit();

			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			strPreferencesName		= null;
			editoBrokerPreferences	= null;
		}
	}

	public static String getCachedBrokerUserDetail(Context context, SharedPreferences sharedPreferences, String strParamKey){
		String strPreferencesName			= null;
		SharedPreferences brokerPreferences	= null;
		String strParamValue				= null;
		try{
			strPreferencesName	= PreferenceConstants.BROKER_PREFERENCE + "_" + sharedPreferences.getString(PreferenceConstants.BROKER_CODE, null) + "_" + sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

			brokerPreferences 	= context.getSharedPreferences(strPreferencesName, Context.MODE_PRIVATE);
			strParamValue		= brokerPreferences.getString(strParamKey, null);

			return strParamValue;
		}catch(Exception e){
			e.printStackTrace();
			return strParamValue;
		}finally{
			strPreferencesName	= null;
			strParamValue		= null;
			brokerPreferences	= null;
		}
	}
	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END

	// Added by Mary@20121114 - Change_Request-20120719, ReqNo.12 - START
	public static boolean cacheBrokerUserTradePreference(Context context, SharedPreferences sharedPreferences, Map<String, String> mapParam){
		String strPreferencesName						= null;
		SharedPreferences.Editor editoBrokerPreferences	= null;
		try{
			// Mary@20121123 - Change_Request-20120719, ReqNo.12
			strPreferencesName		= PreferenceConstants.BROKER_PREFERENCE + "_" + sharedPreferences.getString(PreferenceConstants.BROKER_CODE, null) + "_" + sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null) + "_" + getCachedBrokerUserDetail(context, sharedPreferences, PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE);
			editoBrokerPreferences 	= context.getSharedPreferences(strPreferencesName, Context.MODE_PRIVATE).edit();

			for(Iterator<String> i=mapParam.keySet().iterator(); i.hasNext();){
				String strParamKey	= i.next();
				String strParamValue= mapParam.get(strParamKey);
				if(strParamKey != null){
					editoBrokerPreferences.remove(strParamKey);

					if(strParamValue != null)
						editoBrokerPreferences.putString(strParamKey, strParamValue);
				}
			}
			editoBrokerPreferences.commit();

			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			strPreferencesName		= null;
			editoBrokerPreferences	= null;
		}
	}

	public static String getCachedBrokerUserTradePreference(Context context, SharedPreferences sharedPreferences, String strParamKey){
		String strPreferencesName			= null;
		SharedPreferences brokerPreferences	= null;
		String strParamValue				= null;
		try{
			// Mary@20121123 - Change_Request-20120719, ReqNo.12
			strPreferencesName	= PreferenceConstants.BROKER_PREFERENCE + "_" + sharedPreferences.getString(PreferenceConstants.BROKER_CODE, null) + "_" + sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null) + "_" + getCachedBrokerUserDetail(context, sharedPreferences, PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE);

			brokerPreferences 	= context.getSharedPreferences(strPreferencesName, Context.MODE_PRIVATE);
			strParamValue		= brokerPreferences.getString(strParamKey, null);

			return strParamValue;
		}catch(Exception e){
			e.printStackTrace();
			return strParamValue;
		}finally{
			strPreferencesName	= null;
			strParamValue		= null;
			brokerPreferences	= null;
		}
	}
	// Added by Mary@20121114 - Change_Request-20120719, ReqNo.12 - END

	// Added by Mary@20120827 - Fixes_Request-20120815, ReqNo.3 - START
	public static boolean isMarketPriceStock(float fltPrice){
		if (fltPrice == -999002 || fltPrice == 999002)
			return true;
		return false;
	}

	public static boolean isMarketOrderStock(float fltPrice){
		if (fltPrice == -999001 || fltPrice == 999001)
			return true;
		return false;
	}
	// Added by Mary@20120827 - Fixes_Request-20120815, ReqNo.3 - END

	// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12 - START
	public static void flushConstants(){
		hasDisclaimerShown			= false;
		/* Mary@20130620 - Change_Request-20130424, ReqNo.9
		isShowFundamentalCapitalICQ	= false;
		 */
		noTradeClient 				= false;

		if(News != null)
			News.clear();
		News						= null;

		if(mapQCIp != null)
			mapQCIp.clear();
		else
			mapQCIp	= new HashMap<String, String>();

		// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3 - START
		if(mapQCPort != null)
			mapQCPort.clear();
		else
			mapQCPort = new HashMap<String, Integer>();
		// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3 - END

		selectedAcc 				= null;

		if(QCdata != null)
			QCdata.flushBean();

		QCdata						= new QCBean();

		//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 START
		// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
		if(info2FA != null)
			info2FA.Info2FAFlush();

		selectedDeviceID ="";
		otpValue="";

		// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
		if(StockApplication.getOTPvalidationThread() != null)
			StockApplication.getOTPvalidationThread().setRegisteredListener(null);

		//marketDepth = null;
		// Added by Kw@20130116 - Change_Request20130104, ReqNo.2 END
		// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3
		rdsInfo	= new RDSInfo();

		// Added by Mary@20130529 - Fixes_Request-20130523, ReqNo.4 - START
		userParamInByte = null;
		userParam 		= null;
		encryptedKey 	= null;
		QCUserParamKey 	= null;
		QC_SERVER_IP 	= null;
		// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3
		QC_SERVER_PORT	= 80;
		MAP_QC_IP		= null;
		senderCode 		= null;
		TNCInfo 		= null;

		// Added by Mary@20130620 - Change_Request-20130424,ReqNo.9 - START
		if(mapFCBitMode != null)
			mapFCBitMode.clear();
		else
			mapFCBitMode = new HashMap<String, String>();
		// Added by Mary@20130620 - Change_Request-20130424,ReqNo.9 - END

		if(marketDepth != null)
			marketDepth.clear();
		else
			marketDepth	= new HashMap<String, Integer>();

		if(clientLimitOption != null)
			clientLimitOption.clear();
		else
			clientLimitOption	= new HashMap<String, Integer>();
		// Added by Mary@20130529 - Fixes_Request-20130523, ReqNo.4 - END


		// Added by Mary@20130503 - Fixes_Request-20130424, ReqNo.4
		DefinitionConstants.flushVariable();
	}
	// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12 - END

	// Added by Mary@20121025 - Fixes_Request-20121023, ReqNo.2 - START
	public static List<StockSymbol> sortImageBaseOnDerivativeRules(List<StockSymbol> listDraftStockSymbols){
		List<String> listForSort				= new ArrayList<String>();
		List<StockSymbol> listSortedStockSymbol	= new ArrayList<StockSymbol>();
		try{
			if(listDraftStockSymbols == null)
				return listDraftStockSymbols;				

			for(Iterator<StockSymbol> itr=listDraftStockSymbols.iterator(); itr.hasNext(); ) {
				StockSymbol symbol	= itr.next();
				listForSort.add( symbol.getCompanyName() );
			}
			Collections.sort(listForSort, String.CASE_INSENSITIVE_ORDER);

			for(Iterator<String> itr=listForSort.iterator(); itr.hasNext(); ) {
				String strComName	= itr.next();

				for(Iterator<StockSymbol> itrInner=listDraftStockSymbols.iterator(); itrInner.hasNext(); ) {
					StockSymbol symbol	= itrInner.next();
					if( strComName.equals( symbol.getCompanyName() ) ){
						listSortedStockSymbol.add(symbol);
						break;
					}
				}

				strComName	= null;
			}

			return listSortedStockSymbol;
		}catch(Exception e){
			e.printStackTrace();
			return listSortedStockSymbol;
		}finally{
			listForSort.clear();
			listForSort	= null;
		}
	}
	// Added by Mary@20121025 - Fixes_Request-20121023, ReqNo.2 - END

	// Added by Thinzar@20150515, Change_Request-20150401, ReqNo. 2 - START
	//--overload the above method written by Mary previously
	public static List<StockSymbol> sortImageBaseOnDerivativeRules(List<StockSymbol> listDraftStockSymbols, int sortType){
		String sort_key;

		try{
			//do the sorting here
			switch(sortType){

			case CommandConstants.IMAGE_SORT_MOVER:
				sort_key				= "trade_volume";
				Collections.sort(listDraftStockSymbols, new StockSymbol.orderByVolume());
				break;
			case CommandConstants.IMAGE_SORT_GAINER:
				sort_key		= "change";
				Collections.sort(listDraftStockSymbols, new StockSymbol.orderByPriceChange());
				break;
			case CommandConstants.IMAGE_SORT_GAINER_PERCENT:
				sort_key		= "change_percent";
				Collections.sort(listDraftStockSymbols, new StockSymbol.orderByPriceChangePercent());
				break;
			case CommandConstants.IMAGE_SORT_LOSER:
				sort_key		= "change";
				Collections.sort(listDraftStockSymbols, new StockSymbol.orderByPriceChangeReverse());
				break;
			case CommandConstants.IMAGE_SORT_LOSER_PERCENT:
				sort_key		= "change_percent";
				Collections.sort(listDraftStockSymbols, new StockSymbol.orderByPriceChangePercentReverse());
				break;
			case CommandConstants.IMAGE_SORT_NAME:
				sort_key		= "stock_long_name";				
				Collections.sort(listDraftStockSymbols, new StockSymbol.orderByCompanyName());
				break;
			default:
				sort_key		= "trade_volume";
				Collections.sort(listDraftStockSymbols, new StockSymbol.orderByVolume());
				break;
			}
		}catch(Exception e){
			e.printStackTrace();

		}
		return listDraftStockSymbols;
	}
	// Added by Thinzar@20150515, Change_Request-20150401, ReqNo. 2 - END

	// Added by Mary@20121114 - Change_Request-20120719, ReqNo.12 - START
	public static void updateTradePreferenceAccordanceToCurrentExchange(Context context, SharedPreferences sharedPreferences ){
		AppConstants.brokerConfigBean.resetTradePreferenceConfig();

		String strTemp				= AppConstants.getCachedBrokerUserTradePreference(context, sharedPreferences, PreferenceConstants.IS_AUTOFILL_TRADE_QUANTITY);
		if(strTemp!=null && strTemp.trim().length() > 0)
			AppConstants.brokerConfigBean.isAutoFillTradeQuantity(Boolean.parseBoolean(strTemp) );				

		strTemp				= AppConstants.getCachedBrokerUserTradePreference(context, sharedPreferences, PreferenceConstants.IS_AUTOFILL_TRADE_PRICE);
		if(strTemp!=null && strTemp.trim().length() > 0)
			AppConstants.brokerConfigBean.isAutoFillTradePrice( Boolean.parseBoolean(strTemp) );

		strTemp				= AppConstants.getCachedBrokerUserTradePreference(context, sharedPreferences, PreferenceConstants.CUSTOMED_TRADE_QUANTITY);
		if(strTemp!=null && strTemp.trim().length() > 0)
			AppConstants.brokerConfigBean.setIntCustomTradeQuantity( Integer.parseInt(strTemp) );

		strTemp				= AppConstants.getCachedBrokerUserTradePreference(context, sharedPreferences, PreferenceConstants.TRADE_QUANTITY_MEASUREMENT);
		if(strTemp!=null && strTemp.trim().length() > 0)
			AppConstants.brokerConfigBean.setIntTradeQtyMeasurement( Integer.parseInt(strTemp) );
	}

	public static void updateTradePreferenceSharedPreference(Context context, SharedPreferences sharedPreferences, String strNewValue){
		if(strNewValue == null){
			// Mary@20121123 - Change_Request-20120719, ReqNo.12
			String strTradeExchgCode			= AppConstants.getCachedBrokerUserDetail(context, sharedPreferences, PreferenceConstants.TRADE_EXCHANGE_CODE);
			String strTradePreferenceExchgCode	= AppConstants.getCachedBrokerUserDetail(context, sharedPreferences, PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE);
			if(strTradeExchgCode == null){
				strNewValue	= DEFAULT_EXCHANGE_CODE;
			}else if( strTradePreferenceExchgCode == null || ( strTradePreferenceExchgCode != null && !strTradeExchgCode.equals(strTradePreferenceExchgCode) ) ){
				strNewValue	= strTradeExchgCode;
			}
		}

		Map<String, String> mapTradePreference	= new HashMap<String, String>();
		//  Mary@20121123 - Change_Request-20120719, ReqNo.12
		mapTradePreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, strNewValue);
		cacheBrokerUserDetails(context, sharedPreferences, mapTradePreference);
		mapTradePreference.clear();
		mapTradePreference		= null;

		updateTradePreferenceAccordanceToCurrentExchange(context, sharedPreferences);
	}
	// Added by Mary@20121114 - Change_Request-20120719, ReqNo.12 - END

	// Added by Mary@20121228 - Fixes_Request-20121102, ReqNo.11 - START
	public static double convertDefaultCurrencyToOtherCurrency(double dblValue, String strFromCurrency, String strToCurrency, String strActionType, List<CurrencyExchangeInfo> lstCurrencyExchangeInfo) throws Exception{
		try{
			CurrencyExchangeInfo currencyExchgInfo	= getCurrencyExchangInfo(strFromCurrency, strToCurrency, lstCurrencyExchangeInfo);

			return convertDefaultCurrencyToOtherCurrency(dblValue, strActionType, currencyExchgInfo);
		}catch(Exception e){
			e.printStackTrace();
			return 0.00;
		}
	}

	public static double convertDefaultCurrencyToOtherCurrency(double dblValue, String strActionType, CurrencyExchangeInfo currencyExchgInfo){
		double dblConvertedValue= dblValue;
		double dblCurrencyRate	= 1.00;
		try{
			dblCurrencyRate							= strActionType.equalsIgnoreCase("BUY") ? currencyExchgInfo.getDblSellRate() : currencyExchgInfo.getDblBuyRate();
			dblConvertedValue						= ( dblValue * dblCurrencyRate ) / currencyExchgInfo.getLngDenomination();

			return dblConvertedValue;
		}catch(Exception e){
			e.printStackTrace();
			return 0.00;
		}
	}

	public static CurrencyExchangeInfo getCurrencyExchangInfo(String strFromCurrency, String strToCurrency, List<CurrencyExchangeInfo> lstCurrencyExchangeInfo) throws Exception{
		CurrencyExchangeInfo currencyExchgInfo	= null;
		try{
			if( strFromCurrency.equalsIgnoreCase(strToCurrency) ){
				currencyExchgInfo = new CurrencyExchangeInfo();
				currencyExchgInfo.setLngDenomination(1);
				currencyExchgInfo.setDblBuyRate(1f);
				currencyExchgInfo.setDblSellRate(1f);
				currencyExchgInfo.setStrFromStockCurrency(strFromCurrency);
				currencyExchgInfo.setStrToClientCurrency(strToCurrency);
			}else{
				for(Iterator<CurrencyExchangeInfo> itr=lstCurrencyExchangeInfo.iterator(); itr.hasNext();){
					CurrencyExchangeInfo ceiTemp	= itr.next();
					String strStockCurrency			= ceiTemp.getStrFromStockCurrency();
					String strClientCurrency		= ceiTemp.getStrToClientCurrency();
					if( strStockCurrency.equalsIgnoreCase(strFromCurrency) && strClientCurrency.equalsIgnoreCase(strToCurrency) ){
						currencyExchgInfo	= ceiTemp;
						break;
					}
				}

				// reverse matching
				if(currencyExchgInfo == null){
					CurrencyExchangeInfo ceiTemp	= null;
					for(Iterator<CurrencyExchangeInfo> itr=lstCurrencyExchangeInfo.iterator(); itr.hasNext();){
						CurrencyExchangeInfo ceiCurrent	= itr.next();
						String strStockCurrency			= ceiCurrent.getStrFromStockCurrency();
						String strClientCurrency		= ceiCurrent.getStrToClientCurrency();
						if( strStockCurrency.equalsIgnoreCase(strToCurrency) && strClientCurrency.equalsIgnoreCase(strFromCurrency) ){
							ceiTemp	= ceiCurrent;
							break;
						}
					}

					if(ceiTemp != null){
						double dblBuyRate	= ceiTemp.getLngDenomination() / ceiTemp.getDblBuyRate();
						double dblSellRate	= ceiTemp.getLngDenomination() / ceiTemp.getDblSellRate();

						int intBuyPowerOf	= 0;
						double dblTemp		= dblBuyRate;
						while(dblTemp < 1){
							dblTemp	= dblTemp * ( Math.pow(10, ++intBuyPowerOf) );
						}

						int intSellPowerOf	= 0;
						dblTemp				= dblSellRate;
						while(dblTemp < 1){
							dblTemp	= dblTemp * ( Math.pow(10, ++intSellPowerOf) );
						}

						int intFinalPowerOf	= (intBuyPowerOf == intSellPowerOf && intBuyPowerOf == 0) ? 0 : ( (intBuyPowerOf >= intSellPowerOf) ? intBuyPowerOf : intSellPowerOf);
						long lngDenomination= intFinalPowerOf == 0 ? 1 : Math.round( Math.pow(10, intFinalPowerOf) );

						dblBuyRate			= dblBuyRate * lngDenomination;
						dblSellRate			= dblSellRate * lngDenomination;

						currencyExchgInfo = new CurrencyExchangeInfo();
						currencyExchgInfo.setLngDenomination(lngDenomination);
						currencyExchgInfo.setDblBuyRate(dblBuyRate);
						currencyExchgInfo.setDblSellRate(dblSellRate);
						currencyExchgInfo.setStrFromStockCurrency(strFromCurrency);
						currencyExchgInfo.setStrToClientCurrency(strToCurrency);
						// unfound exchange	
					}else{
						/* replace with throw exception to alert user 
						currencyExchgInfo = new CurrencyExchangeInfo();
						currencyExchgInfo.setLngDenomination(0);
						currencyExchgInfo.setDblBuyRate(0f);
						currencyExchgInfo.setDblSellRate(0f);
						currencyExchgInfo.setStrFromStockCurrency(strFromCurrency);
						currencyExchgInfo.setStrToClientCurrency(strToCurrency);
						 */
						throw new Exception(ErrorCodeConstants.FAIL_GET_CURRENCY_EXCHANGE_DETAIL);
					}
				}
			}

			return currencyExchgInfo;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	// Added by Mary@20121228 - Fixes_Request-20121102, ReqNo.11 - END

	// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.16 - START
	public static void setStrBaseCurrencyCode(String strBaseCurrencyCode){
		DEFAULT_CURRENCY_CODE	= strBaseCurrencyCode;
	}
	public static String getStrBaseCurrencyCode(){
		return DEFAULT_CURRENCY_CODE;
	}
	// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.16 - END

	// Added by Mary@20130826 - Fixes_Request-20130711, ReqNo.14 - START
	public static int getIntBidAskPriceTextColor(Context context, float fltBidAskPrice, float fltLACP) throws Exception{
		if( ! isMarketOrderStock(fltBidAskPrice) && ! isMarketPriceStock(fltBidAskPrice) ){
			if (fltBidAskPrice == fltLACP ||fltBidAskPrice == 0)
				return context.getResources().getColor(R.color.NoStatus_FgColor);
			else if(fltBidAskPrice < fltLACP)
				return context.getResources().getColor(R.color.DownStatus_BgColor);
			else
				return context.getResources().getColor(R.color.UpStatus_BgColor);
		}else{
			return context.getResources().getColor(R.color.UpStatus_BgColor);
		}
	}

	public static int getIntBidAskQtyBlinkTextColor(Context context, float fltBidAskOldQty, float fltBidAskNewQty) throws Exception{
		float fltGapBidSize	= fltBidAskNewQty - fltBidAskOldQty;
		if(fltGapBidSize > 0)
			return context.getResources().getColor(R.color.UpStatus_FgColor);
		else if (fltGapBidSize < 0)
			return context.getResources().getColor(R.color.DownStatus_FgColor);
		else
			return context.getResources().getColor(R.color.NoStatus_FgColor);
	}

	public static int getIntBidAskQtyBlinkBackgroundColor(Context context, float fltBidAskOldQty, float fltBidAskNewQty) throws Exception{
		float fltGapBidSize	= fltBidAskNewQty - fltBidAskOldQty;
		if(fltGapBidSize > 0)
			return context.getResources().getColor(R.color.UpStatus_BgColor);
		else if (fltGapBidSize < 0)
			return context.getResources().getColor(R.color.DownStatus_BgColor);
		else
			return context.getResources().getColor(R.color.NoStatus_BgColor);
	}

	public static int getIntBidAskPriceBlinkTextColor(Context context, float fltBidAskOldPrice, float fltBidAskNewPrice, int intCurrentTextColor) throws Exception{
		if ( ! isMarketOrderStock( fltBidAskNewPrice ) && ! isMarketPriceStock( fltBidAskNewPrice ) ){
			float fltGapBidSize	= fltBidAskNewPrice - fltBidAskOldPrice;
			/* Mary@20130926 - remove unreacheable filtering
			if(fltGapBidSize > 0.0 || isMarketOrderStock( fltBidAskNewPrice ) || isMarketPriceStock( fltBidAskNewPrice ) )
			 */
			if(fltGapBidSize > 0.0)
				return context.getResources().getColor(R.color.UpStatus_FgColor);
			else if(fltGapBidSize < 0.0)
				return context.getResources().getColor(R.color.DownStatus_FgColor);
			else
				return intCurrentTextColor;
		}else{
			if( ! isMarketOrderStock( fltBidAskOldPrice ) && ! isMarketPriceStock( fltBidAskOldPrice )  )
				return context.getResources().getColor(R.color.UpStatus_FgColor);
			else
				return intCurrentTextColor;
		}
	}

	public static int getIntBidAskPriceBlinkBackgroundColor(Context context, float fltBidAskOldPrice, float fltBidAskNewPrice) throws Exception{
		if ( ! isMarketOrderStock( fltBidAskNewPrice ) && ! isMarketPriceStock( fltBidAskNewPrice ) ){
			float fltGapBidSize	= fltBidAskNewPrice - fltBidAskOldPrice;
			/* Mary@20130926 - remove unreacheable filtering
			if(fltGapBidSize > 0.0 || isMarketOrderStock( fltBidAskNewPrice ) || isMarketPriceStock( fltBidAskNewPrice ) )
			 */
			if(fltGapBidSize > 0.0)
				return context.getResources().getColor(R.color.UpStatus_BgColor);
			else if(fltGapBidSize < 0.0)
				return context.getResources().getColor(R.color.DownStatus_BgColor);
			else
				return context.getResources().getColor(R.color.NoStatus_BgColor);
		}else{
			if( ! isMarketOrderStock( fltBidAskOldPrice ) && ! isMarketPriceStock( fltBidAskOldPrice )  )
				return context.getResources().getColor(R.color.UpStatus_BgColor);
			else
				return context.getResources().getColor(R.color.NoStatus_BgColor);
		}
	}

	public static String getStrFormattedBidAskPrice(float fltBidAskPrice, String strStockExchgCode) throws Exception{
		if( isMarketOrderStock(fltBidAskPrice) ){
			return "MO";
		}else if( isMarketPriceStock(fltBidAskPrice) ){
			return "MP";
		}else{
			/*
			if( strStockExchgCode.equalsIgnoreCase("JK") || strStockExchgCode.equalsIgnoreCase("JKD") )  
				return FormatUtil.formatPrice2(fltBidAskPrice);
			else
				return FormatUtil.formatPrice(fltBidAskPrice);
			 */
			//Commented out and Edited by Thinzar, Change_Request-20141101, ReqNo. 8 
			return FormatUtil.formatPrice(fltBidAskPrice, null, strStockExchgCode);
		}
	}
	// Added by Mary@20130826 - Fixes_Request-20130711, ReqNo.14 - END

	//Added by Melissa
	public static DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt;

	//Added by thinzar@20141027, Change_Request-20140901, ReqNo.12
	public static String DEFAULT_FEED_MARKET_TYPE					= "Normal Board Lot";
	public static String DEFAULT_FEED_MARKET_CODE					= "10";

	//Added by thinzar, Change_Request-20150901, ReqNo.5
	public static String SELECTED_FEED_MARKET_CODE					= DEFAULT_FEED_MARKET_CODE;

	//Added by Thinzar@20141106, Change_Request-20141101, ReqNo. 1
	public static String CURRENT_PKG_NAME;

	//Added by Thinzar, Change_Request-20150901, ReqNo.1 - START
	public static int timeoutExceptionCount = 0;
	public static int connectExceptionCount	= 0;

	//Added by Thinzar@20140911, Change_Request-20140901, ReqNo.3
	public static String LMS_JSP_PATH = "http://nogl.asiaebroker.com/ebcServlet/LMSGetSetting.jsp";
	//public static String LMS_JSP_PATH = "http://nopl-sg.itradecimb.com/ebcServlet/LMSGetSetting.jsp";	//for CIMB SG
	//public static String LMS_JSP_PATH = "http://nopl-ph.asiaebroker.com/ebcServlet/LMSGetSetting.jsp?";	//for Abacus

	public static String registerId;

	public class PUSH{
		
		//------ AES KEY------//
		public static final String KEYID="id";
		public static final String DKEY="dkey";
		public static final String DSALT="dsalt";
		public static final String DIV="div";
		public static final String EKEY="ekey";
		public static final String ESALT="esalt";
		public static final String EIV="eiv";

		///REGISTER PUSH///
		public static final String CT="ct";
		public static final String AE="ae";
		public static final String EV="ev";
		public static final String CS="cs";
		
		public static final String EX		= "ex";
		public static final String SPC		= "spc";
		public static final String BH		= "bh";
		public static final String LID		= "lid";
		public static final String APPID	= "appId";
		public static final String IMEI		= "imei";
		public static final String CC		= "cc";
		public static final String BUNDLEID	= "bdlID";
		public static final String DEVICETOKEN= "dvTkn";
		public static final String IOSID	= "iosID";
		public static final String IPADD	= "ipAdd";
		public static final String ACTIVATIONCODE	= "actCd";	
		public static final String MSG		= "msg";
		public static final String STATUS	= "s";

		// Added by Diyana, Change_Request_20160624, ReqNo.6 -START
		public static final String PLATFORMNAME		= "pfmNm";
		public static final String APPVERSION		= "appV";
		public static final String DEVICEMODEL		= "dvMdl";
		public static final String DEVICENEWTYPE	= "dvNwT";
		public static final String DEVICEPROVIDER	= "dvNWPvd";
		public static final String PLATFORMINFO		= "pfmInfo";
		public static final String PLATFORMSCREENSIZE = "pfmScSz";
		public static final String JWT				= "jwt";
		public static final String OPT				= "opt";
		// Added by Diyana, Change_Request_20160624, ReqNo.6 -END
	}

	public class STOCKALERT{

		public static final String KEYS ="keys";	
		public static final String KTY ="kty";
		public static final String KID ="kid";
		public static final String ALG ="alg";
		public static final String publicKey="n";
		public static final String eObj="e";

		public static final String privateKey="MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCMl0ekqJ4SFdjQAZOyrXBcI05joYSvS/wAKilPhSiFn6Kl7NT2ZbiThpLBBBmo0X3Ign3kJwIk09TVsGEAfzMxHDqo4P9onQDvFmbD1rZIO9pzHCGy6Y2jt4p4g0h4DM3IBqHLNNrku9kamhM+x66u5IuKvqkN16Rp/IYLwYSQ2MssGRBvOgHvbmPTGYxib/qY91ougkKp1psu7fXfzxl9tvhJnFH2qb5DPe+8Clt4Sx4dYhQWI8CVjZsvqOyV7lDxAB8hhO3jGKUCTJUwAkJqcJSb3miryda/10vbV/QOHWyYXJld9ymRfimfR/j32u1/6hYtoGIGYy0HP7/TgQYhAgMBAAECggEAELwdKgtsLWjuUuwtIZKlkkIdnZxnR9p6l6ZtHmsSRL2YxbloLCA1lVHqfBE54K93Env98j92cN3KMtl44m9Br7HVha6qmFBAATWga+VN81E0kokhtcM5OeiqEehGowubHxky/xY+jAWUNSdr7Qgl0emgdpZ1VxF5H6iQaCvT1M3avAogp5of86fUadVKy9X31cQ6hB0PFhvvz26kV10hFjkd01jNjGGVUjCktxyLQ0NROt1g+pVIVk3OwE9PhVIm8CTRwGN3nrWMZF82RyjiMiVmstupYm1dLImPHfj0dUROKXKmCatzROs5hCqYt95o79PfrIgNEAWFUguSqyt7JQKBgQD23KMv5KoHdJjN3yWoL9FUbWIlXbLB91WDJ37RH5hSVcTpsHwAdGaouLwSkW+1JE6jhDSkulNh2VKNBeddYJuMRN734UC5xD9XjBxR5ZhpJ9E12c91xX7Hx7eI6rv5a96/T62bjOtQjHEjiLcUJVHHHzzN+Ai0T7q0ZKI/5fvP7wKBgQCRy5OFMIWOt+E7uBr83xO/nJE57H0YK/1OZL+3C7V3iDjOsIlVgaJcEhr86Ge03z1XLv8rTINR6uxs9y8jfAU2/qM9l5VbqIpgRJ23eyPwMqd7GlKK3nVWxkjsqi7iLagAzzyT8NRKruy338JRqKhuI5ypGkP6PR7DRhOl1s567wKBgG/+QPplXs3rE7eyWL8okmsfBLu2+nYM43qQ3HO7Rbt0tdTfACKkrD43MPurh2ZChnqga9GfxlxJ362i1AgE4AyO4ijKxDaC1cm+AssUwkRSkBNCMWBYrN+tt5IiDA1n5rIcT8KV99ufG2TnjUdSdvtDLK60rB5BXY6haQpcKBV5AoGBAIeAe1T4acjhIZaNX5fSBNq7O0Zog0Mp7z0i6gPIABpeIt4GuT0dRLbppOeVYH18Vdhnt4SYkhxaiO4lNkx2gapBM5qU/Z24oRMj8qmK6zXc/ketiTXD166Uj0ZWTUitQk2uThD5bXUT6rJiZonahOAIfRu1a6RxldvLj4s8f59FAoGAfBRLtJLX6sn6nFfieNhSqTWI7qzBPe2DP+XIMOuQYApVIc4pZrVeyTAzyXR7FAtZoaDhvvGbW9mN9RX1Mcl6DpstvMHYdUo9GJQXMo6joF6jVMHfcqPWWTGJCL24+oaKu91gU878vWDpEUFQTa0l0oqTb2kZWo+6wS6H4veuMbI=";

		public static final String email		= "mail";
		public static final String mobilenum	= "mbn";
		public static final String sponsor		= "spr";
		public static final String regenerate	= "reGen";
		public static final String jwt			= "jwt";
		public static final String BHCODE		= "bhCode";
		public static final String BH			= "bh";
		public static final String appId		= "appId";
	}

	// Added by Thinzar, to set senderId automatically based on bundleId
	public static final Map<String, String> pushSenderIdMap;
	static {

		pushSenderIdMap = new HashMap<String, String>();

		pushSenderIdMap.put("com.n2nconnect.iMSL_uat.stock", "1013372157911");
		pushSenderIdMap.put("com.n2nconnect.iTradeCIMB_uat.stock", "101778850261");
		pushSenderIdMap.put("com.n2nconnect.iTradeCIMB_staging.stock", "1081004570143");
		pushSenderIdMap.put("com.n2nconnect.iTradeCIMB.stock", "828972202420");
		pushSenderIdMap.put("com.n2nconnect.iTradeCIMBSG_uat.stock", "845342389000");
		pushSenderIdMap.put("com.n2nconnect.iTradeCIMBSG_staging.stock", "335526225429");
		pushSenderIdMap.put("com.n2nconnect.iTradeCIMBSG.stock", "385072238309");
		pushSenderIdMap.put("ph.com.mytrade_uat.stock", "415892421385");
		pushSenderIdMap.put("ph.com.mytrade.stock", "289283016321");
		pushSenderIdMap.put("com.n2nconnect.taonline_uat.stock", "746527100590");
		pushSenderIdMap.put("com.n2nconnect.taonline.stock", "341563748744");
		pushSenderIdMap.put("com.n2nconnect.taderivatives_uat.stock", "526593983781");
		pushSenderIdMap.put("com.n2nconnect.amsec_uat.stock", "918223926505");
		pushSenderIdMap.put("com.n2nconnect.amfutures_uat.stock", "711109079712");
		pushSenderIdMap.put("com.n2nconnect.amsec.stock", "381890150523");
		pushSenderIdMap.put("com.n2nconnect.amfutures.stock", "214229997114");
		pushSenderIdMap.put("com.n2nconnect.iMSLSJ_uat.stock", "824799496656");

	}

	//Added by Thinzar, to set region code for iScreener module
	public static final Map<String, String> screenerRegionId;
	static {

		screenerRegionId = new HashMap<String, String>();
		screenerRegionId.put("com.n2nconnect.iTradeCIMB_uat.stock", "MY");			//MY uat
		screenerRegionId.put("com.n2nconnect.iTradeCIMB_staging.stock", "MY");		//MY staging
		screenerRegionId.put("com.n2nconnect.iTradeCIMB.stock", "MY");				//MY prod
		screenerRegionId.put("com.n2nconnect.iTradeCIMBSG_uat.stock", "SG");		//SG uat
		screenerRegionId.put("com.n2nconnect.iTradeCIMBSG_staging.stock", "SG");	//SG staging
		screenerRegionId.put("com.n2nconnect.iTradeCIMBSG.stock", "SG");			//SG prod
		screenerRegionId.put("com.n2nconnect.cimbstockchallenge", "SG");			//SG Stock Challenge
	}

	//Added by Thinzar, variables required for stock alert
	public static final String TCANDROID_APP_ID		= "MA";
	public static String userEmail		= "";
	public static String userMobile		= "";
	public static String userSenderName	= "";

	//Added by Thinzar, Change_Request-20160722, ReqNo.6
	public static boolean isAppMultilogin	= false;		//false as default

	//Added by Thinzar, Change_Request-20160722, ReqNo.7
	public static boolean hasStockAlertDisclaimerShown		= false;

	//Added by Thinzar, Change_Request-20160722, ReqNo.8 - START
	public static final byte[] iBillionaireSecretkey		= "4ba2dca2cd70234103611b7f7858cb15".getBytes();
	public static final String iBillionaireToken			= "iBillionaire";
	public static final String iBillionaireVerifyToken		= "verify";				//Change_Request-20161101, ReqNo.5
	public static final String iBillionaireVerifySuccessToken		= "verifysuccess";				//Change_Request-20161101, ReqNo.5
	public static boolean hasWarnOpenExternalApp			= false;
	
	public static final Map<String, String> iBillionairePkgNameMap;
	static {

		iBillionairePkgNameMap = new HashMap<String, String>();
		iBillionairePkgNameMap.put("com.n2nconnect.iTradeCIMB_uat.stock", "com.ibillionaire.my");			//MY uat
		iBillionairePkgNameMap.put("com.n2nconnect.iTradeCIMB_staging.stock", "com.ibillionaire.my");		//MY staging
		iBillionairePkgNameMap.put("com.n2nconnect.iTradeCIMB.stock", "com.ibillionaire.my");				//MY prod
		iBillionairePkgNameMap.put("com.n2nconnect.iTradeCIMBSG_uat.stock", "com.ibillionaire.sg");			//SG uat
		iBillionairePkgNameMap.put("com.n2nconnect.iTradeCIMBSG_staging.stock", "com.ibillionaire.sg");		//SG staging
		iBillionairePkgNameMap.put("com.n2nconnect.iTradeCIMBSG.stock", "com.ibillionaire.sg");				//SG prod
	}

	public static final Map<String, String> iBillionaireAppLinkMap;
	static {

		iBillionaireAppLinkMap = new HashMap<String, String>();
		iBillionaireAppLinkMap.put("com.n2nconnect.iTradeCIMB_uat.stock", "ibillionaireresearchmy");		//MY uat
		iBillionaireAppLinkMap.put("com.n2nconnect.iTradeCIMB_staging.stock", "ibillionaireresearchmy");	//MY staging
		iBillionaireAppLinkMap.put("com.n2nconnect.iTradeCIMB.stock", "ibillionaireresearchmy");			//MY prod
		iBillionaireAppLinkMap.put("com.n2nconnect.iTradeCIMBSG_uat.stock", "ibillionaireresearchsg");		//SG uat
		iBillionaireAppLinkMap.put("com.n2nconnect.iTradeCIMBSG_staging.stock", "ibillionaireresearchsg");	//SG staging
		iBillionaireAppLinkMap.put("com.n2nconnect.iTradeCIMBSG.stock", "ibillionaireresearchsg");			//SG prod
	}

	public static boolean hasRegisteredStockAlert	= false;

	//Fixes_Request-20161101, ReqNo.5
	public static boolean isAppVersionSupportSSL	= false;

	//Fixes_Request-20161101, ReqNo.7
	public static String orgATPIPFromConfigFile		= "";
	public static int orgATPPortFromConfigFile;

	//Change_Request-20170119, ReqNo.5
	public static boolean hasDtiTermsShown			= false;
	public static boolean hasCheckedDti				= false;
	
	public static boolean hasCheckedGCM				= false;

	//Change_Request-20160722, ReqNo.9 
	public static boolean isLoginWithTouchID			= false;
	public static boolean hasSecondLvlSecurityVerified	= false;
	
	//Change_Request-20170601, ReqNo.8
	public static final String SGX_APP_ADDRESS 		= "sgxtrademobile/buysell";
	public static final String SGX_ACTION_PARAM 	= "action";
	public static final String SGX_PRICE_PARAM  	= "price";
	public static final String SGX_QTY_PARAM 		= "quantity";
	public static final String SGX_STK_CODE_PARAM 	= "stockcode";
	
	//Change_Request-20170601, ReqNo.2
	public static final String RESEARCH_REPORTS_SOURCE 	= "CIMB";
	public static final String REUTERS_NEWS_SOURCE 		= "TRKD";
	public static HashMap<String, String> elasticNewsSourceMap = new HashMap<String, String>();
	static {
		elasticNewsSourceMap.put("CIMB", "CIMB Research");
		elasticNewsSourceMap.put("NM", "Nikkei Markets");
		elasticNewsSourceMap.put("NAR", "Nikkei Asian Review");
		elasticNewsSourceMap.put("HKN", "HK News");
		elasticNewsSourceMap.put("BURSA", "Bursa News");
		elasticNewsSourceMap.put("TRKD", "Reuters News");
	}

	//Change_Request-20170601, ReqNo.10
	public static final String DEV_TYPE_2FA_TOKEN 	= "0101";
	public static final String DEV_TYPE_2FA_SMS 	= "0103";
}