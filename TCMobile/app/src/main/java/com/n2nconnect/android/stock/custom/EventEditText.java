package com.n2nconnect.android.stock.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class EventEditText extends EditText {
	
	private OnImeBackListener imeBackListener;

	public interface OnImeBackListener {
	    public void onImeBackEvent(EventEditText editText, String text);
	}

    public EventEditText(Context context) {
        super(context);
    }

    public EventEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EventEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK 
        		&& event.getAction() == KeyEvent.ACTION_UP) {
            if (imeBackListener != null) {
            	imeBackListener.onImeBackEvent(this, this.getText().toString());
            }
        }
        return super.dispatchKeyEvent(event);
    }

    
    public void setOnImeBackListener(OnImeBackListener listener) {
        imeBackListener = listener;
    }

}

