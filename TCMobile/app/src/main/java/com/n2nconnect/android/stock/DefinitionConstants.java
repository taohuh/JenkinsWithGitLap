package com.n2nconnect.android.stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

import com.n2nconnect.android.stock.activity.LoginActivity;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.TradeOrder;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class DefinitionConstants {
	
	public static final String METADATA_HEADER = "(=";
	public static final String RECORD_HEADER = ")=";
	public static final String FIELD_CREDIT_LIMIT = "2";
	public static final String TRADE_TICKET_ERROR = "50";
	public static final String TRADE_TICKET_STATUS = "83";
	public static final String HINT_QUESTION = "9";
	public static final String HINT_ANSWER = ":";
	public static final String HINT_EMAIL = "P";
	public static final String ID_STATUS = "E";
	public static final String EMAIL = "P";
	public static final String ERROR_CODE = "2";
	public static final String ERROR_MESSAGE = "3";
	public static final String TRADE_FORGET_PWD_EMAIL = "+";
	public static final String TRADE_FORGET_PWD = "0";
	public static final String TRADE_FORGET_PWD_PHONE = "1";
	
	public static final String TRADE_FORGET_PIN_EMAIL = "+";
	public static final String TRADE_FORGET_PIN = "/";
	public static final String TRADE_FORGET_PIN_PHONE = "1";
	
	/* Mary@20130503 - Fixes_Request-20130424, ReqNo.4
	public static String ExchangeCode = null;
	public static String ExchangeName = null;
	*/
	public static Map<String,String> mapExchangeName = new HashMap<String,String>();
	public static Map<String,List<String>> mapStatusCode;
	/* Mary@20130527 - comment unused variable
	public static Map mapExchangeSector;
	 */
	public static Map<String,Boolean> mapValidTrade = new HashMap<String,Boolean>();
	public static Map<String,String> mapCurrency = new HashMap<String,String>();
	private static List<String> lstQCExchangeCodes = new ArrayList<String>();
	private static List<String> lstTrdExchangeCodes = new ArrayList<String>();
	public static List<TradeOrder> tradeOrders;
	
	public static final String[] NEWS_CATEGORY_SEQ = {
		// Added by Mary@20130301 - Change_Request-20130225, ReqNo.2
		"All News",
		"Special Announcement",
		"Member Circular",
		"Investor Alert Announcement",
		"General Announcement/Listing Circular",
		"Financial Summary",
		"Entitlement",
		"Changes in BoardRoom/Chief Executive Officer",
		"Trading of Rights",
		"Change in Substantial Shareholder's Interest(CI)",
		"Change in Director's Interest",
		"Notice of Person Interest to be Substantial Shareholder(NA)",
		"Notice of Person Ceasing",
		"Bernama Summary Economic News",
		"Bernama General Economic News",
		"Change of Company Secretary",
		"Notice of Shares Buy-Back-Form 28A(NS)",
		"Change of Address(CA)",
		"Change of Registrar(CR)",
		"Notice of Shares Buy Back by a Company-Form 28B",
		"Notice of Shares Buy Back Immdediate Annoucement",
		"Notice of Resale/Cancel of Share",
		"Changes in Audit Commitee"
	};
	
	public static void setExchangeName(String ExchgCode,String ExchgName){
		mapExchangeName.put(ExchgCode,ExchgName);
	}
	
	public static void setValidTrade(String ExchgCode, boolean valid){
		mapValidTrade.put(ExchgCode, valid);
	}
	
	public static void addTrdExchangeCode(String exchgCode){
		lstTrdExchangeCodes.add(exchgCode);
	}
	
	public static List<String> getTrdExchangeCode(){
		return lstTrdExchangeCodes;
	}
	
	public static void setTrdExchangeCode(List<String> lstTrdExchgCodes){
		lstTrdExchangeCodes	= lstTrdExchgCodes;
	}
	
	public static Map<String, Integer> getTradeForgetPwdMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put(ERROR_CODE, 0);
		mapping.put(ERROR_MESSAGE, 1);
		
		return mapping;
	}
	
	public static Map<String, Integer> getTradeForgetPINMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put(ERROR_CODE, 0);
		mapping.put(ERROR_MESSAGE, 1);
		
		return mapping;
	}
	
	public static Map<String, Integer> getHintMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put(HINT_QUESTION, 0);
		mapping.put(ID_STATUS, 1);
		mapping.put(EMAIL, 2);
		
		return mapping;
	}
	
	public static Map<String, Integer> getHintMapping2() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put(HINT_QUESTION, 0);
		mapping.put(HINT_ANSWER, 1);
		mapping.put(HINT_EMAIL, 2);
		mapping.put(ID_STATUS, 3);
		
		return mapping;
	}
	
	public static Map<String, Integer> getNewPwdMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put(TRADE_FORGET_PWD_EMAIL, 0);
		mapping.put(TRADE_FORGET_PWD, 1);
		
		return mapping;
	}
	
	public static Map<String, Integer> getNewPinMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put(TRADE_FORGET_PIN_EMAIL, 0);
		mapping.put(TRADE_FORGET_PIN, 1);
		
		return mapping;
	}
	
	public static void addValidQCExchangeCodes(String exchgCode){
		lstQCExchangeCodes.add(exchgCode);
	}
	
	public static List<String> getValidQCExchangeCodes() {
		return lstQCExchangeCodes;
	}
	
	public static void setValidQCExchangeCodes(List<String> lstQCExchgCodes){
		lstQCExchangeCodes	= lstQCExchgCodes;
	}
	
	public static Map<String, Boolean> getValidTradeMapping(){
		return mapValidTrade;
	}
	
	public static Map<String, String> getExchangeNameMapping() {	
		return mapExchangeName;
	}
	
	public static void addCurrencyMapping(String exchgCode, String currency){
		mapCurrency.put(exchgCode, currency);
	}
	
	public static Map<String, String> getCurrencyMapping(){
		return mapCurrency;
	}
	
	public static Map<String, String> getExchangeCurrencyMapping() {
		
		Map<String, String> mapping = new HashMap<String, String>();
		
		mapping.put("KL", "RM");
		mapping.put("MY", "RM");
		mapping.put("KLD", "RM");
		mapping.put("SG", "S$");
		mapping.put("SGD", "S$");
		mapping.put("SI", "S$");
		mapping.put("SID", "S$");
		mapping.put("HK", "HKD");
		mapping.put("HKD", "HKD");
		mapping.put("A", "USD");
		mapping.put("O", "USD");
		mapping.put("N", "USD");
		mapping.put("AD", "USD");
		mapping.put("OD", "USD");
		mapping.put("ND", "USD");
		mapping.put("JKD", "IDR");
		
		return mapping;
	}
	
	public static Map<String, List<String>> getStatusCodeMapping(){
		mapStatusCode = new HashMap<String, List<String>>();
		
		List<String> OpenOrdersSCode = new ArrayList<String>();
		OpenOrdersSCode.add("1");
		OpenOrdersSCode.add("0");
		OpenOrdersSCode.add("5");
		OpenOrdersSCode.add("6");
		OpenOrdersSCode.add("E");
		OpenOrdersSCode.add("7");
		OpenOrdersSCode.add("A");
		
		List<String> ActiveOrdersSCode = new ArrayList<String>();
		ActiveOrdersSCode.add("1");
		ActiveOrdersSCode.add("0");
		ActiveOrdersSCode.add("5");
		ActiveOrdersSCode.add("6");
		ActiveOrdersSCode.add("E");
		
		List<String> FiiledOrdersSCode = new ArrayList<String>();
		FiiledOrdersSCode.add("2");
		FiiledOrdersSCode.add("3");
		FiiledOrdersSCode.add("1");	//Added by Thinzar, Fixes_Request-20140820, ReqNo.9, 1-Partial Filled
		
		List<String> InactiveOrdersSCode = new ArrayList<String>();
		InactiveOrdersSCode.add("4");
		InactiveOrdersSCode.add("8");
		InactiveOrdersSCode.add("9");
		InactiveOrdersSCode.add("C");
		
		mapStatusCode.put("OPEN ORDERS", OpenOrdersSCode);
		mapStatusCode.put("ACTIVE ORDERS",ActiveOrdersSCode);
		mapStatusCode.put("FILLED ORDERS", FiiledOrdersSCode);
		mapStatusCode.put("INACTIVE ORDERS", InactiveOrdersSCode);
		
		return mapStatusCode;
	}
		
	public static Map<String, Integer> getExchangeImageMapping() {
		
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put("KL", R.drawable.flag_my);
		mapping.put("KLD", R.drawable.flag_my);
		mapping.put("MY", R.drawable.flag_my);
		mapping.put("MYD", R.drawable.flag_my); 
		mapping.put("SG", R.drawable.flag_sg);
		mapping.put("SGD", R.drawable.flag_sg);
		mapping.put("HK", R.drawable.flag_hk);
		mapping.put("HKD", R.drawable.flag_hk);
		mapping.put("SI", R.drawable.flag_sg);
		mapping.put("SID", R.drawable.flag_sg);
		mapping.put("JK", R.drawable.flag_jk);
		mapping.put("JKD", R.drawable.flag_jk);
		mapping.put("A", R.drawable.flag_us);
		mapping.put("AD", R.drawable.flag_us);
		mapping.put("O", R.drawable.flag_us);
		mapping.put("OD", R.drawable.flag_us);
		mapping.put("N", R.drawable.flag_us);
		mapping.put("ND", R.drawable.flag_us);
		// Added by Mary@20130308 - Fixes_Request-20130108, ReqNo.18 - START
		mapping.put("BK", R.drawable.flag_bk);
		mapping.put("BKD", R.drawable.flag_bk);
		// Added by Mary@20130308 - Fixes_Request-20130108, ReqNo.18 - END
		
		//Added by Thinzar, Fixes_Request-20160101, ReqNo. 4
		mapping.put("PH", R.drawable.flag_ph);
		mapping.put("PHD", R.drawable.flag_ph);
		mapping.put("KLL", R.drawable.flag_my);
		
		return mapping;
	}
	
	public static Map<String, Integer> getNewsCategoryMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		/* Mary@20130226 - Change_Request-20130225, ReqNo.2 - START
		mapping.put("20", 0);
		mapping.put("21", 1);
		mapping.put("22", 2);
		mapping.put("30", 3);
		mapping.put("31", 4);
		mapping.put("32", 5);
		mapping.put("33", 6);
		mapping.put("34", 7);
		mapping.put("36", 8);
		mapping.put("37", 9);
		mapping.put("38", 10);
		mapping.put("39", 11);
		mapping.put("40", 12);
		mapping.put("41", 13);
		mapping.put("42", 14);
		mapping.put("43", 15);
		mapping.put("44", 16);
		mapping.put("45", 17);
		mapping.put("46", 18);
		mapping.put("47", 19);
		mapping.put("49", 20);
		mapping.put("50", 21);
		*/
		mapping.put("-1", 0);
		mapping.put("20", 1);
		mapping.put("21", 2);
		mapping.put("22", 3);
		mapping.put("30", 4);
		mapping.put("31", 5);
		mapping.put("32", 6);
		mapping.put("33", 7);
		mapping.put("34", 8);
		mapping.put("36", 9);
		mapping.put("37", 10);
		mapping.put("38", 11);
		mapping.put("39", 12);
		mapping.put("40", 13);
		mapping.put("41", 14);
		mapping.put("42", 15);
		mapping.put("43", 16);
		mapping.put("44", 17);
		mapping.put("45", 18);
		mapping.put("46", 19);
		mapping.put("47", 20);
		mapping.put("49", 21);
		mapping.put("50", 22);
		// Mary@20130226 - Change_Request-20130225, ReqNo.2 - END
		
		return mapping;
	}
	
	public static Map<String, Integer> getTradePortfolioMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put("43", 0);
		mapping.put("44", 1);
		mapping.put("81", 2);
		mapping.put("82", 3);
		mapping.put("83", 4);
		mapping.put("55", 5);
		mapping.put("78", 6);
		mapping.put("64", 7);
		mapping.put("54", 8);
		mapping.put("132", 9);
		
		
		// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START
		mapping.put("89", 10);
		mapping.put("90", 12);
		mapping.put("58", 13);
		mapping.put("91", 14);
		mapping.put("85", 15);
		mapping.put("93", 16);		//mapping.put("87", 16);	//Edited by Thinzar, Fixes_Request-20140920,ReqNo.7 (CQA reported Android displayed different brokerage fees, so compared with TcLite and iOS, then return the same metadata like them)
		mapping.put("125",17);
		mapping.put("126",18);
		// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END
		
		// Added by Mary@20120914 - Change_Request-20120719, ReqNo.12
		mapping.put("69", 19);
		
		//Added by Thinzar@20140805 - Change_Request-20140701, ReqNo. 9
		mapping.put("94", 20);		//settlement currency 
		mapping.put("151", 21);		//settlement mode aka investment portfolio
		
		return mapping;
	}
	

	public static Map<String, Integer> getDoubleLoginResponseMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put("+", 0);
		mapping.put(",", 1);
		mapping.put("-", 2);
		
		return mapping;
	}
	
	public static Map<String, Integer> getTradeStatusMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();

		int i	= 138;
		char c 	= (char)i;
		int j	= 155;
		char d 	= (char)j;
		
		mapping.put("+", 0);
		mapping.put(".", 1);
		mapping.put("4", 2);
		mapping.put("I", 3);
		mapping.put("J", 4);
		mapping.put(";", 5);
		mapping.put("<", 6);
		mapping.put(">", 7);
		mapping.put("5", 8);
		mapping.put("N", 9);
		mapping.put("6", 10);
		mapping.put("9", 11);
		mapping.put("D", 12);
		mapping.put("E", 13);
		mapping.put("F", 14);
		mapping.put("G", 15);
		mapping.put("8", 16);
		mapping.put("7", 17);
		mapping.put("M", 18);
		mapping.put(",", 19);
		mapping.put(":", 20);
		mapping.put(Character.toString(c), 21);
		mapping.put("B", 22);		// settlement currency code
		mapping.put("L", 23);
		mapping.put("?", 24);
		mapping.put("*", 25);
		
		// Added by Mary@20120806 - Fixes_Request, ReqNo.5
		mapping.put("C", 26);
		
		// Added by Mary@20120912 - Change_Requets-20120815, ReqNo.12 - START
		j	= 158;
		d 	= (char)j;
		mapping.put(Character.toString(d), 27);
		// Added by Mary@20120912 - Change_Requets-20120815, ReqNo.12 - END
		
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		mapping.put("@", 28); // Minimum Qty
		mapping.put("A", 29); // Disclosed Qty
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
		
		// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15 - START			
		j	= 155;				
		d 	= (char)j;			
		mapping.put(Character.toString(d), 30);// stock currency code
		// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15 - END
		
		//Added by Thinzar@20140819, Change_Request-20140801, ReqNo.4 - START
		j	= 177;
		d	= (char)j;
		mapping.put(Character.toString(d), 31);		//Trigger Price Type
		
		j	= 178;
		d	= (char)j;
		mapping.put(Character.toString(d), 32);		//Trigger Price Direction
		//Added by Thinzar@20140819, Change_Request-20140801, ReqNo.4 - END
		
		return mapping;
	}
	
	public static Map<String, Integer> getFavListInfoMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		
		mapping.put("+", 0);
		mapping.put("-", 1);
		mapping.put(".", 2);
		mapping.put("2", 3);
		mapping.put("3", 4);
		mapping.put("4", 5);
		mapping.put(",", 6);
		mapping.put("5", 7);
		
		return mapping;
	}
	
	public static Map<String, Integer> getTradeClientMapping() {
		Map<String, Integer> mapping = new HashMap<String, Integer>();
	
	 	//The numeric is to tell which java object attribute to set.
		//For switch case setting the java object attribute.
		mapping.put("+", 0);
		mapping.put(",", 1);
		mapping.put("-", 2);
		mapping.put(".", 3);
		mapping.put("/", 4);
		mapping.put("0", 5);
		mapping.put("1", 6);
		mapping.put("2", 7);
		mapping.put("3", 8);
		mapping.put("4", 9);
		mapping.put("5", 10);
		mapping.put("6", 11);
		mapping.put("?", 12);
		mapping.put("A", 13);
		mapping.put("C", 14);
		mapping.put("J", 15);
		mapping.put("N", 16);
		mapping.put("O", 17);
		// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3
	//	mapping.put(Character.toString( FormatUtil.convertAsciiToChar(135) ), 18);
		// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
		mapping.put(Character.toString(FormatUtil.convertAsciiToChar(136)), 18);
		
		//Added by Thinzar, Change_Request-20150401, ReqNo.1
		mapping.put("P", 19);	//accountType
		
		//Added by Thinzar, Change_Request-20160101, ReqNo.1
		int j	= 39;				
		char d 	= (char)j;			
		mapping.put(Character.toString(d), 20);	//senderCode
		
		return mapping;
	}
	
	// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
	public static Map<String, Integer> getTradeClientLimitMapping(){
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		mapping.put("2", 0);
		mapping.put("3", 1);
		mapping.put("4", 2);
	
		return mapping;
	}
	
	public static Map<String, String> getOrderSourceDefinitions() {
		
		Map<String, String> definitions = new HashMap<String, String>();
		definitions.put("I", "Internet retail");
		definitions.put("D", "Dealer");
		definitions.put("W", "Wap, Mobile");
		definitions.put("S", "SMS");
		definitions.put("P", "Phone");
		definitions.put("F", "Fax");
		
		return definitions;
	}
	
	
	public static Map<String, String> getOrderStatusDefinitions() {
		
		Map<String, String> definitions = new HashMap<String, String>();
		definitions.put("0", "Q");
		definitions.put("1", "PFL");
		definitions.put("2", "FL");
		definitions.put("3", "DN");
		definitions.put("4", "CN");
		definitions.put("5", "RP");
		definitions.put("6", "PCN");
		definitions.put("7", "PRL");
		definitions.put("8", "RJ");
		definitions.put("9", "SPS");
		definitions.put("A", "PQ");
		definitions.put("B", "CAL");
		definitions.put("C", "EXP");
		definitions.put("D", "AFB");
		definitions.put("E", "PRP");
		
		return definitions;
	}
	
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
	public static Map<String, Integer> getTradeE2EEParamMapping(){
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		mapping.put("+", 0);
		mapping.put(",", 1);
		mapping.put("-", 2);
		mapping.put(".", 3);
		mapping.put("/", 4);
		mapping.put("0", 5);
		
		return mapping;
	}
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
	
	public static void Debug(String Message){
		if(AppConstants.debug){
			System.out.println(Message);
		}
	}
	
	public static void stopAllThread(){
		if(LoginActivity.refreshThread != null)
			LoginActivity.refreshThread.stopRequest();

		if(LoginActivity.QCAlive != null)
			LoginActivity.QCAlive.stopRequest();
		
		if(LoginActivity.doubleLoginThread != null)
			LoginActivity.doubleLoginThread.stopRequest();
		
		// Added by Mary@20130603 - Change_Request-20130424, ReqNo.5
		if(CustomWindow.idleThread != null)
			CustomWindow.idleThread.stopRequest();
	}
	
	// Added by Mary@20130503 - Fixes_Request-20130424, ReqNo.4 - START
	public static void flushVariable(){
		if(lstQCExchangeCodes != null)
			lstQCExchangeCodes.clear();
		
		if(lstTrdExchangeCodes != null)
			lstTrdExchangeCodes.clear();
		
		if(mapCurrency != null)
			mapCurrency.clear();
		
		if(mapExchangeName != null)
			mapExchangeName.clear();
		
		/* Mary@20130527 - comment unused variable 
		if(mapExchangeSector != null)
			mapExchangeSector.clear();
		*/
		
		if(mapValidTrade != null)
			mapValidTrade.clear();
		
		if(tradeOrders != null)
			tradeOrders.clear();
		
		// Added by Sonia@20130913 - Change_Request-20130225, ReqNo.5
		AppConstants.MAXRECORDNO = 0;
	}
	// Added by Mary@20130503 - Fixes_Request-20130424, ReqNo.4 - END
	
	//Added by Thinzar@20140917, Change_Request-20140901, ReqNo.6 - START
	public static Map<String, Integer> getTradeGetPKLoadBalanceMapping(){
		Map<String, Integer> mapping = new HashMap<String, Integer>();
		mapping.put(",", 0);
		mapping.put("-", 1);
		mapping.put(".", 2);
		mapping.put("0", 3);
		
		return mapping;
	}
	//Added by Thinzar@20140917, Change_Request-20140901, ReqNo.6 - END
	
	//Change_Request-20170119, ReqNo.5 - START
	public static Map<String, String> getDtiExchangeMap(String dtiExchgSymbol){
		Map<String, Map<String ,String>> mainHashMap = new HashMap<String, Map<String, String>>();
		
		Map<String, String> detailsMap	= new HashMap<String, String>();
		detailsMap.put(ParamConstants.DTI_COUNTRY_TAG, "Hong Kong");
		detailsMap.put(ParamConstants.DTI_BG_RESID_TAG, Integer.toString(R.color.dti_bg_hong_kong));
		detailsMap.put(ParamConstants.MAPPED_EXCHG_TAG, "HK");
		mainHashMap.put("HKG", detailsMap);
		
		Map<String, String> detailsMap2	= new HashMap<String, String>();
		detailsMap2.put(ParamConstants.DTI_COUNTRY_TAG, "Indonesia");
		detailsMap2.put(ParamConstants.DTI_BG_RESID_TAG, Integer.toString(R.color.dti_bg_indonesia));
		detailsMap2.put(ParamConstants.MAPPED_EXCHG_TAG, "JK");
		mainHashMap.put("JKT", detailsMap2);
		
		Map<String, String> detailsMap3	= new HashMap<String, String>();
		detailsMap3.put(ParamConstants.DTI_COUNTRY_TAG, "Malaysia");
		detailsMap3.put(ParamConstants.DTI_BG_RESID_TAG, Integer.toString(R.color.dti_bg_malaysia));
		detailsMap3.put(ParamConstants.MAPPED_EXCHG_TAG, "KL");
		mainHashMap.put("MYX", detailsMap3);

		Map<String, String> detailsMap4	= new HashMap<String, String>();
		detailsMap4.put(ParamConstants.DTI_COUNTRY_TAG, "Singapore");
		detailsMap4.put(ParamConstants.DTI_BG_RESID_TAG, Integer.toString(R.color.dti_bg_singapore));
		detailsMap4.put(ParamConstants.MAPPED_EXCHG_TAG, "SI");
		mainHashMap.put("SGX-ST", detailsMap4);
		
		Map<String, String> detailsMap5	= new HashMap<String, String>();
		detailsMap5.put(ParamConstants.DTI_COUNTRY_TAG, "Thailand");
		detailsMap5.put(ParamConstants.DTI_BG_RESID_TAG, Integer.toString(R.color.dti_bg_thailand));
		detailsMap5.put(ParamConstants.MAPPED_EXCHG_TAG, "BK");
		mainHashMap.put("SET", detailsMap5);
		
		Map<String, String> detailsMap6	= new HashMap<String, String>();
		detailsMap6.put(ParamConstants.DTI_COUNTRY_TAG, "USA");
		detailsMap6.put(ParamConstants.DTI_BG_RESID_TAG, Integer.toString(R.color.dti_bg_usa));
		detailsMap6.put(ParamConstants.MAPPED_EXCHG_TAG, "O");
		mainHashMap.put("NASDAQ", detailsMap6);
		
		Map<String, String> detailsMap7	= new HashMap<String, String>();
		detailsMap7.put(ParamConstants.DTI_COUNTRY_TAG, "USA");
		detailsMap7.put(ParamConstants.DTI_BG_RESID_TAG, Integer.toString(R.color.dti_bg_usa));
		detailsMap7.put(ParamConstants.MAPPED_EXCHG_TAG, "N");
		mainHashMap.put("NYSE", detailsMap7);
		
		Map<String, String> detailsMap8	= new HashMap<String, String>();
		detailsMap8.put(ParamConstants.DTI_COUNTRY_TAG, "USA");
		detailsMap8.put(ParamConstants.DTI_BG_RESID_TAG, Integer.toString(R.color.dti_bg_usa));
		detailsMap8.put(ParamConstants.MAPPED_EXCHG_TAG, "A");
		mainHashMap.put("AMEX", detailsMap8);
		
		return mainHashMap.get(dtiExchgSymbol);
	}
	//Change_Request-20170119, ReqNo.5 - END
	
	//Change_Request-20170119, ReqNo.7
	/* LMS will provide the new exchange names
	public static String getNewExchangeName(String exchgCode){
		String mExchgCode;
		mExchgCode = (exchgCode.endsWith("D"))? exchgCode.substring(0, exchgCode.length() - 1) : exchgCode;
		
		Map<String, String> exchgMap	= new HashMap<String, String>();
		exchgMap.put("KL", "Bursa Malaysia");
		exchgMap.put("KLL", "Bursa Malaysia (Live)");
		exchgMap.put("MY", "Bursa Malaysia Derivatives");
		exchgMap.put("SG", "Singapore Exchange Ltd");
		exchgMap.put("SI", "Singapore Exchange Ltd");
		exchgMap.put("BK", "Stock Exchange of Thailand");
		exchgMap.put("JK", "Indonesia Stock Exchange");
		exchgMap.put("HK", "Hong Kong Stock Exchange");
		exchgMap.put("PH", "Philippine Stock Exchange");
		exchgMap.put("HC", "Ho Chi Minh Stock Exchange");
		exchgMap.put("HN", "Hanoi Stock Exchange");
		exchgMap.put("O", "Nasdaq Stock Market");
		exchgMap.put("N", "New York Stock Exchange");
		exchgMap.put("A", "NYSE MKT LLC");
		exchgMap.put("T", "T Equities");
		exchgMap.put("MT", "CME Metal");
		exchgMap.put("ENG", "CME Energy");
		exchgMap.put("FX", "FX");
		exchgMap.put("WIDX", "World Indices");
		
		if(exchgMap.containsKey(mExchgCode)){
			return exchgMap.get(mExchgCode) + " (" + exchgCode + ")";
		}else{
			return null;
		}
	}
	*/
}
