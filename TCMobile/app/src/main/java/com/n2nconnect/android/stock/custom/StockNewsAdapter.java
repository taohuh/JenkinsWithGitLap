package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;

import com.n2nconnect.android.stock.model.StockNews;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class StockNewsAdapter extends ArrayAdapter<StockNews>{
	private Activity mActivity;
	private ArrayList<StockNews> mStockNewsArrayList;

	public StockNewsAdapter(Activity activity, ArrayList<StockNews> stockNewsArrayList) {
		super(activity, R.layout.news_update_row);
		
		this.mActivity 				= activity;
		this.mStockNewsArrayList 	= stockNewsArrayList;
	}
	
	public class ViewHolder{
		TextView titleLabel;
		TextView symbolLabel;
		TextView dateLabel;
	}

	@Override
	public int getCount() {
		return mStockNewsArrayList.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		View rowView = convertView;
		
		if(rowView == null){
			LayoutInflater inflater = mActivity.getLayoutInflater();
			rowView 	= inflater.inflate(R.layout.news_update_row, null, true);
			
			viewHolder	= new ViewHolder();
			viewHolder.titleLabel = (TextView)rowView.findViewById(R.id.newsTitleLabel);
			viewHolder.symbolLabel = (TextView)rowView.findViewById(R.id.newsSymbolLabel);
			viewHolder.dateLabel = (TextView)rowView.findViewById(R.id.newsDateLabel);
			
			rowView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) rowView.getTag();
		}
		
		StockNews news = mStockNewsArrayList.get(position);
		String newsTitle	= news.getNewsTitle();
		viewHolder.titleLabel.setText(Html.fromHtml(newsTitle).toString());
		viewHolder.symbolLabel.setText(news.getSymbolCode());
		viewHolder.dateLabel.setText(news.getDateTime());
		
		return rowView;
	}
}