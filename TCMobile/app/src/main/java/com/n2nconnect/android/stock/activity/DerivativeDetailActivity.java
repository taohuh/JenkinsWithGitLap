package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.DerivativeDetail;
import com.n2nconnect.android.stock.model.DervTradePrtfDetailRpt;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class DerivativeDetailActivity extends CustomWindow implements
		OnDoubleLoginListener, OnRefreshStockListener {

	private DoubleLoginThread loginThread;
	private ArrayList<DervTradePrtfDetailRpt> dervTradePrtfDetailRptListO,
			dervTradePrtfDetailRptListD;
	private ViewPager viewPager;
	private ImageView leftImage = null;
	private ImageView rightImage = null;
	private ProgressDialog dialog;
	private String atpUserParam;
	private String senderCode;
	private String stockCode;
	private int intRetryCtr = 0;
	private ImageButton btnRefresh;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Added by Mary@20130718 - Fixes_Request-20130711, ReqNo.4 - START
		loginThread = ((StockApplication) DerivativeDetailActivity.this
				.getApplication()).getDoubleLoginThread();
		if (loginThread == null) {
			if (AppConstants.hasLogout) {
				finish();
			} else {
				loginThread = new DoubleLoginThread(
						sharedPreferences.getString(
								PreferenceConstants.USER_NAME, ""),
						sharedPreferences.getString(
								PreferenceConstants.ATP_USER_PARAM, ""));

				if (AppConstants.brokerConfigBean.isEnableEncryption())
					loginThread.setUserParam(AppConstants.userParamInByte);

				loginThread.start();
				((StockApplication) DerivativeDetailActivity.this.getApplication())
						.setDoubleLoginThread(loginThread);
			}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 -
			// START
		} else if (loginThread.isPaused()) {
			loginThread.setRegisteredListener(DerivativeDetailActivity.this);
			loginThread.resumeRequest();
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
		}
		loginThread.setRegisteredListener(DerivativeDetailActivity.this);
		// Added by Mary@20130718 - Fixes_Request-20130711, ReqNo.4 - END

		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
		currContext = DerivativeDetailActivity.this;
		if (idleThread != null && idleThread.isPaused())
			idleThread.resumeRequest(false);
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END

		setContentView(R.layout.derivative_detail_view);

		this.title.setText(getResources().getString(R.string.derivative_detail_module_title));

		btnRefresh = (ImageButton) findViewById(R.id.btnRefresh);
		btnRefresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (idleThread == null
						|| (idleThread != null && !idleThread.isExpired())) {
					new derivativeTradePrtfDtlRptRefresh().execute();
				}
			}
		});

		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(new PagerAdapter() {

			public int getCount() {
				return 2;
			}

			@Override
			public Object instantiateItem(ViewGroup container, int position) {
				View inflatedView = null;

				if (position == 0) {
					inflatedView = ((LayoutInflater) DerivativeDetailActivity.this
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
							.inflate(R.layout.derivative_detail_day_view,
									container, false);

				} else if (position == 1) {
					inflatedView = ((LayoutInflater) DerivativeDetailActivity.this
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
							.inflate(R.layout.derivative_detail_overnight_view,
									container, false);
				}

				((ViewPager) container).addView(inflatedView);
				return inflatedView;
			}

			@Override
			public boolean isViewFromObject(View view, Object obj) {
				return view == ((LinearLayout) obj);
			}

		});
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageSelected(int page) {
				if (page == ParamConstants.INT_PAGE_DAY) {
					leftImage.setImageResource(LayoutSettings.indicatorArrow);
					rightImage.setImageDrawable(null);
				} else {
					leftImage.setImageDrawable(null);
					rightImage.setImageResource(LayoutSettings.indicatorArrow);
				}

			}
		});

		leftImage = (ImageView) findViewById(R.id.leftImage);
		rightImage = (ImageView) findViewById(R.id.rightImage);
		final LinearLayout leftLayout = (LinearLayout) findViewById(R.id.leftLayout);
		final LinearLayout rightLayout = (LinearLayout) findViewById(R.id.rightLayout);
		leftLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				leftImage.setImageResource(LayoutSettings.indicatorArrow);
				rightImage.setImageDrawable(null);
				viewPager.setCurrentItem(ParamConstants.INT_PAGE_DAY);
			}
		});

		rightLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				leftImage.setImageDrawable(null);
				rightImage.setImageResource(LayoutSettings.indicatorArrow);
				viewPager.setCurrentItem(ParamConstants.INT_PAGE_OVERNIGHT);
			}

		});

		leftLayout.performClick();
	}

	@Override
	protected void onResume() {
		super.onResume();

		dialog = new ProgressDialog(DerivativeDetailActivity.this);

		atpUserParam = sharedPreferences.getString(
				PreferenceConstants.ATP_USER_PARAM, null);
		senderCode = sharedPreferences.getString(
				PreferenceConstants.SENDER_CODE, null);

		stockCode = AppConstants.dervTradePrtfSubDetailRpt.getStockCode();

		if (idleThread == null
				|| (idleThread != null && !idleThread.isExpired())) {
			new derivativeTradePrtfDtlRptRefresh().execute();
		}
	}

	private class derivativeTradePrtfDtlRptRefresh extends
			AsyncTask<Void, Void, DerivativeDetail> {
		Exception exception = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				DerivativeDetailActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				if (!DerivativeDetailActivity.this.isFinishing())
					DerivativeDetailActivity.this.dialog.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected DerivativeDetail doInBackground(Void... params) {

			Map<String, Object> parameters = new HashMap<String, Object>();
			TradeAccount tradeAccount = AppConstants.selectedAcc;
			DerivativeDetail derivativeDetail = null;

			if (tradeAccount != null) {
				if (AppConstants.brokerConfigBean.isEnableEncryption())
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG,
							AppConstants.userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);

				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
				parameters.put(ParamConstants.ACCOUNTNO_TAG,
						tradeAccount.getAccountNo());
				parameters.put(ParamConstants.BROKERCODE_TAG,
						tradeAccount.getBrokerCode());
				parameters.put(ParamConstants.BRANCHCODE_TAG,
						tradeAccount.getBranchCode());
				parameters.put(ParamConstants.EXCHANGECODE_TAG,
						tradeAccount.getExchangeCode());
				parameters.put(ParamConstants.STOCK_CODE_TAG, stockCode);
				parameters.put(ParamConstants.MODE_TAG, "1"); // overnight mode

				try {
					String response = null;
					try {
						response = AtpConnectUtil
								.tradePortfolioDetailRpt(parameters);
						while (intRetryCtr < AppConstants.intMaxRetry
								&& response == null) {
							intRetryCtr++;
							response = AtpConnectUtil
									.tradePortfolioDetailRpt(parameters);
						}
						intRetryCtr = 0;

						if (response == null)
							throw new Exception(
									ErrorCodeConstants.FAIL_GET_PORTFOLIO);
					} catch (Exception e) {
						throw e;
					}

					dervTradePrtfDetailRptListO = AtpMessageParser
							.parseTradePrtfDtlRptRefresh(response);

					parameters.put(ParamConstants.MODE_TAG, "2"); // day mode
					try {
						response = AtpConnectUtil
								.tradePortfolioDetailRpt(parameters);
						while (intRetryCtr < AppConstants.intMaxRetry
								&& response == null) {
							intRetryCtr++;
							response = AtpConnectUtil
									.tradePortfolioDetailRpt(parameters);
						}
						intRetryCtr = 0;

						if (response == null)
							throw new Exception(
									ErrorCodeConstants.FAIL_GET_PORTFOLIO);
					} catch (Exception e) {
						throw e;
					}

					dervTradePrtfDetailRptListD = AtpMessageParser
							.parseTradePrtfDtlRptRefresh(response);

					derivativeDetail = new DerivativeDetail();
					derivativeDetail
							.setDervTradePrtfDetailRptListD(dervTradePrtfDetailRptListD);
					derivativeDetail
							.setDervTradePrtfDetailRptListO(dervTradePrtfDetailRptListO);
					derivativeDetail
							.calculate(AppConstants.dervTradePrtfSubDetailRpt);

				} catch (FailedAuthenicationException e) {
					// isExpired = true;
					if (LoginActivity.QCAlive != null)
						LoginActivity.QCAlive.stopRequest();
					if (LoginActivity.refreshThread != null)
						LoginActivity.refreshThread.stopRequest();
					exception = e;
				} catch (Exception ex) {
					exception = ex;
				}

			}
			return derivativeDetail;
		}

		@Override
		protected void onPostExecute(DerivativeDetail derivativeDetail) {
			super.onPostExecute(derivativeDetail);

			// day
			if (derivativeDetail.getDervTradePrtfDetailRptListD().size() != 0) {
				LinearLayout pageOneLayout = (LinearLayout) viewPager
						.getChildAt(ParamConstants.INT_PAGE_DAY);
				
				((TextView) pageOneLayout.findViewById(R.id.avgBuyPrice))
						.setText(FormatUtil.formatPrice(derivativeDetail.getAvgBuyPriceD(), "0.000"));	//Edited by Thinzar, Fixes_Request-20140820,ReqNo.18
				((TextView) pageOneLayout.findViewById(R.id.avgSellPrice))
						.setText(FormatUtil.formatPrice(derivativeDetail.getAvgSellPriceD(), "0.000")); 	//Edited by Thinzar, Fixes_Request-20140820,ReqNo.18
				((TextView) pageOneLayout.findViewById(R.id.totalBuyQty))
						.setText(String.valueOf(derivativeDetail.getTotalBuyQtyD()));	//Edited by Thinzar, Fixes_Request-20140820,ReqNo.16
				((TextView) pageOneLayout.findViewById(R.id.totalSellQty))
						.setText(String.valueOf(derivativeDetail.getTotalSellQtyD()));	//Edited by Thinzar, Fixes_Request-20140820,ReqNo.16
				TextView titlePrice = (TextView) pageOneLayout
						.findViewById(R.id.titlePrice);
				titlePrice.setText(getString(R.string.derivative_price)
						+ "("
						+ derivativeDetail.getDervTradePrtfDetailRptListD()
								.get(0).getCurrencySymbol() + ")");

				TableLayout tableLayoutD = (TableLayout) pageOneLayout
						.findViewById(R.id.derivativeDayTable);
				tableLayoutD.removeAllViews();

				for (DervTradePrtfDetailRpt dervTradePrtfDetailRpt : dervTradePrtfDetailRptListD) {

					final TableRow tableRow = new TableRow(
							DerivativeDetailActivity.this);
					tableRow.setLayoutParams(new TableLayout.LayoutParams(
							TableLayout.LayoutParams.MATCH_PARENT,
							TableLayout.LayoutParams.WRAP_CONTENT));
					View rowView = getLayoutInflater().inflate(
							R.layout.derivative_detail_day_row, null);

					TextView buyLabel = (TextView) rowView
							.findViewById(R.id.buyLabel);
					TextView sellLabel = (TextView) rowView
							.findViewById(R.id.sellLabel);
					((TextView) rowView.findViewById(R.id.priceLabel))
							.setText(FormatUtil
									.formatFloatToString(dervTradePrtfDetailRpt
											.getMatchedPrice()));

					if (dervTradePrtfDetailRpt.getSide().equalsIgnoreCase(
							ParamConstants.SELL_SIDE)) {
						sellLabel.setText(String.valueOf(dervTradePrtfDetailRpt.getMatchedQty()));		//Edited by Thinzar, Fixes_Request-20140820,ReqNo.16
					} else if (dervTradePrtfDetailRpt.getSide()
							.equalsIgnoreCase(ParamConstants.BUY_SIDE)) {
						buyLabel.setText(String.valueOf(dervTradePrtfDetailRpt.getMatchedQty()));		//Edited by Thinzar, Fixes_Request-20140820,ReqNo.16
					}

					tableRow.addView(rowView);
					tableLayoutD.addView(tableRow,
							new TableLayout.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT,
									TableRow.LayoutParams.WRAP_CONTENT));
				}
			}

			// overnight
			if (derivativeDetail.getDervTradePrtfDetailRptListO().size() != 0) {
				LinearLayout pageTwoLayout = (LinearLayout) viewPager
						.getChildAt(ParamConstants.INT_PAGE_OVERNIGHT);
				
				((TextView) pageTwoLayout.findViewById(R.id.avgBuyPrice))
						.setText(FormatUtil.formatPrice(derivativeDetail.getAvgBuyPriceO(), "0.000"));	//Edited by Thinzar, Fixes_Request-20140820,ReqNo.18
				((TextView) pageTwoLayout.findViewById(R.id.avgSellPrice))
						.setText(FormatUtil.formatPrice(derivativeDetail.getAvgSellPriceO(), "0.000"));	//Edited by Thinzar, Fixes_Request-20140820,ReqNo.18
				((TextView) pageTwoLayout.findViewById(R.id.totalBuyQty))
						.setText(String.valueOf(derivativeDetail
								.getTotalBuyQtyO()));
				((TextView) pageTwoLayout.findViewById(R.id.totalSellQty))
						.setText(String.valueOf(derivativeDetail
								.getTotalSellQtyO()));
				((TextView) pageTwoLayout.findViewById(R.id.totalUnrealised))
						.setText(FormatUtil
								.formatFloatToString(derivativeDetail
										.getTotalUnrealised()));
				TextView titlePrice = (TextView) pageTwoLayout
						.findViewById(R.id.titlePrice);
				titlePrice.setText(getString(R.string.derivative_price)
						+ "("
						+ derivativeDetail.getDervTradePrtfDetailRptListO()
								.get(0).getCurrencySymbol() + ")");

				TableLayout tableLayoutO = (TableLayout) pageTwoLayout
						.findViewById(R.id.derivativeOvernightTable);
				tableLayoutO.removeAllViews();

				for (DervTradePrtfDetailRpt dervTradePrtfDetailRpt : dervTradePrtfDetailRptListO) {

					final TableRow tableRow = new TableRow(
							DerivativeDetailActivity.this);
					tableRow.setLayoutParams(new TableLayout.LayoutParams(
							TableLayout.LayoutParams.MATCH_PARENT,
							TableLayout.LayoutParams.WRAP_CONTENT));
					View rowView = getLayoutInflater().inflate(
							R.layout.derivative_detail_overnight_row, null);

					TextView buyLabel = (TextView) rowView
							.findViewById(R.id.buyLabel);
					TextView sellLabel = (TextView) rowView
							.findViewById(R.id.sellLabel);
					((TextView) rowView.findViewById(R.id.priceLabel))
							.setText(FormatUtil
									.formatFloatToString(dervTradePrtfDetailRpt
											.getMatchedPrice()));
					((TextView) rowView.findViewById(R.id.unrealisedLabel))
							.setText(FormatUtil
									.formatFloatToString(dervTradePrtfDetailRpt
											.getUnrealised()));

					if (dervTradePrtfDetailRpt.getSide().equalsIgnoreCase(
							ParamConstants.SELL_SIDE)) {
						sellLabel.setText(String.valueOf(dervTradePrtfDetailRpt
								.getMatchedQty()));
					} else if (dervTradePrtfDetailRpt.getSide()
							.equalsIgnoreCase(ParamConstants.BUY_SIDE)) {
						buyLabel.setText(String.valueOf(dervTradePrtfDetailRpt
								.getMatchedQty()));
					}

					tableRow.addView(rowView);
					tableLayoutO.addView(tableRow,
							new TableLayout.LayoutParams(
									TableRow.LayoutParams.MATCH_PARENT,
									TableRow.LayoutParams.WRAP_CONTENT));
				}
			}

			if (dialog != null && dialog.isShowing())
				dialog.dismiss();

		}

	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler() {
		public void handleMessage(Message message) {
			List<String> response = (List<String>) message.obj;
			/*
			 * Mary@20130606 - Fixes_Request-20130523, ReqNo.7 if(
			 * response.get(2).equals("1102") ){
			 */
			/*
			 * Mary@20130717 - Fixes_Request-20130711, ReqNo.4 if(response !=
			 * null && response.size() > 2 && response.get(2).equals("1102") ){
			 */
			if (response != null && response.size() > 2
					&& !response.get(2).equals("0")) {
				if (DerivativeDetailActivity.this.mainMenu.isShowing())
					DerivativeDetailActivity.this.mainMenu.hide();

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if (LoginActivity.refreshThread != null)
					LoginActivity.refreshThread.stopRequest();

				if (loginThread != null)
					loginThread.stopRequest();

				/*
				 * Mary@20130715 - Change_Request-20130424, ReqNo.15
				 * if(portfolioThread != null) portfolioThread.stopRequest();
				 */

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if (LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();

				if (!isFinishing())
					DerivativeDetailActivity.this
							.doubleLoginMessage((String) response.get(1));
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
	}

	public void atpsessionExpired() {
		// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
		if (LoginActivity.refreshThread != null)
			LoginActivity.refreshThread.stopRequest();
		// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
		if (loginThread != null)
			loginThread.stopRequest();
		// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
		if (LoginActivity.QCAlive != null)
			LoginActivity.QCAlive.stopRequest();
		this.sessionExpired();
	}

}
