package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;

import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 *
 * This is the class that manages our menu items and the popup window.
 */
public class ScrollCustomMenu {

	/**
	 * Some global variables.
	 */
	private ArrayList<CustomMenuItem> menuItems;
	private OnMenuItemSelectedListener listener;
	private Context context;
	private LayoutInflater layoutInflater;
	private PopupWindow popupWindow;
	private boolean isShowing;
	private boolean hideOnSelect;
	private ImageView iconImage;

	public interface OnMenuItemSelectedListener {
		public void menuItemSelectedEvent(CustomMenuItem selection);
	}

	public boolean isShowing() {
		return isShowing;
	}

	public void setHideOnSelect(boolean doHideOnSelect) {
		hideOnSelect = doHideOnSelect;
	}

	public synchronized void setMenuItems(ArrayList<CustomMenuItem> items) throws Exception {
		if (isShowing) {
			throw new Exception("Menu list may not be modified while menu is displayed.");
		}
		menuItems = items;
	}

	public ScrollCustomMenu(Context context, OnMenuItemSelectedListener listener, LayoutInflater inflater) {
		this.listener = listener;
		menuItems = new ArrayList<CustomMenuItem>();
		this.context = context;
		layoutInflater = inflater;
	}

	public synchronized void show(View view) {
		isShowing = true;
		boolean isLandscape = false;
		int itemCount = menuItems.size();
		if (itemCount < 1) {
			return; // no menu items to show
		}
		if (popupWindow != null) {
			return; // already showing
		}
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		if (display.getWidth() > display.getHeight()) {
			isLandscape = true;
		}
		View menuView = layoutInflater.inflate(R.layout.scroll_custom_menu, null);
		popupWindow = new PopupWindow(menuView, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, false);
		popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
		popupWindow.setWidth(display.getWidth());
		popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

		LinearLayout linear = (LinearLayout) menuView.findViewById(R.id.customMenuLayout);
		linear.removeAllViews();

		for (int i = 0; i < menuItems.size(); i++) {

			final CustomMenuItem menuItem = menuItems.get(i);
			View itemLayout = layoutInflater.inflate(R.layout.custom_menu_item, null);
			// final TextView captionLabel =
			// (TextView)itemLayout.findViewById(R.id.menuItemCaption);
			// captionLabel.setText(menuItem.getCaption()); //commented out by Thinzar, Change_Request-20160101, ReqNo.3

			iconImage = (ImageView) itemLayout.findViewById(R.id.menuItemIcon);

			// set pressed state programatically, Change_Request-20160101, ReqNo.3
			StateListDrawable states = new StateListDrawable();

			//Edited, Fixes_Request_20170701, ReqNo.10
			states.addState(new int[] { android.R.attr.state_pressed },
					ResourcesCompat.getDrawable(context.getResources(), menuItem.getImageResourceIdPressedState(), null));
			states.addState(new int[] {}, ResourcesCompat.getDrawable(context.getResources(), menuItem.getImageResourceId(), null));
			iconImage.setImageDrawable(states);

			iconImage.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {

					// captionLabel.setTextColor(Color.BLACK);
					// captionLabel.setBackgroundDrawable(null);

					listener.menuItemSelectedEvent(menuItem);
					if (hideOnSelect) {
						hide();
					}
				}
			});

			linear.addView(itemLayout);
		}
	}

	public synchronized void hide() {
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
		try {
			isShowing = false;
			if (popupWindow != null && popupWindow.isShowing()) {
				popupWindow.dismiss();
				popupWindow = null;
			}
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
		return;
	}
}