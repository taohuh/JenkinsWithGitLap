package com.n2nconnect.android.stock.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class StockAlertDisclaimerActivity extends CustomWindow {

	private TextView txtStkAlertDisclaimer;
	private Button btnAgreeStkAlertDisclaimer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.stock_alert_disclaimer);
		
		this.title.setText(getResources().getString(R.string.stock_alert_disclaimer_module_title));
		this.icon.setImageResource(R.drawable.disclaimer);
		
		txtStkAlertDisclaimer	= (TextView)findViewById(R.id.txtStkAlertDisclaimer);
		btnAgreeStkAlertDisclaimer	= (Button)findViewById(R.id.btnAgreeStkAlertDisclaimer);
		
		txtStkAlertDisclaimer.setText(AppConstants.brokerConfigBean.getStkAlertDisclaimer());
		
		btnAgreeStkAlertDisclaimer.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				AppConstants.hasStockAlertDisclaimerShown	= true;
				
				Intent i	= new Intent();
				i.setClass(StockAlertDisclaimerActivity.this, StockAlertActivity.class);
				startActivity(i);
				
				StockAlertDisclaimerActivity.this.finish();
			}
		});
	}
}
