package com.n2nconnect.android.stock.gcm;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.activity.QuoteActivity;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.SystemUtil;

import android.content.Context;
import android.content.SharedPreferences;

public class StockAlertUtil {
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST 	= 9000;
	
	public static String getGcmRegistrationId(Context context) throws IOException{
		
		String gcmRegId = null;
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
		String sender_id	= AppConstants.pushSenderIdMap.get(context.getPackageName());
		
		gcmRegId = gcm.register(sender_id);
		
		return gcmRegId;
	}
	
	public static void saveGcmInfoToPref(Context context, String strCurrentUsername, String gcmRegId){
		
		SharedPreferences pushInfoPref = context.getSharedPreferences(PreferenceConstants.GCM_PREF_FILE, context.MODE_PRIVATE);
		SharedPreferences.Editor pushInfoEditor	= pushInfoPref.edit();
		
		pushInfoEditor.putString(PreferenceConstants.GCM_USERNAME, strCurrentUsername);
		pushInfoEditor.putString(PreferenceConstants.GCM_REG_ID, gcmRegId);
		
		pushInfoEditor.commit();
	}
	
	public static void clearGcmInfoFromPref(Context context){
		System.out.println("clearGcmInfoFromPref");
		SharedPreferences pushInfoPref = context.getSharedPreferences(PreferenceConstants.GCM_PREF_FILE, context.MODE_PRIVATE);
		SharedPreferences.Editor pushInfoEditor	= pushInfoPref.edit();
		pushInfoEditor.remove(PreferenceConstants.GCM_REG_APP_VERSION);
		pushInfoEditor.remove(PreferenceConstants.GCM_REG_ID);
		//pushInfoEditor.remove(PreferenceConstants.GCM_USERNAME); //NOTE: remain username
		pushInfoEditor.remove(PreferenceConstants.GCM_REG_OS_VERSION);
		pushInfoEditor.remove(PreferenceConstants.HAS_DEVICE_REGISTERED);
		pushInfoEditor.commit();
	}
	
	public static void saveDeviceRegisterFlag(Context context){
		SharedPreferences pushInfoPref = context.getSharedPreferences(PreferenceConstants.GCM_PREF_FILE, context.MODE_PRIVATE);
		SharedPreferences.Editor pushInfoEditor	= pushInfoPref.edit();
		pushInfoEditor.putString(PreferenceConstants.GCM_REG_APP_VERSION, SystemUtil.getAppVersionName(context));
		pushInfoEditor.putString(PreferenceConstants.GCM_REG_OS_VERSION, SystemUtil.getDeviceOSVersion());
		pushInfoEditor.putBoolean(PreferenceConstants.HAS_DEVICE_REGISTERED, true);
		pushInfoEditor.commit();
		
	}
	
	public static StockAlertPubKey getPublicKey(String brokerCode){
		String getPublicKeyUrl	= AppConstants.brokerConfigBean.getStockAlertPubKeyUrl();

		//Change_Request-20170601, ReqNo.15
		String stockAlertBHCode = AppConstants.brokerConfigBean.getStockAlertBHCode();
		if(stockAlertBHCode != null && stockAlertBHCode.length() > 0)
			brokerCode 			= stockAlertBHCode;

		JSONObject json = new JSONObject();
		try {
			json.put(AppConstants.PUSH.STATUS, "");
			json.put(AppConstants.PUSH.MSG, "");
			json.put(AppConstants.PUSH.CT, "1");
			json.put(AppConstants.PUSH.AE, "1");

			JSONObject jsonOj = new JSONObject();
			jsonOj.put(AppConstants.PUSH.APPID, AppConstants.TCANDROID_APP_ID); // MA mobile Android
			jsonOj.put(AppConstants.PUSH.BH, brokerCode);

			json.put(AppConstants.PUSH.EV, jsonOj);

			String convertJson = jsonOj.toString();
			String md5 = convertMd5(convertJson);

			json.put(AppConstants.PUSH.CS, md5);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		DefinitionConstants.Debug("StockAlertUtil.getPublicKeyUrl=" + getPublicKeyUrl);
		String response = httpPostJson(getPublicKeyUrl, json);
		DefinitionConstants.Debug("StockAlertUtil.getPublicKey response:" + response);
		
		if(response != null)
			return convertJsonToStockAlertObject(response);
		else 
			return null;
	}
	
	public static String generateJWTToken(StockAlertPubKey stockAlertObj, Context context, String brokerCode, JSONObject claimJson)
	throws Exception{
		String jwtToken 	= null;
		String jsonWebPrivateKey 	= null;
		Map<String, String> jwtMap = new HashMap<String, String>();

		//Change_Request-20170601, ReqNo.15
		String stockAlertBHCode = AppConstants.brokerConfigBean.getStockAlertBHCode();
		if(stockAlertBHCode != null && stockAlertBHCode.length() > 0)
			brokerCode 			= stockAlertBHCode;
		
		try {
			RSAPrivateKey rsaPrivateKey = null;
			
			if (!AppConstants.STOCKALERT.privateKey.equals("")) {
				jsonWebPrivateKey = AppConstants.STOCKALERT.privateKey;
			} else {
				if (N2NJwt.generateKey(context))
					jsonWebPrivateKey = N2NJwt.getPrivateKeyFromSharedPref(context);
			}
			
			try {
				rsaPrivateKey = FormatUtil.loadPrivateKey(jsonWebPrivateKey);
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("StockAlertUtil: Failed to generate JWT keys.");
			}
			
			DefinitionConstants.Debug("claimJson:" + claimJson.toString());
			
			String pubKey 	= stockAlertObj.getPublicKey().replace("[", "");
			String newPubkey = pubKey.replace("]", "");
			String audience = "SANS";
			String subject 	= "";
			int expireTime 	= 10; 	
			int nbf 		= 10; 	
			
			jwtMap	= N2NJwt.generateJWT(newPubkey, rsaPrivateKey, stockAlertObj.getKid(), brokerCode, 
							audience, expireTime, nbf, subject, claimJson.toString());

			if (jwtMap.containsKey("s")) {
				if (jwtMap.get("s").equals("200")) {
					jwtToken = jwtMap.get("v");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jwtToken;
	}
	
	public static JSONObject getJsonForRegister(Context context, String senderCode, String brokerCode, String screenSize){
		//Change_Request-20170601, ReqNo.15
		String stockAlertBHCode = AppConstants.brokerConfigBean.getStockAlertBHCode();
		if(stockAlertBHCode != null && stockAlertBHCode.length() > 0)
			brokerCode 			= stockAlertBHCode;

		JSONObject json = new JSONObject();
		try {
			json.put(AppConstants.PUSH.EX, AppConstants.brokerConfigBean.getStrBaseATPExchangeCode());
			json.put(AppConstants.PUSH.SPC, AppConstants.brokerConfigBean.getStrLMSSponsor());
			json.put(AppConstants.PUSH.BH, brokerCode);
			json.put(AppConstants.PUSH.APPID, AppConstants.TCANDROID_APP_ID);
			json.put(AppConstants.PUSH.CC, senderCode);		
			json.put(AppConstants.PUSH.BUNDLEID, context.getPackageName());
		
			json.put(AppConstants.PUSH.DEVICETOKEN, getGcmRegIdFromPref(context));
			json.put(AppConstants.PUSH.PLATFORMNAME, SystemUtil.getDeviceOSType());
			json.put(AppConstants.PUSH.APPVERSION, SystemUtil.getAppVersionName(context));
			json.put(AppConstants.PUSH.DEVICEMODEL, SystemUtil.getDeviceModelName());
			json.put(AppConstants.PUSH.DEVICENEWTYPE, SystemUtil.getSIMOperatorName(context));
			json.put(AppConstants.PUSH.DEVICEPROVIDER,SystemUtil.getDeviceConnectivity(context));
			json.put(AppConstants.PUSH.PLATFORMINFO, SystemUtil.getDeviceOSVersion());
			json.put(AppConstants.PUSH.PLATFORMSCREENSIZE, screenSize);
			json.put(AppConstants.PUSH.IMEI, SystemUtil.getDeviceIMEI(context));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public static JSONObject getJsonForOpenStockAlert(Context context, String brokerCode, String senderCode, String screenSize){
		//Change_Request-20170601, ReqNo.15
		String stockAlertBHCode = AppConstants.brokerConfigBean.getStockAlertBHCode();
		if(stockAlertBHCode != null && stockAlertBHCode.length() > 0)
			brokerCode 			= stockAlertBHCode;

		JSONObject json = new JSONObject();
		try {
			json.put(AppConstants.PUSH.BH, brokerCode);
			json.put(AppConstants.PUSH.EX, AppConstants.brokerConfigBean.getStrBaseATPExchangeCode());
			json.put(AppConstants.PUSH.LID, senderCode);

			json.put(AppConstants.STOCKALERT.appId, AppConstants.TCANDROID_APP_ID);
			json.put(AppConstants.STOCKALERT.email, AppConstants.userEmail);
			json.put(AppConstants.STOCKALERT.mobilenum, AppConstants.userMobile);
			json.put(AppConstants.STOCKALERT.sponsor, AppConstants.brokerConfigBean.getStrLMSSponsor());
			json.put(AppConstants.STOCKALERT.regenerate, "Y");
			
			json.put(AppConstants.PUSH.PLATFORMNAME, SystemUtil.getDeviceOSType());
			json.put(AppConstants.PUSH.APPVERSION, SystemUtil.getAppVersionName(context));
			json.put(AppConstants.PUSH.DEVICEMODEL, SystemUtil.getDeviceModelName());
			json.put(AppConstants.PUSH.DEVICENEWTYPE, SystemUtil.getSIMOperatorName(context));
			json.put(AppConstants.PUSH.DEVICEPROVIDER,SystemUtil.getDeviceConnectivity(context));
			json.put(AppConstants.PUSH.PLATFORMINFO, SystemUtil.getDeviceOSVersion());
			json.put(AppConstants.PUSH.PLATFORMSCREENSIZE, screenSize);
			json.put(AppConstants.PUSH.IMEI, SystemUtil.getDeviceIMEI(context));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public static String registerDevice(String jwtToken, String brokerCode) throws Exception{
		//Change_Request-20170601, ReqNo.15
		String stockAlertBHCode = AppConstants.brokerConfigBean.getStockAlertBHCode();
		if(stockAlertBHCode != null && stockAlertBHCode.length() > 0)
			brokerCode 			= stockAlertBHCode;

		JSONObject json = new JSONObject();
		String status = "";
		
		try {
			json.put(AppConstants.PUSH.JWT, jwtToken);
			json.put(AppConstants.PUSH.BH, brokerCode);
			json.put(AppConstants.PUSH.APPID, AppConstants.TCANDROID_APP_ID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			status	= sendRegisterDeviceRequest(json);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to register device.");
		}
		
		return status;
	}
	
	public static String sendRegisterDeviceRequest(JSONObject json) throws Exception{
		String status = null;
		
		try {
			String jsonString 	= json.toString();
			String checkSum   	= convertMd5(jsonString);
			String url 			= AppConstants.brokerConfigBean.getRegisterPushLink().trim() 
								+ "ct=0&ae=0&ev=" 
								+ URLEncoder.encode(jsonString, AppConstants.STRING_ENCODING_FORMAT)
								+ "&cs=" + checkSum;
			
			DefinitionConstants.Debug("RegisterDevice url: " + url);
			DefinitionConstants.Debug("RegisterDevice json: " + jsonString);
			
			String response = httpGet(url);
			DefinitionConstants.Debug("RegisterDevice response: " + response);
			
			JSONObject regDeviceResponse = new JSONObject(response);
			status	= regDeviceResponse.getString(AppConstants.PUSH.STATUS);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to register device.");
		}
		
		return status;
	}
	
	public static JSONObject getUnregisterJson(Context context, String brokerCode,String senderCode, String username,
			String screenSize) throws Exception{
		//Change_Request-20170601, ReqNo.15
		String stockAlertBHCode = AppConstants.brokerConfigBean.getStockAlertBHCode();
		if(stockAlertBHCode != null && stockAlertBHCode.length() > 0)
			brokerCode 			= stockAlertBHCode;

		JSONObject json = new JSONObject();
		
		try{
			json.put(AppConstants.PUSH.OPT, "X");	//X- remove device
			
			json.put(AppConstants.PUSH.EX, AppConstants.brokerConfigBean.getStrBaseATPExchangeCode());
			json.put(AppConstants.PUSH.SPC, AppConstants.brokerConfigBean.getStrLMSSponsor());
			json.put(AppConstants.PUSH.BH, brokerCode);
			json.put(AppConstants.PUSH.APPID, AppConstants.TCANDROID_APP_ID);
			json.put(AppConstants.PUSH.CC, senderCode);		
			json.put(AppConstants.PUSH.BUNDLEID, context.getPackageName());
			json.put(AppConstants.PUSH.LID, username);
			json.put(AppConstants.PUSH.DEVICETOKEN, getGcmRegIdFromPref(context));
			json.put(AppConstants.PUSH.PLATFORMNAME, SystemUtil.getDeviceOSType());
			json.put(AppConstants.PUSH.APPVERSION, SystemUtil.getAppVersionName(context));
			json.put(AppConstants.PUSH.DEVICEMODEL, SystemUtil.getDeviceModelName());
			json.put(AppConstants.PUSH.DEVICENEWTYPE, SystemUtil.getSIMOperatorName(context));
			json.put(AppConstants.PUSH.DEVICEPROVIDER,SystemUtil.getDeviceConnectivity(context));
			json.put(AppConstants.PUSH.PLATFORMINFO, SystemUtil.getDeviceOSVersion());
			json.put(AppConstants.PUSH.PLATFORMSCREENSIZE, screenSize);
			json.put(AppConstants.PUSH.IMEI, SystemUtil.getDeviceIMEI(context));
			
		}catch(JSONException e){
			e.printStackTrace();
		}
		
		return json;
	}
	
	public static JSONObject getCheckStatusJson(Context context, String brokerCode,String senderCode, String username,
			String screenSize) throws Exception{
		//Change_Request-20170601, ReqNo.15
		String stockAlertBHCode = AppConstants.brokerConfigBean.getStockAlertBHCode();
		if(stockAlertBHCode != null && stockAlertBHCode.length() > 0)
			brokerCode 			= stockAlertBHCode;

		JSONObject json = new JSONObject();
		
		try{
			json.put(AppConstants.PUSH.OPT, "S");	//S-Select
			
			json.put(AppConstants.PUSH.EX, AppConstants.brokerConfigBean.getStrBaseATPExchangeCode());
			json.put(AppConstants.PUSH.SPC, AppConstants.brokerConfigBean.getStrLMSSponsor());
			json.put(AppConstants.PUSH.BH, brokerCode);
			json.put(AppConstants.PUSH.APPID, AppConstants.TCANDROID_APP_ID);
			json.put(AppConstants.PUSH.CC, senderCode);		
			json.put(AppConstants.PUSH.BUNDLEID, context.getPackageName());
			json.put(AppConstants.PUSH.LID, username);
			json.put(AppConstants.PUSH.DEVICETOKEN, getGcmRegIdFromPref(context));
			json.put(AppConstants.PUSH.PLATFORMNAME, SystemUtil.getDeviceOSType());
			json.put(AppConstants.PUSH.APPVERSION, SystemUtil.getAppVersionName(context));
			json.put(AppConstants.PUSH.DEVICEMODEL, SystemUtil.getDeviceModelName());
			json.put(AppConstants.PUSH.DEVICENEWTYPE, SystemUtil.getSIMOperatorName(context));
			json.put(AppConstants.PUSH.DEVICEPROVIDER,SystemUtil.getDeviceConnectivity(context));
			json.put(AppConstants.PUSH.PLATFORMINFO, SystemUtil.getDeviceOSVersion());
			json.put(AppConstants.PUSH.PLATFORMSCREENSIZE, screenSize);
			json.put(AppConstants.PUSH.IMEI, SystemUtil.getDeviceIMEI(context));
			
		}catch(JSONException e){
			e.printStackTrace();
		}
		
		return json;
	}
	
	/*public static String unRegisterDevice(Context context, String brokerCode,String senderCode, String username,
			String screenSize) throws Exception{
		//Change_Request-20170601, ReqNo.15
		String stockAlertBHCode = AppConstants.brokerConfigBean.getStockAlertBHCode();
		if(stockAlertBHCode != null && stockAlertBHCode.length() > 0)
			brokerCode 			= stockAlertBHCode;

		String status = null;
		JSONObject json = new JSONObject();
		
		try{
			json.put(AppConstants.PUSH.OPT, "X");	//X- remove device
			json.put(AppConstants.PUSH.EX, AppConstants.brokerConfigBean.getStrBaseATPExchangeCode());
			json.put(AppConstants.PUSH.SPC, "1");
			json.put(AppConstants.PUSH.BH, brokerCode);
			json.put(AppConstants.PUSH.LID, username);
			json.put(AppConstants.PUSH.APPID, AppConstants.TCANDROID_APP_ID);
			json.put(AppConstants.PUSH.CC, senderCode);
			json.put(AppConstants.PUSH.BUNDLEID, context.getPackageName());
			json.put(AppConstants.PUSH.DEVICETOKEN, getGcmRegIdFromPref(context));
			json.put(AppConstants.PUSH.PLATFORMNAME, SystemUtil.getDeviceOSType());
			json.put(AppConstants.PUSH.APPVERSION, SystemUtil.getAppVersionName(context));
			json.put(AppConstants.PUSH.DEVICEMODEL, SystemUtil.getDeviceModelName());
			json.put(AppConstants.PUSH.DEVICENEWTYPE, SystemUtil.getSIMOperatorName(context));
			json.put(AppConstants.PUSH.DEVICEPROVIDER,SystemUtil.getDeviceConnectivity(context));
			json.put(AppConstants.PUSH.PLATFORMINFO, SystemUtil.getDeviceOSVersion());
			json.put(AppConstants.PUSH.PLATFORMSCREENSIZE, screenSize);
			
		}catch(JSONException e){
			e.printStackTrace();
		}
		
		try {
			status	= sendRegisterDeviceRequest(json);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to register device.");
		}
		
		return status;
	}
	*/
	public static String convertMd5(String data) {
		String original = data;
		MessageDigest md;
		StringBuffer sb = null;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(original.getBytes());
			byte[] digest = md.digest();
			sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	public static StockAlertPubKey convertJsonToStockAlertObject(String result) {
		StockAlertPubKey stockAlertObj = new StockAlertPubKey();
		
		try {
			JSONObject json = new JSONObject(result);
			
			if (json.has(AppConstants.STOCKALERT.KEYS)) {
				
				JSONArray jsonArr = new JSONArray(json.getString(AppConstants.STOCKALERT.KEYS));
				stockAlertObj.setPublicKey(jsonArr.toString());
				
				for (int s = 0; s < jsonArr.length(); s++) {
					JSONObject jsonObj = new JSONObject(jsonArr.getString(s));

					stockAlertObj.setKty(jsonObj.getString(AppConstants.STOCKALERT.KTY));
					stockAlertObj.setKid(jsonObj.getString(AppConstants.STOCKALERT.KID));
					stockAlertObj.setAlg(jsonObj.getString(AppConstants.STOCKALERT.ALG));
					stockAlertObj.seteObject(jsonObj.getString(AppConstants.STOCKALERT.eObj));
				}
			} else{
				stockAlertObj	= null;
			}
			
		} catch (JSONException e) {
			stockAlertObj	= null;
			e.printStackTrace();
		}

		return stockAlertObj;
	}
	
	private static final String stockAlertAESKeyUrl = "https://sans-uat.asiaebroker.com/gcSANS/srvs/getNewKey";
	public static StockAlertAESKey getStockAlertAESKey() throws Exception{
		String response = null;
		StockAlertAESKey aesKeyObject = new StockAlertAESKey();
		
		try {
			response = httpGet(stockAlertAESKeyUrl);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to get AES key");
		}
		
		try {
			JSONObject json = new JSONObject(response);
			aesKeyObject.setKeyId(json.getInt("id"));
			aesKeyObject.setDkey(json.getString("dkey"));
			aesKeyObject.setDsalt(json.getString("dsalt"));
			aesKeyObject.setDiv(json.getString("div"));
			aesKeyObject.setEkey(json.getString("ekey"));
			aesKeyObject.setEsalt(json.getString("esalt"));
			aesKeyObject.setEiv(json.getString("eiv"));
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return aesKeyObject;
	}
	
	public static String httpPostJson(String strUrl, JSONObject jsonObj){
		String response = null;
		InputStream inputStream	= null;
		HttpURLConnection conn = null;
		
		DefinitionConstants.Debug("StockAlertUtil.httpPostJson: url=" + strUrl);
		DefinitionConstants.Debug("StockAlertUtil.httpPostJson: json=" + jsonObj.toString());
		
		try {
			URL url = new URL(strUrl);
			conn	= (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(jsonObj.toString());
            os.close();
            
            inputStream			= conn.getInputStream();
			if(inputStream != null){
				response	= convertInputStreamToString(inputStream);
				
			}
            
            conn.disconnect();
            
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			
			if(inputStream != null){
				try{
					inputStream.close();
				} catch(IOException ex){
					ex.printStackTrace();
				}
			}
			
			if(conn != null){
				conn.disconnect();
				conn	= null;
			}
		}
		
		return response;
	}
	
	public static String httpGet(String strUrl) throws Exception{
		String response = null;
		InputStream inputStream	= null;
		HttpURLConnection conn	= null;
		
		try{
			URL url	= new URL(strUrl);
			conn	= (HttpURLConnection) url.openConnection();
			conn.connect();
			
			int responseCode	= conn.getResponseCode();
			
			if(responseCode == HttpURLConnection.HTTP_OK){
				inputStream			= conn.getInputStream();
				if(inputStream != null){
					response	= convertInputStreamToString(inputStream);
				}
			}else{
				throw new Exception("httpGet Failed.");
			}
			
		}catch(IOException ex){
			ex.printStackTrace();
			throw new Exception(ex.getMessage());
		}finally{
			
			if(inputStream != null){
				try{
					inputStream.close();
				} catch(IOException ex){
					ex.printStackTrace();
				}
			}
			
			if(conn != null){
				conn.disconnect();
				conn	= null;
			}
		}
		
		return response;
	}
	
	private static String convertInputStreamToString(InputStream in) {
		BufferedReader reader = null;
		StringBuffer response = new StringBuffer();
		
		try {
			reader 		= new BufferedReader(new InputStreamReader(in));
			String line = "";
			
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return response.toString();
	}
	
	public static String getGcmRegIdFromPref(Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(PreferenceConstants.GCM_PREF_FILE, Context.MODE_PRIVATE);
		String regId = prefs.getString(PreferenceConstants.GCM_REG_ID, "");
		return regId;
	}
	
	public static void saveDoNotAskAgainForPushNotification(Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(AppConstants.DATA_STORE, Context.MODE_PRIVATE);
		SharedPreferences.Editor sharedPrefEditor	= sharedPreferences.edit();
		sharedPrefEditor.putBoolean(PreferenceConstants.DO_NOT_ASK_PUSH_NOTI, true);
		sharedPrefEditor.commit();
	}
}