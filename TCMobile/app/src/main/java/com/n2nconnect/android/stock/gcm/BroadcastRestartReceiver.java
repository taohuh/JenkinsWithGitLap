package com.n2nconnect.android.stock.gcm;


import com.n2nconnect.iTradeCIMBSG_uat.stock.GCMIntentService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BroadcastRestartReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction() != null
				&& (intent.getAction().equals("com.android.restart")|| intent.getAction().equals(""))) {
			Intent intents = new Intent(context, GCMIntentService.class);
			context.startService(intents);
		}
	}
	
}