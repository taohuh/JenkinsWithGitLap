package com.n2nconnect.android.stock.activity;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.imageslideshow.utils.CirclePageIndicator;
import com.n2nconnect.android.stock.imageslideshow.utils.ImageSlideAdapter;
import com.n2nconnect.android.stock.imageslideshow.utils.PageIndicator;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;

public class TutorialActivity extends Activity {

	private static final long ANIM_VIEWPAGER_DELAY = 5000;
	private static final long ANIM_VIEWPAGER_DELAY_USER_VIEW = 10000;

	private ViewPager mViewPager;
	private PageIndicator mIndicator;
	private Runnable animateViewPager;
	private Handler handler;
	private boolean stopSliding = false;
	
	private int [] tutorialImage;
	private String []title;
	private String []desc;
	
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;
	private String result;
	private Button btnSkip;
	
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutorial);
		
		//Edited by Thinzar, to remove unnecessary file reading
		if(this.getIntent() != null){
			Bundle bundle 	= this.getIntent().getExtras();
			result		 	= bundle.getString(ParamConstants.LOGIN_TYPE_TAG);
		}

		//Edited by Thinzar, handle brokers that does not have subscription
		tutorialImage 	= new int[] {R.drawable.tutorial_quote,R.drawable.tutorial_watchlist,R.drawable.tutorial_stockinfo,R.drawable.tutorial_exchange, R.drawable.tutorial_subscribe};
		if(AppConstants.brokerConfigBean.hasRegistration()) {
			title = new String[]{
					getResources().getString(R.string.tut_title_mkt_overview),
					getResources().getString(R.string.tut_title_watchlist),
					getResources().getString(R.string.tut_title_stk_details),
					getResources().getString(R.string.tut_title_exchange),
					getResources().getString(R.string.tut_title_subscribe)
			};
			desc = new String[]{
					getResources().getString(R.string.tut_desc_mkt_overview),
					getResources().getString(R.string.tut_desc_watchlist),
					getResources().getString(R.string.tut_desc_stk_details),
					getResources().getString(R.string.tut_desc_exchange),
					getResources().getString(R.string.tut_desc_subscribe)
			};

		}else{
			title = new String[]{
					getResources().getString(R.string.tut_title_mkt_overview),
					getResources().getString(R.string.tut_title_watchlist),
					getResources().getString(R.string.tut_title_stk_details),
					getResources().getString(R.string.tut_title_exchange),
					getResources().getString(R.string.tut_title_alert)
			};
			desc = new String[]{
					getResources().getString(R.string.tut_desc_mkt_overview),
					getResources().getString(R.string.tut_desc_watchlist),
					getResources().getString(R.string.tut_desc_stk_details),
					getResources().getString(R.string.tut_desc_exchange),
					getResources().getString(R.string.tut_desc_alert)
			};
		}
		
		btnSkip		= (Button) findViewById(R.id.img_skip);
		mViewPager 	= (ViewPager) findViewById(R.id.view_pager);
		mIndicator 	= (CirclePageIndicator) findViewById(R.id.indicator);

		mIndicator.setOnPageChangeListener(new PageChangeListener());
		mViewPager.setOnPageChangeListener(new PageChangeListener());
		mViewPager.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				switch (event.getAction()) {

				case MotionEvent.ACTION_CANCEL:
					break;

				case MotionEvent.ACTION_UP:
					// calls when touch release on ViewPager
					if (tutorialImage != null && tutorialImage.length != 0) {
						stopSliding = false;
						runnable(tutorialImage.length);
						handler.postDelayed(animateViewPager,
								ANIM_VIEWPAGER_DELAY_USER_VIEW);
					}
					break;

				case MotionEvent.ACTION_MOVE:
					// calls when ViewPager touch
					if (handler != null && stopSliding == false) {
						stopSliding = true;
						handler.removeCallbacks(animateViewPager); 
					}
					break;
				}
				return false;
			}
		});

		mViewPager.setAdapter(new ImageSlideAdapter(getApplicationContext(), tutorialImage,title,desc));
		mIndicator.setViewPager(mViewPager);
		runnable(tutorialImage.length);
		handler.postDelayed(animateViewPager,ANIM_VIEWPAGER_DELAY);
		
		btnSkip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(result.equals(ParamConstants.MULTI_LOGIN_TAG)){
					AppConstants.isAppMultilogin	= true;	//Change_Request-20160722, ReqNo. 6
					
					editor.putBoolean(PreferenceConstants.HAS_MULTI_LOGIN, true);
					editor.commit();
					showBrokerHouseListScreen(getApplicationContext().getPackageName());

					finish();

				} else if(result.equals(ParamConstants.SINGLE_LOGIN_TAG)){
					AppConstants.isAppMultilogin	= false;	//Change_Request-20160722, ReqNo. 6
					
					AppConstants.CURRENT_PKG_NAME	= getApplicationContext().getPackageName();
					showLoginScreen(getApplicationContext().getPackageName());
					finish();
				}
			}
		});
	}
	 
	public void showBrokerHouseListScreen(String mPackageName){

		Intent intent = new Intent();
		intent.putExtra(ParamConstants.PARAM_PACKAGE_NAME, mPackageName);
		intent.setClass(this, SelectBrokerHouseActivity.class);
		startActivity(intent);
	}

	public void showLoginScreen(String mPackageName){
		Intent intent = new Intent();
		intent.putExtra(ParamConstants.PARAM_PACKAGE_NAME, mPackageName);
		intent.setClass(this, LoginActivity.class);
		startActivity(intent);
	}
	
	private class PageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int state) {
			if (state == ViewPager.SCROLL_STATE_IDLE) {
				
			}
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageSelected(int arg0) {
		}
	}
	
	public void runnable(final int size) {
		handler = new Handler();
		animateViewPager = new Runnable() {
			public void run() {
				
				if (!stopSliding) {
					
					if (mViewPager.getCurrentItem() == size - 1) {
						mViewPager.setCurrentItem(0);
					} else {
						mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
					}
					
					handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
					
				}
			}
		};
	}
	
	@Override
	public void onPause() {
		if (handler != null) {
			//Remove callback
			handler.removeCallbacks(animateViewPager);
		}
		super.onPause();
	}
}