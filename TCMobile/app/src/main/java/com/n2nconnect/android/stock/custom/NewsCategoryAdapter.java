package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;

import com.n2nconnect.android.stock.model.NewsCategory;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NewsCategoryAdapter extends ArrayAdapter<NewsCategory>{
	private ArrayList<NewsCategory> mNewsCategories;
	private Activity mActivity;
	
	public NewsCategoryAdapter(Activity activity, ArrayList<NewsCategory> newsCategories) {
		super(activity, R.layout.news_category_row, newsCategories);
		
		this.mActivity = activity;
		this.mNewsCategories = newsCategories;
	}

	@Override
	public int getCount() {
		return mNewsCategories.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		View rowView = convertView;

		if (rowView == null) {
			LayoutInflater inflater = mActivity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.news_category_row, null, true);
			viewHolder = new ViewHolder();

			viewHolder.categoryTitle = (TextView) rowView.findViewById(R.id.tv_news_category_title);
			
			rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) rowView.getTag();
		}
		
		viewHolder.categoryTitle.setText(mNewsCategories.get(position).getName());
		
		return rowView;
	}	
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getView(position, convertView, parent);
	}

	private class ViewHolder{
		public TextView categoryTitle;
	}
}