package com.n2nconnect.android.stock.activity;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;


public class PortfolioDisclaimerActivity extends CustomWindow{
    private PortfolioDisclaimerTask portfolioDisclaimerTask;
    private ProgressDialog dialog;
    private AlertDialog alertDialog;
    private Button btnAgreePortfolioDisclaimer;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
    	try{
	    	final Context context = this;
	    	
	    	super.onCreate(savedInstanceState);
	    	
	        setContentView(R.layout.portfolio_disclaimer_webview);
	        
			dialog 					= new ProgressDialog(this);
			
			this.title.setText(getResources().getString(R.string.portfolio_disclaimer_module_title));
			this.icon.setImageResource(R.drawable.disclaimer);
					
			portfolioDisclaimerTask= new PortfolioDisclaimerTask(PortfolioDisclaimerActivity.this);
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				portfolioDisclaimerTask.execute();
			
			btnAgreePortfolioDisclaimer	= (Button)findViewById(R.id.btnAgreeDisclaimer);
			btnAgreePortfolioDisclaimer.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					AppConstants.hasDisclaimerShown	= true;
					Intent intent = new Intent();
					intent.setClass(PortfolioDisclaimerActivity.this, PortfolioActivity.class);
					startActivity(intent);
				}		
			});
			
			// Added by Mary@20130110 - Fix NullPointerException
			AlertDialog.Builder builder = new AlertDialog.Builder(PortfolioDisclaimerActivity.this);
			alertDialog	= builder.create();
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
	}
    
    // Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
    public void onResume(){
    	// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
    	try{
    		super.onResume();
    		
			currContext	= PortfolioDisclaimerActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	    // Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
    }
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
    
    public class PortfolioDisclaimerTask extends AsyncTask<String, Void, String>{
    	private Context context;
    	private WebView webView;
    	private AsyncTask<String, Void, String> updateTask;
    	
    	public PortfolioDisclaimerTask(PortfolioDisclaimerActivity activity){
    		this.context			= activity;
    	}
    	/* Commented out by Thinzar@20140827, Replaced by WebView's onProgressChanged method
    	@Override
    	protected void onPreExecute(){
    		super.onPreExecute();
    		
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    		try{
	    		this.updateTask	= this;
				updateTask = this;
				
				PortfolioDisclaimerActivity.this.dialog.setMessage("Loading...");
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! PortfolioDisclaimerActivity.this.isFinishing() )
					PortfolioDisclaimerActivity.this.dialog.show();
				
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
    	}
    	*/
    	@Override
		protected String doInBackground(String... arg0){
			return AppConstants.brokerConfigBean.getStrPortfolioDisclaimerUrl();
		}
		
    	@Override
		protected void onPostExecute(final String strUrl){
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    		try{
	    		webView = (WebView) findViewById(R.id.wvPortfolioDisclaimer);
	    		webView.getSettings().setJavaScriptEnabled(true);
	    		webView.getSettings().setAppCacheEnabled(false); //Added by Thinzar, 20170526
	    		webView.setWebViewClient(new WebViewClient() {    			 
					public boolean shouldOverrideUrlLoading(WebView view, String url) {              
						view.loadUrl(url);
						return true;
					}
	
					public void onPageFinished(WebView view, String url) {
						if( PortfolioDisclaimerActivity.this.dialog != null && PortfolioDisclaimerActivity.this.dialog.isShowing() )
							PortfolioDisclaimerActivity.this.dialog.dismiss();
					}
					
					//Added by Thinzar@20140827, Fixes_Request-20140820, ReqNo.6 - START
					public void onReceivedSslError (WebView view, final SslErrorHandler handler, SslError error) {
						//handler.proceed() ;
						//Added by Thinzar, Fixes_Request-20160101, ReqNo.9 - START
						final AlertDialog.Builder builder = new AlertDialog.Builder(PortfolioDisclaimerActivity.this);
					    builder.setMessage(R.string.notification_error_ssl_cert_invalid);
					    builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
					        @Override
					        public void onClick(DialogInterface dialog, int which) {
					        	handler.proceed();
					        }
					    });
					    builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
					        @Override
					        public void onClick(DialogInterface dialog, int which) {
					            handler.cancel();
					        }
					    });
					    final AlertDialog dialog = builder.create();
					    dialog.show();
					  //Added by Thinzar, Fixes_Request-20160101, ReqNo.9 - END
					}
					//Added by Thinzar@20140827, Fixes_Request-20140820, ReqNo.6 - END
	              
					public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
						//Log.i("[PorfolioDisclaimerActivity]", " Error : " + description);
						PortfolioDisclaimerActivity.this.alertDialog.setTitle("Error");
						PortfolioDisclaimerActivity.this.alertDialog.setMessage(description);
						PortfolioDisclaimerActivity.this.alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								PortfolioDisclaimerActivity.this.finish();
							}
						});
						
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! PortfolioDisclaimerActivity.this.isFinishing() )
							PortfolioDisclaimerActivity.this.alertDialog.show();
					}
	    		});
	    		
	    		//Added by Thinzar@20140827, Fixes_Request-20140820, ReqNo.6 - START
	    		webView.setWebChromeClient(new WebChromeClient(){

	    			@Override
	    			public void onProgressChanged(WebView view, int newProgress) {
	    				//Edited by Thinzar, GoogleCrashReport-20141001, ReqNo.1
	    				if( ! PortfolioDisclaimerActivity.this.isFinishing() ){	
		    				dialog.setMessage("Loading");
		    				dialog.show();
	    				}

						if(newProgress == 100 && !PortfolioDisclaimerActivity.this.isFinishing() &&
								dialog.isShowing())
	    					dialog.dismiss();
	    			}

	    		});
	    		webView.loadUrl(strUrl);
	    		//Added by Thinzar@20140827, Fixes_Request-20140820, ReqNo.6 - END 
	    		
	    		/* Commented out by Thinzar
	    		if( PortfolioDisclaimerActivity.this.dialog != null && PortfolioDisclaimerActivity.this.dialog.isShowing() )
	    			PortfolioDisclaimerActivity.this.dialog.dismiss();
	    		*/
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
    	}
    }
}