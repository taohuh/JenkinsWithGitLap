package com.n2nconnect.android.stock.gcm;

public class StockAlertAESKey {
	private int keyId;
	private String dkey;
	private String dsalt;
	private String div;
	private String ekey;
	private String esalt;
	private String eiv;

	public int getKeyId() {
		return keyId;
	}

	public void setKeyId(int keyId) {
		this.keyId = keyId;
	}

	public String getDkey() {
		return dkey;
	}

	public void setDkey(String dkey) {
		this.dkey = dkey;
	}

	public String getDsalt() {
		return dsalt;
	}

	public void setDsalt(String dsalt) {
		this.dsalt = dsalt;
	}

	public String getDiv() {
		return div;
	}

	public void setDiv(String div) {
		this.div = div;
	}

	public String getEkey() {
		return ekey;
	}

	public void setEkey(String ekey) {
		this.ekey = ekey;
	}

	public String getEsalt() {
		return esalt;
	}

	public void setEsalt(String esalt) {
		this.esalt = esalt;
	}

	public String getEiv() {
		return eiv;
	}

	public void setEiv(String eiv) {
		this.eiv = eiv;
	}

}
