package com.n2nconnect.android.stock.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.IdleThread;
import com.n2nconnect.android.stock.IdleThread.OnIdleListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Time;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ChartActivity extends Activity implements OnDoubleLoginListener, OnRefreshStockListener
//Added by Mary@20130920 - Change_Request-20130424, ReqNo.5
, OnIdleListener
{
	private SharedPreferences sharedPreferences;
	private OrientationEventListener orientationListener;
	private ImageView chartView;
	private AnimationDrawable loadAnimation;
	private TextView oneDayLabel;
	private TextView oneMonthLabel;
	private TextView threeMonthLabel;
	private TextView sixMonthLabel;
	private TextView oneYearLabel;
	private TextView twoYearLabel;
	private TextView noChartFoundView;
	private int chartDuration;
	private String symbolCode;
	/* Mary@20130605 - comment unused variable
	private Handler handler = new Handler();
	*/
	private Bitmap bitmap;
	private AlertDialog alert2;
	private DoubleLoginThread loginThread;
	private Handler mHandler2;
	private int ChartType;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr	= 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - START
	public static IdleThread idleThread;
	public static Context currContext;
	private Handler handlerIdle	= new Handler();
	private AlertDialog alertIdle;
	public static boolean isShowingIdleAlert 	= false;
	// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - END

	//Change_Request-20160722, ReqNo.3
	private WebView stockChartWebView;
	private ProgressDialog dialog;
	
	//Fixes_Request-20161101, ReqNo.3
	private String exchangeFromSymbol;
	private boolean isShowImageChart	= true;
	
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			
			AppConstants.showMenu = false;

			requestWindowFeature(Window.FEATURE_NO_TITLE);                

			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);		//Fixes_Request-20161101, ReqNo6

			setContentView(R.layout.landscape_chart_window);
			
			noChartFoundView 	= (TextView) findViewById(R.id.noChartFoundView);
			stockChartWebView	= (WebView) findViewById(R.id.stockChartWebView);
			dialog				= new ProgressDialog(ChartActivity.this);

			sharedPreferences 	= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
			loginThread 		= ((StockApplication) ChartActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) ChartActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(ChartActivity.this);

			Bundle bundle = this.getIntent().getExtras();
			chartDuration = bundle.getInt(ParamConstants.CHART_DURATION_TAG);
			symbolCode = bundle.getString(ParamConstants.SYMBOLCODE_TAG);
			
			String[] items			= symbolCode.split("\\.");
			exchangeFromSymbol		= items[1];
			
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(ChartActivity.this)
			//.setIcon(android.R.attr.alertDialogStyle)
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,	int id) {
					dialog.dismiss();
					return;
				}
			})
			.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			// Added by Mary@20120814 - avoid NullPointerException
			alert2	= new AlertDialog.Builder(ChartActivity.this).create();
			
			orientationListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_UI) {
				public void onOrientationChanged(int orientation) { 
					if (orientation < 70 || orientation > 290) {
						ChartActivity.this.finish();
					} 
				}
			};

			chartView = (ImageView)ChartActivity.this.findViewById(R.id.chartImageWinView);

			orientationListener.enable();

			oneDayLabel = (TextView) this.findViewById(R.id.oneDayWinLabel);
			oneMonthLabel = (TextView) this.findViewById(R.id.oneMonthWinLabel);
			threeMonthLabel = (TextView) this.findViewById(R.id.threeMonthWinLabel);
			sixMonthLabel = (TextView) this.findViewById(R.id.sixMonthWinLabel);
			oneYearLabel = (TextView) this.findViewById(R.id.oneYearWinLabel);
			twoYearLabel = (TextView) this.findViewById(R.id.twoYearWinLabel);

			oneDayLabel.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - END
					
					noChartFoundView.setVisibility(View.GONE);
					chartView.post(new Starter());

					Map<String, Object> parameters = new HashMap<String, Object>();
					parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);

					oneDayLabel.setBackgroundResource(R.drawable.rounded_edge);
					oneMonthLabel.setBackgroundResource(0);
					threeMonthLabel.setBackgroundResource(0);
					sixMonthLabel.setBackgroundResource(0);
					oneYearLabel.setBackgroundResource(0);
					twoYearLabel.setBackgroundResource(0);
					StockActivity.chartDuration = AppConstants.CHART_DURATION_1_DAY;
					ChartActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_INTRADAY_LANDSCAPE);
					
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				}
			});

			oneMonthLabel.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - END
					
					noChartFoundView.setVisibility(View.GONE);
					chartView.post(new Starter());

					Map<String, Object> parameters = new HashMap<String, Object>();
					parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
					parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_1MONTH);

					oneDayLabel.setBackgroundResource(0);
					oneMonthLabel.setBackgroundResource(R.drawable.rounded_edge);
					threeMonthLabel.setBackgroundResource(0);
					sixMonthLabel.setBackgroundResource(0);
					oneYearLabel.setBackgroundResource(0);
					twoYearLabel.setBackgroundResource(0);
					StockActivity.chartDuration = AppConstants.CHART_DURATION_1_MONTH;
					ChartActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL_LANDSCAPE);

				}
			});

			threeMonthLabel.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - END
					
					noChartFoundView.setVisibility(View.GONE);
					chartView.post(new Starter());

					Map<String, Object> parameters = new HashMap<String, Object>();
					parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
					parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_3MONTHS);

					oneDayLabel.setBackgroundResource(0);
					oneMonthLabel.setBackgroundResource(0);
					threeMonthLabel.setBackgroundResource(R.drawable.rounded_edge);
					sixMonthLabel.setBackgroundResource(0);
					oneYearLabel.setBackgroundResource(0);
					twoYearLabel.setBackgroundResource(0);
					StockActivity.chartDuration = AppConstants.CHART_DURATION_3_MONTHS;
					ChartActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL_LANDSCAPE);

				}
			});

			sixMonthLabel.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - END
					
					noChartFoundView.setVisibility(View.GONE);
					chartView.post(new Starter());

					Map<String, Object> parameters = new HashMap<String, Object>();
					parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
					parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_6MONTHS);

					oneDayLabel.setBackgroundResource(0);
					oneMonthLabel.setBackgroundResource(0);
					threeMonthLabel.setBackgroundResource(0);
					sixMonthLabel.setBackgroundResource(R.drawable.rounded_edge);
					oneYearLabel.setBackgroundResource(0);
					twoYearLabel.setBackgroundResource(0);
					StockActivity.chartDuration = AppConstants.CHART_DURATION_6_MONTHS;
					ChartActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL_LANDSCAPE);

				}
			});

			oneYearLabel.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - END
					
					noChartFoundView.setVisibility(View.GONE);
					chartView.post(new Starter());

					Map<String, Object> parameters = new HashMap<String, Object>();
					parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
					parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_12MONTHS);

					oneDayLabel.setBackgroundResource(0);
					oneMonthLabel.setBackgroundResource(0);
					threeMonthLabel.setBackgroundResource(0);
					sixMonthLabel.setBackgroundResource(0);
					oneYearLabel.setBackgroundResource(R.drawable.rounded_edge);
					twoYearLabel.setBackgroundResource(0);
					StockActivity.chartDuration = AppConstants.CHART_DURATION_1_YEAR;
					ChartActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL_LANDSCAPE);

				}
			});

			twoYearLabel.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - END
					
					noChartFoundView.setVisibility(View.GONE);
					chartView.post(new Starter());

					Map<String, Object> parameters = new HashMap<String, Object>();
					parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
					parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_24MONTHS);

					oneDayLabel.setBackgroundResource(0);
					oneMonthLabel.setBackgroundResource(0);
					threeMonthLabel.setBackgroundResource(0);
					sixMonthLabel.setBackgroundResource(0);
					oneYearLabel.setBackgroundResource(0);
					twoYearLabel.setBackgroundResource(R.drawable.rounded_edge);
					StockActivity.chartDuration = AppConstants.CHART_DURATION_2_YEARS;
					ChartActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL_LANDSCAPE);

				}
			});

			switch(chartDuration){
				case AppConstants.CHART_DURATION_1_DAY :
					oneDayLabel.performClick();
					break;
				case AppConstants.CHART_DURATION_1_MONTH :
					oneMonthLabel.performClick();
					break;
				case AppConstants.CHART_DURATION_3_MONTHS :
					threeMonthLabel.performClick();
					break;
				case AppConstants.CHART_DURATION_6_MONTHS :
					sixMonthLabel.performClick();
					break;
				case AppConstants.CHART_DURATION_1_YEAR :
					oneYearLabel.performClick();
					break;
				case AppConstants.CHART_DURATION_2_YEARS :
					twoYearLabel.performClick();
					break;
				case AppConstants.CHART_DURATION_MKT_SUMM:
					displayMarketSummaryChart();
					break;
			}
			// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private void displayMarketSummaryChart(){

		noChartFoundView.setVisibility(View.GONE);
		chartView.post(new Starter());
		
		LinearLayout titleLayout	= (LinearLayout)findViewById(R.id.layoutChartTitleChoice);
		titleLayout.setVisibility(View.GONE);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
		
		ChartActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_INTRADAY_LANDSCAPE);
	}
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onResume(){
		try{
			super.onResume();
			
			currContext	= ChartActivity.this;
			if(idleThread == null)
				initiateIdleThread();
			else if( idleThread.isPaused() )
				idleThread.resumeRequest(false);
			
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(ChartActivity.this);
				loginThread.resumeRequest();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void onPause(){	
		try{			
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
			
			if(dialog.isShowing())
				dialog.dismiss();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	
	public void onStop() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onStop();
			
			orientationListener.disable();
			
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if(idleThread != null)
				idleThread.stopRequest();
			idleThread = null;
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	
	private void displayChartEvent2(final Map<String, Object> parameters, final int chartType) {
		ChartType = chartType;

		new DisplayChartTask().execute(parameters);

	}
	
	private class DisplayChartTask extends AsyncTask<Map<String, Object>, Void, Bitmap> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		String chartServerType;
		String chartURL	= "";
		
		@Override
		protected void onPreExecute() {
			chartServerType	= AppConstants.brokerConfigBean.getChartType();
		}
		
		protected Bitmap doInBackground(Map<String, Object>... map) {
			Bitmap bitmap = null;

			try {
				//Edited by Thinzar, Change_Request-20160722, ReqNo. 5 (Webkit in Android 4.4 below doesn't support HTML5 charts)
				if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.KITKAT || chartServerType.equalsIgnoreCase(ParamConstants.CHARTTYPE.chartTypeImage) 
						|| SystemUtil.isExchangeSupportModulusChart(exchangeFromSymbol) == false) {
					isShowImageChart	= true;
					String imageUrl = QcHttpConnectUtil.chartImage(map[0], ChartType);

					if (imageUrl == null)
						throw new Exception(ErrorCodeConstants.FAIL_GET_PATH);

					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					while (intRetryCtr < AppConstants.intMaxRetry && imageUrl == null) {
						intRetryCtr++;
						imageUrl = QcHttpConnectUtil.chartImage(map[0], ChartType);
					}
					intRetryCtr = 0;

					// NOTE : No retry perform as there is possible that there
					// are null bitmap loaded
					try {
						bitmap = QcHttpConnectUtil.loadBitmap(imageUrl);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					isShowImageChart	= false;
					chartURL = getInteractiveChartURL(chartServerType, map[0], StockActivity.chartDuration);
				}
			} catch (Exception e) {
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END;

			return bitmap;
		}

		protected void onPostExecute(Bitmap bitmap) {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(loadAnimation != null) 	loadAnimation.stop();
				
				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					alertException.setButton(getResources().getString(R.string.dialog_button_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						}
					);
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! ChartActivity.this.isFinishing() )
						alertException.show();
					
					exception = null;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				if(isShowImageChart){
					if (bitmap != null) {
						chartView.setScaleType(ScaleType.FIT_XY);
						chartView.setImageResource(0);
						chartView.setImageBitmap(bitmap);
					} else {
						chartView.setImageResource(0);
						noChartFoundView.setVisibility(View.VISIBLE);
					}
				}else{

					stockChartWebView.setVisibility(View.VISIBLE);
					chartView.setVisibility(View.GONE);
					
					stockChartWebView.getSettings().setJavaScriptEnabled(true);
					stockChartWebView.setWebViewClient(new WebViewClient() {    			 
							public boolean shouldOverrideUrlLoading(WebView view, final String url) {  
								
					            view.loadUrl(url);
					            return true;
							}
							
			    		});
					
					stockChartWebView.setWebChromeClient(new WebChromeClient(){

		    			@Override
		    			public void onProgressChanged(WebView view, int newProgress) {
		    				if( !ChartActivity.this.isFinishing() && !dialog.isShowing()){	
			    				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
			    				dialog.show();
		    				}
		    				
		    				if(newProgress == 100 && !ChartActivity.this.isFinishing() && dialog.isShowing())
		    					dialog.dismiss();
		    			}
		    		});
		    		
					stockChartWebView.loadUrl(chartURL);
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	class Starter implements Runnable {

		public void run() {
			chartView.setScaleType(ScaleType.CENTER);
			chartView.setImageResource(R.drawable.loading);
			loadAnimation = (AnimationDrawable)chartView.getDrawable();
			loadAnimation.start();      
		}
	}

	class Stopper implements Runnable {

		public void run() {
			loadAnimation.stop();
			chartView.setImageResource(0);
			chartView.setImageBitmap(bitmap);
			chartView.setScaleType(ScaleType.FIT_XY);
		}
	}

	final Handler handler2 = new Handler() {
		public void handleMessage(Message message) {
			boolean isResume = (Boolean) message.obj;
			if(isResume==true){
				ChartActivity.this.sessionTimeout();
			}
		}
	};

	public void sessionTimeout() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			AlertDialog alert = new AlertDialog.Builder(this)
			.setTitle(getResources().getString(R.string.session_timeout_title))
			.setMessage(getResources().getString(R.string.session_timeout_msg))
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();    		
				}
			})
			.create();
	
			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! this.isFinishing() )
				alert.show();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	
	// Added by Mary@20120814 - encapsulate logout execution and enhance cache clean - START
	public void logout(){
		// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
		AppConstants.hasLogout	= true;
		
		/* Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
		if( LoginActivity.doubleLoginThread != null )
			LoginActivity.doubleLoginThread.stopRequest();
		*/
		if(loginThread != null)
			loginThread.stopRequest();
		// Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
		
		DefinitionConstants.stopAllThread();
		
		((StockApplication) ChartActivity.this.getApplication()).setPortfolioThread(null);
		((StockApplication) ChartActivity.this.getApplication()).setRefreshThread(null);
		((StockApplication) ChartActivity.this.getApplication()).setDoubleLoginThread(null);
		((StockApplication) ChartActivity.this.getApplication()).setTradeStatusThread(null);
		((StockApplication) ChartActivity.this.getApplication()).setQCAliveThread(null);
		
		( (StockApplication) ChartActivity.this.getApplication() ).setExchangeInfos(null);
		// Added by Mary@20121228-Fixes_Request-20121102, ReqNo.11
		( (StockApplication) ChartActivity.this.getApplication() ).setCurrencyExchangeInfos(null);
		( (StockApplication) ChartActivity.this.getApplication() ).setNewsItems(null);
		( (StockApplication) ChartActivity.this.getApplication() ).setRememberPin(false);
		// Added by Mary@20121009 - Fixes_Request-20120724, ReqNo.7
		( (StockApplication) ChartActivity.this.getApplication() ).setSkipConfirmation(false);
		( (StockApplication) ChartActivity.this.getApplication() ).setSelectedAccount(null);
		( (StockApplication) ChartActivity.this.getApplication() ).setSelectedExchange(null);
		( (StockApplication) ChartActivity.this.getApplication() ).setSelectedExchangeCode(null);
		( (StockApplication) ChartActivity.this.getApplication() ).setTradeAccounts(null);
		( (StockApplication) ChartActivity.this.getApplication() ).setTradingPin(null);
		
		// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - START
		idleThread			= null;
		isShowingIdleAlert	= false;
		// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - END
		
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
		editor.commit();
		
		// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12
		AppConstants.flushConstants();
		// Added by Mary@20121024 - Change_Request-20120719, ReqNo.12
		AppConstants.brokerConfigBean	= AppConstants.brokerConfigBean.restoreConfigBean();
		
		Intent intent = new Intent();
		intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
		intent.setClass(ChartActivity.this, LoginActivity.class);
		startActivity(intent);   
	}
	// Added by Mary@20120814 - standarized logout execution and enhance cache clean - END
	
	Runnable run2 = new Runnable(){

		@Override
		public void run() {
			/* Mary@20120814 - encapsulate logout execution and enhance cache clean - START
			alert2.dismiss();
			
			((StockApplication)	ChartActivity.this.getApplication()).setPortfolioThread(null);
			((StockApplication) ChartActivity.this.getApplication()).setRefreshThread(null);
			((StockApplication) ChartActivity.this.getApplication()).setDoubleLoginThread(null);
			((StockApplication) ChartActivity.this.getApplication()).setTradeStatusThread(null);
			((StockApplication) ChartActivity.this.getApplication()).setQCAliveThread(null);
			
			mHandler2.removeCallbacks(run2);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
			editor.commit();        		

			Intent intent = new Intent();
			intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
			intent.setClass(ChartActivity.this, LoginActivity.class);
			startActivity(intent);   
			*/
			// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6
			try{
				if( alert2 != null &&  alert2.isShowing() )
					alert2.dismiss();
			
				mHandler2.removeCallbacks(run2);
			// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - END
			
			ChartActivity.this.logout();
			// Mary@20120814 - encapsulate logout execution and enhance cache clean - END
		}

	};
	
	private void doubleLoginMessage(String message) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
	
			mHandler2.postDelayed(run2,5000);
	
			alert2 = new AlertDialog.Builder(this)
			.setTitle(getResources().getString(R.string.session_expired_title))
			.setMessage(message)
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6
					try{
						dialog.dismiss();
					
						mHandler2.removeCallbacks(run2);
					// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - START
					}catch(Exception e){
						e.printStackTrace();
					}
					// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - END
					
					/* Mary@20120814 - encapsulate logout execution and enhance cache clean - START
					((StockApplication)	ChartActivity.this.getApplication()).setPortfolioThread(null);
					((StockApplication) ChartActivity.this.getApplication()).setRefreshThread(null);
					((StockApplication) ChartActivity.this.getApplication()).setDoubleLoginThread(null);
					((StockApplication) ChartActivity.this.getApplication()).setTradeStatusThread(null);
					((StockApplication) ChartActivity.this.getApplication()).setQCAliveThread(null);	
	
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
					editor.commit();        		
	
					Intent intent = new Intent();
					intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
					intent.setClass(ChartActivity.this, LoginActivity.class);
					startActivity(intent);
					*/
					ChartActivity.this.logout();
					// Mary@20120814 - encapsulate logout execution and enhance cache clean - END
				}
			})
			.create();
	
			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! this.isFinishing() )
				alert2.show();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}


	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		// TODO Auto-generated method stub
		
	}
	
	
	// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - START
	public void initiateIdleThread(){
		idleThread = new IdleThread();
		idleThread.start();
		idleThread.setRegisteredListener(ChartActivity.this);
		((StockApplication) ChartActivity.this.getApplication()).setIdleThread(idleThread);
		idleThread.resetExpiredTime();
	}
	
	@Override
	public void IdleEvent(boolean isResume){
		new IdleAsyncTask().execute();
	}
	
	private class IdleAsyncTask extends AsyncTask<String,Void,String>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			idleThread.pauseRequest();
		}

		@Override
		protected String doInBackground(String... params) {
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String response) {
			super.onPostExecute(response);
			
			try{
				if(isShowingIdleAlert){
					handlerIdle.removeCallbacks(runIdle);
					ChartActivity.this.logout();
				}else{
					handlerIdle.postDelayed(runIdle,30000);
					Time timeFrom	= new Time();
					timeFrom.setToNow();
		
					alertIdle = new AlertDialog.Builder(currContext)
						.setTitle(getResources().getString(R.string.session_idle_title))
						.setMessage(getResources().getString(R.string.session_idle_msg) + " " + timeFrom.format("%H:%M:%S"))
						.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								dialog.dismiss();
								isShowingIdleAlert = false;
								handlerIdle.removeCallbacks(runIdle);
								idleThread.resumeRequest(true);
							}
						})
						.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								dialog.dismiss();
								isShowingIdleAlert = false;
								handlerIdle.removeCallbacks(runIdle);
								ChartActivity.this.logout();
							}
						})
						.create();
		
					if( alertIdle != null && ! alertIdle.isShowing() ){
						if( ! ( (Activity)currContext ).isFinishing() )
							alertIdle.show();
						isShowingIdleAlert = true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	Runnable runIdle = new Runnable(){
		@Override
		public void run() {
			try{
				if( alertIdle != null && alertIdle.isShowing() ){
					alertIdle.dismiss();
				}
				
				handlerIdle.removeCallbacks(runIdle);
			}catch(Exception e){
				e.printStackTrace();
			}

			ChartActivity.this.logout();
		}
	};
	// Added by Mary@20130920 - Change_Request-20130424, ReqNo.5 - END
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.3 - START
	private String getInteractiveChartURL(String chartServerType, Map<String, Object> params, int mChartDuration){
		
		String chartURL	= "";
		String symbolCode	= params.get(ParamConstants.SYMBOLCODE_TAG).toString();
		String[] items	= symbolCode.split("\\.");
		String code		= items[0];
		String exchange	= items[1];
		String encryptedTime;

		DefinitionConstants.Debug("getInteractiveChartURL: chartServerType=" + chartServerType + ", mChartDuration=" + mChartDuration + ", symbolCode=" + symbolCode);
			
		exchange = (exchange.endsWith("D"))? exchange.substring(0, exchange.length() - 1) : exchange;
			
		if(chartServerType.equalsIgnoreCase(ParamConstants.CHARTTYPE.chartTypeModulus)){
			//Stock Chart: https://poc.asiaebroker.com/mchart/index.jsp?code=0159&exchg=KL&mode=h&amount=30&color=w
			//Market Summ: https://poc.asiaebroker.com/mchart/index.jsp?code=020000000&exchg=KL&mode=h&color=w&isstock=N&view=m
			chartURL	= AppConstants.brokerConfigBean.getNewChartServer() + "code=" + code + "&exchg=" + exchange + "&color=w&view=m";

			if(chartDuration == AppConstants.CHART_DURATION_MKT_SUMM){ 
				chartURL	+= "&isstock=N&mode=h";
			} else{
			
				if(mChartDuration == AppConstants.CHART_DURATION_1_DAY){
					chartURL 	+= "&mode=d";
	
				} else{
					int numOfDays	= ( (Integer) params.get(ParamConstants.CHART_DURATION_TAG) ).intValue();
	
					chartURL 	+= "&mode=h&amount=" + numOfDays;
				}
			}
			
		} else if(chartServerType.equalsIgnoreCase(ParamConstants.CHARTTYPE.chartTypeTeleTrader)){
			//TODO: add code to return Teletrader chart link
			/*
				encryptedTime = FormatUtil.encryptTime();
				String teletraderChartServerTemp ="https://stgntp2.itradecimb.com/gcCIMB/html5/TTchartTCP.jsp?";
				String url = AppConstants.brokerConfigBean.getInteractiveChartServer()
						+"code="+ "code"+"&Name="+stockName+"&exchg="+exchange+"&mode=d&color=b&lang=en&key="+encryptedTime;
			 */
			/*
			 * stockChartWebView.setVisibility(View.VISIBLE);
						WebSettings ws = stockChartWebView.getSettings();
						EnableHtmlFive enableHtmlFive = new EnableHtmlFive(getApplicationContext());
						enableHtmlFive.EnableHtmlFive(ws);
						stockChartWebView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
						stockChartWebView .getSettings().setJavaScriptEnabled(true);
						stockChartWebView .getSettings().setLoadWithOverviewMode(true);
						stockChartWebView .getSettings().setUseWideViewPort(true);  
						encryptedTime = FormatUtil.encryptTime();
						 String delimiter = "\\.";
						 String [] temp = symbolCode.split(delimiter);

						 String code= temp[0];
						// https://stgntp2.itradecimb.com/gcCIMB/html5/rw.jsp?code=1023&Name=HSC9400UBeCW160929@&exchg=KL&mode=d&color=b&lang=en&key=%01%00%05%06%04%03
						 String test ="https://stgntp2.itradecimb.com/gcCIMB/html5/TTchartTCP.jsp?code="+code+"&Name="+stockName+"&exchg=KL&mode=d&color=b&lang=en&key="+encryptedTime;
						 String url = AppConstants.brokerConfigBean.getInteractiveChartServer()+"code="+ "code"+"&Name="+stockName+"&exchg="+selectedExchangeCode+"&mode=d&color=b&lang=en&key="+encryptedTime;

						 chartView.setVisibility(View.GONE);
						 stockChartWebView.setWebViewClient(new WebViewClient() {
					            public boolean shouldOverrideUrlLoading(WebView view, String url) {
					                view.loadUrl(url);
					                return true;
					            }

					            public void onLoadResource(WebView view, String url) {

					            }

					            public void onPageFinished(WebView view, String url) {

					            }
					            public boolean onConsoleMessage(ConsoleMessage cm) {
					                Log.d("MyApplication", cm.message() + " -- From line "
					                                     + cm.lineNumber() + " of "
					                                     + cm.sourceId() );
					                return true;
					              }
					        });
						 stockChartWebView.loadUrl(test);
			 */
		}

		return chartURL;
	}
	//Added by Thinzar, Change_Request-20160722, ReqNo.3 - END
}