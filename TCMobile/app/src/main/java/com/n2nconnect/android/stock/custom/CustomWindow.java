package com.n2nconnect.android.stock.custom;

import java.io.IOException;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.IdleThread;
import com.n2nconnect.android.stock.IdleThread.OnIdleListener;
import com.n2nconnect.android.stock.InvalidPasswordException;
import com.n2nconnect.android.stock.OTPvalidationThread;
import com.n2nconnect.android.stock.OTPvalidationThread.OtpValidationListener;
import com.n2nconnect.android.stock.OneFAValidationThread.OneFAValidationListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.activity.AnnouncementActivity;
import com.n2nconnect.android.stock.activity.DtiTermsActivity;
import com.n2nconnect.android.stock.activity.ElasticNewsActivity;
import com.n2nconnect.android.stock.activity.ExchangeActivity;
import com.n2nconnect.android.stock.activity.IBillionaireActivity;
import com.n2nconnect.android.stock.activity.IndicesActivity;
import com.n2nconnect.android.stock.activity.LoginActivity;
import com.n2nconnect.android.stock.activity.MarketSummaryActivity;
import com.n2nconnect.android.stock.activity.NewsCategoryActivity;
import com.n2nconnect.android.stock.activity.OrderBookActivity;
import com.n2nconnect.android.stock.activity.PortfolioActivity;
import com.n2nconnect.android.stock.activity.PortfolioDisclaimerActivity;
import com.n2nconnect.android.stock.activity.QuoteActivity;
import com.n2nconnect.android.stock.activity.SearchActivity;
import com.n2nconnect.android.stock.activity.SettingsActivity;
import com.n2nconnect.android.stock.activity.StockAlertActivity;
import com.n2nconnect.android.stock.activity.StockAlertDisclaimerActivity;
import com.n2nconnect.android.stock.activity.TheScreenerActivity;
import com.n2nconnect.android.stock.activity.WatchlistActivity;
import com.n2nconnect.android.stock.custom.ScrollCustomMenu.OnMenuItemSelectedListener;
import com.n2nconnect.android.stock.model.E2EEInfo;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.text.format.Time;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CustomWindow extends Activity implements OnMenuItemSelectedListener, OtpValidationListener, OnIdleListener
	// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14
	, OneFAValidationListener
{
	protected TextView title;
	protected ImageView icon;
	protected ImageButton menuButton;
	// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3
	protected Button Register;
	protected SharedPreferences sharedPreferences;
	/* Mary@20130605 - comment unused variable
	private boolean isBackPressed;
	private boolean isShow;
	*/
	private Handler mHandler = new Handler(); 
	private Handler mHandler2 = new Handler();
	private Handler mHandler3 = new Handler();
	private AlertDialog alert;
	private AlertDialog alert2;
	private ProgressDialog progress;
	private Dialog dialog=null;
	
	//Added by Kw@20130116 - Change_Request20130104, ReqNo.2
	private boolean otp=false;
	/* Mary@20130605 - comment unused variable
	private int intRetryCtr;
	*/
	private int choice;
	
	protected boolean isFromLogin = false;

	public ScrollCustomMenu mainMenu;

	public static final int MENU_HOME 		= 1;
	public static final int MENU_SEARCH 	= 2;
	public static final int MENU_WATCHLIST 	= 3;
	public static final int MENU_MKT_SUMMARY = 4;
	public static final int MENU_ORDER_BOOK = 5;
	public static final int MENU_PORTFOLIO 	= 6;
	public static final int MENU_INDICES 	= 7;
	public static final int MENU_EXCHANGE 	= 8;
	public static final int MENU_NEWS 		= 9;
	public static final int MENU_ANNOUNCEMENT = 10;
	public static final int MENU_SETTINGS 	= 11;
	public static final int MENU_LOGOUT 	= 12;
	
	//diyana add for stock alert
	public static final int MENU_STOCKALERT 	= 13;
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.2
	public static final int MENU_THE_SCREENER 	= 14;
	
	//Added by Thinzar, Change_Request-20170119, ReqNo. 5
	public static final int MENU_DTI			= 15;
	
	//Change_Request-20170601, ReqNo.1
	public static final int MENU_IBILLIONAIRE 	= 16;
	
	// Added by Mary@20130603 - Change_Request-20130424, ReqNo.5 - START
	public static IdleThread idleThread;
	public static Context currContext;
	private Handler handlerIdle	= new Handler();
	private AlertDialog alertIdle;
	public static boolean isShowingIdleAlert 	= false;
	
	// Added by Mary@20130603 - Change_Request-20130424, ReqNo.5 - END
	
	private Dialog passwordDialog,noticeDialog;
	private String selectedExchangeCode;
	private ExchangeInfo selectedExchange;
	private StockApplication application;

	//Change_Request-20170601, ReqNo.10 (2FA Dialog, SMS OTP)
	private TextView tv2FAMobilePhone;
	private Button btnRequestOtp;
	private EditText etChallengeCode;
	private EditText etSmsOtpCode;
	private Button btnOkSms;
	private Button btnCancelSms;
	private TextView tvOtpInterval;
	private ImageButton imgBtnInstruction;
	private CountDownTimer otpCountDownTimer;
	private boolean isTimerRunning = false;

	private static final int VALIDATE_TOKEN_OTP = 1;
	private static final int VALIDATE_SMS_OTP	= 2;
	private static final int REQUEST_SMS_OTP	= 3;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
		try{
			super.onCreate(savedInstanceState);
	
			requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
			setContentView(R.layout.window_title);
			
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.window_title);
			
			title = (TextView) this.findViewById(R.id.title);
			icon  = (ImageView) this.findViewById(R.id.icon);
			sharedPreferences = this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
	
			mainMenu = new ScrollCustomMenu(this, this, getLayoutInflater());
			mainMenu.setHideOnSelect(true);
			loadMenuItems();
	
			menuButton = (ImageButton) this.findViewById(R.id.menuButton);
			menuButton.setImageResource(R.drawable.selector_menu_btn);		//Added by Thinzar, to reset back after DMAActivity
			// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6
			menuButton.setVisibility( AppConstants.hasResetPwd() ? View.GONE : View.VISIBLE);
			
			// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - START
			Register = (Button)findViewById(R.id.btnRegister);
			Register.setText("Submit");
			// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - END
			menuButton.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
					
					CustomWindow.this.doMenu();
				}
	
			});
	
			/* Mary@20130605 - comment unused variable
			isShow = false;
			*/
			
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
			if(! AppConstants.hasMultiLogin ){
				( (LinearLayout)findViewById(R.id.LinearWindowTitle) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBgImg() );
				//menuButton.setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );	//commented out by Thinzar, Change_Request-20160101, ReqNo.3
				//Register.setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );	//commented out by Thinzar, Change_Request-20160101, ReqNo.3
				/* Mary@20121018 - Fixes_Request-20121018, ReqNo.3
				( (ImageView)findViewById(R.id.icon) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntHomeMenuIconBgImg() );
				 */
			}
			
			alert	= new AlertDialog.Builder(CustomWindow.this).create();
			alert2	= new AlertDialog.Builder(CustomWindow.this).create();
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			alertIdle = new AlertDialog.Builder(currContext == null ? CustomWindow.this : currContext).create();
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		/* Mary@20130605 - comment unused variable
		isBackPressed = false;
		*/
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
			
			if(AppConstants.showMenu && !isFromLogin){
				doMenu();
			}
			return true; 
		}
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
			
			/* Mary@20130605 - comment unused variable
			isBackPressed = true;
			*/
			if(mainMenu.isShowing()){
				CustomWindow.this.mainMenu.hide();
			}
		}
		if (keyCode == KeyEvent.KEYCODE_HOME) {
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null)
				idleThread.pauseRequest();
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
		}

		return super.onKeyDown(keyCode, event); 
	} 

	private void loadMenuItems() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			ArrayList<CustomMenuItem> menuItems = new ArrayList<CustomMenuItem>();
			CustomMenuItem menuItem 			= new CustomMenuItem();
			
			//Edited by Thinzar, 20160908, menu buttons order edited based on request by CIMB SG
			
			menuItem.setCaption("Home");
			menuItem.setImageResourceId(R.drawable.menu_home);
			menuItem.setImageResourceIdPressedState(R.drawable.menu_home_white);
			menuItem.setId(MENU_HOME);
			menuItems.add(menuItem);
			
			menuItem = new CustomMenuItem();
			menuItem.setCaption("Search");
			menuItem.setImageResourceId(R.drawable.menu_search);
			menuItem.setImageResourceIdPressedState(R.drawable.menu_search_white);
			menuItem.setId(MENU_SEARCH);
			menuItems.add(menuItem);
			
			menuItem = new CustomMenuItem();
			menuItem.setCaption("Watchlist");
			menuItem.setImageResourceId(R.drawable.menu_watchlist);
			menuItem.setImageResourceIdPressedState(R.drawable.menu_watchlist_white);
			menuItem.setId(MENU_WATCHLIST);
			menuItems.add(menuItem);
			
			//Added by Thinzar, Change_Request-20170119, ReqNo.5
			if(AppConstants.brokerConfigBean.isDtiEnabled()){
				menuItem	= new CustomMenuItem();
				menuItem.setCaption("Ideas");
				menuItem.setImageResourceId(R.drawable.menu_dti);
				menuItem.setImageResourceIdPressedState(R.drawable.menu_dti_white);
				menuItem.setId(MENU_DTI);
				menuItems.add(menuItem);
			}
			
			//Added by Thinzar, Change_Request-20160722, ReqNo.2
			if(AppConstants.brokerConfigBean.isEnableScreener()){
				menuItem = new CustomMenuItem();
				menuItem.setCaption("iScreener");
				menuItem.setImageResourceId(R.drawable.menu_the_screener);
				menuItem.setImageResourceIdPressedState(R.drawable.menu_the_screener_white);
				menuItem.setId(MENU_THE_SCREENER);
				menuItems.add(menuItem);
			}
			
			//Change_Request-20170601, ReqNo.1
			if(AppConstants.brokerConfigBean.isIBillionaireMenuEnabled()){
				menuItem = new CustomMenuItem();
				menuItem.setCaption("iBillionaire");
				menuItem.setImageResourceId(R.drawable.menu_ibillionaire);
				menuItem.setImageResourceIdPressedState(R.drawable.menu_ibillionaire_white);
				menuItem.setId(MENU_IBILLIONAIRE);
				menuItems.add(menuItem);
			}
			
			if( ! AppConstants.noTradeClient && AppConstants.brokerConfigBean.enableTrade() ){
				menuItem = new CustomMenuItem();
				menuItem.setCaption("Order Book");
				menuItem.setImageResourceId(R.drawable.menu_ord_book);
				menuItem.setImageResourceIdPressedState(R.drawable.menu_ord_book_white);
				menuItem.setId(MENU_ORDER_BOOK);
				menuItems.add(menuItem);
				
				menuItem = new CustomMenuItem();
				menuItem.setCaption("Portfolio");
				menuItem.setImageResourceId(R.drawable.menu_portfolio);
				menuItem.setImageResourceIdPressedState(R.drawable.menu_portfolio_white);
				menuItem.setId(MENU_PORTFOLIO);
				menuItems.add(menuItem);
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.3 - END
			
			//Added filter by Thinzar@20140905, Fixes_Request-20140820, ReqNo. 19 [show this menu only if client subscribed to equity account]
			if(AppConstants.hasEquityExchange){
				menuItem = new CustomMenuItem();
				menuItem.setCaption("Mkt Summary");
				menuItem.setImageResourceId(R.drawable.menu_summary);
				menuItem.setImageResourceIdPressedState(R.drawable.menu_summary_white);
				menuItem.setId(MENU_MKT_SUMMARY);
				menuItems.add(menuItem);
				
				menuItem = new CustomMenuItem();
				menuItem.setCaption("Indices");
				menuItem.setImageResourceId(R.drawable.menu_indices);
				menuItem.setImageResourceIdPressedState(R.drawable.menu_indices_white);
				menuItem.setId(MENU_INDICES);
				menuItems.add(menuItem);
			}
			
			menuItem = new CustomMenuItem();
			menuItem.setCaption("Exchange");
			menuItem.setImageResourceId(R.drawable.menu_exchg);
			menuItem.setImageResourceIdPressedState(R.drawable.menu_exchg_white);
			menuItem.setId(MENU_EXCHANGE);
			menuItems.add(menuItem);

			// Added by Diyana, Change_Request_20160624, ReqNo.2 -START
			if (!AppConstants.isAppMultilogin && AppConstants.brokerConfigBean.isEnableStockAlert()) {	//Edited by Thinzar, Change_Request-20160722, ReqNo.6
				menuItem = new CustomMenuItem();
				menuItem.setCaption("S.Alert");
				menuItem.setImageResourceId(R.drawable.menu_stock_alert);
				menuItem.setImageResourceIdPressedState(R.drawable.menu_stock_alert_white);
				menuItem.setId(MENU_STOCKALERT);
				menuItems.add(menuItem);
			}
			// Added by Diyana, Change_Request_20160624, ReqNo.2 -END

			menuItem = new CustomMenuItem();
			menuItem.setCaption("News");
			menuItem.setImageResourceId(R.drawable.menu_news);
			menuItem.setImageResourceIdPressedState(R.drawable.menu_news_white);
			menuItem.setId(MENU_NEWS);
			menuItems.add(menuItem);
			
			if( AppConstants.brokerConfigBean.getIntAnnouncementMenuIconBgImg()!=0){
				menuItem = new CustomMenuItem();
				menuItem.setCaption("Announcement");
				menuItem.setImageResourceId(R.drawable.menu_announcement);
				menuItem.setImageResourceIdPressedState(R.drawable.menu_announcement_white);
				menuItem.setId(MENU_ANNOUNCEMENT);
				menuItems.add(menuItem);
			}
			
			menuItem = new CustomMenuItem();
			menuItem.setCaption("Settings");
			menuItem.setImageResourceId(R.drawable.menu_acc_setting);
			menuItem.setImageResourceIdPressedState(R.drawable.menu_acc_setting_white);
			menuItem.setId(MENU_SETTINGS);
			menuItems.add(menuItem);
			
			menuItem = new CustomMenuItem();
			menuItem.setCaption("Logout");
			menuItem.setImageResourceId(R.drawable.menu_logout);
			menuItem.setImageResourceIdPressedState(R.drawable.menu_logout_white);
			menuItem.setId(MENU_LOGOUT);
			menuItems.add(menuItem);
	
			/* Mary@20120822 - Fixes_Request-20120815, ReqNo.2 - START
			if ( ! mainMenu.isShowing() ){
			*/
			if( mainMenu.isShowing() )
				CustomWindow.this.mainMenu.hide();
			// Mary@20120822 - Fixes_Request-20120815, ReqNo.2 - END
			
			try {
				mainMenu.setMenuItems(menuItems);
			} catch (Exception e) {
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.setTitle("No Menu!");
				alert.setMessage(e.getMessage());
				/* Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				// Added by Mary@20130401 - Fixes_Request-20130314, ReqNo.10
				if(alert != null)
				*/
				if( alert != null && ! this.isFinishing() )
					alert.show();
			}
			/* Mary@20120822 - Fixes_Request-20120815, ReqNo.2
			}
			*/
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	public void doMenu() {	
		if (mainMenu.isShowing()) {
			mainMenu.hide();
		} else {
			// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2
			loadMenuItems();
			this.onMenuShownEvent();
			mainMenu.show(findViewById(R.id.title));
		}
	}

	protected void onMenuShownEvent() {

	}

	/**
	 * For the demo just toast the item selected.
	 */
	@Override
	public void menuItemSelectedEvent(CustomMenuItem selection) {
		// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
		boolean isValidSelection	= false;
		Intent intent	= new Intent();
		choice			= selection.getId();
		switch (selection.getId()) {
			case MENU_HOME: 
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;
				
				intent.setClass(CustomWindow.this, QuoteActivity.class);
				intent.putExtra(ParamConstants.FROM_EXCHANGE_TAG, false);
				startActivity(intent);
				break;
			case MENU_SEARCH:
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;
				
				intent.setClass(CustomWindow.this, SearchActivity.class);
				startActivity(intent);
				break;	
			case MENU_WATCHLIST:
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;
				
				intent.setClass(CustomWindow.this, WatchlistActivity.class);
				startActivity(intent);
				break;
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.3
			case 5:
			*/
			case MENU_MKT_SUMMARY :
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;
				
				intent.setClass(CustomWindow.this, MarketSummaryActivity.class);
				startActivity(intent);
				break;
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.3
			case 4:
			*/
			case MENU_ORDER_BOOK:
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;

				if( !AppConstants.info2FA.hasSuccessVerify2FA() && AppConstants.info2FA.is2FARequired() && !AppConstants.info2FA.isForceUse1FA() ){

					if( AppConstants.info2FA.getDeviceList().isEmpty() && AppConstants.info2FA.getMobilePhone2FA() == null){			//Change_Request-20170601, ReqNo.10
						if( !AppConstants.info2FA.is1FAAllowed() ){
							try{	
								AlertDialog.Builder builder = new AlertDialog.Builder(this);
								builder.setMessage("In order to proceed, you need to have a OneKey Token registered and linked with CIMB Securities. Please contact your trading representative for more information.")
										.setPositiveButton("OK", new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int id) {
												dialog.dismiss();
											}
										});
								AlertDialog alert = builder.create();
								if( alert != null && ! this.isFinishing() )
									alert.show();
							}catch(Exception e){
								e.printStackTrace();
							}
							
							break;
						}
					}else{
						if(StockApplication.getOTPvalidationThread() == null){
							if(AppConstants.hasLogout){
								finish();
							}else{
								((StockApplication) CustomWindow.this.getApplication()).setOTPvalidationThread(new OTPvalidationThread());
							}
						}
						
						StockApplication.getOTPvalidationThread().setRegisteredListener(CustomWindow.this);
						
						showTradePin2FADialog();
						
						break;
					}
				}
				
				intent.setClass(CustomWindow.this, OrderBookActivity.class);
				startActivity(intent);
				// Mary@20131120 - Change_Request-20130711, ReqNo.14 - END
				
				break;	
			case MENU_PORTFOLIO:
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;

				if( !AppConstants.info2FA.hasSuccessVerify2FA() && AppConstants.info2FA.is2FARequired() && !AppConstants.info2FA.isForceUse1FA() ){
					if( AppConstants.info2FA.getDeviceList().isEmpty() && AppConstants.info2FA.getMobilePhone2FA() == null){			//Change_Request-20170601, ReqNo.10
						if( !AppConstants.info2FA.is1FAAllowed() ){
							try{	
								AlertDialog.Builder builder = new AlertDialog.Builder(this);
								builder.setMessage("In order to proceed, you need to have a OneKey Token registered and linked with CIMB Securities. Please contact your trading representative for more information.")
										.setPositiveButton("OK", new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int id) {
												dialog.dismiss();
											}
										});
								AlertDialog alert = builder.create();
								if( alert != null && ! this.isFinishing() )
									alert.show();
							}catch(Exception e){
								e.printStackTrace();
							}
							
							break;
						}
					}else{
						if(StockApplication.getOTPvalidationThread() == null){
							if(AppConstants.hasLogout){
								finish();
							}else{
								((StockApplication) CustomWindow.this.getApplication()).setOTPvalidationThread(new OTPvalidationThread());
							}
						}
						
						StockApplication.getOTPvalidationThread().setRegisteredListener(CustomWindow.this);
						
						showTradePin2FADialog();
						
						break;
					}
				}
				
				if(AppConstants.brokerConfigBean.showPortfolioDisclaimer() && ! AppConstants.hasDisclaimerShown){
					// Added by Mary@20121109 - Fixes_Request-20121023, ReqNo.15
					intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
					intent.setClass(CustomWindow.this, PortfolioDisclaimerActivity.class);
				}else{
					intent.setClass(CustomWindow.this, PortfolioActivity.class);
				}
				startActivity(intent);
				// Mary@20131120 - Change_Request-20130711, ReqNo.14 - END
				break;
			case MENU_INDICES:
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;
				
				intent.setClass(CustomWindow.this, IndicesActivity.class);
				startActivity(intent);
				break;
			case MENU_EXCHANGE:
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;
				
				intent.setClass(CustomWindow.this, ExchangeActivity.class);
				startActivity(intent);
				break;
			case MENU_NEWS: 
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;
				
				//Change_Request-20170601, ReqNo.2
				System.out.println("isEnableElastic:" + AppConstants.brokerConfigBean.isEnableElasticNews());
				if(AppConstants.brokerConfigBean.isEnableElasticNews()){
					intent.setClass(CustomWindow.this, ElasticNewsActivity.class);
					startActivity(intent);
				}else{
					intent.setClass(CustomWindow.this, NewsCategoryActivity.class);
					startActivity(intent);
				}
				break;
				
			case MENU_SETTINGS:
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;
				
				intent.setClass(CustomWindow.this, SettingsActivity.class);
				startActivity(intent);
				break;
			case MENU_ANNOUNCEMENT :
				isValidSelection	= true;
				
				intent.setClass(CustomWindow.this, AnnouncementActivity.class);
				intent.putExtra(ParamConstants.FROM_LOGIN_TAG, false);
				startActivity(intent);
				break;
			case MENU_LOGOUT:
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
				isValidSelection	= true;
				
				/* Mary@20120814 - encapsulate logout execution and enhance cache clean
				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
				editor.commit();
				
				String atpUserParam = sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
				Map parameters = new HashMap();
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
				
				((StockApplication) this.getApplication()).setPortfolioThread(null);
				((StockApplication) this.getApplication()).setRefreshThread(null);
				((StockApplication) this.getApplication()).setDoubleLoginThread(null);
				((StockApplication) this.getApplication()).setTradeStatusThread(null);
				((StockApplication) this.getApplication()).setQCAliveThread(null);
				
				LoginActivity.doubleLoginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				*/
				
				new logout().execute();
				
				/* Mary@20120814 - encapsulate logout execution and enhance cache clean
				AppConstants.mapQCIp.clear();
				
				intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
				intent.setClass(CustomWindow.this, LoginActivity.class);
				startActivity(intent);
				*/
				break;
				
			//Added by Diyana, for stock alert module
			case MENU_STOCKALERT:
				isValidSelection	= true;
				//Change_Request-20170119, ReqNo.12
				SharedPreferences pushInfoPref 	= getSharedPreferences(PreferenceConstants.GCM_PREF_FILE, MODE_PRIVATE);
				boolean hasDeviceRegistered		= pushInfoPref.getBoolean(PreferenceConstants.HAS_DEVICE_REGISTERED, false);
				String pushInfoUsername			= pushInfoPref.getString(PreferenceConstants.GCM_USERNAME, "");
				
				String currentUsername			= sharedPreferences.getString(PreferenceConstants.USER_NAME, "");
				
				if(!currentUsername.equals(pushInfoUsername) || !hasDeviceRegistered){
					showAlertForPushNotiRegistration();
				}else{
				
					//Edited by Thinzar, Change_Request-20160722, ReqNo.7
					if(!AppConstants.hasStockAlertDisclaimerShown && AppConstants.brokerConfigBean.getStkAlertDisclaimer().trim().length() > 0){
						Intent disclaimerIntent = new Intent();
						intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						disclaimerIntent.setClass(CustomWindow.this, StockAlertDisclaimerActivity.class);
						startActivity(disclaimerIntent);
					} else{
						Intent stockAlertIntent = new Intent();
						stockAlertIntent.setClass(CustomWindow.this, StockAlertActivity.class);
						startActivity(stockAlertIntent);
					}
				}
				break;
				
			//Added by Thinzar, Change_Request-20160722, ReqNo.2
			case MENU_THE_SCREENER:
				
				Intent theScreenerIntent	= new Intent();
				theScreenerIntent.setClass(CustomWindow.this, TheScreenerActivity.class);
				theScreenerIntent.putExtra(ParamConstants.INFO_TYPE_TAG, ParamConstants.SCREENER_TYPE_GLOBAL);
				startActivity(theScreenerIntent);
				
				break;
				
			//Added by Thinzar, Change_Request-20170119, ReqNo. 5
			case MENU_DTI:
				Intent disclaimerIntent	= new Intent();
				disclaimerIntent.putExtra(ParamConstants.IS_FROM_DTI_TAG, false);
				disclaimerIntent.setClass(CustomWindow.this, DtiTermsActivity.class);
				startActivity(disclaimerIntent);
				
				break;

			case MENU_IBILLIONAIRE:
				if(AppConstants.brokerConfigBean.isLinkIBWebview()){

					Intent iBIntent = new Intent();
					iBIntent.setClass(CustomWindow.this, IBillionaireActivity.class);
					iBIntent.putExtra(ParamConstants.INFO_TYPE_TAG, ParamConstants.IB_TYPE_GLOBAL);
					startActivity(iBIntent);

				}else {
					final String iBillionairePkgName = AppConstants.iBillionairePkgNameMap.get(getApplicationContext().getPackageName());
					try {
						CustomWindow.this.getPackageManager().getApplicationInfo(iBillionairePkgName, 0);
						showDialogWarnOpenExternalApp();

					} catch (Exception e) {
						showDialogToInstallApp(iBillionairePkgName);

					}
				}
				break;
		}
		
		// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
		if(isValidSelection && idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
	}
	
	private class logout extends AsyncTask<Void,Void,String>{
			
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			//Added by Thinzar, Change_Request-20150901, ReqNo.1
			CustomWindow.this.progress 	= ProgressDialog.show(CustomWindow.this, "", "Logging out...", true);
		}

		@Override
		protected String doInBackground(Void... params) {
			// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.10
			AppConstants.hasDisclaimerShown	= false;
			
			AppConstants.hasStockAlertDisclaimerShown	= false;	//Change_Request-20160722, ReqNo.7
			AppConstants.hasRegisteredStockAlert		= false;	//to check for only once per login
			AppConstants.hasCheckedDti					= false;
			AppConstants.hasDtiTermsShown				= false;
			AppConstants.hasCheckedGCM					= false;
			
			String atpUserParam = sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			
			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			try{
				AtpConnectUtil.tradeLogout(parameters);
			}catch(FailedAuthenicationException e){
				e.printStackTrace();
			}
			
			return null;
		}
		
		// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
		@Override
		protected void onPostExecute(String param){
			if(CustomWindow.this.progress != null && CustomWindow.this.progress.isShowing())
				CustomWindow.this.progress.dismiss();
			
			logout(CustomWindow.this);
		}
		// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	}


	Runnable run = new Runnable(){

		@Override
		public void run() {
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
			try{
				/* Mary@20130401 - Fixes_Request-20130314, ReqNo.10
				// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4
				if( alert.isShowing() )
				*/
				if( alert != null && alert.isShowing() )
					alert.dismiss();
				
				mHandler.removeCallbacks(run);
			// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - END
			
			/* Mary@20120814 - encapsulate logout execution and enhance cache clean - START
			((StockApplication) CustomWindow.this.getApplication()).setPortfolioThread(null);
			((StockApplication) CustomWindow.this.getApplication()).setRefreshThread(null);
			((StockApplication) CustomWindow.this.getApplication()).setDoubleLoginThread(null);
			((StockApplication) CustomWindow.this.getApplication()).setTradeStatusThread(null);
			((StockApplication) CustomWindow.this.getApplication()).setQCAliveThread(null);
			
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
			editor.commit();        		

			Intent intent = new Intent();
			intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
			intent.setClass(CustomWindow.this, LoginActivity.class);
			startActivity(intent);   
			*/
			CustomWindow.this.logout(CustomWindow.this);
			// Mary@20120814 - encapsulate logout execution and enhance cache clean - END
		}
	};
	
	public void showNetworkUnavailableAlert() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("No network available.\n\nPlease enable your network and try again.")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							//    	                LoginActivity.this.finish();
						}
					});
			AlertDialog alert = builder.create();
			/* Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			// Added by Mary@20130401 - Fixes_Request-20130314, ReqNo.10
			if(alert != null)
			*/
			if( alert != null && ! this.isFinishing() )
				alert.show();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	Runnable run2 = new Runnable(){

		@Override
		public void run() {
			// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6
			try{
				/* Mary@20130401 - Fixes_Request-20130314, ReqNo.10
				// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4
				if( alert2.isShowing() )
				*/
				if( alert2 != null && alert2.isShowing() )
					alert2.dismiss();
				
				mHandler2.removeCallbacks(run2);
			// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - END
			
			/*  Mary@20120814 - encapsulate logout execution and enhance cache clean - START
			((StockApplication) CustomWindow.this.getApplication()).setPortfolioThread(null);
			((StockApplication) CustomWindow.this.getApplication()).setRefreshThread(null);
			((StockApplication) CustomWindow.this.getApplication()).setDoubleLoginThread(null);
			((StockApplication) CustomWindow.this.getApplication()).setTradeStatusThread(null);
			((StockApplication) CustomWindow.this.getApplication()).setQCAliveThread(null);
			
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
			editor.commit();        		

			Intent intent = new Intent();
			intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
			intent.setClass(CustomWindow.this, LoginActivity.class);
			startActivity(intent);   
			*/
			CustomWindow.this.logout(CustomWindow.this);
			// Mary@20120814 - encapsulate logout execution and enhance cache clean - END
		}

	};
	
	// Added by Mary@20120814 - encapsulate logout execution and enhance cache clean - START
	public void logout(CustomWindow window){
		// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
		AppConstants.hasLogout	= true;
				
		DefinitionConstants.stopAllThread();
		
		( (StockApplication) window.getApplication() ).setPortfolioThread(null);
		( (StockApplication) window.getApplication() ).setRefreshThread(null);
		( (StockApplication) window.getApplication() ).setDoubleLoginThread(null);
		( (StockApplication) window.getApplication() ).setTradeStatusThread(null);
		( (StockApplication) window.getApplication() ).setQCAliveThread(null);
		// Added by Mary@20130603 - Change_Request-20130424, ReqNo.5
		( (StockApplication) window.getApplication() ).setIdleThread(null);
		
		( (StockApplication) window.getApplication() ).setExchangeInfos(null);
		// Added by Mary@20121228-Fixes_Request-20121102, ReqNo.11
		( (StockApplication) window.getApplication() ).setCurrencyExchangeInfos(null);
		( (StockApplication) window.getApplication() ).setNewsItems(null);
		( (StockApplication) window.getApplication() ).setRememberPin(false);
		// Added by Mary@20121009 - Fixes_Request-20120724, ReqNo.7
		( (StockApplication) window.getApplication() ).setSkipConfirmation(false);
		( (StockApplication) window.getApplication() ).setSelectedAccount(null);
		( (StockApplication) window.getApplication() ).setSelectedExchange(null);
		( (StockApplication) window.getApplication() ).setSelectedExchangeCode(null);
		( (StockApplication) window.getApplication() ).setTradeAccounts(null);
		( (StockApplication) window.getApplication() ).setTradingPin(null);
		
		// Added by Mary@20130603 - Change_Request-20130424, ReqNo.5 - START
		idleThread			= null;
		isShowingIdleAlert	= false;
		// Added by Mary@20130603 - Change_Request-20130424, ReqNo.5 - END
		
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
		editor.commit();
		
		//Added by Thinzar, Change_Request-20160722, ReqNo.8
		editor.remove(PreferenceConstants.APP_LINK_DATA_TAG);
		editor.commit();
		AppConstants.hasWarnOpenExternalApp			= false;

		// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12
		AppConstants.flushConstants();
		// Added by Mary@20121024 - Change_Request-20120719, ReqNo.12
		AppConstants.brokerConfigBean	= AppConstants.brokerConfigBean.restoreConfigBean();

		Intent intent = new Intent();
		intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
		intent.setClass(window, LoginActivity.class);
		startActivity(intent);   
	}
	// Added by Mary@20120814 - encapsulate logout execution and enhance cache clean - END

	public void sessionExpired() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			//final TradeAliveThread aliveThread, final RefreshStockThread refreshThread, final DoubleLoginThread doubleLoginThread
	
			mHandler.postDelayed(run,5000);
	
			alert = new AlertDialog.Builder(this)
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			.setTitle("Session Expired")
			.setMessage("Your session is Expired, press Ok button to logout Or System will automatically Logout in 5 seconds")
			 */
			.setTitle("Session Expired / \nInvalid Encryted Key")
			.setMessage("Your session is Expired / encryted key is Invalid, \npress Ok button to logout Or System will automatically Logout in 5 seconds.")
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
					
					mHandler.removeCallbacks(run);
					
					/* Mary@20120814 - encapsulate logout execution and enhance cache clean - START
					((StockApplication) CustomWindow.this.getApplication()).setPortfolioThread(null);
					((StockApplication) CustomWindow.this.getApplication()).setRefreshThread(null);
					((StockApplication) CustomWindow.this.getApplication()).setDoubleLoginThread(null);
					((StockApplication) CustomWindow.this.getApplication()).setTradeStatusThread(null);
					((StockApplication) CustomWindow.this.getApplication()).setQCAliveThread(null);
					
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
					editor.commit();        
	
					Intent intent = new Intent();
					intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
					intent.setClass(CustomWindow.this, LoginActivity.class);
					startActivity(intent);
					*/
					CustomWindow.this.logout(CustomWindow.this);
					// Mary@20120814 - encapsulate logout execution and enhance cache clean - END
				}
			})
			.create();
	
			/* Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			// Added by Mary@20130401 - Fixes_Request-20130314, ReqNo.10
			if(alert != null)
			*/
			if( alert != null && ! this.isFinishing() )
				alert.show();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	public void doubleLoginMessage(String message) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			mHandler2.postDelayed(run2,5000);
	
			alert2 = new AlertDialog.Builder(this)
			.setTitle("Session Expired")
			.setMessage(message)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
	
					mHandler2.removeCallbacks(run2);
					
					/* Mary@20120814 - encapsulate logout execution and enhance cache clean - START
					((StockApplication) CustomWindow.this.getApplication()).setPortfolioThread(null);
					((StockApplication) CustomWindow.this.getApplication()).setRefreshThread(null);
					((StockApplication) CustomWindow.this.getApplication()).setDoubleLoginThread(null);
					((StockApplication) CustomWindow.this.getApplication()).setTradeStatusThread(null);
					((StockApplication) CustomWindow.this.getApplication()).setQCAliveThread(null);
	
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
					editor.commit();        		
	
					Intent intent = new Intent();
					intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
					intent.setClass(CustomWindow.this, LoginActivity.class);
					startActivity(intent);
					*/
					CustomWindow.this.logout(CustomWindow.this);
					// Mary@20120814 - encapsulate logout execution and enhance cache clean - END
				}
			})
			.create();
	
			/* Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			// Added by Mary@20130401 - Fixes_Request-20130314, ReqNo.10
			if(alert2 != null)
			*/
			if( alert2 != null && ! this.isFinishing() )
				alert2.show();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	
	Runnable run3 = new Runnable(){

		@Override
		public void run() {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				alert = new AlertDialog.Builder(CustomWindow.this)
				.setTitle("Session Expired")
				.setMessage("Your session is Expired, press Ok button to logout Or System will automatically Logout in 5 seconds")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
	
						mHandler.removeCallbacks(run);
						
						/* Mary@20120814 - encapsulate logout execution and enhance cache clean - START
						((StockApplication) CustomWindow.this.getApplication()).setPortfolioThread(null);
						((StockApplication) CustomWindow.this.getApplication()).setRefreshThread(null);
						((StockApplication) CustomWindow.this.getApplication()).setDoubleLoginThread(null);
						((StockApplication) CustomWindow.this.getApplication()).setTradeStatusThread(null);
						((StockApplication) CustomWindow.this.getApplication()).setQCAliveThread(null);
						
						SharedPreferences.Editor editor = sharedPreferences.edit();
						editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
						editor.commit();        
	
						Intent intent = new Intent();
						intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
						intent.setClass(CustomWindow.this, LoginActivity.class);
						startActivity(intent);   
						*/
						CustomWindow.this.logout(CustomWindow.this);
						// Mary@20120814 - encapsulate logout execution and enhance cache clean - END
					}
				})
				.create();
	
				/* Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				// Added by Mary@20130401 - Fixes_Request-20130314, ReqNo.10
				if(alert != null)
				*/
				if( alert != null && ! CustomWindow.this.isFinishing() )
					alert.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	};

	public void sessionTimeout(final RefreshStockThread refreshThread, final String[] symbolCodes) {
		mHandler3.postDelayed(run3,500);
	}
	
	// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14 - START
	private class getE2EEEParam extends AsyncTask<String, Void, Exception> {
		String response 		= null;
		String strInputPassword = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			try{
				CustomWindow.this.progress 	= ProgressDialog.show(CustomWindow.this, "", "Validating Trading Password...", true);
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected Exception doInBackground(String... params){
			try{
				response = AtpConnectUtil.tradeGetE2EEParams();

				if(response == null){
					throw new Exception(ErrorCodeConstants.FAIL_GET_E2EE_PARAM);
				}else{ 
					if( ( response.split("\r\n") ).length < 2 )
						throw new Exception(ErrorCodeConstants.FAIL_GET_VALID_E2EE_PARAM);
						
					String strTemp	= response.split("\r\n")[1];
					strTemp			= strTemp.substring( strTemp.indexOf(DefinitionConstants.RECORD_HEADER) + DefinitionConstants.RECORD_HEADER.length(), strTemp.length() );
					if(strTemp.split("\\|").length == 0)
						throw new Exception(ErrorCodeConstants.FAIL_GET_VALID_E2EE_PARAM);
					
					strInputPassword= params[0];
				}
			}catch (InvalidPasswordException e){
				e.printStackTrace();
				return e;
			}catch (FailedAuthenicationException e){
				e.printStackTrace();

				return new IOException(ErrorCodeConstants.FAIL_AUTHENTICATION_IOE);
			}catch (Exception e){
				e.printStackTrace();
				return e;
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);

			try {
				if (result != null) {
					if(CustomWindow.this.progress != null && CustomWindow.this.progress.isShowing() )
						CustomWindow.this.progress.dismiss();
					
					AlertDialog	alertException = new AlertDialog.Builder(CustomWindow.this)
					//.setIcon(android.R.attr.alertDialogStyle)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
						}
					}).create();
			
					alertException.setMessage( result.getMessage() );
					
					if( ! CustomWindow.this.isFinishing() )
						alertException.show();

					return;
				}

				E2EEInfo e2eeInfo				= AtpMessageParser.parseE2EEParams(response);
				String password 				= FormatUtil.encryptWithE2EE(e2eeInfo, strInputPassword);
				String strE2EERandomNumber 		= e2eeInfo.getStrE2EERandomNumber();

				PublicKey pkE2EERSA 			= FormatUtil.loadRSAPublicKey( e2eeInfo.getStrRSAPublicKey(), e2eeInfo.getStrRSAModulus_Base64(), e2eeInfo.getStrRSAExponent_Base64() );
				String strRSAPassword 			= FormatUtil.encryptWithRSA(pkE2EERSA, strInputPassword);

				Map<String, Object> parameters	= new HashMap<String, Object>();
				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null));
				
				parameters.put( ParamConstants.USERNAME_TAG, sharedPreferences.getString(PreferenceConstants.USER_NAME, null) );
				parameters.put( ParamConstants.E2EE_RANDOM_NUM_TAG, strE2EERandomNumber );
				parameters.put( ParamConstants.PASSWORD_TAG, password );
				parameters.put( ParamConstants.RSA_PASSWORD_TAG, strRSAPassword );
				
				new verifyTradePwd().execute(parameters);
			} catch (Exception e) {	
				e.printStackTrace();
				
				AlertDialog	alertException = new AlertDialog.Builder(CustomWindow.this)
				//.setIcon(android.R.attr.alertDialogStyle)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						dialog.dismiss();
					}
				}).create();
		
				alertException.setMessage( e.getMessage() );
				
				if( ! CustomWindow.this.isFinishing() )
					alertException.show();
				
				return;
			}
		}
	}
	
	public void showTradePin1FADialog(){
		System.out.println("debug==showTradePin1FADialog, choice=" + choice);
		try{
			noticeDialog 	= new Dialog(this);
			noticeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			noticeDialog.setContentView(R.layout.notice_dialog);
			Button registerButton 	= (Button) noticeDialog.findViewById(R.id.registerButton);
			registerButton.setOnClickListener(new OnClickListener(){
				public void onClick(View v) {
					try {
						  Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://portal.assurity.sg/naf-web/public/index.do"));
						  startActivity(myIntent);
						} catch (ActivityNotFoundException e) {
						  Toast.makeText(CustomWindow.this, "No application can handle this request,"
						    + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
						  e.printStackTrace();
						}
					noticeDialog.dismiss();
				}
			});
			Button laterButton 	= (Button) noticeDialog.findViewById(R.id.laterButton);
			laterButton.setOnClickListener(new OnClickListener(){
				public void onClick(View v) {
					noticeDialog.dismiss();
					passwordDialog.show();
				}
			});
			noticeDialog.show();
			
			passwordDialog 	= new Dialog(this);
			passwordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			passwordDialog.setContentView(R.layout.trade_password_dialog);
	
			final EditText pwdField = (EditText) passwordDialog.findViewById(R.id.tradePwdField);
			pwdField.setTransformationMethod(PasswordTransformationMethod.getInstance());
			passwordDialog.setOnShowListener(new OnShowListener() {
				public void onShow(DialogInterface dialog) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(pwdField, InputMethodManager.SHOW_IMPLICIT);
				}
			});
	
			Button cancelButton 	= (Button) passwordDialog.findViewById(R.id.cancelButton);
			cancelButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					choice = 0; 	//Fiexs_Request-20170701, ReqNo.3

					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();

					try{
						passwordDialog.dismiss();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			
			Button okButton 		= (Button) passwordDialog.findViewById(R.id.okButton);
			okButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {

					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					
					new getE2EEEParam().execute( pwdField.getText().toString() );
					passwordDialog.dismiss();
				}
			});

			if( passwordDialog.isShowing() )
				passwordDialog.dismiss();

			/*if( ! this.isFinishing() )
				dialog.show();*/
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private class verifyTradePwd extends AsyncTask<Map<String, Object>,Void,String>{
		Exception exception	= null;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try{
				if( CustomWindow.this.progress != null && ! CustomWindow.this.progress.isShowing() )
					CustomWindow.this.progress 	= ProgressDialog.show(CustomWindow.this, "", "Validating Trading Password...", true);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		@Override
		protected String doInBackground(Map<String, Object>...params) {
			Map<String, Object> parameters	= params[0];
			String response 				= null ;
			
			try{
				response = AtpConnectUtil.ValidateOneFA(parameters);
			}catch(Exception e){
				e.printStackTrace();
				exception = e;
			}
			return response;
		}
		
		@Override
		protected void onPostExecute(String response) {
			// TODO Auto-generated method stub
			super.onPostExecute(response);
			
			try{
				if(CustomWindow.this.progress != null && CustomWindow.this.progress.isShowing() )
					CustomWindow.this.progress.dismiss();
				
				if(exception != null){
					AlertDialog	alertException = new AlertDialog.Builder(CustomWindow.this)
					//.setIcon(android.R.attr.alertDialogStyle)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
						}
					}).create();
		
					alertException.setMessage( exception.getMessage() );
					if( ! CustomWindow.this.isFinishing() )
						alertException.show();
				
					return;
				}
				
				StringTokenizer tokens 	= new StringTokenizer(response, "\r\n");
				String lineItem;
				String[] items = null;
				while( tokens.hasMoreTokens() ){
					 lineItem = tokens.nextToken();
					
					if( lineItem.startsWith(DefinitionConstants.RECORD_HEADER) ){
						lineItem	= lineItem.substring(2, lineItem.length() - 1);
						items		= lineItem.split("\\|");
					}
				}
				
				if(items == null || (items != null && items.length < 2) )
					items	= new String[]{"1", ErrorCodeConstants.ATP_INVALID_RESPONSE};
				
				if(StockApplication.getOneFAValidationThread() != null && StockApplication.getOneFAValidationThread().getRegisteredListener() != null)
					StockApplication.getOneFAValidationThread().getRegisteredListener().OneFAValidationEvent( ( items[0].equals("200") ? true : false ), items[1] );
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void OneFAValidationEvent(boolean isValid, String message) {
		try{
			Intent intent= new Intent();
			if(isValid){
				/* Commented by Thinzar, Fixes_Request-20170701, ReqNo.5
				if(choice == MENU_PORTFOLIO){
					if(AppConstants.brokerConfigBean.showPortfolioDisclaimer() && ! AppConstants.hasDisclaimerShown){
						intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						intent.setClass(CustomWindow.this, PortfolioDisclaimerActivity.class);
					}else{
						intent.setClass(CustomWindow.this, PortfolioActivity.class);
					}
					startActivity(intent);
				}else if(choice == MENU_ORDER_BOOK ){
					intent.setClass(CustomWindow.this, OrderBookActivity.class);
					startActivity(intent);
				}else{
				*/
					OneFAValidationEvent2(isValid, message); 
				//}
			}else{				
				AlertDialog	alertException = new AlertDialog.Builder(this)
					//.setIcon(android.R.attr.alertDialogStyle)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
						}
					}).create();
		
				alertException.setMessage(message);
				if( ! this.isFinishing() )
					alertException.show();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void OneFAValidationEvent2(boolean isValid, String message) {
		// TODO Auto-generated method stub
	}
	// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14 - END
	
	//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 START
	public boolean showTradePin2FADialog(){
		System.out.println("debug==showTradePin2FADialog");

		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			dialog= new Dialog(this);
			//dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setTitle("OTP Pin");
			dialog.setContentView(R.layout.trade_pin2fa_dialog);
			dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			dialog.setCanceledOnTouchOutside(false);	//Change_Request-20170601, ReqNo.10

			//Change_Request-20170601, ReqNo.10
			RadioGroup rdGroupOtp 			= (RadioGroup)dialog.findViewById(R.id.rdgroup_otp);
			final LinearLayout llTokenOtp 	= (LinearLayout)dialog.findViewById(R.id.ll_token_otp);
			final LinearLayout llSmsOtp		= (LinearLayout)dialog.findViewById(R.id.ll_sms_otp);

			rdGroupOtp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
					if(checkedId == R.id.rd_token){
						llTokenOtp.setVisibility(View.VISIBLE);
						llSmsOtp.setVisibility(View.GONE);

					}else if(checkedId == R.id.rd_sms){
						llTokenOtp.setVisibility(View.GONE);
						llSmsOtp.setVisibility(View.VISIBLE);

					}
				}
			});

			//Token OTP Layout
			final EditText pinField2FA = (EditText)dialog.findViewById(R.id.tradePin2fafield);
			pinField2FA.setTransformationMethod(PasswordTransformationMethod.getInstance());
			dialog.setOnShowListener(new OnShowListener() {
				public void onShow(DialogInterface dialog) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(pinField2FA, InputMethodManager.SHOW_IMPLICIT);
				}
			});
			
			final Spinner spinnerDeviceLst 		= (Spinner)dialog.findViewById(R.id.deviceListSpinner);
			ArrayAdapter<String> dataAdapter 	= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, AppConstants.info2FA.getDeviceList());
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerDeviceLst.setAdapter(dataAdapter); 
			
			Button okButton = (Button)dialog.findViewById(R.id.okButton);
			okButton.setOnClickListener(new OnClickListener(){
	
				@Override
				public void onClick(View view) {
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END

					//Change_Request-20170601, ReqNo.10
					String deviceId 	= spinnerDeviceLst.getSelectedItem().toString();
					String otpValue 	= pinField2FA.getText().toString();
					String deviceType	= AppConstants.DEV_TYPE_2FA_TOKEN;

					prepareParamsForValidateOTP(deviceId, otpValue, deviceType, null, null, VALIDATE_TOKEN_OTP);

					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
				}});
			
			Button cancelButton 	= (Button) dialog.findViewById(R.id.cancelButton);
			cancelButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					choice = 0; 	//Fiexs_Request-20170701, ReqNo.3

					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
					
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
				}
			});

			//Fiexs_Request-20170701, ReqNo.3
			dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					choice = 0;
					dialog.dismiss();
				}
			});

			//SMS OTP Layout Change_Request-20170601, ReqNo.10
			tv2FAMobilePhone	= (TextView)dialog.findViewById(R.id.tv_2famobphone);
			btnRequestOtp		= (Button)dialog.findViewById(R.id.btn_request_otp);
			etChallengeCode		= (EditText)dialog.findViewById(R.id.et_challenge_code);
			etSmsOtpCode		= (EditText)dialog.findViewById(R.id.et_sms_otp_code);
			btnOkSms			= (Button)dialog.findViewById(R.id.btn_ok_sms);
			btnCancelSms		= (Button)dialog.findViewById(R.id.btn_cancel_sms);
			tvOtpInterval		= (TextView)dialog.findViewById(R.id.tv_otp_interval);
			imgBtnInstruction	= (ImageButton)dialog.findViewById(R.id.img_btn_instruction);

			String mobilePhone2FA 	= AppConstants.info2FA.getMobilePhone2FA();
			if(mobilePhone2FA != null && mobilePhone2FA.length() > 0){
				mobilePhone2FA		= FormatUtil.maskString(mobilePhone2FA, 4);
			}
			tv2FAMobilePhone.setText(mobilePhone2FA);

			btnRequestOtp.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END

					String deviceId 	= AppConstants.info2FA.getMobilePhone2FA();
					String otpValue 	= "";
					String deviceType 	= AppConstants.DEV_TYPE_2FA_SMS;
					String isTriggerSMS = "Y";
					String challengeCode = null;

					prepareParamsForValidateOTP(deviceId, otpValue, deviceType, isTriggerSMS, challengeCode, REQUEST_SMS_OTP);

					view.setEnabled(false);
				}
			});

			btnOkSms.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END

					String deviceId 	= AppConstants.info2FA.getMobilePhone2FA();
					String otpValue 	= etSmsOtpCode.getText().toString().trim();
					String deviceType 	= AppConstants.DEV_TYPE_2FA_SMS;
					String isTriggerSMS = "N";
					String challengeCode = etChallengeCode.getText().toString();

					if(challengeCode.length() == 0) {
						AlertDialog alertException = prepareAlertDialog(getResources().getString(R.string.warn_request_otp_first));

						if( ! CustomWindow.this.isFinishing() )
							alertException.show();

					}else if(otpValue.length() == 0){
						AlertDialog alertException = prepareAlertDialog(getResources().getString(R.string.warn_empty_otp));

						if( ! CustomWindow.this.isFinishing() )
							alertException.show();

					}else {

						if(otpCountDownTimer != null && isTimerRunning) {
							otpCountDownTimer.cancel();
							tvOtpInterval.setVisibility(View.INVISIBLE);
						}

						if(dialog != null && dialog.isShowing() )
							dialog.dismiss();

						prepareParamsForValidateOTP(deviceId, otpValue, deviceType, isTriggerSMS, challengeCode, VALIDATE_SMS_OTP);
					}
				}
			});

			btnCancelSms.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					choice = 0;

					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();

					if(otpCountDownTimer != null && isTimerRunning) {
						otpCountDownTimer.cancel();
						tvOtpInterval.setVisibility(View.INVISIBLE);
					}

					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
				}
			});

			imgBtnInstruction.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					AlertDialog instructionAlert = prepareAlertDialog(getResources().getString(R.string.otp_instruction));

					if( ! CustomWindow.this.isFinishing())
						instructionAlert.show();
				}
			});

			if(AppConstants.info2FA.getDeviceList().isEmpty()){

				rdGroupOtp.setVisibility(View.GONE);
				llTokenOtp.setVisibility(View.GONE);
				llSmsOtp.setVisibility(View.VISIBLE);
			}

			if(AppConstants.info2FA.getMobilePhone2FA() == null || AppConstants.info2FA.getMobilePhone2FA().isEmpty()){
				rdGroupOtp.setVisibility(View.GONE);

				llSmsOtp.setVisibility(View.GONE);
				llTokenOtp.setVisibility(View.VISIBLE);
			}

			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! this.isFinishing() )
				dialog.show();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END	
			
		return otp;
	}

	private void prepareParamsForValidateOTP(String deviceId, String otpValue, String deviceType,
											String isTriggerSMS, String challengeCode, int validateType){

		Map<String, Object> parameters	= new HashMap<String, Object>();

		if( AppConstants.brokerConfigBean.isEnableEncryption() )
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
			parameters.put(ParamConstants.USERPARAM_TAG, sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null));

		parameters.put(ParamConstants.CLIENTCODE_TAG,((StockApplication) CustomWindow.this.getApplication()).getSelectedAccount().getClientCode());
		parameters.put(ParamConstants.SENDERCODE_TAG,sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null));
		parameters.put(ParamConstants.DEVICE_ID_TAG, deviceId);
		parameters.put(ParamConstants.OTP_VALUE_TAG, otpValue);
		parameters.put(ParamConstants.DEVICE_TYPE_2FA_TAG, deviceType);

		if(isTriggerSMS != null)
			parameters.put(ParamConstants.IS_TRIGGER_SMS_TAG, isTriggerSMS);

		if(challengeCode != null)
			parameters.put(ParamConstants.CHALLENGE_CODE_2FA_TAG, challengeCode);

		AppConstants.selectedDeviceID = deviceId;
		AppConstants.otpValue =  otpValue;

		new ValidateOTP(validateType).execute(parameters);
	}

	private class ValidateOTP extends AsyncTask<Map<String, Object>,Void,String>{

		private int validationType;

        //Change_Request-20170601, ReqNo.10
        private String errorCode;
        private String errorMsg;
        private String challengeCode;

		private ValidateOTP(int mValidationType){
			this.validationType = mValidationType;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			String progressDialogMsg = "";

			if(validationType == VALIDATE_SMS_OTP || validationType == VALIDATE_TOKEN_OTP)
				progressDialogMsg = getResources().getString(R.string.progress_validate_otp);
			else if(validationType == REQUEST_SMS_OTP)
				progressDialogMsg = getResources().getString(R.string.progress_request_otp);

			CustomWindow.this.progress 	= ProgressDialog.show(CustomWindow.this, "", progressDialogMsg, true);
		}

		@Override
		protected String doInBackground(Map<String, Object>... params) {
			Map<String, Object> parameters	= params[0];
			String response 				= null ;
			
			try{
				response = AtpConnectUtil.otpValidate(parameters);
			}catch(Exception e){
				e.printStackTrace();
			}
			return response;
		}
		
		@Override
		protected void onPostExecute(String response) {
			super.onPostExecute(response);
			
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				if(CustomWindow.this.progress != null && CustomWindow.this.progress.isShowing() )
					CustomWindow.this.progress.dismiss();
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
			
			//System.out.println("OTP RESPONSE ASYC:"+response);
			StringTokenizer tokens 	= new StringTokenizer(response, "\r\n");
			String lineItem;
			String[] items = null;
			while (tokens.hasMoreTokens()) {
				 lineItem = tokens.nextToken();
				
				if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {

					lineItem = lineItem.substring(2, lineItem.length() - 1);
					 items = lineItem.split("\\|", -1);	//need to add -1 to avoid skipping empty values

                    //only used when requesting SMS OTP
                    errorCode 		= items[0];
                    errorMsg 		= items[1];
                    if(items.length > 2) challengeCode 	= items[2];	//token OTP only return two params.

				}
			}
			
			otp = items[0].equals("0") ? true : false;

			if(validationType == VALIDATE_SMS_OTP || validationType == VALIDATE_TOKEN_OTP) {
				// Added by Mary@20130703 - GooglePlayReport-20130702, CrashNo.3
				if (StockApplication.getOTPvalidationThread() != null && StockApplication.getOTPvalidationThread().getRegisteredListener() != null)
					StockApplication.getOTPvalidationThread().getRegisteredListener().OtpValidationEvent(otp, items[1]);

			}else if(validationType == REQUEST_SMS_OTP){

				if(errorCode.equals("0")){
					if(challengeCode != null)
						etChallengeCode.setText(challengeCode);
				}else {
					AlertDialog alertException = prepareAlertDialog(errorMsg);

					if( ! CustomWindow.this.isFinishing() )
						alertException.show();
				}
				startCountdownTimer();
			}
		}
	}

	@Override
	public void OtpValidationEvent(boolean isValid,String message) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			Intent intent= new Intent();
			if(isValid){
				/* Mary@20130924 - Change_Request-20130724, ReqNo.14
				AppConstants.info2FA.set2FAEnable(false);
				*/
				AppConstants.info2FA.setHasSuccessVerify2FA(true);
				
				if(choice == MENU_PORTFOLIO){
					if(AppConstants.brokerConfigBean.showPortfolioDisclaimer() && ! AppConstants.hasDisclaimerShown){
						// Added by Mary@20121109 - Fixes_Request-20121023, ReqNo.15
						intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						intent.setClass(CustomWindow.this, PortfolioDisclaimerActivity.class);
					}else{
						intent.setClass(CustomWindow.this, PortfolioActivity.class);
					}
					startActivity(intent);
				}else if(choice == MENU_ORDER_BOOK ){
					intent.setClass(CustomWindow.this, OrderBookActivity.class);
					startActivity(intent);
				}else{
					OtpValidationEvent2(isValid,message); 
				}
			}else{
				AppConstants.selectedDeviceID	= "";
				AppConstants.otpValue 			= "";
				
				AlertDialog	alertException = new AlertDialog.Builder(this)
					//.setIcon(android.R.attr.alertDialogStyle)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
						}
					}).create();
		
				alertException.setMessage(message);
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! this.isFinishing() )
					alertException.show();
			}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 START

	public void OtpValidationEvent2(boolean isValid, String message) {
		// TODO Auto-generated method stub
		
	}
	
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
	public void initiateIdleThread(){
		idleThread = new IdleThread();
		idleThread.start();
		idleThread.setRegisteredListener(CustomWindow.this);
		((StockApplication) CustomWindow.this.getApplication()).setIdleThread(idleThread);
		idleThread.resetExpiredTime();
	}
	
	@Override
	public void IdleEvent(boolean isResume){
		new IdleAsyncTask().execute();
	}
	
	private class IdleAsyncTask extends AsyncTask<String,Void,String>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			idleThread.pauseRequest();
		}

		@Override
		protected String doInBackground(String... params) {
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String response) {
			super.onPostExecute(response);
			
			try{
				if(isShowingIdleAlert){
					handlerIdle.removeCallbacks(runIdle);
					CustomWindow.this.logout(CustomWindow.this);
				}else{
					handlerIdle.postDelayed(runIdle,30000);
					Time timeFrom	= new Time();
					timeFrom.setToNow();
					
					int maxIdleTime	= AppConstants.brokerConfigBean.getMaxIdleTime();	//in miliseconds
					int maxIdleTimeInMinutes	= (int)maxIdleTime/60000;
		
					alertIdle = new AlertDialog.Builder(currContext)
						.setTitle("Idle Session")
						.setMessage("Your application has been idle for more than " + maxIdleTimeInMinutes + " minutes."
									+ "\nPlease click OK to resume or Cancel to logout."
									+ "\nElse, your session will expire in 30 seconds starting from " + timeFrom.format("%H:%M:%S"))
						.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								dialog.dismiss();
								isShowingIdleAlert = false;
								handlerIdle.removeCallbacks(runIdle);
								idleThread.resumeRequest(true);
							}
						})
						.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								dialog.dismiss();
								isShowingIdleAlert = false;
								handlerIdle.removeCallbacks(runIdle);
								CustomWindow.this.logout(CustomWindow.this);
							}
						})
						.create();
		
					if( alertIdle != null && ! alertIdle.isShowing() ){
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ( (Activity)currContext ).isFinishing() )
							alertIdle.show();
						isShowingIdleAlert = true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	Runnable runIdle = new Runnable(){
		@Override
		public void run() {
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
			try{
				if( alertIdle != null && alertIdle.isShowing() ){
					alertIdle.dismiss();
				}

				handlerIdle.removeCallbacks(runIdle);
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END

			CustomWindow.this.logout(CustomWindow.this);
		}
	};
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END

	//Added by Thinzar, Change_Request-20150901, ReqNo.1 - START
	@SuppressWarnings("deprecation")
	public void prepareAlertException(AlertDialog alertException, String errorMsg, boolean isLogout){
		alertException.setMessage(errorMsg);

		if(isLogout){
			alertException.setButton(getResources().getString(R.string.dialog_button_logout), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,	int id) {
					dialog.dismiss();
					AppConstants.hasLogout	= true;
					new logout().execute();
				}
			});
		} else {
			alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,	int id) {
					dialog.dismiss();
					finish();
				}
			});
		}
	}
	//Added by Thinzar, Change_Request-20150901, ReqNo.1 - END

	private void showAlertForPushNotiRegistration(){
		AlertDialog.Builder builder	= new AlertDialog.Builder(CustomWindow.this);
		AlertDialog alert	= builder
				.setMessage(getResources().getString(R.string.alert_msg_enable_push_noti))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int arg1) {
						dialog.dismiss();
						Intent settingsIntent	= new Intent();
						settingsIntent.setClass(CustomWindow.this, SettingsActivity.class);
						startActivity(settingsIntent);
					}
				})
				.create();
		alert.show();
	}

	//Change_Request-20170601, ReqNo.1
	private void showDialogWarnOpenExternalApp(){
		if(!AppConstants.hasWarnOpenExternalApp){
			AlertDialog.Builder builder = new AlertDialog.Builder(CustomWindow.this);
	        builder.setMessage(getResources().getString(R.string.open_external_app_warning));
	        builder.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
	                new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int id) {
	                    	AppConstants.hasWarnOpenExternalApp	= true;

	                    	startIBillionaireIntent();
	                    }
	                });
	        builder.setNegativeButton("Cancel",
	                new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int id) {
	                        dialog.cancel();
	                    }
	                });

	        builder.show();
		}else{
			startIBillionaireIntent();
		}
	}

	private void startIBillionaireIntent(){
		final String iBillionaireAppLink	= AppConstants.iBillionaireAppLinkMap.get(getApplicationContext().getPackageName());
		
		try{
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(iBillionaireAppLink + "://")));
			
		}catch(Exception e){
			e.printStackTrace();
			Toast.makeText(CustomWindow.this, getResources().getString(R.string.feature_not_available) + iBillionaireAppLink, Toast.LENGTH_LONG).show();
		}
	}
	
	private void showDialogToInstallApp(final String packageName){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(CustomWindow.this);
        builder.setTitle(R.string.title_install_app);
        builder.setMessage(R.string.app_not_installed);
        builder.setPositiveButton(getResources().getString(R.string.dialog_button_yes),
                new DialogInterface.OnClickListener() {
        	
                    public void onClick(DialogInterface dialog, int id) {
                    	try {
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
						    
						} catch (android.content.ActivityNotFoundException anfe) {
							
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
						    
						}
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.dialog_button_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        
        builder.show();
	}

	private void startCountdownTimer(){
		otpCountDownTimer = new CountDownTimer(15000, 1000) {

			public void onTick(long millisUntilFinished) {
				isTimerRunning = true;
				tvOtpInterval.setVisibility(View.VISIBLE);
				tvOtpInterval.setText("If you do not receive your OTP via SMS, please try again in: " + millisUntilFinished/1000 + " second(s).");
			}

			public void onFinish() {
				isTimerRunning = false;
				tvOtpInterval.setVisibility(View.INVISIBLE);
				btnRequestOtp.setEnabled(true);
			}
		};

		otpCountDownTimer.start();
	}

	public AlertDialog prepareAlertDialog(String errorMsg){
		AlertDialog	alertException = new AlertDialog.Builder(CustomWindow.this)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						dialog.dismiss();
					}
				}).create();

		alertException.setMessage(errorMsg);

		return alertException;
	}
}