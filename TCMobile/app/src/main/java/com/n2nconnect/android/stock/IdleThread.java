package com.n2nconnect.android.stock;

import android.text.format.Time;


public class IdleThread extends Thread {

	private OnIdleListener registeredListener;
	/* Mary@20130531 - comment unused variable and method
	private String userParam;
	*/
	private volatile boolean stopRequested; 
	private Thread runThread;
	private boolean isPauseThread;
	/* Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
	private long seconds;
	private int waitTime = 3000;
	*/
	private long lngExpiredTime;
	private Time timeCurrent= new Time();
	//private static final int IDLE_PERIOD = 1800000;	//Change_Request-20170119, ReqNo.8, moved out to configuration file
	private boolean isExpired;
	// Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
	
	public OnIdleListener getRegisteredListener() {
		return registeredListener;
	}

	public void setRegisteredListener(OnIdleListener registeredListener) {
		this.registeredListener = registeredListener;
	}

	/* Mary@20130531 - comment unused variable and method
	public String getUserParam() {
		return userParam;
	}

	public void setUserParam(String userParam) {
		this.userParam = userParam;
	}
	*/

	public interface OnIdleListener {
		public void IdleEvent(boolean isResume);
	}

	public IdleThread() {
		this.isPauseThread	= false;
		this.stopRequested	= false;
		
		// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
		this.lngExpiredTime	= -1;
		this.isExpired		= false;
	}

	public void run(){

		while(!stopRequested){

			synchronized (this) {
				while (isPauseThread) {
					try{
						wait();
					}catch (InterruptedException e) { }
				}
			}
			
			try {
				/* Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
				Thread.sleep(5000);
				
				if(seconds == 15000){
					Thread.sleep(waitTime);
				*/
				timeCurrent.setToNow();
				if( lngExpiredTime > 0 && timeCurrent.toMillis(false) >= lngExpiredTime ){
				// Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
					if(registeredListener != null){
						registeredListener.IdleEvent(false);
						setIsExpired(true);
					}else{
						Thread.sleep(5000);
					}
				}else{
					Thread.sleep(5000);
				}
				
				/* Mary@20130531 - Change_Request-20130424, ReqNo.5
				Thread.sleep(5000);
				seconds = seconds + 1000;
				*/
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void stopRequest() {
		stopRequested = true;

		if (runThread != null ) {
			runThread.interrupt();
		}
		
		// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5
		this.lngExpiredTime	= -1;
	}

	public void pauseRequest() {
		synchronized (this){
			/* Mary@20130531 - Change_Request-20130424, ReqNo.5
			seconds = 0;
			*/
			isPauseThread = true;
		}
	}

	public void resumeRequest(boolean isResetExpiredTime) {
		synchronized (this){
			isPauseThread = false;
			
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
			if(isResetExpiredTime)
				resetExpiredTime();
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
			
			notify();
		}
	}
	
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START	
	public boolean isPaused(){
		return this.isPauseThread;
	}
	
	public void resetExpiredTime(){
		//Change_Request-20170119, ReqNo.8
		int IDLE_PERIOD	= AppConstants.brokerConfigBean.getMaxIdleTime();
		
		Time timeCurrent= new Time();
		timeCurrent.setToNow();
		
		lngExpiredTime	= timeCurrent.toMillis(false) + IDLE_PERIOD;
		setIsExpired(false);
	}
	
	public boolean isExpired(){
		return this.isExpired;
	}
	
	public void setIsExpired(boolean isExpired){
		this.isExpired = isExpired;
	}
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
}
