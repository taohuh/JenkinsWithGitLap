package com.n2nconnect.android.stock.activity;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.InvalidPasswordException;
import com.n2nconnect.android.stock.OTPvalidationThread;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QCAliveThread;
import com.n2nconnect.android.stock.QCAliveThread.OnQCAliveListener;
import com.n2nconnect.android.stock.QCBean;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.lms.LMSServiceBean;
import com.n2nconnect.android.stock.lms.LMSSubscription;
import com.n2nconnect.android.stock.model.BrokerPlistConfigBean;
import com.n2nconnect.android.stock.model.CurrencyExchangeInfo;
import com.n2nconnect.android.stock.model.E2EEInfo;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.PaymentConfig;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.touchid.FingerprintDecryptDialogFragment;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.PlistFileUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class LoginActivity extends Activity implements OnRefreshStockListener, OnDoubleLoginListener, OnQCAliveListener
	/* Mary@20130829 - ChangeRequest-20120719, ReqNo.4
	, OnItemSelectedListener
	*/ 
{
	private static final String TAG = "LoginActivity";
	
	protected SharedPreferences sharedPreferences;
	//private Dialog splashDialog;		//comment unused variable
	/* Mary@20130527 - comment unused variable
	private Dialog DMADialog;
	*/
	private Map<String, ExchangeInfo> exchangeSectors;
	/* Mary@20130527 - comment unused variable
	private LMSServiceBean oService = new LMSServiceBean();
	*/
	private List<ExchangeInfo> validExchanges;
	// Added by Mary@20121228-Fixes_Request-20121106, ReqNo.11
	private List<CurrencyExchangeInfo> currencyExchangeInfos;
	private String senderCode;
	private String brokerCode;
	private String username;
	private String password;
	// Added by Mary@20130528 - Change_Request-20130424, ReqNo.4
	private boolean isRememberMe;
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
	private String strRSAPassword;
	private String strE2EERandomNumber;
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
	private StringBuffer paramBuffer;
	private String results;
	private String userParam;
	private List<ExchangeInfo> exchangeInfos;
	public static DoubleLoginThread doubleLoginThread;
	public static RefreshStockThread refreshThread;
	public static QCAliveThread QCAlive;
	private ProgressDialog dialog;
	private String encryptedKey = null;
	private String userParamKey = null;
	private byte[] userParamKeyInByte = null;
	protected SharedPreferences.Editor editor;
	
	private List validQCCodes = null;
	private List validTradeCodes = null;
	private List supportedTradeExchange = null;
	
	private List<TradeAccount> tradeAccounts;

	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	private int intRetryCtr = 0;

	private EditText nameField;
	private EditText passwordField;
	private TextView loginButton;
	private Button announcementButton;
	// Added by Mary@20130528 - Change_Request-20130424, ReqNo.4
	private CheckBox cbRememberMe;
	// Added by Mary@20130828 - ChangeRequest-20120719, ReqNo.4
	//private Button btnBrokerHouse;
	
	// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - START
	private TextView forgetPasswordLink;
	private TextView registerLink;
	private TextView termLink;
	private TextView secondaryTermLink;		//Added by Thinzar, Change_Request-20160722, ReqNo. 6
	private TextView emailLink; 			//Change_Request-20170601, ReqNo.16
	private LinearLayout callLink; 				//Change_Request-20170601, ReqNo.16
	private LinearLayout llTopSpace;
	// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - END
	
	//Thinzar@20140701 - Change_Request-20140701,ReqNo.1 - START
	public static String PACKAGE_NAME;
	public static String appDataPath;
	public static String plistPath;
	public static String externalImgPath;
	//Thinzar@20140701 - Change_Request-20140701,ReqNo.1 - END
	
	//Added by Thinzar@20141111, Change_Request-20141101, ReqNo. 3
	public static String brokerId	= null;
	public static String selectedBrokerName	= "";
	
	//Added by Thinzar@20141229 - changed layout to accomodate higher resolution button
	private LinearLayout btnBrokerHouse;
	//private ImageView imgSelectedBroker;	//commented out for TCMobile 2.0 design
	private TextView txtSelectedBrokerName;
	private ImageView imgLoginBottomText;
	
	//Change_Request-20170601, ReqNo.6
	private TextView importantNoteLink;

	//Added by Thinzar, Change_Request-20160722, ReqNo.9
	private List<View> mView;
	private LayoutInflater inflater;
	private View layout0 	= null;
	private View layout1 	= null;
	private int currentPage	= 1;

	private MyPagerAdapter adapter;
	private ViewPager myPager;

	private LinearLayout loginTitleLBarLayout;
	private LinearLayout normalLoginTitleLayout;
	private LinearLayout touchIDLoginTitleLayout;
	private TextView txtLoginPagerTitle;
	private TextView txtNew;
	private TextView txtBlank;
	private TextView txtTouchIDPagerTitle;
	private View normalLoginPagerUnderline;
	private View touchIDPagerUnderline;

	private TextView txt_touchid_terms;
	private Button btnActivateTouchID;
	private TextView txt_touchid_info;
	private TextView touchid_title;
	private ImageButton btn_fingerprint;

	private static final int NORMAL_LOGIN_POS	= 0;
	private static final int TOUCHID_LOGIN_POS	= 1;

	private boolean isTouchIDActivated	= false;
	private int currentPagerId	= -1;

	private KeyStore mKeyStore;
	private KeyGenerator mKeyGenerator;
	private Cipher mDecryptCipher;
	private FingerprintManagerCompat mFingerprintManager;
	private KeyguardManager mKeyguardManager;

	private static final String DIALOG_FRAGMENT_TAG = "myFragment";
	private String loginType	= null;

	//Added by Thinzar, Change_Request-20160722, ReqNo.1
	private final int QR_CODE_LOGIN_REQUEST_ID	= 9001;
	private final int QR_LOGIN_AUTH = 1;
	private final int FINGERPRINT_LOGIN_AUTH = 2;
	private int authType = FINGERPRINT_LOGIN_AUTH;	//To differentiate between fingerprint login and QR

	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130319 - Fixes_Request-20130319, ReqNo.2
		try {
			super.onCreate(savedInstanceState);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			
			dialog									= new ProgressDialog(this);		
			
			AppConstants.alAllSectorCode.clear();
			AppConstants.alAllSectorCode.add("10");
			AppConstants.alAllSectorCode.add("10000");
			
			sharedPreferences 						= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
			SharedPreferences.Editor editor 		= sharedPreferences.edit();
			editor.remove(PreferenceConstants.ATP_USER_PARAM);
			/*
			 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			 * editor.remove(PreferenceConstants.QC_USER_PARAM);
			 */
			editor.remove(PreferenceConstants.ATP_PARAM_STRING);
			/*
			 * Mary@20120814 - Change_Request-20120719, ReqNo.4
			 * editor.remove(PreferenceConstants.EXCHANGE_CODE);
			 * editor.remove(PreferenceConstants.ACCOUNT_NO);
			 * editor.remove(PreferenceConstants.BRANCH_CODE);
			 */
			editor.commit();
			
			//Added by Thinzar@20140701 - Change_Request-20140701, ReqNo.1 - START 
			appDataPath = this.getFilesDir().getPath(); // data/data/PACKAGE_NAME/files
			plistPath = appDataPath + "/" + PlistFileUtil.PLIST_FOLDER_NAME;
			externalImgPath = appDataPath + "/" + PlistFileUtil.ICONS_FOLDER_NAME;
			//Added by Thinzar@20140701 - Change_Request-20140701, ReqNo.1 - END
	
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
			Map<String, String> mapBrokerUserPreference	= new HashMap<String, String>();
			mapBrokerUserPreference.put(PreferenceConstants.EXCHANGE_CODE, null);
			mapBrokerUserPreference.put(PreferenceConstants.ACCOUNT_NO, null);
			mapBrokerUserPreference.put(PreferenceConstants.BRANCH_CODE, null);
			// Added by Mary@20121112 - Change_Request-20120719, ReqNo.12
			mapBrokerUserPreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, null);
			// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
			mapBrokerUserPreference.put( PreferenceConstants.TRADE_EXCHANGE_CODE, null );
			AppConstants.cacheBrokerUserDetails(LoginActivity.this, sharedPreferences, mapBrokerUserPreference);
			mapBrokerUserPreference.clear();
			mapBrokerUserPreference						= null;
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	
			/* Commented out by Thinzar, Change_Request-20141101, ReqNo.1
			this.showLoginScreen();
	
			// The intent flag isDisabled is to tell not to display splash screen.
			boolean isDisabled 							= false;
			Bundle bundle 								= this.getIntent().getExtras();
			if(bundle != null)
				isDisabled = bundle.getBoolean(ParamConstants.REVERT_LOGIN_TAG, false);
	
			if( ! isDisabled )
				this.showSplashScreen();
			*/
		// Added by Mary@20130319 - Fixes_Request-20130319, ReqNo.3 - START
		}catch(Exception e){
			e.printStackTrace();
		}    
		
		//debug for iBillionaire START
		/*
		String textToEncrypt1	= "ibillionaire/verify/123456xxyy87kzmy12098";
		String textToEncrypt2	= "ibillionaire/verify/987654321qwertyuiop22";
		String textToEncrypt3	= "ibillionaire/verify/";
		
		try {
			String encryptedText1		= Base64.encodeToString(TheScreenerActivity.encrypt(AppConstants.iBillionaireSecretkey, textToEncrypt1), Base64.NO_WRAP);
			String encryptedText2		= Base64.encodeToString(TheScreenerActivity.encrypt(AppConstants.iBillionaireSecretkey, textToEncrypt2), Base64.NO_WRAP);
			String encryptedText3		= Base64.encodeToString(TheScreenerActivity.encrypt(AppConstants.iBillionaireSecretkey, textToEncrypt3), Base64.NO_WRAP);
			
			System.out.println("iBillionaire URLEncoded link for itradeappsg://" + textToEncrypt1 + ": " + "itradeappsg://" + URLEncoder.encode(encryptedText1, AppConstants.STRING_ENCODING_FORMAT));
			System.out.println("iBillionaire URLEncoded link for itradeappsg://" + textToEncrypt2 + ": " + "itradeappsg://" + URLEncoder.encode(encryptedText2, AppConstants.STRING_ENCODING_FORMAT));
			System.out.println("iBillionaire URLEncoded link for itradeappsg://" + textToEncrypt3 + ": " + "itradeappsg://" + URLEncoder.encode(encryptedText3, AppConstants.STRING_ENCODING_FORMAT));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		//debug END
	}
	
	public void onResume() {

		// Added by Mary@20130319 - Fixes_Request-20130319, ReqNo.2
		try {
			super.onResume();
			
			//Added by Thinzar@20140702 - to move config files to server
			if(this.getIntent() != null){
				Bundle bundle = this.getIntent().getExtras();
				
				PACKAGE_NAME = bundle.getString(ParamConstants.PARAM_PACKAGE_NAME);
				
				if(bundle.containsKey(ParamConstants.BROKER_ID_TAG)){
					brokerId	= bundle.getString(ParamConstants.BROKER_ID_TAG);
				}
				
				if(bundle.containsKey(ParamConstants.BROKER_NAME_TAG)){
					selectedBrokerName	= bundle.getString(ParamConstants.BROKER_NAME_TAG);
				}
				
			} else{
				PACKAGE_NAME	= AppConstants.CURRENT_PKG_NAME;
			}
			
			//Change_Request-20170119, ReqNo.9 - START
			if(brokerId == null){
				brokerId	= AppConstants.brokerConfigBean.getBrokerCode();
			}
			//Change_Request-20170119, ReqNo.9 - END
			DefinitionConstants.Debug("debug: LoginActivity onResume(), pkg: " + PACKAGE_NAME + ", brokerId=" + brokerId);
			
			SharedPreferences.Editor editor 		= sharedPreferences.edit();
			editor.putString(PreferenceConstants.CURRENT_PACKAGE_NAME, PACKAGE_NAME);
			editor.commit();
			/* Thinzar@20140701 END */
	
			boolean isAppExit = sharedPreferences.getBoolean(PreferenceConstants.IS_APP_EXIT, true);
			if (!isAppExit) {
				StockApplication stockApplication = (StockApplication) this.getApplication();
				if (stockApplication.getExchangeInfos() == null) {
					editor = sharedPreferences.edit();
					editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
					editor.commit();
					return;
				}
				Intent intent = new Intent();
				intent.putExtra(ParamConstants.FROM_LOGIN_TAG, true);
				
				intent.setClass(this, QuoteActivity.class);
				this.startActivity(intent);
			} else {
				
				if(PACKAGE_NAME == null){
					PACKAGE_NAME	= sharedPreferences.getString(PreferenceConstants.LAST_USED_PKG, null);
				}
						
				//this.prepareBrokerConfig(PACKAGE_NAME);
				//this.showLoginScreen();
				prepareBrokerConfig(PACKAGE_NAME);
			}

			//Added by Thinzar, Change_Request-20160722, ReqNo.9 - START
			setContentView(R.layout.login_screen);

			loginTitleLBarLayout		= (LinearLayout)findViewById(R.id.loginTitleLBarLayout);
			normalLoginTitleLayout		= (LinearLayout)findViewById(R.id.normalLoginTitleLayout);
			touchIDLoginTitleLayout		= (LinearLayout)findViewById(R.id.touchIDLoginTitleLayout);
			txtLoginPagerTitle			= (TextView)findViewById(R.id.txtLoginPagerTitle);
			txtNew						= (TextView)findViewById(R.id.txtNew);
			txtBlank					= (TextView)findViewById(R.id.txtBlank);
			txtTouchIDPagerTitle		= (TextView)findViewById(R.id.txtTouchIDPagerTitle);
			normalLoginPagerUnderline	= (View)findViewById(R.id.normalLoginPagerUnderline);
			touchIDPagerUnderline		= (View)findViewById(R.id.touchIDPagerUnderline);

			isTouchIDActivated		= sharedPreferences.getBoolean(PreferenceConstants.IS_PRE_LOGIN_VALIDATED, false);
			togglePagerTitles();

			inflater        		= LayoutInflater.from(getApplicationContext());
			mView 					= new ArrayList<View>();
			myPager					= (ViewPager) findViewById(R.id.login_viewpager);

			View layout1 			= inflater.inflate(R.layout.pager_login, null);
			View layout2 			= inflater.inflate(R.layout.pager_touchid, null);
			mView.add(layout1);
			mView.add(layout2);

			mKeyguardManager 	= (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
			//mFingerprintManager = (FingerprintManagerCompat) getSystemService(MainActivity.this);	//need minSDK 23
			mFingerprintManager = FingerprintManagerCompat.from(getApplicationContext());

			adapter					= new MyPagerAdapter();
			if(AppConstants.brokerConfigBean.isTouchIDEnabled()
					&& mFingerprintManager.isHardwareDetected()){
				adapter.setPageCount(2);
			} else{
				loginTitleLBarLayout.setVisibility(View.GONE);
				adapter.setPageCount(1);
			}
			myPager.setAdapter(adapter);

			if(isTouchIDActivated){
				//only for first load
				myPager.setCurrentItem(TOUCHID_LOGIN_POS, true);
				preparePagerUnderline(TOUCHID_LOGIN_POS);
			}

			myPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){

				@Override
				public void onPageScrollStateChanged(int position) {

				}

				@Override
				public void onPageScrolled(int position, float arg1, int arg2) {

				}

				@Override
				public void onPageSelected(int position) {
					DefinitionConstants.Debug("debug==onPageSelected" + position);

					currentPagerId	= position;
					preparePagerUnderline(position);

					//hide keyboard
					if(position == TOUCHID_LOGIN_POS){
						InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
						inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
					}
				}
			});

			normalLoginTitleLayout.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					myPager.setCurrentItem(NORMAL_LOGIN_POS, true);
				}

			});

			touchIDLoginTitleLayout.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					myPager.setCurrentItem(TOUCHID_LOGIN_POS, true);
				}

			});

			showChangeLog();
			prepareForgetPasswordLink();
			//Added by Thinzar, Change_Request-20160722, ReqNo.9 - END

		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
		try{
			super.onPause();
			
			if(refreshThread != null)
				refreshThread.pauseRequest();
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
	}

	/* Mary@20130829 - ChangeRequest-20120719, ReqNo.4
	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int intPosition, long lngId) {
		// Added by Mary@20130726 - Fixes_Request-20130711, ReqNo.10
		com.n2nconnect.android.stock.model.BrokerConfigBean prevBrokerConfigBean	=  AppConstants.brokerConfigBean;
		
		switch (intPosition) {
			case 0:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.AffinConfigBean();
				break;
			case 1:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.AmSecConfigBean();
				break;
			case 2:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.ApexConfigBean();
				break;
			case 3:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.CimbConfigBean();
				break;
			case 4:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.CimbSGConfigBean();
				break;
			case 5:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.HDCapitalConfigBean();
				break;
			case 6:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.HwangDBConfigBean();
				break;
			case 7:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.InterpacConfigBean();
				break;
			case 8:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.MnAConfigBean();
				break;
			case 9:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.N2NConfigBean();
				break;
			case 10:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.PMSecConfigBean();
				break;
			case 11:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.SJConfigBean();
				break;
			case 12:
				AppConstants.brokerConfigBean = new com.n2nconnect.android.stock.model.TAConfigBean();
				break;
			default:
				AppConstants.brokerConfigBean = null;
				break;
		}

	/* Mary@20130726 - Fixes_Request-20130711, ReqNo.10 - START
		executeBrokerConfig();
	/
		if( AppConstants.brokerConfigBean != null){
			boolean hasChangeATP	= ( prevBrokerConfigBean == null 
										|| prevBrokerConfigBean.getStrAtpIP() == null 
										|| ( prevBrokerConfigBean.getStrAtpIP() != null 
												&&  prevBrokerConfigBean.getStrAtpIP().equalsIgnoreCase( AppConstants.brokerConfigBean.getStrAtpIP() ) 
											) 
									   ) 
									   ? false : true;
			executeBrokerConfig(hasChangeATP);
		}
	// Mary@20130726 - Fixes_Request-20130711, ReqNo.10 - END
	}
	*/

	/* Mary@20130726 - Fixes_Request-20130711, ReqNo.10
	public void executeBrokerConfig(){
	*/
	public void executeBrokerConfig(boolean hasChangeATP){
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			if (this.dialog == null)
				this.dialog = new ProgressDialog(this);
	
			if (this.dialog.isShowing() )
				this.dialog.dismiss();
	
			this.dialog.setMessage("Load Configuration... ");
			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! this.isFinishing() )
				this.dialog.show();
			
			// Added by Mary@20130726 - Fixes_Request-20130711, ReqNo.10 - START
			if(hasChangeATP){
				AppConstants.setEncryptedKey(null);
				AppConstants.userParamInByte	= null;
				AppConstants.userParam			= null;
				
			}
			// Added by Mary@20130726 - Fixes_Request-20130711, ReqNo.10 - END
			
			repaintUI();
	
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			if(this.dialog != null && this.dialog.isShowing() )
				this.dialog.dismiss();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		//Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	
	//Added by Thinzar@20140702 - not to disturb original executeBrokerConfig
	public void prepareBrokerConfig(String mPackageName) {
		Log.d(TAG, "preparing broker config: " + mPackageName);
		SharedPreferences sharedPreferences 	= getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		SharedPreferences.Editor editor 		= sharedPreferences.edit();
		editor.putString(PreferenceConstants.CURRENT_PACKAGE_NAME, mPackageName);
		editor.commit();
		
		//TODO furthur research what does the following do, i'm just following executeBrokerConfig
		AppConstants.setEncryptedKey(null);
		AppConstants.userParamInByte	= null;
		AppConstants.userParam			= null;
		
		try{
			if (this.dialog == null)
				this.dialog = new ProgressDialog(this);
	
			if (this.dialog.isShowing() )
				this.dialog.dismiss();
	
			this.dialog.setMessage(getResources().getString(R.string.login_initialize));

			if( ! this.isFinishing() )
				this.dialog.show();
			
			if (AppConstants.brokerConfigBean != null) {
				AppConstants.brokerConfigBean = null;
			} 
			
			AppConstants.brokerConfigBean = BrokerPlistConfigBean.class.newInstance();
			AppConstants.brokerConfigBean.initialize(mPackageName, this.getApplicationContext());
			//System.out.println(ToStringBuilder.reflectionToString(AppConstants.brokerConfigBean,ToStringStyle.MULTI_LINE_STYLE));
			if(this.dialog != null && this.dialog.isShowing() )
			this.dialog.dismiss();
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//repaintUI();
	}

	public void repaintUI() {
		
		registerLink.setVisibility(View.GONE);
		termLink.setVisibility(View.GONE);
		forgetPasswordLink.setVisibility(View.GONE);
		secondaryTermLink.setVisibility(View.GONE);
		cbRememberMe.setVisibility(View.GONE);
		importantNoteLink.setVisibility(View.GONE);
		
		cbRememberMe.setChecked(false);
		passwordField.setText("");
		
		//Added by Thinzar@20140707
		if(sharedPreferences.getBoolean(PreferenceConstants.HAS_MULTI_LOGIN, false))	btnBrokerHouse.setVisibility(View.VISIBLE);
		else btnBrokerHouse.setVisibility(View.GONE);

		if(AppConstants.brokerConfigBean != null){
			
			TextView tvLoginPoweredBy	= (TextView) findViewById(R.id.tvLoginPoweredBy);
//			( (LinearLayout) findViewById(R.id.ll_login) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntLoginBgImg() );
//			loginButton.setBackgroundResource( AppConstants.brokerConfigBean.getIntLoginBtnBgImg() );
//			( (TextView) findViewById(R.id.tvUsername) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
//			( (TextView) findViewById(R.id.tvPassword) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
//			( (TextView) findViewById(R.id.tvLoginVersion) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );		
//			tvLoginPoweredBy.setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );		
//			cbRememberMe.setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
//			forgetPasswordLink.setTextColor( AppConstants.brokerConfigBean.getIntLinkTextColor() );
//			registerLink.setTextColor( AppConstants.brokerConfigBean.getIntLinkTextColor() );
//			termLink.setTextColor( AppConstants.brokerConfigBean.getIntLinkTextColor() );
			tvLoginPoweredBy.setVisibility( AppConstants.brokerConfigBean.showPoweredByStatement() ? View.VISIBLE : View.GONE);
			
			//imgSelectedBroker.setImageResource(AppConstants.brokerConfigBean.getIntBrokerHouseBtnImg());
			txtSelectedBrokerName.setText(this.selectedBrokerName);
			
			AppConstants.setStrBaseCurrencyCode( AppConstants.brokerConfigBean.getStrDefaultCurrencyType() );
			
			if( AppConstants.brokerConfigBean.showRememberMe() ){
				cbRememberMe.setVisibility(View.VISIBLE);
				isRememberMe	= Boolean.parseBoolean( AppConstants.getCachedBrokerInitParamValue(LoginActivity.this, AppConstants.brokerConfigBean.getStrAppName() + "_" + PreferenceConstants.IS_REMEMBER_ME) );
				cbRememberMe.setChecked(isRememberMe);
			}
		
			nameField.setText( isRememberMe ? AppConstants.getCachedBrokerInitParamValue(LoginActivity.this, AppConstants.brokerConfigBean.getStrAppName() + "_" + PreferenceConstants.USER_NAME) : "");
			
			checkAnnouncementVisiblity();
			
			registerLink.setVisibility(AppConstants.brokerConfigBean.hasRegistration() ? View.VISIBLE : View.GONE);
			
			forgetPasswordLink.setVisibility(AppConstants.brokerConfigBean.isEnablePasswordFeatures() ? View.VISIBLE : View.GONE);
			
			//Change_Request-20160722, ReqNo. 6
			if(AppConstants.brokerConfigBean.hasRegistration() && AppConstants.brokerConfigBean.hasSignInTermsNCondition()){
				termLink.setVisibility(View.GONE);
				secondaryTermLink.setVisibility(View.VISIBLE);
			} else{
				termLink.setVisibility(AppConstants.brokerConfigBean.hasSignInTermsNCondition() ? View.VISIBLE : View.GONE);
				secondaryTermLink.setVisibility(View.GONE);
			}

			//Change_Request-20170601, ReqNo.16
			if(AppConstants.brokerConfigBean.isShowContactUsOnLoginPage()){
				emailLink.setVisibility(View.VISIBLE);
				callLink.setVisibility(View.VISIBLE);
				termLink.setVisibility(View.GONE);
				secondaryTermLink.setVisibility(AppConstants.brokerConfigBean.hasSignInTermsNCondition() ? View.VISIBLE : View.GONE);
			}
			
			//Change_Request-20170601, ReqNo.6
			importantNoteLink.setVisibility(AppConstants.brokerConfigBean.isEnableContraAlert()? View.VISIBLE : View.GONE);
		
		} else {
			DefinitionConstants.Debug("repaintUI(): brokerConfigBean is null! UI not repainted!");
		}
		
		passwordField.setText("");
		
		if(nameField.getText().length() == 0)
			nameField.requestFocus();
		else
			passwordField.requestFocus();
		
		//Added by Thinzar, to cater TCMobile 2.0 design for different brokers
		if(AppConstants.brokerConfigBean.isHasBottomTextOnLoginPage()){
			imgLoginBottomText.setVisibility(View.VISIBLE);
		} else{
			imgLoginBottomText.setVisibility(View.GONE);
		}
	}

	private void checkAnnouncementVisiblity(){
		if(AppConstants.brokerConfigBean.getIntAnnouncementMenuIconBgImg()!=0){
			announcementButton.setVisibility(View.VISIBLE);
			announcementButton.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View view) {
					Intent intent= new Intent();
					intent.setClass(LoginActivity.this, AnnouncementActivity.class);
					intent.putExtra(ParamConstants.FROM_LOGIN_TAG, true);
					startActivity(intent);
				}
			});
		}else{
			announcementButton.setVisibility(View.GONE);
		}
	}
	
	/*removeSplashScreen() and showSplashScreen() commented out by Thinzar, Change_Request-20141101, ReqNo. 1
	//Removes the Dialog that displays the splash screen
	protected void removeSplashScreen() {
		try{
			//commented out by Thinzar@20140701
			//if(splashDialog != null && splashDialog.isShowing()){
			//	splashDialog.dismiss();
			//	splashDialog = null;
			//}
			
			
			//Added by Thinzar@20140701 - Change_Request-20140701,ReqNo.1 - START
			String[] parameters = {PACKAGE_NAME, version};
			new HitJspCheckPlist(this).execute(parameters);
			//Added by Thinzar@20140701 - Change_Request-20140701,ReqNo.1 - END
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//Shows the splash screen over the full Activity
	protected void showSplashScreen() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			splashDialog = new Dialog(this, R.style.SplashScreen);
			// splashDialog = new Dialog(this);
			splashDialog.setContentView(R.layout.splash_screen);
	
			splashDialog.setCancelable(false);
			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! this.isFinishing() )
				splashDialog.show();
	
			// note : Set Runnable to remove splash screen just in case
			
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					LoginActivity.this.removeSplashScreen();
				}
			}, 2000);
			
			// Added by Mary@20130829 - ChangeRequest-20120719, ReqNo.4 - START
			// note : Set Runnable to show broker house list if needed //
			//Commented out by Thinzar@20140702 - Change_Request-20140701, ReqNo.1
			
			//if(AppConstants.brokerConfigBean == null || AppConstants.hasMultiLogin){
			//	new Handler().postDelayed(new Runnable() {
			//		@Override
			//		public void run() {
			//			LoginActivity.this.showBrokerHouseListScreen();
			//		}
			//	}, 1800);
			//}
			 
			// Added by Mary@20130829 - ChangeRequest-20120719, ReqNo.4 - END
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	*/

	// Added by Mary@20130829 - ChangeRequest-20120719, ReqNo.4 - START
	//Edited by Thinzar@20140701 - Change_Request-20140701, ReqNo.1
	private void showBrokerHouseListScreen(String mPackageName){
		
		/*
		Intent intent = new Intent();
		intent.putExtra(ParamConstants.PARAM_PACKAGE_NAME, mPackageName);
		intent.setClass(LoginActivity.this, SelectBrokerHouseActivity.class);
		startActivityForResult(intent, AppConstants.SELECT_BROKER_HOUSE_REQUEST);
		*/
		//Edited by Thinzar, Change_Request-20141101, ReqNo.1
		Intent intent = new Intent();
		intent.putExtra(ParamConstants.PARAM_PACKAGE_NAME, mPackageName);
		intent.setClass(LoginActivity.this, SelectBrokerHouseActivity.class);
		startActivity(intent);
	}
	// Added by Mary@20130829 - ChangeRequest-20120719, ReqNo.4 - END
	
	public void showLoginScreen() {
		setContentView(R.layout.login_screen);
		//linearMainLogin = (LinearLayout) findViewById(R.id.ll_login) ;	//added by Thinzar@20140704
		/* Mary@20130829 - ChangeRequest-20120719, ReqNo.4
		// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
		spinnerBroker 	= (Spinner) findViewById(R.id.spBroker);
		*/

		nameField		= (EditText) findViewById(R.id.nameField);
		passwordField 	= (EditText) findViewById(R.id.passwordField);
		loginButton 	= (TextView) this.findViewById(R.id.loginButton);
		announcementButton = (Button) this.findViewById(R.id.announcementButton);
		nameField.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS); // to disable autocorrect in textfield
		
		// Added by Mary@20130828 - ChangeRequest-20120719, ReqNo.4
		//btnBrokerHouse	= (Button) this.findViewById(R.id.btnBrokerHouse);	//commented by Thinzar@20141229, replaced by below three lines
		btnBrokerHouse			= (LinearLayout) findViewById(R.id.btnBrokerHouse);
		//imgSelectedBroker		= (ImageView)findViewById(R.id.imgSelectedBroker);
		txtSelectedBrokerName	= (TextView)findViewById(R.id.txtSelectedBrokerName);
		imgLoginBottomText		= (ImageView)findViewById(R.id.imgLoginBottomText);
		
		// Added by Mary@20130528 - Change_Request-20130424, ReqNo 4
		cbRememberMe	= (CheckBox)this.findViewById(R.id.cbRememberMe);
		
		// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - START
		forgetPasswordLink 	= (TextView) this.findViewById(R.id.forgetPasswordLabel);
		registerLink 		= (TextView) this.findViewById(R.id.registerLabel);
		termLink 			= (TextView) this.findViewById(R.id.termLabel);
		secondaryTermLink	= (TextView) this.findViewById(R.id.seconary_term_link);		//Change_Request-20160722, ReqNo. 6
		importantNoteLink	= (TextView) this.findViewById(R.id.tv_important_note);			//Change_Request-20170601, ReqNo.6
		emailLink			= (TextView) this.findViewById(R.id.emailLabel); 				//Change_Request-20170601, ReqNo.16
		callLink			= (LinearLayout) this.findViewById(R.id.ll_call_us); 				//Change_Request-20170601, ReqNo.16
		// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - END

		// Added by Mary@20130320 - Fixes_Request-20130319, ReqNo.3
		passwordField.setText("");
		
		//if (AppConstants.hasMultiLogin) {	
		if(sharedPreferences.getBoolean(PreferenceConstants.HAS_MULTI_LOGIN, false)){
			/* Mary@20130829 - ChangeRequest-20120719, ReqNo.4 - START
			strArrBrokerType 					= this.getResources().getStringArray(R.array.broker_arrays);
			ArrayAdapter<String> arrayAdapter 	= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strArrBrokerType);
			spinnerBroker.setAdapter(arrayAdapter);
			spinnerBroker.setOnItemSelectedListener(this);
			spinnerBroker.setVisibility(View.VISIBLE);
			*/
			
			btnBrokerHouse.setVisibility(View.VISIBLE);
			// Mary@20130828 - ChangeRequest-20120719, ReqNo.4 - END
		} else {
			/* Mary@20130828 - ChangeRequest-20120719, ReqNo.4
			spinnerBroker.setVisibility(View.GONE);
			*/
			/* Mary@20130528 - Fixes_Request-20130523, ReqNo.3
			repaintUI();
			*/
			
			// Added by Mary@20130828 - ChangeRequest-20120719, ReqNo.4
			btnBrokerHouse.setVisibility(View.GONE);
		}
		// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
		
		nameField.setSingleLine();
		/*
		 * Mary@20120814 - Change_Request-20120719, ReqNo.4
		 String username = sharedPreferences.getString(PreferenceConstants.USER_NAME, null);
		 nameField.setText(username);
		 */	

		loginButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				try{
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(nameField.getWindowToken(), 0);
	
					// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
					if (AppConstants.brokerConfigBean == null) {
						AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
						builder.setMessage(getResources().getString(R.string.login_choose_broker))
								.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
										/* Mary@20130829 - ChangeRequest-20120719, ReqNo.4
										spinnerBroker.requestFocus();
										*/
										btnBrokerHouse.requestFocus();
									}
								});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! LoginActivity.this.isFinishing() )
							alert.show();
	
						return;
					}
					// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
					
					// Added by Mary@20121224 - Fixes_Request-20121221, ReqNo.2 - START
					if( nameField.getText().toString().trim().length() == 0 ){
						AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
						builder.setMessage(getResources().getString(R.string.login_error_blank_username))
								.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
										nameField.requestFocus();
									}
								});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! LoginActivity.this.isFinishing() )
							alert.show();
						
						return;
					}
					// Added by Mary@20121224 - Fixes_Request-20121221, ReqNo.2 - END
					// Added by Mary@20121224 - Fixes_Request-20121221, ReqNo.1 - START
					if( passwordField.getText().toString().trim().length() == 0 ){
						AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
						builder.setMessage(getResources().getString(R.string.login_error_blank_password))
								.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
										passwordField.requestFocus();
									}
								});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! LoginActivity.this.isFinishing() )
							alert.show();
						
						return;
					}
					// Added by Mary@20121224 - Fixes_request-20121221, ReqNo.1 - END
	
					if (!LoginActivity.this.isNetworkAvailable()) {
						LoginActivity.this.showNetworkUnavailableAlert();	
						// Added by Mary@20130527 - Fixes_Request-20130523, ReqNo.1
						LoginActivity.this.passwordField.setText("");
						return;
					}
					
					login();
				
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
				}catch(Exception e){
					e.printStackTrace();
				}
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
			}
		});

		/* Mary@20130528 - Fixes_Request-20130523, ReqNo.3
		TextView forgetPasswordLabel = (TextView) this.findViewById(R.id.forgetPasswordLabel);
		*/
		forgetPasswordLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				//Fixes_Request-20170103, ReqNo.5 (reset ATP IP and port in case if user click here after first time incorrect password)
				if(AppConstants.orgATPIPFromConfigFile.length() > 0){
					AppConstants.brokerConfigBean.setStrAtpIP(AppConstants.orgATPIPFromConfigFile);
					AppConstants.brokerConfigBean.setIntAtpPort(AppConstants.orgATPPortFromConfigFile);
					
					AppConstants.setEncryptedKey(null);
					AppConstants.userParamInByte	= null;
					AppConstants.userParam			= null;
				}
				
				Intent intent = new Intent();
				// Added by Mary@20130104 - additional information to ease user process
				intent.putExtra( ParamConstants.USERNAME_TAG, nameField.getText().toString() );
				intent.setClass(LoginActivity.this,	ForgetPasswordActivity.class);
				LoginActivity.this.startActivity(intent);

			}
		});

		/* Mary@20130528 - Fixes_Request-20130523, ReqNo.3
		TextView registerLabel = (TextView) this.findViewById(R.id.registerLabel);
		*/
		registerLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(LoginActivity.this, RegisterActivity.class);
				LoginActivity.this.startActivity(intent);
			}

		});

		/* Mary@20130528 - Fixes_Request-20130523, ReqNo.3
		TextView termLabel = (TextView) this.findViewById(R.id.termLabel);
		*/
		// Added by Mary@20130117 - Fixes_Request-20130108, ReqNo.7 - START
		termLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent();
				intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_TERM_TYPE);
				intent.putExtra(ParamConstants.FROM_LOGIN_TAG, true);
				intent.setClass(LoginActivity.this, TermsActivity.class);
				LoginActivity.this.startActivity(intent);

			}
		});
		// Added by Mary@20130117 - Fixes_Request-20130108, ReqNo.7 - END
		
		//Change_Request-20160722, ReqNo.6
		secondaryTermLink.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_TERM_TYPE);
				intent.putExtra(ParamConstants.FROM_LOGIN_TAG, true);
				intent.setClass(LoginActivity.this, TermsActivity.class);
				LoginActivity.this.startActivity(intent);
			}

		});
		
		// Change_Request-20170601, ReqNo.6
		importantNoteLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String strContraTitle = "<font color='#790008'>" + getResources().getString(R.string.contra_alert_title) + "</font>";

				AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
				builder.setTitle(Html.fromHtml(strContraTitle))
						.setMessage(getResources().getText(R.string.contra_alert_msg)).setCancelable(false)
						.setPositiveButton(getResources().getString(R.string.dialog_button_close),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								});

				AlertDialog alert = builder.create();
				if (!LoginActivity.this.isFinishing())
					alert.show();
			}
		});

		//Change_Request-20170601, ReqNo.16
		emailLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				String mailto = "mailto:" + AppConstants.brokerConfigBean.getEmailContact();

				Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
				emailIntent.setData(Uri.parse(mailto));

				try {
					startActivity(emailIntent);
				} catch (ActivityNotFoundException e) {
					//TODO: Handle case where no email app is available
				}
			}
		});

		callLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + AppConstants.brokerConfigBean.getPhoneContact()));
				startActivity(intent);
			}
		});
		
		// Added by Mary@20130828 - ChangeRequest-20120719, ReqNo.4 - START
		btnBrokerHouse.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LoginActivity.this.showBrokerHouseListScreen(getApplicationContext().getPackageName());
			}

		});
		// Added by Mary@20130828 - ChangeRequest-20120719, ReqNo.4 - END
		
		// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - START
		cbRememberMe.setOnCheckedChangeListener( new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				isRememberMe	= isChecked;
			}
		});
		// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - END
		
		/* Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - START
		// Added by Mary@20130528 - Change_Request-20130424, ReqNo 4
		cbRememberMe.setVisibility(View.GONE);

		/*
		 * Mary@2012812 - Change_Request-20120719, ReqNo.3 - START
		 * registerLabel.setVisibility(View.GONE);
		 /
		termLabel.setVisibility(View.GONE);
		registerLabel.setVisibility(View.GONE);
		forgetPasswordLabel.setVisibility(View.GONE);

		if (AppConstants.brokerConfigBean != null) {
			/* Mary@20130502 - Fixes_Request-20130419, ReqNo.2 - START
			if( AppConstants.brokerConfigBean.hasRegistration() )
				registerLabel.setVisibility(View.VISIBLE);
			else
				termLabel.setVisibility(View.VISIBLE);

			forgetPasswordLabel.setVisibility(View.VISIBLE);
			/ 
			registerLabel.setVisibility(AppConstants.brokerConfigBean.hasRegistration() ? View.VISIBLE : View.GONE);
			termLabel.setVisibility(AppConstants.brokerConfigBean.hasSignInTermsNCondition() ? View.VISIBLE : View.GONE);
			forgetPasswordLabel.setVisibility(AppConstants.brokerConfigBean.isEnablePasswordFeatures() ? View.VISIBLE : View.GONE);
			// Mary@20130502 - Fixes_Request-20130419, ReqNo.2 - END
			
			// Added by Mary@20130528 - Change_Request-20130424, ReqNo 4
			cbRememberMe.setVisibility(AppConstants.brokerConfigBean.showRememberMe() ? View.VISIBLE : View.GONE);
		}
		// Mary@2012812 - Change_Request-20120719, ReqNo.3 - END
		*/
		// Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - END

	}
	
	
	// Diyana@20160817 - Fixes_Request-20160614, ReqNo.7 - START
	private void setupBtnTutorial(View mView){

		TextView tvTutorial = (TextView)mView.findViewById(R.id.tv_tutorial);

		if(AppConstants.brokerConfigBean.isTutorialAvailable() && !AppConstants.isAppMultilogin){
			tvTutorial.setVisibility(View.VISIBLE);
			tvTutorial.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent intent = new Intent();
					intent.setClass(LoginActivity.this, TutorialActivity.class);
					intent.putExtra(ParamConstants.LOGIN_TYPE_TAG, ParamConstants.SINGLE_LOGIN_TAG);
					startActivity(intent);
					finish();
				}
			});
		}else{
			tvTutorial.setVisibility(View.GONE);
		}
	}

	//Diyana@20160817 - Fixes_Request-20160614, ReqNo.7 - END
	
	public void showNetworkUnavailableAlert() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.login_error_no_network))
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// LoginActivity.this.finish();
						}
					});
			AlertDialog alert = builder.create();
			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! this.isFinishing() )
				alert.show();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	
	public void showAlertAndExitApp(String errorMsg){
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(errorMsg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					LoginActivity.this.finish();
				}
			});
			
			AlertDialog alert = builder.create();
			if( ! this.isFinishing() )
				alert.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void showMainThreadAlert(Exception ex) {
		Message message = handler.obtainMessage();
		message.obj = ex;
		handler.sendMessage(message);
	}

	final Handler handler = new Handler() {
		public void handleMessage(Message message) {
			Exception ex = (Exception) message.obj;
			SystemUtil.showExceptionAlert(ex, LoginActivity.this);
		}
	};

	// public void refreshTradeAliveThread(List changedSymbols) {
	// Message message = handler.obtainMessage();
	// message.obj = changedSymbols;
	// handler.sendMessage(message);
	// }

	public void login(){
		username 		= nameField.getText().toString().trim();
		password 		= passwordField.getText().toString().trim();
		isRememberMe	= cbRememberMe.isChecked();
		
		// Added by Mary@20130527 - Fixes_Request-20130523, ReqNo.1
		LoginActivity.this.passwordField.setText("");
		
		//Fixes_Request-20161101, ReqNo.7 (reset ATP IP and port in case load balance server failed)
		if(AppConstants.orgATPIPFromConfigFile.length() > 0){
			AppConstants.brokerConfigBean.setStrAtpIP(AppConstants.orgATPIPFromConfigFile);
			AppConstants.brokerConfigBean.setIntAtpPort(AppConstants.orgATPPortFromConfigFile);
		}
		
		// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
		if ( AppConstants.brokerConfigBean.isEnableE2EEEncryption() )
			new E2EEELogin().execute(password);
		else
			// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
			new ATPLogin().execute();	
			
	}

	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
	private class E2EEELogin extends AsyncTask<String, Void, Exception> {
		String response = null;
		String strInputPassword = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				dialog.setMessage(getResources().getString(R.string.login_step1));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! LoginActivity.this.isFinishing() )
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected Exception doInBackground(String... params) {
			try {
				response = AtpConnectUtil.tradeGetE2EEParams();

				if (response == null){
					throw new Exception(ErrorCodeConstants.FAIL_GET_E2EE_PARAM);
				}else{ 
					/* catch invalid response, e.g. ")=||||||"  - START */
					// Added by Fixes_Request-20130424, ReqNo.11
					if( ( response.split("\r\n") ).length < 2)
						throw new Exception(ErrorCodeConstants.FAIL_GET_VALID_E2EE_PARAM);
						
					String strTemp	= response.split("\r\n")[1];
					strTemp			= strTemp.substring( strTemp.indexOf(DefinitionConstants.RECORD_HEADER) + DefinitionConstants.RECORD_HEADER.length(), strTemp.length() );
					if(strTemp.split("\\|").length == 0)
						throw new Exception(ErrorCodeConstants.FAIL_GET_VALID_E2EE_PARAM);
					/* catch invalid response, e.g. ")=||||||"  - END */
					
					strInputPassword= params[0];
				}
			} catch (InvalidPasswordException e) {
				e.printStackTrace();
				return e;
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				/*
				 * Mary@20130131 - while login shouldn't show session expired
				 * error message return e;
				 */
				return new IOException(ErrorCodeConstants.FAIL_AUTHENTICATION_IOE);
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				if (result != null) {
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
					LoginActivity.this.showMainThreadAlert(result);
					return;
				}

				E2EEInfo e2eeInfo	= AtpMessageParser.parseE2EEParams(response);
				/* Mary@20131008 - Change_Request-20130711, ReqNo.14
				password 			= encryptWithE2EE(e2eeInfo, strInputPassword);
				*/
				password 			= FormatUtil.encryptWithE2EE(e2eeInfo, strInputPassword);
				strE2EERandomNumber = e2eeInfo.getStrE2EERandomNumber();

				// RSA Encryption
				PublicKey pkE2EERSAPbKey = FormatUtil.loadRSAPublicKey( e2eeInfo.getStrRSAPublicKey(), e2eeInfo.getStrRSAModulus_Base64(), e2eeInfo.getStrRSAExponent_Base64() );
				strRSAPassword = FormatUtil.encryptWithRSA(pkE2EERSAPbKey, strInputPassword);

				new ATPLogin().execute();
			} catch (Exception e) {
				/* Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				if (dialog.isShowing())
					dialog.dismiss();
				*/
				
				e.printStackTrace();
				LoginActivity.this.showMainThreadAlert(e);
				return;
			}
		}
	}

	/* Mary@20131008 - Change_Request-20130711, ReqNo.14
	private String encryptWithE2EE(E2EEInfo e2eeInfo, String strPassword) {
		String strE2EEEncryptedPwd = null;
		try {
			SncE2EEClient e2eeClient = new SncE2EEClient();
			strE2EEEncryptedPwd = e2eeClient.encryptPIN1( e2eeInfo.getStrE2EEPublicKey(),e2eeInfo.getStrE2EERandomNumber(), strPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return strE2EEEncryptedPwd;
	}
	*/
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END

	private class ATPLogin extends AsyncTask<Void, Void, Exception> {
		/* Mary@20130527 - comment unused variable
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private Context context;
		*/

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				dialog.setMessage(getResources().getString(R.string.login_step2));

				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! LoginActivity.this.isFinishing() && !dialog.isShowing())
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected Exception doInBackground(Void... params) {
			PublicKey RSAPublicKey = null;
			try {

				if (AppConstants.brokerConfigBean.isEnableEncryption()) {
					//Change_Request-20170601, ReqNo.14 - START
					if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
						//1. Get RSA Public Key
						RSAPublicKey = AtpConnectUtil.tradeGetPK2A();

						intRetryCtr = 0;
						while (LoginActivity.this.intRetryCtr < AppConstants.intMaxRetry && RSAPublicKey == null) {
							LoginActivity.this.intRetryCtr++;
							RSAPublicKey = AtpConnectUtil.tradeGetPK2A();
						}
						intRetryCtr = 0;

						if(RSAPublicKey == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_PUBLIC_KEY);

						String[] AESKey = AtpConnectUtil.tradeGetKey2A(RSAPublicKey);
						while (LoginActivity.this.intRetryCtr < AppConstants.intMaxRetry && AESKey == null) {
							LoginActivity.this.intRetryCtr++;
							AESKey = AtpConnectUtil.tradeGetKey2A(RSAPublicKey);
						}
						intRetryCtr = 0;

						if (AESKey != null) {
							encryptedKey = AESKey[0];
							userParamKeyInByte = AtpConnectUtil.tradeGetSession2A(AESKey);

							while (LoginActivity.this.intRetryCtr < AppConstants.intMaxRetry && userParamKeyInByte == null) {
								LoginActivity.this.intRetryCtr++;
								userParamKeyInByte = AtpConnectUtil.tradeGetSession2A(AESKey);
							}
							intRetryCtr = 0;

							if (userParamKeyInByte == null)
								throw new Exception(ErrorCodeConstants.FAIL_GET_ENCRYPTED_SESSION);
						} else {
							throw new Exception(ErrorCodeConstants.FAIL_GET_ENCRYPTED_KEY);
						}
					}
					//Change_Request-20170601, ReqNo.14 - END
					else {
						intRetryCtr = 0;
						//Added by Thinzar, Change_Request-20150901, ReqNo.4 - START
						RSAPublicKey = AtpConnectUtil.tradeGetPK_LBSupport();
						while (LoginActivity.this.intRetryCtr < AppConstants.intMaxRetry && RSAPublicKey == null) {
							LoginActivity.this.intRetryCtr++;
							RSAPublicKey = AtpConnectUtil.tradeGetPK_LBSupport();
						}
						intRetryCtr = 0;
						//Added by Thinzar, Change_Request-20150901, ReqNo.4 - END

						if(RSAPublicKey == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_PUBLIC_KEY);

						String[] AESKey = AtpConnectUtil.tradeGetKey2(RSAPublicKey);
						//Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
						while (LoginActivity.this.intRetryCtr < AppConstants.intMaxRetry && AESKey == null) {
							LoginActivity.this.intRetryCtr++;
							AESKey = AtpConnectUtil.tradeGetKey2(RSAPublicKey);
						}
						intRetryCtr = 0;
						// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

						if (AESKey != null) {
							encryptedKey = AESKey[0];
							userParamKeyInByte = AtpConnectUtil.tradeGetSession2(AESKey);
							// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
							while (LoginActivity.this.intRetryCtr < AppConstants.intMaxRetry && userParamKeyInByte == null) {
								LoginActivity.this.intRetryCtr++;
								userParamKeyInByte = AtpConnectUtil.tradeGetSession2(AESKey);
							}
							intRetryCtr = 0;

							if (userParamKeyInByte == null)
								throw new Exception(ErrorCodeConstants.FAIL_GET_ENCRYPTED_SESSION);
						} else {
							throw new Exception(ErrorCodeConstants.FAIL_GET_ENCRYPTED_KEY);
							// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
						}
					}

				} else {
					userParamKey = AtpConnectUtil.tradeGetSession();
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					while (LoginActivity.this.intRetryCtr < AppConstants.intMaxRetry && userParamKey == null) {
						LoginActivity.this.intRetryCtr++;
						userParamKey = AtpConnectUtil.tradeGetSession();
					}
					intRetryCtr = 0;

					if(userParamKey == null)
						throw new Exception(ErrorCodeConstants.FAIL_GET_SESSION);
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}

				return null;
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				return new IOException(ErrorCodeConstants.FAIL_AUTHENTICATION_IOE);
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
		}

		@Override
		protected void onPostExecute(Exception result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if (result != null) {
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
					LoginActivity.this.showMainThreadAlert(result);
					return;
				}
	
				/*
				 * Mary@20130111 - Fixes_Request-20130108, ReqNo.5
				 * if(AppConstants.EnableEncryption)
				 */
				if (AppConstants.brokerConfigBean.isEnableEncryption()) {
					if (userParamKeyInByte == null)
						return;
				} else {
					if (userParamKey == null)
						return;
				}
	
				if(encryptedKey != null)
					AppConstants.setEncryptedKey(encryptedKey);
	
				new tradeLogin().execute();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	private class tradeLogin extends AsyncTask<Void, Void, Exception> {
		//Added by Thinzar @20140723 - Changes_Request-20140701 - Req. No 6 - START
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(dialog.isShowing())	dialog.setMessage(getResources().getString(R.string.login_step3));
		}
		//Added by Thinzar @20140723 - END
		
		@Override
		protected Exception doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Map<String, Object> parameters = new HashMap<String, Object>();

			if (AppConstants.brokerConfigBean.isEnableEncryption())
				parameters.put(ParamConstants.USERPARAM_KEY_INBYTE_TAG, userParamKeyInByte);
			else
				parameters.put(ParamConstants.USERPARAM_KEY_TAG, userParamKey);
			// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
			
			parameters.put(ParamConstants.USERNAME_TAG, username);
			parameters.put(ParamConstants.PASSWORD_TAG, password);

			// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
			if (AppConstants.brokerConfigBean.isEnableE2EEEncryption()) {
				parameters.put(ParamConstants.E2EE_RANDOM_NUM_TAG, strE2EERandomNumber);
				parameters.put(ParamConstants.RSA_PASSWORD_TAG, strRSAPassword);
			}
			// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
			
			// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - START
			parameters.put( ParamConstants.PARAM_STR_DEVICE_OS_TYPE, SystemUtil.getDeviceOSType() );
			parameters.put( ParamConstants.PARAM_STR_DEVICE_OS_VERSION, SystemUtil.getDeviceOSVersion() );
			parameters.put( ParamConstants.PARAM_STR_DEVICE_MODEL, SystemUtil.getDeviceModelName() );
			parameters.put( ParamConstants.PARAM_STR_DEVICE_CONNECTIVITY, SystemUtil.getDeviceConnectivity(LoginActivity.this) );
			parameters.put( ParamConstants.PARAM_STR_SIM_OPERATOR_NAME, SystemUtil.getSIMOperatorName(LoginActivity.this) );
			parameters.put( ParamConstants.PARAM_STR_EXTRA_INFO, getResources().getString(R.string.version_tag) );
			parameters.put( ParamConstants.PARAM_STR_SCREEN_SIZE, SystemUtil.getDeviceScreenSize(LoginActivity.this) );
			// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - END
			
			//Added by Thinzar@20141111, Change_Request-20141101, ReqNo.3
			parameters.put(ParamConstants.BROKER_ID_TAG, brokerId);
			
			//Added by Thinzar, Change_Request20160722, ReqNo.11
			if(loginType != null)
				parameters.put(ParamConstants.PARAM_STR_LOGIN_TYPE, loginType);		//no need to send for I, send "B" for Fingerprint login
			
			StringBuffer response 	= new StringBuffer(25600);
			paramBuffer 			= new StringBuffer();

			userParam 				= null;
			try {
				userParam = AtpConnectUtil.tradeLogin(parameters, response, paramBuffer);
			} catch (InvalidPasswordException e) {
				return e;
			} catch (FailedAuthenicationException e) {
				/* Mary@20130131 - while login shouldn't show session expired error message
				 return e;
				 */
				return new IOException(ErrorCodeConstants.FAIL_AUTHENTICATION_IOE);
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			} catch (Exception e) {
				return e;
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			}

			results 				= response.toString();

			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result != null) {
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
				try{
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
				}catch(Exception e){
					e.printStackTrace();
				}
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END

				//Change_Request-20160722, ReqNo.9 - START

				if(AppConstants.isLoginWithTouchID){
					if(result.getMessage().contains("1109 ATP")){
						String message = result.getMessage() + "\n\nYour password has been changed. Please re-activate TouchID!";
						showInvalidPwdAlertAndResetTouchID(message);
						return;
					}
				}
				//Change_Request-20160722, ReqNo.9 - END

				LoginActivity.this.showMainThreadAlert(result);
				return;
			}

			Properties properties 	= new Properties();

			// Added by Kw@20130116 - Change_Request20130104, ReqNo. 2
			AppConstants.info2FA = AtpMessageParser.parse2FAInfo(results);//"[Is2FARequired]=Y\n\r[DeviceList]=VA-2646012140|124144124,VA-2646012141|1231232,VA-2646012142|124134");
			// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
			AppConstants.clientLimitOption = AtpMessageParser.parseClientLimitOption(results);

			try {
				properties.load( new ByteArrayInputStream( results.getBytes(AppConstants.STRING_ENCODING_FORMAT) ) );
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				LoginActivity.this.showMainThreadAlert(e);
				e.printStackTrace();
			}

			senderCode = properties.getProperty(AppConstants.ATP_SENDER_CODE);
			brokerCode = properties.getProperty(AppConstants.ATP_BROKER_CODE);
			
			//Added by Thinzar@20140730, Change_Request-20140701, Req No. 9 - START
			List<String> pymtTypes = new ArrayList<String>();
			if(properties.containsKey("[MapPaymentCode]")){
				pymtTypes = AtpMessageParser.parsePaymentType(results);
			}
			((StockApplication)LoginActivity.this.getApplication()).setPaymentTypes(pymtTypes);
			//Added by Thinzar@20140730 - END
			
			// Added by Mary@20120717 - clear AppConstants.selectedAcc when current username is differ with previous username - START
			String strPrevUsername = sharedPreferences.getString(PreferenceConstants.USER_NAME, null);
			if( strPrevUsername != null	&& !strPrevUsername.toLowerCase().equals(LoginActivity.this.username) )
				AppConstants.selectedAcc = null;
			// Added by Mary@20120717 - clear AppConstants.selectedAcc when current username is differ with previous username - END

			editor = sharedPreferences.edit();
			editor.putString(PreferenceConstants.ATP_USER_PARAM, userParam);
			editor.putString(PreferenceConstants.ATP_PARAM_STRING, paramBuffer.toString());
			editor.putString(PreferenceConstants.SENDER_CODE, senderCode);
			//System.out.println("sendercode: " + senderCode);
			editor.putString(PreferenceConstants.BROKER_CODE, brokerCode);
			editor.putString(PreferenceConstants.USER_NAME, username);
			// Added by Mary@20130528 - Change_Request-20130424, ReqNo.4
			editor.putString( PreferenceConstants.IS_REMEMBER_ME, String.valueOf(isRememberMe) );
			editor.commit();
			
			// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - START
			//boolean[] hasResetPwdPin	= AtpMessageParser.parseResetPwdPin(results);		//Commented out by Thinzar, Change_Request-20141101, Reqno 16
			boolean[] hasResetPwdPin	= AtpMessageParser.parseResetPwdPinHint(results);	//Edited by Thinzar, Change_Request-20141101, Reqno 16
			AppConstants.setHasResetPwd( hasResetPwdPin[0] );
			AppConstants.setHasResetPin( hasResetPwdPin[1] );
			boolean isChangeHintAndAnswer	= false;

			//Fixes_Request-20170701, ReqNo.18
			if(AppConstants.brokerConfigBean.isRequireLoginTcPlusToChangeHint()) {
				isChangeHintAndAnswer = hasResetPwdPin[2];    //Added by Thinzar, Change_Request-20141101, Reqno 16.
			}

			if( AppConstants.hasResetPwd() || AppConstants.hasResetPin() || isChangeHintAndAnswer){
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
				try{
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
				}catch(Exception e){
					e.printStackTrace();
				}
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
				
				//Edited by Thinzar, Change_Request-20141101, Reqno 16. - START (Fixes_Request-20170701, ReqNo.18)
				if(isChangeHintAndAnswer){
					String alertMsg = ErrorCodeConstants.REQUIRE_LOGIN_TCPLUS;
					alertMsg.replace("TC_PLUS_LINK", AppConstants.brokerConfigBean.getTcPlusLink());
					//show alert
					AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
					builder.setMessage(alertMsg)
							.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									
								}
							});
					AlertDialog alert = builder.create();
					if( ! LoginActivity.this.isFinishing() )
						alert.show();
					
				} else {
					Intent intent	= new Intent();
					intent.putExtra(ParamConstants.PASSWORD_TYPE_TAG, AppConstants.hasResetPwd() ? AppConstants.CHANGE_PASSWORD_TYPE : AppConstants.CHANGE_PIN_TYPE);
					
					//Change_Request-20190119, ReqNo.11
					boolean isShowExpireMessage = hasResetPwdPin[3];
					if(isShowExpireMessage) 
						intent.putExtra(ParamConstants.STATUS_MSG_TAG, getResources().getString(R.string.password_expiry_message));
					
					intent.setClass(LoginActivity.this, ChangePasswordActivity.class);
					LoginActivity.this.startActivity(intent);
				}
				//Edited by Thinzar, Change_Request-20141101, Reqno 16. - END
				
			}else{
			// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - END
				// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
				Map<String, String> mapBrokerInit = new HashMap<String, String>();
				mapBrokerInit.put(AppConstants.brokerConfigBean.getStrAppName() + "_" + PreferenceConstants.USER_NAME, username);
				// Added by Mary@20130528 - Change_Request-20130424, ReqNo.4
				mapBrokerInit.put( AppConstants.brokerConfigBean.getStrAppName() + "_" + PreferenceConstants.IS_REMEMBER_ME, String.valueOf(isRememberMe) );
				AppConstants.cacheBrokerInit(LoginActivity.this, mapBrokerInit);
				// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
				
				/* Mary@20121228-Fixes_Request-20121106, ReqNo.11 - START
				exchangeInfos 			= AtpMessageParser.parseExchangeInfo(results);
				*/
				StringBuffer strExtraBuf= new StringBuffer();
				exchangeInfos 			= AtpMessageParser.parseExchangeInfo(results, strExtraBuf);
				currencyExchangeInfos	= AtpMessageParser.parseCurrencyExchangeInfo(strExtraBuf.toString());
				// Mary@20121228-Fixes_Request-20121106, ReqNo.11 - END		
				
				//Added by Thinzar, Change_Request-20150401, ReqNo. 1 - START
				if(properties.containsKey("[PaymentCfg_Payment]")){	
					Map<String, PaymentConfig> paymentConfigMap	= AtpMessageParser.parsePaymentConfig(results.substring(results.indexOf("[PaymentCfg_Payment]")));
					String[] arrPaymentTypes	= {"B" , "Def"};		//if new type of payment added to config, add here
					
					//set extra PaymentConfig to ExchangeInfo
					if(paymentConfigMap.size() > 0){
						for( Iterator<ExchangeInfo> itr = exchangeInfos.iterator(); itr.hasNext(); ){
							ExchangeInfo exchgInfo	= itr.next();
							String strQCExchgCode	= exchgInfo.getQCExchangeCode();
							
							for(String paymentType: arrPaymentTypes){
								String key	= strQCExchgCode + "|" + paymentType;
								if(paymentConfigMap.containsKey(key)){
									PaymentConfig configMap	= paymentConfigMap.get(key);
									exchgInfo.addPaymentConfigMap(configMap.getAccType(), configMap);
								}
							}
						}
					}
				}
				
				//--for debug purpose
				/*
				System.out.println("--Debug parse payment config:");
				for( Iterator<ExchangeInfo> itr = exchangeInfos.iterator(); itr.hasNext(); ){
					ExchangeInfo exchgInfo	= itr.next();
					System.out.println("--exchange code:" + exchgInfo.getQCExchangeCode()); 
					System.out.println("--paymentConfig size:" + exchgInfo.getPaymentConfigMap().size());
					String[] arrPaymentTypes	= {"B" , "Def"};
					
					Map<String, PaymentConfig> paymentConfigMap	= exchgInfo.getPaymentConfigMap();
					
					//get accountType B config
					if(paymentConfigMap.containsKey("B")){
						PaymentConfig configB	= paymentConfigMap.get("B");
						System.out.println("--accountType:" + configB.getAccType());
						System.out.println("--paymentType:" + Arrays.toString(configB.getPaymentMethods().toArray()));
						System.out.println("--currencies:" + Arrays.toString(configB.getCurrencies().toArray()));
					}
					
					//get accountType Def config
					if(paymentConfigMap.containsKey("Def")){
						PaymentConfig configDef	= paymentConfigMap.get("Def");
						System.out.println("--accountType:" + configDef.getAccType());
						System.out.println("--paymentType:" + Arrays.toString(configDef.getPaymentMethods().toArray()));
						System.out.println("--currencies:" + Arrays.toString(configDef.getCurrencies().toArray()));
					}
					
				}
				*/
				//--debug end
				
				//Added by Thinzar, Change_Request-20150401, ReqNo. 1 - END
	
				// Added by Mary@20130118 - Change_Request-20130104, ReqNo.3
				AppConstants.setRDSInfo(AtpMessageParser.parseRDSInfo(results));
	
				//Added by Thinzar, to parse additional userData required for stock alert
				//NOTE: if additional params needed in the future, parse inside below function.
				AtpMessageParser.parseAdditionalUserData(results);
				
				if (userParam != null) 
					new loginLMS().execute();
			}
		}
	}

	private class loginLMS extends AsyncTask<Void, Void, Exception> {
		
		private List<LMSServiceBean> infoFromLMS = null;
		/*
		 * Mary@20120814 - Change_Request-20120719, ReqNo.4 private
		 * LMSSubscription lms = new LMSSubscription();
		 */
		private LMSSubscription lms;
		
		//Added by Thinzar@20140813, Change_Request-20140801, ReqNo.3
		private HashMap<String, Integer> marketDepthinfos = new HashMap<String, Integer>();

		@Override
		protected void onPreExecute() {
			// initialize server, sponser, product
			lms = new LMSSubscription(AppConstants.brokerConfigBean.getStrLMSServer(), AppConstants.brokerConfigBean.getStrLMSSponsor(), AppConstants.brokerConfigBean.getStrLMSProduct());
			
			//Added by Thinzar@20140723 - Change_Request-20140701 - Req. 6 - START
			if(dialog.isShowing())	
				dialog.setMessage(getResources().getString(R.string.login_step4));
			//Added by Thinzar@20140723 - END
		}

		@Override
		protected Exception doInBackground(Void... params) {
			// TODO Auto-generated method stub

			boolean isSuccess 	= false;
			String LMSInfo 		= null;
			String lmsUsername	= username;	//Added by Thinzar, Change_Request-20150901, ReqNo. 2
			
			try {
				//Added by Thinzar, Change_Request-20150901, ReqNo. 2
				if(brokerId != null){
					if(brokerId.trim().length() > 0) 
						lmsUsername	= brokerId + "/" + username;
				}
				
				LMSInfo = lms.authenLMSServices(lmsUsername);	//Edited by Thinzar, Change_Request-20150901, ReqNo. 2
				isSuccess = lms.isAuthenSuccess(LMSInfo);

				if(isSuccess){
					lms.processSubServices(LMSInfo);
					
					//Added by Thinzar@20140813, Change_Request-20140801, ReqNo.3
					marketDepthinfos = lms.processSubFeaturesGetMktDepth(LMSInfo);
				// Added by Mary@20130925 - Fixes_Request-20130711, ReqNo.16 - START
				}else{
					//Added by Thinzar, Change_Request-20141101, ReqNo. 17 - START
					if(lms.getSubsAuthFailMsg() != null && lms.getSubsAuthFailMsg().trim().length() > 1){
						if(lms.getSubsAuthFailMsg().equalsIgnoreCase("null")){
							throw new Exception(ErrorCodeConstants.NO_LMS_SUBSCRIPTION);
						} 
					//Added by Thinzar, Change_Request-20141101, ReqNo. 17 - END
						else
							throw new Exception( lms.getSubsAuthFailMsg() );
					}
				}
				// Added by Mary@20130925 - Fixes_Request-20130711, ReqNo.16 - END

				infoFromLMS = lms.getSubscriptions();
				// Set LMS Inf
			/* Mary@20130823 - Fixes_Request-20130711, ReqNo.16
			} catch (FailedAuthenicationException e) {
			*/
			}catch(Exception e){
				e.printStackTrace();
				return e;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if(result != null){
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
				try{
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
				}catch(Exception e){
					e.printStackTrace();
				}
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
				
				LoginActivity.this.showMainThreadAlert(result);
				return;
			}

			String[] currencyFrmLMS	= new String[infoFromLMS.size()];
			String[] exchgCodeFrmLMS= new String[infoFromLMS.size()];
			String FeedUrl[] 		= new String[infoFromLMS.size()]; 
			String ExchgName[] 		= new String[infoFromLMS.size()];
			String TrdExchgCode[] 	= new String[infoFromLMS.size()];
			int[] marketdepthLvl    = new int[infoFromLMS.size()];
			
			if(DefinitionConstants.getValidQCExchangeCodes().size() != 0)
				DefinitionConstants.getValidQCExchangeCodes().clear();
			
			if(DefinitionConstants.getTrdExchangeCode().size() != 0)
				DefinitionConstants.getTrdExchangeCode().clear();
			
			// Added by Mary@20121217 - Remove Duplicate, remain the last IN - START
			Map<String, String> mapQCExchgTradeExchg= new HashMap<String, String>();
			List<String> lstRememberSequence		= new ArrayList<String>();
			// Added by Mary@20121217 - Remove Duplicate, remain the last IN - END
			
			AppConstants.hasEquityExchange = false;	//Added by Thinzar, Fixes_Request-20140820, ReqNo.14, need to reset to false before processing lmsSB
			for (int i = 0; i < infoFromLMS.size(); i++) { 
				LMSServiceBean lmsSB	= infoFromLMS.get(i);
				currencyFrmLMS[i]		= lmsSB.getCurrency();
				exchgCodeFrmLMS[i]		= lmsSB.getExchangeCode();
				FeedUrl[i]				= lmsSB.getFeedURL(); 
				/* Mary@20130430 - Fixes_Request-20130419, ReqNo.23
				ExchgName[i]			= lmsSB.getExchangeDescription();
				*/
				ExchgName[i]			= lmsSB.getExchangeName() + " (" + lmsSB.getExchangeCode() + ")";
				TrdExchgCode[i]			= lmsSB.getTradeExchangeCode();
				
				// Added by Kw@20130222 - Change_Request20130104, ReqNo.6
				
				marketdepthLvl[i] 		= lmsSB.getMarketDepthLvl();
				//Added by Thinzar@20140813, Change_Request-20140801, ReqNo.3
				if(marketDepthinfos.get(lmsSB.getExchangeCode()) != null){
					int marketDepthFromSubFeatures = marketDepthinfos.get(lmsSB.getExchangeCode());
					//if(marketDepthFromSubFeatures > marketdepthLvl[i])	//commented out by Thinzar@20160226, to cater for market depth 3 levels
					marketdepthLvl[i] = marketDepthFromSubFeatures;
				}
				//Change_Request-20140801, ReqNo.3 - END
				
				// Added by Kw@20130222 - Change_Request20130104, ReqNo.6
				AppConstants.addMarketDepthLvl(exchgCodeFrmLMS[i], marketdepthLvl[i]);
				// Added by Mary@20130620 - Change_Request-20130424,ReqNo.9
				AppConstants.addMapFCBitMode( exchgCodeFrmLMS[i], lmsSB.getStrFCBitMode() );
				
				AppConstants.setQCIP(exchgCodeFrmLMS[i], FeedUrl[i]); 
				// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3
				AppConstants.setQCPort(exchgCodeFrmLMS[i], FeedUrl[i]);
				DefinitionConstants.setExchangeName(exchgCodeFrmLMS[i], ExchgName[i]);
				/* Mary@20121217 - Remove Duplicate, remain the last IN - START
				DefinitionConstants.setTrdExchangeCode(TrdExchgCode[i]);
				DefinitionConstants.addValidQCExchangeCodes(exchgCodeFrmLMS[i]);
				*/
				if( ! lstRememberSequence.contains( exchgCodeFrmLMS[i] ) ){
					lstRememberSequence.add( exchgCodeFrmLMS[i] );
				}	
				// Mary@20121217 - Remove Duplicate, remain the last IN - END
				mapQCExchgTradeExchg.put(exchgCodeFrmLMS[i], TrdExchgCode[i]);
				DefinitionConstants.addCurrencyMapping(exchgCodeFrmLMS[i], currencyFrmLMS[i]);
				
				//Added by Thinzar@20140903 - Fixes_Request-20140820, ReqNo.14
				if(lmsSB.getExchangeID().equals("01"))	AppConstants.hasEquityExchange	= true;
			}
			
			// Added by Mary@20121217 - Remove Duplicate, remain the last IN - START
			for(Iterator<String> itr=lstRememberSequence.iterator(); itr.hasNext();){
				String strQCExchgCode	= itr.next();
				String strTrdExchgCode 	= mapQCExchgTradeExchg.get(strQCExchgCode);
				
				DefinitionConstants.addTrdExchangeCode(strTrdExchgCode);
				DefinitionConstants.addValidQCExchangeCodes(strQCExchgCode);
			}
			mapQCExchgTradeExchg.clear();
			mapQCExchgTradeExchg	= null;
			
			lstRememberSequence.clear();
			lstRememberSequence		= null;
			// Added by Mary@20121217 - Remove Duplicate, remain the last IN - END
			
			// Added by Mary@20130308 - Fixes_Request-20130108, ReqNo.21 - START
			if(DefinitionConstants.getTrdExchangeCode().size() == 0 && DefinitionConstants.getValidQCExchangeCodes().size() == 0){
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
				try{
					if( dialog != null && dialog.isShowing() )
						dialog.dismiss();
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
				}catch(Exception e){
					e.printStackTrace();
				}
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
				
				LoginActivity.this.showMainThreadAlert( new Exception(ErrorCodeConstants.NO_LMS_SUBSCRIPTION) );
				return;
			}
			// Added by Mary@20130308 - Fixes_Request-20130108, ReqNo.21 - END
			
			for( Iterator<ExchangeInfo> itr = exchangeInfos.iterator(); itr.hasNext(); ){
				ExchangeInfo exchgInfo	= itr.next();
				String strQCExchgCode	= exchgInfo.getQCExchangeCode();
				// Added by Mary@201211227 - Special Case - ATP missing configuration
				boolean isFound			= false;
				for(int i=0; i<DefinitionConstants.getValidQCExchangeCodes().size(); i++){
					if( strQCExchgCode.equals( DefinitionConstants.getValidQCExchangeCodes().get(i) ) ){
						exchgInfo.setTrdExchangeCode( DefinitionConstants.getTrdExchangeCode().get(i) );
						// Added by Mary@201211227 - Special Case - ATP missing configuration
						isFound	= true;
						break;
					}
				}
				// Added by Mary@201211227 - Special Case - ATP missing configuration - START
				if( ! isFound ){
					exchgInfo.setTrdExchangeCode(strQCExchgCode);
				}
				// Added by Mary@201211227 - Special Case - ATP missing configuration - END
			}

			new retrieveAccount().execute();
		}
	}

	private class retrieveAccount extends AsyncTask<Void, Void, Exception> {
		boolean isSenderCodeMatch	= true;	//Change_Request-20160101, ReqNo.1
		
		//Added by Thinzar @20140723 - Changes_Request-20140701 - Req. No 6 - START
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(dialog.isShowing())	dialog.setMessage(getResources().getString(R.string.login_step5));
		}
		//Added by Thinzar @20140723 - END
		
		@Override
		protected Exception doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Map<String, Object> parameters = new HashMap<String, Object>();

			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if(AppConstants.EnableEncryption)
			*/
			if (AppConstants.brokerConfigBean.isEnableEncryption())
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				byte[] userParamInBytes = AppConstants.userParamInByte;
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, userParamInBytes);
				*/
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, userParam);

			//parameters.put(ParamConstants.USERPARAM_TAG, userParam);
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			String response;
			/*
			 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START try {
			 * response = AtpConnectUtil.tradeClient(parameters); } catch
			 * (FailedAuthenicationException e) { e.printStackTrace(); return e;
			 * }
			 */
			try {
				response = AtpConnectUtil.tradeClient(parameters);
				while(LoginActivity.this.intRetryCtr < AppConstants.intMaxRetry && response == null) {
					LoginActivity.this.intRetryCtr++;
					response = AtpConnectUtil.tradeClient(parameters);
				}
				intRetryCtr = 0;

				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_TRADE_CLIENT);
			// Added by Mary@20130131 - while login shouldn't show session expired error message - START
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				return new IOException(ErrorCodeConstants.FAIL_AUTHENTICATION_IOE);
			// Added by Mary@20130131 - while login shouldn't show session expired error message - END
			} catch (Exception e) {
				return e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			tradeAccounts = AtpMessageParser.parseTradeClient(response);
			
			//Added by Thinzar, Change_Request-20160101, ReqNo.1 - START
			isSenderCodeMatch	= true;
			for (Iterator<TradeAccount> itr = tradeAccounts.iterator(); itr.hasNext(); ) {
				TradeAccount acc			= itr.next();
				//System.out.println("original sender code is:" + senderCode + ", TradeAccount senderCode is:" + acc.getSenderCode());
				if(acc.getSenderCode() != null && !acc.getSenderCode().equals(senderCode)){	//Edited by Thinzar, 20161025, some cimb_sg accounts doesn't return sender code
					isSenderCodeMatch	= false;
					break;
				}
			}
			
			if(!isSenderCodeMatch)	
				return new Exception(ErrorCodeConstants.SENDER_CODE_NOT_MATCH);
			//Added by Thinzar, Change_Request-20160101, ReqNo.1 - END

			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);

			if(result != null){
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
				try{
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
				}catch(Exception e){
					e.printStackTrace();
				}
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
			
				LoginActivity.this.showMainThreadAlert(result);
				return;
			} else{
				
			}

			// Added by Mary@20120822-Fixes_Request-20120815, ReqNo.2
			AppConstants.noTradeClient = false;
			if(tradeAccounts.size() == 0)
				AppConstants.noTradeClient = true;
			
			//Edited by Thinzar@20140902, Fixes_Request-20140820, ReqNo. 11	- START
			if(AppConstants.brokerConfigBean.isHasRestrictionToViewMY()){
				
				//This block of codes were previously commented out by Mary, due to Fixes_Request-20130108, ReqNo.15
				validQCCodes		= DefinitionConstants.getValidQCExchangeCodes();
				validTradeCodes 	= DefinitionConstants.getTrdExchangeCode();
				
				// Verify account supported Exchange has 'MY' Exchange Code ?
				boolean isMYExists 	= false;
				for (Iterator itr = tradeAccounts.iterator(); itr.hasNext();) {
					TradeAccount account = (TradeAccount) itr.next();
					//if(account.getSupportedExchange().contains("MY") )		//Change_Request-20141101, ReqNo 22
					if(account.getExchangeCode().contains("MY") )
						isMYExists = true;

					supportedTradeExchange = account.getSupportedExchange();
				}

				// Add non-exist supportedTradeExchange to QC Exchange Code global list
				if (supportedTradeExchange != null) {
					for (int k = 0; k < supportedTradeExchange.size(); k++) {
						if ( ! ( validTradeCodes.contains( supportedTradeExchange.get(k) ) ) )
							validTradeCodes.add( supportedTradeExchange.get(k) );
					}
				}

				// Remove 'MY' from QC & Trade Exchange Code global list, if trade accounts do not support this exchange
				if( ! isMYExists ){
					for (int i = 0; i < validQCCodes.size(); i++) {
						if( validQCCodes.get(i).equals("MY") ){
							validQCCodes.remove(i);
						}
					}
					
					for (int i = 0; i < validTradeCodes.size(); i++) {
						if(validTradeCodes.get(i).equals("MY") ){
							validTradeCodes.remove(i);
						}
					}
				}
				DefinitionConstants.setValidQCExchangeCodes(validQCCodes);
				DefinitionConstants.setTrdExchangeCode(validTradeCodes);
			} 
			//Edited by Thinzar@20140902, Fixes_Request-20140820, ReqNo. 11	- END
			
			// Get QC server IP
			/*
			 * Mary@20120822 - Fixes_Request-20120815, ReqNo.2 - START
			 * for(int k=0; k<DefinitionConstants.exchangeCodes.size(); k++){
			 * 	if(DefinitionConstants.exchangeCodes.get(k).equals(AppConstants.primaryExchangeCode)){
			 * 		AppConstants.QC_SERVER_IP = AppConstants.mapQCIp.get(strCurrExchCode); 
			 * 	}
			 * 
			 * 	if(DefinitionConstants.exchangeCodes.get(k).equals(AppConstants.secondaryExchangeCode)){ 
			 *		AppConstants.QC_SERVER_IP = AppConstants.mapQCIp.get(DefinitionConstants.exchangeCodes.get(k)); 
			 * 	} 
			 * }
			 */
			String strQCExchgCode		= AppConstants.getCachedBrokerUserDetail(LoginActivity.this, sharedPreferences, PreferenceConstants.EXCHANGE_CODE);
			AppConstants.QC_SERVER_IP 	= null;
			if(strQCExchgCode != null){
				AppConstants.QC_SERVER_IP 	= AppConstants.mapQCIp.get(strQCExchgCode);
				// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3
				AppConstants.QC_SERVER_PORT	= 80;
			}

			if (AppConstants.QC_SERVER_IP == null) {
				for (int k = 0; k < DefinitionConstants.getValidQCExchangeCodes().size(); k++) {
					String strCurrQCExchCode = DefinitionConstants.getValidQCExchangeCodes().get(k);

					/* Mary@20130115 - Change_Request-20130104, ReqNo.3
					if(strCurrQCExchCode.equals(AppConstants.primaryExchangeCode) || strCurrQCExchCode.equals(AppConstants.secondaryExchangeCode) ){
					*/
					if( strCurrQCExchCode.equals( AppConstants.brokerConfigBean.getStrBaseATPExchangeCode() ) || strCurrQCExchCode.equals( AppConstants.brokerConfigBean.getStrBaseQCExchangeCode() ) ){
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
						String strCurrTrdExchCode 					= DefinitionConstants.getTrdExchangeCode().get(k);
						
						AppConstants.QC_SERVER_IP					= AppConstants.mapQCIp.get(strCurrQCExchCode);
						// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3
						AppConstants.QC_SERVER_PORT					= AppConstants.mapQCPort.get(strCurrQCExchCode);
						Map<String, String> mapBrokerUserPreference	= new HashMap<String, String>();
						mapBrokerUserPreference.put(PreferenceConstants.EXCHANGE_CODE,	strCurrQCExchCode);
						mapBrokerUserPreference.put(PreferenceConstants.ACCOUNT_NO, null);
						mapBrokerUserPreference.put(PreferenceConstants.BRANCH_CODE, null);
						// Added by Mary@20121112 - Change_Request-20120719, ReqNo.12
						mapBrokerUserPreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, strCurrTrdExchCode);
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
						mapBrokerUserPreference.put(PreferenceConstants.TRADE_EXCHANGE_CODE, strCurrTrdExchCode);
						AppConstants.cacheBrokerUserDetails(LoginActivity.this, sharedPreferences, mapBrokerUserPreference);
						mapBrokerUserPreference.clear();
						mapBrokerUserPreference						= null;
						
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
						strCurrTrdExchCode							= null;
					}

					strCurrQCExchCode = null;
				}

				// Added by Mary@20130118 - avoid nullpointerexception when LMS does not provide broker base exchange - START
				if (AppConstants.QC_SERVER_IP == null) {
					String strCurrTrdExchCode	= DefinitionConstants.getTrdExchangeCode().get(0);
					String strCurrQCExchCode	= DefinitionConstants.getValidQCExchangeCodes().get(0);
					AppConstants.QC_SERVER_IP	= AppConstants.mapQCIp.get(strCurrQCExchCode);
					// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3
					AppConstants.QC_SERVER_PORT	= AppConstants.mapQCPort.get(strCurrQCExchCode);

					Map<String, String> mapBrokerUserPreference = new HashMap<String, String>();
					mapBrokerUserPreference.put(PreferenceConstants.EXCHANGE_CODE, strCurrQCExchCode);
					mapBrokerUserPreference.put(PreferenceConstants.ACCOUNT_NO,	null);
					mapBrokerUserPreference.put(PreferenceConstants.BRANCH_CODE, null);
					mapBrokerUserPreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, strCurrTrdExchCode);
					mapBrokerUserPreference.put(PreferenceConstants.TRADE_EXCHANGE_CODE, strCurrTrdExchCode);
					AppConstants.cacheBrokerUserDetails(LoginActivity.this,	sharedPreferences, mapBrokerUserPreference);
					mapBrokerUserPreference.clear();
					mapBrokerUserPreference = null;

					strCurrTrdExchCode = null;
					strCurrQCExchCode = null;
				}
				// Added by Mary@20130118 - avoid nullpointerexception when LMS does not provide broker base exchange - END
			}
			// Mary@20120822 - Fixes_Request-20120815, ReqNo.2 - END

			new loginQC().execute();
		}
	}

	private class loginQC extends AsyncTask<Void, Void, Exception> {

		private String userParamKey;
		/*
		 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 private String
		 * userParams; private String aliveTimeOut;
		 */
		private StringBuffer response;
		
		//Added by Thinzar @20140723 - Changes_Request-20140701 - Req. No 6 - START
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(dialog.isShowing())	
				dialog.setMessage(getResources().getString(R.string.login_step6));
		}
		//Added by Thinzar @20140723 - END
				
		@Override
		protected Exception doInBackground(Void... params) {
			// TODO Auto-generated method stub

			try {
				/* Mary@20130517 - Fixes_Request-20130424, ReqNo.9
				userParamKey = QcHttpConnectUtil.getKey();
				*/
				userParamKey = AppConstants.brokerConfigBean.isGetNewKey() ? QcHttpConnectUtil.getNewKey() : QcHttpConnectUtil.getKey();
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				while (intRetryCtr < AppConstants.intMaxRetry && userParamKey == null) {
					intRetryCtr++;
					/* Mary@20130517 - Fixes_Request-20130424, ReqNo.9
					userParamKey = QcHttpConnectUtil.getKey();
					*/
					userParamKey = AppConstants.brokerConfigBean.isGetNewKey() ? QcHttpConnectUtil.getNewKey() : QcHttpConnectUtil.getKey();
				}
				intRetryCtr = 0;

				if (userParamKey == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_QC_KEY);
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

				AppConstants.QCUserParamKey = userParamKey;

				/*
				 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START Map
				 * parameters = new HashMap();
				 * parameters.put(ParamConstants.USERPARAM_KEY_TAG,
				 * userParamKey);
				 * 
				 * response = new StringBuffer(25600);
				 * 
				 * List<String>data = QcHttpConnectUtil.login(parameters, response);
				 */
				response = new StringBuffer(25600);

				//MEL//QcHttpConnectUtil.USERNAME = LoginActivity.this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE).getString(PreferenceConstants.USER_NAME, null);
				QCBean data = QcHttpConnectUtil.login(userParamKey, response);
				while (intRetryCtr < AppConstants.intMaxRetry && data == null) {
					intRetryCtr++;
					data = QcHttpConnectUtil.login(userParamKey, response);
				}
				intRetryCtr = 0;

				if (data == null)
					throw new Exception(ErrorCodeConstants.FAIL_LOGIN_QC);
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

				AppConstants.QCdata = data;

				/*
				 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				 * userParams = data.get(1); aliveTimeOut = data.get(2); } catch
				 * (FailedAuthenicationException e) { // TODO Auto-generated
				 * catch block e.printStackTrace(); return e; }
				 */
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			return null;
		}

		@Override
		protected void onPostExecute(Exception exception) {
			// TODO Auto-generated method stub
			super.onPostExecute(exception);
			if (exception != null) {
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
				try{
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
				}catch(Exception e){
					e.printStackTrace();
				}
				// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
			
				LoginActivity.this.showMainThreadAlert(exception);
				return;
			}

			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			AppConstants.hasLogout	= false;
			
			String[] symbolCodes 	= new String[0];

			int rate = sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 
			 refreshThread = new RefreshStockThread(userParams, symbolCodes, rate);
			*/
			refreshThread = new RefreshStockThread(AppConstants.QCdata.getStrUserParam(), symbolCodes, rate);
			refreshThread.start();
			refreshThread.setRegisteredListener(LoginActivity.this);
			((StockApplication) LoginActivity.this.getApplication()).setRefreshThread(refreshThread);
			
		//	OTPvalidationThread otpThread = new OTPvalidationThread();
			((StockApplication) LoginActivity.this.getApplication()).setOTPvalidationThread(new OTPvalidationThread());

			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 
			double QCAliverate = Double.parseDouble(aliveTimeOut);
			*/
			double QCAliverate 				= AppConstants.QCdata.getDblAliveTimeOut();
			QCAliverate 					= Math.floor(QCAliverate * 0.75);
			AppConstants.QC_KEEP_ALIVE_RATE = (int) QCAliverate;

			/*
			 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 QCAlive = new
			 * QCAliveThread(userParams, AppConstants.QC_KEEP_ALIVE_RATE);
			 */
			QCAlive = new QCAliveThread(AppConstants.QCdata.getStrUserParam(), AppConstants.QC_KEEP_ALIVE_RATE);
			QCAlive.start();
			QCAlive.setRegisteredListener(LoginActivity.this);
			( (StockApplication) LoginActivity.this.getApplication() ).setQCAliveThread(QCAlive);

			// SharedPreferences.Editor editor= sharedPreferences.edit();
			/*
			 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			 * editor.putString(PreferenceConstants.QC_USER_PARAM, userParams);
			 * editor.commit();
			 */
			
			String result 						= response.toString();
			exchangeSectors 					= QcMessageParser.parseExchangeSector(result);
			/* Mary@20130527 - comment unused variable
			DefinitionConstants.mapExchangeSector= exchangeSectors;
			*/
			validExchanges 						= new ArrayList<ExchangeInfo>();
			for (Iterator<ExchangeInfo> itr = exchangeInfos.iterator(); itr.hasNext();) {
				ExchangeInfo info = itr.next();
				// Added by Mary@20130502 - Fixes_Request-20130424, ReqNo.2
				info.setExchnageName( DefinitionConstants.getExchangeNameMapping().get( info.getQCExchangeCode() ) );
				
				/* Mary@20130221 - Fixes_Request-20130108, ReqNo.15
				if( info.getTrdExchgCode() != null && validTradeCodes.contains( info.getTrdExchgCode() ) )
				*/
				if( info.getTrdExchgCode() != null && DefinitionConstants.getTrdExchangeCode().contains( info.getTrdExchgCode() ) )
					validExchanges.add(info);
			}
			
			/* Special Case - ATP missing configuration - START */
			for(int i=0; i<DefinitionConstants.getValidQCExchangeCodes().size(); i++){
				String strQCExchgCode	= DefinitionConstants.getValidQCExchangeCodes().get(i);
				boolean isFound			= false;
				for (Iterator<ExchangeInfo> itr = validExchanges.iterator(); itr.hasNext();) {
					ExchangeInfo info = itr.next();
					if( strQCExchgCode.equals( info.getQCExchangeCode() ) ){
						isFound	= true;
						break;
					}
				}
				
				if( ! isFound ){
					ExchangeInfo info	= new ExchangeInfo();
					info.setQCExchangeCode( strQCExchgCode );
					info.setTrdExchangeCode( DefinitionConstants.getTrdExchangeCode().get(i) );
					
					List<String> lst	= new ArrayList<String>();
					lst.add( DefinitionConstants.getCurrencyMapping().get(strQCExchgCode) );
					info.setCurrencies(lst);
					
					info.setExchnageName( DefinitionConstants.getExchangeNameMapping().get( strQCExchgCode) );
					
					validExchanges.add(info);
				}
			}
			/* Special Case - ATP missing configuration - END */

			// Populate the selected exchange code and trade account for the user.
			// To default the selected exchange or trade account based on 2 criteria
			// 1. The user last selected exchange or trade account
			// 2. First time login, default the first exchange and the first trade account valid for the first exchange.
			StockApplication application = (StockApplication) LoginActivity.this.getApplication();
			application.setTradeAccounts(tradeAccounts);
			application.setExchangeInfos(validExchanges);
			// Added by Mary@20121228-Fixes_Request-20121106, ReqNo.11
			application.setCurrencyExchangeInfos(currencyExchangeInfos);
			application.setRememberPin(false);
			// Added by Mary@20121009 - Fixes_Request-20120724, ReqNo.7
			application.setSkipConfirmation(false);

			/*
			 * Mary@20120814 - Change_Request-20120719, ReqNo.4 - START String
			 * exchangeCode = sharedPreferences.getString(PreferenceConstants.EXCHANGE_CODE, null); 
			 * String accountNo = sharedPreferences.getString(PreferenceConstants.ACCOUNT_NO, null); String branchCode =
			 * sharedPreferences.getString(PreferenceConstants.BRANCH_CODE, null);
			 */
			String strQCExchangeCode 			= AppConstants.getCachedBrokerUserDetail(LoginActivity.this, sharedPreferences, PreferenceConstants.EXCHANGE_CODE);
			String accountNo 					= AppConstants.getCachedBrokerUserDetail(LoginActivity.this, sharedPreferences, PreferenceConstants.ACCOUNT_NO);
			String branchCode 					= AppConstants.getCachedBrokerUserDetail(LoginActivity.this, sharedPreferences, PreferenceConstants.BRANCH_CODE);
			// Mary@20120814 - Change_Request-20120719, ReqNo.4 - END

			if (strQCExchangeCode != null) {
				boolean isExchangeCodeExists = false;
				
				// String strTrdExchgCode = strQCExchangeCode.substring(0, 2);
				for (Iterator<ExchangeInfo> itr = validExchanges.iterator(); itr.hasNext();) {
					ExchangeInfo exchange = itr.next();
					
					if( strQCExchangeCode.equals( exchange.getQCExchangeCode() ) ){
						isExchangeCodeExists				= true;
						application.setSelectedExchange( exchange );
						application.setSelectedExchangeCode( exchange.getTrdExchgCode() );
						// AppConstants.DEFAULT_EXCHANGE_CODE	= exchangeCode;
						AppConstants.setDefaultExchangeCode(strQCExchangeCode);
						// Added by Mary@20121112 - Change_Request-20121112, ReqNo.12 - START
						Map<String, String> mapTradePreference	= new HashMap<String, String>();
						mapTradePreference.put(PreferenceConstants.TRADE_EXCHANGE_CODE, exchange.getTrdExchgCode() );
						mapTradePreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, exchange.getTrdExchgCode() );
						AppConstants.cacheBrokerUserDetails(LoginActivity.this, sharedPreferences, mapTradePreference);
						mapTradePreference.clear();
						mapTradePreference		= null;
						// Added by Mary@20121112 - Change_Request-20121112, ReqNo.12 - END
						
						/* Mary@20130220 - Fixes_Request-20130108, ReqNo.10
						List accounts						= application.getTradeAccounts( exchange.getTrdExchgCode() );
						*/
						List<TradeAccount> accounts						= application.getTradeAccounts();
						boolean hasSet 						= false;
						if (accountNo != null) {
							for (Iterator<TradeAccount> itr2 = accounts.iterator(); itr2.hasNext();) {
								TradeAccount account = itr2.next();
								if( accountNo.equals( account.getAccountNo() ) ){
									application.setSelectedAccount(account);
									hasSet = true;
									break;
								}
							}
						}

						if(!hasSet && accounts.size() > 0)
							application.setSelectedAccount( (TradeAccount) accounts.get(0) );
						
						// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2
						AppConstants.noTradeClient = (accounts.size() > 0) ? false : true;

						break;
					}
				}
				
				if(!isExchangeCodeExists){
					ExchangeInfo exchange	= validExchanges.get(0);
					application.setSelectedExchange(exchange);
					application.setSelectedExchangeCode( exchange.getTrdExchgCode() );
					AppConstants.setDefaultExchangeCode( exchange.getQCExchangeCode() );
					// Added by Mary@20121112 - Change_Request-20121112, ReqNo.12 - START
					Map<String, String> mapTradePreference	= new HashMap<String, String>();
					mapTradePreference.put(PreferenceConstants.TRADE_EXCHANGE_CODE, exchange.getTrdExchgCode() );
					mapTradePreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, exchange.getTrdExchgCode());
					AppConstants.cacheBrokerUserDetails(LoginActivity.this, sharedPreferences, mapTradePreference);
					mapTradePreference.clear();
					mapTradePreference		= null;
					// Added by Mary@20121112 - Change_Request-20121112, ReqNo.12 - END
					
					/* Mary@20130220 - Fixes_Request-20130108, ReqNo.10
					List accounts			= application.getTradeAccounts( exchange.getTrdExchgCode() );
					*/
					List<TradeAccount> accounts	= application.getTradeAccounts();
					boolean hasSet				= false;
					if(accountNo != null){
						for (Iterator<TradeAccount> itr2 = accounts.iterator(); itr2.hasNext();) {
							TradeAccount account = itr2.next();
							if( accountNo.equals( account.getAccountNo() ) ){
								application.setSelectedAccount(account);
								hasSet = true;
								break;
							}
						}
					}
					if (!hasSet && accounts.size() > 0)
						application.setSelectedAccount( (TradeAccount) accounts.get(0) );
					
					// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2
					AppConstants.noTradeClient = (accounts.size() > 0) ? false : true;
				}
			}

			if (strQCExchangeCode == null || application.getSelectedExchange() == null) {
				ExchangeInfo exchange 	= null;
				String strTrdExchCode	= null; 

				for (int k = 0; k < DefinitionConstants.getValidQCExchangeCodes().size(); k++) {
					/*
					 * Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
					 * if
					 * (DefinitionConstants.exchangeCodes.get(k).equals(AppConstants
					 * .primaryExchangeCode)){
					 * AppConstants.DEFAULT_EXCHANGE_CODE =
					 * DefinitionConstants.exchangeCodes.get(k);
					 * 
					 * for(Iterator itr = validExchanges.iterator();
					 * itr.hasNext();){ ExchangeInfo exchanges = (ExchangeInfo)
					 * itr.next();
					 * if(exchanges.getExchangeCode().equals(DefinitionConstants
					 * .tradeExchange.get(k))){ exchange = exchanges;
					 * exchangeCode = exchange.getExchangeCode(); break; } }
					 * 
					 * application.setSelectedExchange(exchange);
					 * application.setSelectedExchangeCode(exchangeCode);
					 * 
					 * break; }
					 * 
					 * if(DefinitionConstants.exchangeCodes.get(k).equals(
					 * AppConstants.secondaryExchangeCode)){
					 * AppConstants.DEFAULT_EXCHANGE_CODE =
					 * DefinitionConstants.exchangeCodes.get(k);
					 * 
					 * for(Iterator itr = validExchanges.iterator();
					 * itr.hasNext();){ ExchangeInfo exchanges = (ExchangeInfo)
					 * itr.next();
					 * if(exchanges.getExchangeCode().equals(DefinitionConstants
					 * .tradeExchange.get(k))){ exchange = exchanges;
					 * exchangeCode = exchange.getExchangeCode(); break; } }
					 * 
					 * application.setSelectedExchange(exchange);
					 * application.setSelectedExchangeCode(exchangeCode);
					 * 
					 * break; }
					 */
					String strCurrQCExchCode = DefinitionConstants.getValidQCExchangeCodes().get(k);
					/* Mary@20130115 - Change_Request-20130104, ReqNo.3
					if (strCurrQCExchCode.equals(AppConstants.primaryExchangeCode) || strCurrQCExchCode.equals(AppConstants.secondaryExchangeCode)) {
					*/
					if( strCurrQCExchCode.equals( AppConstants.brokerConfigBean.getStrBaseATPExchangeCode() ) || strCurrQCExchCode.equals( AppConstants.brokerConfigBean.getStrBaseQCExchangeCode() ) ){
						// AppConstants.DEFAULT_EXCHANGE_CODE = strCurrQCExchCode;
						AppConstants.setDefaultExchangeCode( strCurrQCExchCode );
						
						for (Iterator<ExchangeInfo> itr = validExchanges.iterator(); itr.hasNext();) {
							ExchangeInfo exchanges = itr.next();
							if (exchanges.getQCExchangeCode().equals(strCurrQCExchCode)) {
								exchange		= exchanges;
								strTrdExchCode	= exchange.getTrdExchgCode();
								break;
							}
						}

						application.setSelectedExchange(exchange);
						application.setSelectedExchangeCode(strTrdExchCode);
						
						// Added by Mary@20121112 - Change_Request-20121112, ReqNo.12 - START
						Map<String, String> mapTradePreference		= new HashMap<String, String>();
						mapTradePreference.put(PreferenceConstants.TRADE_EXCHANGE_CODE, strTrdExchCode);
						mapTradePreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, strTrdExchCode);
						AppConstants.cacheBrokerUserDetails(LoginActivity.this, sharedPreferences, mapTradePreference);
						mapTradePreference.clear();
						mapTradePreference			= null;
						// Added by Mary@20121112 - Change_Request-20121112, ReqNo.12 - END

						break;
					}

					strCurrQCExchCode = null;
					// Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
				}

				// Sector sector = (Sector) exchange.getSectors().get(0);
				/* Mary@20130220 - Fixes_Request-20130108, ReqNo.10
				List accounts = application.getTradeAccounts(strTrdExchCode);
				*/
				List<TradeAccount> accounts = application.getTradeAccounts();
				boolean hasSet = false;
				if (accountNo != null) {
					for (Iterator<TradeAccount> itr2 = accounts.iterator(); itr2.hasNext();) {
						TradeAccount account = itr2.next();
						if( accountNo.equals( account.getAccountNo() ) && branchCode.equals( account.getBranchCode() ) ){
							application.setSelectedAccount(account);
							hasSet = true;
							break;
						}
					}
				}

				if (!hasSet && accounts.size() > 0)
					application.setSelectedAccount( (TradeAccount) accounts.get(0) );
					
				// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2
				AppConstants.noTradeClient = (accounts.size() > 0) ? false : true;
				
				/*
				 * Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
				 * editor = sharedPreferences.edit();
				 * editor.putString(PreferenceConstants.EXCHANGE_CODE,
				 * exchangeCode); if (application.getSelectedAccount() != null)
				 * { editor.putString(PreferenceConstants.ACCOUNT_NO,
				 * application.getSelectedAccount().getAccountNo());
				 * editor.putString(PreferenceConstants.BRANCH_CODE,
				 * application.getSelectedAccount().getBranchCode()); }
				 * editor.commit();
				 */
				Map<String, String> mapBrokerUserPreference	= new HashMap<String, String>();
				mapBrokerUserPreference.put( PreferenceConstants.EXCHANGE_CODE, AppConstants.DEFAULT_EXCHANGE_CODE );
				mapBrokerUserPreference.put( PreferenceConstants.ACCOUNT_NO, application.getSelectedAccount().getAccountNo() );
				mapBrokerUserPreference.put( PreferenceConstants.BRANCH_CODE, application.getSelectedAccount().getBranchCode() );
				// Added by Mary@20121112 - Change_Request-20120719, ReqNo.12
				mapBrokerUserPreference.put( PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, application.getSelectedExchangeCode() );
				// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
				mapBrokerUserPreference.put( PreferenceConstants.TRADE_EXCHANGE_CODE, application.getSelectedExchangeCode() );
				AppConstants.cacheBrokerUserDetails(LoginActivity.this,	sharedPreferences, mapBrokerUserPreference);
				mapBrokerUserPreference.clear();
				mapBrokerUserPreference						= null;
				// Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
			}

			LoginActivity.this.populateExchangeSector(validExchanges);

			editor.putBoolean(PreferenceConstants.IS_APP_EXIT, false);
			editor.commit();

			doubleLoginThread = new DoubleLoginThread(username, userParam);
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if(AppConstants.EnableEncryption)
			*/
			if (AppConstants.brokerConfigBean.isEnableEncryption())
				doubleLoginThread.setUserParam(AppConstants.userParamInByte);
			
			doubleLoginThread.start();
			doubleLoginThread.setRegisteredListener(LoginActivity.this);
			((StockApplication) LoginActivity.this.getApplication()).setDoubleLoginThread(doubleLoginThread);
			
			// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12
			AppConstants.updateTradePreferenceAccordanceToCurrentExchange(LoginActivity.this, sharedPreferences);	
			
			// Added by Mary@20121123 - Changes_Request-20121106, ReqNo.2
			String strTemp				= AppConstants.getCachedBrokerUserDetail(LoginActivity.this, sharedPreferences, PreferenceConstants.VIEW_QUANTITY_MEASUREMENT);
			if(strTemp!=null && strTemp.trim().length() > 0)
				AppConstants.brokerConfigBean.setIntViewQtyMeasurement( Integer.parseInt(strTemp) );
			
			
			//if (AppConstants.FLAG_DMA) {	//Edited by Thinzar@20140827, Change_Request-20140801, ReqNo. 6, commented out this line and replace with the line below
			if(AppConstants.brokerConfigBean.isShowDMAAgreement()){
				if (AppConstants.TNCInfo != null) {
					if (AppConstants.TNCInfo.equals("Y")) {
						Intent intent = new Intent();
						intent.setClass(LoginActivity.this, DMAActivity.class);
						intent.putExtra(ParamConstants.USERNAME_TAG, username);
						intent.putExtra(ParamConstants.USERPARAM_TAG, userParam);
						intent.putExtra(ParamConstants.SENDERCODE_TAG, senderCode);
						LoginActivity.this.startActivity(intent);
					} else {
						startQuoteActivity();
					}
				} else {
					startQuoteActivity();
				}
			} else {
				startQuoteActivity();
			}

			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				if(dialog != null && dialog.isShowing() )
					dialog.dismiss();
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
			
			// Added by Mary@020130527 - Fixes_Request-20130523, ReqNo.1 - START
			if( ! isRememberMe )
				nameField.setText("");
			// Added by Mary@020130527 - Fixes_Request-20130523, ReqNo.1 - END
		}
	}
	
	private void startQuoteActivity(){
		Intent intent = new Intent();
		intent.putExtra(ParamConstants.FROM_LOGIN_TAG, true);
		
		intent.setClass(LoginActivity.this, QuoteActivity.class);
		LoginActivity.this.startActivity(intent);
	}

	private void populateExchangeSector(List<ExchangeInfo> exchanges) {
		for (Iterator<ExchangeInfo> itr=exchanges.iterator(); itr.hasNext(); ) {
			ExchangeInfo exchange 		= itr.next();
			/* Mary@20130220 - Fixes_Request-20130108, ReqNo.13
			String exchangeCode 		= AppConstants.DEFAULT_EXCHANGE_CODE;
			*/
			String exchangeCode			= exchange.getQCExchangeCode();
			ExchangeInfo sectorExchgInfo= exchangeSectors.get(exchangeCode);
			if (sectorExchgInfo != null) {
				exchange.setSectors( sectorExchgInfo.getSectors() );
				exchange.setIndiceSymbol( sectorExchgInfo.getIndiceSymbol() );
			}
		}
	}

	/* Mary@20130605 - comment unused method
	private final boolean checkLoginInfo() {
		boolean isNameSet		= sharedPreferences.contains("username");
		boolean isPasswordSet	= sharedPreferences.contains("password"); 
		if( isNameSet || isPasswordSet )
			return true;

		return false;
	}
	*/

	private boolean isNetworkAvailable() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		// TODO Auto-generated method stub

	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub

	}

	@Override
	public void QCAliveEvent(boolean isResume) {
		// TODO Auto-generated method stub

	}
	
	private String getPlistFileVersion(String pkgName){
		String mVersion;
		
		mVersion = sharedPreferences.getString(pkgName, "0");
		
		//to prevent file from being deleted by user
		if(!mVersion.equals("0")){
			String filename = PlistFileUtil.getPlistFileNameFormat(pkgName);
			File file = new File(plistPath, filename);
			
			if(!file.exists())	mVersion = "0";
		}
		
		return mVersion;
	}
	
	//check plist file in plist path, if not exists copy from assets
	public boolean checkPlistFileInStorage(String mPkgName){
		DefinitionConstants.Debug("checkPlistFileInStorage: " + mPkgName);
		String plistFileName = PlistFileUtil.getPlistFileNameFormat(mPkgName);
		File plistFile = new File(plistPath, plistFileName);
			
		if (!plistFile.exists()) {

			try {
				if (Arrays.asList(getResources().getAssets().list(PlistFileUtil.ASSETS_PLIST_FOLDER_NAME))
						.contains(plistFileName)) {
					PlistFileUtil.copyAssets(LoginActivity.this, PlistFileUtil.ASSETS_PLIST_FOLDER_NAME + "/" + plistFileName, plistPath + "/" + plistFileName);
					return true;
				} else {
					return false;
				}
			} catch (IOException e) {
				// 
				e.printStackTrace();
				return false;
			}
		} else{
			return true;
		}
		
	}
	/* Added by Thinzar@20140701 - Change_Request-20140701,ReqNo.1 - END */
	
	// Added by Diyana, Change_Request_20160624, ReqNo.1  -START
	private void showChangeLog(){
		Button btnVersion =(Button)findViewById(R.id.btnVersion);

		btnVersion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				LayoutInflater inflater= LayoutInflater.from(getApplicationContext());
				View view=inflater.inflate(R.layout.changelog_alert, null);

				TextView textview=(TextView)view.findViewById(R.id.textmsg);
				textview.setText(loadChangeLog());

				AlertDialog alert = new AlertDialog.Builder(LoginActivity.this)
				.setView(view)
				.setTitle(getResources().getString(R.string.login_change_log_title) + " " + getResources().getString(R.string.version_tag))
				//.setIcon(android.R.attr.alertDialogStyle)	//commented by Thinzar, this line is giving ResourceNotFound Exception
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						try{
							dialog.dismiss();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				})
				.create();
				alert.show();
			}
		});
	}
	
	private String loadChangeLog(){
		// To load text file
        InputStream input;
        String text="";
        try {
        	AssetManager assetManager = getAssets();
            input = assetManager.open("changelog.txt");
 
             int size = input.available();
             byte[] buffer = new byte[size];
             input.read(buffer);
             input.close();
 
              text = new String(buffer);
 
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
 
	}
	// Added by Diyana, Change_Request_20160624, ReqNo.1  -END

	//Added by Thinzar, Change_Request-20160722, ReqNo.9 - START
	private void prepareForgetPasswordLink(){
		forgetPasswordLink 	= (TextView)findViewById(R.id.forgetPasswordLabel);
		forgetPasswordLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent();
				// Added by Mary@20130104 - additional information to ease user process
				intent.putExtra( ParamConstants.USERNAME_TAG, nameField.getText().toString() );
				intent.setClass(LoginActivity.this,	ForgetPasswordActivity.class);
				LoginActivity.this.startActivity(intent);

			}
		});
	}

	private class MyPagerAdapter extends PagerAdapter {
		private int PAGE_COUNT	= 2;

		public void setPageCount(int count){
			PAGE_COUNT	= count;
		}

		public int getCount() {
			return PAGE_COUNT;
		}

		public Object instantiateItem(View collection, int position) {
			((ViewPager) collection).addView(mView.get(position), 0);

			switch (position) {

				case 0:
					showLoginScreen(mView.get(position));

					break;
				case 1:
					prepareTouchIDScreen(mView.get(position));

					break;
			}

			return mView.get(position);
		}

		@Override
		public void destroyItem(View v, int arg1, Object arg2) {
			((ViewPager) v).removeView(mView.get(arg1));
		}

		@Override
		public boolean isViewFromObject(View v, Object arg1) {
			return v == ((View) arg1);
		}
	}

	private void prepareTouchIDScreen(View v){

		txt_touchid_terms	= (TextView)v.findViewById(R.id.txt_touchid_terms);
		btnActivateTouchID	= (Button)v.findViewById(R.id.btnActivateTouchID);
		touchid_title		= (TextView)v.findViewById(R.id.touchid_title);
		txt_touchid_info	= (TextView)v.findViewById(R.id.txt_touchid_info);
		btn_fingerprint		= (ImageButton)v.findViewById(R.id.btn_fingerprint);

		resetTouchIDPager();

		txt_touchid_terms.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				loadTermsActivity(!isTouchIDActivated);
			}

		});

		btnActivateTouchID.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(checkForFingerprint()){
					loadTermsActivity(!isTouchIDActivated);
				}
			}
		});

		btn_fingerprint.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {

				if(isTouchIDActivated && checkForFingerprint()){
					authType = FINGERPRINT_LOGIN_AUTH;

					try {
						mKeyStore = KeyStore.getInstance("AndroidKeyStore");
					} catch (KeyStoreException e) {
						throw new RuntimeException("Failed to get an instance of KeyStore", e);
					}
					try {
						mKeyGenerator = KeyGenerator
								.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
					} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
						throw new RuntimeException("Failed to get an instance of KeyGenerator", e);
					}

					//do decrypt here
					if (sharedPreferences.getString(PreferenceConstants.KEY_PASSWORD, "") != "") {
						if (initDecryptCipher()) {
							FingerprintDecryptDialogFragment mFragment = new FingerprintDecryptDialogFragment();

							mFragment.setDecryptCipher(mDecryptCipher);
							mFragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
							return;
						}
					}

				}else{
					DefinitionConstants.Debug("Activate TouchID First!");
				}
			}
		});
	}

	private boolean checkForFingerprint(){

		if (!mKeyguardManager.isKeyguardSecure()) {
			String errorMsg	= "You need to enable lock screen and fingerprint first in Settings";
			showAlertMessageAndGoToSettings(errorMsg);

			return false;
		} else if (!mFingerprintManager.hasEnrolledFingerprints()) {

			// This happens when no fingerprints are registered.
			String errorMsg	= "Register at least one fingerprint in Settings";
			showAlertMessageAndGoToSettings(errorMsg);

			return false;
		} else
			return true;
	}

	private void showAlertMessageAndGoToSettings(String msg){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(msg).setCancelable(false)
				.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						startActivityForResult(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS), 0);
					}
				}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});

		AlertDialog alert = builder.create();
		if (!this.isFinishing())
			alert.show();
	}

	private void loadTermsActivity(boolean isProceedTouchIDActivation){
		Intent termsIntent	= new Intent();
		termsIntent.setClass(LoginActivity.this, TermsActivity.class);
		termsIntent.putExtra(ParamConstants.FROM_LOGIN_TAG, true);
		termsIntent.putExtra(ParamConstants.IS_PROCEED_TOUCHID_TAG, isProceedTouchIDActivation);
		if(isProceedTouchIDActivation)
			termsIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

		startActivity(termsIntent);
	}

	private void preparePagerUnderline(int position){

		switch(position){

			case NORMAL_LOGIN_POS:
				normalLoginPagerUnderline.setBackgroundResource(R.color.secondary_theme_color);
				touchIDPagerUnderline.setBackgroundResource(R.color.color_pager_title);
				txtLoginPagerTitle.setTextColor(getResources().getColor(android.R.color.black));
				txtTouchIDPagerTitle.setTextColor(getResources().getColor(R.color.color_pager_title));

				break;
			case TOUCHID_LOGIN_POS:
				normalLoginPagerUnderline.setBackgroundResource(R.color.color_pager_title);
				touchIDPagerUnderline.setBackgroundResource(R.color.secondary_theme_color);
				txtLoginPagerTitle.setTextColor(getResources().getColor(R.color.color_pager_title));
				txtTouchIDPagerTitle.setTextColor(getResources().getColor(android.R.color.black));

				break;
		}
	}

	public boolean initDecryptCipher() {
		mDecryptCipher = getCipher(Cipher.DECRYPT_MODE);
		return (mDecryptCipher != null);
	}

	public void onDecryptAuthenSuccess(String passwd) {

		if(authType == FINGERPRINT_LOGIN_AUTH){
			String savedUserName		= sharedPreferences.getString(PreferenceConstants.KEY_USERNAME, "");
			String decryptedPassword	= passwd;

			//start tradeLogin process
			this.username 		= savedUserName;
			this.password 		= decryptedPassword;
			this.loginType		= "B";
			AppConstants.isLoginWithTouchID	= true;

			if ( AppConstants.brokerConfigBean.isEnableE2EEEncryption() )
				new E2EEELogin().execute(password);
			else
				new ATPLogin().execute();
		}
	}

	/**
	 * Tries to decrypt some data with the generated key in {@link #createKey} which is
	 * only works if the user has just authenticated via fingerprint.
	 */
	public String tryDecrypt(String keyName) {
		try {

			byte[] encodedData = Base64.decode(sharedPreferences.getString(keyName, ""), Base64.DEFAULT);
			byte[] decodedData = mDecryptCipher.doFinal(encodedData);
			return new String(decodedData);

		} catch (BadPaddingException | IllegalBlockSizeException e) {
			Toast.makeText(this, "Failed to decrypt the data with the generated key. "
					+ "Retry the purchase", Toast.LENGTH_LONG).show();
			Log.e(TAG, "Failed to decrypt the data with the generated key." + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}


	private SecretKey getKey() {
		try {
			mKeyStore.load(null);
			SecretKey key = (SecretKey) mKeyStore.getKey(PreferenceConstants.KEY_NAME, null);
			if (key != null) return key;
			return createKey();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return null;
	}

	@TargetApi(23)
	@SuppressLint("NewApi")
	private SecretKey createKey() {
		try {

			// Set the alias of the entry in Android KeyStore where the key will appear and the constrains (purposes) in the constructor of the Builder
			mKeyGenerator.init(new KeyGenParameterSpec.Builder(PreferenceConstants.KEY_NAME,
					KeyProperties.PURPOSE_DECRYPT | KeyProperties.PURPOSE_ENCRYPT)
					.setBlockModes(KeyProperties.BLOCK_MODE_CBC)		 // Require the user to authenticate with a fingerprint to authorize every use of the key
					.setUserAuthenticationRequired(true)
					.setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
					.build());
			return mKeyGenerator.generateKey();

		} catch (Exception e) {

		}
		return null;
	}

	public Cipher getCipher(int mode) {
		Cipher cipher;

		try {
			mKeyStore.load(null);
			byte[] iv;
			cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
					+ KeyProperties.BLOCK_MODE_CBC + "/"
					+ KeyProperties.ENCRYPTION_PADDING_PKCS7);
			IvParameterSpec ivParams;
			if (mode == Cipher.ENCRYPT_MODE) {
				cipher.init(mode, getKey());

			} else {
				SecretKey key = (SecretKey) mKeyStore.getKey(PreferenceConstants.KEY_NAME, null);
				iv = Base64.decode(sharedPreferences.getString(PreferenceConstants.KEY_PASSWORD_IV, ""), Base64.DEFAULT);
				ivParams = new IvParameterSpec(iv);
				cipher.init(mode, key, ivParams);
			}
			return cipher;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			showInvalidKeyMessageAndResetUI();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void showInvalidKeyMessageAndResetUI() {

		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Registered fingerprints have been changed. Please re-activate TouchID!")
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									//reset pager
									SharedPreferences.Editor editor 		= sharedPreferences.edit();
									editor.remove(PreferenceConstants.IS_PRE_LOGIN_VALIDATED);
									editor.commit();

									resetTouchIDPager();
								}
							});

			AlertDialog alert = builder.create();
			if (!this.isFinishing())
				alert.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void resetTouchIDPager(){
		isTouchIDActivated		= sharedPreferences.getBoolean(PreferenceConstants.IS_PRE_LOGIN_VALIDATED, false);

		togglePagerTitles();

		if(isTouchIDActivated){
			touchid_title.setText(getResources().getString(R.string.touchid_title_touch));
			btnActivateTouchID.setVisibility(View.GONE);
			txt_touchid_info.setVisibility(View.GONE);
			txt_touchid_terms.setVisibility(View.VISIBLE);
		}else{
			touchid_title.setText(getResources().getString(R.string.touchid_title_unlock));
			btnActivateTouchID.setVisibility(View.VISIBLE);
			txt_touchid_info.setVisibility(View.VISIBLE);
			txt_touchid_terms.setVisibility(View.GONE);
		}
	}

	private void togglePagerTitles(){
		if(isTouchIDActivated){
			txtBlank.setVisibility(View.GONE);
			txtNew.setVisibility(View.GONE);

			txtTouchIDPagerTitle.setText(getResources().getString(R.string.pager_title_login_touchid));
		}else{
			txtBlank.setVisibility(View.VISIBLE);
			txtNew.setVisibility(View.VISIBLE);

			txtTouchIDPagerTitle.setText(getResources().getString(R.string.pager_title_activate_touchid));
		}
	}

	public void showLoginScreen(View mView) {

		nameField		= (EditText)mView.findViewById(R.id.nameField);
		passwordField 	= (EditText)mView.findViewById(R.id.passwordField);
		loginButton 	= (TextView)mView.findViewById(R.id.loginButton);
		announcementButton = (Button)mView.findViewById(R.id.announcementButton);
		nameField.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS); // to disable autocorrect in textfield

		// Added by Mary@20130828 - ChangeRequest-20120719, ReqNo.4
		btnBrokerHouse			= (LinearLayout)mView.findViewById(R.id.btnBrokerHouse);
		txtSelectedBrokerName	= (TextView)mView.findViewById(R.id.txtSelectedBrokerName);
		imgLoginBottomText		= (ImageView)mView.findViewById(R.id.imgLoginBottomText);

		// Added by Mary@20130528 - Change_Request-20130424, ReqNo 4
		cbRememberMe	= (CheckBox)mView.findViewById(R.id.cbRememberMe);

		// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - START

		registerLink 		= (TextView)mView.findViewById(R.id.registerLabel);
		termLink 			= (TextView)mView.findViewById(R.id.termLabel);
		secondaryTermLink	= (TextView)mView.findViewById(R.id.seconary_term_link);		//Change_Request-20160722, ReqNo. 6
		importantNoteLink 	= (TextView)mView.findViewById(R.id.tv_important_note);
		emailLink			= (TextView) mView.findViewById(R.id.emailLabel); 				//Change_Request-20170601, ReqNo.16
		callLink			= (LinearLayout) mView.findViewById(R.id.ll_call_us); 			//Change_Request-20170601, ReqNo.16
		llTopSpace			= (LinearLayout) mView.findViewById(R.id.ll_top_spacing);
		// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - END

//		//Added by Thinzar, Change_Request-20160722, ReqNo.1
//		txtQRScanner		= (TextView) this.findViewById(R.id.txtQRScanner);
//		txtQRScanner.setText(getResources().getText(R.string.login_link_qr_scanner));
//		txtQRScanner.setVisibility(View.INVISIBLE);

		// Added by Mary@20130320 - Fixes_Request-20130319, ReqNo.3
		passwordField.setText("");

		//if (AppConstants.hasMultiLogin) {
		if(sharedPreferences.getBoolean(PreferenceConstants.HAS_MULTI_LOGIN, false)){

			btnBrokerHouse.setVisibility(View.VISIBLE);
			// Mary@20130828 - ChangeRequest-20120719, ReqNo.4 - END
		} else {
			// Added by Mary@20130828 - ChangeRequest-20120719, ReqNo.4
			btnBrokerHouse.setVisibility(View.GONE);
		}
		// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END

		nameField.setSingleLine();

		loginButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				try{
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(nameField.getWindowToken(), 0);

					// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
					if (AppConstants.brokerConfigBean == null) {
						AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
						builder.setMessage("Please choose a broker")
								.setPositiveButton("OK", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
								/* Mary@20130829 - ChangeRequest-20120719, ReqNo.4
													spinnerBroker.requestFocus();
								 */
										btnBrokerHouse.requestFocus();
									}
								});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! LoginActivity.this.isFinishing() )
							alert.show();

						return;
					}
					// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END

					// Added by Mary@20121224 - Fixes_Request-20121221, ReqNo.2 - START
					if( nameField.getText().toString().trim().length() == 0 ){
						AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
						builder.setMessage("Username must not be blank.")
								.setPositiveButton("OK", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
										nameField.requestFocus();
									}
								});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! LoginActivity.this.isFinishing() )
							alert.show();

						return;
					}
					// Added by Mary@20121224 - Fixes_Request-20121221, ReqNo.2 - END
					// Added by Mary@20121224 - Fixes_Request-20121221, ReqNo.1 - START
					if( passwordField.getText().toString().trim().length() == 0 ){
						AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
						builder.setMessage("Password must not be blank.")
								.setPositiveButton("OK", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
										passwordField.requestFocus();
									}
								});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! LoginActivity.this.isFinishing() )
							alert.show();

						return;
					}
					// Added by Mary@20121224 - Fixes_request-20121221, ReqNo.1 - END

					if (!LoginActivity.this.isNetworkAvailable()) {
						LoginActivity.this.showNetworkUnavailableAlert();
						// Added by Mary@20130527 - Fixes_Request-20130523, ReqNo.1
						LoginActivity.this.passwordField.setText("");
						return;
					}

					login();

					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
				}catch(Exception e){
					e.printStackTrace();
				}
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
			}
		});

		registerLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(LoginActivity.this, RegisterActivity.class);
				LoginActivity.this.startActivity(intent);
			}

		});

		// Added by Mary@20130117 - Fixes_Request-20130108, ReqNo.7 - START
		termLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loadTermsActivity(false);
			}

		});
		// Added by Mary@20130117 - Fixes_Request-20130108, ReqNo.7 - END

		//Change_Request-20160722, ReqNo.6
		secondaryTermLink.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				loadTermsActivity(false);
			}

		});

		// Change_Request-20170601, ReqNo.6
		importantNoteLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String strContraTitle = "<font color='#790008'>" + getResources().getString(R.string.contra_alert_title) + "</font>";

				AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
				builder.setTitle(Html.fromHtml(strContraTitle))
						.setMessage(getResources().getText(R.string.contra_alert_msg)).setCancelable(false)
						.setPositiveButton(getResources().getString(R.string.dialog_button_close),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								});

				AlertDialog alert = builder.create();
				if (!LoginActivity.this.isFinishing())
					alert.show();
			}
		});

		//Change_Request-20170601, ReqNo.16
		emailLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				String mailto = "mailto:" + AppConstants.brokerConfigBean.getEmailContact();

				Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
				emailIntent.setData(Uri.parse(mailto));

				try {
					startActivity(emailIntent);
				} catch (ActivityNotFoundException e) {
					//TODO: Handle case where no email app is available
				}
			}
		});

		callLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + AppConstants.brokerConfigBean.getPhoneContact()));
				startActivity(intent);
			}
		});

		// Added by Mary@20130828 - ChangeRequest-20120719, ReqNo.4 - START
		btnBrokerHouse.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LoginActivity.this.showBrokerHouseListScreen(getApplicationContext().getPackageName());
			}

		});
		// Added by Mary@20130828 - ChangeRequest-20120719, ReqNo.4 - END

		// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - START
		cbRememberMe.setOnCheckedChangeListener( new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				isRememberMe	= isChecked;
			}
		});
		// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.3 - END

		setupBtnTutorial(mView);

		repaintUI();
	}

	private void showInvalidPwdAlertAndResetTouchID(String message){
		AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this)
				.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						SharedPreferences.Editor editor	= sharedPreferences.edit();
						editor.remove(PreferenceConstants.IS_PRE_LOGIN_VALIDATED);
						editor.remove(PreferenceConstants.KEY_PASSWORD);
						editor.remove(PreferenceConstants.KEY_PASSWORD_IV);
						editor.remove(PreferenceConstants.KEY_USERNAME);
						editor.commit();

						//repaint pager_touchId
						resetTouchIDPager();

						dialog.dismiss();
					}
				}).create();

		if (!LoginActivity.this.isFinishing())
			alertDialog.show();
	}
	//Added by Thinzar, Change_Request-20160722, ReqNo.9 - END
}