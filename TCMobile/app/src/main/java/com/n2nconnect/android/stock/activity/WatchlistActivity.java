package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.ExchangeListView;
import com.n2nconnect.android.stock.custom.ExchangeListView.OnExchangListEventListener;
import com.n2nconnect.android.stock.custom.WatchListView;
import com.n2nconnect.android.stock.custom.WatchListView.OnWatchlistEventListener;
import com.n2nconnect.android.stock.model.LotMarket;
import com.n2nconnect.android.stock.model.Sector;
import com.n2nconnect.android.stock.model.StockInfo;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.Watchlist;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class WatchlistActivity extends CustomWindow 
implements OnRefreshStockListener, OnWatchlistEventListener, OnDoubleLoginListener, OnExchangListEventListener{

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private String userParam;
	*/
	private List<StockSymbol> stockSymbols;
	private List<StockSymbol> tableSymbols;
	// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.12
	private List<StockSymbol> filteredStockSymbols;
	private Button editButton;
	private boolean isDisplayPercentage;    
	private List<TextView> nameLabels;
	private List<TextView> priceLabels;
	private List<Button> changeButtons;
	/* Mary@20130718 - Change_Request-20130424, ReqNo.13
	// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13
	private List<TextView> exchgLabels;
	*/
	private List<TextView> volumeLabels;
	private List<TextView> valueLabels;
	private List<TextView> highLabels;
	private List<TextView> lowLabels;
	private List<TextView> tradeLabels;
	private List<TextView> openLabels;
	private WatchListView watchListView;
	private List<Watchlist> watchlists;
	private Watchlist selectedWatchlist;
	private Button watchlistButton;
	private boolean isEditMode;    
	private RefreshStockThread refreshThread;
	private DoubleLoginThread loginThread;
	private ProgressDialog dialog;
	private TableLayout tableLayout;
	private boolean isExpired = false;
	private LinearLayout selectedRow;
	private int selectedRowId;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr	= 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	// Added by Mary@20130122 - Change_Request-20130104, ReqNo.5 - START
	private String strSelectedFilterExchangeType;
	private Button btnExchangeType;
	private ExchangeListView exchgListView;
	private Map<String, Object> selectedExchangeMap;
	private String ALL_TYPE_KEY	= null;
	// Added by Mary@20130122 - Change_Request-20130104, ReqNo.5 - END
	
	//Added by Thinzar, Fixes_Request-20150601, ReqNo.3
	private String favorite_watchlist_id	= "";	
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.1
	private boolean isSenderCodeMatch	= true;
	
	final Handler handler = new Handler() {
		public void handleMessage(Message message) {
			List<StockSymbol> changedSymbols = (List<StockSymbol>) message.obj;
			WatchlistActivity.this.refreshStockData(changedSymbols);
		}
	};
	
	//diyana add for sorting
	private TextView tvNameLabel;
	private boolean isClickFromLabelName=false;
	
	//Change_Request-20170119, ReqNo.1
	private final int INT_STOCK_CHART_PAGE_POS 		= 3;	//same as variables from StockActivity
	private final int INT_STOCK_NEWS_PAGE_POS 		= 6;	//same as variables from StockActivity
	
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.stock_watchlist);

			this.title.setText(getResources().getString(R.string.watchlist_module_title));
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
			this.icon.setImageResource(R.drawable.menuicon_watchlist);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntWatchlistMenuIconBgImg() );

			// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - START
			editButton 		= (Button)findViewById(R.id.editButton);
			editButton.setText(getResources().getString(R.string.watchlist_edit_btn));
			watchlistButton = (Button)findViewById(R.id.watchlistButton);
			// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - END
			//Diyana add for sorting
			setupLabel();
			
			if(! AppConstants.hasMultiLogin ){
				//( (RelativeLayout)findViewById(R.id.rl_sw_TopNavBar) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBgImg() );		//Commented: Change_request-20160101, RequestNo.3
				//editButton.setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );			//Commented: Change_request-20160101, RequestNo.3
				//watchlistButton.setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );		//Commented: Change_request-20160101, RequestNo.3
			}
			// Mary@20120814 - Change_Request-20120719, ReqNo.4 - END

			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(WatchlistActivity.this)
			//.setIcon(android.R.attr.alertDialogStyle)
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,	int id) {
					dialog.dismiss();
					return;
				}
			})
			.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			userParam = sharedPreferences.getString(PreferenceConstants.QC_USER_PARAM, null);
			if (userParam == null) {
				List<String>data = QcHttpConnectUtil.relogin();
				userParam = data.get(0);
				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
				editor.commit();
			}
			*/
			
			// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - START
			ALL_TYPE_KEY	= getResources().getString(R.string.all_type_key);
			btnExchangeType = (Button)findViewById(R.id.btnExchangeType);
			
			if(! AppConstants.hasMultiLogin ){
				//( (RelativeLayout)findViewById(R.id.rl_sw_TopNavBar) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBgImg() );		//Commented: Change_request-20160101, RequestNo.3
				//btnExchangeType.setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );		//Commented: Change_request-20160101, RequestNo.3
			}
			
			exchgListView = new ExchangeListView(WatchlistActivity.this, WatchlistActivity.this, getLayoutInflater(), null);
			exchgListView.setHideOnSelect(true);
			
			btnExchangeType.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					doExchangeList();
				}        	
			});
			// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - END

			//Added by Thinzar@20141204, Change_Request-20141204, ReqNo. 11
			sharedPreferences 						= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.2
			finish();
		}

	}
	
	private void setupLabel(){
		tvNameLabel=(TextView)findViewById(R.id.tv_name);
		tvNameLabel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				isClickFromLabelName=true;
				
				new watchListRefresh().execute();
				
			}
		});
		
	}
	
	public void onResume() {
		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.2
		try{
			super.onResume();
			
			// Added by Mary@20130718 - Fixes_Request-20130711, ReqNo.4 - START
			loginThread = ( (StockApplication) WatchlistActivity.this.getApplication() ).getDoubleLoginThread();
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					
					loginThread.start();
					( (StockApplication) WatchlistActivity.this.getApplication() ).setDoubleLoginThread(loginThread);
				}
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(WatchlistActivity.this);
				loginThread.resumeRequest();
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
			}
			loginThread.setRegisteredListener(WatchlistActivity.this);
			// Added by Mary@20130718 - Fixes_Request-20130711, ReqNo.4 - END
						
			// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.22 - START
			if(refreshThread != null){
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
				refreshThread.setSymbolCodes( SystemUtil.getSymbolCodes(filteredStockSymbols) );
				refreshThread.setRegisteredListener(WatchlistActivity.this);
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END
				
				if( refreshThread.isPaused() )
					refreshThread.resumeRequest();
			}
			// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.22 - END
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= WatchlistActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
			// Added by Mary@20121116 - Change_Request-20120719, ReqNo.12
			AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, null);
	
			dialog 		= new ProgressDialog(this);
			
			/* Mary@20120803 - Add filtering condition to avoid IndexOutOfBoundException
			if(selectedRow!=null){
			*/
			/* Mary@20130618 - Fixes_Request-20130523, ReqNo.12
			if( selectedRow != null && ( stockSymbols != null && ( selectedRowId < stockSymbols.size() ) ) ){
			*/
			if( selectedRow != null && ( filteredStockSymbols != null && ( selectedRowId < filteredStockSymbols.size() ) ) ){
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo. 3
				if( ( selectedRowId + 1 ) % 2 != 0 )
					selectedRow.setBackgroundResource(LayoutSettings.tablerow_dark);
				else
					selectedRow.setBackgroundResource(LayoutSettings.tablerow_light);
				*/
				//selectedRow.setBackgroundResource( ( ( selectedRowId + 1 ) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);			//Commented: Change_request-20160101, RequestNo.3
	
				/* Mary@20130618 - Fixes_Request-20130523, ReqNo.12
				StockSymbol symbol	= stockSymbols.get(selectedRowId);
				*/
				StockSymbol symbol	= filteredStockSymbols.get(selectedRowId);
				if( symbol.isExpand() ){
					int childCount = selectedRow.getChildCount();
					if(childCount!=1)
						selectedRow.removeViewAt(1);
						
					symbol.setExpand(false);
					/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
					if( ( selectedRowId + 1 ) % 2 != 0 ) 
						selectedRow.setBackgroundResource(LayoutSettings.tablerow_dark);
					else
						selectedRow.setBackgroundResource(LayoutSettings.tablerow_light);
					*/
					//selectedRow.setBackgroundResource( ( ( selectedRowId + 1 ) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light );		//Commented: Change_request-20160101, RequestNo.3
				}
			}
			
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
			List<Map<String, Object>> listExchangeMap	= new ArrayList<Map<String, Object>>();
			try{
				// All Exchange
				Map<String, Object> exchgMap			= new HashMap<String, Object>();
				exchgMap.put(ParamConstants.PARAM_STR_TRD_EXCHG_CODE, ALL_TYPE_KEY);
				exchgMap.put(ParamConstants.PARAM_STR_EXCHG_NAME, getResources().getString(R.string.all_exchange_type_label));
				exchgMap.put(ParamConstants.PARAM_INT_EXCHG_IMAGE_ID, null);
				
				listExchangeMap.add(exchgMap);
				
				Map<String, Integer> mapExchgImages		= DefinitionConstants.getExchangeImageMapping();
				List<String> listQCExchgCode			= DefinitionConstants.getValidQCExchangeCodes();
	
				for(int i=0; i<listQCExchgCode.size(); i++){
					String strExchgCode		= DefinitionConstants.getTrdExchangeCode().get(i);
					String strQCExchgCode	= listQCExchgCode.get(i);
					String strExchgName		= DefinitionConstants.getExchangeNameMapping().get(strQCExchgCode);
					
					exchgMap				= new HashMap<String, Object>();
					exchgMap.put(ParamConstants.PARAM_STR_TRD_EXCHG_CODE, strExchgCode);		//Change_Request-20141101, ReqNo 20
					exchgMap.put(ParamConstants.PARAM_STR_EXCHG_NAME, strExchgName);
					exchgMap.put(ParamConstants.PARAM_INT_EXCHG_IMAGE_ID, mapExchgImages.get(strQCExchgCode) == null ? null : ( mapExchgImages.get(strQCExchgCode) ).intValue() );
					
					listExchangeMap.add(exchgMap);
				}
				
				exchgListView.setListExchangeMap(listExchangeMap);
			} catch (Exception e) {			
				e.printStackTrace();
			}
			
			if (selectedExchangeMap == null) {
				for( Iterator<Map<String, Object>> itr=exchgListView.getListExchangeMap().iterator(); itr.hasNext(); ){
					Map<String, Object> mapExchange	= itr.next();
					String strExchgCode 			= String.valueOf( mapExchange.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
					if( strExchgCode.equalsIgnoreCase(ALL_TYPE_KEY) ){
						selectedExchangeMap = mapExchange;
						break;
					}
				}
			}
			btnExchangeType.setText( String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
	
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				new populateWatchlist().execute();
		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
			finish();
		}
		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.2 - END
	}
	
	@Override
	protected void onPause() {
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onPause();
			
			if(refreshThread!=null)
				refreshThread.pauseRequest();
		
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}

	
	
	
	
	
	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		Message message	= handler.obtainMessage();
		message.obj		= changedSymbols;
		handler.sendMessage(message);

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if(this.mainMenu.isShowing()){
				this.mainMenu.hide();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo. 3
				return true;
				*/
			/* Mary@20130703 - GooglePlayReport-20130701, CrashNo.1
			}else if(this.watchListView.isShowing()){
			*/
			}else if(this.watchListView != null && this.watchListView.isShowing()){
				this.watchListView.hide();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo. 3
				return true;
				*/
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
			/* Mary@20130703 - GooglePlayReport-20130701, CrashNo.1
			}else if( this.exchgListView.isShowing() ){
			*/
			}else if( this.exchgListView != null && this.exchgListView.isShowing() ){
				this.exchgListView.hide();
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
			}else{
				finish();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo. 3
				return true;
				*/
			}
			// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo. 3
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}

	protected void onMenuShownEvent() {
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - START
		try{
			if(watchListView == null){
				watchListView = new WatchListView(WatchlistActivity.this, WatchlistActivity.this, getLayoutInflater());
				watchListView.setHideOnSelect(true);
			}
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - END
			
			if (watchListView.isShowing()) {
				watchListView.hide();
			}
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - END
	}

	private void refreshStockData(List<StockSymbol> changedSymbols) {
		try {
			// Added by Mary@20130715 - avoid nullpointerException
			if(stockSymbols == null || changedSymbols == null)
				return;

			int id = 0;
			for (Iterator<StockSymbol> itr=changedSymbols.iterator(); itr.hasNext(); ) {
				final StockSymbol symbol= itr.next();
				/* Mary@20130618 - Fixes_Request-20130523, ReqNo.12
				StockSymbol current 	= stockSymbols.get(id);
				*/
				StockSymbol current 	= ( filteredStockSymbols == null || ( filteredStockSymbols != null && filteredStockSymbols.size() <= id ) ) ? new StockSymbol() : filteredStockSymbols.get(id);
				// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
				int intLotSize			= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? symbol.getIntLotSize() : 1;
				/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
				String[] symbolExchg 	= symbol.getSymbolCode().split("\\.");
				*/
				String strSymbolExchg	= symbol.getSymbolCode().substring( (symbol.getSymbolCode().lastIndexOf(".") + 1), symbol.getSymbolCode().length() );

				/* Mary@20121003 - Fixes_Request-20120815, ReqNo.3
				if (symbol.getSymbolCode().equals(current.getSymbolCode())){
					
					if (symbol.checkDiscrepancy(current)) {
				*/
				if( symbol.getSymbolCode().equals( current.getSymbolCode() ) && symbol.checkDiscrepancy(current) ){
					if (symbol.getLastDone() != current.getLastDone() || symbol.getTotalTrade()!=current.getTotalTrade()) {
						final TextView priceLabel	= priceLabels.get(id);
						float gap 					= symbol.getLastDone()-current.getLastDone();
						current.setLastDone(symbol.getLastDone());

						//Added by Thinzar, Fixes_Request-20160101, ReqNo.3
						/*
						if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
							priceLabel.setText(FormatUtil.formatPrice2(symbol.getLastDone(), "-"));
						}else{
							priceLabel.setText(FormatUtil.formatPrice(symbol.getLastDone(), "-"));
						}
						*/
						priceLabel.setText(FormatUtil.formatPrice(symbol.getLastDone(), "-", strSymbolExchg));

						/* SET COLOR */
						if (gap > 0) {
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO'  status
							priceLabel.setBackgroundColor(Color.parseColor("#33CC33"));
							priceLabel.setTextColor(Color.BLACK);
							*/
							priceLabel.setBackgroundColor( getResources().getColor(R.color.UpStatus_BgColor) );
							priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_FgColor) );
						} else if (gap < 0) {
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO'  status
							priceLabel.setBackgroundColor(Color.parseColor("#CC3333"));
							priceLabel.setTextColor(Color.WHITE);
							*/
							priceLabel.setBackgroundColor( getResources().getColor(R.color.DownStatus_BgColor) );
							priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_FgColor) );
						/* Mary@20120905 - Fixes_Request-20120815, ReqNo.8
						} else if (gap == 0 && symbol.getTotalTrade()!=current.getTotalTrade()) {
						*/
						}else if(gap == 0 && symbol.getVolume() != current.getVolume() ){
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO'  status
							priceLabel.setBackgroundColor(Color.parseColor("#FFD700"));
							priceLabel.setTextColor(Color.BLACK);
							*/
							priceLabel.setBackgroundColor( getResources().getColor(R.color.DiffTtlStatus_BgColor) );
							priceLabel.setTextColor( getResources().getColor(R.color.DiffTtlStatus_FgColor) );
						}
						
						/* SET UPDATER */
						priceLabel.postDelayed(new Runnable() {
							public void run() {
								priceLabel.setBackgroundColor(Color.TRANSPARENT);
								if (symbol.getPriceChange() > 0) {
									/* Mary@20120905 - standarized status color for 'UP', 'DOWN' and 'NO' status
									priceLabel.setTextColor(Color.parseColor("#33CC33"));
									*/
									priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
								} else if (symbol.getPriceChange() < 0) {
									/* Mary@20120905 - standarized status color for 'UP', 'DOWN' and 'NO' status
									priceLabel.setTextColor(Color.parseColor("#CC3333"));
									*/
									priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
								} else {
									/* Mary@20130718 - Fixes_Request-20130711, ReqNo.5
									priceLabel.setTextColor(Color.parseColor("#D3D3D3"));
									*/
									priceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
								}
							}
						}, 1500);


						Button changeButton = changeButtons.get(id);
						/*
						if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
							changeButton.setText(FormatUtil.formatPriceChange2(current.getPriceChange()));
						}else{
							changeButton.setText(FormatUtil.formatPriceChange(current.getPriceChange()));
						}
						*/
						//Added by Thinzar, Fixes_Request-20160101, ReqNo.3
						changeButton.setText(FormatUtil.formatPriceChangeByExchange(current.getPriceChange(), strSymbolExchg));
						
						/* Mary@20121004 - Fixes_Request-20120815, ReqNo. 3
						if (current.getPriceChange()<0) {
							changeButton.setBackgroundResource(R.drawable.buttonchg_red70x28);
						} else {
							changeButton.setBackgroundResource(R.drawable.buttonchg_grn70x28);
						}
						*/
						changeButton.setBackgroundResource( (current.getPriceChange()<0) ? R.drawable.buttonchg_red70x28 : R.drawable.buttonchg_grn70x28);
					}


					if (symbol.getVolume()!=current.getVolume()) {
						current.setVolume(symbol.getVolume());
						TextView volumeLabel = volumeLabels.get(id);
						/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
						/* Mary@20120911 - Change_Request-20120719, ReqNo.12
						volumeLabel.setText(FormatUtil.formatLong(symbol.getVolume()));
						/
						/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
						volumeLabel.setText( FormatUtil.formatLong( symbol.getVolume() / intLotSize ) );
						/
						volumeLabel.setText( FormatUtil.formatLong( symbol.getVolume() / intLotSize, "-" ) );
						*/
						volumeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (symbol.getVolume() / intLotSize), "-" ) );
					}
					if (symbol.getValue()!=current.getValue()) {
						current.setValue(symbol.getValue());
						TextView valueLabel = valueLabels.get(id);
						/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
						if(symbolExchg[1].equals("JKD")){
						*/
						if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
							/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
							valueLabel.setText(FormatUtil.formatDouble2(symbol.getValue()));
							*/
							valueLabel.setText(FormatUtil.formatDouble2(symbol.getValue(), "-"));
						}else{
							/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
							valueLabel.setText(FormatUtil.formatDouble2(symbol.getValue()));
							*/
							valueLabel.setText(FormatUtil.formatDouble(symbol.getValue(), "-"));
						}
					}
					if (symbol.getHigh()!=current.getHigh()) {
						current.setHigh(symbol.getHigh());
						TextView highLabel = highLabels.get(id);
						/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
						if(symbolExchg[1].equals("JKD")){
						*/
						/*
						if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
							highLabel.setText(FormatUtil.formatFloat2(symbol.getHigh(), "-"));
						}else{
							highLabel.setText(FormatUtil.formatFloat(symbol.getHigh(), "-"));
						}*/
						highLabel.setText( FormatUtil.formatPrice(symbol.getHigh(), "-", strSymbolExchg));
						
					}
					if (symbol.getLow()!=current.getLow()) {
						current.setLow(symbol.getLow());
						TextView lowLabel = lowLabels.get(id);
						/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
						if(symbolExchg[1].equals("JKD")){
						*/
						/*
						if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
							lowLabel.setText(FormatUtil.formatFloat2(symbol.getLow(), "-"));
						}else{
							lowLabel.setText(FormatUtil.formatFloat(symbol.getLow(), "-"));
						}
						*/
						lowLabel.setText(FormatUtil.formatPrice(symbol.getLow(), "-", strSymbolExchg));
					}
					if (symbol.getTotalTrade()!=current.getTotalTrade()) {
						current.setTotalTrade(symbol.getTotalTrade());
						TextView tradeLabel = tradeLabels.get(id);
						/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
						tradeLabel.setText(FormatUtil.formatLong(symbol.getTotalTrade()));
						*/
						tradeLabel.setText(FormatUtil.formatLong(symbol.getTotalTrade(), "-"));
					}
					/* Mary@20121003 - Fixes_Request-20120815, ReqNo.3
					}  
					*/
				}
				id++;
			}

		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private class populateWatchlist extends AsyncTask<View, Void, Boolean>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				//System.out.println("==debug: asynctask populateWatchlist");
				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! WatchlistActivity.this.isFinishing() )
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected Boolean doInBackground(View... arg0) {
			// TODO Auto-generated method stub
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			boolean success = WatchlistActivity.this.populateWatchlists();
			*/
			boolean success = false;
			try{
				success = WatchlistActivity.this.populateWatchlists();
			}catch (FailedAuthenicationException e){
				isExpired = true;
				if(refreshThread!=null)
					refreshThread.stopRequest();

				if(loginThread != null)
					loginThread.stopRequest();
				
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
			}catch(Exception e){
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			return success;
		}

		@Override
		protected void onPostExecute(Boolean result){
			super.onPostExecute(result);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( ! result ){
					if(isExpired){
						// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
						if(dialog != null && dialog.isShowing() )
							dialog.dismiss();
						WatchlistActivity.this.sessionExpired();
						return;
					}
					
					//Added by Thinzar, Change_Request-20150901, ReqNo.1 - START
					if(exception != null){
						if(dialog != null && dialog.isShowing() )
							dialog.dismiss();
						
						boolean isATPConnectionLost = (exception.getMessage().equalsIgnoreCase(ErrorCodeConstants.FAIL_CONNECT_ATP))? true:false;
						if(isATPConnectionLost || !isSenderCodeMatch){
							prepareAlertException(WatchlistActivity.this.alertException, exception.getMessage(), true);
						} else {
							alertException.setMessage(exception.getMessage());
						}
						
						if( ! WatchlistActivity.this.isFinishing() ){
							WatchlistActivity.this.alertException.setCanceledOnTouchOutside(false);
							alertException.show();
						}
						
						exception	= null;
						return;
					}
					//Added by Thinzar, Change_Request-20150901, ReqNo.1 - END
					
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					/*
					if(exception != null){
						// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
						if(dialog != null && dialog.isShowing() )
							dialog.dismiss();
						alertException.setMessage( exception.getMessage() );
						alertException.setButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int id) {
								dialog.dismiss();
								finish();
							}
						});
	
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! WatchlistActivity.this.isFinishing() )
							alertException.show();
						
						return;
					}
					*/
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}
				
				if(watchlists == null){
					// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1
					WatchlistActivity.this.setupEditButton();
					return;
				}
	
				/*Commented out by Thinzar, Fixes_Request-20150601, ReqNo. 3
				if(watchlists.size() > 0)
					selectedWatchlist = watchlists.get(0);
				*/
				//Added by Thinzar, Fixes_Request-20150601, ReqNo. 3 - START
				if(watchlists.size() > 0){
					boolean isFavWatchlistIdStillExist	= false;
					favorite_watchlist_id	= sharedPreferences.getString(PreferenceConstants.WATCHLIST_FAV_ID, "");
					if(favorite_watchlist_id.length()>0){
						for (Iterator<Watchlist> itr=watchlists.iterator(); itr.hasNext(); ) {
							Watchlist watchlist = itr.next();
							if (watchlist.getId().equals(favorite_watchlist_id)) {
								selectedWatchlist = watchlist;
								isFavWatchlistIdStillExist	= true;
							}
						}
						
						if(!isFavWatchlistIdStillExist){
							selectedWatchlist	= watchlists.get(0);
						}
						
					} else {
						selectedWatchlist	= watchlists.get(0);
					}
					
					SharedPreferences.Editor editor 		= sharedPreferences.edit();
					editor.putString(PreferenceConstants.WATCHLIST_FAV_ID, selectedWatchlist.getId());
					editor.commit();
				}
				//Added by Thinzar, Fixes_Request-20150601, ReqNo. 3 - END 
				
				
				/* Mary@20120815 - Fixes_Request-20120815, ReqNo.1
				watchlistButton = (Button) WatchlistActivity.this.findViewById(R.id.watchlistButton);
				if (selectedWatchlist != null) {
					watchlistButton.setText(selectedWatchlist.getName());
				}
				*/
				watchlistButton.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						WatchlistActivity.this.doWatchlist();				
					}        	
				});
				
				/* Mary@20120815 - Fixes_Request-20120815, ReqNo.1
				WatchlistActivity.this.setupEditButton();
				*/
	
				watchListView = new WatchListView(WatchlistActivity.this, WatchlistActivity.this, getLayoutInflater());
				watchListView.setHideOnSelect(true);
				try {
					watchListView.setWatchlists(watchlists);
				} catch (Exception e) {			
					e.printStackTrace();
				}
	
				// Added by Mary@20120731 - Fixes_Request-20120724, ReqNo.11 - START
				if( dialog != null && dialog.isShowing() ) 
					dialog.dismiss();
				// Added by Mary@20120731 - Fixes_Request-20120724, ReqNo.11 - END
				
				if (selectedWatchlist == null) {
					// Added by Mary@20120731 - Fixes_Request-20120815, ReqNo.1
					WatchlistActivity.this.setupEditButton();
					return;
				// Added by Mary@20120731 - Fixes_Request-20120815, ReqNo.1
				}else{
					watchlistButton.setText( selectedWatchlist.getName() );
				}
				
				WatchlistActivity.this.refreshWatchlist();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	private void setupEditButton() {
		/* Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - START
		editButton = (Button) this.findViewById(R.id.editButton);
		
		if ( ! watchlists.isEmpty() ) {
			editButton.setText("Edit");
		*/
		editButton.setVisibility(View.INVISIBLE);
		// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5
	 	btnExchangeType.setVisibility(View.INVISIBLE);

		if ( watchlists != null && ! watchlists.isEmpty() ) {
		// Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - END
			editButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					if(!isEditMode){
						editButton.setText(getResources().getString(R.string.watchlist_done_btn));
						refreshThread = ( (StockApplication) WatchlistActivity.this.getApplication() ).getRefreshThread();
						// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
						if(refreshThread != null)
							refreshThread.setSymbolCodes(new String[0]);
						WatchlistActivity.this.enterTableEditMode();
						isEditMode = true;
					}else{
						editButton.setText(getResources().getString(R.string.watchlist_edit_btn));
						WatchlistActivity.this.refreshWatchlist();
						isEditMode = false;
					}
				}        	
			});
			// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1
		 	watchlistButton.setVisibility(View.VISIBLE);
		 	
		 	// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
			if(stockSymbols != null && !stockSymbols.isEmpty())
				btnExchangeType.setVisibility(View.VISIBLE);
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
		}else{
			editButton.setText(getResources().getString(R.string.watchlist_create_btn));
			editButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					WatchlistActivity.this.enterWatchlistName();
				}
			});			
			watchlistButton.setVisibility(View.INVISIBLE);
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5
		 	btnExchangeType.setVisibility(View.INVISIBLE);
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5
			isEditMode = false;
		}
		
		/* Mary@20130618 - Fixes_Request-20130523, ReqNo.12
		// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - START
		if( ( stockSymbols != null && !stockSymbols.isEmpty() ) || ( watchlists == null || ( watchlists != null && watchlists.isEmpty() ) ) )
		*/
		/* Mary@20130703 - Fixes_Request-20130523, ReqNo.26
		if( filteredStockSymbols != null && !filteredStockSymbols.isEmpty() )
		*/
		if( ( editButton.getText().equals(getResources().getString(R.string.watchlist_create_btn)) 
				&& ( watchlists == null || ( watchlists != null && watchlists.isEmpty() ) ) )
			|| editButton.getText().equals(getResources().getString(R.string.watchlist_done_btn)) 
			|| ( editButton.getText().equals(getResources().getString(R.string.watchlist_edit_btn)) 
					&& filteredStockSymbols != null && !filteredStockSymbols.isEmpty() ) )
			editButton.setVisibility(View.VISIBLE);
		// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - END
	}


	private void enterTableEditMode() {
		final TableLayout tableLayout	= (TableLayout) findViewById(R.id.symbolTable);
		tableLayout.removeAllViews();

		int id							= 0;
		tableSymbols 					= new ArrayList<StockSymbol>();
		

		/* Mary@20130618 - Fixes_Request-20130523, ReqNo.12
		for (Iterator<StockSymbol> itr=stockSymbols.iterator(); itr.hasNext(); ) {
		*/
		for (Iterator<StockSymbol> itr=filteredStockSymbols.iterator(); itr.hasNext(); ) {
			StockSymbol symbol 			= itr.next();
			tableSymbols.add(symbol);

			final TableRow tableRow 	= new TableRow(this);   
			tableRow.setId(id);
			tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
			tableRow.setWeightSum(1);
			
			View rowView 				= this.getLayoutInflater().inflate(R.layout.symbol_row, null);  
			final LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.rowLayout);

			TextView nameLabel 			= (TextView) rowView.findViewById(R.id.nameLabel);
			/* Mary@20130718 - Change_Request-20130424, ReqNo.13
			nameLabel.setText(symbol.getStockName());
			*/
			nameLabel.setText( symbol.getStockName(false) );

			TextView priceLabel 		= (TextView) rowView.findViewById(R.id.priceLabel);
			priceLabel.setText("");

			final Button changeButton 	= (Button) rowView.findViewById(R.id.changeButton);             
			changeButton.setText(getResources().getString(R.string.watchlist_delete_btn));
			changeButton.setBackgroundResource(LayoutSettings.btnRed);
			changeButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					/* Mary@20130618 - Fixes_Request-20130523, ReqNo.12
					StockSymbol symbol 	= stockSymbols.get(tableRow.getId());
					*/
					StockSymbol symbol 	= filteredStockSymbols.get(tableRow.getId());
					int index 			= 0;
					boolean toRemove 	= false;
					for (Iterator<StockSymbol> itr=tableSymbols.iterator(); itr.hasNext(); ) {
						StockSymbol rowSymbol = itr.next();
						if (rowSymbol.getSymbolCode().equals(symbol.getSymbolCode())) {
							toRemove = true;
							break;
						}
						index++;
					}
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					if (toRemove) {
						tableSymbols.remove(index);
						tableLayout.removeViewAt(index);	
					}
					new removeWatchlistStock().execute(symbol.getSymbolCode());
					*/
					new removeWatchlistStock(index, toRemove).execute(symbol.getSymbolCode());
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}

			});

			tableRow.addView(rowView);

			/* Mary@20121004 - Fixes_Request-20120815, ReqNo. 3
			if ((id+1)%2 != 0) {
				rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
			} else {
				rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
			}
			*/
			//rowLayout.setBackgroundResource( ( (id+1)%2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light );		//Commented: Change_request-20160101, RequestNo.3

			tableLayout.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );  
			id++;
		}
	}

	private class removeWatchlistStock extends AsyncTask<String, Void, Boolean>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private String strErrMsg	= "ERROR";
		private int intInd			= 0;
		private boolean isRemove	= false;
		// Added by Mary@20120815 - Fix_Request-20120815, ReqNo.1
		private String strSymbolCode= null;
		
		protected removeWatchlistStock(int index, boolean toRemove){
			intInd				= index;
			isRemove			= toRemove;
		}
		
		protected void onPreExecute(){
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
	
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! WatchlistActivity.this.isFinishing() )
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
			
		@Override
		protected Boolean doInBackground(String... arrSymbolCode) {
			// TODO Auto-generated method stub
			boolean isSuccess	= false;
			try{
				/* Added by Mary@20120815 - Fix_Request-20120815, ReqNo.1
				isSuccess = WatchlistActivity.this.removeWatchlistStock(symbolCode[0]);
				 */
				this.strSymbolCode	= arrSymbolCode[0];
				
				// Added by Mary@20130625 - Fixes_Request-20130523, ReqNo.21 - START
				int index 				= this.strSymbolCode.lastIndexOf(".");
				String strOriExchgCode	= this.strSymbolCode.substring( (index + 1), this.strSymbolCode.length() );
				this.strSymbolCode		= (strOriExchgCode.length() > 1 && strOriExchgCode.endsWith("D") ) ? this.strSymbolCode.substring(0, (this.strSymbolCode.length()-1) ) : this.strSymbolCode ;
				// Added by Mary@20130625 - Fixes_Request-20130523, ReqNo.21 - END
				
				isSuccess 			= WatchlistActivity.this.removeWatchlistStock(this.strSymbolCode);				
			}catch(Exception e){
				strErrMsg	= e.getMessage();
			}
			return isSuccess;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( !result ){
					if(isExpired){
						WatchlistActivity.this.sessionExpired();
						return;
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					}else{
						alertException.setMessage(strErrMsg);
						alertException.show();
					}
				}else{
					if (isRemove) {
						tableSymbols.remove(intInd);
						tableLayout.removeViewAt(intInd);
						
						// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - START
						/* Mary@20130130 - avoid indexOutOfBoundException as remove will change the item index
						int intRemove	= -1;
						for(int i=0; i<WatchlistActivity.this.stockSymbols.size(); i++){
							StockSymbol symbol = (StockSymbol) WatchlistActivity.this.stockSymbols.get(i);
							if( symbol.getSymbolCode().equals(this.strSymbolCode) ){
								intRemove = i;
								break;
							}
						}
	
						if(intRemove != -1)
							WatchlistActivity.this.stockSymbols.remove(intRemove);
						*/
							
						WatchlistActivity.this.setupEditButton();
						// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - END
					}
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}
	
				if( dialog != null && dialog.isShowing() )
					dialog.dismiss();
				
				// Added by Mary@20120924 - Fixes_Request-20120815, ReqNo.3 - START
				strSymbolCode	= null;
				strErrMsg		= null;
				intInd			= 0;
				isRemove		= false;
				// Added by Mary@20120924 - Fixes_Request-20120815, ReqNo.3 - END
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	//private boolean removeWatchlistStock(String symbolCode) {
	private boolean removeWatchlistStock(String symbolCode) throws Exception{
		String atpUserParam				= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
		String senderCode				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

		Map<String, Object> parameters = new HashMap<String, Object>();

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo. 3 - START
		if(AppConstants.EnableEncryption==false){
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
		}else{
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		}
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if( AppConstants.brokerConfigBean.isEnableEncryption() )
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo. 3 - END
		
		parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
		parameters.put(ParamConstants.FAVORATEID_TAG, selectedWatchlist.getId());
		
		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
		int index 						= symbolCode.indexOf(".");
		*/
		int index 						= symbolCode.lastIndexOf(".");
		String exchangeCode 			= null;
		if (index != -1) {
			parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode.substring(0, index));
			exchangeCode 				= symbolCode.substring( (index+1), symbolCode.length() );
		} else {
			parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
			StockApplication application= (StockApplication) this.getApplication();
			exchangeCode				= application.getSelectedExchange().getQCExchangeCode(); 
		}

		parameters.put(ParamConstants.EXCHANGECODE_TAG, exchangeCode);

		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		try {

			AtpConnectUtil.deleteFavListStock(parameters);

			return true;

		} catch (FailedAuthenicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getMessage().equals("Session is expired")){
				isExpired = true;
				if(refreshThread!=null){
					refreshThread.stopRequest();
				}
				loginThread.stopRequest();
				LoginActivity.QCAlive.stopRequest();
				return false;
			}
		}
		return false;
		*/
		boolean isSuccess	= false;
		try {
			isSuccess	= AtpConnectUtil.deleteFavListStock(parameters);
			
			while(intRetryCtr < AppConstants.intMaxRetry && ! isSuccess){
				intRetryCtr++;
				isSuccess	= AtpConnectUtil.deleteFavListStock(parameters);
			}
			intRetryCtr	= 0;
			
			if( ! isSuccess )
				throw new Exception(ErrorCodeConstants.FAIL_DEL_STOCK_IN_WATCHLIST);
		}catch (FailedAuthenicationException e){
			isExpired = true;
			if(refreshThread!=null){
				refreshThread.stopRequest();
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(loginThread != null)
				loginThread.stopRequest();
			
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(LoginActivity.QCAlive != null)
				LoginActivity.QCAlive.stopRequest();
			
			return false;
		}catch (Exception e){
			throw e;
		}
		return isSuccess;
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 
	private boolean populateWatchlists() {
	*/
	private boolean populateWatchlists() throws Exception{
		String atpUserParam				= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
		String senderCode 				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

		Map<String, Object> parameters = new HashMap<String, Object>();
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if(AppConstants.EnableEncryption==false){
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
		}else{
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		}
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if( AppConstants.brokerConfigBean.isEnableEncryption() )
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
		// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo. 3 - END
		
		parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
		
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		try {
			String response = AtpConnectUtil.getFavListInfo(parameters);
			watchlists = AtpMessageParser.parseFavListInfo(response);
			return true;
		} catch (FailedAuthenicationException e) {
			e.printStackTrace();
			if(e.getMessage().equals("Session is expired")){
				LoginActivity.QCAlive.stopRequest();
				isExpired = true;
				return false;
			}else{
				populateWatchlists(); // comment : may cause infinite loop
			}
		}
		return false;
		*/
		try{
			String response = AtpConnectUtil.getFavListInfo(parameters);
			//Added by Thinzar, Change_Request-20150901, ReqNo.1
			AppConstants.connectExceptionCount = 0;
			AppConstants.timeoutExceptionCount = 0;
			
			while(intRetryCtr < AppConstants.intMaxRetry && response == null){
				intRetryCtr++;
				response = AtpConnectUtil.getFavListInfo(parameters);
			}
			intRetryCtr	= 0;
			
			//Edited by Thinzar, Change_Request-20150901, ReqNo.1
			if (response == null){
				if(AppConstants.connectExceptionCount >= AppConstants.intMaxRetry){
					throw new Exception(ErrorCodeConstants.NETWORK_UNAVAILABLE);
				}
				else if(AppConstants.timeoutExceptionCount >= AppConstants.intMaxRetry){
					throw new Exception(ErrorCodeConstants.FAIL_CONNECT_ATP);
				} else
					throw new Exception(ErrorCodeConstants.FAIL_GET_WATCHLIST);
			}
			
			watchlists		= AtpMessageParser.parseFavListInfo(response);
			
			//Added by Thinzar, Change_Request-20160101, ReqNo.1 - START
			isSenderCodeMatch	= true;
			for (Iterator<Watchlist> itr = watchlists.iterator(); itr.hasNext(); ) {
				Watchlist w			= itr.next();
				if(!w.getClientCode().equals(senderCode)){
					isSenderCodeMatch	= false;
					break;
				}
			}
			
			if(!isSenderCodeMatch) 
				throw new Exception(ErrorCodeConstants.SENDER_CODE_NOT_MATCH);
			//Added by Thinzar, Change_Request-20160101, ReqNo.1 - END
			
			return true;
		}catch(Exception e){
			throw e;
		}
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}

	private void doWatchlist() {
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - START
		try{
			if(watchListView == null){
				watchListView = new WatchListView(WatchlistActivity.this, WatchlistActivity.this, getLayoutInflater());
				watchListView.setHideOnSelect(true);
			}
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - END
						
			if (watchListView.isShowing()) {
				watchListView.hide();
			} else {
				if (mainMenu.isShowing()) {
					mainMenu.hide();
				}
				watchListView.show(findViewById(R.id.headerLayout), selectedWatchlist);
			}
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - END
	}

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	private String[] getWatchlistSymbols() {
	*/
	private String[] getWatchlistSymbols() throws Exception{
		String atpUserParam				= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
		String senderCode				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

		Map<String, Object> parameters	= new HashMap<String, Object>();
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if(AppConstants.EnableEncryption==false){
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
		}else{
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		}
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if( AppConstants.brokerConfigBean.isEnableEncryption() )
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
		
		//Edited by Thinzar, Fixes_Request-20150601,ReqNo. 3 - START 
		//parameters.put(ParamConstants.FAVORATEID_TAG, selectedWatchlist.getId());	
		favorite_watchlist_id	= sharedPreferences.getString(PreferenceConstants.WATCHLIST_FAV_ID, "");
		if(favorite_watchlist_id.length()>0){
			parameters.put(ParamConstants.FAVORATEID_TAG, favorite_watchlist_id);
		} else{
			parameters.put(ParamConstants.FAVORATEID_TAG, selectedWatchlist.getId());
		}
		
		//Edited by Thinzar, Fixes_Request-20150601,ReqNo. 3 - END
		
		
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		try {
			String response = AtpConnectUtil.getFavListStockCode(parameters);

			String[] stockCodes = AtpMessageParser.parseFavListStockCode(response);

			return stockCodes;
		} catch (FailedAuthenicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e.getMessage().equals("Session is expired")){
				loginThread.stopRequest();
				LoginActivity.QCAlive.stopRequest();
				isExpired = true;
				return null;
			}
		}
		return null;
		*/
		String response						= null;
		try {
			response	= AtpConnectUtil.getFavListStockCode(parameters);
			while(intRetryCtr < AppConstants.intMaxRetry && response == null){
				intRetryCtr++;
				response 	= AtpConnectUtil.getFavListStockCode(parameters);
			}
			intRetryCtr	= 0;

			if(response == null)
				throw new Exception(ErrorCodeConstants.FAIL_GET_WATCHLIST_STOCK);
		}catch(Exception e){
			throw e;
		}
		
		return AtpMessageParser.parseFavListStockCode(response);
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}

	public void refreshWatchlist() {
		new watchListRefresh().execute();
	}

	private class watchListRefresh extends AsyncTask<View, Void, List<StockSymbol>>{

		private AsyncTask<View, Void, List<StockSymbol>> updateTask = null;
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception = null;
		
		@Override
		protected void onPreExecute() {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				//System.out.println("==debug: asynctask watchListRefrsh");
				updateTask = this;
				if(!dialog.isShowing()){
					dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! WatchlistActivity.this.isFinishing() )
						dialog.show();
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
			super.onPreExecute();
		}

		@Override
		protected List<StockSymbol> doInBackground(View... params) {
			// TODO Auto-generated method stub

			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			String[] watchlistSymbols = WatchlistActivity.this.getWatchlistSymbols();
			*/
			String[] watchlistSymbols	= null;
			try{
				watchlistSymbols = WatchlistActivity.this.getWatchlistSymbols();
			}catch(FailedAuthenicationException e){
				if(loginThread != null)
					loginThread.stopRequest();
				
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				isExpired = true;
			}catch(Exception e){
				exception = e;
				return null;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			if(isExpired)
				return null;

			if(watchlistSymbols==null)
				return null;

			if (watchlistSymbols.length != 0) {
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				Map parameters = new HashMap();
				parameters.put(ParamConstants.USERPARAM_TAG, userParam);
				parameters.put(ParamConstants.SYMBOLCODES_TAG, watchlistSymbols);

				try {
					String response = QcHttpConnectUtil.imageQuote(parameters, CommandConstants.IMAGE_QUOTE_DETAIL);
				*/
				
				try {
					String response = QcHttpConnectUtil.imageQuote(watchlistSymbols, CommandConstants.IMAGE_QUOTE_DETAIL);
					while(intRetryCtr < AppConstants.intMaxRetry && response == null){
						intRetryCtr++;
						response = QcHttpConnectUtil.imageQuote(watchlistSymbols, CommandConstants.IMAGE_QUOTE_DETAIL);
					}
					intRetryCtr		= 0;
					
					if(response == null)				
						throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					
					stockSymbols 	= QcMessageParser.parseStockSymbol(response);
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				} catch (FailedAuthenicationException e) {
					e.printStackTrace();
					new watchListRefresh().execute(); // might cause infinite loop if FailedAuthenicationException continuously thrown
					return null;
				}
				*/
				}catch(Exception e){
					exception = e;
				}
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			} else {
				stockSymbols 		= new ArrayList<StockSymbol>();
			}

			return stockSymbols;
		}

		@Override
		protected void onPostExecute(List<StockSymbol> stockSymbols) {
			super.onPostExecute(stockSymbols);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if(isExpired){
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
					WatchlistActivity.this.sessionExpired();
					return;
				}
				
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							finish();
						}
					});
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! WatchlistActivity.this.isFinishing() )
						alertException.show();
					
					return;
				}
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
				String[] symbolCodes = null;
				//Diyana add- sorting name ascending on first launch
				if(isClickFromLabelName==true){
					if(StockSymbol.getSaveCLicked(getApplicationContext(), "namelabel").equals("asc")){
						StockSymbol.saveCLicked(getApplicationContext(), "desc","namelabel");
						Collections.sort(stockSymbols, StockSymbol.DESC_ORDER);
					}else if( StockSymbol.getSaveCLicked(getApplicationContext(),"namelabel").equals("desc")){
						StockSymbol.saveCLicked(getApplicationContext(),"asc","namelabel");
						 Collections.sort(stockSymbols, StockSymbol.ASCENDING_ORDER);
					}
			        			
					isClickFromLabelName=false;
				}else{		
					StockSymbol.saveCLicked(getApplicationContext(),"asc","namelabel");
			        Collections.sort(stockSymbols, StockSymbol.ASCENDING_ORDER);			
				}
		    	//Diyana add- End sorting name ascending on first launch
		        
				if (stockSymbols != null) {
					/* Mary@20130123 - Change_Request-20130104, ReqNo.5
					WatchlistActivity.this.constructTableView();
					*/
					WatchlistActivity.this.constructTableView(strSelectedFilterExchangeType);
					// retake symbol code to avoid stock symbol that does exist in current connected feed exchange to be re-request
					symbolCodes		= SystemUtil.getSymbolCodes(stockSymbols);
					refreshThread	= ( (StockApplication) WatchlistActivity.this.getApplication() ).getRefreshThread();
					if (refreshThread == null) {
						// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
						if(AppConstants.hasLogout){
							finish();
						}else{
						// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
							int rate = sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
							/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
							refreshThread = new RefreshStockThread(userParam, symbolCodes, rate);
							*/
							refreshThread = new RefreshStockThread( AppConstants.QCdata.getStrUserParam(), symbolCodes, rate);
							refreshThread.start();
							( (StockApplication) WatchlistActivity.this.getApplication() ).setRefreshThread(refreshThread);
						}
					/* Mary@20130924 - Fixes_Request-20130711, ReqNo.22
					} else {
					*/
					/* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
					}else if( refreshThread.isPaused() ){
						refreshThread.resumeRequest();
						refreshThread.setSymbolCodes(symbolCodes);
					*/
					}
					refreshThread.setRegisteredListener(WatchlistActivity.this);
					// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
					refreshThread.setSymbolCodes(symbolCodes);
					
					if( refreshThread.isPaused() ){
						refreshThread.resumeRequest();
					}
					// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END
	
					loginThread = ( (StockApplication) WatchlistActivity.this.getApplication() ).getDoubleLoginThread();
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
					if(loginThread == null){
						if(AppConstants.hasLogout){
							finish();
						}else{
							loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
							if (AppConstants.brokerConfigBean.isEnableEncryption())
								loginThread.setUserParam(AppConstants.userParamInByte);
							loginThread.start();
							((StockApplication) WatchlistActivity.this.getApplication()).setDoubleLoginThread(loginThread);
						}
					}
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
					loginThread.setRegisteredListener(WatchlistActivity.this);
				}
				
				// Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - START
				isEditMode	= false;
				WatchlistActivity.this.editButton.setText(getResources().getString(R.string.watchlist_edit_btn));
				WatchlistActivity.this.setupEditButton();
				// Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - END
	
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				if(dialog != null && dialog.isShowing() )
					dialog.dismiss();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

	}

	public void onSaveInstanceState(Bundle savedInstanceState) {
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		savedInstanceState.putString("userParam", userParam);
		*/
		//reverse the order with super method
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putString("userParam", AppConstants.QCdata.getStrUserParam() );
	}

	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		userParam = savedInstanceState.getString("userParam");
		*/
	}


	/* Mary@20130123 - Change_Request-20130104, ReqNo.5
	private void constructTableView() {
	*/
	private void constructTableView(String selectedFilterExchangeType){
		tableLayout 			= (TableLayout) findViewById(R.id.symbolTable);
		tableLayout.removeAllViews();

		changeButtons 			= new ArrayList<Button>();
		nameLabels 				= new ArrayList<TextView>();
		priceLabels 			= new ArrayList<TextView>();
		/* Mary@20130718 - Change_Request-20130424, ReqNo.13
		// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13
		exchgLabels				= new ArrayList<TextView>();
		*/
		volumeLabels 			= new ArrayList<TextView>();
		valueLabels				= new ArrayList<TextView>();
		highLabels 				= new ArrayList<TextView>();
		lowLabels 				= new ArrayList<TextView>();
		tradeLabels 			= new ArrayList<TextView>();
		openLabels 				= new ArrayList<TextView>();
		// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.12
		filteredStockSymbols	= new ArrayList<StockSymbol>();
		//sini
		int id 					= 0;

		if(stockSymbols != null) { //added nullCheck
			for (Iterator<StockSymbol> itr = stockSymbols.iterator(); itr.hasNext(); ) {
				StockSymbol symbol = itr.next();
				/* Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
				tableView(symbol, id);
				id++;
				*/
				StockSymbol tempSS = null;
				if (strSelectedFilterExchangeType == null) {
					tempSS = symbol;
				} else {
					if (strSelectedFilterExchangeType.equalsIgnoreCase(ALL_TYPE_KEY)) {
						tempSS = symbol;
					} else {
						/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
						String strCurrSymbolCode		= symbol.getSymbolCode();
						String strCurrTradeExchgCode	= strCurrSymbolCode.split("\\.")[1];
						*/
						String strCurrTradeExchgCode = symbol.getSymbolCode().substring((symbol.getSymbolCode().lastIndexOf(".") + 1), symbol.getSymbolCode().length());
						strCurrTradeExchgCode = (strCurrTradeExchgCode.length() > 1 && strCurrTradeExchgCode.toUpperCase().endsWith("D")) ? strCurrTradeExchgCode.substring(0, strCurrTradeExchgCode.length() - 1) : strCurrTradeExchgCode;
						if (strSelectedFilterExchangeType.equalsIgnoreCase(strCurrTradeExchgCode))
							tempSS = symbol;
					}
				}

				if (tempSS != null) {
					tableView(tempSS, id);
					id++;

					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.12
					filteredStockSymbols.add(tempSS);
				}
				// Mary@20130123 - Change_Request-20130104, ReqNo.5 - END

				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
				if (refreshThread != null)
					refreshThread.setSymbolCodes(SystemUtil.getSymbolCodes(filteredStockSymbols));
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END
			}
		}
	}
	
	private void tableView(StockSymbol stockSymbol, int intID){
		final TableRow tableRow		= new TableRow(this);   
		tableRow.setId(intID);
		tableRow.setLayoutParams(new TableLayout.LayoutParams( TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
		tableRow.setWeightSum(1);
		
		View rowView 				= this.getLayoutInflater().inflate(R.layout.symbol_row, null);  
		final LinearLayout rowLayout= (LinearLayout) rowView.findViewById(R.id.rowLayout);

		TextView nameLabel 			= (TextView) rowView.findViewById(R.id.nameLabel);
		/* Mary@20130718 - Change_Request-20130424, ReqNo.13
		nameLabel.setText(stockSymbol.getStockName());
		*/
		nameLabel.setText( stockSymbol.getStockName(false) );
		nameLabels.add(nameLabel);

		
		final TextView priceLabel = (TextView) rowView.findViewById(R.id.priceLabel);
		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20 - START
		final String[] symbolExchg = stockSymbol.getSymbolCode().split("\\.");
		if(symbolExchg[1].equals("JKD")){
		*/
		final String strSymbolExchg = stockSymbol.getSymbolCode().substring( (stockSymbol.getSymbolCode().lastIndexOf(".") + 1), stockSymbol.getSymbolCode().length() );
		/*
		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
			priceLabel.setText( FormatUtil.formatPrice2(stockSymbol.getLastDone(), "-") );
		}else{
			priceLabel.setText( FormatUtil.formatPrice(stockSymbol.getLastDone(), "-") );
		}
		*/
		//Added by Thinzar, Fixes_Request-20160101, ReqNo.3
		priceLabel.setText(FormatUtil.formatPrice(stockSymbol.getLastDone(), "-", strSymbolExchg));
		
		if (stockSymbol.getPriceChange() > 0) {
			/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO'  status
			priceLabel.setTextColor(Color.parseColor("#33CC33"));
			*/
			priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
		} else if (stockSymbol.getPriceChange() < 0) {
			/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO'  status
			priceLabel.setTextColor(Color.parseColor("#CC3333"));
			*/
			priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
		} else {
			/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO'  status
			priceLabel.setTextColor(Color.BLACK);
			*/
			priceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
		}

		priceLabel.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				selectedRow				= rowLayout;
				selectedRowId 			= tableRow.getId();

				rowLayout.setBackgroundDrawable(null);
				rowLayout.setBackgroundColor(Color.CYAN);
				
				int id 					= tableRow.getId();
				/* Mary@20130618 - Fixes_Request-20130523, ReqNo.12
				StockSymbol stockSymbol = stockSymbols.get(id);
				*/
				StockSymbol stockSymbol = filteredStockSymbols.get(id);
				Intent intent = new Intent();
				intent.putExtra(ParamConstants.SYMBOLCODE_TAG, stockSymbol.getSymbolCode());
				intent.putExtra(ParamConstants.PARAM_STR_RESTART_ACTIVITY, true);
				intent.setClass(WatchlistActivity.this, StockActivity.class);
				startActivity(intent);
			}

		});
		priceLabels.add(priceLabel);

		final Button changeButton = (Button) rowView.findViewById(R.id.changeButton); 
		/*
		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
			changeButton.setText(FormatUtil.formatPriceChange2(stockSymbol.getPriceChange()));
		else
			changeButton.setText(FormatUtil.formatPriceChange(stockSymbol.getPriceChange()));
		*/
		//Added by Thinzar, Fixes_Request-20160101, ReqNo.3
		changeButton.setText(FormatUtil.formatPriceChangeByExchange(stockSymbol.getPriceChange(), strSymbolExchg));
		
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
		if (symbol.getPriceChange()<0) {
			changeButton.setBackgroundResource(R.drawable.buttonchg_red70x28);
		} else {
			changeButton.setBackgroundResource(R.drawable.buttonchg_grn70x28);
		}
		*/
		changeButton.setBackgroundResource( (stockSymbol.getPriceChange()<0) ? R.drawable.buttonchg_red70x28 : R.drawable.buttonchg_grn70x28);
		changeButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				WatchlistActivity.this.changeButtonPercentage();
			}

		});
		changeButtons.add(changeButton);

		tableRow.addView(rowView);
		
		//Change_Request-20170119, ReqNo.1 - START
		ArrayList<String> menuList	= new ArrayList<String>();
		menuList.add(getResources().getString(R.string.longclick_view_stock_news));
		menuList.add(getResources().getString(R.string.longclick_view_stock_chart));
		if( !AppConstants.noTradeClient && AppConstants.brokerConfigBean.enableTrade() ){
			menuList.add(getResources().getString(R.string.longclick_trade));
		}
		
		final LongClickAdapter adapter = new LongClickAdapter(WatchlistActivity.this, menuList);
		
		final StockSymbol symbol = stockSymbol;
		tableRow.setOnLongClickListener(new OnLongClickListener(){

			@Override
			public boolean onLongClick(View v) {
				
				AlertDialog.Builder longClickBuilder	= new AlertDialog.Builder(WatchlistActivity.this);
				longClickBuilder.setTitle(symbol.getStockName(false))
				.setAdapter(adapter, new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int item) {
				    	
				    	
				    	switch(item){
				    	case 0:
				    		startStockActivityIntent(symbol.getSymbolCode(), INT_STOCK_NEWS_PAGE_POS);
				    		break;
				    	case 1:
				    		startStockActivityIntent(symbol.getSymbolCode(),INT_STOCK_CHART_PAGE_POS);
				    		break;
				    	case 2:
				    		new FetchStockInfoAndTrade().execute(symbol.getSymbolCode());
				    		break;
				    	}
				    }
				})
				.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				longClickBuilder.show();
				
				return false;
			}
			
		});
		//Change_Request-20170119, ReqNo.1 - END

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
		if ((id+1)%2 != 0) {
			rowLayout.setBackgroundResource(R.drawable.tablerow_dark);
		} else {
			rowLayout.setBackgroundResource(R.drawable.tablerow_light);
		}
		*/
		//rowLayout.setBackgroundResource( ( (intID+1)%2 != 0 ) ? R.drawable.tablerow_dark : R.drawable.tablerow_light );	//Commented: Change_request-20160101, RequestNo.3

		final View rowExpandView = WatchlistActivity.this.getLayoutInflater().inflate(R.layout.symbol_expand_row, null);
		rowExpandView.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				priceLabel.performClick();
			}
		});
		populateExpandViewData(rowExpandView, stockSymbol);

		tableRow.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				// Mary@20130715 - avoid nullPointerException
				if(rowExpandView != null){
					/* Mary@20130618 - Fixes_Request-20130523, ReqNo.12
					StockSymbol symbol	= stockSymbols.get(tableRow.getId());
					*/
					StockSymbol symbol	= filteredStockSymbols.get(tableRow.getId());
					if( ! symbol.isExpand() ){
						rowLayout.addView(rowExpandView); 
						/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
						if ((tableRow.getId()+1)%2 != 0) {
							rowLayout.setBackgroundResource(R.drawable.tablerow320x88_dark);
						} else {
							rowLayout.setBackgroundResource(R.drawable.tablerow320x88_light);
						}
						*/
						//rowLayout.setBackgroundResource( ( (tableRow.getId()+1)%2 != 0 ) ? R.drawable.tablerow320x88_dark : R.drawable.tablerow320x88_light);	//Commented: Change_request-20160101, RequestNo.3
						
						symbol.setExpand(true);
					}else{
						/* Mary@20130604 - comment unused variable
						int childCount = rowLayout.getChildCount();
						*/
						rowLayout.removeViewAt(1);
						symbol.setExpand(false);
						/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
						if ((tableRow.getId()+1)%2 != 0) { 
							rowLayout.setBackgroundResource(R.drawable.tablerow_dark);
						} else {
							rowLayout.setBackgroundResource(R.drawable.tablerow_light);
						}
						*/
						//rowLayout.setBackgroundResource( ( (tableRow.getId()+1)%2 != 0 ) ? R.drawable.tablerow_dark : R.drawable.tablerow_light );		//Commented: Change_request-20160101, RequestNo.3
					}
				}
			}
		});
		// Add the TableRow to the TableLayout
		tableLayout.addView(tableRow, new TableLayout.LayoutParams(	TableRow.LayoutParams.MATCH_PARENT,	TableRow.LayoutParams.WRAP_CONTENT));
	}

	private void changeButtonPercentage() {
		int index = 0;

		for (Iterator<Button> itr=changeButtons.iterator(); itr.hasNext(); ) {
			Button changeButton		= itr.next();
			/* Mary@20130618 - Fixes_Request-20130523, ReqNo.12
			StockSymbol symbol		= stockSymbols.get(index);
			*/
			StockSymbol symbol		= filteredStockSymbols.get(index);
			/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
			String[] symbolExchg 	= symbol.getSymbolCode().split("\\.");
			*/
			String strSymbolExchg 	= symbol.getSymbolCode().substring( (symbol.getSymbolCode().lastIndexOf(".") + 1), symbol.getSymbolCode().length() );

			if(isDisplayPercentage){
				/*
				if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
					changeButton.setText(FormatUtil.formatPriceChange2(symbol.getPriceChange()));
				else
					changeButton.setText(FormatUtil.formatPriceChange(symbol.getPriceChange()));
				*/
				changeButton.setText(FormatUtil.formatPriceChangeByExchange(symbol.getPriceChange(), strSymbolExchg));
			}else{
				changeButton.setText(FormatUtil.formatPricePercent(symbol.getChangePercent()));
			}
			index++;
		}
		/* Mary@20120924 - Fixes_Request-20120815, ReqNo.3
		if (isDisplayPercentage) {
			isDisplayPercentage = false;
		} else {
			isDisplayPercentage = true;
		}
		*/
		isDisplayPercentage	= !isDisplayPercentage;
	}

	private void populateExpandViewData(View expandView, StockSymbol symbol) {
		// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
		int intLotSize		= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? symbol.getIntLotSize() : 1;
						
		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
		String[] symbolExchg = symbol.getSymbolCode().split("\\.");
		*/
		String strSymbolExchg = symbol.getSymbolCode().substring( (symbol.getSymbolCode().lastIndexOf(".") + 1), symbol.getSymbolCode().length() );

		/* Mary@20130718 - Change_Request-20130424, ReqNo.13
		// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - START
		TextView exchgLabel 		= (TextView) expandView.findViewById(R.id.tvFieldExchgType);
		exchgLabel.setText(strSymbolExchg);
		exchgLabels.add(exchgLabel);
		// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - END
		*/
		
		TextView volumeLabel = (TextView) expandView.findViewById(R.id.volumeLabel);
		/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
		/* Mary@20120911 - Change_Request20120719, ReqNo.12
		volumeLabel.setText(FormatUtil.formatLong(symbol.getVolume()));
		/
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		volumeLabel.setText( FormatUtil.formatLong(symbol.getVolume() / intLotSize) );
		/
		volumeLabel.setText( FormatUtil.formatLong(symbol.getVolume() / intLotSize, "-") );
		*/
		volumeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (symbol.getVolume() / intLotSize), "-") );
		volumeLabels.add(volumeLabel);
		
		TextView valueLabel = (TextView) expandView.findViewById(R.id.valueLabel);
		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
		if(symbolExchg[1].equals("JKD")){
		*/
		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
			/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
			valueLabel.setText(FormatUtil.formatDouble2(symbol.getValue()));
			*/
			valueLabel.setText( FormatUtil.formatDouble2(symbol.getValue(), "-") );
		}else{
			/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
			valueLabel.setText(FormatUtil.formatDouble(symbol.getValue()));
			*/
			valueLabel.setText( FormatUtil.formatDouble(symbol.getValue(), "-") );
		}
		valueLabels.add(valueLabel);
		
		TextView openLabel = (TextView) expandView.findViewById(R.id.openLabel);
		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
		if(symbolExchg[1].equals("JKD")){
		*/
		/*
		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
			openLabel.setText( FormatUtil.formatPrice2(symbol.getOpen(), "-") );
		}else{
			openLabel.setText( FormatUtil.formatPrice(symbol.getOpen(), "-") );
		}*/
		openLabel.setText(FormatUtil.formatPrice(symbol.getOpen(), "-", strSymbolExchg));
		openLabels.add(openLabel);
		
		TextView highLabel = (TextView) expandView.findViewById(R.id.highLabel);
		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
		if(symbolExchg[1].equals("JKD")){
		*/
		/*
		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
			highLabel.setText(FormatUtil.formatPrice2(symbol.getHigh(), "-"));
		}else{
			highLabel.setText(FormatUtil.formatPrice(symbol.getHigh(), "-"));
		}*/
		highLabel.setText( FormatUtil.formatPrice(symbol.getHigh(), "-", strSymbolExchg));
		highLabels.add(highLabel);
		
		TextView lowLabel = (TextView) expandView.findViewById(R.id.lowLabel);
		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
		if(symbolExchg[1].equals("JKD")){
		*/
		/*
		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
			lowLabel.setText(FormatUtil.formatPrice2(symbol.getLow(), "-"));
		}else{
			lowLabel.setText(FormatUtil.formatPrice(symbol.getLow(), "-"));
		}*/
		lowLabel.setText(FormatUtil.formatPrice(symbol.getLow(), "-", strSymbolExchg));
		lowLabels.add(lowLabel);
		
		TextView tradeLabel = (TextView) expandView.findViewById(R.id.tradeLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		tradeLabel.setText(FormatUtil.formatLong(symbol.getTotalTrade()));
		*/
		tradeLabel.setText(FormatUtil.formatLong(symbol.getTotalTrade(), "-"));
		tradeLabels.add(tradeLabel);

	}

	@Override
	public void watchlistSelectedEvent(Watchlist watchList) {
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
		
		selectedWatchlist = watchList;

		watchlistButton.setText(watchList.getName());
		
		//Added by Thinzar, Change_Request-20150601, ReqNo.3 - START
		SharedPreferences.Editor editor 		= sharedPreferences.edit();
		editor.putString(PreferenceConstants.WATCHLIST_FAV_ID, selectedWatchlist.getId());
		editor.commit();
		//Added by Thinzar, Change_Request-20150601, ReqNo.3 - END
		
		this.refreshWatchlist();
	}

	@Override
	public void showKeyboardEvent(){
		((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
	}

	@Override
	public void hideKeyboardEvent(EditText editText){
		((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	public void watchlistUpdatedEvent(String watchlistName, String watchlistId, boolean isNew) {
		ArrayList<String>arr = new ArrayList<String>();
		arr.add(watchlistId);
		arr.add(watchlistName);
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
		if(isNew==true){
			arr.add("T");
		}else{
			arr.add("F");
		}
		*/
		arr.add(isNew ? "T" : "F");
		new executeWLUpdate().execute(arr);

	}

	private class executeWLUpdate extends AsyncTask<ArrayList<String>, Void, String>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				//System.out.println("==debug: asynctask executeWLUpdate");
				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! WatchlistActivity.this.isFinishing() )
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(ArrayList<String>... params) {
			ArrayList<String> Data	= params[0];
			String watchlistId 		= Data.get(0);
			String watchlistName 	= Data.get(1);
			String New 				= Data.get(2);
			boolean isNew 			= false;
			if( New.equals("T") )
				isNew = true;
			else if( New.equals("F") )
				isNew = false;
			
			String atpUserParam		= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			String senderCode 		= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
			String paramString 		= sharedPreferences.getString(PreferenceConstants.ATP_PARAM_STRING, null);

			Map<String, Object> parameters = new HashMap<String, Object>();

			if(isNew){
				int maxId	= 0;
				for (Iterator<Watchlist> itr = watchlists.iterator(); itr.hasNext(); ) {
					Watchlist watchlist	= itr.next();
					int id 				= Integer.parseInt(watchlist.getId());
					if(id > maxId)
						maxId = id;
				}
				parameters.put(ParamConstants.FAVORATEID_TAG, String.valueOf(maxId + 1));
				watchlistId	= String.valueOf(maxId + 1);
			}else{
				parameters.put(ParamConstants.FAVORATEID_TAG, watchlistId);
			}
			
			//Added by Thinzar, Change_Request-20150601, ReqNo.3 - START
			//update last viewed watchlist after create/update
			SharedPreferences.Editor editor 		= sharedPreferences.edit();
			editor.putString(PreferenceConstants.WATCHLIST_FAV_ID, watchlistId);
			editor.commit();
			//Added by Thinzar, Change_Request-20150601, ReqNo.3 - END
			
			/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
			if(AppConstants.EnableEncryption==false){
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			}else{
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			}
			*/
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if(AppConstants.EnableEncryption)
			*/
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			parameters.put(ParamConstants.FAVORATENAME_TAG, watchlistName);
			parameters.put(ParamConstants.PARAMSTRING_TAG, paramString);

			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			try {

				AtpConnectUtil.updateFavListName(parameters);

			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				if(e.getMessage().equals("Session is expired")){
					isExpired = true;
					if(refreshThread!=null){
						refreshThread.stopRequest();
					}
					loginThread.stopRequest();
					LoginActivity.QCAlive.stopRequest();
				}
				return null;
			}
			*/
			try {
				boolean isSuccess	= AtpConnectUtil.updateFavListName(parameters);
				while( intRetryCtr < AppConstants.intMaxRetry && ! isSuccess ){
					intRetryCtr++;
					isSuccess	= AtpConnectUtil.updateFavListName(parameters);
				}
				intRetryCtr			= 0;
				
				if( ! isSuccess )
					throw new Exception(ErrorCodeConstants.FAIL_UPDATE_WATCHLIST);
			}catch(FailedAuthenicationException e){
				isExpired = true;
				if(refreshThread!=null)
					refreshThread.stopRequest();

				// Added by Mary@20120731 - avoid null pointer exception
				if(loginThread!=null)
					loginThread.stopRequest();

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				exception	= e;
				return null;
			}catch(Exception e){
				Log.e("[WatchlistActivity]","[executeWLUpdate]Exception :" + e.getMessage() );
				exception = e;
				return null;
			}
			return watchlistId;
		}

		@Override
		protected void onPostExecute(String watchlistId){
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			try{
				if(dialog != null && dialog.isShowing() )
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
					dialog.dismiss();
	
				if(isExpired){
					WatchlistActivity.this.sessionExpired();
					return;
				}
				
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! WatchlistActivity.this.isFinishing() )
						alertException.show();
					
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
				new watchlist().execute(watchlistId);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	private class watchlist extends AsyncTask<String,Void,Boolean>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		private String watchlistId;

		@Override
		protected Boolean doInBackground(String... Id) {
			// TODO Auto-generated method stub
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			boolean success = WatchlistActivity.this.populateWatchlists();
			*/
			boolean success = false;
			try{
				//System.out.println("==debug: aysnctask watchlist");
				success = WatchlistActivity.this.populateWatchlists();
			}catch (FailedAuthenicationException e){
				isExpired = true;
				if(refreshThread!=null)
					refreshThread.stopRequest();
				
				if(loginThread != null)
					loginThread.stopRequest();
				
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
			}catch(Exception e){
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			watchlistId = Id[0];
			
			return success;
		}

		@Override
		protected void onPostExecute(Boolean success) {
			// TODO Auto-generated method stub
			super.onPostExecute(success);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				
				if( ! success ){
					if(isExpired){
						WatchlistActivity.this.sessionExpired();
						return;
					}
					
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					if(exception != null){
						alertException.setMessage( exception.getMessage() );
						
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! WatchlistActivity.this.isFinishing() )
							alertException.show();					
						
						exception	= null;
						return;
					}
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}
				
				try {
					// Added by Mary@20120801 - Fixes_Request-20120724, ReqNo.13 - START
					if( watchListView.isShowing() )
						watchListView.hide();
					// Added by Mary@20120801 - Fixes_Request-20120724, ReqNo.13 - END
					watchListView.setWatchlists(watchlists);
				} catch (Exception e) {			
					e.printStackTrace();
				}
				
				for (Iterator<Watchlist> itr=watchlists.iterator(); itr.hasNext(); ) {
					Watchlist watchlist = itr.next();
					if (watchlist.getId().equals(watchlistId)) {
						selectedWatchlist = watchlist;
						watchlistButton.setText( selectedWatchlist.getName() );
						WatchlistActivity.this.refreshWatchlist();
					}
				}
				/* Mary@20120815 - Fixes_Request-20120815, ReqNo.1
				if (watchlists.size()>0) {
				*/
					WatchlistActivity.this.setupEditButton();
				/* Mary@20120815 - Fixes_Request-20120815, ReqNo.1
				 	watchlistButton.setVisibility(View.VISIBLE);
				}
				*/
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	private class executeWLDelete extends AsyncTask<String, Void, String>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			String watchlistId				= params[0];
			String atpUserParam 			= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			String senderCode 				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

			Map<String, Object> parameters = new HashMap<String, Object>();
			/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
			if( ! AppConstants.EnableEncryption )
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			else
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte)
			*/
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if(AppConstants.EnableEncryption)
			*/
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			parameters.put(ParamConstants.FAVORATEID_TAG, watchlistId);

			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			try {
				AtpConnectUtil.deleteFavList(parameters);
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				if(e.getMessage().equals("Session is expired")){
					isExpired = true;
					if(refreshThread!=null){
						refreshThread.stopRequest();
					}
					loginThread.stopRequest();
					LoginActivity.QCAlive.stopRequest();
				}
				return null;
			}
			*/
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			try {
				boolean isSuccess	= AtpConnectUtil.deleteFavList(parameters);
				
				while( intRetryCtr < AppConstants.intMaxRetry && ! isSuccess ){
					intRetryCtr++;
					isSuccess	= AtpConnectUtil.deleteFavList(parameters);
				}
				intRetryCtr	= 0;
				
				if( ! isSuccess )
					throw new Exception(ErrorCodeConstants.FAIL_DEL_WATCHLIST);
			}catch (FailedAuthenicationException e){
				isExpired = true;
				if(refreshThread!=null)
					refreshThread.stopRequest();
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				exception	= e;
				return null;
			}catch(Exception e){
				exception = e;
				return null;
			}
			return watchlistId;
		}

		@Override 
		protected void onPostExecute(String watchlistId){
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(isExpired){
					WatchlistActivity.this.sessionExpired();
					return;
				}
				
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! WatchlistActivity.this.isFinishing() )
						alertException.show();
					
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				boolean success = WatchlistActivity.this.populateWatchlists();
				
				if(isExpired==true){
					WatchlistActivity.this.sessionExpired();
					return;
				}
				*/
				/* Mary@20130604 - comment unused variable
				boolean success = false;
				*/
				/* Mary@20130924 - Move to Asyn Task to avoid android.os.NetworkOnMainThreadException
				try{
					/* Mary@20130604 - comment unused variable
					success = WatchlistActivity.this.populateWatchlists();
					/
					WatchlistActivity.this.populateWatchlists();
				}catch (FailedAuthenicationException e){
					if(refreshThread!=null)
						refreshThread.stopRequest();
	
					if(loginThread != null)
						loginThread.stopRequest();
					
					if(LoginActivity.QCAlive != null)
						LoginActivity.QCAlive.stopRequest();
					
					WatchlistActivity.this.sessionExpired();
					return;
				}catch(Exception e){
					alertException.setMessage( e.getMessage() );
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! WatchlistActivity.this.isFinishing() )
						alertException.show();
					
					return;
				}
				*/
				new populateWatchlist().execute();
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - START
				if (watchlists.isEmpty()) {
					selectedWatchlist = null;
					editButton.setText(getResources().getString(R.string.watchlist_create_btn));
					editButton.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
							if(idleThread != null && ! isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
							
							WatchlistActivity.this.enterWatchlistName();
						}
					});
	
					tableLayout.removeAllViews();
	
					watchlistButton.setVisibility(View.INVISIBLE);
				}
				// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - END			
	
				if(selectedWatchlist!=null){
					if (selectedWatchlist.getId().equals(watchlistId)) {
						if (!watchlists.isEmpty()) {
							selectedWatchlist = watchlists.get(0);
							watchlistButton.setText(selectedWatchlist.getName());
							
							//Added by Thinzar, Change_Request-20150601, ReqNo.3 - START
							SharedPreferences.Editor editor 		= sharedPreferences.edit();
							editor.putString(PreferenceConstants.WATCHLIST_FAV_ID, selectedWatchlist.getId());
							editor.commit();
							//Added by Thinzar, Change_Request-20150601, ReqNo.3 - END
						}
					}
					WatchlistActivity.this.refreshWatchlist();
				}
	
				try {
					watchListView.setWatchlists(watchlists);
				} catch (Exception e) {			
					e.printStackTrace();
				}
				
				// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - START
				if( WatchlistActivity.this.watchlists.isEmpty() )
					WatchlistActivity.this.tableLayout.removeAllViews();
				
				WatchlistActivity.this.setupEditButton();
				// Mary@20120815 - Fixes_Request-20120815, ReqNo.1 - END
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

	}

	public void watchlistDeletedEvent(String watchlistId) {
		new executeWLDelete().execute(watchlistId);
	}

	private void enterWatchlistName() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			/* Mary@20130828 - Fixes_Request-20130711, ReqNo.17 - START
			final EditText input		= new EditText(this);
	
			AlertDialog.Builder alert	= new AlertDialog.Builder(this)
				.setIcon(android.R.attr.alertDialogStyle)
				.setTitle("Watchlist Name")
				.setMessage("Please enter new name:")
				.setView(input)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						String name		= input.getText().toString().trim();
						boolean isExists= false;
						for (Iterator<Watchlist> itr=watchlists.iterator(); itr.hasNext(); ) {
							Watchlist watchlist = itr.next();
							if (name.equalsIgnoreCase(watchlist.getName())) {
								isExists = true;
								break;
							}
						}
		
						if (isExists) {
							AlertDialog warning = new AlertDialog.Builder(WatchlistActivity.this)
								.setIcon(android.R.attr.alertDialogStyle)
								.setTitle("Duplicate Watchlist")
								.setMessage("Watchlist with the name is already existed.")
								.setPositiveButton("OK", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int whichButton) {
										dialog.dismiss();
									}
								})
								.create();
							
							// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
							if( ! WatchlistActivity.this.isFinishing() )
								warning.show();
							
							return;
						}
		
						WatchlistActivity.this.watchlistUpdatedEvent(name, "0", true);
					}
				})
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						dialog.dismiss();
					}
				});
			
			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! this.isFinishing() )
				alert.show();
			*/
			watchListView.enterWatchlistName("0", true);
			// Mary@20130828 - Fixes_Request-20130711, ReqNo.17 - END
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message	= handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7
			try{
				List<String> response = (List<String>) message.obj;
				/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
				if(response.get(2).equals("1102")){
				*/
				/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
				if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
				*/
				if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
					if(refreshThread != null)
						refreshThread.stopRequest();
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
					if(loginThread != null)
						loginThread.stopRequest();
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
					if(LoginActivity.QCAlive != null)
						LoginActivity.QCAlive.stopRequest();

					/* Mary@20130905 - GoogleCrashReport-20130822, ReqNo.7
					if( watchListView.isShowing() )
					*/
					if( watchListView != null && watchListView.isShowing() )
						watchListView.hide();
	
					if( WatchlistActivity.this.mainMenu.isShowing() )
						WatchlistActivity.this.mainMenu.hide();
					
					if( ! isFinishing() )
						WatchlistActivity.this.doubleLoginMessage( (String)response.get(1) );
				}
			// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo. 7 - END
		}
	};
	
	// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
	private void doExchangeList() {
		if( exchgListView.isShowing() ){
			exchgListView.hide();
		}else{
			if( mainMenu.isShowing() )
				mainMenu.hide();
			
			exchgListView.show(findViewById(R.id.rl_sw_TopNavBar), selectedExchangeMap);
		}
	}
	
	public void exchangeListSelectedEvent(Map<String, Object> exchangeMap, Sector sector) {
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
		
		selectedExchangeMap				= exchangeMap;
	
		strSelectedFilterExchangeType	= String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
		btnExchangeType.setText( String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );
		
		constructTableView(strSelectedFilterExchangeType);
		
		// Mary@20130618 - Fixes_Request-20120815, ReqNo.1 - START
		isEditMode	= false;
		editButton.setText(getResources().getString(R.string.watchlist_edit_btn));
		setupEditButton();
		// Mary@20130618 - Fixes_Request-20120815, ReqNo.1 - END
	}
	// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
	
	//Change_Request-20170119, ReqNo.1 - START
	private void startStockActivityIntent(String symbolCode, int pagerId){
		Intent stockIntent	= new Intent();
		stockIntent.setClass(WatchlistActivity.this, StockActivity.class);
		stockIntent.putExtra(ParamConstants.STOCK_PAGER_ID_TAG, pagerId);
		stockIntent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
		stockIntent.putExtra(ParamConstants.PARAM_STR_RESTART_ACTIVITY, true);
		
		startActivity(stockIntent);
	}
	
	private class LongClickAdapter extends ArrayAdapter<String> {
		
		private Context c;
		private ArrayList<String> forceTouchMenuList;

		private class ViewHolder {
			ImageView imgForceTouchMenu;
	        TextView txtForceTouchMenu;
	    }
		
		public LongClickAdapter(Context context, ArrayList<String> forceTouchMenuList) {
			super(context, 0, forceTouchMenuList);
			
			this.forceTouchMenuList	= forceTouchMenuList;
			this.c					= context;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder = null;
			
		    if (convertView == null) {
		    	
		    	LayoutInflater inflater = LayoutInflater.from(getContext());
		    	convertView = inflater.inflate(R.layout.custom_row_force_touch_menu, null, true);
				viewHolder = new ViewHolder();
		    	
				
				viewHolder.imgForceTouchMenu	= (ImageView)convertView.findViewById(R.id.imgForceTouchMenu);
		    	viewHolder.txtForceTouchMenu	= (TextView)convertView.findViewById(R.id.txtForceTouchMenu);
		    	
		    	viewHolder.txtForceTouchMenu.setText(forceTouchMenuList.get(position));
				
				//set image
				switch(position){
				case 0: viewHolder.imgForceTouchMenu.setImageResource(R.drawable.icon_forcetouch_news); break;
				case 1: viewHolder.imgForceTouchMenu.setImageResource(R.drawable.icon_forcetouch_chart); break;
				case 2: viewHolder.imgForceTouchMenu.setImageResource(R.drawable.icon_forcetouch_trade); break;
				}
		    	
		    	convertView.setTag(viewHolder);
		    } else {
		   
		    	viewHolder = (ViewHolder) convertView.getTag();
		    }
		    
			return convertView;
		}
	}
	
	private class FetchStockInfoAndTrade extends AsyncTask<String, Void, String[]> {
		private Exception exception 						= null;
		private StockInfo mStockInfo;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!WatchlistActivity.this.isFinishing()){
				dialog.setMessage(getResources().getString(R.string.fetching_stock_info_msg));
				dialog.show();
			}
		}

		@Override
		protected String[] doInBackground(String... params) {
			
			String[] symbolCode 	= { params[0] };
			
			String response = null;
			try {
				intRetryCtr = 0;
				
				response = QcHttpConnectUtil.imageQuote(symbolCode, CommandConstants.IMAGE_QUOTE_DETAIL);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = QcHttpConnectUtil.imageQuote(symbolCode, CommandConstants.IMAGE_QUOTE_DETAIL);
				}
				intRetryCtr = 0;

				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);

			} catch (Exception e) {
				exception = e;
			}

			if(response != null)
				mStockInfo = QcMessageParser.parseStockDetail(response);

			return symbolCode;
		}

		@Override
		protected void onPostExecute(String[] result) {
			super.onPostExecute(result);

			if (dialog != null && dialog.isShowing())
				dialog.dismiss();
			
			if (exception != null) {
				alertException.setMessage(exception.getMessage());

				if (!WatchlistActivity.this.isFinishing())
					alertException.show();
				return;
			} else{
				Intent tradeIntent 	= new Intent();
				tradeIntent.putExtra( ParamConstants.BID_ASK_PRICE_TAG, mStockInfo.getLastDone());
				tradeIntent.putExtra(ParamConstants.SYMBOLCODE_TAG, mStockInfo.getSymbolCode());
				tradeIntent.putExtra(ParamConstants.LOT_SIZE_TAG, Integer.toString( mStockInfo.getSharePerLot() ) );
				tradeIntent.putExtra(ParamConstants.INSTRUMENT_TAG, mStockInfo.getInstrument() );
				tradeIntent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, mStockInfo.isPayableWithCPF());
				tradeIntent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
				tradeIntent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
				tradeIntent.setClass(WatchlistActivity.this,	TradeActivity.class);

				startActivity(tradeIntent);
				
			}
		}
	}
	//Change_Request-20170119, ReqNo.1 - END
}