package com.n2nconnect.android.stock;

/*
 * Thinzar@20140624 -  Change_Request-20140701, ReqNo.1 
 * based on DefaultConfigBean.java and plistFile
 * We decided to follow iOS plist file, but Android has it's own variable names, 
 * this file is to match the prior defined variable names with that of plist file
 */
public class BHConfigConstants {
	
	public static final String version = "Version";
	
	public static final String strBrokerHouseBtnName			= "strBrokerHouseBtnName";
	public static final String strAnnouncementURL 				= "AnnouncementURL";		//=AnnouncementURL
	
	public static final String intBrokerHouseBtnImg				= "intBrokerHouseBtnImg";
	public static final String intLoginBgImg					= "intLoginBgImg";
	public static final String intLoginBtnBgImg					= "intLoginBtnBgImg";
	public static final String intHomeMenuIconBgImg				= "intHomeMenuIconBgImg";
	public static final String intTextColor						= "intTextColor";
	public static final String intLinkTextColor					= "intLinkTextColor";
	public static final String intTopNavBarBgImg				= "intTopNavBarBgImg";
	public static final String intTopNavBarBtnBgImg				= "intTopNavBarBtnBgImg";
	public static final String intAccSettingMenuIconBgImg		= "intAccSettingMenuIconBgImg";
	public static final String intExchgMenuIconBgImg			= "intExchgMenuIconBgImg";
	public static final String intIndiciesMenuIconBgImg			= "intIndiciesMenuIconBgImg";
	public static final String intLogoutMenuIconBgImg			= "intLogoutMenuIconBgImg";
	public static final String intNewsMenuIconBgImg				= "intNewsMenuIconBgImg";
	public static final String intOrdBookMenuIconBgImg			= "intOrdBookMenuIconBgImg";
	public static final String intPortfolioMenuIconBgImg		= "intPortfolioMenuIconBgImg";
	public static final String intSearchMenuIconBgImg			= "intSearchMenuIconBgImg";
	public static final String intStkInfoMenuIconBgImg			= "intStkInfoMenuIconBgImg";
	public static final String intMktSumMenuIconBgImg			= "intMktSumMenuIconBgImg";
	public static final String intTradeMenuIconBgImg			= "intTradeMenuIconBgImg";
	public static final String intWatchlistMenuIconBgImg		= "intWatchlistMenuIconBgImg";
	public static final String intBgImg							= "intBgImg";
	public static final String intAnnouncementMenuIconBgImg		= "intAnnouncementMenuIconBgImg";
	
	public static final String isEnableTrade						= "isEnableTrade";
	public static final String hasRegistration						= "hasRegistration";
	public static final String hasSignInTermNCondition				= "TermsNCondOnLoginPg";	//=TermsNCondOnLoginPg=
	public static final String showPortfolioDisclaimer				= "PFDisclaimer";	//=PFDisclaimer
	public static final String showPoweredByStatement				= "showPoweredByStatement";	
	public static final String showRememberMe						= "RememberMe";		//=RememberMe
	public static final String isCacheUsername						= "isCacheUsername";
	public static final String strPortfolioDisclaimerUrl			= "PFDisclaimerWebViewURL";		//=PFDisclaimerWebViewURL
	public static final String strRiskDisclosureStatementUrl		= "strRiskDisclosureStatementUrl";
	public static final String strLMSSponsor						= "LMSSponsor";		//=LMSSponsor
	public static final String strLMSServer							= "LMSServer";		//=LMSServer
	public static final String strLMSProduct						= "strLMSProduct";
	public static final String strLMSSubscriptionUrl				= "strLMSSubscriptionUrl";
	public static final String strLMSSubscriptionStatusUrl			= "strLMSSubscriptionStatusUrl";
	public static final String strAtpIP								= "ATPServer";		//=ATPServer
	public static final String intAtpPort							= "intAtpPort";
	public static final String strNewsIP							= "NewsServer";	//=NewsServer
	
	public static final String strChartIP							= "ChartServer";	//=ChartServer
	public static final String interactiveChart						= "InteractiveChart";	//=interactivechart
	
	public static final String strAppName							= "strAppName";	
	public static final String strAppIntro							= "strAppIntro";
	public static final String strAppConclude						= "strAppConclude";
	public static final String strKeyFeaturesTitle					= "strKeyFeaturesTitle";
	public static final String alKeyFeatures						= "alKeyFeatures";
	public static final String alTermsNConditions					= "alTermsNConditions";
	public static final String alTermsOfService						= "alTermsOfService";
	public static final String alPrivacyPolicy						= "alPrivacyPolicy";
	public static final String alDisclaimerOfWarranty				= "alDisclaimerOfWarranty";
	
	public static final String defaultIsAutoFillTradePrice			= "defaultIsAutoFillTradePrice";
	public static final String defaultIsAutoFillTradeQuantity		= "defaultIsAutoFillTradeQuantity";
	public static final	String defaultIntCustomTradeQuantity		= "defaultIntCustomTradeQuantity";
	public static final String defaultIntTradeQtyMeasurement		= "DefaultTradePrefQtyUnit"; //=DefaultTradePrefQtyUnit	//AppConstants.INT_TRADE_QTY_IN_LOT;
	
	public static final String isAutoFillTradePrice					= "isAutoFillTradePrice";
	public static final String isAutoFillTradeQuantity				= "isAutoFillTradeQuantity";
	public static final String intCustomTradeQuantity				= "intCustomTradeQuantity";
	public static final String intTradeQtyMeasurement				= "intTradeQtyMeasurement";	
	
	public static final String strDefaultPaymentType				= "strDefaultPaymentType";
	public static final String strDefaultCurrencyType				= "DefaultCurrency";	//=DefaultCurrency
	public static final String strBaseATPExchangeCode				= "PrimaryExchg";		//=PrimaryExchg
	public static final String strBaseQCExchangeCode				= "SecondaryExchg";	//=SecondaryExchg
	public static final String lstPymtTypeToBeFilter				= "lstPymtTypeToBeFilter";
	
	public static final String intViewQtyMeasurement				= "DefaultViewPrefQtyUnit";	//=DefaultViewPrefQtyUnit
	
	public static final String isConnectJavaQC						= "JavaQC";		//=JavaQC
	public static final String isGetNewKey							= "QCGetNewKey";		//=QCGetNewKey
	
	public static final String isEnableE2EEEncryption				= "E2EE_Encryption";	//=E2EE_Encryption
	public static final String isEnableEncryption					= "AES_Encryption";		//=AES_Encryption
	
	public static final String isEnablePinFeatures					= "isEnablePinFeatures";
	public static final String isEnablePasswordFeatures				= "EnablePassword";		//=EnablePassword
	public static final String isMultilevelMarketDepth             	= "MDLevel";	//=MDLevel

	public static final String isShowSkipConfirmation				= "TrdSkipConfirmation";		//=TrdSkipConfirmation
	public static final String isShowShortSell						= "isShowShortSell";
	public static final String lstExchgShowShortSell				= "lstExchgShowShortSell";	//=TrdShortSellIndicator
	public static final String strAboutURL							= "AboutURL";		//=AboutURL
	public static final String strTermsOfServiceURL					= "TOSURL";		//=TOSURL
	public static final String strPrivacyPolicyURL					= "PPURL";		//=PPURL
	public static final String getStrDisclaimerOfWarrantyURL		= "DWURL";						//=DWURL
	public static final String isImposeMaxLenOnQty					= "isImposeMaxLenOnQty";
	public static final String isShowLimitAtOrderPad				= "CreditLimitOrdPad";			//=CreditLimitOrdPad
	public static final String isShowLimitAtPortfolio				= "CreditLimitPortfolio";		//=CreditLimitPortfolio
	public static final String isShowLimitAtOrderBook				= "CreditLimitOrdBook";			//=CreditLimitOrdBook
	
	public static final String intMaxNoOfWatchlistCreate			= "MaxNoOfWatchlist";			//=MaxNoOfWatchlist
	public static final String intMaxNoOfStockAddToWatchlist		= "MaxNoOfStkInWatchlist";		//=MaxNoOfStkInWatchlist
	
	//public static final String isImposeMaxWatchlistCreate			= intMaxNoOfWatchlistCreate > 0 ? true : false;		//=LimitNoOfWatchlist, but no need
	//public static final String isImposeMaxStockAddToWatchlist		= intMaxNoOfStockAddToWatchlist > 0 ? true : false;	//=LimitNoOfStkInWatchlist, but no need
	
	//Added by Thinzar@20140826, Change_Request-20140801, ReqNo.6 
	public static final String isShowDMAAgreement					= "DMAAgreement";
	public static final String DMAWebViewURL						= "DMAWebViewURL";
	
	//Added by Thinzar@20140902, Fixes_Request-20140820, ReqNo. 11
	public static final String RestrictionToViewMY					= "RestrictionToViewMY";
	
	//Added by Thinzar@20140915, Change_Request-20140901, ReqNo. 6
	public static final String isSupportATPLoadBalance				= "ATPLoadBalance";
	
	//Added by Thinzar@20140926, Fixes_Request-20140820, ReqNo.29
	public static final String currencyRateLabel					= "currencyRateLabel";
	
	//Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 9
	public static final String minVersion							= "MinVersion";
	
	//Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 10
	public static final String strAppRequirement					= "strAppRequirement";
	
	//Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 11
	public static final String filterShowMarketArray				= "filterShowMarket";
	
	//Added by Thinzar@20141103, Change_Request-20140901, ReqNo. 14
	public static final String historicalChartServer				= "HistoricalChartServer";
	public static final String intradayChartServer					= "IntradayChartServer";
	
	//Added by Diyana@20160614, ReqNo. 1
	public static final String InteractiveChartServer				= "InteractiveChartServer";	//=interactivechart
	
	
	//Added by Thinzar@20141110, Change_Request-20141101, ReqNo. 2
	public static final String msgPromptForStopNIfTouchOrdType		= "msgPromptForStopNIfTouchOrdType";
	
	//Added by Thinzar@20141111, Change_Request-20141101, ReqNo. 4
	public static final String tradeValueLabelOnQuoteScreen			= "TradeValueLabelOnQuoteScreen";
	public static final String tradedAtLabelOnTimeNSale				= "TradedAtLabelOnTimeNSale";
	
	//Added by Thinzar@20150522, Change_Request-20150401, ReqNo. 3
	public static final String fundamentalNewsServer				= "FundamentalNewsServer";
	public static final String fundamentalSourceCode				= "FundamentalSourceCode";
	public static final String fundamentalNewsLabel					= "FundamentalNewsLabel";
	public static final String fundamentalDefaultReportCode			= "FundamentalDefaultReportCode";
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.10
	public static final String strIsEnableStockAlert				= "isStockAlertEnabled"; 		//"isEnableStockAlert"; edited variable name so that it won't get turned on on the previous versions
	public static final String strOrderHistoryDuration				= "orderHistoryDuration";
	public static final String strLstSectorNoTrading				= "lstSectorNoTrading";
	public static final String strHasBottomTextOnLoginPage			= "hasBottomTextOnLoginPage";
	
	//Added by Thinzar, Change_Request-20160722, ReqNo. 1 & 2
	public static final String strIsArchiveNews						= "isArchiveNews";
	public static final String strIsEnableQRLogin					= "isEnableQRLogin";
	public static final String strIsEnableScreener					= "isEnableScreener";
	public static final String strTheScreenerUrl					= "theScreenerUrl";
	
	//ChangeRequest-20160722, ReqNo.3
	public static final String strChartType							= "chartType";
	public static final String strNewChartServer					= "newChartServer";
	
	public static final String strArchiveNewsUrl					= "archiveNewsUrl";
	
	//Change_Request-20160101, ReqNo.11
	public static final String strIsATPServerSSL					= "isATPEnabledSSL";		//"isATPServerSSL";
	
	public static final String isTutorialAvailable					= "isTutorialAvailable";
	
	public static final String stockAlertPublicKey 					="stockAlertPublicKey";
	public static final String stockAlertJWT 						="stockAlertJWT";
	public static final String registerPushLink 					="registerPushLink";
	public static final String stockAlertUrl						="stockAlertUrl";
	
	//Change_Request-20160722, ReqNo.7
	public static final String strStkAlertDisclaimer				= "stkAlertDisclaimer";
	
	//Change_Request-20160722, ReqNo.8
	public static final String strIsEnableIBillionaire				= "isIBillionaireEnabled";		//"isEnableIBillionaire"; edited variable name so that it won't get turned on on the previous versions
	
	//Fixes_Request-20161101, ReqNo.3
	public static final String strArrExchgSupportModulus			= "arrExchgSupportModulus";
	
	//Change_Request-20161101, ReqNo.3
	public static final String strLstExchgShowPrivateOrder			= "lstExchgShowPrivateOrder";
	
	//Fixes_Request-20161101, ReqNo.4
	public static final String strLstExchgIBillionaire				= "lstIBillionaireExchg";
	
	//Fixes_Request-20161101, ReqNo.5
	public static final String strIntAtpPortSSL						= "intAtpPortSSL";
	
	//Change_Request-20170119, ReqNo.3
	public static final String strNewFundamentalSourceCode			= "NewFundamentalSourceCode";
	public static final String strNewFundamentalNewsServer			= "NewFundamentalNewsServer";
	public static final String strNewFundamentalNewsLabel			= "NewFundamentalNewsLabel";
	
	//Change_Request-20170119, ReqNo.5
	public static final String strIsDtiEnabled						= "isDtiEnabled";
	public static final String strDtiUrl							= "DtiUrl";
	public static final String strDtiApiUrl							= "DtiApi";
	public static final String strDtiDisclaimer						= "DtiDisclaimer";
	public static final String strIsShowDtiPopUp 					= "isShowDtiPopUp";
	
	//Change_Request-20170119, ReqNo.6
	public static final String strMoversLabelOnQuoteScreen			= "MoversLabelOnQuoteScreen";

	//Change_Request-20170119, ReqNo.8
	public static final String strMaxIdleTime						= "MaxIdleTime";
	
	//Change_Request-20170119, ReqNo.8
	public static final String strBrokerCode						= "brokerCode";
	
	//Change_Request-20160722, ReqNo.8
	public static final String strIsTouchIDEnabled					= "isTouchIDEnabled";
	public static final String strIsSecondLvlSecurityEnabled		= "isSecondLvlSecurityEnabled";
	
	//Change_Request-20170119, ReqNo.12
	public static final String strIsShowTopAtLacp					= "isShowTopAtLacp";
	
	//Change_Request-20170601, ReqNo.1
	public static final String strIsIbillionaireMenuEnabled			= "isIBillionaireMenuEnabled";
	
	//Change_Request-20170601, ReqNo.2
	public static final String strIsEnableElasticNews				= "isElasticNews";
	public static final String strElasticResearchReportsUrl			= "elasticResearchReportsUrl";
	public static final String strElasticReuterNewsUrl				= "elasticReuterNewsUrl";
	
	//Change_Request_20170601, ReqNo.3
	public static final String strIsEnableContactUs					= "enableContactUs";
	public static final String strContactEmail						= "contactEmail";
	
	//Change_Request-20170601, ReqNo.6
	public static final String strIsEnableContraAlert				= "enableContraAlert";
	
	//Change_Request-20170601, ReqNo.8
	public static final String strEnableSgxAppIntegration			= "enableSgxAppIntegration";

	//Change_Request-20170601, ReqNo.13
	public static final String strIsShowResearchReportNote			= "isShowResearchReportNote";

	//Added for AmSec and AmFutures
	public static final String strContactUsOnLoginPg 				= "ContactUsOnLoginPg";
	public static final String strEmailContact 						= "emailContact";
	public static final String strPhoneContact 						= "phoneContact";

	//Change_Request-20170601, ReqNo.13
	public static final String strStockAlertBHCode 					= "stockAlertBHCode";

	//Change_Request-20170601, ReqNo.17
	public static final String strIsShowPortfolioDelayFeed 			= "isShowPortfolioDelayFeed";
	public static final String strLstPortfolioDelayFeedExchg 		= "lstPortfolioDelayFeedExchg";

	//Change_Request-20170601, ReqNo.
	public static final String strIsLinkIBWebview 					= "isLinkIBWebview";
	public static final String strIBMainUrl 						= "iBMainUrl";

	//Fixes_Request-20170701, ReqNo.18
	public static final String strIsRequireLoginTcPlusToChangeHint 	= "isRequireLoginTcPlusToChangeHint";
	public static final String strTcPlusLink						= "TCPlusLink";

	//Change_Request-20170601, ReqNo.20
	public static final String strIsBlockRootedDevices 				= "isBlockRootedDevices";
}