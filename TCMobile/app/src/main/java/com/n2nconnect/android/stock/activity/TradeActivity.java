package com.n2nconnect.android.stock.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.OTPvalidationThread;
import com.n2nconnect.android.stock.OneFAValidationThread;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.EventEditText;
import com.n2nconnect.android.stock.custom.EventEditText.OnImeBackListener;
import com.n2nconnect.android.stock.model.CurrencyExchangeInfo;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.MarketDepth;
import com.n2nconnect.android.stock.model.OrderControlInfo;
import com.n2nconnect.android.stock.model.PaymentConfig;
import com.n2nconnect.android.stock.model.RDSInfo;
import com.n2nconnect.android.stock.model.StockCurrencyPaymentMethod;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;
//import com.sun.istack.internal.NotNull;
import android.support.annotation.NonNull;

public class TradeActivity extends CustomWindow 
implements OnRefreshStockListener, OnImeBackListener, OnDoubleLoginListener{//, OtpValidationListener {

	private static final int DIALOG_INVALID_ORDER_PRICE				= 0;
	private static final int DIALOG_INVALID_ORDER_QUANTITY 			= 1;
	private static final int DIALOG_INVALID_TRADING_PIN 			= 2;
	private static final int DIALOG_ORDER_TICKET_ERROR 				= 3;
	private static final int DIALOG_SUBMIT_ORDER_SUCCESS 			= 4;
	private static final int DIALOG_NO_TRADING_ACCOUNT 				= 5;
	private static final int DIALOG_DATE_PICKER 					= 6;
	private static final int DIALOG_SUBMIT_ORDER_FROM_SERVER 		= 7;
	private static final int DIALOG_INVALID_ORDER_PRICE_ADD 		= 8;
	private static final int DIALOG_INVALID_ORDER_PRICE_MINUS 		= 9;
	private static final int DIALOG_INVALID_ORDER_QUANTITY_ADD 		= 10;
	private static final int DIALOG_INVALID_ORDER_QUANTITY_MINUS	= 11;
	private static final int DIALOG_INVALID_STOPLIMIT_PRICE 		= 12;
	private static final int DIALOG_INVALID_STOPLIMIT_PRICE_ADD 	= 13;
	private static final int DIALOG_INVALID_STOPLIMIT_PRICE_MINUS 	= 14;

	// Added by Mary@20120717 - additional handling for error code 502
	private static final int DIALOG_ORDER_TICKET_SHORT_SELL_ERROR 	= 15;

	// Added by Mary@20121023 - Fixes_Request-20121018, ReqNo.7
	private static final int DIALOG_NO_ORDER_REVISE_DETECTED 		= 16;

	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
	private static final int DIALOG_INVALID_DISCLOSED_QUANTITY_ADD 	= 17;
	private static final int DIALOG_INVALID_DISCLOSED_QUANTITY_MINUS= 18;
	private static final int DIALOG_INVALID_MINIMUM_QUANTITY_ADD 	= 19;
	private static final int DIALOG_INVALID_MINIMUM_QUANTITY_MINUS	= 20;

	// Added by Mary@20130108 - Fixes_Request-20121221, ReqNo.24 - START
	private static final int DIALOG_PYMT_NOT_ALLOWED_BUY_ACTION		= 21;
	private static final int DIALOG_PYMT_NOT_ALLOWED_SELL_ACTION	= 22;

	//Kw@20130404 - Fixes_Request-20130314, ReqNo.11  
	private static final int DIALOG_EXCEED_5_BID_WARNING       		= 23;
	
	//Added by Thinzar@20140829, Fixes_Request-20140820, ReqNo.8
	private static final int DIALOG_INSUFFICIENT_BALANCE_CUT		= 24;
	
	//Added by Thinzar, ChangeRequest_20150401, ReqNo. 7
	private static final int DIALOG_INSUFFICIENT_BALANCE_NONCUT		= 25;
	
	//Added by Thinzar, Fixes_Request-20161101, ReqNo. 16
	private static final int DIALOG_INVALID_MINIMUM_QUANTITY		= 26;
	
	private static final String VALIDITY_GTD 						= "GTD";

	// Added by Mary@20130912 - Change_Request-20130424, ReqNo.12
	private static final String VALIDITY_GTM 						= "GTM";
	private static final String ORDER_TYPE_MARKET 					= "Market";
	private static final String ORDER_TYPE_STOPLIMIT 				= "StopLimit";
	private static final String ORDER_TYPE_LIMIT 					= "Limit";
	private static final String ORDER_TYPE_STOP 					= "Stop";
	private static final String ORDER_TYPE_MARKET_TO_LIMIT 			= "MarketToLimit";

	private String symbolCode;
	private String strStockCurrency;

	//Added by Kw@20130502 - Fix_Request-20130426, ReqNo.3
	private List<MarketDepth> marketDepths							= new ArrayList<MarketDepth>();
	private String bidaskPrice;

	// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12
	private String bidaskQty;
	private StockSymbol stockSymbol;
	private TextView priceLabel;
	private ImageView arrowImage;
	private TextView priceChangeLabel;
	private TextView percentChangeLabel;
	private TextView bidSizeLabel;
	private TextView bidPriceLabel;
	private TextView askSizeLabel;
	private TextView askPriceLabel;
	private EventEditText priceField;
	private EventEditText quantityField;
	private EventEditText stopLimitField;	//note: on the UI, it has been renamed to call "trigger price"

	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
	private EventEditText disclosedQtyField;
	private EventEditText minQtyField;

	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
	private ExchangeInfo exchangeInfo;
	private String selectedOrderType;
	private String selectedValidity;
	private String selectedCurrency;
	private String selectedPayment;
	private String previousSelectedPayment;	//Added by Thinzar@20140806, Change_Request-20140701, ReqNo 9
	private boolean isLayoutAdjust;
	private TradeAccount tradeAccount;
	private String tradingPin;
	private int actionType;
	private String alertMessage;
	private int orderPadType;

	// Added by Mary@20130925 - Change_Request-20130724, ReqNo.18
	private int orderDetailType;
	private float fltReviseStopLimit;
	private String strReviseOrderNo;
	private String strReviseOrdFixedNo;
	private float fltReviseOrdPrice;
	private long lngReviseOrdQty;
	private long lngMatchedQty;

	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
	private long lngReviseDisclosedQty;
	private long lngReviseMinQty;

	// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.5
	private long lngUnmatchQty;

	private int intReviseGTDYear;
	private int intReviseGTDMonth;
	private int intReviseGTDDay;
	private EditText dateField;
	private String strRevisePaymentType;
	private String strReviseOrderType;
	private String strReviseValidity;

	private String strReviseAction; 	// Added by Mary@20130108 - Fixes_Request-20121102, 17
	private boolean isPayableWithCPF;	// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4

	// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 START
	private Spinner spinnerOrdTyp;
	private Spinner spinnerValidity;
	private Spinner spinnerPayment;
	private Spinner spinnerCurrency;

	private Calendar calReviseGTD;
	private String senderCode;
	private String atpUserParam;
	private String strReviseCurrency;
	private String lotSize;
	private int selectedRowOrderType;
	private int selectedRowValidity;
	private int selectedRowCurrency;
	private int selectedRowPaymentType;

	private RefreshStockThread refreshThread;
	private DoubleLoginThread loginThread;
	private ProgressDialog dialog;

	// Added by Mary@20120717 - to confirm is Short Sell Activity proceed
	private boolean isProceedShortSell				= false;

	//Kw@20130404 - Fixes_Request-20130314, ReqNo.11
	private boolean isProceedExceed5Bid 			= false;
	private String instrument;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr = 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12 - START
	private String strQtyFieldAfterEdit				= null;
	private String strDisclosedQtyFieldAfterEdit	= null;
	private String strMinQtyFieldAfterEdit			= null;
	private String strPriceFieldAfterEdit 			= null;
	private String strStopLimitPriceFieldAfterEdit 	= null;
	private boolean isInitFromScratch				= true;
	private long lngCurrentLotSize					= 1;

	// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.12
	private ImageView detailImage;	
	
	// Added by Mary@20121009 - Fixes_Request-20120724, ReqNo.7
	private CheckBox skipConfirmBox;
	
	// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7
	private CheckBox shortSellBox;
	
	// Added by Mary@20121024 - Fixes_Request-20121023, ReqNo.10
	private String strSelectedStockExchangeCode		= null;
	
	// Added by Mary@20121123 - Change_Request-20121106, ReqNo.2
	private long lngViewCurrentLotSize				= 1;
	
	// Added by Mary@20121128 - Change_Request-20121106, ReqNo.1 - START
	private boolean isEditablePrice					= false;
	private boolean isEditableQuantity				= false;
	private boolean isEditableStopLimit				= false;
	private boolean isEditableOrderType				= false;
	private boolean isEditableValidity				= false;
	private boolean isEditableDisclosedQty			= false;
	private boolean isEditableMinQty				= false;
	private boolean isEditablePayment				= false;
	private boolean isEditableCurrency				= false;
	private boolean mustIncreasePrice				= false;
	private boolean mustDecreasePrice				= false;
	private boolean mustIncreaseQuantity			= false;
	private boolean mustDecreaseQuantity			= false;
	private boolean mustIncreaseDisclosedQty		= false;
	private boolean mustDecreaseDisclosedQty		= false;

	private boolean mustIncreaseStopLimitPrice		= false;
	private boolean mustDecreaseStopLimitPrice		= false;
	// Added by Mary@20121128 - Change_Request-20121106, ReqNo.1 - END
	
	// Mary@20121227 - Fixes_Request-20121102, ReqNo.11
	private double dblTradingBalance				= 0.00;
	
	// Added by Mary@20130220 - Fixes_Request-20130108, ReqNo.11 - START
	private String strTradeExchgCode				= null;
	private String strQCExchgCode					= null;

	// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
	private Map<String,String> tradeClientLimit   	= new HashMap<String,String>();
	TableLayout tableLayout;
	boolean isCreditLimit 							= false;
	
	// Added by Mary@20130408 - Fixes_Request-20130314, ReqNo.13
	private String strStoredTicketNo				= null;
	
	// Added by Mary@20130624 - Fixes_Request-20130523, ReqNo.17 - START
	private List<MarketDepth> lstNewMarketDepth;
	private List<TextView> counterLabels			= new ArrayList<TextView>();
	private List<TextView> bidPriceLabels			= new ArrayList<TextView>();
	private List<TextView> bidSizeLabels			= new ArrayList<TextView>();
	private List<TextView> askPriceLabels			= new ArrayList<TextView>();
	private List<TextView> askSizeLabels			= new ArrayList<TextView>();

	//Added by Thinzar@20140806 - Change_Request-20140701, ReqNo. 9
	String accountDisplay;
	
	//Added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 & 5
	private Spinner spnTriggerType;
	private Spinner spnTriggerDirection;
	private LinearLayout triggerTypeLayout;
	private LinearLayout triggerDirectionLayout;
	private String selectedTriggerType;
	private String selectedTriggerDirection;
	private String strReviseTriggerType;
	private String strReviseTriggerDirection;
	private List<String> triggerPriceTypes;
	private List<String> triggerPriceDirections;
	
	//Added by Thinzar@20140827, Fixes_Request-20140820, ReqNo. 7 & 8
	private RelativeLayout stopLimitLayout;
	private RelativeLayout discloseQtyLayout;
	private RelativeLayout minQtyLayout;
	private Double investmentPower		= 0.0;	//set default to zero

	//Added by Thinzar@20140902 - Fixes_Request-20140820, ReqNo.13 - START
	private String ORDERPAD_QTY_LABEL;		//"QTY";
	private String ORDER_QTY_UNIT_LABEL;	//"Order Qty (Unit)";
	private String ORDER_QTY_LOT_LABEL;		//"Order Qty (Lot)";
	private String DISCLOSED_QTY_UNIT_LABEL;//"Disclosed Qty (Unit)";
	private String DISCLOSED_QTY_LOT_LABEL;	//"Disclosed Qty (Lot)";
	private String MIN_QTY_UNIT_LABEL;		//"Min Qty (Unit)";
	private String MIN_QTY_LOT_LABEL;		//"Min Qty (Lot)";

	//Added by Thinzar, Change_Request-20150401, ReqNo.1
	private boolean isCUTAccount				= false;
	
	//Added by Thinzar, Change_Request-20161101, ReqNo.2
	private CheckBox privateOrderBox;
	private String PRIVATE_ORDER_LABEL			= "Private Order";
	
	//Change_Request-20170119, ReqNo.4
	private String externalOrderSource	= null;
	
	//Change_Request-20170601, ReqNo.8
	private int orderQtyFromSgx = 0;
	private String orderActionFromIntent;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.order_pad);
			this.title.setText(getResources().getString(R.string.trade_module_title));
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntTradeMenuIconBgImg() );
			
			Bundle extras 				= this.getIntent().getExtras();
			symbolCode					= extras.getString(ParamConstants.SYMBOLCODE_TAG);

			// Added by Mary@20121024 - Fixes_Request-20121023, ReqNo.10
			symbolCode					= symbolCode == null ? "" : symbolCode;
			orderPadType				= extras.getInt(ParamConstants.ORDERPAD_TYPE_TAG);

			// Added by Mary@20130925 - Change_Request-20130724, ReqNo.18
			orderDetailType				= extras.getInt(ParamConstants.ORDER_DETAILTYPE_TAG);
			strSelectedStockExchangeCode= symbolCode.substring( (symbolCode.lastIndexOf(".") + 1), symbolCode.length() );

			// Added by Mary@20130220 - Fixes_Request-20130108, ReqNo.11 - START
			strQCExchgCode				= strSelectedStockExchangeCode;
			if( strSelectedStockExchangeCode.length() > 1 && strSelectedStockExchangeCode.toUpperCase().endsWith("D") )
				strSelectedStockExchangeCode = strSelectedStockExchangeCode.substring(0, strSelectedStockExchangeCode.length() - 1);
			strTradeExchgCode			= strSelectedStockExchangeCode;

			// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4
			isPayableWithCPF			= extras.getBoolean(ParamConstants.IS_PAYABLE_WITH_CPF);
			
			//Change_Request-20170119, ReqNo.2, localization support
			ORDERPAD_QTY_LABEL			= getResources().getString(R.string.trade_label_qty);					//"QTY";
			ORDER_QTY_UNIT_LABEL		= getResources().getString(R.string.trade_label_order_qty_unit);		//"Order Qty (Unit)";
			ORDER_QTY_LOT_LABEL			= getResources().getString(R.string.trade_label_order_qty_lot);		//"Order Qty (Lot)";
			DISCLOSED_QTY_UNIT_LABEL	= getResources().getString(R.string.trade_label_dis_qty_unit);	//"Disclosed Qty (Unit)";
			DISCLOSED_QTY_LOT_LABEL		= getResources().getString(R.string.trade_label_dis_qty_lot);	//"Disclosed Qty (Lot)";
			MIN_QTY_UNIT_LABEL			= getResources().getString(R.string.trade_label_min_qty_unit);			//"Min Qty (Unit)";
			MIN_QTY_LOT_LABEL			= getResources().getString(R.string.trade_label_min_qty_lot);			//"Min Qty (Lot)";
			
			if(orderPadType==AppConstants.ORDER_PAD_TRADE){
				lotSize			= extras.getString(ParamConstants.LOT_SIZE_TAG);
				bidaskPrice	 	= extras.getString(ParamConstants.BID_ASK_PRICE_TAG);
				instrument 		= extras.getString(ParamConstants.INSTRUMENT_TAG);

				// Added by Mary@20120830-Change_Request-20120719, ReqNo.12
				bidaskQty		= extras.getString(ParamConstants.BID_ASK_QTY_TAG);
				
				//Change_Request-20170119, ReqNo.4
				if(extras.containsKey(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG))
					externalOrderSource		= extras.getString(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG);
				
				//Change_Request-20170601, ReqNo.8
				if(extras.containsKey(ParamConstants.ORDER_QUANTITY_TAG))
					orderQtyFromSgx = Integer.parseInt(extras.getString(ParamConstants.ORDER_QUANTITY_TAG));
				
				if(extras.containsKey(ParamConstants.ACTIONCODE_TAG))
					orderActionFromIntent = extras.getString(ParamConstants.ACTIONCODE_TAG);
				
				StockApplication application= (StockApplication) TradeActivity.this.getApplication();
				for( Iterator<ExchangeInfo> itr2=application.getExchangeInfos().iterator(); itr2.hasNext(); ){
					ExchangeInfo exchgInfo = itr2.next();
					if( exchgInfo.getTrdExchgCode() != null && strQCExchgCode.equals( exchgInfo.getQCExchangeCode() ) ){
						strTradeExchgCode				= exchgInfo.getTrdExchgCode();
						break;
					}
				}
				
				// Added by Mary@020121003 - Fixes_Request-20120815, ReqNo.16 - START
				selectedRowOrderType	= 0;
				selectedRowValidity		= 0;
				selectedRowPaymentType	= 0;
				selectedRowCurrency		= 0;
				// Added by Mary@020121003 - Fixes_Request-20120815, ReqNo.16 - END

			}else if (orderPadType == AppConstants.ORDER_PAD_REVISE){
				strReviseOrderNo 	= extras.getString(ParamConstants.ORDER_NO_TAG);
				strReviseOrdFixedNo	= extras.getString(ParamConstants.ORDER_FIXEDNO_TAG);

				DefinitionConstants.Debug("strReviseOrderNo=" + strReviseOrderNo);
				DefinitionConstants.Debug("strReviseOrdFixedNo=" + strReviseOrdFixedNo);

				fltReviseOrdPrice 	= extras.getFloat(ParamConstants.ORDER_PRICE_TAG);
				lngReviseOrdQty 	= extras.getLong(ParamConstants.ORDER_QUANTITY_TAG);

				// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
				lngReviseDisclosedQty= extras.getLong(ParamConstants.DISCLOSED_QUANTITY_TAG);
				lngReviseMinQty		= extras.getLong(ParamConstants.MINIMUM_QUANTITY_TAG);
				// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END

				lngMatchedQty		= extras.getLong(ParamConstants.ORDER_MATCHEDQTY_TAG);

				// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.5
				lngUnmatchQty		= extras.getLong(ParamConstants.ORDER_UNMATCHQTY_TAG);
				strRevisePaymentType= extras.getString(ParamConstants.PAYMENT_TYPE_TAG);
				strReviseOrderType 	= extras.getString(ParamConstants.ORDER_TYPE_TAG);
				strReviseValidity	= extras.getString(ParamConstants.ORDER_VALIDITY_TAG);

				//Fixes_Request-20170701, ReqNo.16
				strStoredTicketNo 	= extras.getString(ParamConstants.TICKETNO_TAG);
				/* Mary@20121025 - Fixes_Request-20121028, ReqNo.11 - START
				GTD 			= extras.getString(ParamConstants.GO_TO_DATE_TAG);
				*/
				//Edited by Thinzar-20150727, Fixes_Request-20150601, ReqNo.2 - START
				try{
					calReviseGTD 		= Calendar.getInstance();
					calReviseGTD.setTime( new SimpleDateFormat(AppConstants.PARAM_DATE_FORMAT).parse( extras.getString(ParamConstants.GO_TO_DATE_TAG) ) );
					intReviseGTDDay		= calReviseGTD.get(Calendar.DAY_OF_MONTH);
					intReviseGTDMonth	= calReviseGTD.get(Calendar.MONTH);
					intReviseGTDYear	= calReviseGTD.get(Calendar.YEAR);
				} catch(java.text.ParseException pe){
					intReviseGTDDay		= 0;
					intReviseGTDMonth	= 0;
					intReviseGTDYear	= 0;
				}
				//Edited by Thinzar-20150727, Fixes_Request-20150601, ReqNo.2 - END

				strReviseCurrency 	= extras.getString(ParamConstants.PAYMENT_CURRENCY_TAG);
				lotSize 			= extras.getString(ParamConstants.LOT_SIZE_TAG);
				fltReviseStopLimit 	= extras.getFloat(ParamConstants.ORDER_STOPLIMIT_TAG);
				instrument 			= extras.getString(ParamConstants.INSTRUMENT_TAG);
				
				// Added by Mary@20130108 - Fixes_Request-20121102, 17
				strReviseAction		= extras.getString(ParamConstants.ORI_ACTION_TYPE);

				// Added by Mary@020121003 - Fixes_Request-20120815, ReqNo.16 - START
				selectedOrderType	= strReviseOrderType;
				selectedValidity	= strReviseValidity;
				selectedPayment		= strRevisePaymentType;
				selectedCurrency	= strReviseCurrency;
				// Added by Mary@020121003 - Fixes_Request-20120815, ReqNo.16 - END
				
				//Added by Thinzar@20140819 - Change_Request_20140801,ReqNo.4 - START
				strReviseTriggerType = extras.getString(ParamConstants.TRIGGER_PRICE_TYPE_TAG);
				strReviseTriggerDirection = extras.getString(ParamConstants.TRIGGER_PRICE_DIRECTION_TAG);
				selectedTriggerType = strReviseTriggerType;
				selectedTriggerDirection = strReviseTriggerDirection;
			}
			
			// Added by Mary@20121114 - Fixes_Request-20120719, ReqNo.12
			if(strTradeExchgCode != null && !strTradeExchgCode.isEmpty())		//Condition Added, Fixes_Request-20160101, ReqNo.16
				AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, strTradeExchgCode);
			
			/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
			// Added by Mary@20120913 - Change_Request-20120719, ReqNo.12 - START
			lotSize					= lotSize == null ? "1": ( lotSize.trim().equals("0") || lotSize.trim().length() == 0 ? "1" : lotSize.trim() );
			*/
			lotSize					= lotSize == null ? "0": ( lotSize.trim().length() == 0 ? "0" : lotSize.trim() );
			lngCurrentLotSize		= AppConstants.brokerConfigBean.getIntTradeQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? Long.parseLong(lotSize) : 1;
			// Added by Mary@20120913 - Change_Request-20120719, ReqNo.12 - END
			
			// Added by Mary@20121123 - Change_Request-20121106, ReqNo.2
			lngViewCurrentLotSize	= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? Long.parseLong(lotSize) : 1;
			
			// Added by Mary@20121129 - Change_request-20121106, ReqNo.1 - START
			if (orderPadType == AppConstants.ORDER_PAD_REVISE){
				
				strPriceFieldAfterEdit			= String.valueOf(fltReviseOrdPrice);
				strStopLimitPriceFieldAfterEdit	= String.valueOf(fltReviseStopLimit);

				strDisclosedQtyFieldAfterEdit	= String.valueOf( lngCurrentLotSize == 0 ? 0 : (lngReviseDisclosedQty / lngCurrentLotSize) );
				strMinQtyFieldAfterEdit			= String.valueOf( lngCurrentLotSize == 0 ? 0 : (lngReviseMinQty / lngCurrentLotSize) );
				lngReviseOrdQty					= lngUnmatchQty;
				strQtyFieldAfterEdit			= String.valueOf( lngCurrentLotSize == 0 ? 0 : (lngReviseOrdQty / lngCurrentLotSize) );
				// Mary@20130314 - Fixes_Request-20130314, ReqNo.2 - END
			}
			// Added by Mary@20121129 - Change_request-20121106, ReqNo.1 - END

			alertMessage 			= "";

			// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException 			= new AlertDialog.Builder(TradeActivity.this)
										//.setIcon(android.R.attr.alertDialogStyle)
										.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,	int id) {
												dialog.dismiss();
											}
										})
										.create();

			// Added by Mary@20120913 - Change_Request-20120719, ReqNo.12
			isInitFromScratch		= true;
			
			this.loadOrderPad();
			
			// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.12 - START
			detailImage				= (ImageView) TradeActivity.this.findViewById(R.id.detailViewImage);	
			detailImage.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					Intent intent = new Intent();
					intent.putExtra(ParamConstants.FILTER_BY_EXCHG, true);
					intent.putExtra( ParamConstants.EXCHANGECODE_TAG, exchangeInfo.getTrdExchgCode() );
					intent.setClass(TradeActivity.this, SelectAccountActivity.class);
	
					TradeActivity.this.startActivityForResult(intent, AppConstants.SELECT_ACCOUNT_REQUEST);
				}
			});

			dateField 				= (EditText) TradeActivity.this.findViewById(R.id.dateField);

			// Added by KW@20120228 - Change_Request-20130225 , ReqNo.3 START
			if (orderPadType == AppConstants.ORDER_PAD_REVISE){
				dateField.setEnabled(false);
			}
			// Added by KW@20120228 - Change_Request-20130225 , ReqNo.3 END
			
			dateField.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					showDialog(DIALOG_DATE_PICKER);
				}
			});
	
			dateField.setOnFocusChangeListener(new OnFocusChangeListener() {
				public void onFocusChange(View view, boolean isFocus) {
					if(isFocus)
						showDialog(DIALOG_DATE_PICKER);
				}
			});
			// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.12 - END
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//Added by Thinzar@20140827, Fixes_Request-20140820, ReqNo. 7 - START
		stopLimitLayout		= (RelativeLayout)findViewById(R.id.stopLimitLayout);
		discloseQtyLayout	= (RelativeLayout)findViewById(R.id.discloseQtyLayout);
		minQtyLayout		= (RelativeLayout)findViewById(R.id.minQtyLayout);
		
		//Added by Thinzar@20140902 - Fixes_Request-20140820, ReqNo.13
		if(SystemUtil.isDerivative(strSelectedStockExchangeCode))	prepareLabelIfDerivative();

	}
	
	@Override
	public void onResume() {
		//DefinitionConstants.Debug("tradeDebug==TradeActivity, onResume()");

		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.1
		try{
			super.onResume();
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= TradeActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
			atpUserParam 				= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			senderCode 					= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

			String[] symbolCodes 		= {symbolCode};
	
			StockApplication application= (StockApplication) TradeActivity.this.getApplication();

			List<TradeAccount> tradeAccounts = application.getTradeAccounts();
			if (tradeAccounts == null){
				this.showDialog(TradeActivity.DIALOG_NO_TRADING_ACCOUNT);
			}else{
				if(AppConstants.selectedAcc == null){
					tradeAccount = tradeAccounts.get(0);
				}else{
					tradeAccount = AppConstants.selectedAcc;
				}
				
				if (tradeAccount == null)
					this.showDialog(TradeActivity.DIALOG_NO_TRADING_ACCOUNT);
			}
			
			List<ExchangeInfo> exchangeInfos	= application.getExchangeInfos();
			for(Iterator<ExchangeInfo> itr = exchangeInfos.iterator(); itr.hasNext();){
				ExchangeInfo Info	= itr.next();

				if( ( Info.getTrdExchgCode() != null 
						&& ( Info.getTrdExchgCode().equals(this.strSelectedStockExchangeCode) ||  Info.getTrdExchgCode().equals(this.strTradeExchgCode) ) 
					)
					|| ( Info.getQCExchangeCode() != null && ( Info.getQCExchangeCode().equals(this.strQCExchgCode) ) )
				)
					exchangeInfo = Info;
			}
			// Mary@20121024 - Fixes_Request-20121023, ReqNo.10 - END
			
			// Added by Mary@20121128 - Change_Request-20121106, ReqNo.1 - START
			/* Reviseable Category and Text Field */
			isEditablePrice				= false;
			isEditableQuantity			= false;
			isEditableStopLimit			= false;
			isEditableOrderType			= false;
			isEditableValidity			= false;
			isEditableDisclosedQty		= false;
			isEditableMinQty			= false;
			isEditablePayment			= false;
			isEditableCurrency			= false;
			
			mustIncreasePrice			= false;
			mustDecreasePrice			= false;
			mustIncreaseQuantity		= false;
			mustDecreaseQuantity		= false;
			mustIncreaseDisclosedQty	= false;
			mustDecreaseDisclosedQty	= false;
			mustIncreaseStopLimitPrice	= false;
			mustDecreaseStopLimitPrice	= false;
			
			String[] strArrCtrl			= (String[])exchangeInfo.getReviseCodes().toArray( new String[exchangeInfo.getReviseCodes().size()] );
			
			for(int i=0; i<strArrCtrl.length; i++){
				if( strArrCtrl[i].contains("P") ){
					isEditablePrice 		= true;
					
					if( strArrCtrl[i].equals("P+") )
						mustIncreasePrice	= true;
					else if( strArrCtrl[i].equals("P-") )
						mustDecreasePrice	= true;
					
				}else if( strArrCtrl[i].contains("Q") ){
					isEditableQuantity 		= true;
					
					if( strArrCtrl[i].equals("Q+") )
						mustIncreaseQuantity	= true;
					else if( strArrCtrl[i].equals("Q-") )
						mustDecreaseQuantity	= true;
					
				}
				/* Commented by Thinzar, Fixes_Request-20140929, ReqNo. 3
				else if( strArrCtrl[i].contains("L") ){
					isEditableStopLimit 	= true;
					
					if( strArrCtrl[i].equals("L+") )
						mustIncreaseStopLimitPrice	= true;
					else if( strArrCtrl[i].equals("L-") )
						mustDecreaseStopLimitPrice	= true;
					
				}*/
				else if( strArrCtrl[i].contains("D") ){
					isEditableDisclosedQty	= true;
					
					if( strArrCtrl[i].equals("D+") )
						mustIncreaseDisclosedQty	= true;
					else if( strArrCtrl[i].equals("D-") )
						mustDecreaseDisclosedQty	= true;
					
				}else if( strArrCtrl[i].contains("M") ){
					isEditableMinQty		 	= true;

				}else if( strArrCtrl[i].contains("O") ){
					isEditableOrderType		= true;
					
				}else if( strArrCtrl[i].contains("V") ){
					isEditableValidity		= true;
					
				}else if( strArrCtrl[i].contains("Y") ){
					isEditablePayment		= true;
					
				}else if( strArrCtrl[i].contains("T") ){
					isEditableCurrency		= true;
					
				}
			}
			// Added by Mary@20121128 - Change_Request-20121106, ReqNo.1 - END
			
			loginThread 				= ((StockApplication)TradeActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) TradeActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(TradeActivity.this);
				loginThread.resumeRequest();
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(TradeActivity.this);
			
			refreshThread 				= ( (StockApplication) this.getApplication() ).getRefreshThread();
			if(refreshThread == null){
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
				if(AppConstants.hasLogout){
					finish();
				}else{
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
					int rate 		= sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);

					refreshThread 	= new RefreshStockThread(AppConstants.QCdata.getStrUserParam(), symbolCodes, rate);
					refreshThread.start();
					( (StockApplication) this.getApplication() ).setRefreshThread(refreshThread);
				}
			}
			refreshThread.setRegisteredListener(this);
			// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29
			refreshThread.setSymbolCodes(symbolCodes);
			
			// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
			if( refreshThread.isPaused() ){
				refreshThread.resumeRequest();
			}
			// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END
			
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(StockApplication.getOTPvalidationThread() == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					((StockApplication) TradeActivity.this.getApplication()).setOTPvalidationThread(new OTPvalidationThread());
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			StockApplication.getOTPvalidationThread().setRegisteredListener(this);
			
			// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14 - START
			if(StockApplication.getOneFAValidationThread() == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					( (StockApplication) TradeActivity.this.getApplication() ).setOneFAValidationThread( new OneFAValidationThread() );
				}
			}
			StockApplication.getOneFAValidationThread().setRegisteredListener(this);
			// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14 - END
			
			new getClientLimit().execute();

			List<TradeAccount> Accounts = ( (StockApplication) TradeActivity.this.getApplication() ).getTradeAccounts();
			if(orderPadType == AppConstants.ORDER_PAD_REVISE || Accounts.size() <= 1)
				detailImage.setVisibility(View.GONE);
			// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.1 - START
			else
				detailImage.setVisibility(View.VISIBLE);
			// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.1 - END

			if (orderPadType == AppConstants.ORDER_PAD_TRADE) {
				dateField.setInputType(InputType.TYPE_NULL);
			}
			
			//Added by Thinzar, Change_Request-20140801,ReqNo.4&5 - START
			triggerPriceTypes 		= exchangeInfo.getTriggerPriceTypes();
			triggerPriceDirections 	= exchangeInfo.getTriggerPriceDirections();
			triggerTypeLayout 		= (LinearLayout)findViewById(R.id.triggerTypeLayout);
			triggerDirectionLayout 	= (LinearLayout)findViewById(R.id.triggerDirectionLayout);
			spnTriggerType 			= (Spinner)findViewById(R.id.spnTriggerType);
			spnTriggerDirection 	= (Spinner)findViewById(R.id.spnTriggerDirection);

			constructTriggerTypeAndDirection();
			//Added by Thinzar, Change_Request-20140801,ReqNo.4&5 - END
			
			toggleRelatedOrderTypeView();
			toggleRelatedValidityView();

			TextView Quantity 			= (TextView)findViewById(R.id.tvQuantityLabel);
			Quantity.setText(ORDERPAD_QTY_LABEL + "(x" + lngCurrentLotSize + ")");		//Edited for Fixes_Request-20140820, ReqNo.13
			
			prepareOrderPadFieldsVisibility();
			
			// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12 - START
			if(isInitFromScratch)
				isInitFromScratch = false;
			else
				repaintOrderPadScreen();
			// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12 - END
		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.1 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.1 - END
	}
	
	@Override
	protected void onPause() {
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onPause();
			
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
	
			if(refreshThread!=null)
				refreshThread.pauseRequest();

			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}
	
	
	private void submitTradeOrder() {
		
		new submitTrade().execute();
	}

	private class submitTrade extends AsyncTask<Void,Void,Exception>{
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13
		private boolean isDialogOwner	= false;
		private String response 		= null;

		private String priceText;
		private String quantityText;
		private String strMinQty;
		private String strDisclosedQty;
		private String stopLimit;
		private String dateString;

		private boolean isMinQtyFieldEnabled		= false;
		private boolean isDisclosedQtyFieldEnabled 	= false;

		private boolean isShortSellAware 	= false;
		private boolean isPrivateOrder 		= false;
		private boolean hasTriggerType 		= false;
		private boolean hasTriggerDirection = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13
				if(! TradeActivity.this.dialog.isShowing() ){
					TradeActivity.this.dialog.setMessage(getResources().getString(R.string.trade_progress_submitting_order));
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! TradeActivity.this.isFinishing() ){
						TradeActivity.this.dialog.show();
						// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13
						isDialogOwner	= true;
					}
				}

				//Edited by Thinzar, moved from workerThread
				priceText		= priceField.getText().toString().replace(",", "").trim();
				priceText 		= priceText.length()==0? "0": String.valueOf( Float.parseFloat(priceText) );

				quantityText 	= quantityField.getText().toString().replace(",", "").trim();
				strMinQty		= minQtyField.getText().toString().replace(",", "").trim();
				strDisclosedQty	= disclosedQtyField.getText().toString().replace(",", "").trim();
				stopLimit		= stopLimitField.getText().toString().replace(",", "").trim();
				dateString 		= dateField.getText().toString().trim();

				isMinQtyFieldEnabled 		= minQtyField.isEnabled();
				isDisclosedQtyFieldEnabled	= disclosedQtyField.isEnabled();

				isShortSellAware = shortSellBox.isChecked();
				if(privateOrderBox.getVisibility() == View.VISIBLE)
					isPrivateOrder 	 = privateOrderBox.isChecked();

				if(spnTriggerType.getVisibility() == View.VISIBLE && spnTriggerType.isEnabled()) {
					hasTriggerType = true;
					selectedTriggerType = spnTriggerType.getSelectedItem().toString();
				}

				if(spnTriggerDirection.getVisibility() == View.VISIBLE && spnTriggerDirection.isEnabled()) {
					hasTriggerDirection = true;
					selectedTriggerDirection = spnTriggerDirection.getSelectedItem().toString();
				}

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected Exception doInBackground(Void... arg0) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			
			try {

				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);

				strStoredTicketNo	= strStoredTicketNo == null ? UUID.randomUUID().toString() : strStoredTicketNo;
				parameters.put(ParamConstants.TICKETNO_TAG, strStoredTicketNo);
				// Mary@20130408 - Fixes_Request-20130314, ReqNo.13 - END
				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
				parameters.put(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
				parameters.put(ParamConstants.CLIENTCODE_TAG, tradeAccount.getClientCode());
				parameters.put(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
				parameters.put(ParamConstants.ACTIONCODE_TAG, TradeActivity.this.translateActionName(actionType));

				String strTradeSymbolCode	= symbolCode.substring( 0, symbolCode.lastIndexOf(".") ).concat(".").concat(strSelectedStockExchangeCode);
				parameters.put(ParamConstants.SYMBOLCODE_TAG, strTradeSymbolCode);
				// Mary@20121212 - ATP trade should have Trade exchange code instead of feed exchange code - END
				
				parameters.put( ParamConstants.ORDER_TIMESTAMP_TAG, FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S") );

				quantityText 		= quantityText.length()==0 ? "0" : quantityText;
				long Quantity 		= Long.parseLong(quantityText);
				Quantity			= Quantity * lngCurrentLotSize;
				quantityText 		= Long.toString(Quantity);
				// Mary@20120830 - Change_Request-20120719, ReqNo.12 - END
				parameters.put(ParamConstants.ORDER_QUANTITY_TAG, quantityText);
				
				// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1 - START

				if(isMinQtyFieldEnabled){
					strMinQty 				= strMinQty.length()==0 ? "0" : strMinQty;
					long lngMinQty 			= Long.parseLong(strMinQty);
					lngMinQty				= lngMinQty * lngCurrentLotSize;
					strMinQty 				= Long.toString(lngMinQty);
					parameters.put(ParamConstants.MINIMUM_QUANTITY_TAG, strMinQty);
				}else{
					parameters.put(ParamConstants.MINIMUM_QUANTITY_TAG, "");
				}

				if(isDisclosedQtyFieldEnabled){
					strDisclosedQty			= strDisclosedQty.length()==0 ? "0" : strDisclosedQty;
					long lngDisclosedQty 	= Long.parseLong(strDisclosedQty);
					lngDisclosedQty			= lngDisclosedQty * lngCurrentLotSize;
					strDisclosedQty 		= Long.toString(lngDisclosedQty);
					parameters.put(ParamConstants.DISCLOSED_QUANTITY_TAG, strDisclosedQty);
				}else{
					parameters.put(ParamConstants.DISCLOSED_QUANTITY_TAG, "");
				}

				// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1 - END
				
				parameters.put(ParamConstants.ORDER_SOURCE_TAG, "W");
				parameters.put(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
				//parameters.put(ParamConstants.EXCHANGECODE_TAG, DefinitionConstants.getTrdExchangeCode());
				parameters.put(ParamConstants.EXCHANGECODE_TAG, tradeAccount.getExchangeCode());

				parameters.put(ParamConstants.TRADINGPIN_TAG, tradingPin);

				priceText 			= priceText.length()==0? "0": String.valueOf( Float.parseFloat(priceText) );

				parameters.put( ParamConstants.ORDER_PRICE_TAG, priceText);

				//Change_Request-20140801, ReqNo.4&5. ATP's [OrderCtrl2] has been enhanced, some of ORDER_TYPE_ALSO got stopLimitField enabled
				if(selectedOrderType.equalsIgnoreCase(ORDER_TYPE_STOPLIMIT) || selectedOrderType.equalsIgnoreCase(ORDER_TYPE_STOP)){

					stopLimit		= stopLimit.length()==0? "0":String.valueOf( Float.parseFloat(stopLimit) );
					parameters.put(ParamConstants.ORDER_STOPLIMIT_TAG, stopLimit);
				}else{
					parameters.put(ParamConstants.ORDER_STOPLIMIT_TAG, "");
				}

				parameters.put(ParamConstants.ORDER_TYPE_TAG, selectedOrderType);
				parameters.put(ParamConstants.ORDER_VALIDITY_TAG, selectedValidity);
				parameters.put(ParamConstants.PAYMENT_TYPE_TAG, selectedPayment);
				parameters.put(ParamConstants.CURRENCY_TAG, selectedCurrency);
	
				if (actionType == AppConstants.TRADE_REVISE_ACTION || actionType == AppConstants.TRADE_CANCEL_ACTION) {
					parameters.put(ParamConstants.ORDER_NO_TAG, strReviseOrderNo);
					parameters.put(ParamConstants.ORDER_FIXEDNO_TAG, strReviseOrdFixedNo);
					parameters.put(ParamConstants.ORDER_MATCHEDQTY_TAG, lngMatchedQty);
				}
	
				if( selectedValidity.equals(VALIDITY_GTD) ) {
					Date expiryDate = FormatUtil.parseDateString(dateString, AppConstants.PARAM_DATE_FORMAT);
					parameters.put( ParamConstants.ORDER_EXPIRYDATE_TAG, FormatUtil.formatDateString(expiryDate, "yyyyMMddhhmmss.S") );
				}
				
				// Added by Mary@20120717 - to confirm is Short Sell Activity proceed
				parameters.put(ParamConstants.SHORT_SELL_IS_PROCEED, isProceedShortSell);
				//Kw@20130404 - Fixes_Request-20130314, ReqNo.11 
				parameters.put(ParamConstants.EXCEED_5_BID_IS_PROCEED, isProceedExceed5Bid);
				
				// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7
				parameters.put( ParamConstants.SHORT_SELL_IS_AWARE,  isShortSellAware);
				
				//Added by Thinzar, Change_Request-20161101, ReqNo.2

				if(isPrivateOrder)
					parameters.put(ParamConstants.IS_PRIVATE_ORDER, isPrivateOrder);

				
				//Added by Thinzar@20140815 - Change_Request-20140815, ReqNo.4 - START
				//Fixes_Request-20170119-ReqNo. 3
				if(hasTriggerType){
					if(selectedTriggerType != null && selectedTriggerType.length() > 0)
						parameters.put(ParamConstants.TRIGGER_PRICE_TYPE_TAG, selectedTriggerType);
				}
				
				if(hasTriggerDirection){
					if(selectedTriggerDirection!= null && selectedTriggerDirection.length() > 0)
						parameters.put(ParamConstants.TRIGGER_PRICE_DIRECTION_TAG, selectedTriggerDirection);
				}
				//Added by Thinzar@20140815 - Change_Request-20140815, ReqNo.4 - END
				
				//Change_Request_20170119, ReqNo.4
				if(externalOrderSource != null && externalOrderSource.length() > 0){
					parameters.put(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG, externalOrderSource);
				}

				logTradeInitiate(PARAM_ORDER_SUBMIT);	//Change_Request-20170601, ReqNo.9
				response = AtpConnectUtil.tradeTicket(parameters);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response = AtpConnectUtil.tradeTicket(parameters);
				}
				intRetryCtr	= 0;
				if(response == null)
					throw new Exception(ErrorCodeConstants.FAIL_TRADE);
			} catch (FailedAuthenicationException e) {        	
				e.printStackTrace();

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(refreshThread != null)
					refreshThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				// Added by Mary@20130408 - Fixes_Request-20130314, ReqNo.13 - START
				strStoredTicketNo	= null;
				
				return e;
			} catch (Exception e) {
				e.printStackTrace();
				response = e.getMessage();
				
				// Added by Mary@20130408 - Fixes_Request-20130314, ReqNo.13 - START
				strStoredTicketNo	= null;
			}
			
			return null;
		}
		
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - START
		@Override
		 protected void onProgressUpdate(Void... params) {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( ! TradeActivity.this.dialog.isShowing() ){
					TradeActivity.this.dialog.setMessage(getResources().getString(R.string.trade_progress_submitting_order));
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! TradeActivity.this.isFinishing() ){
						TradeActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - END

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				//  Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - START
				if( TradeActivity.this.dialog != null && TradeActivity.this.dialog.isShowing() && isDialogOwner){
					TradeActivity.this.dialog.dismiss();
					isDialogOwner = false;
				}
				//  Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - END
				
				if( TradeActivity.this.mainMenu.isShowing() )
					TradeActivity.this.mainMenu.hide();
	
				if(result!=null){
					TradeActivity.this.sessionExpired();
					return;
				}

				if (response == null) {
					logTradeInitiate(PARAM_ORDER_FAIL_UNEXPECTED);    //Change_Request-20170601, ReqNo.3
					logTradeError(response, actionType + "," + symbolCode + "," + priceText + "," + quantityText);

					alertMessage = ErrorCodeConstants.FAIL_TRADE_UNEXPECTED;
					TradeActivity.this.showDialog(DIALOG_SUBMIT_ORDER_FROM_SERVER);
				}else {

					if ((response.toUpperCase().indexOf("ERROR") != -1) && (response.toUpperCase().indexOf("PIN") != -1)) {
						logTradeInitiate(PARAM_ORDER_SUCCESS);    //Change_Request-20170601, ReqNo.9

						alertMessage = response;
						TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_TRADING_PIN);
						// Mary@20130614 - Fixes_Request-20130523, ReqNo.10 - END
						StockApplication application = (StockApplication) TradeActivity.this.getApplication();
						application.setRememberPin(false);
					}
					//handle Error 513, Fixes_Request-20160101, ReqNo.10
					else if ((response.toUpperCase().indexOf("ERROR") != -1) && (response.indexOf("513") != -1)) {
						logTradeInitiate(PARAM_ORDER_SUCCESS);    //Change_Request-20170601, ReqNo.9

						alertMessage = (response.split("\\|\\|"))[1];
						alertMessage = alertMessage.split("\r\n")[0];
						TradeActivity.this.showDialog(TradeActivity.DIALOG_ORDER_TICKET_ERROR);
					}
					//handle Error 510
					else if ((response.toUpperCase().indexOf("ERROR") != -1) && (response.indexOf("510") != -1)) {
						logTradeInitiate(PARAM_ORDER_SUCCESS);    //Change_Request-20170601, ReqNo.9

						alertMessage = (response.split("\\|\\|"))[1];
						alertMessage = alertMessage.split("\r\n")[0];
						TradeActivity.this.showDialog(TradeActivity.DIALOG_ORDER_TICKET_ERROR);
					} else if ((response.toUpperCase().indexOf("ERROR") != -1) && (response.indexOf("502") != -1)
							|| ((response.toUpperCase().indexOf("WARNING") != -1) && (response.indexOf("50") != -1))
							) {
						logTradeInitiate(PARAM_ORDER_SUCCESS);    //Change_Request-20170601, ReqNo.9

						alertMessage = (response.split("\\|\\|"))[1];
						// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7
						alertMessage = alertMessage.split("\r\n")[0];
						TradeActivity.this.showDialog(TradeActivity.DIALOG_ORDER_TICKET_SHORT_SELL_ERROR);
						// Added by Mary@20120717 - additional handling for error code 502 - END

					} //Kw@20130404 - Fixes_Request-20130314, ReqNo.11
					else if ((response.toUpperCase().indexOf("ERROR") != -1) && (response.indexOf("509") != -1) && (response.indexOf("509") < 15)) {    //condition edited by Thinzar, Fixes_Request-20140820, ReqNo.12
						logTradeInitiate(PARAM_ORDER_SUCCESS);    //Change_Request-20170601, ReqNo.9

						alertMessage = (response.split("\\|\\|"))[1];
						TradeActivity.this.showDialog(TradeActivity.DIALOG_EXCEED_5_BID_WARNING);
					} else if (response.toUpperCase().indexOf("ERROR") != -1) {
						logTradeInitiate(PARAM_ORDER_SUCCESS);    //Change_Request-20170601, ReqNo.9

						alertMessage = response;
						TradeActivity.this.showDialog(TradeActivity.DIALOG_ORDER_TICKET_ERROR);
					} else {

						String status = AtpMessageParser.parseTradeTicket(response);

						if (status != null) {
							alertMessage = status;
							TradeActivity.this.showDialog(DIALOG_SUBMIT_ORDER_FROM_SERVER);

							logTradeInitiate(PARAM_ORDER_SUCCESS);    //Change_Request-20170601, ReqNo.3
						} else {
							//Fixes_Request-20170103, ReqNo. 7 - START
							//TradeActivity.this.showDialog(DIALOG_SUBMIT_ORDER_SUCCESS);
							if (response.toLowerCase().indexOf(strStoredTicketNo.toLowerCase()) != -1) {
								logTradeInitiate(PARAM_ORDER_SUCCESS);    //Change_Request-20170601, ReqNo.3

								TradeActivity.this.showDialog(DIALOG_SUBMIT_ORDER_SUCCESS);
							} else {
								logTradeInitiate(PARAM_ORDER_FAIL_UNEXPECTED);    //Change_Request-20170601, ReqNo.3
								logTradeError(response, actionType + "," + symbolCode + "," + priceText + "," + quantityText);

								alertMessage = ErrorCodeConstants.FAIL_TRADE_UNEXPECTED;
								TradeActivity.this.showDialog(DIALOG_SUBMIT_ORDER_FROM_SERVER);
							}
							//Fixes_Request-20170103, ReqNo. 7 - END

						}

						// Added by Mary@20130408 - Fixes_Request-20130314, ReqNo.13 - START
					}
				}

				strStoredTicketNo	= null;

				AppConstants.selectedAcc= tradeAccount;
				// Added by Mary@20120717 - to confirm is Short Sell Activity proceed
				isProceedShortSell		= false;
				isProceedExceed5Bid		= false;

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if(keyCode == KeyEvent.KEYCODE_BACK){

			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if( this.mainMenu.isShowing() ){
				this.mainMenu.hide();
			}else{
				finish();
			}
			// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}

	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case DIALOG_INVALID_ORDER_PRICE:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_price_title))
				.setMessage(getResources().getString(R.string.trade_invalid_price_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							priceField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
	
			case DIALOG_INVALID_ORDER_PRICE_ADD:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_price_title))
				.setMessage(getResources().getString(R.string.trade_revise_less_price_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							priceField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
	
			case DIALOG_INVALID_ORDER_PRICE_MINUS:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_price_title))
				.setMessage(getResources().getString(R.string.trade_revise_more_price_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							priceField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
	
			case DIALOG_INVALID_STOPLIMIT_PRICE:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_stoplimit_title))
				.setMessage(getResources().getString(R.string.trade_invalid_tr_price_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							stopLimitField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
	
			case DIALOG_INVALID_STOPLIMIT_PRICE_ADD:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_stoplimit_title))
				.setMessage(getResources().getString(R.string.trade_revise_less_tr_price_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							stopLimitField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
	
			case DIALOG_INVALID_STOPLIMIT_PRICE_MINUS:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_stoplimit_title))
				.setMessage(getResources().getString(R.string.trade_revise_more_tr_price_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							stopLimitField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
	
			case DIALOG_INVALID_ORDER_QUANTITY:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_qty_title))
				.setMessage(getResources().getString(R.string.trade_invalid_qty_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							quantityField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
				
			case DIALOG_INVALID_ORDER_QUANTITY_ADD:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_qty_title))
				.setMessage(getResources().getString(R.string.trade_revise_less_qty_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							quantityField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
				
			case DIALOG_INVALID_ORDER_QUANTITY_MINUS:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_qty_title))
				.setMessage(getResources().getString(R.string.trade_revise_more_qty_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							quantityField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
				
			// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
			case DIALOG_INVALID_DISCLOSED_QUANTITY_ADD:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_dis_qty_title))
				.setMessage(getResources().getString(R.string.trade_revise_less_dis_qty_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							disclosedQtyField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
				
			case DIALOG_INVALID_DISCLOSED_QUANTITY_MINUS:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_dis_qty_title))
				.setMessage(getResources().getString(R.string.trade_revise_more_dis_qty_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {				
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							disclosedQtyField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
				
			case DIALOG_INVALID_MINIMUM_QUANTITY_ADD:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_min_qty_title))
				.setMessage(getResources().getString(R.string.trade_revise_less_min_qty_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.5
							minQtyField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
				
			case DIALOG_INVALID_MINIMUM_QUANTITY_MINUS:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_min_qty_title))
				.setMessage(getResources().getString(R.string.trade_revise_more_min_qty_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							minQtyField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
			// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
				
			case DIALOG_INVALID_TRADING_PIN:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_pin_title))
				.setMessage(alertMessage)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
						
						// Added by Mary@20130614 - Fixes_Request-20120815, ReqNo.10
						removeDialog(DIALOG_INVALID_TRADING_PIN);
					}
				})
				.create();
				
			case DIALOG_ORDER_TICKET_ERROR:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_submission_error_title))
				.setMessage(alertMessage)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
						
						// Added by Mary@20120906 - Fixes_Request-20120815, ReqNo.10
						removeDialog(DIALOG_ORDER_TICKET_ERROR);
					}
				})
				.create();
	
			// Added by Mary@20120717 - additional handling for error code 502 - START
			case DIALOG_ORDER_TICKET_SHORT_SELL_ERROR:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_short_sell_conf_title))
				.setMessage(alertMessage)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
						
						// Added by Mary@20120906 - Fixes_Request-20120815, ReqNo.10
						removeDialog(DIALOG_ORDER_TICKET_SHORT_SELL_ERROR);
						// Added by Mary@20120717 - to confirm is Short Sell Activity proceed - START
						isProceedShortSell	= true;
						submitTradeOrder();
						// Added by Mary@20120717 - to confirm is Short Sell Activity proceed - END
					}
				})
				.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
						
						// Added by Mary@20120906 - Fixes_Request-20120815, ReqNo.10
						removeDialog(DIALOG_ORDER_TICKET_SHORT_SELL_ERROR);
						// Added by Mary@20120717 - to confirm is Short Sell Activity proceed
						isProceedShortSell	= false;
					}
				})
				.create();
				// Added by Mary@20120717 - additional handling for error code 502 - END
			
				//Kw@20130404 - Fixes_Request-20130314, ReqNo.11  - START	
				case DIALOG_EXCEED_5_BID_WARNING:
					return new AlertDialog.Builder(this)
					.setTitle(getResources().getString(R.string.trade_submission_warning_title))
					.setMessage(alertMessage)
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
							if(idleThread != null && ! isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
							
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
							try{
								dialog.dismiss();
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
							}catch(Exception e){
								e.printStackTrace();
							}
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
							
							isProceedExceed5Bid	= true;
							removeDialog(DIALOG_EXCEED_5_BID_WARNING);
							submitTradeOrder();
					
						}
					})
					.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
							if(idleThread != null && ! isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
							
							isProceedExceed5Bid	= false;
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
							try{
								dialog.dismiss();
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
							}catch(Exception e){
								e.printStackTrace();
							}
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
							
							removeDialog(DIALOG_EXCEED_5_BID_WARNING);
						}
					})
					.create();
					//Kw@20130404 - Fixes_Request-20130314, ReqNo.11  - END
				
			case DIALOG_SUBMIT_ORDER_SUCCESS:
				return new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage(getResources().getString(R.string.trade_submit_success_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
						TradeActivity.this.finish();
					}
				})
				.create();
	
			case DIALOG_SUBMIT_ORDER_FROM_SERVER:
				return new AlertDialog.Builder(this)
				.setTitle("")
				.setMessage(alertMessage)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END

						// Added by Mary@20120906 - Fixes_Request-20120815, ReqNo.10
						removeDialog(DIALOG_SUBMIT_ORDER_FROM_SERVER);
						TradeActivity.this.finish();
					}
				})
				.create();
	
			case DIALOG_NO_TRADING_ACCOUNT:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_no_account_title))
				.setMessage(getResources().getString(R.string.trade_no_account_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
						TradeActivity.this.finish();
					}
				})
				.create();
	
			case DIALOG_DATE_PICKER:
				return new DatePickerDialog(this, dateSetListener, intReviseGTDYear, intReviseGTDMonth, intReviseGTDDay);	
			
			// Added by Mary@20121023 - Fixes_Request-20121018, ReqNo.7 - START
			case DIALOG_NO_ORDER_REVISE_DETECTED:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_no_changes_title))
				.setMessage(getResources().getString(R.string.trade_no_changes_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							priceField.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
			// Added by Mary@20121023 - Fixes_Request-20121018, ReqNo.7 - END
				
			// Added by Mary@20130108 - Fixes_Request-20120101, ReqNo.24 - START
			case DIALOG_PYMT_NOT_ALLOWED_BUY_ACTION:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_payment_title))
				.setMessage(getResources().getString(R.string.trade_invalid_payment_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							spinnerPayment.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
				
			case DIALOG_PYMT_NOT_ALLOWED_SELL_ACTION:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_invalid_payment_msg))
				.setMessage(getResources().getString(R.string.trade_invalid_payment_sell_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
							spinnerPayment.requestFocus();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
			// Added by Mary@20130108 - Fixes_Request-20120101, ReqNo.24 - END
				
			//Added by Thinzar@20140829, Fixes_Request-20140820, ReqNo. 8 - START
			case DIALOG_INSUFFICIENT_BALANCE_CUT:
				return new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.trade_insufficient_title))
				.setMessage(getResources().getString(R.string.trade_insufficient_msg))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						try{
							dialog.dismiss();
							quantityField.requestFocus();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				})
				.create();
			//Added by Thinzar@20140829, Fixes_Request-20140820, ReqNo. 8 - START
				
			//Added by Thinzar, Change_Request-20150401, ReqNo. 7 - START
			case DIALOG_INSUFFICIENT_BALANCE_NONCUT:

				AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
				dialogBuilder.setTitle(getResources().getString(R.string.trade_insufficient_title));
				TextView textView = new TextView(this);
				textView.setMovementMethod(LinkMovementMethod.getInstance());
				textView.setText(Html.fromHtml(getResources().getString(R.string.trade_insufficient_cut_msg) +   "<a href=\"https://www.itradecimb.com.sg/app/help.client.services.z?cat=01&subcat=01_03&subsubcat=8a3e938c4761fbe0014766fbe86b017d\">"+ getResources().getString(R.string.trade_insufficient_cut_faq) + "</a> "));
				dialogBuilder.setView(textView);
				
				dialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						try{
							dialog.dismiss();
							quantityField.requestFocus();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				});
				
		        return dialogBuilder.create();
			//Added by Thinzar, Change_Request-20150401, ReqNo. 7 - END
		        
		    //Added by Thinzar, Fixes_Request-20161101, ReqNo. 16 - START
			case DIALOG_INVALID_MINIMUM_QUANTITY:
				return new AlertDialog.Builder(this)
					.setMessage(getResources().getString(R.string.trade_invalid_min_qty))
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int arg1) {
							dialog.dismiss();
							minQtyField.requestFocus();
						}
					})
					.create();
		    //Added by Thinzar, Fixes_Request-20161101, ReqNo. 16 - END
		}
		return null;
	}


	private void loadOrderPad() {
		Calendar c		= Calendar.getInstance();

		//Edited by Thinzar, Fixes_Request-20160101, ReqNo.1
		if(intReviseGTDYear == 0 && intReviseGTDMonth == 0 && intReviseGTDDay == 0){

			intReviseGTDYear 			= c.get(Calendar.YEAR);
			intReviseGTDMonth 			= c.get(Calendar.MONTH);
			intReviseGTDDay 			= c.get(Calendar.DAY_OF_MONTH);
		}
		// Mary@20121025 - Fixes_Request-20121023, ReqNo.11 - END

		dialog 			= new ProgressDialog(TradeActivity.this);
		
		priceField		= (EventEditText) this.findViewById(R.id.priceField);
		priceField.setOnImeBackListener(this);
		
		quantityField 	= (EventEditText) this.findViewById(R.id.quantityField);
		quantityField.setOnImeBackListener(this);
		// Added by Mary@20130705 - Change_Request-20130424, ReqNo.10 - START
		if( AppConstants.brokerConfigBean.isImposeMaxLenOnQty() ){
			InputFilter[] FilterArray 	= new InputFilter[1];
			FilterArray[0] 				= new InputFilter.LengthFilter(AppConstants.brokerConfigBean.getIntTradeQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? 4 : 6);
			quantityField.setFilters(FilterArray);
		}
		// Added by Mary@20130705 - Change_Request-20130424, ReqNo.10 - END
		
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		disclosedQtyField 	= (EventEditText) this.findViewById(R.id.discloseQtyEditText);
		disclosedQtyField.setOnImeBackListener(this);
		
		minQtyField 	= (EventEditText) this.findViewById(R.id.minQtyEditText);
		minQtyField.setOnImeBackListener(this);
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
		
		
		stopLimitField 	= (EventEditText)this.findViewById(R.id.stoplimitField);
		stopLimitField.setOnImeBackListener(this);

		// Added by Mary@20121009 - Fixes_Request-20120724, ReqNo.7 - START
		skipConfirmBox	= (CheckBox) TradeActivity.this.findViewById(R.id.skipConfirmCheckBox);
		// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7 - START
		if( AppConstants.brokerConfigBean.isShowSkipConfirmation() ){
			skipConfirmBox.setVisibility(View.VISIBLE);
			// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7 - END
			skipConfirmBox.setChecked( ( (StockApplication) TradeActivity.this.getApplication() ).isSkipConfirmation() );
			skipConfirmBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					( (StockApplication) TradeActivity.this.getApplication() ).setSkipConfirmation( isChecked );
				}
			});
			// Added by Mary@20121009 - Fixes_Request-20120724, ReqNo.7 - END
		// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7 - START
		}else{
			skipConfirmBox.setVisibility(View.GONE);
		}
		// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7 - END
		
		// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7 - START
		shortSellBox	= (CheckBox) TradeActivity.this.findViewById(R.id.shortSellCheckBox);
		shortSellBox.setChecked(false);
		if( ( AppConstants.brokerConfigBean.isShowShortSell() && AppConstants.brokerConfigBean.lstExchgShowShortSell().contains(strSelectedStockExchangeCode)
				&& (orderPadType == AppConstants.ORDER_PAD_TRADE) ) 
		)
			shortSellBox.setVisibility(View.VISIBLE);
		else
			shortSellBox.setVisibility(View.GONE);
		// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7 - END
		
		//Added by Thinzar, Change_Request-20161101, ReqNo.2 - START
		privateOrderBox	= (CheckBox) TradeActivity.this.findViewById(R.id.privateOrderCheckbox);
		privateOrderBox.setChecked(false);
		if(AppConstants.brokerConfigBean.getLstExchgShowPrivateOrder().contains(strSelectedStockExchangeCode)
				&& (orderPadType == AppConstants.ORDER_PAD_TRADE)){
			privateOrderBox.setVisibility(View.VISIBLE);	
		}else{
			privateOrderBox.setVisibility(View.GONE);
		}
		//Added by Thinzar, Change_Request-20161101, ReqNo.2 - END
		
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
		if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
			new stockRefresh().execute();
	}

	private class MarketDepthTask extends AsyncTask<View, Void, Boolean> {
		private Exception exception 						= null;
		private AsyncTask<View, Void, Boolean> updateTask 	= null;
		private List<MarketDepth> lstOldMarketDepth			= new ArrayList<MarketDepth>();
		private boolean isDialogOwner						= false;

		@Override
		protected void onPreExecute() {
			// Added by Mary@20130926 - Change_Request-20130724, ReqNo.18
			try{
				updateTask = this;
				if(lstNewMarketDepth != null){
					for(Iterator<MarketDepth> itr = lstNewMarketDepth.iterator(); itr.hasNext();){
						lstOldMarketDepth.add( itr.next() );
					}
				}
				
			// Added by Mary@20130926 - Change_Request-20130724, ReqNo.18 - START
				if( TradeActivity.this.dialog != null && TradeActivity.this.dialog.isShowing() )
					isDialogOwner	= true;
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130926 - Change_Request-20130724, ReqNo.18 - END
		}

		@Override
		protected Boolean doInBackground(View... view) {
			String[] symbolCodes = { symbolCode };

			String response = null;
			try {
				 if(AppConstants.brokerConfigBean.isMultilevelMarketDepth()){
					 response	= QcHttpConnectUtil.imageQuote2(symbolCode,AppConstants.getMarketDepthLvl(strQCExchgCode));
					 while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
						intRetryCtr++;
						response	= QcHttpConnectUtil.imageQuote2(symbolCode, AppConstants.getMarketDepthLvl(strQCExchgCode));
					}
					intRetryCtr	= 0;
					
					if (response == null)
						throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
					lstNewMarketDepth= QcMessageParser.parseMarketDepth4(response);
				 }else{
					response	= QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
					while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
						intRetryCtr++;
						response	= QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
					}
					intRetryCtr	= 0;
					
					if (response == null)
						throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
					
					lstNewMarketDepth= QcMessageParser.parseMarketDepth3(response,AppConstants.getMarketDepthLvl(strQCExchgCode));
				 }
			} catch (Exception e) {
				exception = e;
				return false;
			}


			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean isSuccess) {
			super.onPostExecute(isSuccess);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{			
				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					alertException.setButton(getResources().getString(R.string.dialog_button_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
	
					if( ! TradeActivity.this.isFinishing() )
						alertException.show();
					exception = null;
					return;
				}
				
				// Added by Mary@20130926 - Change_Request-20130724, ReqNo.18 - START
				if( TradeActivity.this.dialog != null && TradeActivity.this.dialog.isShowing() && isDialogOwner){
					TradeActivity.this.repaintOrderPadScreen();
					TradeActivity.this.dialog.dismiss();
					isDialogOwner = false;
				}
				// Added by Mary@20130926 - Change_Request-20130724, ReqNo.18 - END
	
				TradeActivity.this.prepareMarketDepthView(lstOldMarketDepth, lstNewMarketDepth);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	private void prepareMarketDepthView(final List<MarketDepth> lstOldMarketDepths, final List<MarketDepth> lstNewMarketDepths) {
		try{			
			for(int id = 0; id<counterLabels.size(); id++){
				final MarketDepth marketDepthNew= ( lstNewMarketDepths.size() > id ) ? lstNewMarketDepths.get(id) : new MarketDepth();
				final MarketDepth marketDepthOld= ( lstOldMarketDepths.size() > id ) ? lstOldMarketDepths.get(id) : null;
								
				/** COUNTER - START **/
				counterLabels.get(id).setText( String.valueOf(id + 1) );
				/** COUNTER - END **/
				
				/** BID SIZE - START **/
				final TextView tvBidSizeLabel 	=  bidSizeLabels.get(id);
				tvBidSizeLabel.setText( FormatUtil.formatLong( lngViewCurrentLotSize == 0 ? 0 : (marketDepthNew.getBidSize() / lngViewCurrentLotSize) ) );
				if(marketDepthOld != null){
					tvBidSizeLabel.setBackgroundColor( AppConstants.getIntBidAskQtyBlinkBackgroundColor(this, marketDepthOld.getBidSize(), marketDepthNew.getBidSize() ) );
					tvBidSizeLabel.setTextColor( AppConstants.getIntBidAskQtyBlinkTextColor(this, marketDepthOld.getBidSize(), marketDepthNew.getBidSize() ) );
					
					tvBidSizeLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvBidSizeLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
							tvBidSizeLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
						}
					}, 1500);
				}
				/** BID SIZE - END **/
				
				/** BID PRICE - START **/
				final TextView tvBidPriceLabel 	=  bidPriceLabels.get(id);
				tvBidPriceLabel.setText( AppConstants.getStrFormattedBidAskPrice(marketDepthNew.getBidPrice(), strQCExchgCode) );

				if(marketDepthOld != null){
					tvBidPriceLabel.setBackgroundColor( AppConstants.getIntBidAskPriceBlinkBackgroundColor( this, marketDepthOld.getBidPrice(), marketDepthNew.getBidPrice() ) );
					tvBidPriceLabel.setTextColor( AppConstants.getIntBidAskPriceBlinkTextColor( this, marketDepthOld.getBidPrice(), marketDepthNew.getBidPrice(), tvBidPriceLabel.getCurrentTextColor() ) );
					
					tvBidPriceLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvBidPriceLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
							tvBidPriceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
						}
					}, 1500);
				}else{
					tvBidPriceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
				}
				/** BID PRICE - END **/

				/** ASK SIZE - START **/
				final TextView tvAskSizeLabel		= askSizeLabels.get(id);
				tvAskSizeLabel.setText( FormatUtil.formatLong( lngViewCurrentLotSize == 0 ? 0 : (marketDepthNew.getAskSize() / lngViewCurrentLotSize) ) );
				
				if(marketDepthOld != null){
					tvAskSizeLabel.setBackgroundColor( AppConstants.getIntBidAskQtyBlinkBackgroundColor( this, marketDepthOld.getAskSize(), marketDepthNew.getAskSize() ) );
					tvAskSizeLabel.setTextColor( AppConstants.getIntBidAskQtyBlinkTextColor( this, marketDepthOld.getAskSize(), marketDepthNew.getAskSize() ) );
					
					tvAskSizeLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvAskSizeLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
							tvAskSizeLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
						}
					}, 1500);
				}
				/** ASK SIZE - END **/
				
				/** ASK PRICE - START **/
				final TextView tvAskPriceLabel	= askPriceLabels.get(id);
				tvAskPriceLabel.setText( AppConstants.getStrFormattedBidAskPrice(marketDepthNew.getAskPrice(), strQCExchgCode) );
				
				if(marketDepthOld != null){
					tvAskPriceLabel.setBackgroundColor( AppConstants.getIntBidAskPriceBlinkBackgroundColor( this, marketDepthOld.getAskPrice(), marketDepthNew.getAskPrice() ) );
					tvAskPriceLabel.setTextColor( AppConstants.getIntBidAskPriceBlinkTextColor( this, marketDepthOld.getAskPrice(), marketDepthNew.getAskPrice(), tvAskPriceLabel.getCurrentTextColor() ) );
					
					tvAskPriceLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvAskPriceLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
							tvAskPriceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
						}
					}, 1500);
				}else{
					tvAskPriceLabel.setTextColor(getResources().getColor(R.color.NoStatus_FgColor));
				}
				/** ASK PRICE - END **/
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Mary@20130826 - Fixes_Request-20130711, ReqNo.14 - END

	private class stockRefresh extends AsyncTask<View, Void, String> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private Exception exception						= null;
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13
		private boolean isDialogOwner					= false;
		private AsyncTask<View, Void, String> updateTask= null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				updateTask = this;
	
				// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - START
				if(! TradeActivity.this.dialog.isShowing() ){
					// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - END
					TradeActivity.this.dialog.setMessage(getResources().getString(R.string.trade_progress_loading_stock_details));
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! TradeActivity.this.isFinishing() ){
						TradeActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(View... params) {
			String[] symbolCodes = {symbolCode};

			String response = null;
			try {
				response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_TRADE);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_TRADE);
				}
				intRetryCtr	= 0;

				if(response == null)				
					throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);

			}catch(Exception e){
				exception = e;
			}
			
			if(response!=null)
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				//Added by Kw@20130502 - Fix_Request-20130426, ReqNo.3 
				stockSymbol = loadMarketDepth(QcMessageParser.parseStockTrade(response)); 

			return null;
		}
		
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - START
		@Override
		 protected void onProgressUpdate(Void... params) {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( ! TradeActivity.this.dialog.isShowing() ){
					TradeActivity.this.dialog.setMessage(getResources().getString(R.string.trade_progress_loading_stock_details));
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! TradeActivity.this.isFinishing() ){
						TradeActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - END

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){

					if( TradeActivity.this.dialog != null && TradeActivity.this.dialog.isShowing() && isDialogOwner){
						TradeActivity.this.dialog.dismiss();
						isDialogOwner = false;
					}
					
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							finish();
						}
					});
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! TradeActivity.this.isFinishing() )
						alertException.show();	
					
					exception = null;
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				strStockCurrency = stockSymbol.getCurrency();
				// Added by Mary@20121121 - Change_Request-20121106, ReqNo.1 - START
				if(strStockCurrency != null)
					strStockCurrency	= strStockCurrency.trim();
				// Added by Mary@20121121 - Change_Request-20121106, ReqNo.1 - END
				
				TradeActivity.this.initOrderPadScreen();

				if( TradeActivity.this.dialog != null && TradeActivity.this.dialog.isShowing() && isDialogOwner){
					isDialogOwner = false;
				}
				
				// Added by Mary@20130624 - Fixes_Request-20130523, ReqNo.17
				new MarketDepthTaskNew().execute();
				
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	//Added by Kw@20130502 - Fix_Request-20130426, ReqNo.3 - START
	public StockSymbol loadMarketDepth(StockSymbol stockSymbol){
	//	marketDepths
		
		int id 					= 0;
		//DefinitionConstants.Debug("MARKET DEPTH SIZE:"+marketDepths.size());
		if(!marketDepths.isEmpty()){
			for (Iterator<MarketDepth> itr = marketDepths.iterator(); itr.hasNext();) {
				MarketDepth marketDepth	= itr.next();
				
				switch(id){
					//if(id==0){
					case 0 :
						stockSymbol.setLastAskPrice(marketDepth.getAskPrice());
						stockSymbol.setLastBidPrice(marketDepth.getBidPrice());
						stockSymbol.setLastAskSize(marketDepth.getAskSize());
						stockSymbol.setLastBidSize(marketDepth.getBidSize());
						break;
					//}else if(id==1){
					case 1 :
						stockSymbol.setLastAskPrice2(marketDepth.getAskPrice());
						stockSymbol.setLastBidPrice2(marketDepth.getBidPrice());
						stockSymbol.setLastAskSize2(marketDepth.getAskSize());
						stockSymbol.setLastBidSize2(marketDepth.getBidSize());
						break;
					//}else if(id==2){
					case 2 :
						stockSymbol.setLastAskPrice3(marketDepth.getAskPrice());
						stockSymbol.setLastBidPrice3(marketDepth.getBidPrice());
						stockSymbol.setLastAskSize3(marketDepth.getAskSize());
						stockSymbol.setLastBidSize3(marketDepth.getBidSize());
						break;
				}
				
				id++;	
			}
		}else{
			stockSymbol.setLastAskPrice(0);
			stockSymbol.setLastBidPrice(0);
			stockSymbol.setLastAskSize(0);
			stockSymbol.setLastBidSize(0);
			stockSymbol.setLastAskPrice2(0);
			stockSymbol.setLastBidPrice2(0);
			stockSymbol.setLastAskSize2(0);
			stockSymbol.setLastBidSize2(0);
		}
		return stockSymbol;
	
	}
	//Added by Kw@20130502 - Fix_Request-20130426, ReqNo.3 - END
	
	// Mary@20120920 - Fixes_Request-20120815, ReqNo.12 - START
	private class getClientLimit extends AsyncTask<Void,Void,String>{
		private boolean isExpired		= false;
		private Exception exception 	= null;
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13
		private boolean isDialogOwner	= false;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13
				if( ! TradeActivity.this.dialog.isShowing() ){
					TradeActivity.this.dialog.setMessage(getResources().getString(R.string.trade_progress_refresh_client));
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! TradeActivity.this.isFinishing() ){
						TradeActivity.this.dialog.show();
						// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13
						isDialogOwner	= true;
					}
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		
		@Override
		protected String doInBackground(Void... params) {
			Map<String, Object> parameters = new HashMap<String, Object>();

			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			parameters.put(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
			parameters.put(ParamConstants.CLIENTCODE_TAG, tradeAccount.getClientCode());
			parameters.put(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
			parameters.put(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
			parameters.put(ParamConstants.EXCHANGECODE_TAG, tradeAccount.getExchangeCode());
			parameters.put(ParamConstants.PAYMENT_TYPE_TAG, selectedPayment);
			parameters.put(ParamConstants.PAYMENT_CURRENCY_TAG, selectedCurrency);

			String result = null;
			try {
				result = AtpConnectUtil.tradeClientLimit(parameters);

				while(intRetryCtr < AppConstants.intMaxRetry && result == null){
					intRetryCtr++;
					result = AtpConnectUtil.tradeClientLimit(parameters);
				}
				intRetryCtr	= 0;
				
				//Edited by Thinzar, Change_Request-20150901, ReqNo.1
				if (result == null){
					if(AppConstants.connectExceptionCount >= AppConstants.intMaxRetry){
						throw new Exception(ErrorCodeConstants.NETWORK_UNAVAILABLE);
					}
					else if(AppConstants.timeoutExceptionCount >= AppConstants.intMaxRetry){
						throw new Exception(ErrorCodeConstants.FAIL_CONNECT_ATP);
					} else
						throw new Exception(ErrorCodeConstants.FAIL_GET_TRADE_LIMIT);
				}
				
			}catch(FailedAuthenicationException e){        	
				isExpired	= true;
				// Added by Mary@20130703 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				// Added by Mary@20130703 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.refreshThread != null)
					LoginActivity.refreshThread.stopRequest();
				exception	= e;
			} catch (Exception e) {
				exception	= e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			return result;
		}
	
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - START
		@Override
		 protected void onProgressUpdate(Void... params) {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( ! TradeActivity.this.dialog.isShowing() ){
					TradeActivity.this.dialog.setMessage(getResources().getString(R.string.trade_progress_refresh_client));
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! TradeActivity.this.isFinishing() ){
						TradeActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - END

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{

				if( TradeActivity.this.dialog != null && TradeActivity.this.dialog.isShowing() && isDialogOwner ){
					TradeActivity.this.dialog.dismiss();
					isDialogOwner = false;
				}
	
				if(isExpired){
					TradeActivity.this.sessionExpired();
					return;
				}
	
				if(result == null || exception != null){

					//Added by Thinzar, Change_Request-20150901, ReqNo.1 - START
					boolean isATPConnectionLost = (exception.getMessage().equalsIgnoreCase(ErrorCodeConstants.FAIL_CONNECT_ATP))? true:false;
					prepareAlertException(TradeActivity.this.alertException, exception.getMessage(), isATPConnectionLost);
					//Added by Thinzar, Change_Request-20150901, ReqNo.1 - END
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! TradeActivity.this.isFinishing() && !AppConstants.hasLogout){	//Added hasLogout checking to avoid displaying logout dialog many times
						TradeActivity.this.alertException.setCanceledOnTouchOutside(false);
						
						if(TradeActivity.this.dialog.isShowing()) TradeActivity.this.dialog.dismiss();
						
						alertException.show();
					}
					
					exception	= null;
					return;
				}
				
				accountDisplay	= tradeAccount.getBranchCode()+" - "+tradeAccount.getAccountNo() + "-" + tradeAccount.getClientName().toUpperCase();

				// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
				tableLayout				= (TableLayout) findViewById(R.id.limitTable);
				tableLayout.removeAllViews();
				makeRow(getResources().getString(R.string.trade_limit_account),accountDisplay);

				tradeClientLimit		= AtpMessageParser.extractTradeClientLimit2(result);

				makeClientLimit();

			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	// Mary@20120920 - Fixes_Request-20120815, ReqNo.12 - END
	
	// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
	private void makeClientLimit(){
		
		//Added by Thinzar@20140806, Change_Request-20140701,ReqNo.9 - START
		tableLayout.removeAllViews();
		makeRow(getResources().getString(R.string.trade_limit_account),accountDisplay);
		//Added by Thinzar@20140806 - END

		//Commented above line and replace, Fixes_Request-20150401, ReqNo.1
		if(!SystemUtil.isDerivative(strSelectedStockExchangeCode)){
			
			//int flag	= AppConstants.getClientLimitOption(TradeActivity.this.strSelectedStockExchangeCode);
			//added if/else block by Thinzar@20140806, Change_Request-20140701, ReqNo.9
			if(selectedPayment != null && selectedPayment.equalsIgnoreCase("CUT")){
				investmentPower	=	Double.parseDouble( tradeClientLimit.get("Trading Limit") );
				makeRow(getResources().getString(R.string.trade_limit_inv_power), selectedCurrency + " " +  FormatUtil.formatDoubleFull( investmentPower, "0.00"));
				isCreditLimit = true;
			}
			//if(selectedPayment == null || selectedPayment.length() == 0 || selectedPayment.equalsIgnoreCase("Cash") || selectedPayment.equalsIgnoreCase("CPF") || selectedPayment.equalsIgnoreCase("SRS")){
			else{	
				switch( AppConstants.getClientLimitOption(TradeActivity.this.strSelectedStockExchangeCode) ){
					//if(flag==1){
					case 1 :
						/* CREDIT LIMIT */
						makeRow(getResources().getString(R.string.trade_limit_trading), AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull( Double.parseDouble( tradeClientLimit.get("Trading Limit") ), "0.00") );
						isCreditLimit = true;
						break;
					//}else if(flag==2){
					case 2 :
						/* BUY & SELL LIMIT */
						// Mary@20130627 - Fixes_Request-20130523, ReqNo.19 - START
						makeRow( getResources().getString(R.string.trade_limit_buy), AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull( Double.parseDouble( tradeClientLimit.get("Buy Limit") ), "0.00" ) );
						makeRow( getResources().getString(R.string.trade_limit_sell), AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull( Double.parseDouble( tradeClientLimit.get("Sell Limit") ), "0.00" ) );

						isCreditLimit = false;
						break;
					//}else if(flag==3){
					case 3 :
						if(tradeAccount.isMarginAccount()){
							/* CREDIT LIMIT */
							// Mary@20130627 - Fixes_Request-20130523, ReqNo.19
							makeRow(getResources().getString(R.string.trade_limit_trading), AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull( Double.parseDouble( tradeClientLimit.get("Trading Limit") ), "0.00") );
							isCreditLimit = true;
						}else{
							/* BUY & SELL LIMIT */
							makeRow( getResources().getString(R.string.trade_limit_buy), AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull( Double.parseDouble( tradeClientLimit.get("Buy Limit") ), "0.00" ) );
							makeRow( getResources().getString(R.string.trade_limit_sell), AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull( Double.parseDouble( tradeClientLimit.get("Sell Limit") ), "0.00" ) );

							isCreditLimit = false;
						}
						break;
					//}else{
					default :
						if(TradeActivity.this.strSelectedStockExchangeCode.equals("SG") || TradeActivity.this.strSelectedStockExchangeCode.equals("SI")){
							/* BUY & SELL LIMIT */

							makeRow( getResources().getString(R.string.trade_limit_buy), AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull( Double.parseDouble( tradeClientLimit.get("Buy Limit") ), "0.00" ) );
							makeRow( getResources().getString(R.string.trade_limit_sell), AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull( Double.parseDouble( tradeClientLimit.get("Sell Limit") ), "0.00" ) );
							isCreditLimit = false;

						}else{
							/* CREDIT LIMIT */

							makeRow(getResources().getString(R.string.trade_limit_trading), AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull( Double.parseDouble( tradeClientLimit.get("Trading Limit") ), "0.00") );
							isCreditLimit = true;
						}
						break;
				}
			}//if Cash, CPF, SRS, null
		}
	}
	
	private void makeRow(String title,String value){
		TableRow tableRow	= new TableRow(this);
		tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
		tableRow.setWeightSum(1);

		View rowView 		= this.getLayoutInflater().inflate(R.layout.limit_row, null);
		TextView nameLabel  = (TextView) rowView.findViewById(R.id.limitTypeLabel);
		nameLabel.setText(title+":");
		if(title.equalsIgnoreCase("account")) nameLabel.setSingleLine(true);
		
		//assign repective credit limit into here
		TextView valueLabel = (TextView) rowView.findViewById(R.id.accountBalanceLabel);
		valueLabel.setText(value);
		
		tableRow.addView(rowView);

		tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));  
	}

	// Added by Mary@20120904 - Change_Request-20120719, ReqNo.12 - START
	private void initPriceFieldBaseOnTradePreference(){
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1		
		if(orderPadType == AppConstants.ORDER_PAD_TRADE){
			if(strPriceFieldAfterEdit != null){
				priceField.setText(strPriceFieldAfterEdit);
			}else{
				if( AppConstants.brokerConfigBean.autoFillTradePrice() ){
					if(bidaskPrice != null){
						if( bidaskPrice.equalsIgnoreCase("MP") || bidaskPrice.equalsIgnoreCase("MO") )
							priceField.setText("0");
						else
							priceField.setText(bidaskPrice);
					}else{
						/* Mary@20130926 - Change_Request-20130724, ReqNo.18 - START
						float fltTemp	= stockSymbol.getLastAskPrice();
						fltTemp			= fltTemp == 0 ? stockSymbol.getLastBidPrice() : fltTemp;
						*/
						float fltTemp	= 0;
						if( lstNewMarketDepth != null && lstNewMarketDepth.size() > 0){
							MarketDepth marketDepth	= lstNewMarketDepth.get(0);
							fltTemp					= marketDepth.getAskPrice();
							fltTemp					= ( fltTemp == 0 || orderDetailType == AppConstants.PORTFOLIO_DETAIL_TRADE) ? marketDepth.getBidPrice() : fltTemp;
						}
						// Mary@20130926 - Change_Request-20130724, ReqNo.18 - END
						// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1
						fltTemp			= fltTemp == 0 ? stockSymbol.getLastDone() : fltTemp;
						if( AppConstants.isMarketOrderStock(fltTemp) || AppConstants.isMarketPriceStock(fltTemp) )
							priceField.setText("0");
						else
							priceField.setText( FormatUtil.formatPrice(fltTemp, "", strSelectedStockExchangeCode));
						
						fltTemp			= 0f;
					}
				}else{
					priceField.setText("0");
				}
			}
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - START
		}else if(orderPadType == AppConstants.ORDER_PAD_REVISE){
			if(strPriceFieldAfterEdit != null)
				priceField.setText(strPriceFieldAfterEdit);
			else
				priceField.setText( String.valueOf(fltReviseOrdPrice) );
		}
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - END
		
		//Edited by Thinzar, Fixes_Request-20151001, ReqNo. 4
		priceField.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
		if( SystemUtil.isDerivative(this.strSelectedStockExchangeCode) ){
			if(instrument.indexOf("|") != -1)
				priceField.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED|InputType.TYPE_NUMBER_FLAG_DECIMAL);
		}
		//Edited by Thinzar, Fixes_Request-20151001, ReqNo. 4
	}
	
	private void initQuantityFieldBaseOnTradePreference(){	
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1
		if(orderPadType == AppConstants.ORDER_PAD_TRADE){
			if( strQtyFieldAfterEdit != null ){
				quantityField.setText( strQtyFieldAfterEdit );
			}else{
				if( ! AppConstants.brokerConfigBean.autoFillTradeQuantity() ){
					quantityField.setText( String.valueOf( AppConstants.brokerConfigBean.getIntCustomTradeQuantity() ) );
				}else{
					// Mary@20130926 - Change_Request-20130724, ReqNo.18 - START
					long lngBidAskQty	= 0;
					if(lstNewMarketDepth != null && lstNewMarketDepth.size() > 0){
						MarketDepth marketDepth	= lstNewMarketDepth.get(0);
						lngBidAskQty			= bidaskQty == null ? ( ( marketDepth.getAskSize() == 0 || orderDetailType == AppConstants.PORTFOLIO_DETAIL_TRADE ) ? marketDepth.getBidSize() :  marketDepth.getAskSize() ) : Long.parseLong(bidaskQty);
					}
					// Mary@20130926 - Change_Request-20130724, ReqNo.18 - END

					quantityField.setText( String.valueOf( lngCurrentLotSize == 0 ? 0 : (lngBidAskQty / lngCurrentLotSize) ) );
				}
				
				//Change_Request-20170601, ReqNo.8
				if(orderQtyFromSgx > 0){
					quantityField.setText( String.valueOf( lngCurrentLotSize == 0 ? 0 : (orderQtyFromSgx / lngCurrentLotSize)));
				}
			}
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - START
		}else if(orderPadType == AppConstants.ORDER_PAD_REVISE){
			if(strQtyFieldAfterEdit != null)
				quantityField.setText(strQtyFieldAfterEdit);
			else
				/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
				quantityField.setText( String.valueOf(lngUnmatchQty / lngCurrentLotSize) );
				*/
				quantityField.setText( String.valueOf( lngCurrentLotSize == 0 ? 0 : (lngUnmatchQty / lngCurrentLotSize) ) );
		}
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - END
		
		quantityField.setRawInputType(InputType.TYPE_CLASS_NUMBER);	//Added by Thinzar, Fixes_Request-20140820, ReqNo. 21
	}
	// Added by Mary@20120904 - Change_Request-20120719, ReqNo.12 - END
	
	// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1 - START
	private void initStopLimitPriceField(){	
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1
		if(orderPadType == AppConstants.ORDER_PAD_TRADE){
			if( strStopLimitPriceFieldAfterEdit != null )
				stopLimitField.setText(strStopLimitPriceFieldAfterEdit);
			else
				stopLimitField.setText("0");
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - START
		}else if(orderPadType == AppConstants.ORDER_PAD_REVISE){
			if(strStopLimitPriceFieldAfterEdit != null)
				stopLimitField.setText(strStopLimitPriceFieldAfterEdit);
			else
				stopLimitField.setText( String.valueOf(fltReviseStopLimit) );	
		}
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - END
		
		stopLimitField.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);	//Added by Thinzar, Fixes_Request-20140820, ReqNo. 21
	}
	
	private void initDisclosedQtyField(){
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1
		if(orderPadType == AppConstants.ORDER_PAD_TRADE){
			if( strDisclosedQtyFieldAfterEdit != null )
				disclosedQtyField.setText(strDisclosedQtyFieldAfterEdit);
			else
				disclosedQtyField.setText("");
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - START
		}else if(orderPadType == AppConstants.ORDER_PAD_REVISE){
			if(strDisclosedQtyFieldAfterEdit != null)
				disclosedQtyField.setText(strDisclosedQtyFieldAfterEdit);
			else
				/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
				disclosedQtyField.setText( String.valueOf(lngReviseDisclosedQty / lngCurrentLotSize) );
				*/
				disclosedQtyField.setText( String.valueOf( lngCurrentLotSize == 0 ? 0 : (lngReviseDisclosedQty / lngCurrentLotSize) ) );
		}
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - END
		
		disclosedQtyField.setRawInputType(InputType.TYPE_CLASS_NUMBER);	//Added by Thinzar, Fixes_Request-20140820, ReqNo. 21
	}
	
	private void initMinimumQtyField(){
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1
		if(orderPadType == AppConstants.ORDER_PAD_TRADE){
			if( strMinQtyFieldAfterEdit != null )
				minQtyField.setText(strMinQtyFieldAfterEdit);
			else
				minQtyField.setText("");
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - START
		}else if(orderPadType == AppConstants.ORDER_PAD_REVISE){
			if(strMinQtyFieldAfterEdit != null)
				minQtyField.setText(strMinQtyFieldAfterEdit);
			else
				/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
				minQtyField.setText( String.valueOf(lngReviseMinQty / lngCurrentLotSize) );
				*/
				minQtyField.setText( String.valueOf( lngCurrentLotSize == 0 ? 0 : (lngReviseMinQty / lngCurrentLotSize) ) );
		}
		// Added by Mary@20121023 - Fixes_Request-20121023, ReqNo.1 - END
		
		minQtyField.setRawInputType(InputType.TYPE_CLASS_NUMBER);	//Added by Thinzar, Fixes_Request-20140820, ReqNo. 21
	}
	// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1 - END	

	private void constructTriggerTypeAndDirection(){
		
		if(selectedTriggerType != null){
			for(int i=0;i<triggerPriceTypes.size();i++){
				if(triggerPriceTypes.get(i).equalsIgnoreCase(selectedTriggerType)){
					spnTriggerType.setSelection(i);
					break;
				}
			}
		}
		
		if(selectedTriggerDirection != null){
			for(int i=0;i<triggerPriceDirections.size();i++){
				if(triggerPriceDirections.get(i).equalsIgnoreCase(selectedTriggerDirection)){
					spnTriggerDirection.setSelection(i);
					break;
				}
			}
		}
	}
	
	private void toggleRelatedOrderTypeView(){	
		
		if(selectedOrderType != null){		
			/* Commented out by Thinzar@20140911, Fixes_Request-20140820, ReqNo.21
			if( orderPadType == AppConstants.ORDER_PAD_TRADE
					&& ( selectedOrderType.equalsIgnoreCase(ORDER_TYPE_MARKET) || selectedOrderType.equalsIgnoreCase(ORDER_TYPE_MARKET_TO_LIMIT) ) ){
				priceField.setText("0");
			}
			else
				priceField.setText(strPriceFieldAfterEdit);
			*/
			//Fixes_Request-20140820, ReqNo5 - reset triggerType and Direction
			/*comment out by Thinzar, Fixes_Request-20170119-ReqNo. 3
			if(spnTriggerType.isShown())	spnTriggerType.setSelection(0);
			if(spnTriggerDirection.isShown())	spnTriggerDirection.setSelection(0);
			*/
			
			//Added by Thinzar@20140909, Change_Request-20140901, ReqNo.1 - START
			List<OrderControlInfo> lstOrderCtrl2 = null;//	= exchangeInfo.getOrderCtrltoList();
			if(orderPadType == AppConstants.ORDER_PAD_REVISE){
				
				lstOrderCtrl2	= exchangeInfo.getReviseOrderCtrltoList();		//from ATP [ReviseCtrl]
				
			} else {
				lstOrderCtrl2	= exchangeInfo.getOrderCtrltoList();			//from ATP, either [OrderCtrl] or [OrderCtrl2]
			}
			//Added by Thinzar@20140909, Change_Request-20140901, ReqNo.1 - END
			
			//Added by Thinzar@20141203, Change_Request-20141101, ReqNo. 18 - START
			//note: to cater for the old ATP, if [reviseCtrl] not found
			if(lstOrderCtrl2.size() == 0){
				stopLimitField.setEnabled(false);
				spnTriggerType.setEnabled(false);
				spnTriggerDirection.setEnabled(false);
			}
			//Added by Thinzar@20141203, Change_Request-20141101, ReqNo. 18 - END
			else {
				for(Iterator<OrderControlInfo> itr=lstOrderCtrl2.iterator(); itr.hasNext();){
					OrderControlInfo oci	= itr.next();
					if( selectedOrderType.equalsIgnoreCase( oci.getStrOrderType() ) ){
						
						priceField.setEnabled( oci.isEnablePriceField() );
						quantityField.setEnabled( oci.isEnableQuantityField() );
						// dateField.setEnabled( ori.isEnableGTDDateField() );
						stopLimitField.setEnabled( oci.isEnableStopLimitPriceField() );
						
						constructValidityTableView(oci.getLstAvailableValidity(), isEditableValidity);
						
						toggleMinimumQtyField(oci, selectedValidity);
						toggleDisclosedQtyField(oci, selectedValidity);
						
						//added by Thinzar@20140815, Change_Request-20140801, ReqNo.4 - START
						spnTriggerType.setEnabled(oci.isEnableTriggerType());
						spnTriggerDirection.setEnabled(oci.isEnableTriggerDirection());
						//added by Thinzar@20140815, Change_Request-20140801, ReqNo.4 - END
						
						break;
					}
				}
			}
			
			if(orderPadType == AppConstants.ORDER_PAD_REVISE){
				/* TRADE - PRICE */
				priceField.setEnabled( priceField.isEnabled() ? isEditablePrice : priceField.isEnabled() );
				
				/* TRADE - QUANTITY */
				quantityField.setEnabled( quantityField.isEnabled() ? isEditableQuantity : quantityField.isEnabled() );
				
				/* TRADE - DISCLOSED QUANTITY */
				disclosedQtyField.setEnabled( disclosedQtyField.isEnabled() ? isEditableDisclosedQty : disclosedQtyField.isEnabled() );
				
				/* TRADE - Minimum QUANTITY */
				minQtyField.setEnabled( minQtyField.isEnabled() ? isEditableMinQty : minQtyField.isEnabled() );
				
				/* TRADE - TRIGGER PRICE */
				/* commented out by Thinzar@20140930, Fixes_Request-20140929,ReqNo.3
				stopLimitField.setEnabled( stopLimitField.isEnabled() ? isEditableStopLimit : stopLimitField.isEnabled() );
				*/
			}
			
			//Added by Thinzar@20140910, Fixes_Request 20140820, ReqNo. 21 - START
			if( orderPadType == AppConstants.ORDER_PAD_TRADE ){
				if(priceField.isEnabled()){
					initPriceFieldBaseOnTradePreference();
					priceField.requestFocus();
				}
				else {
					priceField.setText("");	
					quantityField.requestFocus();
				}
				
				if(stopLimitField.isEnabled())		initStopLimitPriceField();
				else 	stopLimitField.setText("");
				
				if(quantityField.isEnabled())		initQuantityFieldBaseOnTradePreference();
				else 	quantityField.setText("");
				
				if(disclosedQtyField.isEnabled()) 	initDisclosedQtyField();
				else 	disclosedQtyField.setText("");
				
				if(minQtyField.isEnabled()) 		initMinimumQtyField();
				else	minQtyField.setText("");
			}
			//Added by Thinzar@20140910, Fixes_Request 20140820, ReqNo. 21 - END
			
				
		}
	}
	// Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
	
	private void toggleRelatedValidityView(){

		//Edited by Thinzar@20140912, Fixes_Request-20140820, ReqNo21 - START
		if(selectedValidity!=null){
			if(selectedValidity.equals(VALIDITY_GTD)){
				if(orderPadType == AppConstants.ORDER_PAD_TRADE)
					dateField.setEnabled(true);
				else if(orderPadType == AppConstants.ORDER_PAD_REVISE){
					if(isEditableValidity)	dateField.setEnabled(true);
					else 					dateField.setEnabled(false);
				}
				updateDateField(dateField, intReviseGTDDay, intReviseGTDMonth, intReviseGTDYear);
				
			}else{
				dateField.setEnabled(false);
			}
			
			if(selectedOrderType != null){
				List<OrderControlInfo> lstOrderCtrl	= exchangeInfo.getOrderCtrltoList();
				for(Iterator<OrderControlInfo> itr=lstOrderCtrl.iterator(); itr.hasNext();){
					OrderControlInfo oci	= itr.next();
					
					if( selectedOrderType.equalsIgnoreCase( oci.getStrOrderType() ) ){		
						toggleDisclosedQtyField(oci, selectedValidity);
						
						break;
					}
				}
			}
			//Edited by Thinzar@20140912, Fixes_Request-20140820, ReqNo21 - END
			
			/* Commented out by Thinzar@20140911 - The following code not needed, already would have performed the same thing while toggle order type
		
				if(orderPadType == AppConstants.ORDER_PAD_REVISE){
					// TRADE - PRICE 
					priceField.setEnabled( priceField.isEnabled() ? isEditablePrice : priceField.isEnabled() );
					
					// TRADE - QUANTITY
					quantityField.setEnabled( quantityField.isEnabled() ? isEditableQuantity : quantityField.isEnabled() );
					
					// TRADE - DISCLOSED QUANTITY
					disclosedQtyField.setEnabled( disclosedQtyField.isEnabled() ? isEditableDisclosedQty : disclosedQtyField.isEnabled() );
					
					// TRADE - Minimum QUANTITY
					minQtyField.setEnabled( minQtyField.isEnabled() ? isEditableMinQty : minQtyField.isEnabled() );
					
					// TRADE - TRIGGER PRICE
					stopLimitField.setEnabled( stopLimitField.isEnabled() ? isEditableStopLimit : stopLimitField.isEnabled() );
					
					if( priceField.isEnabled() )
						priceField.requestFocus();
					else
						quantityField.requestFocus();
				}
			*/
		}
	}
	// Added by Mary@20120904 - Fixes_Request-20120815, ReqNo.4 - END

	// Added by Mary@20121123 - Changes_Request-20121106, ReqNo.1 - START
	private void toggleDisclosedQtyField(OrderControlInfo oci, String strSelectedValidity){
		
		if(oci == null || ( strSelectedValidity == null || (strSelectedValidity != null && strSelectedValidity.trim().length() < 1) ) )
			return;
		
		disclosedQtyField.setEnabled(false);
		
		if( oci.isEnableDisclosedQtyField() ){
			if( oci.getLstValidityThatEnableDisclosedQtyField().size() > 0 )
				disclosedQtyField.setEnabled( oci.getLstValidityThatEnableDisclosedQtyField().contains( strSelectedValidity.trim() ) );
			else
				//disclosedQtyField.setEnabled(false);
				disclosedQtyField.setEnabled(true);		//Fixes_Request_20170103, ReqNo.6	
		}
	}
	
	private void toggleMinimumQtyField(OrderControlInfo oci, String strSelectedValidity){
		if(oci == null || ( strSelectedValidity == null	|| (strSelectedValidity != null && strSelectedValidity.trim().length() < 1) ) )
			return;
		
		minQtyField.setEnabled(false);
		
		if( oci.isEnableMinQtyField() ){
			if( oci.getLstValidityThatEnableMinQtyField().size() > 0 )
				minQtyField.setEnabled( oci.getLstValidityThatEnableMinQtyField().contains( strSelectedValidity.trim() ) );
			else
				minQtyField.setEnabled(true);
		}
	}
	// Added by Mary@20121123 - Changes_Request-20121106, ReqNo.1 - END

	private void updateDateField(EditText etDateField, int intDay, int intMonth, int intYear) {
		etDateField.setText( new StringBuilder().append(intDay).append("-").append(intMonth + 1).append("-").append(intYear) );
	}

	private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			TradeActivity.this.intReviseGTDYear = year;
			TradeActivity.this.intReviseGTDMonth = monthOfYear;
			TradeActivity.this.intReviseGTDDay = dayOfMonth;

			updateDateField(TradeActivity.this.dateField, TradeActivity.this.intReviseGTDDay, TradeActivity.this.intReviseGTDMonth, TradeActivity.this.intReviseGTDYear);
		}
	};

	public void onImeBackEvent(EventEditText editText, String text) {
		/* Mary@20130605 - comment unneccessary process
		this.adjustLayoutDismissKeyboard();
		*/
	}

	public void handleInputButtonEvent(Button button, int inputType) {
		String text 	= button.getText().toString();
		String operator = text.substring(0, 1);
		String value 	= text.substring(1);

		switch(inputType){			
			case AppConstants.TRADE_INPUT_PRICE :
				String priceText= priceField.getText().toString().replace(",", "").trim();
				float price 	= Float.parseFloat(priceText.trim().length() == 0 ? "0" : priceText);
				float newPrice = price;
				
				if( operator.equals("-") )
					newPrice = price - Float.parseFloat(value);
				else if( operator.equals("+") )
					newPrice = price + Float.parseFloat(value);
				
				if (newPrice > 0)
					priceField.setText( FormatUtil.formatPrice(newPrice) );
				
				priceField.setSelection( priceField.getText().length() );
				
				break;
			case AppConstants.TRADE_INPUT_QUANTITY :
				String quantityText = quantityField.getText().toString().replace(",", "").trim();
				long quantity 		= Long.parseLong(quantityText.trim().length() == 0 ? "0" : quantityText);
				long newQuantity 	= quantity;
				
				if( operator.equals("-") )
					newQuantity = quantity - Long.parseLong(value);
				else if( operator.equals("+") )
					newQuantity = quantity + Long.parseLong(value);
				
				if(newQuantity > 0)
					quantityField.setText( FormatUtil.formatLong(newQuantity) );
					
				quantityField.setSelection( quantityField.getText().length() );
				
				break;
			case AppConstants.TRADE_INPUT_STOPLIMIT :
				String stopLimitText= stopLimitField.getText().toString().replace(",", "").trim();
				float stopLimit 	= Float.parseFloat(stopLimitText.trim().length() == 0 ? "0" : stopLimitText);
				float newStopLimit	= stopLimit;
				if(operator.equals("-") )
					newStopLimit = stopLimit - Float.parseFloat(value);
				else if( operator.equals("+") )
					newStopLimit = stopLimit + Float.parseFloat(value);
				
				if(newStopLimit > 0)
					stopLimitField.setText( FormatUtil.formatPrice(newStopLimit) );
				
				stopLimitField.setSelection( stopLimitField.getText().length() );
				
				break;
				
			// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
			case AppConstants.TRADE_INPUT_MINIMUM_QUANTITY :
				String strMinQty 	= minQtyField.getText().toString().replace(",", "").trim();
				long lngMinQty 		= Long.parseLong(strMinQty.trim().length() == 0 ? "0" : strMinQty);
				long lngNewMinQty 	= lngMinQty;
				
				if( operator.equals("-") )
					lngNewMinQty = lngMinQty - Long.parseLong(value);
				else if( operator.equals("+") )
					lngNewMinQty = lngMinQty + Long.parseLong(value);
				
				if(lngNewMinQty > 0)
					minQtyField.setText( FormatUtil.formatLong(lngNewMinQty) );
					
				minQtyField.setSelection( minQtyField.getText().length() );
				
				break;
			
			case AppConstants.TRADE_INPUT_DISCLOSED_QTY :
				String strDisclosedQty 	= disclosedQtyField.getText().toString().replace(",", "").trim();
				long lngDisclosedQty 	= Long.parseLong(strDisclosedQty.trim().length() == 0 ? "0" : strDisclosedQty);
				long lngNewDisclosedQty = lngDisclosedQty;
				
				if( operator.equals("-") )
					lngNewDisclosedQty = lngDisclosedQty - Long.parseLong(value);
				else if( operator.equals("+") )
					lngNewDisclosedQty = lngDisclosedQty + Long.parseLong(value);
				
				if(lngNewDisclosedQty > 0)
					disclosedQtyField.setText( FormatUtil.formatLong(lngNewDisclosedQty) );
					
				disclosedQtyField.setSelection( disclosedQtyField.getText().length() );
				
				break;
			// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
		}
		// Mary@20120905 - Fixes_Request-20120815, ReqNo.6 - END
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		// Checks whether a hardware or on-screen keyboard is available
		if (newConfig.keyboardHidden == Configuration.KEYBOARDHIDDEN_YES) {
			if(isLayoutAdjust)
				isLayoutAdjust = false;
		}
	}

	private void initOrderPadScreen(){	
		TextView stockNameLabel	= (TextView) this.findViewById(R.id.stockNameLabel);
		stockNameLabel.setText( stockSymbol.getStockName(true) );
		
		TextView companyName 	= (TextView) this.findViewById(R.id.companyNameLabel);
		companyName.setText( stockSymbol.getCompanyName() );
		
		arrowImage 				= (ImageView) this.findViewById(R.id.arrowImage);
		
		priceLabel 				= (TextView) this.findViewById(R.id.priceLabel);
		priceChangeLabel 		= (TextView) this.findViewById(R.id.priceChangeLabel);
		percentChangeLabel 		= (TextView) this.findViewById(R.id.pricePercentLabel);

		// Added by Mary@20120906 - Fixes_Request-20120719, ReqNo.12 - START
		priceField.addTextChangedListener(new TextWatcher(){
			public void afterTextChanged(Editable editable){
				String strTemp	= editable.toString().trim();
				if(strTemp.length() > 0 && priceField.isEnabled() && ( strPriceFieldAfterEdit == null || (strPriceFieldAfterEdit != null && !strPriceFieldAfterEdit.equals(strTemp) ) ) )
					strPriceFieldAfterEdit = strTemp;

				strTemp			= null;
			}
			
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3){				
			}

			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3){			
			}
		});
		// Added by Mary@20120906 - Fixes_Request-20120719, ReqNo.12 - END
		
		
		/* TRADE - STOP LIMIT PRICE */
		stopLimitField.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable editable) {

				// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
				String strTemp	= editable.toString().trim();
				if(strTemp.length() > 0 && stopLimitField.isEnabled() && ( strStopLimitPriceFieldAfterEdit == null || (strStopLimitPriceFieldAfterEdit != null && !strStopLimitPriceFieldAfterEdit.equals(strTemp) ) ) )
					strStopLimitPriceFieldAfterEdit = strTemp;

				strTemp			= null;
				// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
			}
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {				
			}

			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {			
			}
		});
		
		
		/* TRADE - QUANTITY */
		quantityField.addTextChangedListener(new TextWatcher() {
			String strQtyFieldBeforeEdit	= null;

			public void afterTextChanged(Editable editable) {
				long lngText	= 0;
				
				String strTemp	= editable.toString().trim();
				if( strTemp.length() > 0 && !strTemp.equals(strQtyFieldBeforeEdit) ){
					lngText	= Long.parseLong(strTemp);
					
					editable.replace(0, editable.length(), String.valueOf(lngText) );
				}
				
				strTemp			= editable.toString().trim();
				if(quantityField.isEnabled() && strTemp.length() > 0 
						&& ( strQtyFieldAfterEdit == null || ( strQtyFieldAfterEdit != null && !strQtyFieldAfterEdit.equals(strTemp) ) ) )
					strQtyFieldAfterEdit 		= strTemp;

				strTemp			= null;
			}

			public void beforeTextChanged(CharSequence arg0, int start, int count, int after) {
				String strTemp	= arg0.toString();
				if(strQtyFieldBeforeEdit == null || ( strQtyFieldBeforeEdit != null && !strQtyFieldBeforeEdit.equals(strTemp) ) )
					strQtyFieldBeforeEdit	= strTemp;
				
				strTemp			= null;
			}
			// Mary@20120830 - Change_Request-20120719, ReqNo.12 - END
			
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {			
			}

		});

		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		/* TRADE - DISCLOSED QUANTITY */
		disclosedQtyField.addTextChangedListener(new TextWatcher() {
			String strDisclosedQtyFieldBeforeEdit	= null;
			
			public void afterTextChanged(Editable editable) {
				long lngText	= 0;
				
				String strTemp	= editable.toString().trim();
				if( strTemp.length() > 0 && !strTemp.equals(strDisclosedQtyFieldBeforeEdit) ){
					lngText	= Long.parseLong( editable.toString() );
					
					editable.replace(0, editable.length(), String.valueOf(lngText) );
				}
				
				strTemp			= editable.toString().trim();
				if(disclosedQtyField.isEnabled() && strTemp.length() > 0 
						&& ( strDisclosedQtyFieldAfterEdit == null || ( strDisclosedQtyFieldAfterEdit != null && !strDisclosedQtyFieldAfterEdit.equals(strTemp) ) ) )
					strDisclosedQtyFieldAfterEdit 		= strTemp;

				strTemp			= null;
			}

			public void beforeTextChanged(CharSequence arg0, int start, int count, int after) {
				String strTemp	= arg0.toString();
				if(strDisclosedQtyFieldBeforeEdit == null || ( strDisclosedQtyFieldBeforeEdit != null && !strDisclosedQtyFieldBeforeEdit.equals(strTemp) ) )
					strDisclosedQtyFieldBeforeEdit	= strTemp;
				
				strTemp			= null;
			}
			
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {			
			}

		});
		
		/* TRADE - MINIMUM QUANTITY */
		minQtyField.addTextChangedListener(new TextWatcher() {
			String strMinQtyFieldBeforeEdit	= null;
			
			public void afterTextChanged(Editable editable) {
				long lngText	= 0;
				
				String strTemp	= editable.toString().trim();
				if( strTemp.length() > 0 && !strTemp.equals(strMinQtyFieldBeforeEdit) ){
					lngText	= Long.parseLong( editable.toString() );
					
					editable.replace(0, editable.length(), String.valueOf(lngText) );
				}
				
				strTemp			= editable.toString().trim();
				if(minQtyField.isEnabled() && strTemp.length() > 0 
						&& ( strMinQtyFieldAfterEdit == null || ( strMinQtyFieldAfterEdit != null && !strMinQtyFieldAfterEdit.equals(strTemp) ) ) )
					strMinQtyFieldAfterEdit 		= strTemp;

				strTemp			= null;
			}

			public void beforeTextChanged(CharSequence arg0, int start, int count, int after) {
				String strTemp	= arg0.toString();
				if(strMinQtyFieldBeforeEdit == null || ( strMinQtyFieldBeforeEdit != null && !strMinQtyFieldBeforeEdit.equals(strTemp) ) )
					strMinQtyFieldBeforeEdit	= strTemp;
				
				strTemp			= null;
			}
			
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {			
			}

		});
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		
		Handler keyboardHandler 	= new Handler();
		keyboardHandler.postDelayed(new Runnable() {
			public void run() {
				InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(priceField.getWindowToken(), 0);
			}
		}, 500);
		
		final Button buyButton = (Button) this.findViewById(R.id.buyButton);
		if (orderPadType == AppConstants.ORDER_PAD_TRADE)
			buyButton.setText(getResources().getString(R.string.orderpad_btn_buy));
		else if (orderPadType == AppConstants.ORDER_PAD_REVISE)
			buyButton.setText(getResources().getString(R.string.orderpad_btn_revise));

		buyButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				// Added by Mary@20121023 - Fixes_Request-20121018, ReqNo.7 - START				
				if( orderPadType == AppConstants.ORDER_PAD_REVISE && !hasRevise() ){
					TradeActivity.this.showDialog(TradeActivity.DIALOG_NO_ORDER_REVISE_DETECTED);
					return;
				}
				// Added by Mary@20121023 - Fixes_Request-20121018, ReqNo.7 - END
				
				String text			= priceField.getText().toString().replace(",", "");
				text 				= text.length()==0? "0" : text;

				// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.3
				float fltReviseOrdPrc= Float.parseFloat(text);

				if( AppConstants.getListDerivativesExchgCode().contains(TradeActivity.this.strSelectedStockExchangeCode) ){
					//Added by Thinzar, Change_Request-20140901, ReqNo.7 - START
					if(strSelectedStockExchangeCode.equalsIgnoreCase("MY")){
						//allow price zero to go through
						DefinitionConstants.Debug("debug: letting price 0 to go thru");
					}
					//Added by Thinzar, Change_Request-20140901, ReqNo.7 - END
					else {
						if(instrument.indexOf("|")==-1){

							if( fltReviseOrdPrc == 0 && priceField.isEnabled() ){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_ORDER_PRICE);
								return;
							}
						}
					}
				}else{

					if( fltReviseOrdPrc == 0 && priceField.isEnabled() ){
						TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_ORDER_PRICE);
						return;
					}
				}

				if (orderPadType == AppConstants.ORDER_PAD_REVISE){

					if( priceField.isEnabled() ){
						if(mustIncreasePrice){
							if(fltReviseOrdPrc<fltReviseOrdPrice){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_ORDER_PRICE_ADD);
								return;
							}

						}else if(mustDecreasePrice){
							if(fltReviseOrdPrc>fltReviseOrdPrice){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_ORDER_PRICE_MINUS);
								return;
							}
						}
					}
				}
				
				//Fixes_Request-20161101-ReqNo. 16 - START
				if(minQtyField.isEnabled()){
					text 			= minQtyField.getText().toString().replace(",", "");
					text 			= text.length()==0 ? "0" : text;
					long minQty		= Long.parseLong(text) * lngCurrentLotSize;
					
					String quantityText = quantityField.getText().toString().replace(",", "").trim();
					quantityText 		= quantityText.length()==0 ? "0" : quantityText;
					long qty 			= Long.parseLong(quantityText) * lngCurrentLotSize;
					
					if(minQty > qty){
						TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_MINIMUM_QUANTITY);
						return;
					}
				}
				//Fixes_Request-20161101-ReqNo. 16 - END

				if( stopLimitField.isEnabled() ){
					text 					= stopLimitField.getText().toString().trim();
					text					= text.equals("-") || text.equals("") ? "0" : text;

					float fltReviseStopLimit= Float.parseFloat(text);
					if (fltReviseStopLimit == 0) {
						TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_STOPLIMIT_PRICE);
						return;
					}

					if(orderPadType == AppConstants.ORDER_PAD_REVISE){

						if(mustIncreaseStopLimitPrice){
							if(fltReviseStopLimit<TradeActivity.this.fltReviseStopLimit){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_STOPLIMIT_PRICE_ADD);
								return;
							}
						}else if(mustDecreaseStopLimitPrice){
							if(fltReviseStopLimit>TradeActivity.this.fltReviseStopLimit){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_STOPLIMIT_PRICE_MINUS);
								return;
							}
						}
					}
				}

				text 				= quantityField.getText().toString().replace(",", "");
				text 				= text.length()==0 ? "0" : text;

				long lngReviseOrdQty= Long.parseLong(text);
				if(lngReviseOrdQty == 0){
					TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_ORDER_QUANTITY);
					return;
				}
				if (orderPadType == AppConstants.ORDER_PAD_REVISE){

					long lngOrginalOrdQty	= lngCurrentLotSize == 0 ? 0 : (TradeActivity.this.lngReviseOrdQty / lngCurrentLotSize);

					if( quantityField.isEnabled() ){
						if(mustIncreaseQuantity){

							if(lngReviseOrdQty < lngOrginalOrdQty){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_ORDER_QUANTITY_ADD);
								return;
							}

						}else if(mustDecreaseQuantity){
							if(lngReviseOrdQty > lngOrginalOrdQty){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_ORDER_QUANTITY_MINUS);
								return;
							}
						}

					// Mary@20120905 - Fixes_Request-20120815, ReqNo.7 & 3 - END
					}
				}
				
				// Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
				if (orderPadType == AppConstants.ORDER_PAD_REVISE){
					text 						= disclosedQtyField.getText().toString().replace(",", "");
					text 						= text.length()==0 ? "0" : text;
					long lngReviseDisclosedQty	= Long.parseLong(text);
					if( disclosedQtyField.isEnabled() ){
						/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
						long lngOrginalDisclosedQty	= TradeActivity.this.lngReviseDisclosedQty / lngCurrentLotSize;
						*/
						long lngOrginalDisclosedQty	= lngCurrentLotSize == 0 ? 0 : (TradeActivity.this.lngReviseDisclosedQty / lngCurrentLotSize);
						if(mustIncreaseDisclosedQty){
							if(lngReviseDisclosedQty < lngOrginalDisclosedQty){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_DISCLOSED_QUANTITY_ADD);
								return;
							}
						}else if(mustDecreaseDisclosedQty){
							if(lngReviseDisclosedQty > lngOrginalDisclosedQty){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_DISCLOSED_QUANTITY_MINUS);
								return;
							}
						}
					}
					
					text 						= minQtyField.getText().toString().replace(",", "");
					text 						= text.length()==0 ? "0" : text;
					long lngReviseMinQty		= Long.parseLong(text);
					if( minQtyField.isEnabled() ){
						/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
						long lngOrginalMinQty	= TradeActivity.this.lngReviseMinQty / lngCurrentLotSize;
						*/
						long lngOrginalMinQty	= lngCurrentLotSize == 0 ? 0 : (TradeActivity.this.lngReviseMinQty / lngCurrentLotSize);
						if(mustIncreaseDisclosedQty){
							if(lngReviseMinQty < lngOrginalMinQty){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_MINIMUM_QUANTITY_ADD);
								return;
							}
						}else if(mustDecreaseDisclosedQty){
							if(lngReviseMinQty > lngOrginalMinQty){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_MINIMUM_QUANTITY_MINUS);
								return;
							}
						}
					}
				}
				// Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
				
				// Added by Mary@20130108 - Fixes_Request-20121221, ReqNo.24 - START
				if( orderPadType != AppConstants.ORDER_PAD_REVISE || (strReviseAction != null && strReviseAction.trim().equalsIgnoreCase("BUY") ) ){
					boolean isAllowedBuyAction	= true;
					for(Iterator<StockCurrencyPaymentMethod> itr=exchangeInfo.getLstStockCurrencyPaymentMethods().iterator(); itr.hasNext();){
						StockCurrencyPaymentMethod scpm	= itr.next();
						if( scpm.getStrPaymentMethod().equalsIgnoreCase(selectedPayment) ){
							isAllowedBuyAction	= scpm.isAllowedBuyAction();
							break;
						}
					}
					if( ! isAllowedBuyAction ){
						TradeActivity.this.showDialog(TradeActivity.DIALOG_PYMT_NOT_ALLOWED_BUY_ACTION);
						return;
					}
				}
				// Added by Mary@20130108 - Fixes_Request-20121221, ReqNo.24 - END

				actionType 					= translateActionType( buyButton.getText().toString() );
				
				// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3 - START
				RDSInfo rdsInfo				= AppConstants.getRDSInfo();

				if(rdsInfo != null 
						&& !rdsInfo.getStrLocalExchangeCode().contains(strSelectedStockExchangeCode) 
						&& !rdsInfo.getStrLocalExchangeCode().contains(strTradeExchgCode) 
						&& rdsInfo.isRDSEnable() 
				){
					Intent intent	= new Intent();
					intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
					
					if( AppConstants.brokerConfigBean.isEnableEncryption() )
						intent.putExtra(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
					else
						intent.putExtra(ParamConstants.USERPARAM_TAG, atpUserParam);
					
					intent.putExtra(ParamConstants.SENDERCODE_TAG, senderCode);
					intent.putExtra(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
					intent.putExtra(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
					intent.putExtra(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
					intent.putExtra(ParamConstants.EXCHANGECODE_TAG, tradeAccount.getExchangeCode());
					intent.putExtra(ParamConstants.RDS_VERSIONNO_TAG, rdsInfo.getStrRDSVersion());

					// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - START
					intent.putExtra(ParamConstants.RDS_URL_TAG, rdsInfo.getStrRDSUrl());
					intent.putExtra(ParamConstants.IS_BLOCK_BY_RDS_TAG, rdsInfo.isBlockByRDS());
					// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - END
					
					intent.setClass(TradeActivity.this, RiskDisclosureStatementActivity.class);
					startActivityForResult(intent, AppConstants.RDS_REQUEST);
					return;
				}
				// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3 - END
				
				//Added by Thinzar@20140829 - Fixes_Request-20140820, ReqNo. 8 - START
				if(selectedPayment != null && selectedPayment.equalsIgnoreCase("CUT")){
					//calculate order value : for CUT case - price * quantity (referred to the old code exactly)
					String priceText	= priceField.getText().toString().replace(",", "");
					priceText			= priceText.length()==0? "0":priceText;
					float price			= Float.parseFloat(priceText);
					
					String qtyText 		= quantityField.getText().toString().replace(",", "");
					qtyText 			= qtyText.length()==0 ? "0" : qtyText;
					long quantity 		= Long.parseLong(qtyText);
					quantity 			= quantity * lngCurrentLotSize;
					double value		= price * quantity;
					double valueBeforeRevise 	= 0;
					
					if(orderPadType == AppConstants.ORDER_PAD_REVISE){
						valueBeforeRevise	= fltReviseOrdPrice * lngReviseOrdQty;
					}
					
					try{
						if( ! selectedCurrency.equalsIgnoreCase(strStockCurrency) ){
							String strActionType						= buyButton.getText().toString();
							List<CurrencyExchangeInfo> lstCurrExchgInfo	= ( (StockApplication) getApplication() ).getCurrencyExchangeInfos();
							CurrencyExchangeInfo currencyExchgInfo		= AppConstants.getCurrencyExchangInfo(strStockCurrency, AppConstants.getStrBaseCurrencyCode(), lstCurrExchgInfo );
							if(currencyExchgInfo != null){
								value	= AppConstants.convertDefaultCurrencyToOtherCurrency(value, strStockCurrency, AppConstants.getStrBaseCurrencyCode(), strActionType, lstCurrExchgInfo);
								if( ! selectedCurrency.equalsIgnoreCase( AppConstants.getStrBaseCurrencyCode() ) ){
									value	= AppConstants.convertDefaultCurrencyToOtherCurrency(value, strActionType, currencyExchgInfo);
									if(orderPadType == AppConstants.ORDER_PAD_REVISE){
										valueBeforeRevise	= AppConstants.convertDefaultCurrencyToOtherCurrency(valueBeforeRevise, strActionType, currencyExchgInfo);
									}
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					
					//if other currency, allows to proceed
					if(selectedCurrency.equalsIgnoreCase("SGD")){
						if (orderPadType == AppConstants.ORDER_PAD_TRADE){
							if(value > investmentPower){
								//DefinitionConstants.Debug("value:" + value + ", investment Power:" + investmentPower);
								
								if(isCUTAccount) TradeActivity.this.showDialog(TradeActivity.DIALOG_INSUFFICIENT_BALANCE_CUT);
								else TradeActivity.this.showDialog(TradeActivity.DIALOG_INSUFFICIENT_BALANCE_NONCUT);
								
								return;
								
							}
						} else if(orderPadType == AppConstants.ORDER_PAD_REVISE){
							double balancePayment		= value - valueBeforeRevise;
							//DefinitionConstants.Debug("debug: orderpad - CUT - valueBeforeRevise:" + valueBeforeRevise + ", value:" + value + ", balancePayment:" + balancePayment);
							if(balancePayment > investmentPower){
								TradeActivity.this.showDialog(TradeActivity.DIALOG_INSUFFICIENT_BALANCE_CUT);
								return;
							}
						}
					}
				} 
				//Added by Thinzar@20140829 - Fixes_Request-20140820, ReqNo. 8 - END
				
				StockApplication application= (StockApplication) TradeActivity.this.getApplication();

				if( AppConstants.info2FA.isForceUse1FA() 
					|| ( AppConstants.info2FA.is1FAAllowed() && AppConstants.info2FA.getDeviceList().isEmpty() && AppConstants.info2FA.getMobilePhone2FA() == null)
				){
					showTradePin1FADialog();
				}else if( !AppConstants.info2FA.isForceUse1FA() && ( AppConstants.info2FA.is2FARequired() && !AppConstants.info2FA.hasSuccessVerify2FA() ) ){
					if( AppConstants.info2FA.getDeviceList().isEmpty() && AppConstants.info2FA.getMobilePhone2FA() == null){
						alertException.setMessage("In order to proceed, you need to have a OneKey Token registered and linked with CIMB Securities. Please contact your trading representative for more information.");
						alertException.setButton(getResources().getString(R.string.dialog_button_ok),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.dismiss();
								}
							});
		
						if( ! TradeActivity.this.isFinishing() )
							alertException.show();
					}else{
						showTradePin2FADialog();
					}
				// Added by Mary@20131008 - Change_Request-20130711, ReqNo.14 - END
				}else if( AppConstants.brokerConfigBean.isEnablePinFeatures() && !application.isRememberPin() ){
					TradeActivity.this.showTradePinDialog();
				}else{
					tradingPin = application.getTradingPin();
					TradeActivity.this.handleTradeAction();
				}
			}

		});

		final Button sellButton = (Button) this.findViewById(R.id.sellButton);
		if(orderPadType == AppConstants.ORDER_PAD_TRADE)
			sellButton.setText(getResources().getString(R.string.orderpad_btn_sell));
		else if (orderPadType == AppConstants.ORDER_PAD_REVISE)
			sellButton.setText(getResources().getString(R.string.orderpad_btn_cancel));
		
		sellButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				String text 				= priceField.getText().toString().replace(",", "");
				text 						= text.length()==0? "0":text;
				// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.3 & 9
				float fltReviseOrdPrc		= Float.parseFloat(text);

				//Added by Thinzar, Change_Request-20140901, ReqNo.7 - START
				if(strSelectedStockExchangeCode.equalsIgnoreCase("MY")){
					//allow price zero to go through for MY exchange
					DefinitionConstants.Debug("debug: letting price 0 to go thru");
				}
				//Added by Thinzar, Change_Request-20140901, ReqNo.7 - END
				else {
					if( fltReviseOrdPrc == 0 && priceField.isEnabled() ){
				
					TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_ORDER_PRICE);
					return;
					}
				}
				
				text 						= quantityField.getText().toString().replace(",", "");
				text 						= text.length()==0? "0":text;
				// Added by Mary@20120905 - Fixes_Request-20120815, ReqNo.3
				float fltReviseOrdQty		= Float.parseFloat(text);

				if(fltReviseOrdQty == 0){
					TradeActivity.this.showDialog(TradeActivity.DIALOG_INVALID_ORDER_QUANTITY);
					return;
				}
				
				// Added by Mary@20130108 - Fixes_Request-20121221, ReqNo.24 - START
				if( orderPadType != AppConstants.ORDER_PAD_REVISE){
					boolean isAllowedSellAction	= true;
					for(Iterator<StockCurrencyPaymentMethod> itr=exchangeInfo.getLstStockCurrencyPaymentMethods().iterator(); itr.hasNext();){
						StockCurrencyPaymentMethod scpm	= itr.next();
						if( scpm.getStrPaymentMethod().equalsIgnoreCase(selectedPayment) ){
							isAllowedSellAction	= scpm.isAllowedSellAction();
							break;
						}
					}
					if( ! isAllowedSellAction ){
						TradeActivity.this.showDialog(TradeActivity.DIALOG_PYMT_NOT_ALLOWED_SELL_ACTION);
						return;
					}
				}
				// Added by Mary@20130108 - Fixes_Request-20121221, ReqNo.24 - END
				
				actionType 					= translateActionType(sellButton.getText().toString());
				
				// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3 - START
				RDSInfo rdsInfo	= AppConstants.getRDSInfo();
				/* Mary@20130220 - Fixes_Request-20130108, ReqNo.11
				if(rdsInfo != null && !rdsInfo.getStrLocalExchangeCode().contains(strSelectedStockExchangeCode) && AppConstants.getRDSInfo().isRDSEnable() ){
				*/
				if(rdsInfo != null 
						&& !rdsInfo.getStrLocalExchangeCode().contains(strSelectedStockExchangeCode) 
						&& !rdsInfo.getStrLocalExchangeCode().contains(strTradeExchgCode) 
						&& rdsInfo.isRDSEnable() 
				){
					Intent intent	= new Intent();
					intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
					
					if( AppConstants.brokerConfigBean.isEnableEncryption() )
						intent.putExtra(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
					else
						intent.putExtra(ParamConstants.USERPARAM_TAG, atpUserParam);
					
					intent.putExtra(ParamConstants.SENDERCODE_TAG, senderCode);
					intent.putExtra(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
					intent.putExtra(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
					intent.putExtra(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
					intent.putExtra(ParamConstants.EXCHANGECODE_TAG, tradeAccount.getExchangeCode());
					intent.putExtra(ParamConstants.RDS_VERSIONNO_TAG, rdsInfo.getStrRDSVersion());
					// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - START
					intent.putExtra(ParamConstants.RDS_URL_TAG, rdsInfo.getStrRDSUrl());
					intent.putExtra(ParamConstants.IS_BLOCK_BY_RDS_TAG, rdsInfo.isBlockByRDS());
					// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - END
					
					intent.setClass(TradeActivity.this, RiskDisclosureStatementActivity.class);
					startActivityForResult(intent, AppConstants.RDS_REQUEST);
					return;
				}
				// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3 - END
				
				StockApplication application= (StockApplication) TradeActivity.this.getApplication();

				if( AppConstants.info2FA.isForceUse1FA() 
					|| ( AppConstants.info2FA.is1FAAllowed() && AppConstants.info2FA.getDeviceList().isEmpty() && AppConstants.info2FA.getMobilePhone2FA() == null)
				){
					showTradePin1FADialog();
				}else if( !AppConstants.info2FA.isForceUse1FA() && ( AppConstants.info2FA.is2FARequired() && !AppConstants.info2FA.hasSuccessVerify2FA() ) ){
					if( AppConstants.info2FA.getDeviceList().isEmpty() && AppConstants.info2FA.getMobilePhone2FA() == null){
						alertException.setMessage("In order to proceed, you need to have a OneKey Token registered and linked with CIMB Securities. Please contact your trading representative for more information.");
						alertException.setButton(getResources().getString(R.string.dialog_button_ok),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.dismiss();
								}
							});
		
						if( ! TradeActivity.this.isFinishing() )
							alertException.show();
					}else{
						showTradePin2FADialog();
					}
				// Added by Mary@20131008 - Change_Request-20130711, ReqNo.14 - END

				}else if( AppConstants.brokerConfigBean.isEnablePinFeatures() && !application.isRememberPin() ){
					TradeActivity.this.showTradePinDialog();
				}else{
					tradingPin = application.getTradingPin();
					TradeActivity.this.handleTradeAction();					
				}
			}
		});

		//Change_Request-20170601, ReqNo.8
		if(orderActionFromIntent != null){
			if(orderActionFromIntent.equalsIgnoreCase("buy")){
				sellButton.setBackgroundResource(R.drawable.button_trd_disabled);
			} else if(orderActionFromIntent.equalsIgnoreCase("sell")){
				buyButton.setBackgroundResource(R.drawable.button_trd_disabled);
			}else if(orderActionFromIntent.equalsIgnoreCase("shortsell")){
				shortSellBox.setChecked(true);
				buyButton.setBackgroundResource(R.drawable.button_trd_disabled);
			}
		}
	}
	
	// Added by Mary@20121023 - Fixes_Request-20121018, ReqNo.7 - START				
	private boolean hasRevise(){
		String strTemp;
		float fltRevisedOrderPrice;
		float fltRevisedStopLimitPrice;
		long lngRevisedQuantity;

		// Added by Mary@20121123 - Change_Request-20121106, ReqNo.1 - START
		long lngRevisedDisclosedQty;
		long lngRevisedMinQty;
		String strGTD;
		
		try{			
			strTemp					= priceField.getText().toString().replaceAll(",", "").trim();
			fltRevisedOrderPrice	= strTemp.length() == 0 ? 0 : Float.parseFloat(strTemp);
			
			strTemp					= quantityField.getText().toString().replaceAll(",", "").trim();
			lngRevisedQuantity		= strTemp.length() == 0 ? 0 : Long.parseLong(strTemp) * lngCurrentLotSize;
			
			strTemp					= stopLimitField.getText().toString().replaceAll(",", "").trim();
			fltRevisedStopLimitPrice= strTemp.length() == 0 ? 0 : Float.parseFloat(strTemp);
			
			strGTD					= dateField.getText().toString().trim();
			
			// Added by Mary@20121123 - Change_Request-20121106, ReqNo.1 - START
			strTemp					= disclosedQtyField.getText().toString().replaceAll(",", "").trim();
			lngRevisedDisclosedQty	= strTemp.length() == 0 ? 0 : Long.parseLong(strTemp) * lngCurrentLotSize;
			
			strTemp					= minQtyField.getText().toString().replaceAll(",", "").trim();
			lngRevisedMinQty		= strTemp.length() == 0 ? 0 : Long.parseLong(strTemp) * lngCurrentLotSize;
			// Added by Mary@20121123 - Change_Request-20121106, ReqNo.1 - END

			if( ( priceField.isEnabled() && priceField.isShown() ) && fltRevisedOrderPrice != fltReviseOrdPrice )
				return true;
			
			if( ( isEditableQuantity || ( quantityField.isEnabled() && quantityField.isShown() ) ) && lngRevisedQuantity != lngReviseOrdQty)
				return true;
			
			if( ( dateField.isEnabled() && dateField.isShown() ) && ! strGTD.equalsIgnoreCase( new SimpleDateFormat(AppConstants.PARAM_DATE_FORMAT).format( calReviseGTD.getTime() ) ) )
				return true;
			
			if( ( isEditableStopLimit || ( stopLimitField.isEnabled() && stopLimitField.isShown() ) ) && fltRevisedStopLimitPrice != fltReviseStopLimit)
				return true;
			
			// Added by Mary@20121123 - Change_Request-20121106, ReqNo.1 - START
			if( ( isEditableDisclosedQty || ( disclosedQtyField.isEnabled() && disclosedQtyField.isShown() ) ) && lngRevisedDisclosedQty != lngReviseDisclosedQty)
				return true;
			
			if( ( isEditableMinQty || ( minQtyField.isEnabled() && minQtyField.isShown() ) ) && lngRevisedMinQty != lngReviseMinQty)
				return true;
			// Added by Mary@20121123 - Change_Request-20121106, ReqNo.1 - END
			
			if( isEditableOrderType && !selectedOrderType.equalsIgnoreCase(strReviseOrderType) )
				return true;
			
			if( isEditableValidity && !selectedValidity.equalsIgnoreCase(strReviseValidity) )
				return true;
			
			// Added by Mary@20121123 - Change_Request-20121106, ReqNo.1 - START
			/* Mary@20130626 - Fixes_Request-20130523, ReqNo.22
			if( isEditableCurrency && !selectedCurrency.equalsIgnoreCase(strReviseCurrency) )
			*/
			if( isEditableCurrency && ( selectedCurrency != null && !selectedCurrency.equalsIgnoreCase(strReviseCurrency) ) )
				return true;

			/* Mary@20130626 - Fixes_Request-20130523, ReqNo.22
			if( isEditablePayment && !selectedPayment.equalsIgnoreCase(strRevisePaymentType) )
			*/
			if( isEditablePayment && (selectedPayment != null && !selectedPayment.equalsIgnoreCase(strRevisePaymentType) ) )
				return true;
			// Added by Mary@20121123 - Change_Request-20121106, ReqNo.1 - END
			
			//Added by Thinzar@20140819, Change-Request-20140801, ReqNo4 - START
			if(spnTriggerType.isEnabled()){
				if(!strReviseTriggerType.equalsIgnoreCase(selectedTriggerType))
					return true;
			}
			
			if(spnTriggerDirection.isEnabled()){
				if(!strReviseTriggerDirection.equalsIgnoreCase(selectedTriggerDirection))
					return true;
			}
			//Added by Thinzar@20140819, Change-Request-20140801, ReqNo4 - END
			
			return false;
		}catch(Exception e){
			e.printStackTrace();
			return true;
		}finally{
			strTemp					= null;
			fltRevisedOrderPrice	= 0;
			fltRevisedStopLimitPrice= 0;
			lngRevisedQuantity		= 0;
			lngRevisedDisclosedQty	= 0;
			lngRevisedMinQty		= 0;
			isEditablePrice			= false;
			isEditableQuantity		= false;
			isEditableStopLimit		= false;
			isEditableDisclosedQty	= false;
			isEditableMinQty		= false;
			isEditablePayment		= false;
			isEditableCurrency		= false;
		}
	}
	// Added by Mary@20121023 - Fixes_Request-20121018, ReqNo.7 - END

	private void repaintOrderPadScreen(){

		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		initPriceFieldBaseOnTradePreference();
		initQuantityFieldBaseOnTradePreference();
		initStopLimitPriceField();
		initDisclosedQtyField();
		initMinimumQtyField();
		updateDateField(dateField, intReviseGTDDay, intReviseGTDMonth, intReviseGTDYear);
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
		
		if (stockSymbol.getPriceChange() > 0)
			arrowImage.setImageResource(LayoutSettings.upArrow);
		else if (stockSymbol.getPriceChange() < 0)
			arrowImage.setImageResource(LayoutSettings.downArrow);
		else
			arrowImage.setImageDrawable(null);

		priceLabel.setText(FormatUtil.formatPrice(stockSymbol.getLastDone(), "-", strSelectedStockExchangeCode));
		priceChangeLabel.setText( FormatUtil.formatPriceChangeByExchange(stockSymbol.getPriceChange(), strSelectedStockExchangeCode));
		percentChangeLabel.setText( FormatUtil.formatPricePercent( stockSymbol.getChangePercent() ) );

		int intStatusColor	= getResources().getColor(R.color.NoStatus_FgColor);
		if(stockSymbol.getPriceChange() > 0)
			intStatusColor	= getResources().getColor(R.color.UpStatus_BgColor);
		else if(stockSymbol.getPriceChange() < 0)
			intStatusColor	= getResources().getColor(R.color.DownStatus_BgColor);
		
		priceLabel.setTextColor(intStatusColor);
		priceChangeLabel.setTextColor(intStatusColor);
		percentChangeLabel.setTextColor(intStatusColor);

		/* TRADE - ORDER TYPE */
		constructOrderTypeTableView( (orderPadType == AppConstants.ORDER_PAD_REVISE) ? exchangeInfo.getReviseOrderTypes() : exchangeInfo.getOrderTypes(), isEditableOrderType);
		
		
		/* TRADE - VALIDITY */
		constructValidityTableView( (orderPadType == AppConstants.ORDER_PAD_REVISE) ? exchangeInfo.getReviseValidities() : exchangeInfo.getValidities(), isEditableValidity);
		
		//Added by Thinzar, Change_Request-20150401, ReqNo. 1 - START
		List<String> lstPaymentMethods 	= new ArrayList<String>();
		List<String> lstCurrencies		= new ArrayList<String>();
		lstPaymentMethods.clear();
		lstCurrencies.clear();
		isCUTAccount	= false;		//set back to false; 
		
		if(exchangeInfo.getPaymentConfigMap().size() > 0){
			
			Map<String, PaymentConfig> paymentConfigMap	= exchangeInfo.getPaymentConfigMap();
			
			if(tradeAccount.getAccountType().equalsIgnoreCase("B")){
				isCUTAccount	= true;
				
				if(paymentConfigMap.containsKey("B")){
					PaymentConfig config			= paymentConfigMap.get("B");
					lstPaymentMethods				= config.getPaymentMethods();
					String currencyString			= config.getCurrencyString();
					
					String[] arrCurrency			= currencyString.split(",");
					for(int j=0;j<arrCurrency.length;j++){
						lstCurrencies.add(arrCurrency[j]);
					}
				}
			
			} else{
				
				if(paymentConfigMap.containsKey("Def")){
					PaymentConfig config			= paymentConfigMap.get("Def");
					lstPaymentMethods				= config.getPaymentMethods();
					lstCurrencies					= config.getCurrencies();
				}
				
				if(lstPaymentMethods.size() == 0)	lstPaymentMethods	= exchangeInfo.getPaymentMethods();
				if(lstCurrencies.size()	== 0)		lstCurrencies		= exchangeInfo.getCurrencies();
			}
		}
		else{
			lstPaymentMethods	= exchangeInfo.getPaymentMethods();
			lstCurrencies		= exchangeInfo.getCurrencies();
		}
		//Added by Thinzar, Change_Request-20150401, ReqNo. 1 - END
		
		/* TRADE - CURRENCY */
		constructCurrencyTableView(lstCurrencies, isEditableCurrency);

		/* TRADE - PAYMENT */
		constructPaymentTableView(lstPaymentMethods, null, isEditablePayment);
				
		//Added by thinzar, Change_Request-20140801, ReqNo4
		constructTriggerTypeAndDirection();	//trigger type and direction

	}
	
	
	// Mary@20121121 - Change_Request-20121106, ReqNo.1 - END

	private int translateActionType(String text) {
		if(text.equalsIgnoreCase("Buy") )
			return AppConstants.TRADE_BUY_ACTION;
		else if(text.equalsIgnoreCase("Sell") )
			return AppConstants.TRADE_SELL_ACTION;
		else if ( text.equalsIgnoreCase("Revise") )
			return AppConstants.TRADE_REVISE_ACTION;
		else if(text.equalsIgnoreCase("Cancel") )
			return AppConstants.TRADE_CANCEL_ACTION;
		
		return 0;
	}

	private String translateActionName(int type) {

		switch(type){
			case AppConstants.TRADE_BUY_ACTION :
				return "Buy";
			case AppConstants.TRADE_SELL_ACTION :
				return "Sell";
			case AppConstants.TRADE_REVISE_ACTION :
				return "Revise";
			case AppConstants.TRADE_CANCEL_ACTION :
				return "Cancel";
			default :
				return null;
		}
		// Mary@20120905 - Fixes_Request-20120815, ReqNo.3 - END
	}



	private void showTradePinDialog() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			final Dialog dialog 	= new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.trade_pin_dialog);

			final EditText pinField = (EditText) dialog.findViewById(R.id.tradePinField);
			pinField.setTransformationMethod(PasswordTransformationMethod.getInstance());
			dialog.setOnShowListener(new OnShowListener() {
				public void onShow(DialogInterface dialog) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(pinField, InputMethodManager.SHOW_IMPLICIT);
				}
			});
	
			Button cancelButton 	= (Button) dialog.findViewById(R.id.cancelButton);
			cancelButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
					try{
						dialog.dismiss();
					// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
					}catch(Exception e){
						e.printStackTrace();
					}
					// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
				}
			});
			
			Button okButton 		= (Button) dialog.findViewById(R.id.okButton);
			okButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					tradingPin 				= pinField.getText().toString();
					CheckBox rememberBox 	= (CheckBox) dialog.findViewById(R.id.rememberCheckBox);
	
					if( rememberBox.isChecked() ){
						StockApplication application = (StockApplication) TradeActivity.this.getApplication();
						application.setRememberPin(true);
						application.setTradingPin(tradingPin);
					}
	
					TradeActivity.this.handleTradeAction();
					dialog.dismiss();
				}
			});
	
			// Added by Mary@20120810 - cancel dialog before reset message - START
			if( dialog.isShowing() )
				dialog.dismiss();
			// Added by Mary@20120810 - cancel dialog before reset message - END
			
			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! this.isFinishing() )
				dialog.show();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	public void handleTradeAction() {

		if( !skipConfirmBox.isChecked() ){
			HashMap<String,String> details 				= new HashMap<String, String>();
			
			details.put( "Account No", tradeAccount.getAccountNo());

			String balance 								= "";
			String strActionType						= this.translateActionName(actionType);
			// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6 - START
			if(isCreditLimit){
				dblTradingBalance = Double.parseDouble(tradeClientLimit.get("Trading Limit"));
				if(selectedPayment!= null && selectedPayment.equalsIgnoreCase("CUT"))
						balance = "Investment Power";
				else	balance="Trading Limit";
			}else{
				if(strActionType.equals("Buy") || (strActionType.equals("Revise") && strReviseAction.equals("Buy")) ||  (strActionType.equals("Cancel") && strReviseAction.equals("Buy"))){ 
					dblTradingBalance = Double.parseDouble(tradeClientLimit.get("Buy Limit"));
					balance			  = "Buy Limit";
				}else if(strActionType.equals("Sell") || (strActionType.equals("Revise") && strReviseAction.equals("Sell")) || (strActionType.equals("Cancel") && strReviseAction.equals("Sell"))){
					dblTradingBalance = Double.parseDouble(tradeClientLimit.get("Sell Limit"));
					balance			  = "Sell Limit";
				}
			}
			// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6 - END
		
			// Added by Mary@20121227 - Fixes_Request-20121102, ReqNo.11 - START
			List<CurrencyExchangeInfo> lstCurrExchgInfo	= ( (StockApplication) getApplication() ).getCurrencyExchangeInfos();
			String strCurrencyRate						= "1 " + strStockCurrency + " : 1.000 " + strStockCurrency;
			String strBalanceLabel						= AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDouble(dblTradingBalance);//balanceLabel.getText().toString()	//edited By Thinzar@20140925, Fixes_Request-20140820, ReqNo.32
			
			CurrencyExchangeInfo currencyExchgInfo		= null;
			
			//Added by Thinzar@20140926, Fixes_Request-20140820, ReqNo.29 - START
			String strCurrencyLabel						= AppConstants.brokerConfigBean.getCurrencyRateLabel();
			if(strCurrencyLabel == null)	strCurrencyLabel = "Currency Rate";
			//Added by Thinzar@20140926, Fixes_Request-20140820, ReqNo.29 - END
			
			try{
				currencyExchgInfo		= AppConstants.getCurrencyExchangInfo(strStockCurrency, AppConstants.getStrBaseCurrencyCode(), lstCurrExchgInfo );
				double dblCurrencyRate	= strActionType.equalsIgnoreCase("BUY") ? currencyExchgInfo.getDblSellRate() : currencyExchgInfo.getDblBuyRate();
				// e.g. 1 SGD : 2.4455 MYR
				strCurrencyRate			= currencyExchgInfo.getLngDenomination() + " " + strStockCurrency + " : " + FormatUtil.formatDouble3(dblCurrencyRate)  + " " + AppConstants.getStrBaseCurrencyCode();
				currencyExchgInfo		= AppConstants.getCurrencyExchangInfo(AppConstants.getStrBaseCurrencyCode(), strStockCurrency, lstCurrExchgInfo );
				
				if( ! strStockCurrency.equalsIgnoreCase( AppConstants.getStrBaseCurrencyCode() ) ){
					if(!selectedPayment.equalsIgnoreCase("CUT")){	//Added CUT filter by Thinzar, Fixes_Request-20140820,ReqNo 32 (if CUT, display inv. power in sett.Currency only) 
						double dblSettlementCurrencyBalance	= AppConstants.convertDefaultCurrencyToOtherCurrency(dblTradingBalance, strActionType, currencyExchgInfo);
						// e.g. <Default currency trading balance value> / <Stock currency trading balance value>
						strBalanceLabel						+= " / " + strStockCurrency + " " + FormatUtil.formatDouble(dblSettlementCurrencyBalance);
					}
					//Fixes_Request-20170103, ReqNo.12
					else{
						strBalanceLabel 					 = selectedCurrency + " " + FormatUtil.formatDouble(dblTradingBalance);
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			details.put(strCurrencyLabel, strCurrencyRate);

			// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6 
			details.put(balance,  strBalanceLabel);
			details.put("Action", strActionType);
			details.put("Order Type", selectedOrderType);
			
			ArrayList<String> sequences 				= new ArrayList<String>();
			sequences.add("Account No");
			// Added by Mary@20121227 - Fixes_Request-20121102, ReqNo.11 - START
			sequences.add(strCurrencyLabel);
			// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
			sequences.add(balance);

			sequences.add("Action");
			sequences.add( "Order Type" );
			

			if( selectedValidity.equalsIgnoreCase(VALIDITY_GTD) 
				|| ( selectedValidity.equalsIgnoreCase(VALIDITY_GTM) && orderPadType == AppConstants.ORDER_PAD_REVISE )	
				){

				// Mary@20130912 - Change_Request-20130424, ReqNo.12 - END
				String dateString	= dateField.getText().toString().trim();
				Date expiryDate 	= FormatUtil.parseDateString(dateString, AppConstants.PARAM_DATE_FORMAT);
				details.put("Validity", selectedValidity + " (" + FormatUtil.formatDateString(expiryDate, "dd/MM/yyyy") + ")");
			}else{
				details.put("Validity", selectedValidity);
			}
			sequences.add("Validity");
			
			// Added by Mary@20121227 - Fixes_Request-20121102, ReqNo.11 - START
			details.put("Settlement Currency", selectedCurrency == null ? "" : selectedCurrency);
			details.put("Payment", selectedPayment == null ? "" : selectedPayment);
			
			sequences.add("Settlement Currency");
			sequences.add("Payment");
			// Added by Mary@20121227 - Fixes_Request-20121102, ReqNo.11 - END

			if(stopLimitField.isEnabled()){
				String stopLimit= stopLimitField.getText().toString().replace(",", "").trim();
				stopLimit		= stopLimit.length()==0? "0":String.valueOf( Float.parseFloat(stopLimit) );
				sequences.add("Trigger Price");
				details.put("Trigger Price", stopLimit);
			}
				
			if(spnTriggerType.isShown() && spnTriggerType.isEnabled()){
				sequences.add("Trigger Type");
				details.put("Trigger Type", spnTriggerType.getSelectedItem().toString());
			}
				
			if(spnTriggerDirection.isShown() && spnTriggerDirection.isEnabled()){
				sequences.add("Trigger Direction");
				details.put("Trigger Direction", spnTriggerDirection.getSelectedItem().toString());
			}
			//}
			//Added by Thinzar@20140816, Change_Request-20140810, ReqNo. 4 & 5 - END
			
			String priceText	= priceField.getText().toString().replace(",", "");
			priceText			= priceText.length()==0? "0":priceText;
			float price			= Float.parseFloat(priceText);

			details.put("Order Price", strStockCurrency + " " + FormatUtil.formatPrice(price, "-", strSelectedStockExchangeCode));
			sequences.add("Order Price");
			
			String text 		= quantityField.getText().toString().replace(",", "");
			text 				= text.length()==0 ? "0" : text;

			long quantity 		= Long.parseLong(text);

			if((AppConstants.brokerConfigBean.getIntTradeQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT) && Integer.parseInt(lotSize) > 1){ //Edited by Thinzar, Change_Request-20150601, ReqNo.1
				details.put(ORDER_QTY_LOT_LABEL, FormatUtil.formatLong(quantity, "-"));
				sequences.add(ORDER_QTY_LOT_LABEL);
			}else{
				details.put(ORDER_QTY_UNIT_LABEL, FormatUtil.formatLong(quantity * lngCurrentLotSize, "-"));
				sequences.add(ORDER_QTY_UNIT_LABEL);
			}
			quantity 			= quantity * lngCurrentLotSize;
			// Mary@20130129 - Fixes_Request-20130110, ReqNo.6 - END
			
			double value			= price * quantity;

			// e.g. Default = "MYR", SelecteSettlementCurrency = "SGD", StockCurrency = "AUD"; # SGD 24.455;
			String strOrderValue	= strStockCurrency + " "  + FormatUtil.formatDouble3(value, "-");
			try{
				if( ! selectedCurrency.equalsIgnoreCase(strStockCurrency) ){
					if(currencyExchgInfo != null){
						value	= AppConstants.convertDefaultCurrencyToOtherCurrency(value, strStockCurrency, AppConstants.getStrBaseCurrencyCode(), strActionType, lstCurrExchgInfo);
						if( ! selectedCurrency.equalsIgnoreCase( AppConstants.getStrBaseCurrencyCode() ) ){
							value	= AppConstants.convertDefaultCurrencyToOtherCurrency(value, strActionType, currencyExchgInfo);
						}
						
						strOrderValue	= selectedCurrency + " " + FormatUtil.formatDouble3(value, "-");
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			details.put( "Order Value", strOrderValue);
			// Mary@20121227 - Fixes_Request-20121102, ReqNo.11 - END
			sequences.add("Order Value");
			
			// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
			String strDisclosedQty 	= disclosedQtyField.getText().toString().replace(",", "");
			strDisclosedQty 		= strDisclosedQty.length()==0 ? "0" : strDisclosedQty;

			long lngDisclosedQty	= Long.parseLong(strDisclosedQty);
			// Added by Mary@20130129 - Fixes_Request-20130110, ReqNo.6 - START
			if((AppConstants.brokerConfigBean.getIntTradeQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT)&& Integer.parseInt(lotSize) > 1){ //Edited by Thinzar, Change_Request-20150601, ReqNo.1
				details.put( DISCLOSED_QTY_LOT_LABEL, FormatUtil.formatLong(lngDisclosedQty, "-") );
				sequences.add(DISCLOSED_QTY_LOT_LABEL);
			}else{
			// Added by Mary@20130129 - Fixes_Request-20130110, ReqNo.6 - END
				details.put( DISCLOSED_QTY_UNIT_LABEL, FormatUtil.formatLong(lngDisclosedQty * lngCurrentLotSize, "-") );
				sequences.add(DISCLOSED_QTY_UNIT_LABEL);
			}
			
			String strMinQty 		= minQtyField.getText().toString().replace(",", "");
			strMinQty 				= strMinQty.length()==0 ? "0" : strMinQty;

			long lngDMinQty			= Long.parseLong(strMinQty);

			// Added by Mary@20130129 - Fixes_Request-20130110, ReqNo.6 - START
			if((AppConstants.brokerConfigBean.getIntTradeQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT)&& Integer.parseInt(lotSize) > 1){ //Edited by Thinzar, Change_Request-20150601, ReqNo.1
				details.put( MIN_QTY_LOT_LABEL, FormatUtil.formatLong(lngDMinQty, "-") );
				sequences.add(MIN_QTY_LOT_LABEL);
			}else{
			// Added by Mary@20130129 - Fixes_Request-20130110, ReqNo.6 - END
				details.put( MIN_QTY_UNIT_LABEL, FormatUtil.formatLong(lngDMinQty * lngCurrentLotSize, "-") );
				sequences.add(MIN_QTY_UNIT_LABEL);
			}
			// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
			
			//Added by Thinzar, Change_Request-20161101, ReqNo.2
			DefinitionConstants.Debug("strSelectedStockExchangeCode=" + strSelectedStockExchangeCode + ", ischecked: " + privateOrderBox.isChecked());
			if(strSelectedStockExchangeCode.equalsIgnoreCase(AppConstants.EXCHANGECODE_PH)
					&& privateOrderBox.isChecked()){
				details.put(PRIVATE_ORDER_LABEL, "YES");
				sequences.add(PRIVATE_ORDER_LABEL);
			}
					
			Intent intent 		= new Intent();
			intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
			intent.putExtra(ParamConstants.ORDER_DETAILS_TAG, details);
			intent.putExtra(ParamConstants.ORDER_DISPLAYSEQ_TAG, sequences);
			intent.putExtra(ParamConstants.ORDER_TYPE_TAG, actionType);
			intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
	
			intent.setClass(TradeActivity.this, OrderDetailActivity.class);
			this.startActivityForResult(intent, AppConstants.ORDER_DETAILS_REQUEST);
			
		} else {
			
			this.submitTradeOrder();			

		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			if (requestCode == AppConstants.ORDER_DETAILS_REQUEST) {
				if (resultCode == Activity.RESULT_OK) {
					this.submitTradeOrder();
				}
			} else if (requestCode == AppConstants.SELECT_ACCOUNT_REQUEST) {
				if (resultCode == Activity.RESULT_OK) {
					// Added by Mary@20120928 - Fixes_Request-20120816, ReqNo.13
					boolean isDialogOwner	= false;
					
					// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
					if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) ){
						// Added by Mary@20120810 - show loading animation - START
						if( ! this.dialog.isShowing() ){
							this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
							
							// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
							if( ! this.isFinishing() ){
								this.dialog.show();
								// Added by Mary@20120928 - Fixes_Request-20120816, ReqNo.13
								isDialogOwner	= true;
							}
						}
						// Added by Mary@20120810 - show loading animation - END
					}
					
					Bundle bundle 				= data.getExtras();
					String accountNo 			= bundle.getString(ParamConstants.ACCOUNTNO_TAG);
					String branchCode	 		= bundle.getString(ParamConstants.BRANCHCODE_TAG);
	
					StockApplication application= (StockApplication) TradeActivity.this.getApplication();
					/* Mary@20130220 - Fixes_Request-20130108, ReqNo.10
					List accounts 				= application.getTradeAccounts( exchangeInfo.getTrdExchgCode() );
					*/
					List<TradeAccount> accounts	= application.getTradeAccounts();
					for (Iterator<TradeAccount> itr=accounts.iterator(); itr.hasNext(); ) {
						TradeAccount account	= itr.next();
						if (account.getAccountNo().equals(accountNo) && account.getBranchCode().equals(branchCode)) {
							tradeAccount 								= account; 
							application.setSelectedAccount(account);
							AppConstants.selectedAcc 					= account;
							
							String strQCExchgCode	= null;
							// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
							String strTrdExchgCode	= null;
							for( Iterator<ExchangeInfo> itr2=application.getExchangeInfos().iterator(); itr2.hasNext(); ){
								ExchangeInfo exchgInfo = itr2.next();
								if( exchgInfo.getTrdExchgCode() != null && account.getExchangeCode().equals( exchgInfo.getTrdExchgCode() ) ){
									strQCExchgCode	= exchgInfo.getQCExchangeCode();
									// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
									strTrdExchgCode	= exchgInfo.getTrdExchgCode();
									break;
								}
							}
							
							// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
							Map<String, String> mapBrokerUserPreference	= new HashMap<String, String>();
							mapBrokerUserPreference.put( PreferenceConstants.EXCHANGE_CODE,  strQCExchgCode );
							mapBrokerUserPreference.put( PreferenceConstants.ACCOUNT_NO, account.getAccountNo() );
							mapBrokerUserPreference.put( PreferenceConstants.BRANCH_CODE, account.getBranchCode() );

							// Added by Mary@20121112 - Change_Request-20120719, ReqNo.12
							mapBrokerUserPreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, strTrdExchgCode);
							mapBrokerUserPreference.put(PreferenceConstants.TRADE_EXCHANGE_CODE, strTrdExchgCode);
							AppConstants.cacheBrokerUserDetails(TradeActivity.this, sharedPreferences, mapBrokerUserPreference);
							mapBrokerUserPreference.clear();
							mapBrokerUserPreference						= null;
							// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
							
							// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12 - START
							strQCExchgCode 		= null;
							strTrdExchgCode 	= null;
							// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12 - END
							break;
						}
					}
					
					// Added by Mary@20120810 - show loading animation - START
					if( this.dialog != null && this.dialog.isShowing() && isDialogOwner){
						isDialogOwner	= false;
						// Mary@20120928 - Fixes_Request-20120816, ReqNo.13 - END
						this.dialog.dismiss();
					}
					
				}
			// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3 - START
			}else if(requestCode == AppConstants.RDS_REQUEST){
				if(resultCode == Activity.RESULT_OK){
					AppConstants.getRDSInfo().setIsRDSEnable(false);
					
					StockApplication application= (StockApplication) TradeActivity.this.getApplication();		

					if( ( AppConstants.info2FA.is2FARequired() && !AppConstants.info2FA.hasSuccessVerify2FA() ) && !AppConstants.info2FA.isForceUse1FA() ){
						showTradePin2FADialog();
					// Added by Mary@20131008 - Change_Request-20130711, ReqNo.14 - START
					}else if( AppConstants.info2FA.isForceUse1FA() || ( !AppConstants.info2FA.is2FARequired() && AppConstants.info2FA.is1FAAllowed() ) ){
						showTradePin1FADialog();
					// Added by Mary@20131008 - Change_Request-20130711, ReqNo.14 - END
					}else if( AppConstants.brokerConfigBean.isEnablePinFeatures() && !application.isRememberPin() ){
						TradeActivity.this.showTradePinDialog();
					}else{
						tradingPin = application.getTradingPin();
						TradeActivity.this.handleTradeAction();					
					}
				}
			// Added by Mary@20130115 - Change_Request-20130104, ReqNo.3 - END
			}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	private void constructOrderTypeTableView(List<String> lstOrderType, boolean canReviseOrderType){
		String[] orderTypes 			= (String[]) lstOrderType.toArray( new String[lstOrderType.size()] );
		String[] orderType_2 			= {strReviseOrderType};
		String[] default_order_Types	= {"Limit"};
		
		if(orderPadType == AppConstants.ORDER_PAD_REVISE){
			if(canReviseOrderType){
				if( exchangeInfo.getReviseOrderTypes().contains(strReviseOrderType) )
					orderTypes	= (String[]) lstOrderType.toArray( new String[exchangeInfo.getReviseOrderTypes().size()] );
				else
					orderTypes	= orderType_2;
			}else{
				orderTypes = orderType_2;
			}
		}else{
			if(orderTypes==null || ( orderTypes != null && orderTypes.length == 0 ) )
				orderTypes = default_order_Types;
		}
		
		final String[] strArrOrderType	= orderTypes;

		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1-START
		spinnerOrdTyp 					= (Spinner)findViewById(R.id.spinnerOrdType);
		ArrayAdapter<String> dataAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, strArrOrderType);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerOrdTyp.setAdapter(dataAdapter);
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - END
		
		for(int i=0; i<strArrOrderType.length; i++){

			if( ( selectedOrderType == null && i==selectedRowOrderType ) || (selectedOrderType != null && strArrOrderType[i].equalsIgnoreCase(selectedOrderType) ) ){

					spinnerOrdTyp.setSelection(i);
					selectedOrderType	= strArrOrderType[i];
					selectedRowOrderType= i;
					break;
			}
		}
			
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - START
		spinnerOrdTyp.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {			
				selectedOrderType 			= strArrOrderType[(int)spinnerOrdTyp.getSelectedItemId()];
				selectedRowOrderType 		= (int)spinnerOrdTyp.getSelectedItemId();
				
				toggleRelatedOrderTypeView();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});

		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		if(orderPadType == AppConstants.ORDER_PAD_REVISE)
			spinnerOrdTyp.setEnabled(canReviseOrderType);
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
	}

	private void constructValidityTableView(List<String> lstValidity, boolean canReviseValidity){
		String[] validities				= (String[]) lstValidity.toArray( new String[lstValidity.size()] );
		String[] validities_2 			= {strReviseValidity};
		String[] default_validity 		= {"Day","GTD"};
		if(orderPadType == AppConstants.ORDER_PAD_REVISE){
			if(canReviseValidity){
				if( exchangeInfo.getReviseValidities().contains(strReviseValidity) )
					validities = (String[]) exchangeInfo.getReviseValidities().toArray( new String[exchangeInfo.getReviseValidities().size()] );
				else
					validities = validities_2;
			}else{
				validities = validities_2;
			}
		}else{
			if(validities==null || ( validities != null && validities.length == 0 ) )
				validities = default_validity;
		}
		
		final String[] strArrValidity	= validities;

		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - START
		spinnerValidity 				= (Spinner)findViewById(R.id.spinnerValidity);
		ArrayAdapter<String> dataAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, strArrValidity);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerValidity.setAdapter(dataAdapter);
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - END

		for(int i=0; i<strArrValidity.length; i++){

			if( (selectedValidity == null && i==selectedRowValidity)  || (selectedValidity != null && strArrValidity[i].equalsIgnoreCase(selectedValidity) ) ){

				spinnerValidity.setSelection(i);
				selectedValidity 	= strArrValidity[i];
				selectedRowValidity = i; 
				break;
			}
				
		}
		
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - START
		spinnerValidity.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				selectedValidity 			= strArrValidity[(int)spinnerValidity.getSelectedItemId()];
				selectedRowValidity 		= (int)spinnerValidity.getSelectedItemId();
				
				toggleRelatedValidityView();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - END

		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		if(orderPadType == AppConstants.ORDER_PAD_REVISE)
			spinnerValidity.setEnabled(canReviseValidity);
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
	}

	private void constructCurrencyTableView(@NonNull List<String> lstCurrency, boolean canReviseCurrency){
		DefinitionConstants.Debug("---constructCurrencyTableView: " + Arrays.toString(lstCurrency.toArray()));
		
		if(! lstCurrency.contains( AppConstants.getStrBaseCurrencyCode() ) && !isCUTAccount)	//Edited by Thinzar, Change_Request-20150401, ReqNo.1
			lstCurrency.add( 0, AppConstants.getStrBaseCurrencyCode() );

		String[] currencies				= (String[]) lstCurrency.toArray(new String[lstCurrency.size()]);
		String[] reviseCurrency			= {strReviseCurrency};
		if(orderPadType==AppConstants.ORDER_PAD_REVISE){		

			if(reviseCurrency != null){
				List<String> lstTemp	= new ArrayList<String>();
				for(int i=0; i<reviseCurrency.length; i++){
					if(reviseCurrency[i] != null && reviseCurrency[i].trim().length() > 0)
						lstTemp.add(reviseCurrency[i]);
				}
				
				currencies				= (String[]) lstTemp.toArray(new String[lstTemp.size()]);
			}
		}else{
			List<String> listClone	= new ArrayList<String>();
			int i					= 0;
			
			for(Iterator<String> itr=lstCurrency.iterator(); itr.hasNext();){

				String strTempCurr	= itr.next();
				// Added by Mary@20130620 - Fixes_Request-20130523, ReqNo.13
				if(strTempCurr != null && strTempCurr.trim().length() > 0){
					if( strTempCurr.equals("+") ){
						if( ! lstCurrency.contains(strStockCurrency) )
							listClone.add(i++, strStockCurrency);
					}else{
						listClone.add(i++,  strTempCurr);
					}
				}
			}
			// Mary@20130305 - Fixes_Request-20130227, ReqNo.2 - END
			
			//Added by Thinzar, Change_Request-20150401, ReqNo. 5 - START [remove currencyNotSupported]
			if(exchangeInfo.getCurrencyNotSupported() != null){
				
				String currencyNotSupported	= exchangeInfo.getCurrencyNotSupported();
				
				for(Iterator<String> itr2 = listClone.iterator();itr2.hasNext();){
					 String element = itr2.next();
			         if(element.equals(currencyNotSupported))
			         {
			        	 itr2.remove();
			         }
				 }
			}
			//Added by Thinzar, Change_Request-20150401, ReqNo. 5 - END
			
			currencies				= (String[])listClone.toArray( new String[listClone.size()] );
		}

		final String[] strArrCurrency	= currencies;

		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1-START
		spinnerCurrency 				= (Spinner)findViewById(R.id.spinnerCurrency);
		ArrayAdapter<String> dataAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, strArrCurrency);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerCurrency.setAdapter(dataAdapter);
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - END

		for(int i=0; i<strArrCurrency.length; i++){

			if( ( ( selectedCurrency == null  || (selectedCurrency != null && selectedCurrency.trim().length() == 0) ) && i==selectedRowCurrency) 
					|| (selectedCurrency != null && strArrCurrency[i].equalsIgnoreCase(selectedCurrency) ) 
			){

				spinnerCurrency.setSelection(i);
				selectedCurrency	= strArrCurrency[i];
				selectedRowCurrency =i;
				break;
			}
		}
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - START
		spinnerCurrency.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				selectedCurrency 			= strArrCurrency[(int)spinnerCurrency.getSelectedItemId()];
				selectedRowCurrency 		= (int)spinnerCurrency.getSelectedItemId();
				
				//Added by Thinzar, Change_Request-20150401, ReqNo. 6
				new getClientLimit().execute();
				
				//==comment out at the moment==constructPaymentTableView(exchangeInfo.getPaymentMethods(), selectedCurrency, isEditablePayment);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});

		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		if(orderPadType == AppConstants.ORDER_PAD_REVISE)
			spinnerCurrency.setEnabled(canReviseCurrency);
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
		
		// Added by Mary@20130627 - Fixes_Request-20130523, ReqNo.22
		spinnerCurrency.setEnabled( strArrCurrency.length > 0 );
	}

	private void constructPaymentTableView(@NonNull List<String> lstPayment, String strSelectedCurrency, boolean canRevisePayment){

		// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4 - START
		List<StockCurrencyPaymentMethod> lstTempStockCurrencyPaymentMethod	= new ArrayList<StockCurrencyPaymentMethod>();
		for(Iterator<StockCurrencyPaymentMethod> itr=exchangeInfo.getLstStockCurrencyPaymentMethods().iterator(); itr.hasNext(); ){
			lstTempStockCurrencyPaymentMethod.add( itr.next() );
		}

		if( ! isPayableWithCPF ){
			for(Iterator<String> itr=AppConstants.brokerConfigBean.getLstPymtTypeToBeFilter().iterator(); itr.hasNext();){
				String strToBeFilterOut	= itr.next();
				// Added by Mary@20130620 - Fixes_Request-20130523, ReqNo.13
				if( strToBeFilterOut != null && strToBeFilterOut.trim().length() > 0){
					lstPayment.remove(strToBeFilterOut);
					
					for(int i=0; i<lstTempStockCurrencyPaymentMethod.size(); i++){
						StockCurrencyPaymentMethod scpm	= lstTempStockCurrencyPaymentMethod.get(i);

						if( scpm != null && scpm.getStrPaymentMethod() != null && scpm.getStrPaymentMethod().equalsIgnoreCase(strToBeFilterOut) ){
							lstTempStockCurrencyPaymentMethod.remove(i);
							break;
						}
					}
				}
			}
		}
		// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4 - END
		
		String[] payments												= (String[]) lstPayment.toArray(new String[lstPayment.size()]);
		String[] RevisePayment 											= {strRevisePaymentType};
		
		if(orderPadType == AppConstants.ORDER_PAD_REVISE){

			if(RevisePayment != null){
				List<String> lstTemp	= new ArrayList<String>();
				for(int i=0; i<RevisePayment.length; i++){
					if(RevisePayment[i] != null && RevisePayment[i].trim().length() > 0)
						lstTemp.add(RevisePayment[i]);
				}
				payments	= (String[]) lstTemp.toArray(new String[lstTemp.size()]);
			}
		}else{
			if(strSelectedCurrency != null && strSelectedCurrency.trim().length() > 0
					&& exchangeInfo.isIncludeStockCurrency() 
					&& ! exchangeInfo.getCurrencies().contains(strSelectedCurrency)
					&& lstTempStockCurrencyPaymentMethod.size() > 0
			){

				int intSize	= lstPayment.size() + lstTempStockCurrencyPaymentMethod.size();
				payments	= new String[intSize];
				int i		= 0;
				for(Iterator<String> itr=lstPayment.iterator(); itr.hasNext();){
					/* Mary@20130620 - Fixes_Request-20130523, ReqNo.13 - START
					payments[i++] 	= itr.next();
					*/
					String strTemp	= itr.next();
					if(strTemp != null && strTemp.trim().length() > 0)
						payments[i++] 	= strTemp;
					// Mary@20130620 - Fixes_Request-20130523, ReqNo.13 - END
				}
				/*  Mary@20130108 - Change_Request-20130104, ReqNo.4
				for(Iterator<StockCurrencyPaymentMethod> itr=exchangeInfo.getLstStockCurrencyPaymentMethods().iterator(); itr.hasNext();){
				*/
				for(Iterator<StockCurrencyPaymentMethod> itr=lstTempStockCurrencyPaymentMethod.iterator(); itr.hasNext();){
					StockCurrencyPaymentMethod scpm	= itr.next();
					// Added by Mary@20130620 - Fixes_Request-20130523, ReqNo.13
					if(scpm != null && scpm.getStrPaymentMethod() != null && scpm.getStrPaymentMethod().trim().length() > 0)
						payments[i++]					= scpm.getStrPaymentMethod();
				}
			}
		}
		
		
		final String[] strArrPayment									= payments;
//		DefinitionConstants.Debug("--constructPaymentTableView: final strArrPayment =" + Arrays.toString(strArrPayment));
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - START
		spinnerPayment 													= (Spinner)findViewById(R.id.spinnerPayment);
		ArrayAdapter<String> dataAdapter 								= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, strArrPayment);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerPayment.setAdapter(dataAdapter);
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - END

//		DefinitionConstants.Debug("debug22==selectedPayment:" + selectedPayment + ", selectedRowPaymentType=" + selectedRowPaymentType);
		for(int i=0; i<strArrPayment.length; i++){

			if( ( ( selectedPayment == null || (selectedPayment != null && selectedPayment.trim().length() == 0 ) ) && i==selectedRowPaymentType) 
					|| (selectedPayment != null && strArrPayment[i].equalsIgnoreCase(selectedPayment) ) 
			){

				spinnerPayment.setSelection(i);
				selectedPayment			= strArrPayment[i];
				selectedRowPaymentType	= i;
				break;
			}
		}
		// Added by Kw@20121128 - Change_Request-20121106, ReqNo.1 - START
		spinnerPayment.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				previousSelectedPayment 	= selectedPayment;	//Added for Change_Request-20140701, ReqNo. 9
				selectedPayment 			= strArrPayment[(int)spinnerPayment.getSelectedItemId()];
				selectedRowPaymentType 		= (int)spinnerPayment.getSelectedItemId();
				
				//Added by Thinzar@20140806, Change_Request-20140701, ReqNo. 9 - START
				if(previousSelectedPayment.equalsIgnoreCase("CUT") || selectedPayment.equalsIgnoreCase("CUT"))
					new getClientLimit().execute();
				//Added by Thinzar@20140806 - END
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});

		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		if(orderPadType == AppConstants.ORDER_PAD_REVISE)
			spinnerPayment.setEnabled(canRevisePayment);
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
		
		// Added by Mary@20130626 - Fixes_Request-20130523, ReqNo.22
		spinnerPayment.setEnabled( strArrPayment.length > 0 );
	}
	
	
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {

		Message message	= handler.obtainMessage();
		message.obj 	= changedSymbols;
		handler.sendMessage(message);
	}

	final Handler handler = new Handler() {
		public void handleMessage(Message message) {
			List<StockSymbol> changedSymbols	= (List<StockSymbol>) message.obj;
			TradeActivity.this.refreshStockData(changedSymbols);
		}
	};


	private void refreshStockData(List<StockSymbol> changedSymbols) {
		try {
			//Added by Kw@20130502 - Fix_Request-20130426, ReqNo.3 
			StockSymbol symbol = loadMarketDepth((StockSymbol) changedSymbols.get(0));

			// Mary@20120717 - avoid nullpointerexception
			if ( symbol != null && stockSymbol != null && symbol.getSymbolCode().equals( stockSymbol.getSymbolCode() ) ){
				
				if( symbol.checkDiscrepancy(stockSymbol) ){
					
					if( symbol.getLastDone() != stockSymbol.getLastDone() || symbol.getTotalTrade() != stockSymbol.getTotalTrade() ){
						float gap = symbol.getLastDone() - stockSymbol.getLastDone();
						stockSymbol.setLastDone( symbol.getLastDone() );

						priceLabel.setText( FormatUtil.formatPrice( stockSymbol.getLastDone(), "-", strSelectedStockExchangeCode));
						priceChangeLabel.setText( FormatUtil.formatPriceChangeByExchange(stockSymbol.getPriceChange(), strSelectedStockExchangeCode));
						percentChangeLabel.setText( FormatUtil.formatPricePercent( stockSymbol.getChangePercent() ) );
						
						if(gap > 0){
							priceLabel.setBackgroundColor( getResources().getColor(R.color.UpStatus_BgColor) );
							priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_FgColor) );

						}else if( gap < 0){

							priceLabel.setBackgroundColor( getResources().getColor(R.color.DownStatus_BgColor) );
							priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_FgColor) );

						}else if( gap == 0 && symbol.getVolume() != stockSymbol.getVolume() ){
							priceLabel.setBackgroundColor( getResources().getColor(R.color.DiffTtlStatus_BgColor) );
							priceLabel.setTextColor( getResources().getColor(R.color.DiffTtlStatus_FgColor) );
						}
						priceLabel.postDelayed(new Runnable() {
							public void run() {
								priceLabel.setBackgroundColor(Color.TRANSPARENT);

								int intStatusColor	= getResources().getColor(R.color.NoStatus_FgColor);
								if (stockSymbol.getPriceChange() > 0) {
									intStatusColor	= getResources().getColor(R.color.UpStatus_BgColor);
								} else if (stockSymbol.getPriceChange() < 0) {
									intStatusColor	= getResources().getColor(R.color.DownStatus_BgColor);
								}
								priceLabel.setTextColor(intStatusColor);
								priceChangeLabel.setTextColor(intStatusColor);
								percentChangeLabel.setTextColor(intStatusColor);
								// Mary@20120905 - standarized status color for 'UP', 'DOWN' and 'NO' status - END
							}
						}, 1500);

						if(stockSymbol.getPriceChange() > 0)
							arrowImage.setImageResource(LayoutSettings.upArrow);
						else if (stockSymbol.getPriceChange() < 0)
							arrowImage.setImageResource(LayoutSettings.downArrow);
						else
							arrowImage.setImageDrawable(null);

					}

					new MarketDepthTaskNew().execute();
					// Mary@20130624 - Fixes_Request-20130523, ReqNo.17 - END
				}  
			}
		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if( response.get(2).equals("1102") ){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if( TradeActivity.this.mainMenu.isShowing() )
					TradeActivity.this.mainMenu.hide();

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(refreshThread != null)
					refreshThread.stopRequest();
					
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				if( !isFinishing() )
					TradeActivity.this.doubleLoginMessage( (String)response.get(1) );
			}
		}
	};
	
	// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14 - START
	@Override
	public void OneFAValidationEvent2(boolean isValid, String message) {
		StockApplication application	= (StockApplication) TradeActivity.this.getApplication();
		
		if( AppConstants.brokerConfigBean.isEnablePinFeatures() && !application.isRememberPin() ){
			TradeActivity.this.showTradePinDialog();
		}else{
			tradingPin = application.getTradingPin();
			TradeActivity.this.handleTradeAction();					
		}
	}
	// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14 - END
	
	//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 START
	@Override
	public void OtpValidationEvent2(boolean isValid, String message) {

		StockApplication application= (StockApplication) TradeActivity.this.getApplication();

		AppConstants.info2FA.setHasSuccessVerify2FA(true);
		if( AppConstants.brokerConfigBean.isEnablePinFeatures() && !application.isRememberPin() ){
			TradeActivity.this.showTradePinDialog();
		}else{
			tradingPin = application.getTradingPin();
			TradeActivity.this.handleTradeAction();
		}
	}
	//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 END
	
	//Added by Thinzar@20140827, Fixes_Request-20140820, ReqNo.7 - START
	//to hide the fields which are never used by a particular exchange
	private void prepareOrderPadFieldsVisibility(){
		int isShowStoplimitLayout		= View.GONE;
		int isShowDiscloseQtyLayout		= View.GONE;
		int isShowMinQtyLayout			= View.GONE;
		int isShowTriggerTypeLayout		= View.GONE;
		int isShowTriggerDirectionLayout = View.GONE;
		
		List<OrderControlInfo> lstOrderCtrl	= exchangeInfo.getOrderCtrltoList();
		for(Iterator<OrderControlInfo> itr=lstOrderCtrl.iterator(); itr.hasNext();){
			OrderControlInfo oci	= itr.next();
			
			if(oci.isEnableStopLimitPriceField())	isShowStoplimitLayout	= View.VISIBLE;
			if(oci.isEnableDisclosedQtyField())		isShowDiscloseQtyLayout	= View.VISIBLE;
			if(oci.isEnableMinQtyField())			isShowMinQtyLayout		= View.VISIBLE;
			if(oci.isEnableTriggerType())			isShowTriggerTypeLayout	= View.VISIBLE;
			if(oci.isEnableTriggerDirection())		isShowTriggerDirectionLayout	= View.VISIBLE;
		}
		
		if(triggerPriceTypes.size()>0)			prepareSpnTriggerType();
		if(triggerPriceDirections.size() > 0)	prepareSpnTriggerDirection();
		
		stopLimitLayout.setVisibility(isShowStoplimitLayout);
		discloseQtyLayout.setVisibility(isShowDiscloseQtyLayout);
		minQtyLayout.setVisibility(isShowMinQtyLayout);
		triggerTypeLayout.setVisibility(isShowTriggerTypeLayout);
		triggerDirectionLayout.setVisibility(isShowTriggerDirectionLayout);
	}
	//Added by Thinzar@20140827, Fixes_Request-20140820, ReqNo.7 - END
	
	
	//Added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 & 5 - START
	private void prepareSpnTriggerType(){

		ArrayAdapter<String> typesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, triggerPriceTypes.toArray(new String[triggerPriceTypes.size()]));
		typesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnTriggerType.setAdapter(typesAdapter);
			
		spnTriggerType.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				selectedTriggerType = spnTriggerType.getSelectedItem().toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
					
			}
		});
		
		spnTriggerType.setEnabled(true);
	}
	
	private void prepareSpnTriggerDirection(){
		ArrayAdapter<String> directionsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, triggerPriceDirections.toArray(new String[triggerPriceDirections.size()]));
		directionsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnTriggerDirection.setAdapter(directionsAdapter);
			
		spnTriggerDirection.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				selectedTriggerDirection = spnTriggerDirection.getSelectedItem().toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
				
		});
			
		triggerDirectionLayout.setVisibility(View.VISIBLE);
		spnTriggerDirection.setEnabled(true);
		
	}
	//Added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 & 5 - END
	
	//Added by Thinzar@20140902 - Fixes_Request-20140820, ReqNo.13 - START
	private void prepareLabelIfDerivative(){
		ORDERPAD_QTY_LABEL			= getResources().getString(R.string.trade_label_qty_der);		//"CONTRACT";
		ORDER_QTY_UNIT_LABEL		= getResources().getString(R.string.trade_label_order_qty_der);	//"Contract";
		ORDER_QTY_LOT_LABEL			= getResources().getString(R.string.trade_label_order_qty_der);	//"Contract";
		DISCLOSED_QTY_UNIT_LABEL	= getResources().getString(R.string.trade_label_dis_qty_der);	//"Disclosed Qty";
		DISCLOSED_QTY_LOT_LABEL		= getResources().getString(R.string.trade_label_dis_qty_der);	//"Disclosed Qty";
		MIN_QTY_UNIT_LABEL			= getResources().getString(R.string.trade_label_min_qty_der);	//"Min Qty";
		MIN_QTY_LOT_LABEL			= getResources().getString(R.string.trade_label_min_qty_der);	//"Min Qty";
	}
	//Added by Thinzar@20140902 - Fixes_Request-20140820, ReqNo.13 - END
	
	//Added by Thinzar@20141002, Change_Request-20140901,ReqNo.5 - START
	/* Based on the MarketDepthTask of StockActivity.java written by Mary, minor modifications for different parameter naming only */
	private class MarketDepthTaskNew extends AsyncTask<View, Void, Boolean> {
		
		private Exception exception 						= null;
		private AsyncTask<View, Void, Boolean> updateTask 	= null;
		private List<MarketDepth> lstOldMarketDepth			= new ArrayList<MarketDepth>();
		private boolean isDialogOwner						= false;

		@Override
		protected void onPreExecute() {
			updateTask = this;
			if(lstNewMarketDepth != null){
				for(Iterator<MarketDepth> itr = lstNewMarketDepth.iterator(); itr.hasNext();){
					lstOldMarketDepth.add( itr.next() );
				}
			}
			
			// Added by Mary@20130926 - Change_Request-20130724, ReqNo.18 - START
			if( TradeActivity.this.dialog != null && TradeActivity.this.dialog.isShowing() )
				isDialogOwner	= true;
		}

		@Override
		protected Boolean doInBackground(View... view) {
			String[] symbolCodes = { symbolCode };
			String response = null;
			try {
			
				 if(AppConstants.brokerConfigBean.isMultilevelMarketDepth()){
					 response	= QcHttpConnectUtil.imageQuote2(symbolCode,AppConstants.getMarketDepthLvl(strQCExchgCode));//AppConstants.getMarketDepthLvl("SG"));//
					 while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
						intRetryCtr++;
						response	= QcHttpConnectUtil.imageQuote2(symbolCode, AppConstants.getMarketDepthLvl(strQCExchgCode));//AppConstants.getMarketDepthLvl("SG"));//splitTrdExchgCode));
			
					}
					intRetryCtr	= 0;
					
					if (response == null)
						throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
					lstNewMarketDepth= QcMessageParser.parseMarketDepth4(response);
				 }else{
					response	= QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
					while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
						intRetryCtr++;
						response	= QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
					}
					intRetryCtr	= 0;
					
					if (response == null)
						throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
					
					lstNewMarketDepth= QcMessageParser.parseMarketDepth3(response,AppConstants.getMarketDepthLvl(strQCExchgCode));
				 }
			} catch (Exception e) {
				exception = e;
				return false;
			}

			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean isSuccess) {
			super.onPostExecute(isSuccess);

			try{
				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					alertException.setButton(getResources().getString(R.string.dialog_button_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
				
					if( ! TradeActivity.this.isFinishing() )
						alertException.show();
					exception = null;
					return;
				}
				
				//stockInfo.setMarketDepths(lstNewMarketDepth);	//i think no need this, commenting out
				// Added by Mary@20130926 - Change_Request-20130724, ReqNo.18 - START
				if( TradeActivity.this.dialog != null && TradeActivity.this.dialog.isShowing() && isDialogOwner){
					TradeActivity.this.repaintOrderPadScreen();
					TradeActivity.this.dialog.dismiss();
					isDialogOwner = false;
				}
				// Added by Mary@20130926 - Change_Request-20130724, ReqNo.18 - END
				
				TradeActivity.this.prepareMarketDepthView2(lstOldMarketDepth, lstNewMarketDepth);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void prepareMarketDepthView2(final List<MarketDepth> lstOldMarketDepths, final List<MarketDepth> lstNewMarketDepths) {
		String strSymbolExchg			= null;
		TableLayout tableLayoutBidAsk	= null;
		TableLayout tableLayoutSummary	= null;

		int id 							= 0;
		long totalBid 					= 0;
		long totalAsk 					= 0;
		double bidSum 					= 0;
		double askSum 					= 0;

		long intLotSize 					= 0;
		
		try{			
			
			//strSymbolExchg 		= stockInfo.getSymbolCode().substring( (stockInfo.getSymbolCode().lastIndexOf(".") + 1), stockInfo.getSymbolCode().length() );
			strSymbolExchg			= strQCExchgCode;
			if(lstOldMarketDepths.size() == 0){
				tableLayoutBidAsk	= (TableLayout)findViewById(R.id.bidAskTable1);
				tableLayoutBidAsk.removeAllViews();
				
				bidSizeLabels 		= new ArrayList<TextView>();
				bidPriceLabels		= new ArrayList<TextView>();
				askPriceLabels		= new ArrayList<TextView>();
				askSizeLabels		= new ArrayList<TextView>();
				counterLabels		= new ArrayList<TextView>();
			}
		
			//intLotSize			= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? stockInfo.getSharePerLot() : 1;
			intLotSize				= lngViewCurrentLotSize;
			
			for (Iterator<MarketDepth> itr = lstNewMarketDepths.iterator(); itr.hasNext();) {
				final MarketDepth marketDepthNew= itr.next();
				final MarketDepth marketDepthOld= ( lstOldMarketDepths.size() > id ) ? lstOldMarketDepths.get(id) : null;
				
				View rowView		= null;
				TableRow tableRow 	= null; 
				if(marketDepthOld == null){
					tableRow 				= new TableRow(this);
					tableRow.setId(id);
					tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
					tableRow.setWeightSum(1);
					
					//rowView					= this.getLayoutInflater().inflate( (width > 480 & height > 800) ? R.layout.bid_ask_row2 : R.layout.bid_ask_row, null);
					rowView					= this.getLayoutInflater().inflate(R.layout.bid_ask_row, null);	//added, Change_Request-20160101, ReqNo.3
					LinearLayout rowLayout 	= (LinearLayout) rowView.findViewById(R.id.bidAskLayout);	
					//rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);	//commented, Change_Request-20160101, ReqNo.3
				}
				
				/** COUNTER - START **/
				final TextView tvCountLabel		= marketDepthOld != null ? counterLabels.get(id) : (TextView) rowView.findViewById(R.id.countLabel);
				tvCountLabel.setText( String.valueOf(id + 1) );
				if(marketDepthOld == null)
					counterLabels.add(tvCountLabel);

				/** COUNTER - END **/
				
				/** BID SIZE - START **/
				final TextView tvBidSizeLabel 	=  marketDepthOld != null ? bidSizeLabels.get(id) : (TextView) rowView.findViewById(R.id.bidSizeLabel);
				tvBidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepthNew.getBidSize() / intLotSize) ) );
				if(marketDepthOld != null){
					tvBidSizeLabel.setBackgroundColor( AppConstants.getIntBidAskQtyBlinkBackgroundColor(this, marketDepthOld.getBidSize(), marketDepthNew.getBidSize() ) );
					tvBidSizeLabel.setTextColor( AppConstants.getIntBidAskQtyBlinkTextColor(this, marketDepthOld.getBidSize(), marketDepthNew.getBidSize() ) );

					tvBidSizeLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvBidSizeLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
							tvBidSizeLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
						}
					}, 1500);
				}else{
					bidSizeLabels.add(tvBidSizeLabel);
				}
				/** BID SIZE - END **/
				
				/** BID PRICE - START **/
				final TextView tvBidPriceLabel 	=  marketDepthOld != null ? bidPriceLabels.get(id) : (TextView) rowView.findViewById(R.id.bidPriceLabel);
				tvBidPriceLabel.setText( AppConstants.getStrFormattedBidAskPrice(marketDepthNew.getBidPrice(), strQCExchgCode) );
	
				if(marketDepthOld != null){
					tvBidPriceLabel.setBackgroundColor( AppConstants.getIntBidAskPriceBlinkBackgroundColor( this, marketDepthOld.getBidPrice(), marketDepthNew.getBidPrice() ) );
					tvBidPriceLabel.setTextColor( AppConstants.getIntBidAskPriceBlinkTextColor( this, marketDepthOld.getBidPrice(), marketDepthNew.getBidPrice(), tvBidPriceLabel.getCurrentTextColor() ) );

					tvBidPriceLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							try{
								tvBidPriceLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
								//tvBidPriceLabel.setTextColor( AppConstants.getIntBidAskPriceTextColor( TradeActivity.this, marketDepthNew.getBidPrice(), stockInfo.getLACP() ) );
								tvBidPriceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}, 1500);
				}else{
					//tvBidPriceLabel.setTextColor( AppConstants.getIntBidAskPriceTextColor( this, marketDepthNew.getBidPrice(), stockInfo.getLACP() ) );
					tvBidPriceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
				}
				
				if(marketDepthOld == null)
					bidPriceLabels.add(tvBidPriceLabel);
				/** BID PRICE - END **/
				
				/** ASK SIZE - START **/
				final TextView tvAskSizeLabel		= marketDepthOld != null ? askSizeLabels.get(id) : (TextView) rowView.findViewById(R.id.askSizeLabel);
				tvAskSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepthNew.getAskSize() / intLotSize) ) );
				
				if(marketDepthOld != null){
					tvAskSizeLabel.setBackgroundColor( AppConstants.getIntBidAskQtyBlinkBackgroundColor( this, marketDepthOld.getAskSize(), marketDepthNew.getAskSize() ) );
					tvAskSizeLabel.setTextColor( AppConstants.getIntBidAskQtyBlinkTextColor( this, marketDepthOld.getAskSize(), marketDepthNew.getAskSize() ) );
					
					tvAskSizeLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvAskSizeLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
							tvAskSizeLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
						}
					}, 1500);
				}else{
					askSizeLabels.add(tvAskSizeLabel);
				}
				/** ASK SIZE - END **/
				
				/** ASK PRICE - START **/
				final TextView tvAskPriceLabel	= marketDepthOld != null ? askPriceLabels.get(id) : (TextView) rowView.findViewById(R.id.askPriceLabel);
				tvAskPriceLabel.setText( AppConstants.getStrFormattedBidAskPrice(marketDepthNew.getAskPrice(), strSymbolExchg) );
				
				if(marketDepthOld != null){
					tvAskPriceLabel.setBackgroundColor( AppConstants.getIntBidAskPriceBlinkBackgroundColor( this, marketDepthOld.getAskPrice(), marketDepthNew.getAskPrice() ) );
					tvAskPriceLabel.setTextColor( AppConstants.getIntBidAskPriceBlinkTextColor( this, marketDepthOld.getAskPrice(), marketDepthNew.getAskPrice(), tvAskPriceLabel.getCurrentTextColor() ) );
					
					tvAskPriceLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							try{
								tvAskPriceLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
								//tvAskPriceLabel.setTextColor( AppConstants.getIntBidAskPriceTextColor( StockActivity.this, marketDepthNew.getAskPrice(), stockInfo.getLACP() ) );
								tvAskPriceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}, 1500);
				}else{
					//tvAskPriceLabel.setTextColor( AppConstants.getIntBidAskPriceTextColor( this, marketDepthNew.getAskPrice(), stockInfo.getLACP() ) );
					tvAskPriceLabel.setTextColor(getResources().getColor(R.color.NoStatus_FgColor));
				}
				
				if(marketDepthOld == null)
					askPriceLabels.add(tvAskPriceLabel);
				/** ASK PRICE - END **/
				
				if(marketDepthOld == null){
					tableRow.addView(rowView);			
					tableLayoutBidAsk.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
				}
	
				
				/** COMPUTATION - SUMMARY OF BID & ASK - START **/
				if( ! AppConstants.isMarketOrderStock( marketDepthNew.getBidPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthNew.getBidPrice() ) ){
					totalBid	+= marketDepthNew.getBidSize();
					bidSum		+= marketDepthNew.getBidSize() * marketDepthNew.getBidPrice();
				}

				if( ! AppConstants.isMarketOrderStock( marketDepthNew.getAskPrice() ) && !AppConstants.isMarketPriceStock( marketDepthNew.getAskPrice() ) ){
					totalAsk	+= marketDepthNew.getAskSize();
					askSum 		+= marketDepthNew.getAskPrice() * marketDepthNew.getAskSize();
				}
				/** COMPUTATION - SUMMARY OF BID & ASK - END **/
				
				id++;
			}

			/** AVERAGE OF BID & ASK - START **/
			TableRow tableRow	= null;
			View rowView		= null;
			if(lstOldMarketDepths.size() == 0){
				tableRow 				= new TableRow(this);
				tableRow.setId(id);
				tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
				tableRow.setWeightSum(1);
				
				//rowView					= this.getLayoutInflater().inflate( (width > 480 & height > 800) ? R.layout.bid_ask_row2 : R.layout.bid_ask_row, null);
				rowView					= this.getLayoutInflater().inflate(R.layout.bid_ask_row, null);
				TextView countLabel		= (TextView) rowView.findViewById(R.id.countLabel);
				countLabel.setText("");
			}
	
			/*** AVERAGE OF BID - START ***/
			if(lstOldMarketDepths.size() == 0)
				bidSizeLabel	= (TextView) rowView.findViewById(R.id.bidSizeLabel);
			bidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalBid / intLotSize) ) );
	
			if(lstOldMarketDepths.size() == 0){
				bidPriceLabel 	= (TextView) rowView.findViewById(R.id.bidPriceLabel);
				bidPriceLabel.setTextColor(Color.BLACK);
			}
			bidPriceLabel.setText( FormatUtil.formatPrice( (bidSum != 0 && totalBid != 0) ? ( (float) bidSum / totalBid ) : 0 ) );
			/*** AVERAGE OF BID - END ***/
			
			/*** AVERAGE OF ASK - START ***/
			if(lstOldMarketDepths.size() == 0)
				askSizeLabel 	= (TextView) rowView.findViewById(R.id.askSizeLabel);
			askSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalAsk / intLotSize) ) );
				
			if(lstOldMarketDepths.size() == 0){
				askPriceLabel 	= (TextView) rowView.findViewById(R.id.askPriceLabel);
				askPriceLabel.setTextColor(Color.BLACK);
			}
			askPriceLabel.setText(FormatUtil.formatPrice( (askSum != 0 && totalAsk != 0) ? ( (float) askSum / totalAsk ) : 0 ) );
			/*** AVERAGE OF ASK - END ***/
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			strSymbolExchg	= null;
			
			if(tableLayoutBidAsk != null){
				tableLayoutBidAsk.destroyDrawingCache();
				tableLayoutBidAsk = null;
			}
			
			if(tableLayoutSummary != null){
				tableLayoutSummary.destroyDrawingCache();
				tableLayoutSummary = null;
			}

			id			= 0;
			totalBid 	= 0;
			totalAsk 	= 0;
			bidSum 		= 0;
			askSum 		= 0;
			intLotSize 	= 0;
		}
	}
	//Added by Thinzar@20141002, Change_Request-20140901,ReqNo.5 - END

	//Change_Request-20170601, ReqNo.9 - START
	private final String PARAM_ORDER_SUBMIT 			= "orderSubmit";
	private final String PARAM_ORDER_SUCCESS			= "orderSuccess";
	private final String PARAM_ORDER_FAIL_UNEXPECTED	= "orderFailUnexpected";
	private final String PARAM_TRADE_INITIATE_EVENT 	= "TradeInitiate";
	private final String PARAM_TRADE_ERROR_EVENT		= "TradeError";

	//Status: submit, success, fail
	public void logTradeInitiate(String status){
		//DefinitionConstants.Debug("logTradeInitiate:" + status);

		Answers.getInstance().logCustom(new CustomEvent(PARAM_TRADE_INITIATE_EVENT)
				.putCustomAttribute("status", status));
	}

	//param: e.g., 20170609|C382256|server not found
	public void logTradeError(String response, String params){
		//DefinitionConstants.Debug("logTradeError");

		String currentTimeStamp	= SystemUtil.getCurrentTimeStamp();
		String responseParam 	= currentTimeStamp + "|" + senderCode + "|" + response;
		String tradeParam	 	= currentTimeStamp + "|" + senderCode + "|" + params;

		Answers.getInstance().logCustom(new CustomEvent(PARAM_TRADE_ERROR_EVENT)
				.putCustomAttribute("response", responseParam)
				.putCustomAttribute("tradeParam", tradeParam));
	}
	//Change_Request-20170601, ReqNo.9 - END
}