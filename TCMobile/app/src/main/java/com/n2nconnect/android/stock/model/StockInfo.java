package com.n2nconnect.android.stock.model;

import java.util.List;

import com.n2nconnect.android.stock.AppConstants;

public class StockInfo extends StockSymbol {
	private String sectorName;
	private int sharePerLot;
	private float nta;
	private float parValue;
	private float eps;
	private float peRatio;
	private float floorPrice;
	private float ceilingPrice;
	private List<MarketDepth> marketDepths;
	private long shareIssued;
	private String status;
	private long bVolume;
	private long sVolume;
	private long bTrans;
	private long sTrans;
	private String instrument;
	
	// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4 - START
	private boolean isPayableWithCPF;
	// Added by Sonia@20130705 - Change_Request-20130225, ReqNo.5
	private List<TimeSales> timeSales;
	// Added by Sonia@20130823 - Change_Request-20130225, ReqNo.5
	private int lastTradeNo;

	// Added by Thinzar@20140808 - Change_Request-20140801, ReqNo. 1
	private boolean isCorporateActionNews;

	// Added by Thinzar@20141112 - Change_Request-20141101, ReqNo. 6
	private String foreignLimit;
	private String foreignOwnershipLimit;
	private String price52WeekHi;
	private String price52WeekLo;
	private String dynamicHi;
	private String dynamicLo;

	// Added by Thinzar, Change_Request-20160722, ReqNo.2
	private int theScreenerStarRating;
	private int theScreenerRiskLevel;
	
	//Added by Thinzar, Change_Request-20161101, ReqNo.3
	private long totalShortSellVolume;
	
	//Change_Request-20170119, ReqNo.12
	private float theorecticalPrice;
	
	public double getMarketCap() {
		return shareIssued * this.getLastDone();
	}

	public long getShareIssued() {
		return shareIssued;
	}

	public void setShareIssued(long shareIssued) {
		this.shareIssued = shareIssued;
	}

	public String getSectorName() {
		return sectorName;
	}

	public void setSectorName(String sectorName) {
		this.sectorName = sectorName;
	}

	public int getSharePerLot() {
		return sharePerLot;
	}

	public void setSharePerLot(int sharePerLot) {
		this.sharePerLot = sharePerLot;
	}

	public float getNta() {
		return nta;
	}

	public void setNta(float nta) {
		this.nta = nta;
	}

	public float getParValue() {
		return parValue;
	}

	public void setParValue(float parValue) {
		this.parValue = parValue;
	}

	public float getEps() {
		return eps;
	}

	public void setEps(float eps) {
		this.eps = eps;
	}

	public float getPeRatio() {
		return peRatio;
	}

	public void setPeRatio(float peRatio) {
		this.peRatio = peRatio;
	}

	public float getFloorPrice() {
		return floorPrice;
	}

	public void setFloorPrice(float floorPrice) {
		this.floorPrice = floorPrice;
	}

	public float getCeilingPrice() {
		return ceilingPrice;
	}

	public void setCeilingPrice(float ceilingPrice) {
		this.ceilingPrice = ceilingPrice;
	}

	public List<MarketDepth> getMarketDepths() {
		return marketDepths;
	}

	public void setMarketDepths(List<MarketDepth> marketDepths) {
		this.marketDepths = marketDepths;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setBuyVolume(long bVol) {
		this.bVolume = bVol;
	}

	public long getBuyVolume() {
		return bVolume;
	}

	public void setSellVolume(long sVol) {
		this.sVolume = sVol;
	}

	public long getSellVolume() {
		return sVolume;
	}

	public void setBuyTrans(long bTrans) {
		this.bTrans = bTrans;
	}

	public long getBuyTrans() {
		return bTrans;
	}

	public void setSellTrans(long sTrans) {
		this.sTrans = sTrans;
	}

	public long getSellTrans() {
		return sTrans;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setLastTradeNo(Integer lastTradeNo) {
		this.lastTradeNo = lastTradeNo;
	}

	public int getLastTradeNo() {
		return lastTradeNo;
	}

	public boolean isPayableWithCPF() {
		return isPayableWithCPF;
	}

	public void setPayableWithCPF(boolean isPayableWithCPF) {
		this.isPayableWithCPF = isPayableWithCPF;
	}

	// Added by Thinzar@20140808 - Change_Request-20140801, ReqNo.1 - START
	public boolean isCorporateActionNews() {
		return isCorporateActionNews;
	}

	public void setCorporateActionNews(boolean isCorporateActionNews) {
		this.isCorporateActionNews = isCorporateActionNews;
	}
	// Added by Thinzar@20140808 - Change_Request-20140801, ReqNo.1 - END

	// Added by Thinzar@20141112 - Change_Request-20141101, ReqNo. 6 - START
	public String getForeignLimit() {
		return foreignLimit;
	}

	public void setForeignLimit(String foreignLimit) {
		this.foreignLimit = foreignLimit;
	}

	public String getForeignOwnershipLimit() {
		return foreignOwnershipLimit;
	}

	public void setForeignOwnershipLimit(String foreignOwnershipLimit) {
		this.foreignOwnershipLimit = foreignOwnershipLimit;
	}

	public String getPrice52WeekHi() {
		return price52WeekHi;
	}

	public void setPrice52WeekHi(String price52WeekHi) {
		this.price52WeekHi = price52WeekHi;
	}

	public String getPrice52WeekLo() {
		return price52WeekLo;
	}

	public void setPrice52WeekLo(String price52WeekLo) {
		this.price52WeekLo = price52WeekLo;
	}

	public String getDynamicHi() {
		return dynamicHi;
	}

	public void setDynamicHi(String dynamicHi) {
		this.dynamicHi = dynamicHi;
	}

	public String getDynamicLo() {
		return dynamicLo;
	}

	public void setDynamicLo(String dynamicLo) {
		this.dynamicLo = dynamicLo;
	}
	// Added by Thinzar@20141112 - Change_Request-20141101, ReqNo. 6 - END

	// Added by Thinzar@20140402, Change_Request-20150402, ReqNo. 2 - START
	// @int viewPreference: 0=unit, 1=lot
	public long getBuyVolume(int viewPreference) {
		if (viewPreference == AppConstants.INT_TRADE_QTY_IN_UNIT)
			return bVolume;
		else
			return bVolume / this.sharePerLot;
	}

	public long getSellVolume(int viewPreference) {
		if (viewPreference == AppConstants.INT_TRADE_QTY_IN_UNIT)
			return sVolume;
		else
			return sVolume / this.sharePerLot;
	}
	// Added by Thinzar@20140402, Change_Request-20150402, ReqNo. 2 - END

	// Added by Thinzar, Change_Request-20160722, ReqNo.2 - START
	public int getTheScreenerStarRating() {
		return theScreenerStarRating;
	}

	public void setTheScreenerStarRating(int theScreenerStarRating) {
		this.theScreenerStarRating = theScreenerStarRating;
	}

	public int getTheScreenerRiskLevel() {
		return theScreenerRiskLevel;
	}

	public void setTheScreenerRiskLevel(int theScreenerRiskLevel) {
		this.theScreenerRiskLevel = theScreenerRiskLevel;
	}
	// Added by Thinzar, Change_Request-20160722, ReqNo.2 - END
	
	//Added by Thinzar, Change_Request-20161101, ReqNo.3- START
	public long getTotalShortSellVolume() {
		return totalShortSellVolume;
	}

	public void setTotalShortSellVolume(long totalShortSellVolume) {
		this.totalShortSellVolume = totalShortSellVolume;
	}
	//Added by Thinzar, Change_Request-20161101, ReqNo.3- END
	
	public float getTheorecticalPrice() {
		return theorecticalPrice;
	}

	public void setTheorecticalPrice(float theorecticalPrice) {
		this.theorecticalPrice = theorecticalPrice;
	}
}