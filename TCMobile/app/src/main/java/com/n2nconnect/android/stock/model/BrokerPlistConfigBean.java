package com.n2nconnect.android.stock.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.BHConfigConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.activity.SplashScreenActivity;
import com.n2nconnect.android.stock.util.PlistFileUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.BuildConfig;
//import com.sun.istack.internal.NotNull;
import android.support.annotation.NonNull;

public class BrokerPlistConfigBean {
private String strBrokerHouseBtnName;
	
	private String strAnnouncementURL;
	
	private int intBrokerHouseBtnImg;
	private int intLoginBgImg;	
	private int intLoginBtnBgImg;
	private int intTextColor;
	private int intLinkTextColor;
	private int intTopNavBarBgImg;
	private int intTopNavBarBtnBgImg;
	private int intAccSettingMenuIconBgImg;
	private int intExchgMenuIconBgImg;
	private int intHomeMenuIconBgImg;
	private int intIndiciesMenuIconBgImg;
	private int intLogoutMenuIconBgImg;
	private int intNewsMenuIconBgImg;
	private int intOrdBookMenuIconBgImg;
	private int intPortfolioMenuIconBgImg;
	private int intSearchMenuIconBgImg;
	private int intStkInfoMenuIconBgImg;
	private int intMktSumMenuIconBgImg;
	private int intTradeMenuIconBgImg;
	private int intWatchlistMenuIconBgImg;
	private int intBgImg;
	private int intAnnouncementMenuIconBgImg;
	
	private boolean isEnableTrade			= false;
	private boolean hasRegistration			= false;
	private boolean hasSignInTermNCondition	= false;
	private boolean showPortfolioDisclaimer	= false;
	private boolean showPoweredByStatement	= false;
	private boolean showRememberMe			= false;
	private boolean isCacheUsername			= false;

	private String strPortfolioDisclaimerUrl = null;
	private String strRiskDisclosureStatementUrl = null;
	private String strLMSSponsor = null;
	private String strLMSServer = null;
	private String strLMSProduct = null;
	private String strLMSSubscriptionUrl = null;
	private String strLMSSubscriptionStatusUrl = null;
	private String strAtpIP = null;
	private int intAtpPort;
	private String strNewsIP = null;
	
	private String strChartIP = null;
	private String strAppName = null;
	private String strAppIntro = null;
	private String strAppConclude = null;
	private String strKeyFeaturesTitle = null;
	
	private ArrayList<String> alKeyFeatures				= new ArrayList<String>();
	private ArrayList<String> alTermsNConditions		= new ArrayList<String>();
	private ArrayList<String> alTermsOfService			= new ArrayList<String>();
	private ArrayList<String> alPrivacyPolicy			= new ArrayList<String>();
	private ArrayList<String> alDisclaimerOfWarranty	= new ArrayList<String>();
	
	private boolean defaultIsAutoFillTradePrice			= true;
	private boolean defaultIsAutoFillTradeQuantity		= false;
	private int defaultIntCustomTradeQuantity			= 0;
	private final int defaultIntTradeQtyMeasurement		= AppConstants.INT_TRADE_QTY_IN_LOT;
	
	
	private boolean isAutoFillTradePrice				= defaultIsAutoFillTradePrice;
	private boolean isAutoFillTradeQuantity				= defaultIsAutoFillTradeQuantity;
	private int intCustomTradeQuantity					= defaultIntCustomTradeQuantity;
	//private int intTradeQtyMeasurement				= defaultIntTradeQtyMeasurement;
	private int intTradeQtyMeasurement					= AppConstants.DEFAULT_TRADE_QTY_TYPE; 
			
	private String strDefaultPaymentType 				= null;
	private String strDefaultCurrencyType 				= null;
	private String strBaseATPExchangeCode 				= null;
	private String strBaseQCExchangeCode 				= null;
	private List<String> lstPymtTypeToBeFilter			= new ArrayList<String>();
	
	private int intViewQtyMeasurement;
	
	private boolean isConnectJavaQC				= true;
	private boolean isGetNewKey					= true;
	
	private boolean isEnableE2EEEncryption		= false;
	private boolean isEnableEncryption			= true;
	
	private boolean isEnablePinFeatures			= false;
	private boolean isEnablePasswordFeatures	= false;
	private boolean isMultilevelMarketDepth		= false;

	private boolean isShowSkipConfirmation		= false;
	private boolean isShowShortSell				= false;
	private List<String> lstExchgShowShortSell			= new ArrayList<String>();
	private String strAboutURL = null;
	private String strTermsOfServiceURL = null;
	private String strPrivacyPolicyURL = null;
	private String getStrDisclaimerOfWarrantyURL = null;
	
	private boolean isImposeMaxLenOnQty				= true;
	private boolean isShowLimitAtOrderPad			= true;
	private boolean isShowLimitAtPortfolio			= true;
	private boolean isShowLimitAtOrderBook			= true;

	private int intMaxNoOfWatchlistCreate;
	private int intMaxNoOfStockAddToWatchlist;
	private boolean isImposeMaxWatchlistCreate;
	private boolean isImposeMaxStockAddToWatchlist;
	
	//Added by Thinzar@20140826 - Change_Request-20140801, ReqNo.6
	private boolean isShowDMAAgreement 				= false;		//defailt is false
	private String DMAWebViewURL 					= "";
	
	//Added by Thinzar@20140902, Fixes_Request-20140820, ReqNo. 11
	private boolean hasRestrictionToViewMY			= false;		//default is false;
	
	//Added by Thinzar@20140915, Change_Request-20140901, ReqNo. 6
	private boolean isSupportATPLoadBalance			= false;	//default is false
	
	//Added by Thinzar@20140926, Fixes_Request-20140820, ReqNo.29
	private String currencyRateLabel				= "Currency Rate";	//setting default value
	
	//Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 9
	private String minVersion						= "1.2.0";
		
	//Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 10
	private String strAppRequirement				= "Compatible with Android Mobile Phones. Supported OS Version: Android 2.3.3 and above.";
		
	//Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 11
	private ArrayList<String> filterShowMarketArray	= new ArrayList<String>();
	
	//Added by Thinzar@20141103, Change_Request-20140901, ReqNo. 14
	private String historicalChartServer			= "";
	private String intradayChartServer				= "";
	
	//Added by Diyana@20160614, ReqNo. 1
	private String interactiveChartServer				= "";
	
	//Added by Thinzar@20141110, Change_Request-20141101, ReqNo. 2
	private boolean msgPromptForStopNIfTouchOrdType	= false;
	
	//Added by Thinzar@20141111, Change_Request-20141101, ReqNo. 4
	private String tradeValueLabelOnQuoteScreen		= "T.Value";	
	private String tradedAtLabelOnTimeNSale			= "Traded At";
	
	//Added by Thinzar@20150522, Change_Request-20150401, ReqNo. 3
	//set the CPIQ as default in case plist files are not updated
	private String fundamentalNewsServer			= "http://plc.asiaebroker.com/plc/";
	private String fundamentalSourceCode			= "CPIQ";
	private String fundamentalNewsLabel				= "S&P Capital IQ";
	private String fundamentalDefaultReportCode		= AppConstants.strDefaultReportPeriod;
	
	//Added by Diyana interactive chart
	private String chartServer						= "";
	private boolean isInteractiveChart				= false;
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.10
	private boolean isEnableStockAlert				= false;
	private int orderHistoryDuration;
	private ArrayList<String> lstSectorNoTrading	= new ArrayList<String>();
	
	//Added by Thinzar, to cater TCMobile 2.0 design for different brokers
	private boolean hasBottomTextOnLoginPage		= false;
	
	//Added by Thinzar, Change_Request-20160722, ReqNo. 1 & 2
	private boolean isArchiveNews					= false;
	private boolean isEnableQRLogin					= false;
	private boolean isEnableScreener				= false;
	private String theScreenerUrl					= "";
	
	//ChangeRequest-20160722, ReqNo.3
	private String chartType			= ParamConstants.CHARTTYPE.chartTypeImage;		//IMAGE, MODULUS, INTERACTIVE (IMAGE AS DEFAULT)
	private String newChartServer		= null;
	private String archiveNewsUrl		="";
	private boolean isTutorialAvailable	=false;
	
	//Change_Request-20160101,Req.11
	private boolean isATPServerSSL		= false;
	
	public String stockAlertPublicKey;
	public String stockAlertJWT;
	public String registerPushLink;
	public String stockAlertUrl;
	
	//Change_Request-20160722, ReqNo.7
	public String stkAlertDisclaimer			= "";	//empty string as default
	
	//Change_Request-20160722, ReqNo.8
	public boolean isEnableIBillionaire			= false;	//disabled by default
	//Change_Request-20170601, ReqNo.1
	private boolean isIBillionaireMenuEnabled	= false;
	//Fixes_Request-20161101, ReqNo.4
	private List<String> arrExchgIBillionaire	= new ArrayList<String>();
		
	//Fixes_Request-20161101, ReqNo.3
	private List<String> arrExchgSupportModulus	= new ArrayList<String>();
	
	//Change_Request-20161101, ReqNo.3
	private List<String> lstExchgShowPrivateOrder	= new ArrayList<String>();
	
	//Change_Request-20170119, ReqNo.3
	private String newFundamentalSourceCode	= "";
	private String newFundamentalNewsServer	= "";
	private String newFundamentalNewsLabel	= "";
	
	//Change_Request-20170119, ReqNo.5
	private boolean isDtiEnabled			= false;	//fase as default
	private String DtiUrl					= "";
	private String DtiApiUrl				= "";
	private ArrayList<String> alDtiDisclaimer	= new ArrayList<String>();
	private boolean isShowDtiPopUp 			= false; 	//false as default

	//Change_Request-20170119, ReqNo.6
	private String moversLabelOnQuoteScreen	= "Movers";		//Movers as default, Abacus wants customized
	
	//Change_Request-20170119, ReqNo.8
	private int maxIdleTime					= 1800000;			//default is 1800 seconds, 1800,000 milliseconds
	
	//Change_Request-20170119, ReqNo.8
	private String brokerCode				= null;
	
	//Change_Request-20160722, ReqNo.8
	private boolean isTouchIDEnabled				= false;
	private boolean isSecondLvlSecurityEnabled		= false;
	
	//Change_Request-20170119, ReqNo.12
	private boolean isShowTopAtLactp				= false;	//false as default
	
	//Change_Request-20170601, ReqNo.2
	private boolean isEnableElasticNews				= false;
	private String elasticResearchReportsUrl;
	private String elasticReuterNewsUrl;

	//Change_Request-20170601, ReqNo.3
	private boolean isEnableContactUs				= false;
	private String contactEmail;
	
	//Change_Request-20170601, ReqNo.6
	private boolean isEnableContraAlert				= false;
	
	//Change_Request-20170601, ReqNo.8
	private boolean isEnableSgxAppIntegration		= false;

	//Change_Request-20170601, ReqNo.13
	private boolean isShowResearchReportNote 		= false;

	//Added for AmSec and AmFutures
	private boolean isShowContactUsOnLoginPage   	= false;
	private String phoneContact;
	private String emailContact;

	//Change_Request-20170601, ReqNo.15
	private String stockAlertBHCode;

	//Change_Request-20170601, ReqNo.17
	private boolean isShowPortfolioDelayFeed 			= false;
	private ArrayList<String> lstPortfolioDelayFeedExchg = new ArrayList<String>();

	//Change_Request-2017-0601, ReqNo.18
	private boolean isLinkIBWebview 					= false;
	private String iBMainUrl;

	//Fixes_Request-20170701, ReqNo.18
	private boolean isRequireLoginTcPlusToChangeHint	= false;
	private String tcPlusLink;

	//Change_Request-20170601, ReqNo.20
	private boolean isBlockRootedDevices 				= false;

	public BrokerPlistConfigBean(){
		initKeyFeatures();
	}
	
	private void initKeyFeatures(){
		alKeyFeatures.add(" - Bursa Equities Real-Time Streaming Feed");
		alKeyFeatures.add(" - Singapore Equities Real-Time Streaming Feed");
		alKeyFeatures.add(" - Hong Kong Equities Real-Time Streaming Feed");
		alKeyFeatures.add(" - Symbol Search");
		alKeyFeatures.add(" - Watch List");
    	alKeyFeatures.add(" - Stock Info");
    	alKeyFeatures.add(" - Market Depth");
    	alKeyFeatures.add(" - Intraday & Historical Charts");
    	alKeyFeatures.add(" - Stock News");
    	alKeyFeatures.add(" - Market Summary");
    	alKeyFeatures.add(" - Order Book & Order Details");
    	alKeyFeatures.add(" - Multi-Instrument Order Pad");
    	alKeyFeatures.add(" - Equity Portfolio");
	}
	
	/* Button Icon - Name - START */
	public String getStrBrokerHouseBtnName(){
		return this.strBrokerHouseBtnName;
	}
	public void setStrBrokerHouseBtnName(String strBrokerHouseBtnName){
		this.strBrokerHouseBtnName = strBrokerHouseBtnName;
	}
	/* Button Icon - Name - END */
	
	/* Button Icon - Image - START */
	public int getIntBrokerHouseBtnImg(){
		return this.intBrokerHouseBtnImg;
	}
	public void setIntBrokerHouseBtnImg(int intBrokerHouseBtnImg){
		this.intBrokerHouseBtnImg = intBrokerHouseBtnImg;
	}
	/* Button Icon - Name - END */
	
	/* Login Screen - background image - START */
	public int getIntLoginBgImg() {
		return this.intLoginBgImg;
	}
	public void setIntLoginBgImg(int intLoginBgImg){
		this.intLoginBgImg	= intLoginBgImg;
	}
	/* Login Screen - background image - END */
	
	/* Login Screen - button background image - START */
	
	public int getIntLoginBtnBgImg(){
		return this.intLoginBtnBgImg;
	}
	
	public void setIntLoginBtnBgImg(int intLoginBtnBgImg){
		this.intLoginBtnBgImg = intLoginBtnBgImg;
	}
	/* Login Screen - button background image - END */
	
	/* Login Screen - Powered By Statement - START */
	public boolean showPoweredByStatement(){
		return this.showPoweredByStatement;
	}
	public void isShowPoweredByStatement(boolean showPoweredByStatement){
		this.showPoweredByStatement = showPoweredByStatement;
	}
	/* Login Screen - Powered By Statement - END */
	
	/* Login Screen - has sign in Terms and Condition - START */
	public boolean hasSignInTermsNCondition(){
		return this.hasSignInTermNCondition;
	}
	public void setHasSignInTermsNCondition(boolean hasSignInTermsNCondition){
		this.hasSignInTermNCondition = hasSignInTermsNCondition;
	}
	/* Login Screen - has sign in Term and Condition - END */

	/* Login Screen - Remember Me - START */
	public boolean showRememberMe(){
		return this.showRememberMe;
	}
	public void isShowRememberMe(boolean showRememberMe){
		this.showRememberMe = showRememberMe;
	}
	/* Login Screen - Remember Me - END */
	
	/* Login Screen - is cache username - START */
	public boolean isCacheUsername(){
		return (! showRememberMe ? false :  this.isCacheUsername);
	}
	public void setIsCacheUsername(boolean isCacheUsername){
		this.isCacheUsername = ! showRememberMe ? false : isCacheUsername;
	}
	/* Login Screen - is cache username - END */
	
	/* Login - E2EE encryption - START */
	public boolean isEnableE2EEEncryption(){
		return this.isEnableE2EEEncryption;
	}
	public void setEnableE2EEEncryption(boolean isEnableE2EEEncryption){
		this.isEnableE2EEEncryption = isEnableE2EEEncryption;
	}
	/* Login - E2EE encryption - END */
	
	/* Text color - START */
	
	public int getIntTextColor(){
		return this.intTextColor;
	}
	
	public void setIntTextColor(int intTextColor){
		this.intTextColor	= intTextColor;
	}
	/* Text color - END */
	
	/* Link text color - START */
	
	public int getIntLinkTextColor(){
		return this.intLinkTextColor;
	}
	
	public void setIntLinkTextColor(int intLinkTextColor){
		this.intLinkTextColor	= intLinkTextColor;
	}
	/* Link text color - END */
	
	/* Top Navigation Bar - background image - START */
	public int getIntTopNavBarBgImg(){
		return this.intTopNavBarBgImg;
	}
	public void setIntTopNavBarBgImg(int intTopNavBarBgImg){
		this.intTopNavBarBgImg	= intTopNavBarBgImg;
	}
	/* Top Navigation Bar - background image - END */
	
	/* Top Navigation Bar - button background image - START */
	public int getIntTopNavBarBtnBgImg(){
		return this.intTopNavBarBtnBgImg;
	}
	public void setIntTopNavBarBtnBgImg(int intTopNavBarBtnBgImg){
		this.intTopNavBarBtnBgImg	= intTopNavBarBtnBgImg;
	}
	/* Top Navigation Bar - button background image - END */
	
	/* Menu Icon - background image - START */
	
	public int getIntAccSettingMenuIconBgImg(){
		return this.intAccSettingMenuIconBgImg;
	}
	
	public void setIntAccSettingMenuIconBgImg(int intAccSettingMenuIconBgImg){
		this.intAccSettingMenuIconBgImg = intAccSettingMenuIconBgImg;
	}
	
	
	public int getIntExchgMenuIconBgImg(){
		return this.intExchgMenuIconBgImg;
	}
	
	public void setIntExchgMenuIconBgImg(int intExchgMenuIconBgImg){
		this.intExchgMenuIconBgImg = intExchgMenuIconBgImg;
	}
	
	
	public int getIntHomeMenuIconBgImg(){
		return this.intHomeMenuIconBgImg;
	}
	
	public void setIntHomeMenuIconBgImg(int intHomeMenuIconBgImg){
		this.intHomeMenuIconBgImg = intHomeMenuIconBgImg;
	}
	
	
	public int getIntIndiciesMenuIconBgImg(){
		return this.intIndiciesMenuIconBgImg;
	}
	
	public void setIntIndiciesMenuIconBgImg(int intIndiciesMenuIconBgImg){
		this.intIndiciesMenuIconBgImg = intIndiciesMenuIconBgImg;
	}
	
	
	public int getIntLogoutMenuIconBgImg(){
		return this.intLogoutMenuIconBgImg;
	}
	
	public void setIntLogoutMenuIconBgImg(int intLogoutMenuIconBgImg){
		this.intLogoutMenuIconBgImg = intLogoutMenuIconBgImg;
	}
	
	public int getIntNewsMenuIconBgImg(){
		return this.intNewsMenuIconBgImg;
	}
	
	public void setIntNewsMenuIconBgImg(int intNewsMenuIconBgImg){
		this.intNewsMenuIconBgImg = intNewsMenuIconBgImg;
	}
	
	
	public int getIntOrdBookMenuIconBgImg(){
		return this.intOrdBookMenuIconBgImg;
	}
	
	public void setIntOrdBookMenuIconBgImg(int intOrdBookMenuIconBgImg){
		this.intOrdBookMenuIconBgImg = intOrdBookMenuIconBgImg;
	}
	
	
	public int getIntPortfolioMenuIconBgImg(){
		return this.intPortfolioMenuIconBgImg;
	}
	
	public void setIntPortfolioMenuIconBgImg(int intPortfolioMenuIconBgImg){
		this.intPortfolioMenuIconBgImg = intPortfolioMenuIconBgImg;
	}
	
	public int getIntSearchMenuIconBgImg(){
		return this.intSearchMenuIconBgImg;
	}
	
	public void setIntSearchMenuIconBgImg(int intSearchMenuIconBgImg){
		this.intSearchMenuIconBgImg = intSearchMenuIconBgImg;
	}
	
	public int getIntStkInfoMenuIconBgImg(){
		return this.intStkInfoMenuIconBgImg;
	}
	
	public void setIntStkInfoMenuIconBgImg(int intStkInfoMenuIconBgImg){
		this.intStkInfoMenuIconBgImg = intStkInfoMenuIconBgImg;
	}
	
	public int getIntMktSumMenuIconBgImg(){
		return this.intMktSumMenuIconBgImg;
	}
	
	public void setIntMktSumMenuIconBgImg(int intMktSumMenuIconBgImg){
		this.intMktSumMenuIconBgImg = intMktSumMenuIconBgImg;
	}
	
	public int getIntTradeMenuIconBgImg(){
		return this.intTradeMenuIconBgImg;
	}
	
	public void setIntTradeMenuIconBgImg(int intTradeMenuIconBgImg){
		this.intTradeMenuIconBgImg = intTradeMenuIconBgImg;
	}
	
	public int getIntWatchlistMenuIconBgImg(){
		return this.intWatchlistMenuIconBgImg;
	}
	
	public void setIntWatchlistMenuIconBgImg(int intWatchlistMenuIconBgImg){
		this.intWatchlistMenuIconBgImg = intWatchlistMenuIconBgImg;
	}
	
	public int getIntAnnouncementMenuIconBgImg() {
		return this.intAnnouncementMenuIconBgImg;
	}
	
	public void setIntAnnouncementMenuIconBgImg(int intAnnouncementMenuIconBgImg) {
		this.intAnnouncementMenuIconBgImg = intAnnouncementMenuIconBgImg;
	}
	/* Menu Icon - background image - END */
	
	/* Menu Icon Setting - START*/
	public String getStrAnnouncementURL() {
		return strAnnouncementURL;
	}
	public void setStrAnnouncementURL(String strAnnouncementURL) {
		this.strAnnouncementURL = strAnnouncementURL;
	}
	/* Menu Icon Setting - END*/
	
	/* Page - background image - START */
	
	public int getIntBgImg(){
		return this.intBgImg;
	}
	
	public void setIntBgImg(int intBgImg){
		this.intBgImg = intBgImg;
	}
	/* Page - background image - END */
	
	/* Has Registration facility - START */
	
	public boolean hasRegistration(){
		return this.hasRegistration;
	}
	
	public void isHasRegistration(boolean hasRegistration){
		this.hasRegistration = hasRegistration;
	}
	/* Has Registration facility - END */
	
	
	/* Disclaimer facility - START */
	public boolean showPortfolioDisclaimer(){
		return this.showPortfolioDisclaimer;
	}
	
	public void isShowPortfolioDisclaimer(boolean showPortfolioDisclaimer){
		this.showPortfolioDisclaimer = showPortfolioDisclaimer;
	}
	
	
	public String getStrPortfolioDisclaimerUrl(){
		return this.strPortfolioDisclaimerUrl;
	}
	
	public void setStrPortfolioDisclaimerUrl(String strPortfolioDisclaimerUrl){
		this.strPortfolioDisclaimerUrl = strPortfolioDisclaimerUrl;
	}
	/* Disclaimer facility - END */
	
	/* Risk Disclosure Statement - START */
	public String getStrRiskDisclosureStatementUrl(){
		return this.strRiskDisclosureStatementUrl;
	}
	
	public void setStrRiskDisclosureStatementUrl(String strRiskDisclosureStatementUrl){
		this.strRiskDisclosureStatementUrl = strRiskDisclosureStatementUrl;
	}
	/* Risk Disclosure Statement - END */
	
	
	/* LMS Subscription Config - START */
	public String getStrLMSSponsor(){
		return this.strLMSSponsor;
	}
	
	public void setStrLMSSponsor(@NonNull String strLMSSponsor){
		this.strLMSSponsor	= strLMSSponsor;
	}
	
	
	public String getStrLMSProduct(){
		return this.strLMSProduct;
	}
	
	public void setStrLMSProduct(@NonNull String strLMSProduct){
		this.strLMSProduct	= strLMSProduct;
	}
	
	public String getStrLMSServer(){
		return this.strLMSServer;
	}
	
	public void setStrLMSServer(@NonNull String strLMSServer){
		this.strLMSServer	= strLMSServer;
	}
	
	public String getStrLMSSubscriptionUrl(){
		return this.strLMSSubscriptionUrl;
	}
	
	public void setStrLMSSubscriptionUrl(@NonNull String strLMSSubscriptionUrl){
		this.strLMSSubscriptionUrl = strLMSSubscriptionUrl;
	}
	
	
	public String getStrLMSSubscriptionStatusUrl(){
		return this.strLMSSubscriptionStatusUrl;
	}
	
	public void setStrLMSSubscriptionStatusUrl(@NonNull String strLMSSubscriptionStatusUrl){
		this.strLMSSubscriptionStatusUrl = strLMSSubscriptionStatusUrl;
	}
	/* LMS Subscription Config - END */
	
	/* ATP Config - START */
	public String getStrAtpIP(){
		return this.strAtpIP;
	}
	
	public void setStrAtpIP(@NonNull String strAtpIP){
		this.strAtpIP	= strAtpIP;
	}
	
	
	public int getIntAtpPort(){
		return this.intAtpPort;
	}
	
	public void setIntAtpPort(int intAtpPort){
		this.intAtpPort = intAtpPort;
	}
	/* ATP Config - END */

	/* QC(Feed) Config - START */
	public boolean isGetNewKey(){
		return this.isGetNewKey;
	}
	public void setIsGetNewKey(boolean isGetNewKey){
		this.isGetNewKey = isGetNewKey;
	}
	/* QC(Feed) Config - END */
	
	/* News Config - START */
	public String getStrNewsIP(){
		return this.strNewsIP;
	}
	public void setStrNewsIP(@NonNull String strNewsIP){
		this.strNewsIP = strNewsIP;		
	}
	/* News Config - END */
	
	/* Chart Config - START */
	public String getStrChartIP(){
		return this.strChartIP;
	}
	public void setStrChartIP(@NonNull String strChartIP){
		this.strChartIP = strChartIP;		
	}
	/* Chart Config - END */
	
	/* Application Name - START */
	public String getStrAppName(){
		return this.strAppName;
	}
	public void setStrAppName(@NonNull String strAppName){
		this.strAppName = strAppName;
	}
	/* Application Name - END */
	
	/* Application Introduction - START */
	public String getStrAppIntro(){
		return this.strAppIntro;
	}
	public void setStrAppIntro(@NonNull String strAppIntro){
		this.strAppIntro = strAppIntro;
	}
	/* Application Introduction - END */
	
	/* Application Conclusion - START */
	public String getStrAppConclude(){
		return strAppConclude;
	}
	public void setStrAppConclude(@NonNull String strAppConclude){
		this.strAppConclude = strAppConclude;
	}
	/* Application Conclusion - END */
	
	/* Key Features Title - START */
	public String getStrKeyFeaturesTitle(){
		return strKeyFeaturesTitle;
	}
	public void setStrKeyFeaturesTitle(@NonNull String strKeyFeaturesTitle){
		this.strKeyFeaturesTitle	= strKeyFeaturesTitle;
	}
	/* Key Features Title - END */

	/* Key Features - START */
	public ArrayList<String> getALKeyFeatures(){
		return this.alKeyFeatures;
	}
	
	public void setALKeyFeatures(@NonNull ArrayList<String> alKeyFeatures){
		this.alKeyFeatures = alKeyFeatures;
	}
	/* Key Features - END */
	
	/* Terms and Conditions - START */
	public ArrayList<String> getALTermsAndConditions(){
		return this.alTermsNConditions;
	}
	
	public void setALTermsAndConditions(@NonNull ArrayList<String> alTermsNConditions){
		this.alTermsNConditions = alTermsNConditions;
	}
	/* Terms and Conditions - END */
	
	/* Terms of Service - START */
	public ArrayList<String> getALTermsOfService(){
		return this.alTermsOfService;
	}
	public void setALTermsOfService(@NonNull ArrayList<String> alTermsOfService){
		this.alTermsOfService = alTermsOfService;
	}
	/* Terms of Service - END */
	
	/* Privacy Policy - START */
	public ArrayList<String> getALPrivacyPolicy(){
		return this.alPrivacyPolicy;
	}
	public void setALPrivacyPolicy(@NonNull ArrayList<String> alPrivacyPolicy){
		this.alPrivacyPolicy = alPrivacyPolicy;
	}
	/* Privacy Policy - END */
	
	/* Disclaimer of Warranty - START */
	public ArrayList<String> getALDisclaimerOfWarranty(){
		return this.alDisclaimerOfWarranty;
	}
	public void setALDisclaimerOfWarranty(@NonNull ArrayList<String> alDisclaimerOfWarranty){
		this.alDisclaimerOfWarranty = alDisclaimerOfWarranty;
	}
	/* Disclaimer of Warranty - END */
	
	/* Trade Preference - Trade Quantity Measurement - START */
	public int getIntTradeQtyMeasurement(){
		return this.intTradeQtyMeasurement;
	}
	
	public void setIntTradeQtyMeasurement(int intTradeQtyMeasurement){
		this.intTradeQtyMeasurement = intTradeQtyMeasurement;
	}
	/* Trade Preference - Trade Quantity Measurement - END */
	
	/* Trade Preference - Auto Fill Quantity - START */
	public boolean autoFillTradeQuantity(){
		return this.isAutoFillTradeQuantity;
	}
	
	public void isAutoFillTradeQuantity(boolean isAutoFillTradeQuantity){
		this.isAutoFillTradeQuantity = isAutoFillTradeQuantity;
	}
	/* Trade Preference - Auto Fill Quantity - END */
	
	/* Trade Preference - Custom Quantity - START */
	public int getIntCustomTradeQuantity(){
		return this.intCustomTradeQuantity;
	}
	
	public void setIntCustomTradeQuantity(int intCustomTradeQuantity){
		this.intCustomTradeQuantity	= intCustomTradeQuantity;
	}
	/* Trade Preference - Custom Quantity - END */
	
	/* Trade Preference - Auto Fill Quantity - START */
	public boolean autoFillTradePrice(){
		return this.isAutoFillTradePrice;
	}
	
	public void isAutoFillTradePrice(boolean isAutoFillTradePrice){
		this.isAutoFillTradePrice	= isAutoFillTradePrice;
	}
	/* Trade Preference - Auto Fill Quantity - END */
	
	/* Restore Default Config - START */
	public BrokerPlistConfigBean restoreConfigBean(){
		return new BrokerPlistConfigBean();
	}
	/* Restore Default Config - END */
	
	/* Restore Default Trade Preference Config - START */
	public void resetTradePreferenceConfig(){
		//this.intTradeQtyMeasurement	= this.defaultIntTradeQtyMeasurement;
		this.intTradeQtyMeasurement	= AppConstants.DEFAULT_TRADE_QTY_TYPE;
		this.intCustomTradeQuantity	= this.defaultIntCustomTradeQuantity;
		this.isAutoFillTradePrice	= this.defaultIsAutoFillTradePrice;
		this.isAutoFillTradeQuantity= this.defaultIsAutoFillTradeQuantity;
	}
	/* Restore Default Trade Preference Config - END */
	
	/* Base ATP Exchange Code - START */
	public String getStrBaseATPExchangeCode(){
		return this.strBaseATPExchangeCode;
	}
	
	public void setStrBaseATPExchangeCode(String strBaseATPExchangeCode){
		this.strBaseATPExchangeCode = strBaseATPExchangeCode;
	}
	/* Base ATP Exchange Code - END */
	
	/* Base QC Exchange Code - START */
	public String getStrBaseQCExchangeCode(){
		return this.strBaseQCExchangeCode;
	}
	
	public void setStrBaseQCExchangeCode(String strBaseQCExchangeCode){
		this.strBaseQCExchangeCode = strBaseQCExchangeCode;
	}
	/* Base QC Exchange Code - END */
	
	/* Order Pad - Default Payment Type - START */
	
	public String getStrDefaultPaymentType(){
		return this.strDefaultPaymentType;
	}
	
	public void setStrDefaultPaymentType(String strPaymentType){
		this.strDefaultPaymentType	= strPaymentType;
	}
	/* Order Pad - Default Payment Type - END */
	
	/* Order Pad - Default Currency Type - START */
	
	public String getStrDefaultCurrencyType(){
		return this.strDefaultCurrencyType;
	}
	
	public void setStrDefaultCurrencyType(String strCurrencyType){
		this.strDefaultCurrencyType = strCurrencyType;
	}
	/* Order Pad - Default Currency Type - END */

	/* Order Pad - Payment Type to be filter - START */
	public List<String> getLstPymtTypeToBeFilter(){
		return this.lstPymtTypeToBeFilter;
	}
	public void setLstPymtTypeToBeFilter(List<String> lstPymtTypeToBeFilter){
		this.lstPymtTypeToBeFilter	= lstPymtTypeToBeFilter;
	}
	/* Order Pad - Payment Type to be filter - END */
	
	/* Order Pad - Is show limit - START */
	public boolean isShowLimitAtOrderPad(){
		return isShowLimitAtOrderPad;
	}
	public void setShowLimitAtOrderPad(boolean isShowLimitAtOrderPad){
		this.isShowLimitAtOrderPad = isShowLimitAtOrderPad;
	}
	/* Order Pad - Is show limit - END */
	
	/* Portfolio - Is show limit - START */
	public boolean isShowLimitAtPortfolio(){
		return isShowLimitAtPortfolio;
	}
	public void setShowLimitAtPortfolio(boolean isShowLimitAtPortfolio){
		this.isShowLimitAtPortfolio = isShowLimitAtPortfolio;
	}
	/* Portfolio - Is show limit - END */
	
	/* Order Book - Is show limit - START */
	public boolean isShowLimitAtOrderBook(){
		return isShowLimitAtOrderBook;
	}
	public void setShowLimitAtOrderBook(boolean isShowLimitAtOrderBook){
		this.isShowLimitAtOrderBook = isShowLimitAtOrderBook;
	}
	/* Order Book - Is show limit - END */
	
	/* View Preference - View Quantity Measurement - START */
	
	public int getIntViewQtyMeasurement(){
		return this.intViewQtyMeasurement;
	}
	
	public void setIntViewQtyMeasurement(int intViewQtyMeasurement){
		this.intViewQtyMeasurement = intViewQtyMeasurement;
	}
	/* View Preference - View Quantity Measurement - END */
	
	/* Request Preference - Enable Encryption - START */
	public boolean isEnableEncryption(){
		return this.isEnableEncryption;
	}
	public void setEnableEncryption(boolean isEnableEncryption){
		this.isEnableEncryption = isEnableEncryption;
	}
	/* Request Preference - Enable Encryption - END */
	
	/* Setting - Enable Pin Features - START */
	public boolean isEnablePinFeatures(){
		return this.isEnablePinFeatures;
	}
	public void setEnablePinFeatures(boolean isEnablePinFeatures){
		this.isEnablePinFeatures = isEnablePinFeatures;
	}
	/* Setting - Enable Pin Features - END */
	
	/* Setting - Enable Password Features - START */
	public boolean isEnablePasswordFeatures(){
		return this.isEnablePasswordFeatures;
	}
	public void setEnablePasswordFeatures(boolean isEnablePasswordFeatures){
		this.isEnablePasswordFeatures = isEnablePasswordFeatures;
	}
	/* Setting - Enable Password Features - END */
	
	/*Multilevel - Marketdepth -START */
	
	public boolean isMultilevelMarketDepth() {
		return this.isMultilevelMarketDepth;
	}
	
	public void setMultilevelMarketDepth(boolean isMultilevelMarketDepth) {
		this.isMultilevelMarketDepth =isMultilevelMarketDepth;
	}
	/*Multilevel - Marketdepth -END */
	
	/* Setting - Enable Trade Features - START */
	
	public boolean enableTrade(){
		return this.isEnableTrade;
	}
	public void isEnableTrade(boolean isEnableTrade){
		this.isEnableTrade	= isEnableTrade;
	}
	/* Setting - Enable Trade Features - END */
	
	/* Trade Setting - show Skip Confirmation Option - START */
	public boolean isShowSkipConfirmation(){
		return isShowSkipConfirmation;
	}
	public void setShowSkipConfirmation(boolean isShowSkipConfirmation){
		this.isShowSkipConfirmation = isShowSkipConfirmation;
	}
	/* Trade Setting - show Skip Confirmation Option - END */
	
	/* Trade Setting - show Short Sell Option - START */
	public boolean isShowShortSell(){
		return isShowShortSell;
	}
	public void setShowShortSell(boolean isShowShortSell){
		this.isShowShortSell = isShowShortSell;
	}
	/* Trade Setting - show Short Sell Option - END */
	
	/* Trade Setting - List of Exchange that show Short Sell Option - START */
	public List<String> lstExchgShowShortSell(){
		return lstExchgShowShortSell;
	}
	public void setLstExchgShowSortSell(List<String> lstExchgShowShortSell){
		this.lstExchgShowShortSell	= lstExchgShowShortSell;
	}
	/* Trade Setting - List of Exchange that show Short Sell Option - END */
	
	/* Setting - About Link - START */
	public String getStrAboutURL(){
		return this.strAboutURL;
	}
	public void setAboutURL(String strAboutURL){
		this.strAboutURL = strAboutURL;
	}
	/* Setting - show About - END */
	
	/* Setting - Terms of Service Link - START */
	public String getStrTermsOfServiceURL(){
		return this.strTermsOfServiceURL;
	}
	public void setStrTermsOfServiceURL(String strTermsOfServiceURL){
		this.strTermsOfServiceURL = strTermsOfServiceURL;
	}
	/* Setting - Terms Of Service Link - END */
	
	/* Setting - show Privacy Policy - START */
	public String getStrPrivacyPolicyURL(){
		return this.strPrivacyPolicyURL;
	}
	public void setStrPrivacyPolicyURL(String strPrivacyPolicyURL){
		this.strPrivacyPolicyURL = strPrivacyPolicyURL;
	}
	/* Setting - show Privacy Policy - END */
	
	/* Setting - show Disclaimer of Warranty - START */
	public String getStrDisclaimerOfWarrantyURL(){
		return this.getStrDisclaimerOfWarrantyURL;
	}
	public void setStrDisclaimerOfWarrantyURL(String getStrDisclaimerOfWarrantyURL){
		this.getStrDisclaimerOfWarrantyURL = getStrDisclaimerOfWarrantyURL;
	}
	/* Setting - show Disclaimer of Warranty - END */
	
	/* Setting - Connect to Java QC instead of Delphi QC - START */
	public boolean isConnectJavaQC(){
		return isConnectJavaQC;
	}
	public void setConnectJavaQC(boolean isConnectJavaQC){
		this.isConnectJavaQC = isConnectJavaQC;
	}
	/* Setting - Connect to Java QC instead of Delphi QC - END */
	
	/* Trade Setting - Impose Max Input Length On Trade Quantity - START*/
	public boolean isImposeMaxLenOnQty(){
		return isImposeMaxLenOnQty;
	}
	public void setImposeMaxLenOnQty(boolean isImposeMaxLenOnQty){
		this.isImposeMaxLenOnQty = isImposeMaxLenOnQty;
	}
	/* Trade Setting - Impose Max Input Length On Trade Quantity - END*/
	
	/* Watchlist Setting - Set Maximum No Of Watchlist Create - START */
	public int getIntMaxNoOfWatchlistCreate(){
		return this.intMaxNoOfWatchlistCreate;
	}
	public void setIntMaxNoOfWatchlistCreate(int intMaxNoOfWatchlistCreate){
		this.intMaxNoOfWatchlistCreate = intMaxNoOfWatchlistCreate;
	}
	/* Watchlist Setting - Set Maximum No Of Watchlist Create - END */
	
	/* Watchlist Setting - Impose Max No of Watchlist Create - START */
	public boolean isImposeMaxWatchlistCreate(){
		//return this.isImposeMaxWatchlistCreate;
		return this.getIntMaxNoOfWatchlistCreate() > 0 ? true : false;
	}
	public void setImposeMaxWatchlistCreate(boolean isImposeMaxWatchlistCreate){
		this.isImposeMaxWatchlistCreate = isImposeMaxWatchlistCreate;
	}
	/* Watchlist Setting - Impose Max No of Watchlist Create - END */
	
	/* Watchlist Setting - Set Maximum No Of Stock Add Into Watchlist - START */
	public int getIntMaxNoOfStockAddToWatchlist(){
	 return this.intMaxNoOfStockAddToWatchlist;
	}
	public void setIntMaxNoOfStockAddToWatchlistd(int intMaxNoOfStockAddToWatchlist){
		this.intMaxNoOfStockAddToWatchlist = intMaxNoOfStockAddToWatchlist;
	}
	/* Watchlist Setting - Set Maximum No Of Stock Add Into Watchlist - END */
	
	/* Watchlist Setting - Impose Max No of stock added into Watchlist - START */
	public boolean isImposeMaxStockAddToWatchlist(){
		return this.getIntMaxNoOfStockAddToWatchlist() > 0 ? true : false;
	}
	public void setImposeMaxStockAddToWatchlist(boolean isImposeMaxStockAddToWatchlist){
		this.isImposeMaxStockAddToWatchlist = isImposeMaxStockAddToWatchlist;
	}
	/* Watchlist Setting - Impose Max No of stock added into Watchlist - END */		
	
	public boolean isShowDMAAgreement() {
		return isShowDMAAgreement;
	}

	public void setShowDMAAgreement(boolean isShowDMAAgreement) {
		this.isShowDMAAgreement = isShowDMAAgreement;
	}

	public String getDMAWebViewURL() {
		return DMAWebViewURL;
	}

	public void setDMAWebViewURL(String dMAWebViewURL) {
		DMAWebViewURL = dMAWebViewURL;
	}
	
	public boolean isHasRestrictionToViewMY() {
		return hasRestrictionToViewMY;
	}

	public void setHasRestrictionToViewMY(boolean hasRestrictionToViewMY) {
		this.hasRestrictionToViewMY = hasRestrictionToViewMY;
	}
	
	public boolean isSupportATPLoadBalance() {
		return isSupportATPLoadBalance;
	}

	public void setSupportATPLoadBalance(boolean isSupportATPLoadBalance) {
		this.isSupportATPLoadBalance = isSupportATPLoadBalance;
	}
	
	public String getCurrencyRateLabel() {
		return currencyRateLabel;
	}

	public void setCurrencyRateLabel(String currencyRateLabel) {
		this.currencyRateLabel = currencyRateLabel;
	}
	
	public String getMinVersion() {
		return minVersion;
	}

	public void setMinVersion(String minVersion) {
		this.minVersion = minVersion;
	}

	public String getStrAppRequirement() {
		return strAppRequirement;
	}

	public void setStrAppRequirement(String strAppRequirement) {
		this.strAppRequirement = strAppRequirement;
	}

	public ArrayList<String> getFilterShowMarketArray() {
		return filterShowMarketArray;
	}

	public void setFilterShowMarketArray(ArrayList<String> filterShowMarketArray) {
		this.filterShowMarketArray = filterShowMarketArray;
	}
	
	public void addFilterShowMarketArray(String strMarketCode){
		this.filterShowMarketArray.add(strMarketCode);
	}
	
	public String getHistoricalChartServer() {
		return historicalChartServer;
	}

	public void setHistoricalChartServer(String historicalChartServer) {
		this.historicalChartServer = historicalChartServer;
	}

	public String getIntradayChartServer() {
		return intradayChartServer;
	}

	public void setIntradayChartServer(String intradayChartServer) {
		this.intradayChartServer = intradayChartServer;
	}
	
	public boolean isMsgPromptForStopNIfTouchOrdType() {
		return msgPromptForStopNIfTouchOrdType;
	}

	public void setMsgPromptForStopNIfTouchOrdType(
			boolean msgPromptForStopNIfTouchOrdType) {
		this.msgPromptForStopNIfTouchOrdType = msgPromptForStopNIfTouchOrdType;
	}
	

	public String getInteractiveChartServer() {
		return interactiveChartServer;
	}
	
	public void setInteractiveChartServer(String interactiveChartServer) {
		this.interactiveChartServer = interactiveChartServer;
	}
	
	public String getTradeValueLabelOnQuoteScreen() {
		return tradeValueLabelOnQuoteScreen;
	}

	public void setTradeValueLabelOnQuoteScreen(String tradeValueLabelOnQuoteScreen) {
		this.tradeValueLabelOnQuoteScreen = tradeValueLabelOnQuoteScreen;
	}

	public String getTradedAtLabelOnTimeNSale() {
		return tradedAtLabelOnTimeNSale;
	}

	public void setTradedAtLabelOnTimeNSale(String tradedAtLabelOnTimeNSale) {
		this.tradedAtLabelOnTimeNSale = tradedAtLabelOnTimeNSale;
	}
	
	public String getFundamentalNewsServer() {
		return fundamentalNewsServer;
	}

	public void setFundamentalNewsServer(String fundamentalNewsServer) {
		this.fundamentalNewsServer = fundamentalNewsServer;
	}

	public String getFundamentalSourceCode() {
		return fundamentalSourceCode;
	}

	public void setFundamentalSourceCode(String fundamentalSourceCode) {
		this.fundamentalSourceCode = fundamentalSourceCode;
	}

	public String getFundamentalNewsLabel() {
		return fundamentalNewsLabel;
	}

	public void setFundamentalNewsLabel(String fundamentalNewsLabel) {
		this.fundamentalNewsLabel = fundamentalNewsLabel;
	}
	
	public String getFundamentalDefaultReportCode() {
		return fundamentalDefaultReportCode;
	}

	public void setFundamentalDefaultReportCode(String fundamentalDefaultReportCode) {
		this.fundamentalDefaultReportCode = fundamentalDefaultReportCode;
	}
	
	public boolean getisInteractiveChart() {
		return isInteractiveChart;
	}

	public void setInteractiveChart(boolean isInteractiveChart) {
		this.isInteractiveChart = isInteractiveChart;
	}
	
	public String getChartServer() {
		return chartServer;
	}

	public void setChartServer(String chartServer) {
		this.chartServer = chartServer;
	}
	
	public boolean isEnableStockAlert() {
		return isEnableStockAlert;
	}

	public void setEnableStockAlert(boolean isEnableStockAlert) {
		this.isEnableStockAlert = isEnableStockAlert;
	}

	public int getOrderHistoryDuration() {
		return orderHistoryDuration;
	}

	public void setOrderHistoryDuration(int orderHistoryDuration) {
		this.orderHistoryDuration = orderHistoryDuration;
	}

	public ArrayList<String> getLstSectorNoTrading() {
		return lstSectorNoTrading;
	}

	public void setLstSectorNoTrading(ArrayList<String> lstSectorNoTrading) {
		this.lstSectorNoTrading = lstSectorNoTrading;
	}
	
	public boolean isHasBottomTextOnLoginPage() {
		return hasBottomTextOnLoginPage;
	}

	public void setHasBottomTextOnLoginPage(boolean hasBottomTextOnLoginPage) {
		this.hasBottomTextOnLoginPage = hasBottomTextOnLoginPage;
	}
	
	public boolean isArchiveNews() {
		return isArchiveNews;
	}

	public void setArchiveNews(boolean isArchiveNews) {
		this.isArchiveNews = isArchiveNews;
	}

	public boolean isEnableQRLogin() {
		return isEnableQRLogin;
	}

	public void setEnableQRLogin(boolean isEnableQRLogin) {
		this.isEnableQRLogin = isEnableQRLogin;
	}

	public boolean isEnableScreener() {
		return isEnableScreener;
	}

	public void setEnableScreener(boolean isEnableScreener) {
		this.isEnableScreener = isEnableScreener;
	}
	
	public String getTheScreenerUrl() {
		return theScreenerUrl;
	}

	public void setTheScreenerUrl(String theScreenerUrl) {
		this.theScreenerUrl = theScreenerUrl;
	}
	
	public String getChartType() {
		return chartType;
	}

	public void setChartType(String chartType) {
		this.chartType = chartType;
	}

	public String getNewChartServer() {
		return newChartServer;
	}

	public void setNewChartServer(String newChartServer) {
		this.newChartServer = newChartServer;
	}
	
	public String getArchiveNewsUrl(){
		return this.archiveNewsUrl;
	}
	public void setArchiveNewsUrl(String archiveNewsUrl){
		this.archiveNewsUrl = archiveNewsUrl;
	}
	
	public boolean isTutorialAvailable() {
		return isTutorialAvailable;
	}

	public void setisTutorialAvalable(boolean isTutorialAvailable) {
		this.isTutorialAvailable = isTutorialAvailable;
	}
	
	public String getStockAlertPubKeyUrl() {
		return stockAlertPublicKey;
	}

	public void setStockAlertPublicKey(String stockAlertPublicKey) {
		this.stockAlertPublicKey = stockAlertPublicKey;
	}
	
	public String getStockAlertJWT() {
		return stockAlertJWT;
	}

	public void setStockAlertJWT(String stockAlertJWT) {
		this.stockAlertJWT = stockAlertJWT;
	}
	
	public String getRegisterPushLink() {
		return registerPushLink;
	}

	public void setRegisterPushLink(String registerPushLink) {
		this.registerPushLink = registerPushLink;
	}
	
	public String getStockAlertUrl() {
		return stockAlertUrl;
	}

	public void setStockAlertUrl(String stockAlertUrl) {
		this.stockAlertUrl = stockAlertUrl;
	}
 
	public boolean isATPServerSSL() {
		return isATPServerSSL;
	}

	public void setATPServerSSL(boolean isATPServerSSL) {
		this.isATPServerSSL = isATPServerSSL;
	}
	
	public String getStkAlertDisclaimer() {
		return stkAlertDisclaimer;
	}

	public void setStkAlertDisclaimer(String stkAlertDisclaimer) {
		this.stkAlertDisclaimer = stkAlertDisclaimer;
	}
	
	public boolean isEnableIBillionaire() {
		return isEnableIBillionaire;
	}

	public void setEnableIBillionaire(boolean isEnableIBillionaire) {
		this.isEnableIBillionaire = isEnableIBillionaire;
	}
	
	public List<String> getArrExchgSupportModulus() {
		return arrExchgSupportModulus;
	}

	public void setArrExchgSupportModulus(List<String> arrExchgSupportModulus) {
		this.arrExchgSupportModulus = arrExchgSupportModulus;
	}
	
	public List<String> getLstExchgShowPrivateOrder() {
		return lstExchgShowPrivateOrder;
	}

	public void setLstExchgShowPrivateOrder(List<String> lstExchgShowPrivateOrder) {
		this.lstExchgShowPrivateOrder = lstExchgShowPrivateOrder;
	}
	
	public List<String> getArrExchgIBillionaire() {
		return arrExchgIBillionaire;
	}

	public void setArrExchgIBillionaire(List<String> arrExchgIBillionaire) {
		this.arrExchgIBillionaire = arrExchgIBillionaire;
	}
	
	public String getNewFundamentalSourceCode() {
		return newFundamentalSourceCode;
	}

	public void setNewFundamentalSourceCode(String newFundamentalSourceCode) {
		this.newFundamentalSourceCode = newFundamentalSourceCode;
	}

	public String getNewFundamentalNewsServer() {
		return newFundamentalNewsServer;
	}

	public void setNewFundamentalNewsServer(String newFundamentalNewsServer) {
		this.newFundamentalNewsServer = newFundamentalNewsServer;
	}

	public String getNewFundamentalNewsLabel() {
		return newFundamentalNewsLabel;
	}

	public void setNewFundamentalNewsLabel(String newFundamentalNewsLabel) {
		this.newFundamentalNewsLabel = newFundamentalNewsLabel;
	}
	
	public boolean isDtiEnabled() {
		return isDtiEnabled;
	}

	public void setDtiEnabled(boolean isDtiEnabled) {
		this.isDtiEnabled = isDtiEnabled;
	}

	public String getDtiUrl() {
		return DtiUrl;
	}

	public void setDtiUrl(String dtiUrl) {
		DtiUrl = dtiUrl;
	}

	public ArrayList<String> getAlDtiDisclaimer() {
		return alDtiDisclaimer;
	}

	public void setAlDtiDisclaimer(ArrayList<String> alDtiDisclaimer) {
		this.alDtiDisclaimer = alDtiDisclaimer;
	}
	
	public String getDtiApiUrl() {
		return DtiApiUrl;
	}

	public void setDtiApiUrl(String dtiApiUrl) {
		DtiApiUrl = dtiApiUrl;
	}

	public void setShowDtiPopUp(boolean showDtiPopUp) {
		isShowDtiPopUp = showDtiPopUp;
	}

	public boolean isShowDtiPopUp() {
		return isShowDtiPopUp;
	}

	public String getMoversLabelOnQuoteScreen() {
		return moversLabelOnQuoteScreen;
	}

	public void setMoversLabelOnQuoteScreen(String moversLabelOnQuoteScreen) {
		this.moversLabelOnQuoteScreen = moversLabelOnQuoteScreen;
	}
	
	public int getMaxIdleTime() {
		return maxIdleTime;
	}

	public void setMaxIdleTime(int maxIdleTime) {
		this.maxIdleTime = maxIdleTime;
	}
	
	public String getBrokerCode() {
		return brokerCode;
	}

	public void setBrokerCode(String brokerCode) {
		this.brokerCode = brokerCode;
	}
	
	public boolean isTouchIDEnabled() {
		return isTouchIDEnabled;
	}

	public void setTouchIDEnabled(boolean isTouchIDEnabled) {
		this.isTouchIDEnabled = isTouchIDEnabled;
	}

	public boolean isSecondLvlSecurityEnabled() {
		return isSecondLvlSecurityEnabled;
	}

	public void setSecondLvlSecurityEnabled(boolean isSecondLvlSecurityEnabled) {
		this.isSecondLvlSecurityEnabled = isSecondLvlSecurityEnabled;
	}
	
	public boolean isShowTopAtLactp() {
		return isShowTopAtLactp;
	}

	public void setShowTopAtLactp(boolean isShowTopAtLactp) {
		this.isShowTopAtLactp = isShowTopAtLactp;
	}
	
	public boolean isIBillionaireMenuEnabled() {
		return isIBillionaireMenuEnabled;
	}

	public void setIBillionaireMenuEnabled(boolean isIBillionaireMenuEnabled) {
		this.isIBillionaireMenuEnabled = isIBillionaireMenuEnabled;
	}
	
	public boolean isEnableElasticNews() {
		return isEnableElasticNews;
	}

	public void setEnableElasticNews(boolean isEnableElasticNews) {
		this.isEnableElasticNews = isEnableElasticNews;
	}

	public boolean isEnableContactUs() {
		return isEnableContactUs;
	}

	public void setEnableContactUs(boolean isEnableContactUs) {
		this.isEnableContactUs = isEnableContactUs;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	
	public boolean isEnableContraAlert() {
		return isEnableContraAlert;
	}

	public void setEnableContraAlert(boolean isEnableContraAlert) {
		this.isEnableContraAlert = isEnableContraAlert;
	}
	
	public boolean isEnableSgxAppIntegration() {
		return isEnableSgxAppIntegration;
	}

	public void setEnableSgxAppIntegration(boolean isEnableSgxAppIntegration) {
		this.isEnableSgxAppIntegration = isEnableSgxAppIntegration;
	}

	public String getElasticResearchReportsUrl() {
		return elasticResearchReportsUrl;
	}

	public void setElasticResearchReportsUrl(String elasticResearchReportsUrl) {
		this.elasticResearchReportsUrl = elasticResearchReportsUrl;
	}

	public String getElasticReuterNewsUrl() {
		return elasticReuterNewsUrl;
	}

	public void setElasticReuterNewsUrl(String elasticReuterNewsUrl) {
		this.elasticReuterNewsUrl = elasticReuterNewsUrl;
	}

	public boolean isShowResearchReportNote() {
		return isShowResearchReportNote;
	}

	public void setShowResearchReportNote(boolean showResearchReportNote) {
		isShowResearchReportNote = showResearchReportNote;
	}

	public void setShowContactUsOnLoginPage(boolean showContactUsOnLoginPage) {
		isShowContactUsOnLoginPage = showContactUsOnLoginPage;
	}

	public boolean isShowContactUsOnLoginPage() {
		return isShowContactUsOnLoginPage;
	}

	public String getPhoneContact() {
		return phoneContact;
	}

	public void setPhoneContact(String phoneContact) {
		this.phoneContact = phoneContact;
	}

	public String getEmailContact() {
		return emailContact;
	}

	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}

	public String getStockAlertPublicKey() {
		return stockAlertPublicKey;
	}

	public void setStockAlertBHCode(String stockAlertBHCode) {
		this.stockAlertBHCode = stockAlertBHCode;
	}

	public String getStockAlertBHCode() {
		return stockAlertBHCode;
	}

	public void setShowPortfolioDelayFeed(boolean showPortfolioDelayFeed) {
		isShowPortfolioDelayFeed = showPortfolioDelayFeed;
	}

	public boolean isShowPortfolioDelayFeed() {
		return isShowPortfolioDelayFeed;
	}

	public void setLstPortfolioDelayFeedExchg(ArrayList<String> lstPortfolioDelayFeedExchg) {
		this.lstPortfolioDelayFeedExchg = lstPortfolioDelayFeedExchg;
	}

	public ArrayList<String> getLstPortfolioDelayFeedExchg() {
		return lstPortfolioDelayFeedExchg;
	}

	public void setLinkIBWebview(boolean linkIBWebview) {
		isLinkIBWebview = linkIBWebview;
	}

	public boolean isLinkIBWebview() {
		return isLinkIBWebview;
	}

	public void setiBMainUrl(String iBMainUrl) {
		this.iBMainUrl = iBMainUrl;
	}

	public String getiBMainUrl() {
		return iBMainUrl;
	}

	public void setRequireLoginTcPlusToChangeHint(boolean requireLoginTcPlusToChangeHint) {
		isRequireLoginTcPlusToChangeHint = requireLoginTcPlusToChangeHint;
	}

	public boolean isRequireLoginTcPlusToChangeHint() {
		return isRequireLoginTcPlusToChangeHint;
	}

	public void setTcPlusLink(String tcPlusLink) {
		this.tcPlusLink = tcPlusLink;
	}

	public String getTcPlusLink() {
		return tcPlusLink;
	}

	public boolean isBlockRootedDevices() {
		return isBlockRootedDevices;
	}

	public void setBlockRootedDevices(boolean blockRootedDevices) {
		isBlockRootedDevices = blockRootedDevices;
	}

	private boolean getDefaultBooleanValue(String key){
		boolean value	= false;
		
		if(key == BHConfigConstants.isEnableTrade) 		  			value = this.enableTrade();
		else if(key == BHConfigConstants.hasRegistration) 			value = this.hasRegistration();
		else if(key == BHConfigConstants.hasSignInTermNCondition) 	value = this.hasSignInTermsNCondition();
		else if(key == BHConfigConstants.showPortfolioDisclaimer) 	value = this.showPortfolioDisclaimer();
		else if(key == BHConfigConstants.showPoweredByStatement) 	value = this.showPoweredByStatement();
		else if(key == BHConfigConstants.showRememberMe) 			value = this.showRememberMe();
		else if(key == BHConfigConstants.isCacheUsername) 			value = this.isCacheUsername();
		
		else if(key == BHConfigConstants.isConnectJavaQC) 			value = this.isConnectJavaQC();
		else if(key == BHConfigConstants.isGetNewKey) 				value = this.isGetNewKey();
		else if(key == BHConfigConstants.isEnableE2EEEncryption) 	value = this.isEnableE2EEEncryption();
		else if(key == BHConfigConstants.isEnableEncryption) 		value = this.isEnableEncryption();
		else if(key == BHConfigConstants.isEnablePinFeatures) 		value = this.isEnablePinFeatures();
		else if(key == BHConfigConstants.isEnablePasswordFeatures) 	value = this.isEnablePasswordFeatures();
		else if(key == BHConfigConstants.isMultilevelMarketDepth) 	value = this.isMultilevelMarketDepth();
		else if(key == BHConfigConstants.isShowSkipConfirmation) 	value = this.isShowSkipConfirmation();
		else if(key == BHConfigConstants.isShowShortSell) 			value = this.isShowShortSell();
		
		else if(key == BHConfigConstants.isImposeMaxLenOnQty) 		value = this.isImposeMaxLenOnQty();
		else if(key == BHConfigConstants.isShowLimitAtOrderPad) 	value = this.isShowLimitAtOrderPad();
		else if(key == BHConfigConstants.isShowLimitAtPortfolio) 	value = this.isShowLimitAtPortfolio();
		else if(key == BHConfigConstants.isShowLimitAtOrderBook) 	value = this.isShowLimitAtOrderBook();
		else if(key == BHConfigConstants.isShowDMAAgreement)		value = this.isShowDMAAgreement();
		else if(key == BHConfigConstants.RestrictionToViewMY)		value = this.isHasRestrictionToViewMY();
		else if(key == BHConfigConstants.isSupportATPLoadBalance)	value = this.isSupportATPLoadBalance();
		else if(key == BHConfigConstants.msgPromptForStopNIfTouchOrdType)	value = this.isMsgPromptForStopNIfTouchOrdType();
		else value= false;	//if the key is not defined, just return false
		
		return value;
	}
	
	private String getStringFromMap(Map<String, Object> properties, String key){
		Object obj	= properties.get(key);
		
		return obj.toString();
	}
	
	private Boolean getBooleanFromMap(Map<String, Object> properties, String key){
		Object obj	= properties.get(key);
		
		if(obj == null) return getDefaultBooleanValue(key);
		else return Boolean.parseBoolean(obj.toString());
	}
	
	private int getIntFromMap(Map<String, Object> properties, String key){
		Object obj	= properties.get(key);
		
		return Integer.parseInt(obj.toString());
	}
	
	private int getImageResIdFromMap(Context c, Map<String, Object> properties, String key){
		int resId = 0;
		Object obj = properties.get(key);
		if(obj != null){
			resId = c.getResources().getIdentifier(obj.toString(), "drawable", c.getApplicationContext().getPackageName());
		}
		
		return resId;
	}
	
	@SuppressWarnings("unchecked")
	public void initialize(String mPackageName, Context c){
		System.out.println("Initializing BrokerPlistConfigBean...");
		
		Map<String, Object> properties = null;
		SharedPreferences sharedPref = c.getSharedPreferences(AppConstants.DATA_STORE, c.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		
		String current_plistFileName = PlistFileUtil.getPlistFileNameFormat(mPackageName);
		File file = new File(SplashScreenActivity.plistPath, current_plistFileName);
		
		AppConstants.hasMultiLogin = sharedPref.getBoolean(PreferenceConstants.HAS_MULTI_LOGIN, false);
		
		if(!file.exists()){
			//by right, when it reach here, all latest plist Files should have been downloaded to /data/data/PACKAGENAME/files/plist already
		} else {
			
			properties = PlistFileUtil.readPlistFile(SplashScreenActivity.plistPath, current_plistFileName);
			
			//save version into SharedPreferences
			
			Object m_version = properties.get(BHConfigConstants.version);
			if(m_version != null) {
				
				//if(sharedPref.contains(mPackageName)) editor.remove(mPackageName);	//no need, will overwrite automatically
				editor.putString(mPackageName, m_version.toString());
				editor.commit();
			}
			
			editor.putString(PreferenceConstants.LAST_USED_PKG, mPackageName);
			editor.commit();
			
			//other parameters
			this.setStrBrokerHouseBtnName(getStringFromMap(properties, BHConfigConstants.strBrokerHouseBtnName));
			this.setStrAnnouncementURL(getStringFromMap(properties, BHConfigConstants.strAnnouncementURL));
			
			//int intBrokerHouseBtnImg		= R.drawable.btnicon_broker_amsec;
			this.setIntBrokerHouseBtnImg(getImageResIdFromMap(c, properties, BHConfigConstants.intBrokerHouseBtnImg));
			
			//int intLoginBgImg				= R.drawable.loginpage_bg;
			this.setIntLoginBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intLoginBgImg));
			
			//int intLoginBtnBgImg			= R.drawable.loginbutton_bg;
			this.setIntLoginBtnBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intLoginBtnBgImg));
			
			//int intTextColor				= Color.WHITE;
			Object m_intTextColor = properties.get(BHConfigConstants.intTextColor);
			if(m_intTextColor != null){
				this.setIntTextColor(Color.parseColor(m_intTextColor.toString()));
			}
			
			//int intLinkTextColor			= Color.WHITE;
			Object m_intLinkTextColor = properties.get(BHConfigConstants.intLinkTextColor);
			if(m_intLinkTextColor != null){
				this.setIntLinkTextColor(Color.parseColor(m_intLinkTextColor.toString()));
			}
			
			//int intTopNavBarBgImg			= R.drawable.top_nav_bar;
			this.setIntTopNavBarBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intTopNavBarBgImg));
			
			//int intTopNavBarBtnBgImg		= R.drawable.top_nav_bar_btn;
			this.setIntTopNavBarBtnBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intTopNavBarBtnBgImg));
			
			//int intAccSettingMenuIconBgImg	= R.drawable.menuicon_acc_setting;
			this.setIntAccSettingMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intAccSettingMenuIconBgImg));
			
			//int intExchgMenuIconBgImg		= R.drawable.menuicon_exchg;
			this.setIntExchgMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intExchgMenuIconBgImg));
			
			//int intHomeMenuIconBgImg		= R.drawable.menuicon_home;
			this.setIntHomeMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intHomeMenuIconBgImg));
			
			//int intIndiciesMenuIconBgImg	= R.drawable.menuicon_indices;
			this.setIntIndiciesMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intIndiciesMenuIconBgImg));
			
			//int intLogoutMenuIconBgImg		= R.drawable.menuicon_logout;
			this.setIntLogoutMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intLogoutMenuIconBgImg));
			
			//int intNewsMenuIconBgImg		= R.drawable.menuicon_news;
			this.setIntNewsMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intNewsMenuIconBgImg));
			
			//int intOrdBookMenuIconBgImg		= R.drawable.menuicon_ord_book;
			this.setIntOrdBookMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intOrdBookMenuIconBgImg));
			
			//int intPortfolioMenuIconBgImg	= R.drawable.menuicon_portfolio;
			this.setIntPortfolioMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intPortfolioMenuIconBgImg));
			
			//int intSearchMenuIconBgImg		= R.drawable.menuicon_search;
			this.setIntSearchMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intSearchMenuIconBgImg));
			
			//int intStkInfoMenuIconBgImg		= R.drawable.menuicon_stkinfo;
			this.setIntStkInfoMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intStkInfoMenuIconBgImg));
			
			//int intMktSumMenuIconBgImg		= R.drawable.menuicon_summary;
			this.setIntMktSumMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intMktSumMenuIconBgImg));
			
			//int intTradeMenuIconBgImg		= R.drawable.menuicon_trade;
			this.setIntTradeMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intTradeMenuIconBgImg));
			
			//int intWatchlistMenuIconBgImg	= R.drawable.menuicon_watchlist;
			this.setIntWatchlistMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intWatchlistMenuIconBgImg));
			
			//int intBgImg					= R.drawable.setting_bg;
			this.setIntBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intBgImg));
			
			//int intAnnouncementMenuIconBgImg	= 0;
			this.setIntAnnouncementMenuIconBgImg(getImageResIdFromMap(c, properties, BHConfigConstants.intAnnouncementMenuIconBgImg));
			
			this.isEnableTrade(getBooleanFromMap(properties, BHConfigConstants.isEnableTrade));
			this.isHasRegistration(getBooleanFromMap(properties, BHConfigConstants.hasRegistration));
			this.setHasSignInTermsNCondition(getBooleanFromMap(properties, BHConfigConstants.hasSignInTermNCondition));
			this.isShowPortfolioDisclaimer(getBooleanFromMap(properties, BHConfigConstants.showPortfolioDisclaimer));
			this.isShowPoweredByStatement(getBooleanFromMap(properties, BHConfigConstants.showPoweredByStatement));
			this.isShowRememberMe(getBooleanFromMap(properties, BHConfigConstants.showRememberMe));
			this.setIsCacheUsername(getBooleanFromMap(properties, BHConfigConstants.isCacheUsername));
			this.setStrPortfolioDisclaimerUrl(getStringFromMap(properties, BHConfigConstants.strPortfolioDisclaimerUrl));
			this.setStrRiskDisclosureStatementUrl(getStringFromMap(properties, BHConfigConstants.strRiskDisclosureStatementUrl));
			this.setStrLMSSponsor(getStringFromMap(properties, BHConfigConstants.strLMSSponsor));
			this.setStrLMSServer(getStringFromMap(properties, BHConfigConstants.strLMSServer));
			this.setStrLMSProduct(getStringFromMap(properties, BHConfigConstants.strLMSProduct));
			this.setStrLMSSubscriptionUrl(getStringFromMap(properties, BHConfigConstants.strLMSSubscriptionUrl));
			this.setStrLMSSubscriptionStatusUrl(getStringFromMap(properties, BHConfigConstants.strLMSSubscriptionStatusUrl));
			this.setStrAtpIP(getStringFromMap(properties, BHConfigConstants.strAtpIP));
			this.setIntAtpPort(getIntFromMap(properties, BHConfigConstants.intAtpPort));
			
			this.setStrNewsIP(getStringFromMap(properties, BHConfigConstants.strNewsIP));
			//this.setStrChartIP(getStringFromMap(properties, BHConfigConstants.strChartIP));
			this.setStrAppName(getStringFromMap(properties, BHConfigConstants.strAppName));
			this.setStrAppIntro(getStringFromMap(properties, BHConfigConstants.strAppIntro));
			this.setStrAppConclude(getStringFromMap(properties, BHConfigConstants.strAppConclude));
			this.setStrKeyFeaturesTitle(getStringFromMap(properties, BHConfigConstants.strKeyFeaturesTitle));
			
			ArrayList<String> m_alKeyFeatures = (ArrayList<String>) properties.get(BHConfigConstants.alKeyFeatures);
			if(m_alKeyFeatures != null) 	this.setALKeyFeatures(m_alKeyFeatures);
			
			ArrayList<String> m_alTermsNConditions		= (ArrayList<String>) properties.get(BHConfigConstants.alTermsNConditions);
			if(m_alTermsNConditions != null) 	this.setALTermsAndConditions(m_alTermsNConditions);
			
			ArrayList<String> m_alTermsOfService		= (ArrayList<String>) properties.get(BHConfigConstants.alTermsOfService);
			if(m_alTermsOfService != null) 	this.setALTermsOfService(m_alTermsOfService);
			
			ArrayList<String> m_alPrivacyPolicy			= (ArrayList<String>) properties.get(BHConfigConstants.alPrivacyPolicy);
			if(m_alPrivacyPolicy != null) 	this.setALPrivacyPolicy(m_alPrivacyPolicy);
			
			ArrayList<String> m_alDisclaimerOfWarranty	= (ArrayList<String>) properties.get(BHConfigConstants.alDisclaimerOfWarranty);
			if(m_alDisclaimerOfWarranty != null) 	this.setALDisclaimerOfWarranty(m_alDisclaimerOfWarranty);
			
			this.isAutoFillTradePrice(getBooleanFromMap(properties, BHConfigConstants.isAutoFillTradePrice));
			this.isAutoFillTradeQuantity(getBooleanFromMap(properties, BHConfigConstants.isAutoFillTradeQuantity));
			this.setIntCustomTradeQuantity(getIntFromMap(properties, BHConfigConstants.intCustomTradeQuantity));
			AppConstants.DEFAULT_TRADE_QTY_TYPE	=	getIntFromMap(properties, BHConfigConstants.defaultIntTradeQtyMeasurement);	//Added by Thinzar, Change_Request-20140901, ReqNo. 2
			this.setIntTradeQtyMeasurement(getIntFromMap(properties, BHConfigConstants.intTradeQtyMeasurement));
			this.setStrDefaultPaymentType(getStringFromMap(properties, BHConfigConstants.strDefaultPaymentType));
			this.setStrDefaultCurrencyType(getStringFromMap(properties, BHConfigConstants.strDefaultCurrencyType));
			this.setStrBaseATPExchangeCode(getStringFromMap(properties, BHConfigConstants.strBaseATPExchangeCode));
			this.setStrBaseQCExchangeCode(getStringFromMap(properties, BHConfigConstants.strBaseQCExchangeCode));
			
			ArrayList<String> m_lstPymtTypeToBeFilter		= (ArrayList<String>) properties.get(BHConfigConstants.lstPymtTypeToBeFilter);
			if(m_lstPymtTypeToBeFilter != null) 	this.setLstPymtTypeToBeFilter(m_lstPymtTypeToBeFilter);
			
			this.setIntViewQtyMeasurement(getIntFromMap(properties, BHConfigConstants.intViewQtyMeasurement));
			this.setConnectJavaQC(getBooleanFromMap(properties, BHConfigConstants.isConnectJavaQC));
			this.setIsGetNewKey(getBooleanFromMap(properties, BHConfigConstants.isGetNewKey));
			this.setEnableE2EEEncryption(getBooleanFromMap(properties, BHConfigConstants.isEnableE2EEEncryption));
			this.setEnableEncryption(getBooleanFromMap(properties, BHConfigConstants.isEnableEncryption));
			this.setEnablePinFeatures(getBooleanFromMap(properties, BHConfigConstants.isEnablePinFeatures));
			this.setEnablePasswordFeatures(getBooleanFromMap(properties, BHConfigConstants.isEnablePasswordFeatures));
			this.setMultilevelMarketDepth(getBooleanFromMap(properties, BHConfigConstants.isMultilevelMarketDepth));
			this.setShowSkipConfirmation(getBooleanFromMap(properties, BHConfigConstants.isShowSkipConfirmation));
			this.setShowShortSell(getBooleanFromMap(properties, BHConfigConstants.isShowShortSell));
			
			ArrayList<String> m_lstExchgShowShortSell		= (ArrayList<String>) properties.get(BHConfigConstants.lstExchgShowShortSell);
			if(m_lstExchgShowShortSell != null) 	this.setLstExchgShowSortSell(m_lstExchgShowShortSell);
			
			this.setAboutURL(getStringFromMap(properties, BHConfigConstants.strAboutURL));
			this.setStrTermsOfServiceURL(getStringFromMap(properties, BHConfigConstants.strTermsOfServiceURL));
			this.setStrPrivacyPolicyURL(getStringFromMap(properties, BHConfigConstants.strPrivacyPolicyURL));
			this.setStrDisclaimerOfWarrantyURL(getStringFromMap(properties, BHConfigConstants.getStrDisclaimerOfWarrantyURL));
			this.setImposeMaxLenOnQty(getBooleanFromMap(properties, BHConfigConstants.isImposeMaxLenOnQty));
			this.setShowLimitAtOrderPad(getBooleanFromMap(properties, BHConfigConstants.isShowLimitAtOrderPad));
			this.setShowLimitAtPortfolio(getBooleanFromMap(properties, BHConfigConstants.isShowLimitAtPortfolio));
			this.setShowLimitAtOrderBook(getBooleanFromMap(properties, BHConfigConstants.isShowLimitAtOrderBook));
			this.setIntMaxNoOfWatchlistCreate(getIntFromMap(properties, BHConfigConstants.intMaxNoOfWatchlistCreate));
			this.setIntMaxNoOfStockAddToWatchlistd(getIntFromMap(properties, BHConfigConstants.intMaxNoOfStockAddToWatchlist));
			
			this.setShowDMAAgreement(getBooleanFromMap(properties, BHConfigConstants.isShowDMAAgreement));
			this.setDMAWebViewURL(getStringFromMap(properties, BHConfigConstants.DMAWebViewURL));
			
			//Added by Thinzar@20140902, Fixes_Request-20140820, ReqNo. 11
			this.setHasRestrictionToViewMY(getBooleanFromMap(properties, BHConfigConstants.RestrictionToViewMY));
			
			//Added by Thinzar@20140915, Change_Request-20140901, ReqNo. 6
			this.setSupportATPLoadBalance(getBooleanFromMap(properties, BHConfigConstants.isSupportATPLoadBalance));
			
			//Added by Thinzar@20140926, Fixes_Request-20140820, ReqNo.29
			if(properties.containsKey(BHConfigConstants.currencyRateLabel)) 
				this.setCurrencyRateLabel(getStringFromMap(properties, BHConfigConstants.currencyRateLabel));
			
			//Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 9, 10, 11 - START
			if(properties.containsKey(BHConfigConstants.minVersion)) 
				this.setMinVersion(getStringFromMap(properties, BHConfigConstants.minVersion));
			
			if(properties.containsKey(BHConfigConstants.strAppRequirement))
				this.setStrAppRequirement(getStringFromMap(properties, BHConfigConstants.strAppRequirement));
			
			ArrayList<String> m_filterShowMarketArray	= (ArrayList<String>) properties.get(BHConfigConstants.filterShowMarketArray);
			if(m_filterShowMarketArray != null) 	this.setFilterShowMarketArray(m_filterShowMarketArray);
			//Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 9, 10, 11 - END
			
			//Added by Thinzar@20141103, Change_Request-20140901, ReqNo. 14 - START
			if(properties.containsKey(BHConfigConstants.historicalChartServer))
				this.setHistoricalChartServer(getStringFromMap(properties, BHConfigConstants.historicalChartServer));
			
			if(properties.containsKey(BHConfigConstants.intradayChartServer))
				this.setIntradayChartServer(getStringFromMap(properties, BHConfigConstants.intradayChartServer));
			//Added by Thinzar@20141103, Change_Request-20140901, ReqNo. 14 - END	
			
			//Added by Thinzar@20141110, Change_Request-20141101, ReqNo. 2
			if(properties.containsKey(BHConfigConstants.msgPromptForStopNIfTouchOrdType))
				this.setMsgPromptForStopNIfTouchOrdType(getBooleanFromMap(properties, BHConfigConstants.msgPromptForStopNIfTouchOrdType));
			
			//Added by Thinzar@20141111, Change_Request-20141101, ReqNo. 4
			if(properties.containsKey(BHConfigConstants.tradeValueLabelOnQuoteScreen))
				this.setTradeValueLabelOnQuoteScreen(getStringFromMap(properties, BHConfigConstants.tradeValueLabelOnQuoteScreen));
			
			if(properties.containsKey(BHConfigConstants.tradedAtLabelOnTimeNSale))
				this.setTradedAtLabelOnTimeNSale(getStringFromMap(properties, BHConfigConstants.tradedAtLabelOnTimeNSale));
			
			//Added by Thinzar@20150522, Change_Request-20150401, ReqNo. 3
			if(properties.containsKey(BHConfigConstants.fundamentalNewsServer))
				this.setFundamentalNewsServer(getStringFromMap(properties, BHConfigConstants.fundamentalNewsServer));
			
			if(properties.containsKey(BHConfigConstants.fundamentalSourceCode))
				this.setFundamentalSourceCode(getStringFromMap(properties, BHConfigConstants.fundamentalSourceCode));
			
			if(properties.containsKey(BHConfigConstants.fundamentalNewsLabel))
				this.setFundamentalNewsLabel(getStringFromMap(properties, BHConfigConstants.fundamentalNewsLabel));
				
			if(properties.containsKey(BHConfigConstants.fundamentalDefaultReportCode))
				this.setFundamentalDefaultReportCode(getStringFromMap(properties, BHConfigConstants.fundamentalDefaultReportCode));
			
			//Added by diyana for interactive chart
			if(properties.containsKey(BHConfigConstants.strChartIP))
				this.setChartServer(getStringFromMap(properties, BHConfigConstants.strChartIP));
			
			if(properties.containsKey(BHConfigConstants.interactiveChart))
				this.setInteractiveChart(getBooleanFromMap(properties, BHConfigConstants.interactiveChart));
			
			if(properties.containsKey(BHConfigConstants.InteractiveChartServer))
				this.setInteractiveChartServer(getStringFromMap(properties, BHConfigConstants.InteractiveChartServer));
			
			//Added by Thinzar, Change_Request-20160101, ReqNo. 10
			if(properties.containsKey(BHConfigConstants.strIsEnableStockAlert))
				this.setEnableStockAlert(getBooleanFromMap(properties, BHConfigConstants.strIsEnableStockAlert));
			
			if(properties.containsKey(BHConfigConstants.strOrderHistoryDuration))
				this.setOrderHistoryDuration(getIntFromMap(properties, BHConfigConstants.strOrderHistoryDuration));
			
			if(properties.containsKey(BHConfigConstants.strLstSectorNoTrading)){
				ArrayList<String> m_lstSectorNoTrading		= (ArrayList<String>) properties.get(BHConfigConstants.strLstSectorNoTrading);
				if(m_lstSectorNoTrading != null) 	this.setLstSectorNoTrading(m_lstSectorNoTrading);	
			}
			
			if(properties.containsKey(BHConfigConstants.strHasBottomTextOnLoginPage))
				this.setHasBottomTextOnLoginPage(getBooleanFromMap(properties, BHConfigConstants.strHasBottomTextOnLoginPage));
			
			if(properties.containsKey(BHConfigConstants.strIsArchiveNews))
				this.setArchiveNews(getBooleanFromMap(properties, BHConfigConstants.strIsArchiveNews));
			
			if(properties.containsKey(BHConfigConstants.strArchiveNewsUrl))
				this.setArchiveNewsUrl(	getStringFromMap(properties, BHConfigConstants.strArchiveNewsUrl));
			
			if(properties.containsKey(BHConfigConstants.strIsEnableQRLogin))
				this.setEnableQRLogin(getBooleanFromMap(properties, BHConfigConstants.strIsEnableQRLogin));
			
			if(properties.containsKey(BHConfigConstants.strIsEnableScreener))
				this.setEnableScreener(getBooleanFromMap(properties, BHConfigConstants.strIsEnableScreener));
			
			if(properties.containsKey(BHConfigConstants.strTheScreenerUrl))
				this.setTheScreenerUrl(getStringFromMap(properties, BHConfigConstants.strTheScreenerUrl));
			
			if(properties.containsKey(BHConfigConstants.strChartType))
				this.setChartType(getStringFromMap(properties, BHConfigConstants.strChartType));
			
			if(properties.containsKey(BHConfigConstants.strNewChartServer))
				this.setNewChartServer(getStringFromMap(properties, BHConfigConstants.strNewChartServer));
			
			if(properties.containsKey(BHConfigConstants.strIsATPServerSSL))
				this.setATPServerSSL(getBooleanFromMap(properties, BHConfigConstants.strIsATPServerSSL));
			
			if(properties.containsKey(BHConfigConstants.isTutorialAvailable))
				this.setisTutorialAvalable(getBooleanFromMap(properties, BHConfigConstants.isTutorialAvailable));
			
			if(properties.containsKey(BHConfigConstants.stockAlertPublicKey))
				this.setStockAlertPublicKey(getStringFromMap(properties, BHConfigConstants.stockAlertPublicKey));
			
			if(properties.containsKey(BHConfigConstants.stockAlertJWT))
				this.setStockAlertJWT(getStringFromMap(properties, BHConfigConstants.stockAlertJWT));
			
			if(properties.containsKey(BHConfigConstants.registerPushLink))
				this.setRegisterPushLink(getStringFromMap(properties, BHConfigConstants.registerPushLink));
			
			if(properties.containsKey(BHConfigConstants.stockAlertUrl))
				this.setStockAlertUrl(getStringFromMap(properties, BHConfigConstants.stockAlertUrl));
			
			//Change_Request-20160722, ReqNo.7
			if(properties.containsKey(BHConfigConstants.strStkAlertDisclaimer))
				this.setStkAlertDisclaimer(getStringFromMap(properties, BHConfigConstants.strStkAlertDisclaimer));
			
			//Change_Request-20160722, ReqNo.8
			if(properties.containsKey(BHConfigConstants.strIsEnableIBillionaire))
				this.setEnableIBillionaire(getBooleanFromMap(properties, BHConfigConstants.strIsEnableIBillionaire));
		
			//Fixes_Request-20161101, ReqNo.3
			if(properties.containsKey(BHConfigConstants.strArrExchgSupportModulus)){
				ArrayList<String> m_arrExchgSupportModulus		= new ArrayList<String>();
				m_arrExchgSupportModulus						= (ArrayList<String>)properties.get(BHConfigConstants.strArrExchgSupportModulus);
				
				if(m_arrExchgSupportModulus.size() > 0)			this.setArrExchgSupportModulus(m_arrExchgSupportModulus);
			}
	
			//Change_Request-20161101, ReqNo.3
			if(properties.containsKey(BHConfigConstants.strLstExchgShowPrivateOrder)){
				ArrayList<String> m_lstExchgShowPrivateOrder	= new ArrayList<String>();
				m_lstExchgShowPrivateOrder						= (ArrayList<String>) properties.get(BHConfigConstants.strLstExchgShowPrivateOrder);
				if(m_lstExchgShowPrivateOrder.size() > 0) 	this.setLstExchgShowPrivateOrder(m_lstExchgShowPrivateOrder);
			}
			
			//Fixes_Request-20161101, ReqNo.4
			if(properties.containsKey(BHConfigConstants.strLstExchgIBillionaire)){
				ArrayList<String> m_lstExchgIBillionaire		= new ArrayList<String>();
				m_lstExchgIBillionaire							= (ArrayList<String>)properties.get(BHConfigConstants.strLstExchgIBillionaire);
				if(m_lstExchgIBillionaire.size() > 0)		this.setArrExchgIBillionaire(m_lstExchgIBillionaire);
			}
			
			//Fixes_Request-20161101, ReqNo.7
			AppConstants.orgATPIPFromConfigFile		= getStringFromMap(properties, BHConfigConstants.strAtpIP);
			AppConstants.orgATPPortFromConfigFile	= getIntFromMap(properties, BHConfigConstants.intAtpPort);
			
			//Fixes_Request-20161101, ReqNo.5 - overwrite ATP port with SSL port
			String currentAppVersion;
			try {
				currentAppVersion = c.getPackageManager().getPackageInfo(c.getPackageName(), 0).versionName;
				
				if(this.isATPServerSSL && isVersionSupportHttps(currentAppVersion)){
					AppConstants.isAppVersionSupportSSL	= true;
					
					if(properties.containsKey(BHConfigConstants.strIntAtpPortSSL)){
						this.setIntAtpPort(this.getIntFromMap(properties, BHConfigConstants.strIntAtpPortSSL));
						AppConstants.orgATPPortFromConfigFile	= getIntFromMap(properties, BHConfigConstants.strIntAtpPortSSL);	//Fixes_Request-20161101, ReqNo.7
					}else{
						this.setIntAtpPort(443);
						AppConstants.orgATPPortFromConfigFile	= 443;	//Fixes_Request-20161101, ReqNo.7
					}
				}
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//Change_Request-20170119, ReqNo.3
			if(properties.containsKey(BHConfigConstants.strNewFundamentalSourceCode)){
				this.setNewFundamentalSourceCode(getStringFromMap(properties, BHConfigConstants.strNewFundamentalSourceCode));
			}
			
			if(properties.containsKey(BHConfigConstants.strNewFundamentalNewsLabel)){
				this.setNewFundamentalNewsLabel(getStringFromMap(properties, BHConfigConstants.strNewFundamentalNewsLabel));
			}
			if(properties.containsKey(BHConfigConstants.strNewFundamentalNewsServer)){
				this.setNewFundamentalNewsServer(getStringFromMap(properties, BHConfigConstants.strNewFundamentalNewsServer));
			}
			
			//Change_Request-20170119, ReqNo.5
			if(properties.containsKey(BHConfigConstants.strIsDtiEnabled)){
				this.setDtiEnabled(getBooleanFromMap(properties, BHConfigConstants.strIsDtiEnabled));
			}

			if(properties.containsKey(BHConfigConstants.strDtiUrl)){
				this.setDtiUrl(getStringFromMap(properties, BHConfigConstants.strDtiUrl));
			}
			
			if(properties.containsKey(BHConfigConstants.strDtiApiUrl))
				this.setDtiApiUrl(getStringFromMap(properties, BHConfigConstants.strDtiApiUrl));
			
			if(properties.containsKey(BHConfigConstants.strDtiDisclaimer)){
				ArrayList<String> alDtiTerms		= (ArrayList<String>)properties.get(BHConfigConstants.strDtiDisclaimer);
				if(alDtiTerms != null) this.setAlDtiDisclaimer(alDtiTerms);
			}

			if(properties.containsKey(BHConfigConstants.strIsShowDtiPopUp))
				this.setShowDtiPopUp(getBooleanFromMap(properties, BHConfigConstants.strIsShowDtiPopUp));
			
			//Change_Request-20170119, ReqNo.6
			if(properties.containsKey(BHConfigConstants.strMoversLabelOnQuoteScreen)){
				this.setMoversLabelOnQuoteScreen(getStringFromMap(properties, BHConfigConstants.strMoversLabelOnQuoteScreen));
			}

			//Change_Request-20170119, ReqNo.8
			if(properties.containsKey(BHConfigConstants.strMaxIdleTime)){
				int maxIdleTimeInMiliSeconds	= getIntFromMap(properties, BHConfigConstants.strMaxIdleTime) * 1000; 
				this.setMaxIdleTime(maxIdleTimeInMiliSeconds);
			}
			
			//Change_Request-20170119, ReqNo.9
			if(properties.containsKey(BHConfigConstants.strBrokerCode)){
				this.setBrokerCode(getStringFromMap(properties, BHConfigConstants.strBrokerCode));
			}
			
			//Change_Request-20160722, ReqNo.8
			if(properties.containsKey(BHConfigConstants.strIsTouchIDEnabled))
				this.setTouchIDEnabled(getBooleanFromMap(properties, BHConfigConstants.strIsTouchIDEnabled));
			
			if(properties.containsKey(BHConfigConstants.strIsSecondLvlSecurityEnabled))
				this.setSecondLvlSecurityEnabled(getBooleanFromMap(properties, BHConfigConstants.strIsSecondLvlSecurityEnabled));
			
			//Change_Request-20160722, ReqNo.12
			if(properties.containsKey(BHConfigConstants.strIsShowTopAtLacp))
				this.setShowTopAtLactp(getBooleanFromMap(properties, BHConfigConstants.strIsShowTopAtLacp));
			
			//Change_Request-20170601, ReqNo.1
			if(properties.containsKey(BHConfigConstants.strIsIbillionaireMenuEnabled))
				this.setIBillionaireMenuEnabled(getBooleanFromMap(properties, BHConfigConstants.strIsIbillionaireMenuEnabled));
			
			//Change_Request-20170601, ReqNo.2
			if(properties.containsKey(BHConfigConstants.strIsEnableElasticNews))
				this.setEnableElasticNews(getBooleanFromMap(properties, BHConfigConstants.strIsEnableElasticNews));

			if(properties.containsKey(BHConfigConstants.strElasticResearchReportsUrl))
				this.setElasticResearchReportsUrl(getStringFromMap(properties, BHConfigConstants.strElasticResearchReportsUrl));

			if(properties.containsKey(BHConfigConstants.strElasticReuterNewsUrl))
				this.setElasticReuterNewsUrl(getStringFromMap(properties, BHConfigConstants.strElasticReuterNewsUrl));

			//Change_Request-20170601, ReqNo.3
			if(properties.containsKey(BHConfigConstants.strIsEnableContactUs))
				this.setEnableContactUs(getBooleanFromMap(properties, BHConfigConstants.strIsEnableContactUs));
			
			if(properties.containsKey(BHConfigConstants.strContactEmail))
				this.setContactEmail(getStringFromMap(properties, BHConfigConstants.strContactEmail));
			
			//Change_Request-20170601, ReqNo.6
			if(properties.containsKey(BHConfigConstants.strIsEnableContraAlert))
				this.setEnableContraAlert(getBooleanFromMap(properties, BHConfigConstants.strIsEnableContraAlert));
			
			//Change_Request-20170601, ReqNo.8
			if(properties.containsKey(BHConfigConstants.strEnableSgxAppIntegration))
				this.setEnableSgxAppIntegration(getBooleanFromMap(properties, BHConfigConstants.strEnableSgxAppIntegration));

			//Change_Request-20170601, ReqNo.13
			if(properties.containsKey(BHConfigConstants.strIsShowResearchReportNote))
				this.setShowResearchReportNote(getBooleanFromMap(properties, BHConfigConstants.strIsShowResearchReportNote));

			//Added for AmSec and AmFutures
			if(properties.containsKey(BHConfigConstants.strContactUsOnLoginPg))
				this.setShowContactUsOnLoginPage(getBooleanFromMap(properties, BHConfigConstants.strContactUsOnLoginPg));

			if(properties.containsKey(BHConfigConstants.strPhoneContact))
				this.setPhoneContact(getStringFromMap(properties, BHConfigConstants.strPhoneContact));

			if(properties.containsKey(BHConfigConstants.strEmailContact))
				this.setEmailContact(getStringFromMap(properties, BHConfigConstants.strEmailContact));

			//Change_Request-20170601, ReqNo.15
			if(properties.containsKey(BHConfigConstants.strStockAlertBHCode))
				this.setStockAlertBHCode(getStringFromMap(properties, BHConfigConstants.strStockAlertBHCode));

			//Change_Request-20170601, ReqNo.17
			if(properties.containsKey(BHConfigConstants.strIsShowPortfolioDelayFeed))
				this.setShowPortfolioDelayFeed(getBooleanFromMap(properties, BHConfigConstants.strIsShowPortfolioDelayFeed));

			if(properties.containsKey(BHConfigConstants.strLstPortfolioDelayFeedExchg)) {
				ArrayList<String> m_lstPortfolioDelayFeedExchg = (ArrayList<String>) properties.get(BHConfigConstants.strLstPortfolioDelayFeedExchg);
				if (m_lstPortfolioDelayFeedExchg != null)
					this.setLstPortfolioDelayFeedExchg(m_lstPortfolioDelayFeedExchg);
			}

			//Change_Request-2017-0601, ReqNo.
			if(properties.containsKey(BHConfigConstants.strIsLinkIBWebview))
				this.setLinkIBWebview(getBooleanFromMap(properties, BHConfigConstants.strIsLinkIBWebview));

			if(properties.containsKey(BHConfigConstants.strIBMainUrl))
				this.setiBMainUrl(getStringFromMap(properties, BHConfigConstants.strIBMainUrl));

			//Fixes_Request-20170701, ReqNo.18
			if(properties.containsKey(BHConfigConstants.strIsRequireLoginTcPlusToChangeHint))
				this.setRequireLoginTcPlusToChangeHint(getBooleanFromMap(properties, BHConfigConstants.strIsRequireLoginTcPlusToChangeHint));

			if(properties.containsKey(BHConfigConstants.strTcPlusLink))
				this.setTcPlusLink(getStringFromMap(properties, BHConfigConstants.strTcPlusLink));

			if(properties.containsKey(BHConfigConstants.strIsBlockRootedDevices))
				this.setBlockRootedDevices(getBooleanFromMap(properties, BHConfigConstants.strIsBlockRootedDevices));
		}
	}
	
	//Fixes_Request-20161101, ReqNo.5
	public static boolean isVersionSupportHttps(String currentVer) {
		final String MIN_VERSION_SUPPORT_HTTPS	= "2.2.0";
		
		String[] minVerArray = MIN_VERSION_SUPPORT_HTTPS.split("\\.");
		String[] currentVerArray = currentVer.split("\\.");

		for (int i = 0; i < currentVerArray.length; i++) {
			if (currentVerArray[i].compareTo(minVerArray[i]) > 0) {
				return true;
			} else if (currentVerArray[i].compareTo(minVerArray[i]) < 0) {
				return false;
			}
		}
		return true;
	}
}