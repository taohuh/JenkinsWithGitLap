package com.n2nconnect.android.stock.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TableRow.LayoutParams;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.activity.CheckPlistTask.CheckPlistTaskFinishedListener;
import com.n2nconnect.android.stock.model.Broker;
import com.n2nconnect.android.stock.util.PlistFileUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class SelectBrokerHouseActivity extends Activity implements CheckPlistTaskFinishedListener {
	private TableLayout tlBrokerHouseList;
	
	//Added by Thinzar@20140702, to move config files to server
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;
	String plistPath;
	String plistFileName;
	String mPackageName;
	Map<String, Object> properties;
	Object[] brokerObj;
	ArrayList<Broker> Brokers = new ArrayList<Broker>();
	String version;
	String imgPath;
	int screenWidth;
	int imgScaledWidth;
	//Added by Thinzar@20140702 - END
	
	private ProgressDialog dialog;
	private String appDataPath;
	private String selectedPackageName;
	private String selectedBrokerId; 					//Added by Thinzar, Change_Request-20141101, ReqNo.3
	private String selectedBrokerName;					//Added by Thinzar, Change_Request-20141101, ReqNo.10
	private boolean isShowBrokerListInTable	= false;	//Added by Thinzar, Change_Request-20141101, ReqNo.10
	private String sortBrokerListBy			= null;

	//Added by Thinzar@20141128, Change_Request-20141101, ReqNo. 10
	private LinearLayout brokerListLinearLayout;
	private LayoutInflater layoutInf;

	private ListView brokerHouseListView;

	private EditText txtSearchKeyword;

	private CustomListViewAdapter adapter;

	private LinkedHashMap<String, Integer> mapIndex;

	private ImageButton btnClearKeyword;
	
	private TextView btnBack;
	
	public void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.list_broker_house_screen);
			
			//Added by Thinzar@20140702 - to move config files to server
			dialog				= new ProgressDialog(this);
			appDataPath 		= getFilesDir().getPath();		// data/data/PACKAGE_NAME/files
			plistPath 			= appDataPath + "/" + PlistFileUtil.PLIST_FOLDER_NAME;
			imgPath 			= appDataPath + "/" + PlistFileUtil.ICONS_FOLDER_NAME;
			
			//Added by Thinzar@20141128, Change_Request-20141101, ReqNo. 10
			brokerListLinearLayout	= (LinearLayout)findViewById(R.id.brokerListLinearLayout);
			
			if(this.getIntent() != null){
				Bundle bundle = this.getIntent().getExtras();
				
				mPackageName = bundle.getString(ParamConstants.PARAM_PACKAGE_NAME);
			}
			
			//get display dimension - to set the size of icons
			Display display = getWindowManager().getDefaultDisplay(); 
			screenWidth = display.getWidth();  // deprecated
	
			btnBack	= (TextView)findViewById(R.id.txtBackToBrokerList);
			btnBack.setVisibility(View.INVISIBLE);
			
			btnBack.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					SelectBrokerHouseActivity.this.onBackPressed();
				}
			});

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//Added by Thinzar@20141106, Change_Request-20141101, ReqNo. 1 - START
	private ArrayList<Broker> readPlistGetBrokerList(String pkgName){
		
		ArrayList<Broker> brokers = new ArrayList<Broker>();
		
		plistFileName 		= PlistFileUtil.getPlistFileNameFormat(pkgName); 
		Log.d("TAG", "plistFull: " + plistPath + "/" + plistFileName);
		
		properties = PlistFileUtil.readPlistFile(plistPath, plistFileName);
		version = (String)properties.get("Version");
		
		//save "version" to sharedPref
		sharedPreferences 						= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		SharedPreferences.Editor editor 		= sharedPreferences.edit();
		editor.putString(pkgName, version);
		editor.commit();
		
		//Change_Request-20141101, ReqNo.10 - START
		if(properties.containsKey("ShowBrokerListInTable")) 
			isShowBrokerListInTable	= Boolean.parseBoolean(properties.get("ShowBrokerListInTable").toString());
		
		if(properties.containsKey("SortBrokerListBy"))
			sortBrokerListBy	= properties.get("SortBrokerListBy").toString();
		//Change_Request-20141101, ReqNo.10 - END
		
		ArrayList<?> brokerList = (ArrayList<?>)properties.get("BrokerList");
		brokerObj = brokerList.toArray();
		
		for(int k = 0; k < brokerObj.length; k ++){
			Map<?, ?> subMap = (Map<?, ?>)brokerObj[k];
			
			Broker b = new Broker();
			b.setAppName(subMap.get("AppName").toString());
			b.setIconName(subMap.get("IconName").toString());
			b.setIconLink(subMap.get("IconLink").toString());
			b.setPackageName(subMap.get("PlistLink").toString());
			if(subMap.containsKey("BrokerID")) 	b.setBrokerId(subMap.get("BrokerID").toString());	//Added by Thinzar, Change_Request-20141101, ReqNo. 3
			
			brokers.add(b);
		}
		
		//Sort the brokers
		if(sortBrokerListBy != null && sortBrokerListBy.equalsIgnoreCase("AppName")){
			Collections.sort(brokers, new Comparator<Broker>(){
				@Override
				public int compare(Broker b1, Broker b2) {
					return b1.getAppName().compareToIgnoreCase(b2.getAppName());
				}
			});
		}
		
		return brokers;
	}
	
	public void repaintBrokerHouseScreen(String pkgName){
		mPackageName	= pkgName;		//Added by Thinzar@20150108, to solve onBackPressed()
		Brokers	= this.readPlistGetBrokerList(pkgName);
		
		if(isShowBrokerListInTable){
			
			this.prepareBrokerHouseListView(Brokers);
		} else {
			
			this.prepareBrokerHouseGridView(Brokers);
		}
		
	}
	//Added by Thinzar@20141106, Change_Request-20141101, ReqNo. 1 - END
	
	public void onResume(){	
		super.onResume();
		System.out.println("onresume, SelectBrokerHouseActivity: mPackageName=" + mPackageName);
		repaintBrokerHouseScreen(mPackageName);
		
		//The following commented out by Thinzar@20140702 - to move config files to server
		/*
		this.prepareBrokerHouseListView(tlBrokerHouseList);
		List<String> arrBHB			= null;
		String[] strArrBrokerHouse	= null;
		
		try{
			super.onResume();
			
			arrBHB						= new ArrayList<String>();
			strArrBrokerHouse			= this.getResources().getStringArray(R.array.broker_house_bean_arrays);
			
			if( strArrBrokerHouse != null & strArrBrokerHouse.length > 0 ){
				// note : Filter to remove "" or null value 
				for(int i=0; i<strArrBrokerHouse.length; i++){
					if( strArrBrokerHouse[i] != null && strArrBrokerHouse[i].trim().length() > 0){
						arrBHB.add(strArrBrokerHouse[i]);
					}
				}
				
				// note : sort list 
				if(AppConstants.enumBrokerHouseListOrder != ENUM_BROKER_HOUSE_LIST_ORDER.RANDOM){
					Collections.sort(arrBHB, String.CASE_INSENSITIVE_ORDER);
					if(AppConstants.enumBrokerHouseListOrder == ENUM_BROKER_HOUSE_LIST_ORDER.DESCENDING){
						Collections.sort( arrBHB, Collections.reverseOrder() );
					}
				}
				
				prepareBrokerHouseListView(tlBrokerHouseList, arrBHB);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(arrBHB != null){
				arrBHB.clear();
				arrBHB = null;
			}
			
			if(strArrBrokerHouse != null)
				strArrBrokerHouse = null;
		}
		*/
	}
	
	//Added by Thinzar@20141127, Change_Request-20141101, ReqNo. 10 - START
	public void prepareBrokerHouseListView(ArrayList<Broker> Brokers){
		//TODO sort brokers
		//
		
		layoutInf	= getLayoutInflater();
		View v	= layoutInf.inflate(R.layout.list_broker_house_listview, null);
		brokerListLinearLayout.removeAllViews();
		brokerListLinearLayout.addView(v);
		
		brokerHouseListView				= (ListView) findViewById (R.id.brokerHouseListView);
		adapter	= new CustomListViewAdapter(this, Brokers);
		brokerHouseListView.setAdapter(adapter);
		
		txtSearchKeyword				= (EditText) findViewById(R.id.txtSearchKeyword);
		txtSearchKeyword.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable arg0) {
				String text	= txtSearchKeyword.getText().toString().toLowerCase(Locale.getDefault());
				adapter.filter(text);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				
			}
			
		});
		
		btnClearKeyword				= (ImageButton)findViewById(R.id.btnClearKeyword);
		btnClearKeyword.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				txtSearchKeyword.setText("");
			}
		});
		
		getIndexList(Brokers);
		displayIndex();
		
	}
	
	public class CustomListViewAdapter extends BaseAdapter {

		// Declare Variables
		Context mContext;
		LayoutInflater inflater;
		private ArrayList<Broker> lstBrokers = null;
		private ArrayList<Broker> lstTempBrokers;

		public CustomListViewAdapter(Context context, ArrayList<Broker> ListItemObjectlist) {
			mContext = context;
			this.lstBrokers = ListItemObjectlist;
			inflater = LayoutInflater.from(mContext);
			this.lstTempBrokers = new ArrayList<Broker>();
			this.lstTempBrokers.addAll(ListItemObjectlist);
		}

		public class ViewHolder {
			TextView txtValue;
		}

		@Override
		public int getCount() {
			return lstBrokers.size();
		}

		@Override
		public Broker getItem(int position) {
			return lstBrokers.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View view, ViewGroup parent) {
			final ViewHolder holder;
			if (view == null) {
				holder = new ViewHolder();
				view = inflater.inflate(R.layout.list_broker_house_listview_row, null);
				
				//Locate UI elements
				holder.txtValue 	= (TextView) view.findViewById(R.id.txtListItemBrokerName); 
				
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}
			// Set the results into TextViews
			holder.txtValue.setText(lstBrokers.get(position).getAppName());
			
			// Listen for ListView Item Click
			view.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Broker b = lstBrokers.get(position);
					selectedPackageName	= b.getPackageName();
					selectedBrokerId	= b.getBrokerId();
					selectedBrokerName	= b.getAppName();
					
					String[] parameters = {selectedPackageName, getPlistFileVersion(selectedPackageName), ParamConstants.LOGIN_ACTIVITY_TAG};
					new CheckPlistTask(SelectBrokerHouseActivity.this, dialog, SelectBrokerHouseActivity.this).execute(parameters);
				}
			});

			return view;
		}

		// Filter Class
		public void filter(String charText) {
			charText = charText.toLowerCase(Locale.getDefault());
			lstBrokers.clear();
			if (charText.length() == 0) {
				lstBrokers.addAll(lstTempBrokers);
			} else {
				for (Broker wp : lstTempBrokers) {
					if (wp.getAppName().toLowerCase(Locale.getDefault()).contains(charText)) {
						lstBrokers.add(wp);
					} else if(wp.getAppName().toLowerCase(Locale.getDefault())
							.contains(charText)) {
						lstBrokers.add(wp);
					}
				}
			}
			notifyDataSetChanged();
		}

	}
	
	private void getIndexList(ArrayList<Broker> brokerList) {
		mapIndex = new LinkedHashMap<String, Integer>();
		for (int i = 0; i < brokerList.size(); i++) {
			Broker b			= brokerList.get(i);
			String brokerName	= b.getAppName();
			String index = brokerName.substring(0, 1).toLowerCase();

			if (mapIndex.get(index) == null)
				mapIndex.put(index, i);
		}
	}
	
	private void displayIndex() {
		LinearLayout indexLayout = (LinearLayout) findViewById(R.id.siteIndexLayout);

		TextView textView;
		List<String> indexList = new ArrayList<String>(mapIndex.keySet());
		for (String index : indexList) {
			textView = (TextView) getLayoutInflater().inflate(
					R.layout.side_index_item, null);
			textView.setText(index.toUpperCase());
			textView.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					TextView selectedIndex	= (TextView) v;
					brokerHouseListView.setSelection(mapIndex.get(selectedIndex.getText().toString().toLowerCase()));
					
				}
				
			});
			indexLayout.addView(textView);
		}
	}
	
	
	//Added by Thinzar@20141127, Change_Request-20141101, ReqNo. 10 - END
	
	//Added by Thinzar@20140207 - to move config files to server - START
	public void prepareBrokerHouseGridView(ArrayList<Broker> Brokers){
		
		layoutInf	= getLayoutInflater();
		View v	= layoutInf.inflate(R.layout.list_broker_house_gridview, null);
		layoutInf	= getLayoutInflater();
		brokerListLinearLayout.removeAllViews();
		brokerListLinearLayout.addView(v);
		
		String iconImgPath;
		String appName;
		String plistLink;
		String iconName;
		BitmapDrawable mDrawable;
		
		/*
		 * PREPARE ICONS IMAGE FILES
		 * 1. try check data/data/images folder
		 * 2. check asset and move to data/data/images
		 * 3. download to data/data/images
		 * else
		 * 4. set default image
		 */
		/*comment this out first, we will move image files later
		try{

			for(int k = 0; k < Brokers.size(); k ++){
				Broker b = Brokers.get(k);
				iconName = b.getIconName();
				iconImgPath = imgPath + "/" + iconName;
				File iconImgFile = new File(iconImgPath);
				
				if(!iconImgFile.exists()){
					
					if(Arrays.asList(getResources().getAssets().list("icons")).contains(iconName)) {
						
						Log.d("TAG", iconName + " found in assets. copying...");
						boolean result = PlistFileUtil.copyAssets(this, "icons/" + iconName, imgPath + "/" + iconName);
						
						if(!result) PlistFileUtil.copyAssets(this, "icons/btnicon_broker_default.png", imgPath + "/" + iconName);	//if failed to copy from assets
					
					} else {
						
						Log.d("TAG", iconName + " not found. need to download!");
						new DownloadImgAndSave(this).execute(b);
						
					}
				}
			}
		}catch (IOException ioe){
			ioe.printStackTrace();
		}
		*/
		
		//populate the UI here
		final int intMaxColumnPerRow	= 4;
		int widthForSpace	= (int) screenWidth / (5 * intMaxColumnPerRow);
		imgScaledWidth = ((int) screenWidth / intMaxColumnPerRow) - widthForSpace;	//for some reason, the img was a little small, so i added 50
		/*
		Log.d("TAG", "screenWidth=" + screenWidth);
		Log.d("TAG", "imgScaledWidth=" + imgScaledWidth);
		Log.d("TAG", "widthForSpace=" + widthForSpace);
		*/
		
		/*
		if(tlBrokerHouseList == null){
			tlBrokerHouseList	= (TableLayout) findViewById(R.id.tlBrokerHouseList);
		} else {
			tlBrokerHouseList.removeAllViews();
		}
		*/
		tlBrokerHouseList	= (TableLayout) findViewById(R.id.tlBrokerHouseList);
		/* note : use to exclude rejected icon setup */
		int intCtrReject				= 0;
		
		TableRow tableRow				= null;
		
		for(int k = 0; k < Brokers.size(); k ++){
			try{
				final Broker b = Brokers.get(k);
				appName = b.getAppName();
				iconName = b.getIconName();
					
				if( (k-intCtrReject) % intMaxColumnPerRow == 0){
					if(tableRow != null){
						tlBrokerHouseList.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );  
					}
						
					tableRow		= new TableRow(SelectBrokerHouseActivity.this);   
					tableRow.setId( (k-intCtrReject) / intMaxColumnPerRow );
					tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
					tableRow.setWeightSum(1);
				}
					
				View columnView 						= getLayoutInflater().inflate(R.layout.list_broker_house_column, null);
				( (TextView) columnView.findViewById(R.id.tvBrokerHouseButtonName) ).setText( appName );
					
				ImageButton ibBrokerHouseButtonImg		= ( (ImageButton) columnView.findViewById(R.id.ibBrokerHouseButtonImg) );
				ibBrokerHouseButtonImg.setLayoutParams(new LinearLayout.LayoutParams(imgScaledWidth,imgScaledWidth));	//Added by Thinzar@20141229
				
				//mDrawable = PlistFileUtil.getImgFrmPath(imgPath + "/" + iconName, imgScaledWidth, imgScaledWidth);
				//ibBrokerHouseButtonImg.setBackgroundDrawable(mDrawable);
				
				int resId = getResources().getIdentifier(iconName, "drawable", getApplication().getPackageName());	//Edited by Thinzar, Change_Request-20141101, ReqNo. 1
				if(resId == 0){
					ibBrokerHouseButtonImg.setBackgroundResource(R.drawable.btnicon_broker_default);
				} else {
					ibBrokerHouseButtonImg.setBackgroundResource(resId);
				}
				
				ibBrokerHouseButtonImg.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						/* 
						Intent data = new Intent();
						//data.putExtra("appName", b.getAppName());
						//data.putExtra("iconName", b.getIconName());
						data.putExtra(ParamConstants.PARAM_PACKAGE_NAME, b.getPackageName());
						
						SelectBrokerHouseActivity.this.setResult(RESULT_OK, data);
						SelectBrokerHouseActivity.this.finish();
						*/
						//Added by Thinzar@20141106, Change_Request-20141101, ReqNo. 1
						selectedPackageName	= b.getPackageName();
						selectedBrokerId	= b.getBrokerId();
						selectedBrokerName	= b.getAppName();
						
						String[] parameters = {selectedPackageName, getPlistFileVersion(selectedPackageName), ParamConstants.LOGIN_ACTIVITY_TAG};
						new CheckPlistTask(SelectBrokerHouseActivity.this, dialog, SelectBrokerHouseActivity.this).execute(parameters);
						
					}
					
				});
				
				tableRow.addView(columnView);
					
			}catch (Exception e){
				e.printStackTrace();
				intCtrReject++;
			}
		}//end for loop of brokerList
	
		if(tableRow != null)
			tlBrokerHouseList.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
	}//prepareBrokerHouseListView()
	
	//temporarily decided not to download image from server
	private class DownloadImgAndSave extends AsyncTask<Broker, Void, Bitmap>
    {
		Context mContext;
		
		public DownloadImgAndSave(Context c){
			this.mContext = c;	
		}
		
		@Override
        protected Bitmap doInBackground(Broker... bb) 
        {           
            downloadImagesToSdCard(bb[0].getIconLink(),bb[0].getIconName());
           
            return null;
        }

        private void downloadImagesToSdCard(String downloadUrl, String imageName)
        {
        	try
            {
        		Log.d("TAG", " downloading " + imageName + " to SD card.");
        		
                InputStream is = null;
                FileOutputStream fos = null;
                URL url = new URL(downloadUrl);
                File outFile = new File(imgPath, imageName);
                
                HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
                httpConn.setRequestMethod("GET");
                httpConn.connect();
                
                if(httpConn.getResponseCode() == HttpURLConnection.HTTP_OK){
                	is = httpConn.getInputStream();
                	fos = new FileOutputStream(outFile);
                	
                	PlistFileUtil.copyFile(is, fos);
                	
                	 fos.flush();
                     fos.close();
                     is.close();
                     Log.d("TAG", "Image saved to SD card!");
                } else {
                	Log.d("TAG", "Download link not found, replacing with default icon.");
                	PlistFileUtil.copyAssets(mContext, "icons/btnicon_broker_default.png", imgPath + "/" + imageName);
                }
               
            }
            catch(IOException ioe)
            {                  
            	ioe.printStackTrace();
            	
            }
        }
    }
	
	//Added by Thinzar@20150108, to solve onBackPressed() - START
	@Override
	public void onBackPressed() {
		System.out.println("onBackPressed(): mPackageName=" + mPackageName);
		String app_pkg_name	= getApplicationContext().getPackageName();
		if(!mPackageName.equalsIgnoreCase(app_pkg_name)){
			showBrokerHouseListScreen(app_pkg_name);
		} else {
			super.onBackPressed();
		}
	}
	//Added by Thinzar@20150108, to solve onBackPressed() - END
	
	//Added by Thinzar@20140207 - to move config files to server - END
	/*
	@Override
	public void onBackPressed() {
		System.out.println("onBackPressed");
		if(AppConstants.brokerConfigBean == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(SelectBrokerHouseActivity.this);
			builder.setMessage("Please choose a broker")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		} else{
			super.onBackPressed();
		}
	}
	*/
	/*
	 * The following function commented out by Thinzar@20140207 - to move config files to server
	private void prepareBrokerHouseListView(TableLayout tlBrokerHouseList, List<String> arrBHB){
		try{
			final int intMaxColumnPerRow	= 4;
			
			if(tlBrokerHouseList == null)
				tlBrokerHouseList	= (TableLayout) findViewById(R.id.tlBrokerHouseList);
			
			tlBrokerHouseList.removeAllViews();
			// note : use to exclude rejected icon setup 
			int intCtrReject				= 0;
			
			TableRow tableRow				= null;
			for(int i=0; i<arrBHB.size(); i++){
				try{
					final BrokerPlistConfigBean brokerConfigBean	= ( (Class<BrokerPlistConfigBean>)Class.forName( BrokerPlistConfigBean.class.getPackage().getName() + "." + arrBHB.get(i) ) ).newInstance();
					
					if( (i-intCtrReject) % intMaxColumnPerRow == 0){
						if(tableRow != null){
							tlBrokerHouseList.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );  
						}
						tableRow		= new TableRow(SelectBrokerHouseActivity.this);   
						tableRow.setId( (i-intCtrReject) / intMaxColumnPerRow );
						tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
						tableRow.setWeightSum(1);
					}
					
					View columnView 						= getLayoutInflater().inflate(R.layout.list_broker_house_column, null);
					( (TextView) columnView.findViewById(R.id.tvBrokerHouseButtonName) ).setText( brokerConfigBean.getStrBrokerHouseBtnName() );
					
					ImageButton ibBrokerHouseButtonImg		= ( (ImageButton) columnView.findViewById(R.id.ibBrokerHouseButtonImg) );
					
					// note : default to btnicon_broker_default if broker config bean image is invalid 
					try{
						getResources().getDrawable( brokerConfigBean.getIntBrokerHouseBtnImg() );
					}catch(Exception e){
						brokerConfigBean.setIntBrokerHouseBtnImg(R.drawable.btnicon_broker_default);
					}
					
					ibBrokerHouseButtonImg.setBackgroundResource( brokerConfigBean.getIntBrokerHouseBtnImg() );
					
					ibBrokerHouseButtonImg.setOnClickListener( new OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent data 							= new Intent();
							
							//BrokerConfigBean prevBrokerConfigBean	=  AppConstants.brokerConfigBean;
							BrokerPlistConfigBean prevBrokerConfigBean	=  AppConstants.brokerConfigBean;
							AppConstants.brokerConfigBean			= brokerConfigBean;
							// note : filter to set boolean that flag of should reset ATP related variable or not 
							if( AppConstants.brokerConfigBean != null
								&& ( prevBrokerConfigBean != null && prevBrokerConfigBean.getStrAtpIP() != null 
									&& prevBrokerConfigBean.getStrAtpIP().equalsIgnoreCase( AppConstants.brokerConfigBean.getStrAtpIP() ) 
								) 
							){
								data.putExtra(ParamConstants.PARAM_BLN_HAS_ATP_CHANGE, false);
							}else{
								data.putExtra(ParamConstants.PARAM_BLN_HAS_ATP_CHANGE, true);
							}
							
							SelectBrokerHouseActivity.this.setResult(RESULT_OK, data);
							SelectBrokerHouseActivity.this.finish();
						}
					});
					
					tableRow.addView(columnView);
				}catch(Exception e){
					e.printStackTrace();
					intCtrReject++;
				}
			}
			
			if(tableRow != null)
				tlBrokerHouseList.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	
	//Added by Thinzar@20141106, Change_Request-20141101, ReqNo. 1 - START
	private String getPlistFileVersion(String pkgName){
		String mVersion;
		sharedPreferences 						= getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		mVersion = sharedPreferences.getString(pkgName, "0");
		
		//to prevent file from being deleted by user
		if(!mVersion.equals("0")){
			String filename = PlistFileUtil.getPlistFileNameFormat(pkgName);
			File file = new File(plistPath, filename);
			
			if(!file.exists())	mVersion = "0";
		}
		
		return mVersion;
	}

	// This is the callback for when your async task has finished
    @Override
	public void onTaskFinished() {
    	
    	sharedPreferences 						= getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		SharedPreferences.Editor editor 		= sharedPreferences.edit();
		
		plistFileName = PlistFileUtil.getPlistFileNameFormat(selectedPackageName);
		plistPath = SplashScreenActivity.plistPath;
    	boolean isMultiLogin = PlistFileUtil.checkPlistIsMultiLogin(SelectBrokerHouseActivity.this, plistPath, plistFileName);
    	
    	if (isMultiLogin) {
			editor.putBoolean(PreferenceConstants.HAS_MULTI_LOGIN, true);
			editor.commit();
			showBrokerHouseListScreen(this.selectedPackageName);
			
		} else {
			AppConstants.CURRENT_PKG_NAME	=  selectedPackageName;
			showLoginScreen(selectedPackageName);
		}
    }
    
    private void showBrokerHouseListScreen(String mPkgName){
    	if(!mPkgName.equals(getApplication().getPackageName())){
			btnBack.setVisibility(View.VISIBLE);
		}else{
			btnBack.setVisibility(View.INVISIBLE);
		}
		this.repaintBrokerHouseScreen(mPkgName);
	}
    
    private void showLoginScreen(String mPackageName){    
    	
    	Intent intent = new Intent();
		intent.putExtra(ParamConstants.PARAM_PACKAGE_NAME, mPackageName);
		intent.putExtra(ParamConstants.BROKER_ID_TAG, selectedBrokerId);		//special case for PSE
		intent.putExtra(ParamConstants.BROKER_NAME_TAG, selectedBrokerName);
		
		intent.setClass(SelectBrokerHouseActivity.this, LoginActivity.class);
		startActivity(intent);
    }
	//Added by Thinzar@20141106, Change_Request-20141101, ReqNo. 1 - END
}
