package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.StockNews;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class NewsCategoryActivity extends CustomWindow implements
		OnDoubleLoginListener, OnRefreshStockListener {

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	private String userParam;
	*/
	/* Mary@20130605 - comment unused variable
	private List newsCategories;
	*/
	private List<StockNews> newList 	= new ArrayList<StockNews>();
	private String response;
	private List<StockNews> newsUpdates	= null;
	private ProgressDialog dialog 		= null;
	private DoubleLoginThread loginThread;
	private boolean isRefresh 			= false;
	
	/* Mary@20130605 - comment unused variable
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	private int intRetryCtr	= 0;
	*/
	
	// Added by Mary@20130306 - Change_Request-20130225, ReqNo.2 - START
	private int intSelectedRow;
	private TableLayout tableLayout;
	private boolean isFromOnCreate;
	// Added by Mary@20130306 - Change_Request-20130225, ReqNo.2 - END

	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.news_category);
	
			this.title.setText(getResources().getString(R.string.news_module_title));
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(R.drawable.menuicon_news);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntNewsMenuIconBgImg() );
	
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
			/* commented out by Thinzar, Change_Request-20160101, ReqNo.3
			if(! AppConstants.hasMultiLogin ){
				( (LinearLayout)findViewById(R.id.rl_nc_TopNavBar) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBgImg() );
				( (ImageButton)findViewById(R.id.refreshButton) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );
			}*/
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
					
			dialog = new ProgressDialog(this);
	
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			userParam = sharedPreferences.getString(PreferenceConstants.QC_USER_PARAM, null);
						
			try {
				if (userParam == null) {
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					List<String>data = QcHttpConnectUtil.relogin();
					userParam = data.get(0);
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
					editor.commit();
				}
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
			}
			*/
	
			// Added by Mary@20130306 - Change_Request-20130225, ReqNo.2
			tableLayout = (TableLayout) this.findViewById(R.id.categoryTable);
			
			this.loadNews();
	
			ImageButton refreshButton = (ImageButton) this.findViewById(R.id.refreshButton);
			refreshButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					isRefresh = true;
					NewsCategoryActivity.this.loadNews();
	
				}
			});
			
			// Added by Mary@20130306 - Change_Request-20130225, ReqNo.2
			isFromOnCreate = true;
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	
	public void onPause() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}

	public void onResume() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onResume();
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= NewsCategoryActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
			// Added by Mary@20121116 - Change_Request-20120719, ReqNo.12
			AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, null);
			
			loginThread = ((StockApplication)NewsCategoryActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) NewsCategoryActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(NewsCategoryActivity.this);
				loginThread.resumeRequest();
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(this);
	
			AppConstants.showMenu = true;
			
			/* Mary@20130306 - Change_Request-20130225, ReqNo.2 - START
			TableLayout tableLayout = (TableLayout) this.findViewById(R.id.categoryTable);
			tableLayout.removeAllViews();
	
			final String[] categories = DefinitionConstants.NEWS_CATEGORY_SEQ;
			for (int id = 0; id < categories.length; id++) {
				String categoryName = categories[id];
	
				final TableRow tableRow = new TableRow(this);
				tableRow.setId(id);
				tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
				tableRow.setWeightSum(1);
				View rowView = this.getLayoutInflater().inflate(R.layout.search_row, null);
				TextView nameLabel = (TextView) rowView.findViewById(R.id.nameLabel);
				nameLabel.setText(categoryName);
	
				TextView codeLabel = (TextView) rowView.findViewById(R.id.codeLabel);
				codeLabel.setText("");
	
				tableRow.addView(rowView);
	
				final LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.rowLayout);
	
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				if ((id + 1) % 2 != 0) {
					rowLayout.setBackgroundResource(R.drawable.tablerow_dark);
				} else {
					rowLayout.setBackgroundResource(R.drawable.tablerow_light);
				}
				/
				rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? R.drawable.tablerow_dark : R.drawable.tablerow_light );
	
				tableRow.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						if (newList.size() != 0)
							newList.clear();
						
						rowLayout.setBackgroundDrawable(null);
						rowLayout.setBackgroundColor(Color.CYAN);
	
						Intent intent = new Intent();
						int rowId = tableRow.getId();
						intent.putExtra(ParamConstants.CATEGORY_NAME_TAG, categories[rowId]);
						if (newsCategories == null)
							newsCategories = new ArrayList();
						
	
						Map categoryMapping = DefinitionConstants.getNewsCategoryMapping();
	
						if (newsUpdates == null) {
							newsUpdates = new ArrayList();
						}
	
						for (Iterator itr = newsUpdates.iterator(); itr.hasNext();) {
							final StockNews news = (StockNews) itr.next();
							String categoryId = news.getCategoryId();
	
							if (categoryId != null) {
								if ((Integer) categoryMapping.get(categoryId) == rowId) {
									newList.add(news);
								}
							}
						}
	
						List newsItems = (List) newList;
						StockApplication application = (StockApplication) NewsCategoryActivity.this.getApplication();
						application.setNewsItems(newsItems);
						// intent.putExtra(ParamConstants.NEWS_ITEMS_TAG,
						// (ArrayList) newsCategories.get(rowId));
						intent.setClass(NewsCategoryActivity.this, NewsItemActivity.class);
						startActivity(intent);
	
					}
	
				});
	
				tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
	
			}
			*/
			if(isFromOnCreate){
				isFromOnCreate = false;
			}else{
				repaintNewsLayout();
			}
			//  Mary@20130306 - Change_Request-20130225, ReqNo.2 - END
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}

	private void loadNews() {
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
		if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
			new refreshNews().execute();
	}

	private class refreshNews extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( dialog != null && !dialog.isShowing() ){
					/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
					if (!isRefresh) {
						dialog.setMessage("Loading news. Please wait...");
					} else {
						dialog.setMessage("Refreshing news. Please wait...");
					}
					*/
					dialog.setMessage(isRefresh ? getResources().getString(R.string.dialog_progress_refreshing) : getResources().getString(R.string.dialog_progress_loading));
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! NewsCategoryActivity.this.isFinishing() )
						dialog.show();
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			NewsCategoryActivity.this.refreshNews(isRefresh);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( dialog != null && dialog.isShowing() )
					dialog.dismiss();
				
				// Added by Mary@20130306 - Change_Request-20130225, ReqNo.2
				repaintNewsLayout();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	private void refreshNews(boolean isRefresh) {
				
		try {

			/* Mary@20130605 - comment unused variable
			String[] categories = DefinitionConstants.NEWS_CATEGORY_SEQ;
			*/

			/* Mary@20130312 - Fixes_Request-20130108, ReqNo.19
			// Added by Mary@20130110 - Fixes_Request-20130108, ReqNo.4
			String strExchgCode	= AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("MY") ? "KL" : AppConstants.DEFAULT_EXCHANGE_CODE;
			*/
			String strExchgCode	= AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase(AppConstants.MY_EXCHANGE_CODE) ? "KL" : AppConstants.DEFAULT_EXCHANGE_CODE;
			
			if(!isRefresh){
				/* Mary@20130110 - Fixes_Request-20130108, ReqNo.4
				if (AppConstants.News == null || AppConstants.News.get(AppConstants.DEFAULT_EXCHANGE_CODE) == null) {
				*/
				if (AppConstants.News == null || AppConstants.News.get(strExchgCode) == null) {
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					Map parameters = new HashMap();
					parameters.put(ParamConstants.USERPARAM_TAG, userParam);
					parameters.put(ParamConstants.EXCHANGECODE_TAG,AppConstants.DEFAULT_EXCHANGE_CODE);
					
					response = QcHttpConnectUtil.newsAll(parameters);
					*/
					/* Mary@20130110 - Fixes_Request-20130108, ReqNo.4
					response = QcHttpConnectUtil.newsAll(AppConstants.DEFAULT_EXCHANGE_CODE);
					*/
					response = QcHttpConnectUtil.newsAll(strExchgCode);
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					/* NOTE : NO retry or alert of exception as it already have handling for null value 
					  and NULL is acceptable where it will auto defaulted to get default exchange code
					*/					

					Map<String, String> news = new HashMap<String, String>();
					/* Mary@20130110 - Fixes_Request-20130108, ReqNo.4
					news.put(AppConstants.DEFAULT_EXCHANGE_CODE, response);
					*/
					news.put(strExchgCode, response);
					AppConstants.News = news;
				}else{
					/* Mary@20130110 - Fixes_Request-20130108, ReqNo.4
					response = AppConstants.News.get(AppConstants.DEFAULT_EXCHANGE_CODE);
					*/
					response = AppConstants.News.get(strExchgCode);
				}

				if(response != null)
					newsUpdates = QcMessageParser.parseNewsUpdate(response);
			} else {
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				Map parameters = new HashMap();
				parameters.put(ParamConstants.USERPARAM_TAG, userParam);
				parameters.put(ParamConstants.EXCHANGECODE_TAG,AppConstants.DEFAULT_EXCHANGE_CODE);

				response = QcHttpConnectUtil.newsAll(parameters);
				*/
				/* Mary@20130110 - Fixes_Request-20130108, ReqNo.4
				response = QcHttpConnectUtil.newsAll(AppConstants.DEFAULT_EXCHANGE_CODE);
				*/
				response = QcHttpConnectUtil.newsAll(strExchgCode);
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				/* NOTE : NO retry or alert of exception as it already have handling for null value 
				  and NULL is acceptable where it will auto defaulted to get default exchange code
				*/ 

				Map<String, String> news = new HashMap<String, String>();
				/* Mary@20130110 - Fixes_Request-20130108, ReqNo.4
				news.put(AppConstants.DEFAULT_EXCHANGE_CODE, response);
				*/
				news.put(strExchgCode, response);
				AppConstants.News = news;

				if(response != null)
					newsUpdates = QcMessageParser.parseNewsUpdate(response);
			}

			/* Mary@20130605 - comment unused variable
			newsCategories = new ArrayList();

			for (int i = 0; i < categories.length; i++) {
				newsCategories.add(new ArrayList());
			}
			*/

			if(response == null)
				return;
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		} catch (FailedAuthenicationException e) {
			e.printStackTrace();
			this.refreshNews(isRefresh);	// might caused infinite loop if FailedAuthenicationException continuously thrown
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.v("NEWS", "error", ex);
		}
		*/
		}catch(Exception ex){
			Log.v("NEWS", "error", ex);
		}
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END;

	}
	
	// Added by Mary@20130306 - Change_Request-20130225, ReqNo.2 - START
	private void repaintNewsLayout(){
		tableLayout.removeAllViews();

		boolean[] isShowCategory	= new boolean[DefinitionConstants.getNewsCategoryMapping().size()];
		for(Iterator<String> itr=DefinitionConstants.getNewsCategoryMapping().keySet().iterator(); itr.hasNext();){
			String strKey	= itr.next();
			// Added by Mary@20130610 - Fixes_Request-20130523, ReqNo.8 - START
			if( DefinitionConstants.getNewsCategoryMapping().get(strKey) == null )
				continue;
			// Added by Mary@20130610 - Fixes_Request-20130523, ReqNo.8 - END
			
			int intVal		= DefinitionConstants.getNewsCategoryMapping().get(strKey);
			
			if( strKey.equals("-1") ){
				isShowCategory[intVal] = true;
			}else if( newsUpdates != null){
				for (Iterator<StockNews> itr1= newsUpdates.iterator(); itr1.hasNext();){
					final StockNews stockNews	= itr1.next();
					if( stockNews.getCategoryId() != null && strKey.equals( stockNews.getCategoryId() ) ){
						isShowCategory[intVal] = true;
						break;
					}
				}
			}
		}
		int intColor	= 1;
		for (int id = 0; id < DefinitionConstants.NEWS_CATEGORY_SEQ.length; id++) {
			if( isShowCategory[id]){
				String categoryName 		= DefinitionConstants.NEWS_CATEGORY_SEQ[id];
				
				final TableRow tableRow 	= new TableRow(this);
				tableRow.setId(id);
				tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
				tableRow.setWeightSum(1);
				
				View rowView 				= this.getLayoutInflater().inflate(R.layout.search_row, null);
				
				TextView nameLabel 			= (TextView) rowView.findViewById(R.id.nameLabel);
				nameLabel.setText(categoryName);
				
				TextView codeLabel 			= (TextView) rowView.findViewById(R.id.codeLabel);
				codeLabel.setText("");
				
				final LinearLayout rowLayout= (LinearLayout) rowView.findViewById(R.id.rowLayout);
				//rowLayout.setBackgroundResource( (intColor % 2) != 0 ? R.drawable.tablerow_dark : R.drawable.tablerow_light );	//commented out, Change_Request-20160101, ReqNo.3
				
				tableRow.addView(rowView);
				
				tableRow.setOnClickListener(new OnClickListener() {
					public void onClick(View view){					
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						if (newList.size() != 0)
							newList.clear();
						
						/* commented by Thinzar, Change_Request-20160101, ReqNo.3
						rowLayout.setBackgroundDrawable(null);
						rowLayout.setBackgroundColor(Color.CYAN);
						 */
						
						Intent intent 					= new Intent();
						intSelectedRow					= tableRow.getId();
						intent.putExtra(ParamConstants.CATEGORY_NAME_TAG, DefinitionConstants.NEWS_CATEGORY_SEQ[intSelectedRow]);
						/* Mary@20130605 - comment unused variable
						if (newsCategories == null)
							newsCategories = new ArrayList();
						*/
	
						if (newsUpdates == null) {
							newsUpdates = new ArrayList<StockNews>();
						}
	
						for (Iterator<StockNews> itr 	= newsUpdates.iterator(); itr.hasNext();) {
							final StockNews news= itr.next();
							/* Mary@20130610 - Fixes_Request-20130523, ReqNo.8 - START
							String categoryId 	= news.getCategoryId();
							if (categoryId != null) {
								if( DefinitionConstants.getNewsCategoryMapping().get(categoryId) == intSelectedRow || DefinitionConstants.getNewsCategoryMapping().get("-1") == intSelectedRow ){
								*/
							if(DefinitionConstants.getNewsCategoryMapping().get("-1") == intSelectedRow
									|| ( DefinitionConstants.getNewsCategoryMapping().get( news.getCategoryId() ) != null
											&& DefinitionConstants.getNewsCategoryMapping().get( news.getCategoryId() ) == intSelectedRow 
										) 
							){
								newList.add(news);
							// Mary@20130610 - Fixes_Request-20130523, ReqNo.8 - END
							}
						}
	
						List<StockNews> newsItems 		= newList;
						StockApplication application	= (StockApplication) NewsCategoryActivity.this.getApplication();
						application.setNewsItems(newsItems);
						intent.setClass(NewsCategoryActivity.this, NewsItemActivity.class);
						startActivity(intent);
					}
	
				});
	
				tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
				intColor++;
			}
		}
	}
	// Added by Mary@20130306 - Change_Request-20130225, ReqNo.2 - END

	/* Mary@20130605 - comment unused method
	private void handleFailedAuthentication() {
		try {
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			List<String>data = QcHttpConnectUtil.relogin();
			userParam = data.get(0);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
			editor.commit();
			/

			RefreshStockThread refreshThread = ((StockApplication) this	.getApplication()).getRefreshThread();

			if (refreshThread != null)
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				refreshThread.setUserParam(userParam);
				/
				refreshThread.setUserParam( AppConstants.QCdata.getStrUserParam() );

			QCAliveThread QCAlive = ((StockApplication) this.getApplication()).getQCAliveThread();

			if (QCAlive != null)
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				QCAlive.setUserParam(userParam);
				/
				QCAlive.setUserParam( AppConstants.QCdata.getStrUserParam() );
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		} catch (FailedAuthenicationException e) {
		/
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler() {
		public void handleMessage(Message message) {
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if (response.get(2).equals("1102")) {
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(NewsCategoryActivity.this.mainMenu.isShowing()){
					NewsCategoryActivity.this.mainMenu.hide();
				}
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					NewsCategoryActivity.this.doubleLoginMessage((String) response.get(1));
				}
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		// TODO Auto-generated method stub

	}

}
