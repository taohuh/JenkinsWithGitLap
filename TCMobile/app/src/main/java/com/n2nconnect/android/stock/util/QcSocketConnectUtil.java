package com.n2nconnect.android.stock.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.FeedObject;


public class QcSocketConnectUtil {
	private static Socket receiverSocket;
	private static InetSocketAddress socketAddress;
	private static InputStream receiverInputStream;
	private static OutputStream receiverOutputStream;
	private static ArrayList <Byte> remainDataBytes;
	private static Thread receiverThread;
	private static String userParam;
	
	public static boolean initializeSocket() {
		
		try {
			boolean isCreated = createQcDataSocket();
			
			if (!isCreated) {
				return false;
			}
			
			receiverInputStream = receiverSocket.getInputStream();
			receiverOutputStream = receiverSocket.getOutputStream();
			
	        //Get Key
			receiverOutputStream.write("GET /GetKey HTTP/1.1".getBytes());
	        
	        receiverOutputStream.write("\r\n\r\n".getBytes());
	        receiverOutputStream.flush();
	        
	        byte[] userParamKeyBytes = readFromSocketReceiver(true);
	        String userParamKey = SystemUtil.generateAESKey(userParamKeyBytes);
	        
	       // System.out.println("the user param key is: " + userParamKey);
	        
	        String loginCommand = "GET /[" + userParamKey + "]Login2?ClientApp=mobile&PullMode=0&ClientID=Testing HTTP/1.1";
	        
	        receiverOutputStream.write(loginCommand.getBytes());
	        
	        receiverOutputStream.write("\r\n\r\n".getBytes());
	        receiverOutputStream.flush();
	        
	        byte[] loginDataBytes = readFromSocketReceiver(true);
	        
	        String loginData = new String(loginDataBytes,"ISO-8859-1");
	        
	        //System.out.println("the login data is: " + loginData);
	        
	        userParam = SystemUtil.extractUserParam(loginDataBytes, "[UserParams]=",false);
	        
	        //System.out.println("the user param is: " + userParam);
	        
	        return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
        
	}
		
	public static boolean imageReplace(String[] stockSymbols) {	
		
		try {
	        Socket senderSocket = createQcCommandSocket();
	        boolean isConnected = connect(senderSocket, userParam);
	        
	        if (!isConnected) {
	        	return false;
	        }
	        
	        StringBuffer stockList = new StringBuffer();
	        for (int i=0; i<stockSymbols.length; i++) {
	        	stockList.append(stockSymbols[i]);
	        	if (i < stockSymbols.length-1) {
	        		stockList.append(",");
	        	}
	        }
	           
	        String replaceCommand = "GET /[" + userParam + "]Image?[REPLACE]=" + stockList.toString() + " HTTP/1.1";
	        
	        //Call Replace
	        OutputStream senderOutputStream = senderSocket.getOutputStream();
			senderOutputStream.write(replaceCommand.getBytes());
	        
			senderOutputStream.write("\r\n\r\n".getBytes());
			senderOutputStream.flush();
			
			closeSenderSocket(senderSocket);    
			
	        
	        receiverThread = new Thread(new Runnable() {
				
				public void run() {	
					try {
				        readFromSocket();
				        
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
	        
	        receiverThread.start();
	        return true;
	        
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
            
	}
	
	private static byte[] readFromSocketReceiver(boolean delay) {

        if(delay) {
            int timeOutCount = 10;
            try {
                while (receiverInputStream.available() == 0) {
                	if (timeOutCount == 0) return null;
                    Thread.sleep(1000);
                    
                    timeOutCount--;
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        return readFromSocketReceiver();
    }

    private static byte[] readFromSocketReceiver() {

        try {
            int totalSize = 0;
            List dataList = new ArrayList();    
            
            while (receiverInputStream.available() > 0) {
            	int size = receiverInputStream.available();
            	byte[] chunkBytes = new byte[size];
                receiverInputStream.read(chunkBytes, 0, size);
                dataList.add(chunkBytes);
                totalSize += size;
            }
            
            byte[] dataBytes = new byte[totalSize];
            
            int pos = 0;
            for (Iterator itr=dataList.iterator(); itr.hasNext(); ) {
            	byte[] chunkBytes = (byte[]) itr.next();
            	System.arraycopy(chunkBytes, 0, dataBytes, pos, chunkBytes.length);
            	pos += chunkBytes.length;
            }
            
            return dataBytes;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
	
	
	private static boolean createQcDataSocket() {
		try {
			receiverSocket = new Socket();
			socketAddress = new InetSocketAddress(AppConstants.QC_SERVER_IP, 20000);
			
            try {
            	receiverSocket.setKeepAlive(true);
            	receiverSocket.setTcpNoDelay(true);
            	receiverSocket.setReuseAddress(true);
                receiverSocket.connect(socketAddress, 10000);
                int iTimeOut = 10;
                while (!receiverSocket.isConnected()) {
                    Thread.sleep(1000);
                    receiverSocket.connect(socketAddress, 10000);
                    iTimeOut--;
                	if (iTimeOut == 0) {
                		return false;
                	}
                }
            } catch (Exception e) {
                return false;
            }
            
            return true;
        } catch(Exception e) {
        	return false;
        }
	}
	
	private static void readFromSocket() throws Exception {
      
        byte[] abtData;
        ArrayList<?> alFeed;
        
        FeedObject[] aFeedObj;
        
        int ch;

        while(!receiverSocket.isClosed() && (ch = receiverInputStream.read()) != -1) {
        	
            alFeed = readFromSocketReceiver(ch);
            
            if(alFeed != null) {
            	int iLength = alFeed.size();
            	for (int i = 0; i < iLength; i++) {
            		abtData = (byte[]) alFeed.get(i);
            		if(abtData.length != 0) {
            			if((char) abtData[0] == '-' &&
                                (char) abtData[1] == 'A' &&
                                (char) abtData[2] == '0' &&
                                (char) abtData[3] == '5' &&
                                (char) abtData[4] == 30) {
            				aFeedObj = N2NFeedUtil.processPushData(abtData, new ArrayList<String>());
            				for (FeedObject fobj : aFeedObj) {
            					System.out.println("----------------------------------");
            					for (int j = 0; j < fobj.getTotalFeedElement(); j++) {
            						System.out.println("the feed element value is: " + fobj.getFeedElementAt(j) + "=" + fobj.getFeedValueAt(j));
            					}
            					System.out.println("----------------------------------");
            				}
            			}
            		}
            	}
            }
        }
    }
	
	private static ArrayList<byte[]> readFromSocketReceiver(int ch) {
        try {
            int iSize;
            int iLength;
            int iAscii;
            byte btBit;
            byte[] abtChunk;
            byte[] abtFullData;
            ArrayList <Byte> albtFullData;
            ArrayList<byte[]> alObj = new ArrayList<byte[]>();
            boolean bStartBit;

            if (receiverInputStream.available() > 0) {
                iSize = receiverInputStream.available();
                abtChunk = new byte[iSize + 1];
                abtChunk[0] = (byte) ch;
                receiverInputStream.read(abtChunk, 1, iSize);
                if(remainDataBytes != null) {
                    albtFullData = remainDataBytes;
                    bStartBit = true;
                } else {
                    bStartBit = false;
                    albtFullData = null;
                }
                for (int i = 0; i < iSize; i++) {
                    btBit = abtChunk[i];
                    iAscii = btBit & 0xff;
                    if (bStartBit) {
                        albtFullData.add(btBit);
                    }
                    if (iAscii == 2) {
                        if (albtFullData == null) {
                            albtFullData = new ArrayList<Byte>();
                        }
                        bStartBit = true;
                    }
                    if (iAscii == 3) {
                        bStartBit = false;
                        if (albtFullData != null) {
                            iLength = albtFullData.size();
                            abtFullData = new byte[iLength];
                            for (int j = 0; j < iLength; j++) {
                                abtFullData[j] = albtFullData.get(j);
                            }
                            //System.out.println("the fulldata bytes:" + new String(abtFullData, "ISO-8859-1"));
                            alObj.add(abtFullData);
                            albtFullData.clear();
                        }
                    }
                }
                if(bStartBit) {
                    remainDataBytes = albtFullData;
                } else {
                    remainDataBytes = null;
                }
                return alObj;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
	
	private static Socket createQcCommandSocket() {
    	Socket senderSocket = new Socket();
    	try {

            try {

            	senderSocket.setSoTimeout(10000);
            	senderSocket.setKeepAlive(false);
            	senderSocket.setTcpNoDelay(true);
            	senderSocket.setReuseAddress(true);
            	senderSocket.connect(socketAddress);
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            int timeOutCount = 10;
            while (!senderSocket.isConnected()) {
                Thread.sleep(1000);
                timeOutCount--;
            	if (timeOutCount == 0) {
            		return null;
            	}
            }
            InputStream senderInputStream = senderSocket.getInputStream();
            
            if (senderInputStream.available() > 0) {
            	readFromSocketSender(senderInputStream,false);
            } 

            return senderSocket;
            
        } catch (Exception e) {
        	e.printStackTrace();
    		if (senderSocket != null) {
            	try { 
            		senderSocket.close();
    			} catch (Exception e2) {
    				e2.printStackTrace();
    			}
    		}
        }
		return null;
    }
	
	private static boolean connect(Socket senderSocket, String userParam) {
		try {
			String connectCommand = "GET /[" + userParam + "]Connect HTTP/1.1";
			OutputStream senderOutputStream = senderSocket.getOutputStream();
			senderOutputStream.write(connectCommand.getBytes());
	        
			senderOutputStream.write("\r\n\r\n".getBytes());
			senderOutputStream.flush();
	        
			InputStream senderInputStream = senderSocket.getInputStream();
			
			byte[] returnBytes = readFromSocketSender(senderInputStream, true);
			
			if (returnBytes != null) {
				String response = new String(returnBytes, "ISO-8859-1");
				int index = response.indexOf("OK");
				
				if (response.indexOf("OK") != -1) {
					//System.out.println();
					return true;
				} else if (response.indexOf("401") != -1) {  
					return false;
		        } 
			}
		} catch (Exception e) {
		} finally {
			//release();
		}
		return false;
  	}
	
	private static synchronized byte[] readFromSocketSender(InputStream isSender, boolean bConnect) {

        try {
            int ch;
            int iCount = 0;
            StringBuffer sbData = new StringBuffer();

            if(!bConnect) {
                iCount = 2;
            } else {
                iCount = 0;
            }

            while((ch = isSender.read()) != -1) {
                sbData.append((char)ch);
                if(ch == 10) {
                    if(iCount == 1) {
                        iCount++;
                    } else if(iCount == 3) {
                        break;
                    } else {
                        if(!bConnect) {
                            iCount = 2;
                        } else {
                            iCount = 0;
                        }
                    }
                } else if(ch == 13) {
                    if(iCount == 0 || iCount == 2) {
                        iCount++;
                    } else {
                        if(!bConnect) {
                            iCount = 2;
                        } else {
                            iCount = 0;
                        }
                    }
                } else {
                    if(!bConnect) {
                        iCount = 2;
                    } else {
                        iCount = 0;
                    }
                }
            }
            return sbData.toString().getBytes("ISO-8859-1");
        } catch(IOException e) {
            return null;
        }
    }
	
   private static void closeSenderSocket(Socket senderSocket) {
    	try {
    		if (senderSocket != null) {
//	        		sSender.getOutputStream().close();
//	        		sSender.getInputStream().close();
    			senderSocket.close();
        	}
		} catch (Exception e) {
		}
    }
    
}
