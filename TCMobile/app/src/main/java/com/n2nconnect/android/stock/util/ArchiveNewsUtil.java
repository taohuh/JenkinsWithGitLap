package com.n2nconnect.android.stock.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.model.StockNews;

public class ArchiveNewsUtil {
	
	//http://sgnewsstg.itradecimb.com/ebcNews/web/ext/getReutersNews.jsp?t=&df=2013-05-27&dt=2016-06-27&s=0108&k=&ns=REUTERS&r=KL&tr=20&sz=20
	public static String getArchiveNews(String symbolCode, String exchange, String dateFrom, String dateTo){
		
		String archiveNewsResult	= "";
		URL fullArchiveNewsURL	= null;
		HttpURLConnection conn	= null;
		Integer responseCode	= null;
		InputStream inputStream	= null;
		
		String encryptTime 	= getEncryptedTime();
		
		String url	= AppConstants.brokerConfigBean.getArchiveNewsUrl() +
				"t=" + encryptTime +
				"&df="+ dateFrom +"&" +
				"dt="+ dateTo +
				"&s="+ symbolCode + "&" +
				"k=" +
				"&ns=REUTERS" +
				"&r="+ exchange +
				"&tr=20" +
				"&sz=20";
		DefinitionConstants.Debug("getArchiveNews url: " + url);
		
		try{
			fullArchiveNewsURL	= new URL(url);
			conn	= (HttpURLConnection) fullArchiveNewsURL.openConnection();
			conn.connect();
			
			responseCode	= conn.getResponseCode();
			
			if(responseCode == HttpURLConnection.HTTP_OK){
				inputStream			= conn.getInputStream();
				if(inputStream != null){
					archiveNewsResult	= convertInputStreamToString(inputStream);
				}
			} else{
				DefinitionConstants.Debug("Unable to fetch Archive News.");
			}
			
		}catch(IOException ex){
			ex.printStackTrace();
			
		}finally{
			
			if(inputStream != null){
				try{
					inputStream.close();
				} catch(IOException ex){
					ex.printStackTrace();
				}
			}
			
			if(conn != null){
				conn.disconnect();
				conn	= null;
			}
		}
		
		return archiveNewsResult;
	}
	
	private static String convertInputStreamToString(InputStream in) {
		BufferedReader reader = null;
		StringBuffer response = new StringBuffer();
		
		try {
			reader 		= new BufferedReader(new InputStreamReader(in));
			String line = "";
			
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return response.toString();
	}
	
	public static ArrayList<StockNews> convertArchiveNews(String response){

		ArrayList<StockNews>array = new ArrayList<StockNews>();
		String message = "";
		
		try {
			JSONObject json = new JSONObject(response);
			if(json.has("msg")){
				message = json.getString("msg");
			}

			if(json.has("val")){
				JSONArray jsonArr =  new JSONArray(json.getString("val"));
				for(int k =0;k<jsonArr.length();k++){
					JSONObject jsonObj= jsonArr.getJSONObject(k);
					StockNews archiveObj= new StockNews();

					archiveObj.setNewsDate( FormatUtil.parseDateString(jsonObj.getString("dt"), FormatUtil.NEWSDATE_FORMAT_2) );
					String title = jsonObj.getString("t");
					System.out.println("titlenews: " + title);
					if(title.contains("</Origin>") || title.contains("</origin>")){
						title = title.replaceAll("<[^>]*>", "");//.toUpperCase();	//Don't convert to uppercase
					}

					archiveObj.setNewsTitle(title);	

					archiveObj.setNewsId(jsonObj.getString("id"));
					archiveObj.setSymbolCode(jsonObj.getString("stk"));
					array.add(archiveObj);
				}
			}
		} catch (JSONException e) {

			e.printStackTrace();
		}
		return array;
	}
	
	public static String getEncryptedTime() {
		String encryptedTime = "";

		Date todayDate	= Calendar.getInstance().getTime();
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat dateFormat2 = new SimpleDateFormat("hhmmss");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("GMT+8.00"));
		dateFormat2.setTimeZone(TimeZone.getTimeZone("GMT+8.00"));

		String time	= dateFormat2.format(todayDate);
		String key	= dateFormat1.format(todayDate);

		char[] timeArray = time.toCharArray();
		char[] keyArray = key.toCharArray();

		for (int i = 0; i < timeArray.length; i++) {
			// Using bitwise xor operator
			int xor = ((char) timeArray[i] ^ keyArray[i]);
			// convert integer to hex with 2 digit
			encryptedTime += "%" + String.format("%02X", xor);
		}

		return encryptedTime;
	}

	public static String getCurrentDate()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = new Date();

		return convertDateToString(today);
	}

	public static String getYesterdayDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);    

		return convertDateToString(cal.getTime());
	}

	public static String getStringDate(Date date){
		String dateCon = convertDateToString(date);
		return dateCon;
	}
	public static String convertDateToString(Date date){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String todayDate = dateFormat.format(date);
		return todayDate;
	}

	public static String getOneMonthBeforeDate(){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		Date date = calendar.getTime();
		
		return convertDateToString(date);
	}
}