package com.n2nconnect.android.stock.model;

import com.n2nconnect.android.stock.util.SystemUtil;



public class DervTradePrtfSubDetailRpt {
	//is used to during display
	private boolean isExpand;
	
	//values get from ATP
	private int sequenceNo;
	private String stockName;
	private String stockCode;
	private float averagePrice;
	
	//Edited by Thinzar, changed the primitive type from float to int, Fixes_Request-20140820,ReqNo.17 -START
	private int grossBuy;
	private int grossSell;
	private int dayBuy;
	private int daySell;
	private int bfBuy;
	private int bfSell;
	//Edited by Thinzar, Fixes_Request-20140820,ReqNo.17 - END
	
	private float realisedPL;
	private float totalPL;
	private String productCode;
	private String homeCurrency;
	private float forexExchangeRate;
	private String currencySymbol;
	
	//values calculated
	private float unrealisedPL;
	private int nettPosition;	//edited by Thinzar, changed the primitive type from float to int, Fixes_Request-20140929, ReqNo.2
	
	private float multiplier;

	private float lastDone = 0.0f;
	private float refPrice = 0.0f;
	private String derivativeStockType;
	
	
	public boolean isExpand() {
		return isExpand;
	}

	public void setExpand(boolean isExpand) {
		this.isExpand = isExpand;
	}

	public int getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(int sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public float getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(float averagePrice) {
		this.averagePrice = averagePrice;
	}

	public int getGrossBuy() {
		return grossBuy;
	}

	public void setGrossBuy(int grossBuy) {
		this.grossBuy = grossBuy;
	}

	public int getGrossSell() {
		return grossSell;
	}

	public void setGrossSell(int grossSell) {
		this.grossSell = grossSell;
	}

	public int getDayBuy() {
		return dayBuy;
	}

	public void setDayBuy(int dayBuy) {
		this.dayBuy = dayBuy;
	}

	public int getDaySell() {
		return daySell;
	}

	public void setDaySell(int daySell) {
		this.daySell = daySell;
	}

	public int getBfBuy() {
		return bfBuy;
	}

	public void setBfBuy(int bfBuy) {
		this.bfBuy = bfBuy;
	}

	public int getBfSell() {
		return bfSell;
	}

	public void setBfSell(int bfSell) {
		this.bfSell = bfSell;
	}

	public float getRealisedPL() {
		return realisedPL;
	}

	public void setRealisedPL(float realisedPL) {
		this.realisedPL = realisedPL;
	}

	public float getTotalPL() {
		return totalPL;
	}

	public void setTotalPL(float totalPL) {
		this.totalPL = totalPL;
	}

	public void calculateTotalPL(){
		this.setTotalPL(realisedPL+unrealisedPL);
	}
	
	public float getUnrealisedPL() {
		return unrealisedPL;
	}

	public void setUnrealisedPL(float unrealisedPL) {
		this.unrealisedPL = unrealisedPL;  
	}	
	
	public void calculateUnrealisedPL(){ 
		float result;
		if(derivativeStockType != null){
			if(derivativeStockType.equalsIgnoreCase("F")){//FUTURE
				setUnrealisedPL((lastDone- averagePrice)* nettPosition* multiplier);
			}else if(derivativeStockType.equalsIgnoreCase("O")){//OPTION
				
				if(SystemUtil.isCall(stockName)){//CALL
					if (nettPosition >=0){ //CALL BUY
			          if (lastDone- averagePrice <=0){ result = 0; }
			          else{
			        	  result =( lastDone - averagePrice)* nettPosition * multiplier;
			          }
					}
					else{ //CALL SELL
			          if (averagePrice - lastDone >= averagePrice){
			        	  result = averagePrice * Math.abs(nettPosition) * multiplier;
			          }
			          else{
			        	  //result = (lastDone- averagePrice)* nettPosition* multiplier //Kam Fai 01/08/2013
			        	  result = (averagePrice - lastDone) * Math.abs(nettPosition) * multiplier;
			          } 
					}
				}else{//PUT
					if (nettPosition >=0){ //PUT BUY
			          if (lastDone - averagePrice >=0) { result = 0; }
			          else { 
			        	  //result = (lastDone- averagePrice)* nettPosition* multiplier  //Kam Fai 01/08/2013
			        	  result = (averagePrice - lastDone) * nettPosition * multiplier ;
			          }
					}
			      else{//PUT SELL
			          if (lastDone - averagePrice >= averagePrice){
			            result = averagePrice * Math.abs(nettPosition)* multiplier;
			          }
			          else{
			        	  //result = (lastDone - averagePrice)* nettPosition * multiplier;
			        	  result = (lastDone - averagePrice) * Math.abs(nettPosition) * multiplier; //Kam Fai 01/08/2013
			          }
			      }
				}
				
				this.setUnrealisedPL(result);
			}
		}
	}
	
	public int getNettPosition() {
		return nettPosition;
	}

	public void setNettPosition(int nettPosition) {
		this.nettPosition = nettPosition;
	}

	public void calculateNettPosition(){
		this.setNettPosition(grossBuy-grossSell);
		
	}
	
	public float getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(float multiplier) {
		this.multiplier = multiplier;
	}

	public float getLastDone() {
		return lastDone;
	}

	public void setLastDone(float lastDone) {
		if(lastDone == 0.0)
			this.lastDone = refPrice;
		else
			this.lastDone= lastDone;
	}
	
	public float getRefPrice() {
		return refPrice;
	}

	public void setRefPrice(float refPrice) {
		this.refPrice = refPrice;
	}

	public String getDerivativeStockType() {
		return derivativeStockType;
	}

	public void setDerivativeStockType(String derivativeStockType) {
		this.derivativeStockType = derivativeStockType;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
		String lastDoneStr = productCode.substring(productCode.indexOf('~')+1); 
		setLastDone(Float.valueOf(lastDoneStr));
	}

	
	public String getHomeCurrency() {
		return homeCurrency;
	}

	
	public void setHomeCurrency(String homeCurrency) {
		this.homeCurrency = homeCurrency;
	}

	
	public float getForexExchangeRate() {
		return forexExchangeRate;
	}

	
	public void setForexExchangeRate(float forexExchangeRate) {
		this.forexExchangeRate = forexExchangeRate;
	}

	
	public String getCurrencySymbol() {
		return currencySymbol;
	}

	
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	
}
