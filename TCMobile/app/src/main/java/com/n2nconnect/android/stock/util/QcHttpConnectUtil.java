package com.n2nconnect.android.stock.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.QCBean;
import com.n2nconnect.android.stock.QcMessageParser;

public class QcHttpConnectUtil {
	// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
	private static final String strStartContentIndicator	= "\\[\\]";
	private static final String strEndContentIndicator		= "\\[END\\]";
	private static final String strNewLineIndicator			= "\r\n";
	// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
	
	public static String USERNAME;
	/*  Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static List<String> relogin() throws FailedAuthenicationException {

		String userParamKey = getKey();

		Map parameters = new HashMap();
		parameters.put(ParamConstants.USERPARAM_KEY_TAG, userParamKey);

		return login(parameters, null);

	}
	*/
	public static QCBean relogin() throws Exception {
		/* Mary@20130517 - Fixes_Request-20130424, ReqNo.9
		return login(getKey(), null);
		*/
		return login(AppConstants.brokerConfigBean.isGetNewKey() ? getNewKey() : getKey(), null);
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/*  Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String getKey() throws FailedAuthenicationException {

		try {

			URL url;  
			String userParamKey = null;

			if(AppConstants.isJavaQC == false){
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/GetKey");

				DefinitionConstants.Debug("Url Address :"+"http://" + AppConstants.QC_SERVER_IP + "/GetKey");

				byte[] bytes = readUrlData(url);

				userParamKey = SystemUtil.generateAESKey(bytes);

			}else{

				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/GetKey|||");

				DefinitionConstants.Debug("Url Address :"+"http://" + AppConstants.QC_SERVER_IP + "/GetKey|||");

				userParamKey = readUrlData2(url);
			}

			return userParamKey;
		} catch (MalformedURLException e) { 				
			e.printStackTrace();
		}  catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	*/
	
	public static String getKey() throws Exception{
		URL url				= null;  
		String userParamKey = null;
		/* Mary@20130605 - comment unused variable
		byte[] dataBytes	= null;
		*/
		String strUrlPath	= null;
		
		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);

			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlPath	= "http://" + AppConstants.QC_SERVER_IP + "/GetKey";
			*/
			strUrlPath	= "/GetKey";
			/* Mary@20130620 - To be configurable according to broker
			strUrlPath	+= AppConstants.isJavaQC ? "|||" : "";
			*/
			/* Mary@20130709 - Fixes_Request-20130523, ReqNo.28
			strUrlPath	+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : "";
			*/
			strUrlPath	+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : "?";
			// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.28
			strUrlPath	+= FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S");
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url			= new URL(strUrlPath);
			*/
			url			= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlPath);
			DefinitionConstants.Debug("[QcHttpConnectUtil][getKey]Url Address :" + url.toString() );
			
			/* Mary@20130620 - To be configurable according to broker
			userParamKey	= AppConstants.isJavaQC ? readUrlData2(url) : SystemUtil.generateAESKey( readUrlData(url) );
			*/
			userParamKey	= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url) : SystemUtil.generateAESKey( readUrlData(url) );
			DefinitionConstants.Debug("[QcHttpConnectUtil][getKey]userParamKey :" + userParamKey);
			
			 if( ErrorCodeConstants.isBackendExceptionMsg(userParamKey) )
				throw new FailedAuthenicationException(userParamKey);

			return userParamKey;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			/* Mary@20130605 - comment unused variable
			dataBytes	= null;
			*/
			url			= null;
			strUrlPath	= null;
		}
		return null;
	}
	//  Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	// Added by Mary@20130517 - Fixes_Request-20130424, ReqNo.9 - START
	public static String getNewKey() throws Exception{
		URL url				= null;  
		String userParamKey = null;
		String strUrlPath	= null;
		
		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);

			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlPath	= "http://" + AppConstants.QC_SERVER_IP + "/GetNewKey";
			*/
			strUrlPath	= "/GetNewKey";
			/* Mary@20130620 - To be configurable according to broker
			strUrlPath	+= AppConstants.isJavaQC ? "|||" : "";
			*/
			/* Mary@20130709 - Fixes_Request-20130523, ReqNo.28
			strUrlPath	+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : "";
			*/
			strUrlPath	+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : "?";
			// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.28
			strUrlPath	+= FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S");
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url			= new URL(strUrlPath);
			*/
			url			= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlPath);
			DefinitionConstants.Debug("[QcHttpConnectUtil][getNewKey]Url Address :" + url.toString() );
			
			/* Mary@20130620 - To be configurable according to broker
			String strResponse	= AppConstants.isJavaQC ? readUrlData2(url) : SystemUtil.generateAESKey( readUrlData(url) );
			*/
			String strResponse	= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url) : SystemUtil.generateAESKey( readUrlData(url) );
			DefinitionConstants.Debug("[QcHttpConnectUtil][getNewKey]response :" + strResponse);
			
			if( ErrorCodeConstants.isBackendExceptionMsg(strResponse) )
				throw new FailedAuthenicationException(strResponse);
			
			userParamKey		= QcMessageParser.parseGetNewKey(strResponse);
			DefinitionConstants.Debug("[QcHttpConnectUtil][getNewKey]userParamKey :" + userParamKey);
			
			return userParamKey;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			strUrlPath	= null;
		}
		return null;
	}
	// Added by Mary@20130517 - Fixes_Request-20130424, ReqNo.9 - END
	
	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static List<String> login(Map parameters, StringBuffer buffer) throws FailedAuthenicationException {

		String userParamKey = (String) parameters.get(ParamConstants.USERPARAM_KEY_TAG);

		URL url = null;

		List<String> data = null;

		try {

			if(AppConstants.isJavaQC == false){
				url = new URL("http://" +AppConstants.QC_SERVER_IP + "/[" + userParamKey +"]Login2?ClientApp=mobile&PullMode=1");

				DefinitionConstants.Debug("http://" +AppConstants.QC_SERVER_IP + "/[" + userParamKey +"]Login2?ClientApp=mobile&PullMode=1");

				byte[] bytes = readUrlData(url);
				if (buffer != null) {
					try {
						String response = new String(bytes, AppConstants.STRING_ENCODING);

						DefinitionConstants.Debug("sector: "+response);

						buffer.append(response);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
				String userParam = SystemUtil.extractUserParam2(bytes, "[UserParams]=", false);
				String aliveTimeOut = SystemUtil.extractQCTimeOut(bytes, "[AliveTimeOut]=", false);

				data = new ArrayList<String>();
				if(buffer!=null){
					data.add(buffer.toString());
				}
				data.add(userParam);
				data.add(aliveTimeOut);

			}else{

				if(AppConstants.qcCompress){
					url = new URL("http://" +AppConstants.QC_SERVER_IP + "/[" + userParamKey +"]Login2?ClientApp=mobile&UseEncryption=1&PullMode=1&&SortFields=33,38,48,50,51,56,57,68,200,58,190,170,88,220,78,210,180,98,99,125,101,41,135,235,236,234,123,238,237,239,241,242,49,55,102,69,59,171,89,79,181,70,60,172,90,80,182,71,61,173,91,81,183,72,62,174,92,82,184,153,156&|||");
				}else{
					url = new URL("http://" +AppConstants.QC_SERVER_IP + "/[" + userParamKey +"]Login2?ClientApp=mobile&PullMode=1&&SortFields=33,38,48,50,51,56,57,68,200,58,190,170,88,220,78,210,180,98,99,125,101,41,135,235,236,234,123,238,237,239,241,242,49,55,102,69,59,171,89,79,181,70,60,172,90,80,182,71,61,173,91,81,183,72,62,174,92,82,184,153,156&|||");
				}

				DefinitionConstants.Debug("http://" +AppConstants.QC_SERVER_IP + "/[" + userParamKey +"]Login2?ClientApp=mobile&PullMode=1&SortFields=33,38,48,50,51,56,57,68,200,58,190,170,88,220,78,210,180,98,99,125,101,41,135,235,236,234,123,238,237,239,241,242,49,55,102,69,59,171,89,79,181,70,60,172,90,80,182,71,61,173,91,81,183,72,62,174,92,82,184,153,156&|||");

				String response = readUrlData2(url);
				if (buffer != null) {
					buffer.append(response);
				}
				String userParam = SystemUtil.extractUserParam3(response, "[UserParams]=", false);
				String aliveTimeOut = SystemUtil.extractQCTimeOut2(response, "[AliveTimeOut]=", false);

				data = new ArrayList<String>();
				if(buffer!=null){
					data.add(buffer.toString());
				}
				data.add(userParam);
				data.add(aliveTimeOut);
			}

			return data;
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}*/
	public static QCBean login(String userParamKey, StringBuffer buffer) throws Exception {
		URL url 			= null;
		String response 	= null; 
		QCBean data 		= null;
		byte[] dataBytes	= null;
		String strUrlPath	= null;
		
		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);

			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlPath	= "http://" +AppConstants.QC_SERVER_IP + "/[" + userParamKey +"]Login2?ClientApp=mobile&PullMode=1";
			*/
			strUrlPath	= "/[" + userParamKey +"]Login2?ClientApp=mobile&PullMode=1";
			//MEL//strUrlPath	= "/[" + userParamKey +"]Login2?ClientApp=mobile&PullMode=1&ClientID="+USERNAME+"_"+AppConstants.brokerConfigBean.getStrAppName()+"_A";
			/* Mary@20130620 - To be configurable according to broker
			if( ! AppConstants.isJavaQC ){
			*/
			if( ! AppConstants.brokerConfigBean.isConnectJavaQC() ){
				/* Mary@20130801 - Change_Request-20130724, ReqNo.3
				url			= new URL(strUrlPath);
				*/
				url			= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlPath);
				
				DefinitionConstants.Debug("[QcHttpConnectUtil][login] url :" + url.toString() );
				
				/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
				dataBytes	= readUrlData(url);
				*/
				/* Mary@20130620 - To be configurable according to broker
				dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
				*/
				dataBytes 		= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
				//dataBytes 	= readUrlData2(url).getBytes();	//Fixes_Request-20170103, ReqNo.11
				
				if (buffer != null) {
					response	= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
					DefinitionConstants.Debug("[QcHttpConnectUtil][login]response: " + response);
					
					if( ErrorCodeConstants.isBackendExceptionMsg(response) )
						throw new Exception(response);

					buffer.append(response);
				}
				String userParam	= SystemUtil.extractUserParam2(dataBytes, "[UserParams]=", false);
				String aliveTimeOut = SystemUtil.extractQCTimeOut(dataBytes, "[AliveTimeOut]=", false);
				aliveTimeOut		= aliveTimeOut == null ? "0" : aliveTimeOut;

				data 				= new QCBean();
				if(buffer!=null)
					data.setStrRawParam(buffer.toString());
				data.setStrUserParam(userParam);
				data.setDblAliveTimeOut( Double.parseDouble(aliveTimeOut) );
				data.setStrUserParamKey(userParamKey);
			}else{
				strUrlPath	+= AppConstants.qcCompress ? "&UseEncryption=1" : "";
				/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
				strUrlPath	+= "&SortFields=33,38,48,50,51,56,57,68,200,58,190,170,88,220,78,210,180,98,99,125,101,41,135,235,236,234,123,238,237,239,241,242,49,55,102,69,59,171,89,79,181,70,60,172,90,80,182,71,61,173,91,81,183,72,62,174,92,82,184,153,156&|||";
				*/
				strUrlPath	+= "|||";
				/* Mary@20130801 - Change_Request-20130724, ReqNo.3
				url			= new URL(strUrlPath);
				*/
				url			= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlPath);
				DefinitionConstants.Debug( "[QcHttpConnectUtil][login] url : " + url.toString() );
				
				response 	= readUrlData2(url);
				DefinitionConstants.Debug( "[QcHttpConnectUtil][login] response : " + response);
				
				if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(response);
				
				if(buffer != null)
					buffer.append(response);
				
				String userParam 	= SystemUtil.extractUserParam3(response, "[UserParams]=", false);
				String aliveTimeOut = SystemUtil.extractQCTimeOut2(response, "[AliveTimeOut]=", false);
				aliveTimeOut		= aliveTimeOut == null ? "0" : aliveTimeOut;
				
				data 				= new QCBean();
				if(buffer!=null)
					data.setStrRawParam(buffer.toString());
				data.setStrUserParam(userParam);
				data.setDblAliveTimeOut( Double.parseDouble(aliveTimeOut) );
				data.setStrUserParamKey(userParamKey);
			}
			
			return data;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 		= null;
			response 	= null;
			dataBytes	= null;
		}

		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static Bitmap loadBitmap(String urlString) 
			throws FailedAuthenicationException {
		try {
			URL url = new URL(urlString);
			InputStream inputStream = url.openStream();

			//			byte[] dataBytes= readUrlData(url);
			//			int imageSize = dataBytes.length;
			//			BitmapFactory.Options options = new BitmapFactory.Options();
			//			Bitmap bitmap = BitmapFactory.decodeByteArray(dataBytes, 0, dataBytes.length, options);
			Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

			return bitmap;
		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	*/
	public static Bitmap loadBitmap(String urlString) throws Exception {
		URL url 				= null;
		InputStream inputStream = null;
		Bitmap bitmap 			= null;
		try {
			url 		= new URL(urlString);
			DefinitionConstants.Debug("[QcHttpConnectUtil][loadBitmap]url: " + url.toString() );
			inputStream = url.openStream();
			bitmap 		= BitmapFactory.decodeStream(inputStream);

			return bitmap;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 		= null;
			if(inputStream != null){
				inputStream.close();
				inputStream	= null;
			}
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static void QCAlive(Map parameters)throws FailedAuthenicationException {

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);

		URL url = null;

		if(AppConstants.isJavaQC==true){
			try{
				url = new URL("http://"+ AppConstants.QC_SERVER_IP + "/["+ userParam + "]Alive&|||");

				DefinitionConstants.Debug("QCAliveUrl: "+"http://"+ AppConstants.QC_SERVER_IP + "/["+ userParam + "]Alive&|||");

				byte[] dataBytes = readUrlData(url);

			}catch (MalformedURLException e) {			
				e.printStackTrace();
			}
		}else{
			try{
				url = new URL("http://"+ AppConstants.QC_SERVER_IP + "/["+ userParam + "]Alive");

				DefinitionConstants.Debug("QCAliveUrl: "+"http://"+ AppConstants.QC_SERVER_IP + "/["+ userParam + "]Alive&|||");

				byte[] dataBytes = readUrlData(url);

			}catch (MalformedURLException e) {			
				e.printStackTrace();
			}

		}
	}
	*/
	public static void QCAlive(String userParam) throws Exception{
		byte[] dataBytes	= null;
		URL url 			= null;
		String strUrlAddress= null;
		try{
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress	= "http://"+ AppConstants.QC_SERVER_IP + "/["+ userParam + "]Alive";
			*/
			strUrlAddress	= "/["+ userParam + "]Alive";
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress	+= AppConstants.isJavaQC ? "&|||" : "";
			*/
			/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
			strUrlAddress	+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "&|||" : "";
			*/
			strUrlAddress	+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : "";
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url 			= new URL(strUrlAddress);
			*/
			url			= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][QCAlive] url: " +  url.toString() );
			
			/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			dataBytes 		= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 		= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 		= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			DefinitionConstants.Debug("[QcHttpConnectUtil][QCAlive] response: " +  new String(dataBytes) );
		// NOTE : as this just work as heartbeat to server so no exception throw but boolean is return to have retry when heartbeat sending fail
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			url				= null;
			dataBytes		= null;
			strUrlAddress	= null;
		}
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String sectorFast(Map parameters, String exchgCode) throws FailedAuthenicationException {

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);

		CommandConstants.setCurrentExchgCode(exchgCode);

		URL url = null;

		byte[] dataBytes = null;

		String add = null;

		String response = null;

		try {

			if(AppConstants.isJavaQC == false){

				if(AppConstants.qcCompress){
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
							+ CommandConstants.getSectorCommand()+"&[COMPRESS]=1");
				}else{
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
							+ CommandConstants.getSectorCommand()+"&[COMPRESS]=0");
				}

				DefinitionConstants.Debug("URL: "+"http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
						+ CommandConstants.getSectorCommand()+"&[COMPRESS]=1");

				dataBytes = readUrlData(url);

				response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;

			}else{

				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/["+userParam+"]Sector?[FAST]="+exchgCode+"&[FIELD]=36,37,35,109,131|||");

				DefinitionConstants.Debug("URL: http://" + AppConstants.QC_SERVER_IP + "/["+userParam+"]Sector?[FAST]="+exchgCode+"&[FIELD]=36,37,35,109,131|||");

				dataBytes = readUrlData(url);

				response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;
			}

		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	*/
	public static String sectorFast(String exchgCode) throws Exception {	
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;

		try{
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			CommandConstants.setCurrentExchgCode(exchgCode);
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() +"]" + CommandConstants.getSectorCommand();
			*/
			strUrlAddress		= "/[" + AppConstants.QCdata.getStrUserParam() +"]" + CommandConstants.getSectorCommand();
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0") ;
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0") ;
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url 				= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][sectorFast]URL: " + url.toString() );
			
			/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			dataBytes 			= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][sectorFast]response: " + response );
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			if(response!=null){
				if(response.length() <= 9)
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
					return null;
				*/
					return "";
				
				/*  Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
				response	= response.substring(4, response.length() - 7); // first 4 is "[]/r/n", last 7 is "[END]/r/n"
				*/
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;
				
				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
				// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][sectorFast]final response : " + response );
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress 	= null;
		}
		return null;

	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String getIndices(Map parameters) throws FailedAuthenicationException {

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		URL url = null;

		try {

			if(AppConstants.isJavaQC == false){
				if(AppConstants.qcCompress){
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
							+ "]Indices?" + exchangeCode + "&[FIELD]=33,38,51,98,55,56,57,132&[DELTA]=0&[COMPRESS]=1");
				}else{
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
							+ "]Indices?" + exchangeCode + "&[FIELD]=33,38,51,98,55,56,57,132&[DELTA]=0&[COMPRESS]=0");
				}

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;

			}else{
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
						+ "]Indices?[FAST]=" + exchangeCode + "&[FIELD]=33,38,51,98,55,56,57,132&[DELTA]=0|||");

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;
			}

		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	*/
	public static String getIndices(String exchangeCode) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;

		try{
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() + "]Indices?";
			*/
			strUrlAddress		= "/[" + AppConstants.QCdata.getStrUserParam() + "]Indices?";
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? "[FAST]=" : "";
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "[FAST]=" : "";
			strUrlAddress		+= exchangeCode + "&[FIELD]=33,38,50,51,98,55,56,57,132&[DELTA]=0";		//Added by Thinzar, Change_Request-20141101, ReqNo 7 (added FID 50)
			strUrlAddress		+= AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0";
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? "|||" : "";
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : "";

			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url 				= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][getIndices]url: " + url.toString() );
			
			/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			dataBytes 			= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][getIndices]response: " + response );
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			if(response!=null){
				if(response.length() <= 9)
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
					return null;
					*/
					return "";
				
				/*  Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
				response = response.substring(4, response.length() - 7); // first 4 is "[]/r/n", last 7 is "[END]/r/n"
				*/
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;
				
				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
				// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][getIndices]final response : " + response );
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress	= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String imageSearch(Map parameters) throws FailedAuthenicationException {

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		String searchText = (String) parameters.get(ParamConstants.SEARCH_TEXT_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		String sectorCode = (String) parameters.get(ParamConstants.SECTORCODE_TAG);
		if (sectorCode == null) {
			sectorCode = AppConstants.DEFAULT_SECTOR_CODE;
		}
		URL url = null;

		try {

			if(AppConstants.isJavaQC == false){

				if(AppConstants.qcCompress){
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
							+ "]Image?[SEARCH]=0,16,1," + sectorCode + ",30,0," + searchText + "," + exchangeCode+"&[COMPRESS]=1");
				}else{
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
							+ "]Image?[SEARCH]=0,16,1," + sectorCode + ",30,0," + searchText + "," + exchangeCode+"&[COMPRESS]=0");
				}

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;

			}else{
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
						+ "]Image?[SEARCH]=0,16,1," + sectorCode + ",30,0," + searchText + "," + exchangeCode+"&[FIELD]=33,38&|||");

				DefinitionConstants.Debug("Url Address: "+"http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
						+ "]Image?[SEARCH]=0,16,1," + sectorCode + ",30,0," + searchText + "," + exchangeCode+"&[FIELD]=33,38&|||");

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if(response!=null){
						if (response.length() <= 9) {
							return null;
						}
						response = response.substring(4, response.length() - 7);
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
					
					if(response!=null){
						if (response.length() <= 9) {
							return null;
						}
						response = response.substring(4, response.length() - 7);
					}

				}

				return response;
			}


		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	*/
	public static String imageSearch(Map<String, String> parameters) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;
		
		String searchText	= parameters.get(ParamConstants.SEARCH_TEXT_TAG);
		String exchangeCode = parameters.get(ParamConstants.EXCHANGECODE_TAG);
		String sectorCode	= parameters.get(ParamConstants.SECTORCODE_TAG);
		if(sectorCode == null)
			sectorCode = AppConstants.DEFAULT_SECTOR_CODE;
		
		try{
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam()
			*/
			strUrlAddress		= "/[" + AppConstants.QCdata.getStrUserParam()
									/* Mary@20130204 - Fixes_Request-20130108, ReqNo.8
									+ "]Image?[SEARCH]=0,16,1," 
									*/
									+ "]Image?[SEARCH]=0,2,0,"
									+ sectorCode + ",30,0," + searchText + "," + exchangeCode;
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? 	"&[FIELD]=33,38&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			*/
			/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? 	"&[FIELD]=33,38&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? 	"&[FIELD]=33,38|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url					= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageSearch]Url Address: " + url.toString() );
			
			/* Mary@20130108 - Fixes_Request-20130108, ReqNo.1
			dataBytes 			= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageSearch]response: " + response );
						
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			if(response!=null){
				if(response.length() <= 9)
					return null;
				
				/*  Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
				response 		= response.substring(4, response.length() - 7); // first 4 is "[]/r/n", last 7 is "[END]/r/n"
				*/
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
				// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageSearch]final response : " + response );
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress	= null;
			searchText		= null;
			exchangeCode 	= null;
			sectorCode		= null;
		}
		return null;

	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	// Added by Mary@20130220 - Change_Request-20130104, ReqNo.5 - START
	public static String imageMSearch(Map<String, String> parameters) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;
		
		String searchText	= parameters.get(ParamConstants.SEARCH_TEXT_TAG);
		String exchangeCode = parameters.get(ParamConstants.EXCHANGECODE_TAG);
		
		try{
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam()
			*/
			strUrlAddress		= "/[" + AppConstants.QCdata.getStrUserParam()
									+ "]Image?[MSEARCH]=" + searchText 
									+ "&[EXCHANGE]=" + exchangeCode + "&[MARKET]=" + AppConstants.SELECTED_FEED_MARKET_CODE;	//edited by Thinzar, Change_Request-20150901, ReqNo.5
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? 	"&[FIELD]=33,38&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			*/
			/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? 	"&[FIELD]=33,38&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? 	"&[FIELD]=33,38|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url					= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageSearch]Url Address: " + url.toString() );
			
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageSearch]response: " + response );
						
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			if(response!=null){
				if(response.length() <= 9)
					return null;

				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageSearch]final response : " + response );
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress	= null;
			searchText		= null;
			exchangeCode 	= null;
		}
		return null;

	}
	// Added by Mary@20130220 - Change_Request-20130104, ReqNo.5 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String chartImage(Map parameters, int chartType) 
			throws FailedAuthenicationException {
		String symbolCode = (String) parameters.get(ParamConstants.SYMBOLCODE_TAG);

		try {
			String urlString = "http://" + AppConstants.CHART_SERVER_IP + 
					CommandConstants.CHART_IMAGE_COMMANDS[chartType] + symbolCode;

			if (chartType == CommandConstants.CHART_TYPE_HISTORICAL || 
					chartType == CommandConstants.CHART_TYPE_HISTORICAL_LANDSCAPE) {
				int duration = ((Integer) parameters.get(ParamConstants.CHART_DURATION_TAG)).intValue();
				urlString += "&d=" + String.valueOf(duration);
			}

			URL url = new URL(urlString);

			byte[] dataBytes = readUrlData(url);

			String response = new String(dataBytes, AppConstants.STRING_ENCODING);

			if (response.length() <= 24) {
				return null;
			}
			response = response.substring(12, response.length() - 12);			

			return response;
		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}
		return null;
	}
	*/
	public static String chartImage(Map<String, Object> parameters, int chartType) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;
		String chartServer	= null;		//Added by Thinzar@20141103, Change_Request-20140901, ReqNo. 14
		
		String symbolCode	= (String)parameters.get(ParamConstants.SYMBOLCODE_TAG);

		try {
			/* Mary@20130320 - Change_Request-20130325, ReqNo.4
			if(AppConstants.CHART_SERVER_IP == null)
			*/
			if(AppConstants.brokerConfigBean.getIntradayChartServer() == null || AppConstants.brokerConfigBean.getHistoricalChartServer() == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_CHART);
			
			/* Mary@20130320 - Change_Request-20130325, ReqNo.4
			strUrlAddress	= "http://" + AppConstants.CHART_SERVER_IP + CommandConstants.CHART_IMAGE_COMMANDS[chartType] + symbolCode;
			*/
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress	= "http://" + AppConstants.brokerConfigBean.getStrChartIP() + CommandConstants.CHART_IMAGE_COMMANDS[chartType] + symbolCode;
			*/
			strUrlAddress	= CommandConstants.CHART_IMAGE_COMMANDS[chartType] + symbolCode;
			if (chartType == CommandConstants.CHART_TYPE_HISTORICAL || chartType == CommandConstants.CHART_TYPE_HISTORICAL_LANDSCAPE) {
				int duration	= ( (Integer) parameters.get(ParamConstants.CHART_DURATION_TAG) ).intValue();
				strUrlAddress 	+= "&d=" + String.valueOf(duration);
			}

			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url 			= new URL(strUrlAddress);
			*/
			//Added by Thinzar@20141103, Change_Request-20140901, ReqNo. 14 - START
			switch(chartType){
			case CommandConstants.CHART_TYPE_INTRADAY:
				chartServer = AppConstants.brokerConfigBean.getIntradayChartServer();
				break;
			case CommandConstants.CHART_TYPE_INTRADAY_LANDSCAPE:
				chartServer = AppConstants.brokerConfigBean.getIntradayChartServer();
				break;
			case CommandConstants.CHART_TYPE_HISTORICAL:
				chartServer = AppConstants.brokerConfigBean.getHistoricalChartServer();
				break;
			case CommandConstants.CHART_TYPE_HISTORICAL_LANDSCAPE:
				chartServer = AppConstants.brokerConfigBean.getHistoricalChartServer();
				break;
			case CommandConstants.CHART_TYPE_MARKET_INTRADAY:
				chartServer = AppConstants.brokerConfigBean.getIntradayChartServer();
				break;
			}
			
			url				= new URL(chartServer + strUrlAddress);

			//url				= new URL("http", AppConstants.brokerConfigBean.getStrChartIP(), 80, strUrlAddress);	//commented out by Thinzar
			//Added by Thinzar@20141103, Change_Request-20140901, ReqNo. 14 - END
			
			DefinitionConstants.Debug("[QcHttpConnectUtil][chartImage]Url Address: " + url.toString() );
			
			/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			dataBytes 		= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 		= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 		= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 		= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][chartImage]response: " + response );
			
			if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			if (response.length() <= 24)
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				return null;
			*/
				return "";
			
			response 		= response.substring(12, response.length() - 12);			

			return response;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress	= null;
			symbolCode		= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String newsAll(Map parameters) throws FailedAuthenicationException {

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);

		URL url = null;

		try {
			if(AppConstants.isJavaQC == false){

				if(AppConstants.qcCompress){
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
							+ "]News?" + exchangeCode+"&[COMPRESS]=1");
				}else{
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
							+ "]News?" + exchangeCode+"&[COMPRESS]=0");
				}

				DefinitionConstants.Debug("News: "+"http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
						+ "]News?" + exchangeCode);

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if (response.length() <= 9) {
					return null;
				}

				return response.toString().substring(4, response.length() - 7);			

			}else{
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
						+ "]News?" + exchangeCode+"|||");

				DefinitionConstants.Debug("News: "+"http://" + AppConstants.QC_SERVER_IP + "/[" + userParam 
						+ "]News?" + exchangeCode+"|||");

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;
			}
		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}
		return null;

	}
	*/
	public static String newsAll(String exchangeCode) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;

		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() + "]News?" + exchangeCode;
			*/
			strUrlAddress		= "/[" + AppConstants.QCdata.getStrUserParam() + "]News?" + exchangeCode;
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url					= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][newsAll]url : " + url.toString() );
			
			/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			dataBytes 			= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][newsAll]response : " + response );
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			if(response!=null){
				if(response.length() <= 9)
					return null;

				/*  Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
				response = response.substring(4, response.length() - 7); // first 4 is "[]/r/n", last 7 is "[END]/r/n"
				*/
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
				// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][newsAll]final response : " + response );

			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress	= null;
		}
		return null;

	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String newsUpdate(Map parameters) throws FailedAuthenicationException {

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		String symbolCode = (String) parameters.get(ParamConstants.SYMBOLCODE_TAG);
		URL url = null;

		try {
			if(AppConstants.isJavaQC == false){

				if(AppConstants.qcCompress){
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]News?," 
							+ symbolCode+"&[COMPRESS]=1");
				}else{
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]News?," 
							+ symbolCode+"&[COMPRESS]=0");
				}

				DefinitionConstants.Debug("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]News?," 
						+ symbolCode);

				byte[] dataBytes = readUrlData(url);

				String response = null;
				
				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if (response.length() <= 9) {
					return null;
				}
				response = response.substring(4, response.length() - 7);			

				return response;

			}else{
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]News?," 
						+ symbolCode+"|||");

				DefinitionConstants.Debug("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]News?," 
						+ symbolCode);

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;
			}

		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}
		return null;

	}
	*/
	public static String newsUpdate(String symbolCode) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;
		
		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() + "]News?," + symbolCode;
			*/
			strUrlAddress		= "/[" + AppConstants.QCdata.getStrUserParam() + "]News?," + symbolCode;
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress 		+= AppConstants.isJavaQC ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			*/
			strUrlAddress 		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url					= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][newsUpdate]url: " + url.toString() );
			
			/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			dataBytes 			= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][newsUpdate]response: " + response );
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);

			if(response!=null){
				if(response.length() <= 9)
					return null;
				
				/*  Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
				response = response.substring(4, response.length() - 7); // first 4 is "[]/r/n", last 7 is "[END]/r/n"
				*/
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
				// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][newsUpdate]final response : " + response );
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress	= null;
		}
		return null;

	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String imageMarket(Map parameters) throws FailedAuthenicationException {

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		String symbolCode = (String) parameters.get(ParamConstants.SYMBOLCODE_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		URL url = null;

		if(exchangeCode.equals("MY")){
			exchangeCode = "KL";
		}

		try {

			if(AppConstants.isJavaQC == false){

				if(AppConstants.qcCompress){
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
							+ CommandConstants.MARKET_SORT_COMMAND + "," + exchangeCode + ",1," + symbolCode + "&[FIELD]=" 
							+ CommandConstants.MARKET_SORT_FIELDS + "&[SUMMARY]=" + exchangeCode + "&[DELTA]=0&[COMPRESS]=1");
				}else{
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
							+ CommandConstants.MARKET_SORT_COMMAND + "," + exchangeCode + ",1," + symbolCode + "&[FIELD]=" 
							+ CommandConstants.MARKET_SORT_FIELDS + "&[SUMMARY]=" + exchangeCode + "&[DELTA]=0&[COMPRESS]=0");
				}

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;

			}else{
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
						+ CommandConstants.MARKET_SORT_COMMAND + "," + exchangeCode + ",1," + symbolCode + "&[FIELD]=" 
						+ CommandConstants.MARKET_SORT_FIELDS + "&[SUMMARY]=" + exchangeCode + "&[DELTA]=0|||");

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;
			}

		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;					
	}
	*/
	public static String imageMarket(Map<String, String> parameters) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;
		
		String symbolCode 	= parameters.get(ParamConstants.SYMBOLCODE_TAG);
		String exchangeCode = parameters.get(ParamConstants.EXCHANGECODE_TAG);
		
		/* Mary@20130312 - Fixes_Request-20130108, ReqNo.19
		if( exchangeCode.equals("MY") )
		*/
		if( exchangeCode.equals(AppConstants.MY_EXCHANGE_CODE) )
			exchangeCode	= "KL";

		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress 		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() +"]"
			*/
			strUrlAddress 		= "/[" + AppConstants.QCdata.getStrUserParam() +"]"
									+ CommandConstants.MARKET_SORT_COMMAND + "," + exchangeCode + ",1," + symbolCode + "&[FIELD]=" 
									+ CommandConstants.MARKET_SORT_FIELDS + "&[SUMMARY]=" + exchangeCode + "&[DELTA]=0";
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url					= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageMarket]url: " + url.toString() );
			
			/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			dataBytes 			= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageMarket]response: " + response );
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);

			if(response!=null){
				if (response.length() <= 9)
					return null;

				/*  Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
				response = response.substring(4, response.length() - 7); // first 4 is "[]/r/n", last 7 is "[END]/r/n"
				*/
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
				// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageMarket]final response : " + response );

			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress	= null;
			symbolCode		= null;
			exchangeCode	= null;
		}
		return null;					
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static int imageCount(Map parameters) throws FailedAuthenicationException {
		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		String sectorCode = (String) parameters.get(ParamConstants.SECTORCODE_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);

		URL url = null;

		try {	
			if(AppConstants.isJavaQC == false){
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]Image?[COUNT]=" 
						+ sectorCode + "," + exchangeCode);

				byte[] dataBytes = readUrlData(url);

				String response = new String(dataBytes, AppConstants.STRING_ENCODING);
				String[] tokens = response.split("=");
				if (tokens.length >= 2) {
					return Integer.parseInt(tokens[1]);
				}

			}else{
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]Image?[COUNT]=" 
						+ sectorCode + "," + exchangeCode+"|||");

				String response = readUrlData2(url);

				String[] tokens = response.split("=");
				if (tokens.length >= 2) {
					return Integer.parseInt(tokens[1]);
				}
			}
		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}
		return 0;

	}
	*/
	/* Mary@20130312 - Fixes_Request-20120724, ReqNo.2
	public static int imageCount(Map parameters) throws Exception {
	*/
	public static String imageCount(Map<String, String> parameters) throws Exception {
		URL url 			= null;
		String response 	= null;
		String strUrlAddress= null;
		
		String sectorCode 	= parameters.get(ParamConstants.SECTORCODE_TAG);
		String exchangeCode = parameters.get(ParamConstants.EXCHANGECODE_TAG);

		try{
			// Added by  Mary@20130329 - Fixes_Request-20130314, ReqNo.7 - START
			if(sectorCode == null || ( sectorCode != null && sectorCode.trim().equals("10000") ) )
				sectorCode	= AppConstants.DEFAULT_SECTOR_CODE;
			// Added by  Mary@20130329 - Fixes_Request-20130314, ReqNo.7 - END
						
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress		=  "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() +"]Image?[COUNT]=" + sectorCode + "," + exchangeCode;
			*/
			strUrlAddress		=  "/[" + AppConstants.QCdata.getStrUserParam() +"]Image?[COUNT]=" + sectorCode + "," + exchangeCode;
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? "|||" : "";
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : "";
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url 				= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageCount]url: " + url.toString() );
			
			/* Mary@20130620 - To be configurable according to broker
			response			= AppConstants.isJavaQC ? readUrlData2(url) : new String(readUrlData(url), AppConstants.STRING_ENCODING);
			*/
			response			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url) : new String(readUrlData(url), AppConstants.STRING_ENCODING_FORMAT);
			// Added by Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(response.getBytes(), "", "a").toString() : response;
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageCount]response: " + response );
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
			String[] strArr	= response.split(strStartContentIndicator); 
			response		= strArr.length > 1 ? strArr[1] : strArr[0];
			response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;
			
			strArr			= response.split(strEndContentIndicator);
			response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
			// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
			
			String[] tokens = response.split("=");
			if(tokens.length >= 2)
				/* Mary@20130312 - Fixes_Request-20120724, ReqNo.2
				return Integer.parseInt(tokens[1]);
				*/
				return tokens[1];
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			response 		= null;
			strUrlAddress	= null;
			sectorCode		= null;
			exchangeCode	= null;
		}
		/* Mary@20130312 - Fixes_Request-20120724, ReqNo.2
		return 0;
		*/
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String imageSort(Map parameters, int sortType) throws FailedAuthenicationException {

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		String sectorCode = (String) parameters.get(ParamConstants.SECTORCODE_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		URL url = null;

		if (sectorCode == null) {
			sectorCode = AppConstants.DEFAULT_SECTOR_CODE;
		}

		try {
			String command = CommandConstants.IMAGE_SORT_COMMANDS[sortType];

			command = command.replace("%SECTOR", sectorCode);
			command = command.replace("%EXCHANGE", exchangeCode);
			command = command.replace("%COUNT", String.valueOf(AppConstants.DEFAULT_SORT_COUNT)); 

			if(AppConstants.isJavaQC == false){

				if(AppConstants.qcCompress){
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
							+ command + "&[FIELD]=" + CommandConstants.IMAGE_SORT_FIELDS 
							+ "&[DELTA]=0&[MARKET]=10&[COMPRESS]=1");
				}else{
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
							+ command + "&[FIELD]=" + CommandConstants.IMAGE_SORT_FIELDS 
							+ "&[DELTA]=0&[MARKET]=10&[COMPRESS]=0");
				}

				DefinitionConstants.Debug("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
						+ command + "&[FIELD]=" + CommandConstants.IMAGE_SORT_FIELDS 
						+ "&[DELTA]=0&[MARKET]=10");

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;

			}else{
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
						+ command + "&[FIELD]=" + CommandConstants.IMAGE_SORT_FIELDS 
						+ "&[DELTA]=0&[MARKET]=10&|||");

				DefinitionConstants.Debug("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam +"]" 
						+ command + "&[FIELD]=" + CommandConstants.IMAGE_SORT_FIELDS 
						+ "&[DELTA]=0&[MARKET]=10&|||");

				byte[] dataBytes = readUrlData(url);
				
				DefinitionConstants.Debug(new String(dataBytes,AppConstants.STRING_ENCODING));

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;
			}
		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;					
	}
	*/
	
	//Edited by Thinzar@20150515, Change_Request-20150101, ReqNo. 2 [added boolean isDerivativeExchg]
	public static String imageSort(Map<String, String> parameters, int sortType, boolean isDerivativeExchg) throws Exception {
		// Added by Mary@20131004 - eliminate unneccessary global static variable
		final int DEFAULT_SORT_COUNT = 20;
		
		URL url 					= null;
		byte[] dataBytes 			= null;
		String response 			= null;
		String strUrlAddress		= null;
		
		String sectorCode 			= parameters.get(ParamConstants.SECTORCODE_TAG);
		String exchangeCode 		= parameters.get(ParamConstants.EXCHANGECODE_TAG);
		String marketCode			= parameters.get(ParamConstants.MARKETCODE_TAG);	//Added by thinzar@20141027, Change_Request-20140901, ReqNo.12
		
		//Added by Thinzar@20150515, Change_Request-20150101, ReqNo. 2 - START
		String strImgSortFields		= CommandConstants.IMAGE_SORT_FIELDS;
		if(isDerivativeExchg){
			sortType				= CommandConstants.IMAGE_SORT_DERIVATIVES;
			strImgSortFields		= CommandConstants.IMAGE_SORT_DER_FIELDS;
		}
		//Added by Thinzar@20150515, Change_Request-20150101, ReqNo. 2 - END
		
		try {
			/* Mary@20130329 - Fixes_Request-20130314, ReqNo.7
			if(sectorCode == null)
			*/
			if(sectorCode == null || ( sectorCode != null && sectorCode.trim().equals("10000") ) )
				sectorCode = AppConstants.DEFAULT_SECTOR_CODE;
			
			//Added by thinzar@20141027, Change_Request-20140901, ReqNo.12
			if(marketCode == null)	marketCode	= AppConstants.DEFAULT_FEED_MARKET_CODE;
	
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			String command 	= CommandConstants.IMAGE_SORT_COMMANDS[sortType];
			command 		= command.replace("%SECTOR", sectorCode);
			command 		= command.replace("%EXCHANGE", exchangeCode);
			/* Mary@20131004 - eliminate unneccessary global static variable
			command 		= command.replace("%COUNT", String.valueOf(AppConstants.DEFAULT_SORT_COUNT) );
			*/
			command 		= command.replace("%COUNT", String.valueOf(DEFAULT_SORT_COUNT) );
			//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
			command         = command.replace("%PAGE",String.valueOf(ParamConstants.PAGE_NUMBER));
			//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() +"]" + command + "&[FIELD]=" + CommandConstants.IMAGE_SORT_FIELDS
			*/
			strUrlAddress		= "/[" + AppConstants.QCdata.getStrUserParam() +"]" + command + "&[FIELD]=" + strImgSortFields
								+ "&[DELTA]=0&[MARKET]=" + marketCode; 	//edited by Thinzar, added parameter marketCode, Change_Request-20140901, ReqNo.12
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? "&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			*/
			/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "&[COMPRESS]=0" );
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url					= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageSort] url : " + url.toString() );
			
			/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			dataBytes 			= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageSort] response : " + response );
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			if(response!=null){
				if(response.length() <= 9)
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
					return null;
					*/
					return "";

				/*  Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
				response = response.substring(4, response.length() - 7); // first 4 is "[]/r/n", last 7 is "[END]/r/n"
				*/
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
				// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageSort]final response : " + response );

			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			response 		= null;
			strUrlAddress	= null;
			sectorCode		= null;
			exchangeCode	= null;
		}
		return null;					
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String imageQuote(Map parameters, int quoteType) 
			throws FailedAuthenicationException {

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		String[] symbolCodes = (String[]) parameters.get(ParamConstants.SYMBOLCODES_TAG);	
		URL url = null;

		try {
			StringBuffer symbolList = new StringBuffer();
			for (int i=0; i<symbolCodes.length; i++) {
				symbolList.append(symbolCodes[i]);
				if (i != symbolCodes.length - 1) {
					symbolList.append(",");
				}
			}

			if(AppConstants.isJavaQC == false){
				if(AppConstants.qcCompress){
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam + 
							"]Image?[LSQUOTE]=" + symbolList.toString() +  "&[FIELD]=" + CommandConstants.IMAGE_QUOTE_FIELDS[quoteType]
									+ "&[DELTA]=0&[COMPRESS]=1");
				}else{
					url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam + 
							"]Image?[LSQUOTE]=" + symbolList.toString() +  "&[FIELD]=" + CommandConstants.IMAGE_QUOTE_FIELDS[quoteType]
									+ "&[DELTA]=0&[COMPRESS]=0");
				}

				DefinitionConstants.Debug("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam + 
						"]Image?[LSQUOTE]=" + symbolList.toString() +  "&[FIELD]=" + CommandConstants.IMAGE_QUOTE_FIELDS[quoteType]
								+ "&[DELTA]=0&[COMPRESS]=0");

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;


			}else{
				url = new URL("http://" + AppConstants.QC_SERVER_IP + "/[" + userParam + 
						"]Image?[LSQUOTE]=" + symbolList.toString() +  "&[FIELD]=" + CommandConstants.IMAGE_QUOTE_FIELDS[quoteType]
								+ "&[DELTA]=0&|||");

				DefinitionConstants.Debug("MEssage: "+"http://" + AppConstants.QC_SERVER_IP + "/[" + userParam + 
						"]Image?[LSQUOTE]=" + symbolList.toString() +  "&[FIELD]=" + CommandConstants.IMAGE_QUOTE_FIELDS[quoteType]
								+ "&[DELTA]=0&|||");

				byte[] dataBytes = readUrlData(url);

				String response = null;

				if(AppConstants.qcCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING);
				}

				if(response!=null){
					if (response.length() <= 9) {
						return null;
					}
					response = response.substring(4, response.length() - 7);
				}

				return response;
			}

		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;					
	}
	*/ 
	
	//Added by Kw@20130225 - Change_Request20130104, ReqNo.6 START
	public static String imageQuote2(String symbolCodes, int marketdepthlevel) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;

		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress 		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() + "]Image?[BIDASK]=" + symbolCodes
			*/
			strUrlAddress 		= "/[" + AppConstants.QCdata.getStrUserParam() + "]Image?[BIDASK]=" + symbolCodes
									+ "&[LEVEL]=" + marketdepthlevel;
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? "&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "[COMPRESS]=0" );
			*/
			/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "[COMPRESS]=0" );
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "[COMPRESS]=0" );
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url					= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageQuote2] url response: " + url.toString());

			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageQuote2] depth response : " + response);
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);

			if(response!=null){
				if(response.length() <= 9)
			
					return "";
				
			
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
				
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageQuote2]final response2 : " + response );
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			strUrlAddress	= null;
		}
		return null;					
	}
	//Added by Kw@20130225 - Change_Request20130104, ReqNo.6 END
	
	public static String imageQuote(String[] symbolCodes, int quoteType) throws Exception {
		
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;

		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			StringBuffer symbolList	= new StringBuffer();
			for(int i=0; i<symbolCodes.length; i++){
				symbolList.append(symbolCodes[i]);
				if(i != symbolCodes.length - 1)
					symbolList.append(",");
			}
			
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			strUrlAddress 		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() + "]Image?[LSQUOTE]=" + symbolList.toString()
			*/
			strUrlAddress 		= "/[" + AppConstants.QCdata.getStrUserParam() + "]Image?[LSQUOTE]=" + symbolList.toString()
									+ "&[FIELD]=" + CommandConstants.IMAGE_QUOTE_FIELDS[quoteType] + "&[DELTA]=0";
			
			//Added by Thinzar, Change_Request-20160722, ReqNo.2
			if(AppConstants.brokerConfigBean.isEnableScreener()){
				strUrlAddress		+= "&[FIELD17]=152,153";		
			}
			
			/* Mary@20130620 - To be configurable according to broker
			strUrlAddress		+= AppConstants.isJavaQC ? "&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "[COMPRESS]=0" );
			*/
			/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "[COMPRESS]=0" );
			*/
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "[COMPRESS]=0" );
			/* Mary@20130801 - Change_Request-20130724, ReqNo.3
			url					= new URL(strUrlAddress);
			*/
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageQuote] url : " + url.toString());
			/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
			dataBytes 			= readUrlData(url);
			*/
			/* Mary@20130620 - To be configurable according to broker
			dataBytes 			= AppConstants.isJavaQC ? readUrlData2(url).getBytes() : readUrlData(url);
			*/
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageQuote] response : " + response);
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);

			if(response!=null){
				if(response.length() <= 9)
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
					return null;
				*/
					return "";
				
				/*  Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - START
				response = response.substring(4, response.length() - 7); // first 4 is "[]/r/n", last 7 is "[END]/r/n"
				*/
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
				// Added by Mary@20130108 - Fixes_Request-20130108, ReqNo.1 - END
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageQuote]final response : " + response );
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			strUrlAddress	= null;
		}
		return null;					
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private static String readUrlData2(URL url) throws FailedAuthenicationException {

		InputStream is = null;
		byte[] abtData = null;

		try {
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			is = urlConnection.getInputStream();

			int ch = -1;
			StringBuffer buf = new StringBuffer();
			while ((ch = is.read()) != -1) {
				buf.append((char)ch);

				if(buf.indexOf("[END]")!=-1){
					break;
				}
			}
			return buf.toString();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				is = null;
			}
		}
		return null;
	}
	*/
	
	private static String readUrlData2(URL url) throws Exception {
		HttpURLConnection urlConnection	= null;
		InputStream inputStream 		= null;
		
		try {
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - START 
			// HTTP connection reuse which was buggy pre-froyo
			if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO)
			     System.setProperty("http.keepAlive", "false");
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - END
			
			urlConnection		= (HttpURLConnection) url.openConnection();
			
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - START
			urlConnection.setRequestMethod("GET");
			/* Mary@20130925 - Change_Request-20130724, ReqNo.16
			urlConnection.setConnectTimeout(5000);
			*/
			urlConnection.setConnectTimeout(8000);
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - END
			//DefinitionConstants.Debug("debug: readurldata2() responseCode=" + urlConnection.getResponseCode());
			inputStream			= urlConnection.getInputStream();
			
			/* Mary@20130131 - Fixes_Request-20130108, ReqNo.1 - START
			int ch 				= -1;
			StringBuffer buf 	= new StringBuffer();
			while ( ( ch = inputStream.read() ) != -1 ) {
				buf.append( (char)ch );
				/* Mary@20130109 - Fixes_Request-20130108, ReqNo.1
				if(buf.indexOf("[END]")!=-1)
				
				if(buf.indexOf( strEndContentIndicator.replaceAll("\\\\", "") )!=-1)
					break;
			}
			
			return buf.toString();
			*/
			int length							= 0;
			/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
			int size 							= 1024;
			*/
			int size							= inputStream.available() > 0 ? inputStream.available() : 1024;
			byte[] dataBytes 					= null;
			ByteArrayOutputStream outputStream 	= null;
			if(inputStream instanceof ByteArrayInputStream){
				/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
				size		= inputStream.available();
				*/
				dataBytes	= new byte[size];
				length		= inputStream.read(dataBytes, 0, size);
			}else{
				/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
				size		= inputStream.available();
				*/
				outputStream= new ByteArrayOutputStream();
				dataBytes 	= new byte[size];
				
				while( (length = inputStream.read(dataBytes, 0, size) ) != -1 ){
					outputStream.write(dataBytes, 0, length);
					
					if( new String(dataBytes).indexOf("[END]") != -1 )
						break;
				}

				dataBytes	= outputStream.toByteArray();
			}
			
			return new String(dataBytes);
			// Mary@20130131 - Fixes_Request-20130108, ReqNo.1 - END
		}catch(EOFException eof){
			eof.printStackTrace();
			throw new EOFException(ErrorCodeConstants.FAIL_COMMUNICATION_EOF);
		}catch (IOException ioe){
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				throw ioe;
			else{
				
				//Added by Thinzar@20141119, Change_Request-20141101, ReqNo. 14 - START
				DefinitionConstants.Debug("debug: readurldata2() responseCode=" + urlConnection.getResponseCode());
				DefinitionConstants.Debug("debug: readurldata2() errorStream=" + urlConnection.getErrorStream());
				if(urlConnection.getResponseCode() > 400) AppConstants.QCdata	= relogin();	//it returns 401 when QC timeout, so in order to be safe, i will relogin if it returns response code above 400
				//Added by Thinzar@20141119, Change_Request-20141101, ReqNo. 14 - END
				
				throw new IOException(ErrorCodeConstants.FAIL_COMMUNICATION_IOE);
			}
		}catch(NullPointerException npe){
			npe.printStackTrace();
			throw new NullPointerException(ErrorCodeConstants.FAIL_COMMUNICATION_NPE);
		}catch (Exception e){
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			if(inputStream != null){
				inputStream.close();
				inputStream = null;
			}
			if(urlConnection != null){
				urlConnection.disconnect();
				urlConnection	= null;
			}
		}
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private static byte[] readUrlData(URL url) throws FailedAuthenicationException {
		int length = 0;
		int size = 1024;
		byte[] dataBytes = null;
		InputStream inputStream = null;
		ByteArrayOutputStream outputStream = null;
		URL urlAdd = url;

		try {
			HttpURLConnection conn = (HttpURLConnection) urlAdd.openConnection();
			inputStream = conn.getInputStream() ;

			if (inputStream instanceof ByteArrayInputStream) {
				size = inputStream.available();

				dataBytes = new byte[size];
				length = inputStream.read(dataBytes, 0, size);
			} else {

				size = inputStream.available();

				outputStream = new ByteArrayOutputStream();
				dataBytes = new byte[size];
				while ((length = inputStream.read(dataBytes, 0, size)) != -1) {
					outputStream.write(dataBytes, 0, length);
				}
				dataBytes = outputStream.toByteArray();
			}

			inputStream.close();
			outputStream.close();
			conn.disconnect();

			return dataBytes;
		} catch (IOException e) {
			e.printStackTrace();
			throw new FailedAuthenicationException(e.getMessage());
		}

	}
	*/
	private static byte[] readUrlData(URL url) throws Exception {
		int length							= 0;
		int size 							= 1024;
		byte[] dataBytes 					= null;
		InputStream inputStream 			= null;
		ByteArrayOutputStream outputStream 	= null;
		URL urlAdd 							= url;
		HttpURLConnection urlConnection		= null;

		try {
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - START 
			if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO)
			     System.setProperty("http.keepAlive", "false");
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - END
			
			urlConnection 	= (HttpURLConnection) urlAdd.openConnection();
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - START
			urlConnection.setRequestMethod("GET");
			/* Mary@20130925 - Change_Request-20130724, ReqNo.16
			urlConnection.setConnectTimeout(5000);
			*/
			urlConnection.setConnectTimeout(8000);
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - END
			DefinitionConstants.Debug("debug: readurldata() responseCode=" + urlConnection.getResponseCode());			
			inputStream		= urlConnection.getInputStream() ;

			// Added by Mary@20130923 - Fixes_Request-20130711, ReqNo.20
			size			= inputStream.available() > 0 ? inputStream.available() : 1024;
			if(inputStream instanceof ByteArrayInputStream){
				/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
				size		= inputStream.available();
				*/
				dataBytes	= new byte[size];
				length		= inputStream.read(dataBytes, 0, size);
			}else{
				/* Mary@20130923 - Fixes_Request-20130711, ReqNo.20
				size		= inputStream.available();
				*/
				outputStream= new ByteArrayOutputStream();
				dataBytes 	= new byte[size];
				
				while( (length = inputStream.read(dataBytes, 0, size) ) != -1 )
					outputStream.write(dataBytes, 0, length);

				dataBytes	= outputStream.toByteArray();
			}
			
			return dataBytes;
		}catch(EOFException eof){
			eof.printStackTrace();
			throw new EOFException(ErrorCodeConstants.FAIL_COMMUNICATION_EOF);
		}catch (IOException ioe){
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				throw ioe;
			else
				throw new IOException(ErrorCodeConstants.FAIL_COMMUNICATION_IOE);
		}catch(NullPointerException npe){
			npe.printStackTrace();
			throw new NullPointerException(ErrorCodeConstants.FAIL_COMMUNICATION_NPE);
		}catch (Exception e){
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			if(outputStream != null){
				outputStream.close();
				outputStream = null;
			}
			if(inputStream != null){
				inputStream.close();
				inputStream = null;
			}
			if(urlConnection != null){
				urlConnection.disconnect();
				urlConnection	= null;
			}
			urlAdd		= null;
			size		= 0;
			length		= 0;
		}

	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	//Added by Sonia@20130702 - Change_Request20130225, ReqNo.5 START
	public static String transactionTimeSales(String symbolCodes, int indexFrom, int indexTo ) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;

		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			//Added by Thinzar, to avoid delay feed not getting timeSales
			symbolCodes				= (symbolCodes.length() > 1 && symbolCodes.toUpperCase().endsWith("D") ) ? symbolCodes.substring(0, (symbolCodes.length() - 1) )  : symbolCodes;		
			
			strUrlAddress 		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() + "]TRANSACT?[T]=" + symbolCodes
									+ ",0,0,0," + indexFrom + "," + indexTo;
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "[COMPRESS]=0" );
			url					= new URL(strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][transactionTimeSales] url response: " + url.toString());
			
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			
			DefinitionConstants.Debug("[QcHttpConnectUtil][transactionTimeSales] Transac response : " + response);
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			strUrlAddress	= null;
		}
		return null;					
	}
	//Added by Sonia@20130702 - Change_Request20130225, ReqNo.5 END
	
	//Added by Sonia@20130702 - Change_Request20130225, ReqNo.5 START
	public static String getBusinessDoneFeed(String symbolCodes) throws Exception {
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;

		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			//Added by Thinzar, to avoid delay feed not getting timeSales
			symbolCodes				= (symbolCodes.length() > 1 && symbolCodes.toUpperCase().endsWith("D") ) ? symbolCodes.substring(0, (symbolCodes.length() - 1) )  : symbolCodes;		
			
			strUrlAddress 		= "http://" + AppConstants.QC_SERVER_IP + "/[" + AppConstants.QCdata.getStrUserParam() + "]BusinessDone?,," + symbolCodes
									+ ",10,10";
			strUrlAddress		+= AppConstants.brokerConfigBean.isConnectJavaQC() ? "&|||" : ( AppConstants.qcCompress ? "&[COMPRESS]=1" : "[COMPRESS]=0" );
			url					= new URL(strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][getBusinessDoneFeed] url response: " + url.toString());
			
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			
			DefinitionConstants.Debug("[QcHttpConnectUtil][getBusinessDoneFeed] BusinessDoneFeed response : " + response);
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			strUrlAddress	= null;
		}
		return null;					
	}
	// Added by Sonia@20130702 - Change_Request20130225, ReqNo.5 END
	
	//Added by Thinzar@20141113, Change_Request-20141101, ReqNo. 6 - START
	public static String doImageRequest06(String symbolCode) throws Exception {
		
		URL url 				= null;
		byte[] dataBytes		= null;
		String response			= null;
		String strUrlParams		= null;
		String qcExchangeCode	= null;
		
		try{
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			qcExchangeCode	= symbolCode.substring( (symbolCode.lastIndexOf(".") + 1), symbolCode.length() );
			
			//http://49.236.201.97/[c38419b10479459fa6c1ed45ef40ff64]Image6?[EXCHANGE]=KL&[LSQUOTE]=2607.KL&[FIELD]=33,56,57,232,231,191|||
			strUrlParams	= "/[" + AppConstants.QCdata.getStrUserParam() + "]Image6?[EXCHANGE]=" +  qcExchangeCode + "&[LSQUOTE]=" + symbolCode + "&[FIELD]=" + "33,56,57,232,231,191|||";
			
			url				= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlParams);
			DefinitionConstants.Debug("[QcHttpConnectUtil][ImageQuote6]url:" + url.toString());
			
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageQuote6]response : " + response);
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);

			if(response!=null){
				if(response.length() <= 9)
					return "";
				
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][imageQuote6]final response : " + response );
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			strUrlParams	= null;
		}
		
		return null;	
	}
	//Added by Thinzar@20141113, Change_Request-20141101, ReqNo. 6 - END

	//Fixes_Request-20170103, ReqNo.11
	public static String getAllNewsCategories() throws Exception {

		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;

		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);

			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();

			//http://118.201.69.222/[67f95c80d3f64e1896b9a2b154041d1d]NEWSCATEGORY?[SOURCE]=999|||
			strUrlAddress		= "/[" + AppConstants.QCdata.getStrUserParam() + "]NEWSCATEGORY?[SOURCE]=999|||";

			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][NEWSCATEGORY]url: " + url.toString() );

			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][NEWSCATEGORY]response: " + response );

			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);

			if(response!=null){
				if(response.length() <= 9)
					return null;

				String[] strArr	= response.split(strStartContentIndicator);
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][NEWSCATEGORY]final response : " + response );

			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress	= null;
		}
		return null;
	}

	public static String getV2News(String keyword, String target, String guid, String category, String source,
			String fromDate, String toDate, int size, int startIndex) throws Exception {
		
		URL url 			= null;
		byte[] dataBytes 	= null;
		String response 	= null;
		String strUrlAddress= null;
		
		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);
			
			if(AppConstants.QCdata == null)
				AppConstants.QCdata	= relogin();
			
			//V2NEWS?[KWORD]=&[TARGET]=&[GUID]=&[CAT]=&[SOURCE]=NAR&[ACTIVE]=&[FRDT]=&[TODT]=&[SIZE]=20&[FROM]=0|||
			strUrlAddress		= "/[" + AppConstants.QCdata.getStrUserParam() + "]V2NEWS?";
			strUrlAddress	   += "[KWORD]=" + keyword + "&[TARGET]=" + target + "&[GUID]=" + guid
						+ "&[CAT]=" + category + "&[SOURCE]=" + source + "&[ACTIVE]="
						+ "&[FRDT]=" + fromDate + "&[TODT]=" + toDate + "&[SIZE]=" + ((size > 0)? size : "")
						+ "&[FROM]=" + startIndex + "|||";
			
			url					= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlAddress);
			DefinitionConstants.Debug("[QcHttpConnectUtil][getV2News]url: " + url.toString() );
			
			dataBytes 			= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url).getBytes() : readUrlData(url);
			response 			= AppConstants.qcCompress ? FormatUtil.uncompress(dataBytes, "", "a").toString() : new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[QcHttpConnectUtil][getV2News]response: " + response );
			
			if( ErrorCodeConstants.isInvalidQCClient(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(response);

			if(response!=null){
				if(response.length() <= 9)
					return null;
				
				String[] strArr	= response.split(strStartContentIndicator); 
				response		= strArr.length > 1 ? strArr[1] : strArr[0];
				response		= response.startsWith(strNewLineIndicator) ? response.substring( strNewLineIndicator.length(), response.length() ) : response;

				strArr			= response.split(strEndContentIndicator);
				response		= strArr[0].endsWith(strNewLineIndicator) ? strArr[0].substring( 0, strArr[0].length() - strNewLineIndicator.length() ) : strArr[0];
			}
			DefinitionConstants.Debug("[QcHttpConnectUtil][getV2News]final response : " + response );
			
			return response;
		}catch(FailedAuthenicationException fae){
			fae.printStackTrace();
			if( ErrorCodeConstants.isInvalidQCClient( fae.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) )
				AppConstants.QCdata	= relogin();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url 			= null;
			dataBytes 		= null;
			strUrlAddress	= null;
		}
		return null;
	}

	//Change_Request-20170601, ReqNo.12 (to map native stock code to Reuters code)
	public static String getStockMapping(String stockCode, String targetExchange) throws Exception{
		URL url				= null;
		String mSymbolCode = null;
		String strUrlPath	= null;

		try {
			if(AppConstants.QC_SERVER_IP == null)
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_QC);

			//http://203.115.249.177/[5a3901fb572c46df96997cd1344e1b09]FSEARCH?[F]=getstkmap(O39,SI)|||
			strUrlPath		= "/[" + AppConstants.QCdata.getStrUserParam() + "]FSEARCH?";
			strUrlPath	   += "[F]=getstkmap(" + stockCode + "," + targetExchange + ")|||";

			url			= new URL("http", AppConstants.QC_SERVER_IP, AppConstants.QC_SERVER_PORT, strUrlPath);
			DefinitionConstants.Debug("[QcHttpConnectUtil][FSEARCH]Url Address :" + url.toString() );

			String strResponse	= AppConstants.brokerConfigBean.isConnectJavaQC() ? readUrlData2(url) : SystemUtil.generateAESKey( readUrlData(url) );
			DefinitionConstants.Debug("[QcHttpConnectUtil][FSEARCH]response :" + strResponse);

			if( ErrorCodeConstants.isBackendExceptionMsg(strResponse) )
				throw new FailedAuthenicationException(strResponse);

			mSymbolCode = QcMessageParser.parseStockMapping(strResponse);
			DefinitionConstants.Debug("[QcHttpConnectUtil][FSEARCH]mSymbolCode :" + mSymbolCode);

			return mSymbolCode;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			strUrlPath	= null;
		}
		return null;
	}
}