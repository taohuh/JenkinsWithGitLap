package com.n2nconnect.android.stock.model;
/*
 * Added by Thinzar@20141028, Change_Request-20140901, ReqNo. 12
 */
public class LotMarket {
	private String code;
	private String name;
	private String symbol;
	
	public LotMarket(){
		
	}
	
	public LotMarket(String m_code, String m_name, String m_symbol){
		this.code		= m_code;
		this.name		= m_name;
		this.symbol		= m_symbol;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
}
