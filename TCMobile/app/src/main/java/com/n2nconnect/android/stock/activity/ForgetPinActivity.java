package com.n2nconnect.android.stock.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class ForgetPinActivity extends CustomWindow implements OnDoubleLoginListener, OnRefreshStockListener {

	private String hintQuestion;
	private String hintEmail;
	/* Mary@20130605 - comment unused variable
	private String idStatus;
	private String hintAnswer;
	private String userParam;
	private String senderCode;
	*/
	private EditText hintField;
	private String tradeForgetNewPin;
	private ProgressDialog dialog;
	private boolean isExpired	= false;
	private DoubleLoginThread loginThread;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr 	= 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.forget_pin);
	
			this.title.setText("Forgot PIN");
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(R.drawable.menuicon_acc_setting);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntAccSettingMenuIconBgImg() );
	
			hintField = (EditText) this.findViewById(R.id.hintField);
			hintField.setSingleLine();
	
			dialog = new ProgressDialog(this);
	
			/* Mary@20130605 - comment unneccessary process
			senderCode = sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
			*/
			
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(ForgetPinActivity.this)
				//.setIcon(android.R.attr.alertDialogStyle)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					
			new getAccountInfo().execute();
	
			Button submitButton = (Button) this.findViewById(R.id.submitButton);
			submitButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					ForgetPinActivity.this.handleForgetPinEvent2();
				}
			});
			
			hintField	= (EditText) ForgetPinActivity.this.findViewById(R.id.hintField);
	
			loginThread = ((StockApplication)this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) ForgetPinActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(this);
			
	        /* Mary@20130605 - comment unneccessary process
			userParam = sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			*/
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}
	
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
	@Override
	public void onResume(){
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onResume();
			
			currContext	= ForgetPinActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(ForgetPinActivity.this);
				loginThread.resumeRequest();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END

	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	//private void RegisterMailEvent(){
	/* Mary@20130528 - Fixes_Request-20130523, ReqNo.2
	private String RegisterMailEvent(){
	*/
	private void RegisterMailEvent() throws Exception {
		//String mailType = 
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(ParamConstants.HINT_EMAIL_TAG, hintEmail);
		parameters.put(ParamConstants.TRADEFORGET_NEWPWD_TAG, tradeForgetNewPin);

		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		try {
			String response = AtpConnectUtil.RegisterMail(parameters, AppConstants.CHANGE_PIN_TYPE);
		} catch (FailedAuthenicationException e) {
			// TODO Auto-generated catch block
			isExpired = true;
			LoginActivity.QCAlive.stopRequest();
			LoginActivity.refreshThread.stopRequest();
			loginThread.stopRequest();
			e.printStackTrace();
			//this.sessionExpired();
		}
		*/
		try {
			String response = AtpConnectUtil.RegisterMail(parameters, AppConstants.CHANGE_PIN_TYPE);
			while(intRetryCtr < AppConstants.intMaxRetry && response == null){
				intRetryCtr++;
				response = AtpConnectUtil.RegisterMail(parameters, AppConstants.CHANGE_PIN_TYPE);
			}
			intRetryCtr	= 0;
			if(response == null)				
				throw new Exception(ErrorCodeConstants.FAIL_REGISTER_MAIL);

			/* Mary@20130528 - Fixes_Request-20130523, ReqNo.2
			return null;
			*/
		} catch (FailedAuthenicationException e) {
			// TODO Auto-generated catch block
			isExpired = true;
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(LoginActivity.QCAlive != null)
				LoginActivity.QCAlive.stopRequest();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(LoginActivity.refreshThread != null)
				LoginActivity.refreshThread.stopRequest();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(loginThread != null)
				loginThread.stopRequest();
			e.printStackTrace();
			//this.sessionExpired();
			/* Mary@20130528 - Fixes_Request-20130523, ReqNo.2
			return e.getMessage();
			*/
			throw e;
		}catch(Exception e){
			/* Mary@20130528 - Fixes_Request-20130523, ReqNo.2
			return e.getMessage();
			*/
			throw e;
		}
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}

	private void handleForgetPinEvent2() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			if(hintField.getText().toString().trim().equals("")){
				AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPinActivity.this);
				builder.setMessage(getResources().getString(R.string.keyin_hint_answer))
				       .setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				        	   dialog.dismiss();
				           }
				       });
				AlertDialog alert = builder.create();
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ForgetPinActivity.this.isFinishing() )
					alert.show();
				return;
			}
			
			new forgetPIN().execute();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	
	private class forgetPIN extends AsyncTask<Void,Void,String>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				dialog.setMessage(getResources().getString(R.string.processing_request));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ForgetPinActivity.this.isFinishing() )
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String hintText = hintField.getText().toString();

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put( ParamConstants.USERNAME_TAG, sharedPreferences.getString(PreferenceConstants.USER_NAME, null) );
			parameters.put(ParamConstants.ADMINKEY_TAG, AppConstants.ADMIN_KEY);
			parameters.put(ParamConstants.HINT_QUESTION_TAG, hintQuestion);
			parameters.put(ParamConstants.HINT_ANSWER_TAG, hintText);
//				parameters.put(ParamConstants.HINT_EMAIL_TAG, "yee.ase3@gmail.com");
			parameters.put(ParamConstants.HINT_EMAIL_TAG, hintEmail);

			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			try {
				String response = AtpConnectUtil.tradeForgetPasswd(parameters, AppConstants.CHANGE_PIN_TYPE);
				return response;
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				isExpired = true;
				LoginActivity.QCAlive.stopRequest();
				LoginActivity.refreshThread.stopRequest();
				loginThread.stopRequest();
				e.printStackTrace();
			}
			*/
			try {
				String response = AtpConnectUtil.tradeForgetPasswd(parameters, AppConstants.CHANGE_PIN_TYPE);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response = AtpConnectUtil.tradeForgetPasswd(parameters, AppConstants.CHANGE_PIN_TYPE);
				}
				intRetryCtr	= 0;
				
				if(response == null)					
					throw new Exception(ErrorCodeConstants.FAIL_FORGET_PIN_REQ);

				return response;
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				isExpired = true;
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.refreshThread != null)
					LoginActivity.refreshThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				e.printStackTrace();
			}catch(Exception e){
				return e.getMessage();
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			return null;
		}

		@Override
		protected void onPostExecute(String response) {
			// TODO Auto-generated method stub
			super.onPostExecute(response);
			
			try{
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				if(dialog != null && dialog.isShowing() )
					dialog.dismiss();
				
				if(isExpired){
					ForgetPinActivity.this.sessionExpired();
					return;
				}
				
				if(response == null)
					return;
	
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				if (response.indexOf("Error") != -1) {
				*/
				if (response.toUpperCase().indexOf("ERROR") != -1) {
					AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPinActivity.this);
					builder.setMessage(response)
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
					AlertDialog alert = builder.create();
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! ForgetPinActivity.this.isFinishing() )
						alert.show();
				}else{
					Map<String, String> newPasswordInfo = AtpMessageParser.extractNewPinInfo(response);
	
					tradeForgetNewPin 					= newPasswordInfo.get(DefinitionConstants.TRADE_FORGET_PIN);
					/* Mary@20130103 - Fixes_Request-20121102, ReqNo.12
					if(tradeForgetNewPin.equals("0")){
					*/
					/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
					if( ! AppConstants.EnableEncryption && ( tradeForgetNewPin == null || ( tradeForgetNewPin != null && tradeForgetNewPin.trim().equals("0") ) ) ){
					*/
					if( ! AppConstants.brokerConfigBean.isEnableEncryption() && ( tradeForgetNewPin == null || ( tradeForgetNewPin != null && tradeForgetNewPin.trim().equals("0") ) ) ){
						AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPinActivity.this);
						builder.setMessage(getResources().getString(R.string.pin_hint_not_matched))
						.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ForgetPinActivity.this.isFinishing() )
							alert.show();
						return;
					}
					
					/* Mary@20130528 - Fixes_Request-20130523, ReqNo.2 - START
					new RegisterMail().execute();
					*/
					try{
						//RegisterMailEvent();		//Commented out by Thinzar@20141009, Fixes_Request-20140929,ReqNo.8
						
						AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPinActivity.this);
						/* Mary@20121221-Fixes_request-20121224, ReqNo.9
						builder.setMessage("Password reset successfully. Please check your email.")
						*/
						builder.setMessage(getResources().getString(R.string.pin_reset_success_msg))
						.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								Intent intent = new Intent();
								intent.setClass(ForgetPinActivity.this, SettingsActivity.class);
								ForgetPinActivity.this.startActivity(intent);
							}
						});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ForgetPinActivity.this.isFinishing() )
							alert.show();
					}/* Commented out by Thinzar@20141009, Fixes_Request-20140929,ReqNo.8
					catch(FailedAuthenicationException fae){
						ForgetPinActivity.this.sessionExpired();
					}*/
					catch(Exception e){
						alertException.setMessage( e.getMessage() );
						alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int id) {
								dialog.dismiss();
							}
						});
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ForgetPinActivity.this.isFinishing() )
							alertException.show();
					}				
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	/* Mary@20130528 - Fixes_Request-20130523, ReqNo.2
	private class RegisterMail extends AsyncTask<Void,Void,String>{

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			RegisterMailEvent();
			return null;
			/
			return RegisterMailEvent();
		}

		@Override
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			if(isExpired == true){
				ForgetPinActivity.this.sessionExpired();
				return;
			}
		/
		protected void onPostExecute(String strErrMsg) {
			super.onPostExecute(strErrMsg);
					
			if(isExpired == true){
				ForgetPinActivity.this.sessionExpired();
				return;
			}
			
			if(strErrMsg != null){
				alertException.setMessage( strErrMsg );
				alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						dialog.dismiss();
					}
				});
				alertException.show();
				return;
			}
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}
	}
	*/
	
	private class getAccountInfo extends AsyncTask<Void,Void,String>{
		// Added by Mary@20130103 - Fixes_Request-20121102, ReqNo.12
		private boolean isException	= false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				dialog.setMessage(getResources().getString(R.string.retrieving_account));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ForgetPinActivity.this.isFinishing() )
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Map<String, Object> parameters = new HashMap<String, Object>();
			/* Mary@20130103 - Fixes_Request-20121102, ReqNo.12
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			*/
			parameters.put(ParamConstants.USERNAME_TAG, sharedPreferences.getString(PreferenceConstants.USER_NAME, null) );
			parameters.put(ParamConstants.ADMINKEY_TAG, AppConstants.ADMIN_KEY);

			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if(AppConstants.EnableEncryption)
			*/
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			
//			try {
//				String response = AtpConnectUtil.getHintInfo(parameters);
//				Map hints = AtpMessageParser.extractHintInfo(response);
//				if (hints != null) {
//					hintQuestion = (String) hints.get(DefinitionConstants.HINT_QUESTION);
//					idStatus = (String) hints.get(DefinitionConstants.ID_STATUS);
//				}
	//
//				TextView hintLabel = (TextView) this.findViewById(R.id.hintLabel);
//				hintLabel.setText((hintQuestion.indexOf("?") == -1)? hintQuestion + "?" : hintQuestion);
	//
//			} catch (FailedAuthenicationException e) {
//				e.printStackTrace();
//				//this.sessionExpired();
//			}
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			try {
				String response = AtpConnectUtil.getAccountInfo2(parameters);
				
				return response;

			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				//this.sessionExpired();
				isExpired = true;
				LoginActivity.QCAlive.stopRequest();
				LoginActivity.refreshThread.stopRequest();
				loginThread.stopRequest();
			}
			return null;
			*/
			String response = null;
			try {
			/* Mary@20130103 - Fixes_Request-20121102, ReqNo.12 - START
				response = AtpConnectUtil.getAccountInfo2(parameters);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response = AtpConnectUtil.getAccountInfo2(parameters);
				}
				intRetryCtr	= 0;
				
				if(response == null)						
					throw new Exception(ErrorCodeConstants.FAIL_RETRIEVE_ACCINFO_2);
			*/
				response = AtpConnectUtil.getAccountInfo(parameters);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response = AtpConnectUtil.getAccountInfo(parameters);
				}
				intRetryCtr	= 0;
				if(response == null)
					throw new Exception(ErrorCodeConstants.FAIL_RETRIEVE_ACCINFO);
				
			// Mary@20130103 - Fixes_Request-20121102, ReqNo.12 - END
			}catch (FailedAuthenicationException e){
				e.printStackTrace();
				//this.sessionExpired();
				isExpired = true;
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.refreshThread != null)
					LoginActivity.refreshThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
			}catch(Exception e){
				// Added by Mary@20130103 - Fixes_Request-20121102, ReqNo.12
				isException = true;
				response = e.getMessage();
			}
			return response;
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}
		
		@Override
		protected void onPostExecute(String response){
			super.onPostExecute(response);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( dialog != null && dialog.isShowing() )
					dialog.dismiss();
				
				if(isExpired){
					ForgetPinActivity.this.sessionExpired();
					return;
				}
				
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				// if (response.indexOf("Error") != -1) {
				/* Mary@20130103 - Fixes_Request-20121102, ReqNo.12
				if (response.toUpperCase().indexOf("ERROR") != -1) {
				*/
				if (response.toUpperCase().indexOf("ERROR") != -1 || isException) {
					AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPinActivity.this);
					builder.setMessage(response)
					       .setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					           public void onClick(DialogInterface dialog, int id) {
					        	   dialog.dismiss();
					           }
					       });
					AlertDialog alert = builder.create();
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! ForgetPinActivity.this.isFinishing() )
						alert.show();
					return;
				}
				Map<String, String> hints = AtpMessageParser.extractHintInfo2(response);
				if (hints != null) {
					hintQuestion= hints.get(DefinitionConstants.HINT_QUESTION);
					/* Mary@20130605 - comment unneccessary process
					hintAnswer	= hints.get(DefinitionConstants.HINT_ANSWER);
					*/
					hintEmail	= hints.get(DefinitionConstants.HINT_EMAIL);
				}
	
				TextView hintLabel = (TextView) ForgetPinActivity.this.findViewById(R.id.hintLabel);
				hintLabel.setText((hintQuestion.indexOf("?") == -1)? hintQuestion + "?" : hintQuestion);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	/* Mary@20130103 - Fixes_Request-20121102, ReqNo.12
	public void handleForgetPinEvent() {
		String hintText = hintField.getText().toString().trim();
		Map parameters = new HashMap();
		String username = sharedPreferences.getString(PreferenceConstants.USER_NAME, null);
		parameters.put(ParamConstants.USERNAME_TAG, username);
		parameters.put(ParamConstants.HINT_QUESTION_TAG, hintQuestion);
		parameters.put(ParamConstants.HINT_ANSWER_TAG, hintText);

		if(AppConstants.EnableEncryption)
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
			parameters.put(ParamConstants.USERPARAM_TAG, userParam);

		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		try {
			String response = AtpConnectUtil.tradeForgetPasswd2(parameters, AppConstants.CHANGE_PIN_TYPE);

			if(response.indexOf("Error")==-1){
				Map tradeForget = AtpMessageParser.extractTradeForgetPINInfo(response);

				String errorCode = (String) tradeForget.get(DefinitionConstants.ERROR_CODE);
				String errorMessage = (String) tradeForget.get(DefinitionConstants.ERROR_MESSAGE);

				if (!errorCode.equals("0")) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(errorMessage)
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
				} else {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("PIN reset successfully. Please check your email.")
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							Intent intent = new Intent();
							intent.setClass(ForgetPinActivity.this, LoginActivity.class);
							ForgetPinActivity.this.startActivity(intent);
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
			}else{
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(response)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}

		} catch (FailedAuthenicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//this.sessionExpired();
		}
		/ 
		try {
			String response = AtpConnectUtil.tradeForgetPasswd2(parameters, AppConstants.CHANGE_PIN_TYPE);

			while(intRetryCtr < AppConstants.intMaxRetry && response == null){
				intRetryCtr++;
				response = AtpConnectUtil.tradeForgetPasswd2(parameters, AppConstants.CHANGE_PIN_TYPE);
			}
			intRetryCtr	= 0;

			if(response == null){						
				throw new Exception(ErrorCodeConstants.FAIL_CHANGE_PIN_2);
			}
			
			if (response.toUpperCase().indexOf("ERROR") == -1){
				Map tradeForget = AtpMessageParser.extractTradeForgetPINInfo(response);

				String errorCode = (String) tradeForget.get(DefinitionConstants.ERROR_CODE);
				String errorMessage = (String) tradeForget.get(DefinitionConstants.ERROR_MESSAGE);

				if (!errorCode.equals("0")) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(errorMessage)
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
				} else {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("PIN reset successfully. Please check your email.")
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							Intent intent = new Intent();
							intent.setClass(ForgetPinActivity.this, LoginActivity.class);
							ForgetPinActivity.this.startActivity(intent);
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
			}else{
				throw new Exception(response);
			}
		}catch (Exception e){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage( e.getMessage() )
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	}
	*/
	
	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj		= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(ForgetPinActivity.this.mainMenu.isShowing()){
					ForgetPinActivity.this.mainMenu.hide();
				}
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					ForgetPinActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		// TODO Auto-generated method stub

	}
}
