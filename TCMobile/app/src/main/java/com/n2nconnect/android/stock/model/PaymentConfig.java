package com.n2nconnect.android.stock.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Added by Thinzar, Change_Request-20150401, ReqNo.1, CIMB_SG CUT Phase 2
public class PaymentConfig {
	
	private String exchangeCode;
	private String accType;
	private List<String> paymentMethods;
	private List<String> currencies;
	private String currencyString; 	
	
	public PaymentConfig() {
		paymentMethods	= new ArrayList<String>();
		currencies		= new ArrayList<String>();
	}

	public String getExchangeCode() {
		return exchangeCode;
	}

	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}

	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	public List<String> getPaymentMethods() {
		return paymentMethods;
	}
	
	public void addPaymentMethod(String paymentMethod){
		this.paymentMethods.add(paymentMethod);
	}
	
	public void setPaymentMethods(List<String> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	
	public List<String> getCurrencies() {
		return currencies;
	}
	
	public void addCurrency(String currency){
		this.currencies.add(currency);
	}

	public String getCurrencyString() {
		return currencyString;
	}

	public void setCurrencyString(String currencyString) {
		this.currencyString = currencyString;
	}
	
}
