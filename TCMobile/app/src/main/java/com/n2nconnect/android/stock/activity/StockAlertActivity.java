package com.n2nconnect.android.stock.activity;

import java.util.List;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.gcm.StockAlertPubKey;
import com.n2nconnect.android.stock.gcm.StockAlertUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class StockAlertActivity extends CustomWindow implements OnDoubleLoginListener {

	private SharedPreferences sharedPreferences;

	private String jwtToken;

	private WebView webView;
	private ProgressDialog progDialog;

	private DoubleLoginThread loginThread;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.stockalert_page);
		this.title.setText(getResources().getString(R.string.stock_alert_module_title));
		this.icon.setImageResource(R.drawable.menuicon_stock_alert);

		sharedPreferences 	= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		webView 			= (WebView) findViewById(R.id.web_views);
	}

	public class WebViewController extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			//sample- stockalert://media:push_notifications/ex:/stockcode:/alerttype:stock_price/comparetype:absolute_value/conditions:>/value:1.000/status:a/

			if(url.startsWith("stockalert:")){
				resetATPExpireTime();
				return true;
			} else{
				view.loadUrl(url);

				return false;
			}
		}

		//Added by Thinzar, Fixes_Request-20160101, ReqNo.9 - START
		public void onReceivedSslError (WebView view, final SslErrorHandler handler, SslError error) {

			final AlertDialog.Builder builder = new AlertDialog.Builder(StockAlertActivity.this);
			builder.setMessage(R.string.notification_error_ssl_cert_invalid);
			builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					handler.proceed();
				}
			});
			builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					handler.cancel();
				}
			});
			final AlertDialog dialog = builder.create();
			dialog.show();
		}
		//Added by Thinzar, Fixes_Request-20160101, ReqNo.9 - END

		@Override
		public void onPageFinished(WebView view, String url) {
			System.out.println("onPageFinished:" + url);
			if (!StockAlertActivity.this.isFinishing() && progDialog.isShowing()) {
				progDialog.dismiss();
			}
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description,
				String failingUrl) {
			Toast.makeText(StockAlertActivity.this, "Error:" + description, Toast.LENGTH_SHORT).show();

		}
	}

	public void onResume(){
		super.onResume();

		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
		currContext	= StockAlertActivity.this;
		if( idleThread != null && idleThread.isPaused() )
			idleThread.resumeRequest(false);
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END

		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		loginThread = ((StockApplication) StockAlertActivity.this.getApplication()).getDoubleLoginThread();
		if( loginThread != null && loginThread.isPaused() ){
			loginThread.setRegisteredListener(StockAlertActivity.this);
			loginThread.resumeRequest();
		} 
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END

		//new GetPublicKey().execute();
		new GetPublicKeyTask().execute();
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message	= handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;

			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){

				if(loginThread != null)
					loginThread.stopRequest();

				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();

				if( StockAlertActivity.this.mainMenu.isShowing() )
					StockAlertActivity.this.mainMenu.hide();

				if( !isFinishing() )
					StockAlertActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (webView.canGoBack() && (keyCode == KeyEvent.KEYCODE_BACK)) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

			if(this.mainMenu.isShowing())
				this.mainMenu.hide();

			webView.goBack();
			finish();

			return true;
		}else if(keyCode==KeyEvent.KEYCODE_BACK){
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onPause(){
		try{
			super.onPause();

			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END

	private void resetATPExpireTime(){
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
	}

	private class GetPublicKeyTask extends AsyncTask<Void, Void, Exception> {
		StockAlertPubKey stockAlertObj;
		String senderCode;
		String brokerCode;
		String screenSize;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDialog 	= new ProgressDialog(StockAlertActivity.this);
			senderCode 	= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
			brokerCode	= sharedPreferences.getString(PreferenceConstants.BROKER_CODE, "");
			screenSize	= SystemUtil.getDeviceScreenSize(StockAlertActivity.this);

			if(progDialog != null)
				progDialog.setMessage(getResources().getString(R.string.dialog_progress_loading));

			if(!progDialog.isShowing())
				progDialog.show();
		}

		@Override
		protected Exception doInBackground(Void... params) {

			try{
				stockAlertObj = StockAlertUtil.getPublicKey(brokerCode);

				if(stockAlertObj == null){
					throw new Exception("Error getting public key for stock alert.");
				}else{
					JSONObject claimJson = StockAlertUtil.getJsonForOpenStockAlert(StockAlertActivity.this, brokerCode, senderCode, screenSize);
					jwtToken = StockAlertUtil.generateJWTToken(stockAlertObj, StockAlertActivity.this, brokerCode, claimJson);

					if(jwtToken == null){
						throw new Exception("Failed to generate JWT Token");
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				return e;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Exception ex) {
			super.onPostExecute(ex);
			if(progDialog != null && progDialog.isShowing())
				progDialog.dismiss();

			//Change_Request-20170601, ReqNo.15
			String stockAlertBHCode = AppConstants.brokerConfigBean.getStockAlertBHCode();
			if(stockAlertBHCode != null && stockAlertBHCode.length() > 0)
				brokerCode 			= stockAlertBHCode;

			if (ex != null) {
				DefinitionConstants.Debug("GetPublicKeyTask, Exception:" + ex.getMessage());
				//showAlert(getResources().getString(R.string.quote_stock_alert_register_failed));
			}else{

				webView.getSettings().setJavaScriptEnabled(true);
				webView.getSettings().setDomStorageEnabled(true);
				webView.getSettings().setAllowFileAccess(true);

				String url = AppConstants.brokerConfigBean.getStockAlertUrl()+ "jwt=" + jwtToken + "&bh=" + brokerCode
						+ "&appId=" + AppConstants.TCANDROID_APP_ID;

				//to enhance the performance with hardware acceleration, android 19 has Chromium engine for WebView
				if (Build.VERSION.SDK_INT >= 19) {
					webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
				}       
				else {
					webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				}

				webView.loadUrl(url);
				webView.setWebViewClient(new WebViewController());
				webView.setWebChromeClient(new WebChromeClient());
			} 
		}
	}
}