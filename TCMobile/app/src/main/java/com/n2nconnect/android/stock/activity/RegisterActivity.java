package com.n2nconnect.android.stock.activity;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CustomizedBase64;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class RegisterActivity extends CustomWindow {

	private EditText Name;
	private EditText MobileNumber;
	private EditText Email;
	private EditText Username;
	private EditText Password;
	private EditText ConfirmPwd;
	private EditText IC;
	private EditText HintOthQues;
	private EditText HintAnswer;
	/* KwokWai@2012812 - Change_Request-20120719, ReqNo.3
	private Button Register;
	*/
	/* Mary@20130605 - comment unused variable
	private Button Clear;
	private EditText othQuestion;
	// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3
	private ScrollView ScrollViewItem;
	*/
	private Spinner hintQuestion;	
	private String[] array_spinner;
	private LinearLayout linearOthQuestion;
	private String hintQues;
	private Drawable error_indicator;
	
	private ProgressDialog dialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
		try{
			super.onCreate(savedInstanceState);
	
			setContentView(R.layout.register_view);
			
			this.title.setText(getResources().getString(R.string.registration_module_title));
			this.menuButton.setVisibility(View.GONE);
			// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3
			this.Register.setVisibility(View.VISIBLE);
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(R.drawable.menuicon_acc_setting);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntAccSettingMenuIconBgImg() );
			
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
			/*Commented by Thinzar, revamp for TcMobile 2.0
			if(! AppConstants.hasMultiLogin ){
		 		( (LinearLayout)findViewById(R.id.ll_register) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntBgImg() );
		 		( (TextView)findViewById(R.id.nameLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.NRICLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.mobileLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.emailLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.usernameLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.passwordLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.confirmpasswordLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.passwordhintLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.othhintQuesLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.hintAnswerLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.nameInfoLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.nricInfoLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.mobileInfoLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.emailInfoLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.usernameInfoLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.pwInfoLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.comfirmpwInfoLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
		 		( (TextView)findViewById(R.id.hintInfoLabel) ).setTextColor( AppConstants.brokerConfigBean.getIntTextColor() );
			}*/
	 		// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	
			Name = (EditText)findViewById(R.id.nameTxt);
			MobileNumber = (EditText)findViewById(R.id.mobileTxt);
			IC = (EditText)findViewById(R.id.NRICTxt);
			Email = (EditText)findViewById(R.id.emailTxt);
			Username = (EditText)findViewById(R.id.usernameTxt);
			Password = (EditText)findViewById(R.id.passwordTxt);
			ConfirmPwd = (EditText)findViewById(R.id.confirmpasswordTxt);
			HintOthQues = (EditText)findViewById(R.id.othHintQuesTxt); 
			HintAnswer = (EditText)findViewById(R.id.hintAnswerTxt);
	
			/* KwokWai@2012812 - Change_Request-20120719, ReqNo.3
			Register = (Button)findViewById(R.id.btnRegister);
			*/
			// Clear = (Button)findViewById(R.id.btnClear);
			hintQuestion = (Spinner)findViewById(R.id.HintQuestSpin);
			linearOthQuestion = (LinearLayout)findViewById(R.id.linearOthQuestion);
			error_indicator = getResources().getDrawable(R.drawable.disclaimer);
			/* Mary@20130605 - comment unused variable
			// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - START
			ScrollViewItem = (ScrollView)findViewById(R.id.scrollView);
			*/
			
			final EditText[] ets = new EditText[]{Name,IC,MobileNumber,Email,Username,Password,ConfirmPwd,HintOthQues,HintAnswer};
			// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - END
	
			dialog	= new ProgressDialog(RegisterActivity.this);
			
			int left = 0;
			int top = 0;
			int right = error_indicator.getIntrinsicHeight();
			int bottom = error_indicator.getIntrinsicWidth();
			error_indicator.setBounds(new Rect(left,top,right,bottom));
	
			Name.setOnEditorActionListener(new EmptyTextListener(Name));
			IC.setOnEditorActionListener(new EmptyTextListener(IC));
			MobileNumber.setOnEditorActionListener(new EmptyTextListener(MobileNumber));
			Email.setOnEditorActionListener(new EmptyTextListener(Email));
			Username.setOnEditorActionListener(new EmptyTextListener(Username));
			Password.setOnEditorActionListener(new EmptyTextListener(Password));
			ConfirmPwd.setOnEditorActionListener(new EmptyTextListener(ConfirmPwd));
			HintOthQues.setOnEditorActionListener(new EmptyTextListener(HintOthQues));
			HintAnswer.setOnEditorActionListener(new EmptyTextListener(HintAnswer));
			// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - START
			setClearTxtBtn(Name);
			setClearTxtBtn(IC);
			setClearTxtBtn(MobileNumber);
			setClearTxtBtn(Email);
			setClearTxtBtn(Username);
			setClearTxtBtn(Password);
			setClearTxtBtn(ConfirmPwd);
			setClearTxtBtn(HintOthQues);
			setClearTxtBtn(HintAnswer); 
			// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - END
			int maxNameLength = 50;
			InputFilter[] fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxNameLength);
			Name.setFilters(fArray);
	
			int maxICLength = 15;
			fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxICLength);
			IC.setFilters(fArray);
	
			int maxMobileLength = 30;
			fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxMobileLength);
			MobileNumber.setFilters(fArray);
	
			int maxEmailLength = 40;
			fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxEmailLength);
			Email.setFilters(fArray);
	
			int maxUsernameLength = 15;
			fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxUsernameLength);
			Username.setFilters(fArray);
	
			int maxPasswordLength = 12;
			fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxPasswordLength);
			Password.setFilters(fArray);
	
			int maxHintQuesLength = 50;
			fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxHintQuesLength);
			HintOthQues.setFilters(fArray);
	
			int maxHintAnswerLength = 20;
			fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxHintAnswerLength);
			HintAnswer.setFilters(fArray);
	
			array_spinner=new String[5];
			array_spinner[0]= getResources().getString(R.string.regis_passwd_hint_choose_question);	//"Choose a question";
			array_spinner[1]= getResources().getString(R.string.regis_passwd_hint_own_question);	//"My own question";
			array_spinner[2]= getResources().getString(R.string.regis_passwd_hint_mother_name);	//"Mother maiden's name";
			array_spinner[3]= getResources().getString(R.string.regis_passwd_hint_fav_car);		//"My favorite car";
			array_spinner[4]= getResources().getString(R.string.regis_passwd_hint_fav_cartoon);	//"My favorite cartoon";
			
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, array_spinner);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			hintQuestion.setAdapter(adapter);
	
			hintQuestion.setOnItemSelectedListener(new SpinnerItemSelected());
			
			/* KwokWai@2012812 - Change_Request-20120719, ReqNo.3
			Clear.setOnClickListener(new OnClickListener(){
	
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Name.setText("");
					IC.setText("");
					MobileNumber.setText("");
					Email.setText("");
					Username.setText("");
					Password.setText("");
					ConfirmPwd.setText("");
					HintOthQues.setText("");
					HintAnswer.setText("");
					hintQuestion.setSelection(0);
					if(linearOthQuestion.isShown()){
						linearOthQuestion.setVisibility(View.GONE);
					}
					Name.requestFocus();
				}
	
			});
			*/
	
			Register.setOnClickListener(new OnClickListener(){
	
				@Override
				public void onClick(View v) {
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					try{
						if (!RegisterActivity.this.isNetworkAvailable()) {
							RegisterActivity.this.createAndShowAlertDialog(
									getResources().getString(R.string.login_error_no_network));
							return;
						}
						
						/* KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - START
						if(VerifyControl()){
						*/
						/* Mary@20130605 - comment unused variable
						boolean verify;
						*/
						int count =0;
						for(int i=0;i<ets.length;i++){
							if(i==7){
								if(validateSpinner(hintQuestion)){
									count++;
								}else{
									break;
								}
							}
							
							if(validation(ets[i])){
								count++;
							}else{
								break;
							}
						}
					
						
						if(count == ets.length+1){
						/*	if(hintQuestion.getSelectedItemPosition() == 0){
							
							final AlertDialog alertdialog = new AlertDialog.Builder(RegisterActivity.this).create();
							alertdialog.setTitle("Error");
							alertdialog.setMessage("You did not select the Password Hint");
							alertdialog.setButton("OK", new DialogInterface.OnClickListener() {
							      public void onClick(DialogInterface dialog, int which) {
							    	  
							         alertdialog.dismiss();
							         hintQuestion.setFocusable(true);
							         hintQuestion.setFocusableInTouchMode(true);
										hintQuestion.requestFocus();
							       } });
							alertdialog.show();
							
							
							
						}else
						{count++;}*/
						
							// KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - END
							byte[] bytesName, bytesIC, bytesHpNo, bytesEmail, bytesUserName, bytesPassword, bytesConfirmPwd, bytesHintType, bytesHintQues = null, bytesHintAnswer = null;
		
							try {
								bytesName = Name.getText().toString().getBytes("UTF-8");
								bytesIC = IC.getText().toString().getBytes("UTF-8");
								bytesHpNo = MobileNumber.getText().toString().getBytes("UTF-8");
								bytesEmail = Email.getText().toString().getBytes("UTF-8");
								bytesUserName = Username.getText().toString().getBytes("UTF-8");
								bytesPassword = Password.getText().toString().getBytes("UTF-8");
								bytesConfirmPwd = ConfirmPwd.getText().toString().getBytes("UTF-8");
								if(linearOthQuestion.isShown()){
									String type = "OTR";
									bytesHintType = type.getBytes("UTF-8");
									bytesHintQues = HintOthQues.getText().toString().getBytes("UTF-8");
									bytesHintAnswer = HintAnswer.getText().toString().getBytes("UTF-8");
		
								}else{
									bytesHintType = HintOthQues.getText().toString().getBytes("UTF-8");
									bytesHintQues = hintQues.getBytes("UTF-8");
									bytesHintAnswer = HintAnswer.getText().toString().getBytes("UTF-8");
								}
		
		
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								bytesName = Name.getText().toString().getBytes();
								bytesIC = IC.getText().toString().getBytes();
								bytesHpNo = MobileNumber.getText().toString().getBytes();
								bytesEmail = Email.getText().toString().getBytes();
								bytesUserName = Username.getText().toString().getBytes();
								bytesPassword = Password.getText().toString().getBytes();
								bytesConfirmPwd = ConfirmPwd.getText().toString().getBytes();
								if(linearOthQuestion.isShown()){
									String type = "OTR";
									bytesHintType = type.getBytes();
									bytesHintQues = HintOthQues.getText().toString().getBytes();
									bytesHintAnswer = HintAnswer.getText().toString().getBytes();
								}else{
									bytesHintType = HintOthQues.getText().toString().getBytes();
									bytesHintQues = hintQues.getBytes();
									bytesHintAnswer = HintAnswer.getText().toString().getBytes();
		
								}
		
							}
		
							String EncodeName = CustomizedBase64.encodeBytes(bytesName);
							String EncodeIC = CustomizedBase64.encodeBytes(bytesIC);
							String EncodeHpNo = CustomizedBase64.encodeBytes(bytesHpNo);
							String EncodeEmail = CustomizedBase64.encodeBytes(bytesEmail);
							String EncodeUserName = CustomizedBase64.encodeBytes(bytesUserName);
							String EncodePassword = CustomizedBase64.encodeBytes(bytesPassword);
							String EncodeConfirmPwd = CustomizedBase64.encodeBytes(bytesConfirmPwd);
							String EncodeHintType = CustomizedBase64.encodeBytes(bytesHintType);
							String EncodeHintQues = CustomizedBase64.encodeBytes(bytesHintQues);
							String EncodeHintAnswer = CustomizedBase64.encodeBytes(bytesHintAnswer);
		
							String httpURL="http://plc.asiaebroker.com/gcN2N/acctReg.jsp?";
							String urlParameters = 
									"ind=D&category=M&title=&displayName="+
											"&cliName=" + EncodeName + "&oldIcNo=&telno1=&telno=&displayName="+
											"&citizenShip="+
											"&icNo=" + EncodeIC +
											"&passport="+
											"&email=" + EncodeEmail +
											"&mobile1=" +
											"&mobile=" + EncodeHpNo +
											"&loginID=" + EncodeUserName +
											"&pwd=" + EncodePassword +
											"&txtConfirmed=" + EncodeConfirmPwd +
											"&pin=&txtConPIN=&motherName="+
											"&hintType=" + EncodeHintType +
											"&hint=" + EncodeHintQues +
											"&hintAns=" + EncodeHintAnswer;
		
							String urlAdd = httpURL+urlParameters;
							
							new RegisterAsyncTask().execute(urlAdd);
						}
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
					}catch(Exception e){
						e.printStackTrace();
					}
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
				}
			});
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
	}
	
	//Added by Thinzar, to solve android.os.NetworkOnMainThreadException issue
	private class RegisterAsyncTask extends AsyncTask<String, Void, String>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(dialog != null){
				dialog.setMessage(getResources().getString(R.string.processing_request));
				dialog.show();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			String urlAddress	= params[0];
			
			byte[] result = null;
			URL url = null;
			try {
				url = new URL(urlAddress);
				
				result = AtpConnectUtil.readUrlData(url);
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
			}catch (MalformedURLException e) {
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}

			String response = new String(result);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			if(!RegisterActivity.this.isFinishing())
				dialog.dismiss();
			
			StringTokenizer tokens = new StringTokenizer(result.trim(), "|");
			ArrayList<String> aL = new ArrayList<String>();
			ArrayList<String> aL2 = new ArrayList<String>();
			ArrayList<String> aL3 = new ArrayList<String>();
			while(tokens.hasMoreTokens()){
				String token = (String)tokens.nextToken();
				aL.add(token);	
			}
			tokens = new StringTokenizer(aL.get(1), "=");
			while(tokens.hasMoreTokens()){
				String token = (String)tokens.nextToken();
				aL2.add(token);
			}
			tokens = new StringTokenizer(aL.get(2), "=");
			while(tokens.hasMoreTokens()){
				String token = (String)tokens.nextToken();
				aL3.add(token);
			}

			if(aL2.get(1).equals("S")){
				Toast.makeText(RegisterActivity.this, getResources().getString(R.string.regis_success), Toast.LENGTH_LONG).show();
				Intent intent = new Intent();
				intent.setClass(RegisterActivity.this, LoginActivity.class);
				startActivity(intent);
				finish();
				Name.setText("");
				MobileNumber.setText("");
				Email.setText("");
				Username.setText("");
				Password.setText("");
				ConfirmPwd.setText("");
				IC.setText("");
				HintAnswer.setText("");
			}else if(aL2.get(1).equals("F")){
				createAndShowAlertDialog(aL3.get(1));
				Username.requestFocus();
			}
		}
	}
	
	public void createAndShowAlertDialog(String msg){
		
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(msg).setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {

				}
			});
			AlertDialog alert = builder.create();

			if (!this.isFinishing())
				alert.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private boolean isNetworkAvailable() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	/* KwokWai@2012812 - Change_Request-20120719, ReqNo.3
	private boolean VerifyControl(){
		if(Name.getText().toString().equals("")){
			Name.requestFocus();
			InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			mgr.showSoftInput(Name, InputMethodManager.SHOW_IMPLICIT);
			Name.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
			return false;
		}

		if(IC.getText().toString().equals("")){
			IC.requestFocus();
			IC.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
			return false;
		}

		if(MobileNumber.getText().toString().equals("")){
			MobileNumber.requestFocus();
			MobileNumber.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
			return false;
		}

		if(Email.getText().toString().equals("")){
			Email.requestFocus();
			Email.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
			return false;
		}else if(!validateEmail(Email.getText().toString())){
			Email.setError("Incorrect Email! Example: abc@def.com.my", error_indicator);
			return false;
		}

		if(Username.getText().toString().equals("")){
			Username.requestFocus();
			Username.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
			return false;
		}else if(Username.getText().toString().matches(".*\\s+.*")){
			Username.setError("No space allowed for user name!", error_indicator);
			return false;
		}

		if(Password.getText().toString().equals("")){
			Password.requestFocus();
			Password.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
			return false;
		}else if(Password.getText().toString().trim().length()<6){
			Password.setError("Invalid password format, password must consists at least six character.",error_indicator);
		}else if(!validatePwd(Password.getText().toString())){
			Password.setError("Invalid password format, password must consists alphabet and number, no special characters allowed.", error_indicator);
			return false;
		}

		if(ConfirmPwd.getText().toString().equals("")){
			ConfirmPwd.requestFocus();
			ConfirmPwd.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
			return false;
		}else if(!ConfirmPwd.getText().toString().equals(Password.getText().toString())){
			ConfirmPwd.setError("Password Not Match!",error_indicator);
			return false;
		}

		if(linearOthQuestion.isShown()){
			if(HintOthQues.getText().toString().equals("")){
				HintOthQues.requestFocus();
				HintOthQues.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
				return false;
			}
		}

		if(HintAnswer.getText().toString().equals("")){
			HintAnswer.requestFocus();
			HintAnswer.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
			return false;
		}
		return true;
	}
	*/

	public class SpinnerItemSelected implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			// TODO Auto-generated method stub
			switch(position){
			case 1:
				linearOthQuestion.setVisibility(View.VISIBLE);
				HintOthQues.requestFocus();
				break;
			case 2:
				linearOthQuestion.setVisibility(View.GONE);
				HintAnswer.requestFocus();
				hintQues = arg0.getItemAtPosition(position).toString();
				break;
			case 3:
				linearOthQuestion.setVisibility(View.GONE);
				HintAnswer.requestFocus();
				hintQues = arg0.getItemAtPosition(position).toString();
				break;
			case 4:
				linearOthQuestion.setVisibility(View.GONE);
				HintAnswer.requestFocus();
				hintQues = arg0.getItemAtPosition(position).toString();
				break;
			}	
		}

		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	private class EmptyTextListener implements OnEditorActionListener{
		private EditText et;

		public EmptyTextListener(EditText editT){
			this.et = editT;
		}

		public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
			if(actionId == EditorInfo.IME_ACTION_NEXT){
				// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3
				boolean ans = validation(v);
				/* KwokWai@2012812 - Change_Request-20120719, ReqNo.3
				switch(v.getId()){

				case R.id.nameTxt: {
					if(et.getText().toString().trim().equals("")){
						et.requestFocus();
						et.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					}
				}
				break;
				case R.id.NRICTxt: {
					if(et.getText().toString().trim().equals("")){
						et.requestFocus();
						et.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					}
				}
				break;
				case R.id.mobileTxt: {
					if(et.getText().toString().trim().equals("")){
						et.requestFocus();
						et.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					}
				}
				break;
				case R.id.emailTxt: {
					if(et.getText().toString().trim().equals("")){
						et.requestFocus();
						et.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					}else if(!validateEmail(et.getText().toString().trim())){
						et.setError("Incorrect Email, Example: abc@def.com.my", error_indicator);
					}
				}
				break;
				case R.id.usernameTxt: {
					if(et.getText().toString().trim().equals("")){
						et.requestFocus();
						et.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					}else if(et.getText().toString().trim().matches(".*\\s+.*")){
						et.setError("No space allowed for user name.", error_indicator);
					}
				}
				break;

				case R.id.passwordTxt: {
					if(et.getText().toString().trim().equals("")){
						et.requestFocus();
						et.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					}else if(Password.getText().toString().trim().length()<6){
						Password.setError("Invalid password format, password must consists at least six character.",error_indicator);
					}else if(!validatePwd(et.getText().toString().trim())){
						et.setError("Invalid password format, password must consists an alphabet and a number, no special characters allowed.", error_indicator);
					}
				}
				break;

				case R.id.confirmpasswordTxt: {
					EditText t = (EditText)findViewById(R.id.passwordTxt);
					String pwd = t.getText().toString();
					if(et.getText().toString().trim().equals("")){
						et.requestFocus();
						et.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					}else if(!et.getText().toString().trim().equals(pwd)){
						et.setError("Password Not Match.",error_indicator);
					}
				}
				break;

				case R.id.othHintQuesTxt: {
					if(et.getText().toString().trim().equals("")){
						et.requestFocus();
						et.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					}
				}
				break;
				}*/
			}else if(actionId == EditorInfo.IME_ACTION_DONE){
				// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3
				/* Mary@20130605 - comment unused variable
				boolean ans = validation(v);
				*/
				 validation(v);
				/* KwokWai@2012812 - Change_Request-20120719, ReqNo.3
				switch(v.getId()){
				case R.id.hintAnswerTxt: {
					if(et.getText().toString().trim().equals("")){
						et.requestFocus();
						et.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					}
				}
				break;
				} */
			}
			return false;
		}
	}

	// KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - START
	public boolean validation(TextView v){
		
		switch(v.getId()){

		case R.id.nameTxt: {
			if(Name.getText().toString().trim().equals("")){
				Name.requestFocus();
				InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				mgr.showSoftInput(Name, InputMethodManager.SHOW_IMPLICIT);
				Name.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
				return false;
			}
		}
		break;
		case R.id.NRICTxt: {
			if(IC.getText().toString().trim().equals("")){
				IC.requestFocus();
				IC.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
				return false;
			}
			else if(!validateIC(IC.getText().toString().trim())){
				IC.setError(getResources().getString(R.string.regis_err_ic_invalid), error_indicator);
				return false;
			}
		}
		break;
		case R.id.mobileTxt: {
			if(MobileNumber.getText().toString().trim().equals("")){
				MobileNumber.requestFocus();
				MobileNumber.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
				return false;
			}
		}
		break;
		case R.id.emailTxt: {
			if(Email.getText().toString().equals("")){
				Email.requestFocus();
				Email.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
				return false;
			}else if(!validateEmail(Email.getText().toString())){
				Email.setError(getResources().getString(R.string.regis_err_email_incorrect), error_indicator);
				Email.requestFocus();
				return false;
			}
		}
		break;
		case R.id.usernameTxt: {
			if(Username.getText().toString().equals("")){
				Username.requestFocus();
				Username.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
				return false;
			}else if(Username.getText().toString().matches(".*\\s+.*")){
				Username.requestFocus();
				Username.setError(getResources().getString(R.string.regis_err_username), error_indicator);
				
				return false;
			}
		}
		break;

		case R.id.passwordTxt: {
			if(Password.getText().toString().equals("")){
				Password.requestFocus();
				Password.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
				return false;
			}else if(Password.getText().toString().trim().length()<6){
				Password.requestFocus();
				Password.setError(getResources().getString(R.string.regis_err_passwd_invalid_length),error_indicator);
				return false;
			}else if(!validatePwd(Password.getText().toString())){
				Password.requestFocus();
				Password.setError(getResources().getString(R.string.regis_err_passwd_invalid_format), error_indicator);
				return false;
			}
		}
		break;

		case R.id.confirmpasswordTxt: {
			if(ConfirmPwd.getText().toString().equals("")){
				ConfirmPwd.requestFocus();
				ConfirmPwd.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
				return false;
			}else if(!ConfirmPwd.getText().toString().equals(Password.getText().toString())){
				ConfirmPwd.requestFocus();
				ConfirmPwd.setError(getResources().getString(R.string.regis_err_passwd_not_matched),error_indicator);
				return false;
			}
		}
		break;

		case R.id.othHintQuesTxt: {
			if(linearOthQuestion.isShown()){
				if(HintOthQues.getText().toString().equals("")){
					HintOthQues.requestFocus();
					HintOthQues.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
					return false;
				}
			}
		}
		break;
		
		case R.id.hintAnswerTxt:{
			if(HintAnswer.getText().toString().equals("")){
				HintAnswer.requestFocus();
				HintAnswer.setError(getResources().getString(R.string.regis_err_text_empty), error_indicator);
				return false;
			}
		}break;
		
	}
		
		return true;
	}
	// KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - END
	
	
	private boolean validateEmail(String Email){

		Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
				"[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
						"\\@" +
						"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
						"(" +
						"\\." +
						"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
						")+"
				);

		Matcher m = EMAIL_ADDRESS_PATTERN.matcher(Email);

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
		if(m.matches()){
			return true;
		}else{
			return false;
		}
		*/
		return ( m.matches() ? true : false);
	}

	// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - START
	private boolean validateSpinner(Spinner s){
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		boolean isValid = false;
		try{
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
			if(s.getSelectedItemPosition() == 0){
				final AlertDialog alertdialog = new AlertDialog.Builder(RegisterActivity.this).create();
				alertdialog.setTitle(getResources().getString(R.string.dialog_title_error));
				alertdialog.setMessage(getResources().getString(R.string.regis_hint_warning));
				alertdialog.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
				        alertdialog.dismiss();
				        hintQuestion.setFocusable(true);
				        hintQuestion.setFocusableInTouchMode(true);
						hintQuestion.requestFocus();
					} 
				});
				
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! RegisterActivity.this.isFinishing() )
					alertdialog.show();
				
				/* Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				return false;
				*/
				isValid = false;
			}else{
				/* Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				return true;
				*/
				isValid = true;
			}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		return isValid;
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - END

	
	private boolean validatePwd(String password){

		String PASSWORD_PATTERN = "(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,12})$";

		Pattern p = Pattern.compile(PASSWORD_PATTERN);
		Matcher m = p.matcher(password);

		if(m.matches()){
			CharSequence cs = new String(password);

			for(int i=0; i<cs.length(); i++){
				if(!Character.isLetterOrDigit(cs.charAt(i))){
					return false;
				}
				return true;
			}
		}

		return false;
	}
	
	// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - START
	private boolean validateIC(String ic){

		String PASSWORD_PATTERN = "[a-zA-Z0-9]{0,15}";

		Pattern p = Pattern.compile(PASSWORD_PATTERN);
		Matcher m = p.matcher(ic);
		if(m.matches()){
			CharSequence cs = new String(ic);


			for(int i=0; i<cs.length(); i++){
				if(!Character.isLetterOrDigit(cs.charAt(i))){
					return false;
				}
				return true;
			}
		}

		return false;
	}
	
	public void setClearTxtBtn(final EditText et){
		final Drawable x = getResources().getDrawable(R.drawable.clear_icon);
		x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());
		et.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		et.setCompoundDrawables(null, null, et.getText().toString().equals("") ? null : x, null);
		et.setOnTouchListener(new OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		        if (et.getCompoundDrawables()[2] == null) {
		            return false;
		        }
		        if (event.getAction() != MotionEvent.ACTION_UP) {
		            return false;
		        }
		        if (event.getX() > et.getWidth() - et.getPaddingRight() - x.getIntrinsicWidth()) {
		            et.setText("");
		            et.setCompoundDrawables(null, null, null, null);
		        }
		        return false;
		    }
		});
		et.addTextChangedListener(new TextWatcher() {
		    @Override
		    public void onTextChanged(CharSequence s, int start, int before, int count) {
		        et.setCompoundDrawables(null, null, et.getText().toString().equals("") ? null : x, null);
		    }

		    @Override
		    public void afterTextChanged(Editable arg0) {
		    }

		    @Override
		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		    }
		});

	} 
	// Added by KwokWai@2012812 - Change_Request-20120719, ReqNo.3 - END
}
