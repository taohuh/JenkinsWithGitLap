package com.n2nconnect.android.stock.custom;

/**
 *
 * This is class basically a container for you menu items.
 *
 */
public class CustomMenuItem {
	
	/**
	 * Some global variables.
	 */
	private String caption = null;
	private int imageResourceId = -1;
	private int imageResourceIdPressedState	= -1;		//Added by Thinzar, Change_Request-20160101, ReqNo.3
	private int id = -1;
	
	/**
	 * Use this method to set the caption displayed under the icon for a menu item.
	 * @param String caption
	 * @return void
	 */	
	public void setCaption(String caption) { 
		this.caption = caption;	
	}
	
	/**
	 * Use this method to get the caption displayed under the icon for a menu item.
	 * @return String caption
	 */	
	public String getCaption() { 
		return caption; 
	}

	/**
	 * Use this method to set the resource ID for the drawable displayed for a menu item.
	 * @param int imageResourceId
	 * @return void
	 */	
	public void setImageResourceId(int imageResourceId) { 
		this.imageResourceId = imageResourceId; 
	}

	/**
	 * Use this method to get the resource ID for the drawable displayed for a menu item.
	 * @return int imageResourceId
	 */
	public int getImageResourceId() { 
		return imageResourceId;	
	}

	/**
	 * Use this method to set an ID to be returned when this menu item is clicked.
	 * This is really for convenience only and optional.
	 * @param int id
	 * @return void
	 */
	public void setId(int id) { 
		this.id = id; 
	}

	/**
	 * Use this method to get an ID assigned to a menu item.
	 * This is really for convenience only and optional.
	 * @return int id
	 */
	public int getId() { 
		return id; 
	}

	//Added by Thinzar, Change_Request-20160101, ReqNo.3
	public int getImageResourceIdPressedState() {
		return imageResourceIdPressedState;
	}

	public void setImageResourceIdPressedState(int imageResourceIdPressedState) {
		this.imageResourceIdPressedState = imageResourceIdPressedState;
	}
}
