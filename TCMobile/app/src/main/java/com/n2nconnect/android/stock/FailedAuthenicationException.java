package com.n2nconnect.android.stock;


public class FailedAuthenicationException extends Exception {
	
	private String errorCode;
	
	public FailedAuthenicationException(String errorMessage) {
	    super(errorMessage);
	}

	public FailedAuthenicationException(String errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
	}
	
}
