package com.n2nconnect.android.stock.activity;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class ShowInfoActivity extends CustomWindow implements OnDoubleLoginListener, OnRefreshStockListener {
	
	private TextView Version;
	/* Mary@20130125 - Fixes_Request-20130110, ReqNo.15
	private ListView KeyFeatures;
	*/
	private TableLayout tlKeyFeatures;
	private TextView TextRequirements;
	/* Mary@20130605 - comment unused variable
	private ArrayList<String> item = new ArrayList<String>();
	*/
	/* Amend Text Color
	private ListViewItem adapter;
	*/
	private ArrayAdapter<String> adapter;
	private DoubleLoginThread loginThread;
	  
	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			Bundle bundle		= this.getIntent().getExtras();
			int infoType 		= bundle.getInt(ParamConstants.INFO_TYPE_TAG);
			// Added by Mary@20130117 - Fixes_Request-20130108, ReqNo.7 - START
			boolean isFromLogin = bundle.getBoolean(ParamConstants.FROM_LOGIN_TAG, false);
			menuButton.setVisibility(isFromLogin ? View.GONE : View.VISIBLE);
			// Added by Mary@20130117 - Fixes_Request-20130108, ReqNo.7 - END
	
			AppConstants.showMenu = true;
	
			if (infoType == AppConstants.INFO_ABOUT_TYPE) {
				this.title.setText(getResources().getString(R.string.settings_info_about));
				setContentView(R.layout.about);
	
				Version = (TextView) findViewById(R.id.Version);
				Version.setText(R.string.version_tag);
	
				// Added by Mary@20130125 - Fixes_Request-20130110, ReqNo.15 - START
	        	TextView tvAppIntro	= (TextView)findViewById(R.id.app_intro);
	        	String strAppIntro	= AppConstants.brokerConfigBean.getStrAppIntro();
	        	if( strAppIntro == null || (strAppIntro != null && strAppIntro.trim().length() == 0) ){
		        	RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		        	lp.setMargins(0, 0, 0, 0);
		        	tvAppIntro.setLayoutParams(lp);
	        	}
	        	tvAppIntro.setText( strAppIntro );
	        	strAppIntro			= null;
	        	// Added by Mary@20130125 - Fixes_Request-20130110, ReqNo.15 - END
	        	
	        	// Added by Mary@20130718 - Fixes_Request-20130424, ReqNo.8 - START
	        	TextView tvAppConclude	= (TextView)findViewById(R.id.app_conclude);
	        	String strAppConclude	= AppConstants.brokerConfigBean.getStrAppConclude();
	        	tvAppConclude.setText( strAppConclude );
	        	strAppConclude			= null;
	        	
	        	TextView tvKeyFeatureTitle	= (TextView)findViewById(R.id.keyFeatures);
	        	String strKeyFeatureTitle	= AppConstants.brokerConfigBean.getStrKeyFeaturesTitle();
	        	tvKeyFeatureTitle.setText( strKeyFeatureTitle );
	        	strKeyFeatureTitle			= null;
	        	// Added by Mary@20130718 - Fixes_Request-20130424, ReqNo.8 - END
	        	
	        	/* Mary@20130125 - Fixes_Request-20130110, ReqNo.15 - START
	        	KeyFeatures = (ListView)findViewById(R.id.listKeyFeatures);
	        	/* Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	        	item.add(" - Singapore Equities Real-Time Streaming Feed");
	        	item.add(" - Symbol Search");
	        	item.add(" - Watch List");
	        	item.add(" - Stock Info");
	        	item.add(" - Market Depth");
	        	item.add(" - Intraday & Historical Charts");
	        	item.add(" - Stock News");
	        	item.add(" - Order Book & Order Details");
	        	item.add(" - Multi-Instrument Order Pad");
	        	item.add(" - Equity Portfolio");
	        	/* Amend text color
	        	adapter = new ListViewItem(ShowInfoActivity.this, item);
	        	/
	        	adapter = new ArrayAdapter(this, R.layout.list_item_1, android.R.id.text1, item);
	        	/        	
	        	adapter = new ArrayAdapter( this, R.layout.list_item_1, R.id.tvItem, AppConstants.brokerConfigBean.getALKeyFeatures() )
	        	// Added by Mary@20121009 - Fixes_Request-20120815, ReqNo.3 - START
	        	{
	        		@Override
	        	    public View getView(int position, View convertView, ViewGroup parent) {
	        			View rowView	= convertView;
	        			 if (rowView == null) {
	    	                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	                rowView = vi.inflate(R.layout.list_item_1, null);
	    	            }
	        	        TextView textView	= (TextView) rowView.findViewById(R.id.tvItem);
	        	        if (textView != null){
	        	        	if(! AppConstants.hasMultiLogin)
	        	        		textView.setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
	        	        	
	        	        	textView.setText( AppConstants.brokerConfigBean.getALKeyFeatures().get(position) );
	        	        }
	
	        	        return rowView;
	        	    }
	        	};
	        	// Added by Mary@20121009 - Fixes_Request-20120815, ReqNo.3 - END
	        	// Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	        	KeyFeatures.setAdapter(adapter);
	        	*/
	        	tlKeyFeatures	= (TableLayout)findViewById(R.id.listKeyFeatures);
	        	tlKeyFeatures.removeAllViews();
	        	
	        	for( int i=0; i<AppConstants.brokerConfigBean.getALKeyFeatures().size(); i++ ){
	        		final TableRow tableRow			= new TableRow(this);   
	    			tableRow.setId(i);
	    			tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
	    			tableRow.setWeightSum(1);
	    			View rowView 					= this.getLayoutInflater().inflate(R.layout.list_item_1, null);  
	    			
	    			TextView textView				= (TextView) rowView.findViewById(R.id.tvItem);
	    	        if (textView != null){
	    	        	/*
	    	        	if(! AppConstants.hasMultiLogin)
	    	        		textView.setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
	    	        	*/
	    	        	textView.setText( AppConstants.brokerConfigBean.getALKeyFeatures().get(i) );
	    	        }
	    	        tableRow.addView(rowView);
	    	        
	    	        tlKeyFeatures.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) ); 
	        	}
	        	// Mary@20130125 - Fixes_Request-20130110, ReqNo.15 - END
	
				TextRequirements = (TextView) findViewById(R.id.textrequirement);
				TextRequirements.setText(AppConstants.brokerConfigBean.getStrAppRequirement());		//Edited by Thinzar@20141023, Change_Request-20140901, ReqNo. 10
	
				// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
				if (!AppConstants.hasMultiLogin) {
					// Added by Mary@20121009 - Fixes_Request-20120815, ReqNo.3
					TextView tvAboutPoweredBy = (TextView) findViewById(R.id.tvAboutPoweredBy);
	
					((LinearLayout) findViewById(R.id.ll_about)).setBackgroundResource(AppConstants.brokerConfigBean.getIntBgImg());
	
					//((TextView) findViewById(R.id.keyFeatures)).setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
					//((TextView) findViewById(R.id.requirement)).setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
					//((TextView) findViewById(R.id.textrequirement)).setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
					//Version.setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
					// Added by Mary@20121009 - Fixes_Request-20120815, ReqNo.3
					//tvAboutPoweredBy.setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
					// Added by Mary@20130718 - Fixes_Request-20130424, ReqNo.8 - START
					//((TextView) findViewById(R.id.app_intro)).setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
					//((TextView) findViewById(R.id.app_conclude)).setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
					// Added by Mary@20130718 - Fixes_Request-20130424, ReqNo.8 - END
	
					// Added by Mary@20121009 - Fixes_Request-20120815, ReqNo.3
					tvAboutPoweredBy.setVisibility(AppConstants.brokerConfigBean.showPoweredByStatement() ? View.VISIBLE : View.GONE);
				}
				// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	
			} else if (infoType == AppConstants.INFO_TERM_TYPE) {
				/*
				 * Mary@20121102 - Fixes_Request-20121102, ReqNo.3
				 * this.title.setText("Terms of Service");
				 */
				setContentView(R.layout.terms_conditions);
	
				this.title.setText(getResources().getString(R.string.terms_module_title));
				( (TextView) findViewById(R.id.tvTitle)).setVisibility(View.GONE);
				// ( (TextView)findViewById(R.id.tvTitle) ).setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
				this.icon.setImageResource(AppConstants.brokerConfigBean.getIntStkInfoMenuIconBgImg());
				
				// Added by Mary@20130718 - Fixes_Request-20130424, ReqNo.8 - START
				/*
				if (!AppConstants.hasMultiLogin) {				
					( (TextView) findViewById(R.id.tvTitle)).setTextColor(AppConstants.brokerConfigBean.getIntTextColor() );
				}*/
				// Added by Mary@20130718 - Fixes_Request-20130424, ReqNo.8 - START
	
				// Added by Mary@20121102 - Fixes_Request-20121102, ReqNo.3 - START
				adapter = new ArrayAdapter<String>(this, R.layout.list_item_1, R.id.tvItem, AppConstants.brokerConfigBean.getALTermsAndConditions()) {
					@Override
					public View getView(int position, View convertView, ViewGroup parent) {
						View rowView = convertView;
						if (rowView == null) {
							LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
							rowView = vi.inflate(R.layout.list_item_1, null);
						}
						TextView textView = (TextView) rowView.findViewById(R.id.tvItem);
						if (textView != null) {
							/*
							if (!AppConstants.hasMultiLogin)
								textView.setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
							*/
							textView.setText(AppConstants.brokerConfigBean.getALTermsAndConditions().get(position));
						}
	
						return rowView;
					}
				};
	
				((ListView) findViewById(R.id.lvTermsNConditions)).setAdapter(adapter);
				// Added by Mary@20121102 - Fixes_Request-20121102, ReqNo.3 - END
			} else if (infoType == AppConstants.INFO_PRIVACY_TYPE) {
				this.title.setText(getResources().getString(R.string.settings_info_privacy));
				setContentView(R.layout.terms_conditions);
	
				( (TextView) findViewById(R.id.tvTitle)).setVisibility(View.GONE);
				// ( (TextView)findViewById(R.id.tvTitle) ).setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
				this.icon.setImageResource(AppConstants.brokerConfigBean.getIntStkInfoMenuIconBgImg());
				/*
				if (!AppConstants.hasMultiLogin) {			
					( (TextView) findViewById(R.id.tvTitle)).setTextColor(AppConstants.brokerConfigBean.getIntTextColor() );
				}*/
	
				adapter = new ArrayAdapter<String>(this, R.layout.list_item_1, R.id.tvItem, AppConstants.brokerConfigBean.getALPrivacyPolicy()) {
					@Override
					public View getView(int position, View convertView, ViewGroup parent) {
						View rowView = convertView;
						if (rowView == null) {
							LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
							rowView = vi.inflate(R.layout.list_item_1, null);
						}
						TextView textView = (TextView) rowView.findViewById(R.id.tvItem);
						if (textView != null) {
							/*
							if (!AppConstants.hasMultiLogin)
								textView.setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
							*/
							textView.setText(AppConstants.brokerConfigBean.getALPrivacyPolicy().get(position));
						}
	
						return rowView;
					}
				};
	
				((ListView) findViewById(R.id.lvTermsNConditions)).setAdapter(adapter);
			} else if (infoType == AppConstants.INFO_WARRANTY_TYPE) {
				this.title.setText(getResources().getString(R.string.settings_info_disclaimer));
				setContentView(R.layout.terms_conditions);
	
				// ( (TextView)findViewById(R.id.tvTitle) ).setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
				this.icon.setImageResource(AppConstants.brokerConfigBean.getIntStkInfoMenuIconBgImg());
				
				( (TextView) findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.further_warranty));
				/*
				if (!AppConstants.hasMultiLogin) {
					( (TextView) findViewById(R.id.tvTitle)).setTextColor(AppConstants.brokerConfigBean.getIntTextColor() );
				}
				*/
	
				adapter = new ArrayAdapter<String>(this, R.layout.list_item_1, R.id.tvItem, AppConstants.brokerConfigBean.getALDisclaimerOfWarranty()) {
					@Override
					public View getView(int position, View convertView, ViewGroup parent) {
						View rowView = convertView;
						if (rowView == null) {
							LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
							rowView = vi.inflate(R.layout.list_item_1, null);
						}
						TextView textView = (TextView) rowView.findViewById(R.id.tvItem);
						if (textView != null) {
							/*
							if (!AppConstants.hasMultiLogin)
								textView.setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
	*/
							textView.setText(AppConstants.brokerConfigBean.getALDisclaimerOfWarranty().get(position));
						}
	
						return rowView;
					}
				};
	
				((ListView) findViewById(R.id.lvTermsNConditions)).setAdapter(adapter);
			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - START
			}else if(infoType == AppConstants.INFO_SERVICE_TYPE){
				this.title.setText(getResources().getString(R.string.settings_info_terms));
			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - END
				setContentView(R.layout.terms_conditions);
	
				( (TextView) findViewById(R.id.tvTitle)).setVisibility(View.GONE);
				//( (TextView)findViewById(R.id.tvTitle) ).setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
				this.icon.setImageResource(AppConstants.brokerConfigBean.getIntStkInfoMenuIconBgImg());
				/*
				if (!AppConstants.hasMultiLogin) {			
					( (TextView) findViewById(R.id.tvTitle)).setTextColor(AppConstants.brokerConfigBean.getIntTextColor() );
				}
				*/
	
				adapter = new ArrayAdapter<String>(this, R.layout.list_item_1, R.id.tvItem, AppConstants.brokerConfigBean.getALTermsOfService()) {
					@Override
					public View getView(int position, View convertView, ViewGroup parent) {
						View rowView = convertView;
						if (rowView == null) {
							LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
							rowView = vi.inflate(R.layout.list_item_1, null);
						}
						TextView textView = (TextView) rowView.findViewById(R.id.tvItem);
						if (textView != null) {
							/*
							if (!AppConstants.hasMultiLogin)
								textView.setTextColor(AppConstants.brokerConfigBean.getIntTextColor());
							*/
							textView.setText(AppConstants.brokerConfigBean.getALTermsOfService().get(position));
						}
	
						return rowView;
					}
				};
	
				((ListView) findViewById(R.id.lvTermsNConditions)).setAdapter(adapter);
			// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - START
			}else if (infoType == AppConstants.INFO_SUBSCRIBE_SERVICE_TYPE){
	        	this.title.setText(getResources().getString(R.string.settings_info_subcr_services));
	        	setContentView(R.layout.subscription_service_webview);
	    		
	    		( (WebView)findViewById(R.id.webSubscriptionService) ).loadUrl( AppConstants.brokerConfigBean.getStrLMSSubscriptionUrl() 
	    																			+ "?identifier=" + sharedPreferences.getString(PreferenceConstants.USER_NAME, null) 
	    																			+ "&sponsor=" + AppConstants.brokerConfigBean.getStrLMSSponsor() 
	    																			+ "&prod=" + AppConstants.brokerConfigBean.getStrLMSProduct() 
	    																			+ "&showallsubsp=1" 
	    																		);
	    		
	    		this.menuButton.setOnClickListener(new OnClickListener(){
	    			@Override
	    			public void onClick(View arg0) {
	    				finish();
	    				
	    				Intent intent = new Intent();
	    				intent.setClass(ShowInfoActivity.this, LoginActivity.class);
	    				startActivity(intent);
	    			}
	    			
	    		});
	        } else if (infoType == AppConstants.INFO_SUBSCRIBE_STATUS_TYPE){
	        	this.title.setText(getResources().getString(R.string.settings_info_subcr_status));
	        	setContentView(R.layout.subscription_service_webview);
	        	
	        	( (WebView)findViewById(R.id.webSubscriptionService) ).loadUrl( AppConstants.brokerConfigBean.getStrLMSSubscriptionStatusUrl() 
	        																		+ "?identifier=" + sharedPreferences.getString(PreferenceConstants.USER_NAME, null) 
	        																		+ "&sponsor=" + AppConstants.brokerConfigBean.getStrLMSSponsor() 
	        																		+ "&prod=" + AppConstants.brokerConfigBean.getStrLMSProduct() 
	        																		+ "&showallsubsp=1" 
	        																	);
			// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - END
			}
	
			/*
			 * Mary@20120814 - Change_Request-20120719, ReqNo.4
			 * this.icon.setImageResource(R.drawable.menuicon_acc_setting);
			 */
			this.icon.setImageResource(AppConstants.brokerConfigBean.getIntAccSettingMenuIconBgImg());
	
			loginThread = ((StockApplication) ShowInfoActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) ShowInfoActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			
			// Added by Mary@20130710 - GooglePlayReport-20130702, CrashNo.3
			if(loginThread != null)
				loginThread.setRegisteredListener(this);
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}

	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
	@Override
	public void onResume(){
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onResume();
			
			currContext	= ShowInfoActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(ShowInfoActivity.this);
				loginThread.resumeRequest();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
	
	// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	
	@Override
	protected void onDestroy() {
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onDestroy();
			
			if (this.mainMenu.isShowing()) {
				this.mainMenu.hide();
			}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj		= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler() {
		public void handleMessage(Message message) {
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if (response.get(2).equals("1102")) {
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if (ShowInfoActivity.this.mainMenu.isShowing()) {
					ShowInfoActivity.this.mainMenu.hide();
				}
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				
				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					ShowInfoActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		// TODO Auto-generated method stub
		
	}
}
