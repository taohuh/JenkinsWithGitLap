package com.n2nconnect.android.stock.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class DtiTermsActivity extends CustomWindow {

	private ArrayAdapter<String> adapter;
	private Button btnAgreeDtiTerms;
	private Button btnDtiBackHome;
	private ListView lvDtiTerms;
	private boolean isFromDtiActivity	= false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_dti_terms);
		
		this.title.setText(getResources().getString(R.string.dti_terms_module_title));
		this.icon.setImageResource(R.drawable.disclaimer);
		
		Bundle bundle	= this.getIntent().getExtras();
		isFromDtiActivity	= bundle.getBoolean(ParamConstants.IS_FROM_DTI_TAG);
		
		btnAgreeDtiTerms	= (Button) findViewById(R.id.btn_agree_dti_terms);
		btnDtiBackHome		= (Button) findViewById(R.id.btn_back_home);
		lvDtiTerms			= (ListView)findViewById(R.id.lv_dti_terms);
		
		adapter = new ArrayAdapter<String>(this, R.layout.list_item_1, R.id.tvItem, AppConstants.brokerConfigBean.getAlDtiDisclaimer()) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View rowView = convertView;
				if (rowView == null) {
					LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					rowView = vi.inflate(R.layout.list_item_1, null);
				}
				TextView textView = (TextView) rowView.findViewById(R.id.tvItem);
				if (textView != null) {
					textView.setText(AppConstants.brokerConfigBean.getAlDtiDisclaimer().get(position));
				}

				return rowView;
			}
		};

		lvDtiTerms.setAdapter(adapter);
		
		btnAgreeDtiTerms.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				AppConstants.hasDtiTermsShown	= true;
				if(isFromDtiActivity){
					Intent intent = new Intent();
					setResult(RESULT_OK, intent);        
					finish();
				}else{
					Intent i = new Intent();
					i.setClass(DtiTermsActivity.this, DtiActivity.class);
					startActivity(i);
				}
				
				DtiTermsActivity.this.finish();
			}
		});
		
		btnDtiBackHome.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(DtiTermsActivity.this, QuoteActivity.class);
				intent.putExtra(ParamConstants.FROM_EXCHANGE_TAG, false);
				startActivity(intent);
				
				finish();
			}
		});
	}
}