package com.n2nconnect.android.stock;

import java.util.ArrayList;

public class FeedObject{
    
    private ArrayList <String> m_alFeedElementData = new ArrayList <String>();
    private ArrayList <String> m_alFeedValueData = new ArrayList <String>();

    public String getFeedElementAt(int iIndex) {

        return (String)m_alFeedElementData.get(iIndex);
    }

    public String getFeedValueAt(int iIndex) {

        return (String)m_alFeedValueData.get(iIndex);
    }

    public int getTotalFeedElement() {

        return m_alFeedElementData.size();
    }

    public String getStockCode() {

        int iIndex = m_alFeedElementData.indexOf("33");

        if(iIndex != -1) {
            return m_alFeedValueData.get(iIndex);
        } else {
            return "";
        }
    }

    public String getExchangeCode() {

        int iIndex = m_alFeedElementData.indexOf("131");

        if(iIndex != -1) {
            return m_alFeedValueData.get(iIndex);
        } else {
            return "";
        }
    }

    public void setStringData(int iFieldId, String sValue) {

        String sField = String.valueOf(iFieldId);

        if(!m_alFeedElementData.contains(sField)) {
            m_alFeedElementData.add(sField);
            m_alFeedValueData.add(sValue);
        } else {
        	m_alFeedValueData.set(m_alFeedElementData.indexOf(sField), sValue);
        }
    }

    public void setNumericData(int iFieldId, String sValue) {

        String sField = String.valueOf(iFieldId);

        sValue = formatInteger(sValue);
        if(!m_alFeedElementData.contains(sField)) {
            m_alFeedElementData.add(sField);
            m_alFeedValueData.add(sValue);
        } else {
        	m_alFeedValueData.set(m_alFeedElementData.indexOf(sField), sValue);
        }
    }

    public void setNumericWithRoundupData(int iFieldId, String sValue) {

        String sField = String.valueOf(iFieldId);

        sValue = formatInteger(sValue);
        sValue = roundupInteger(sValue);
        if(!m_alFeedElementData.contains(sField)) {
            m_alFeedElementData.add(sField);
            m_alFeedValueData.add(sValue);
        } else {
        	m_alFeedValueData.set(m_alFeedElementData.indexOf(sField), sValue);
        }
    }

    public void setDateTimeData(int iFieldId, String sValue) {

        String sField = String.valueOf(iFieldId);

        sValue = formatInteger(sValue);
        if(!m_alFeedElementData.contains(sField)) {
            m_alFeedElementData.add(sField);
            m_alFeedValueData.add(sValue);
        } else {
        	m_alFeedValueData.set(m_alFeedElementData.indexOf(sField), sValue);
        }
    }

    public void replaceTo(ArrayList <String> alOriginalElement, ArrayList <String> alOriginalValue) throws Exception {

        int iLengthOri = alOriginalElement.size();
        int iLengthDest = m_alFeedElementData.size();
        String sFeedElement;
        boolean bFound;

        for(int i = 0; i < iLengthOri; i++) {
            sFeedElement = alOriginalElement.get(i);
            if(sFeedElement != null) {
                bFound = false;
                for(int j = 0; j < iLengthDest; j++) {
                    try {
                        if(sFeedElement.compareTo(m_alFeedElementData.get(j)) == 0) {
                            m_alFeedValueData.set(j, alOriginalValue.get(i));
                            bFound = true;
                        }
                    } catch (Exception e) {
                        bFound = false;
                    }
                }
                if(!bFound) {
                    m_alFeedElementData.add(sFeedElement);
                    m_alFeedValueData.add(alOriginalValue.get(i));
                }
            }
        }
    }

    public ArrayList <String> getElementArrayList() {

        return m_alFeedElementData;
    }

    public ArrayList <String> getValueArrayList() {

        return m_alFeedValueData;
    }

    private String formatInteger(String value) {

        if (value == null || value.length() == 0) {
            return null;
        }

        if (value.length() == 1) {
            return value;
        }

        String sDigit = value.substring(value.length()-1, value.length());
        value = value.substring(0, value.length()-1);

        int digit = 0;
        try {
            digit = Integer.parseInt(sDigit);
        } catch (NumberFormatException e) {
        }
        StringBuffer buf = new StringBuffer(value);
        if (digit > 0) {
            if (buf.length() - digit >= 0) {
                buf = buf.insert(buf.length()-digit, ".");
            } else {
                while (buf.length() - digit < 0) {
                    buf.insert(0, '0');
                }
                buf = buf.insert(0, ".");
            }
            // Added by Mary@20130605 - avoid IndexOutOfBoundException
            if(buf.length() > 0){
	            while (buf.charAt(0) == '0') {
	                buf.deleteCharAt(0);
	            }
	            if(buf.charAt(0) == '.')
	                buf.insert(0, '0');
            }
            value = buf.toString();
        } else {
        	// Added by Mary@20130605 - avoid IndexOutOfBoundException
        	if(buf.length() > 0){
	            while (buf.charAt(0) == '0') {
	                if(buf.length() > 1){
	                    buf.deleteCharAt(0);
	                }else{
	                    break;
	                }
	            }
        	}
            value = buf.toString();
        }
        return value;
    }

    private String roundupInteger(String value) {

        if (value == null) {
            return null;
        }

        int dotIdx = value.indexOf(".");
        if (dotIdx == -1) {
            return value;
        } else {
            if(dotIdx == 0) {
                value = "0";
            } else {
                value = value.substring(0, dotIdx);
            }
            return value;
        }
    }

    @Override
    public String toString() {

        StringBuffer sbResult = new StringBuffer();
        int iLength = m_alFeedElementData.size();

        for(int i = 0; i < iLength; i++) {
            sbResult.append(m_alFeedElementData.get(i));
            sbResult.append(':');
            sbResult.append(m_alFeedValueData.get(i));
            sbResult.append(",");
        }
        return sbResult.toString();
    }
}
