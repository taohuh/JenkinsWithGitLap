package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.IBinder;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.model.Watchlist;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;
//import com.sun.istack.internal.NotNull;
import android.support.annotation.NonNull;

/**
 *
 * This is the class that manages our menu items and the popup window.
 */
public class WatchListView {

	/**
	 * Some global variables.
	 */
	private List<Watchlist> watchlists;
	private OnWatchlistEventListener listener;
	private Context context;
	private LayoutInflater layoutInflater;
	private PopupWindow popupWindow;
	private boolean isShowing;
	private boolean hideOnSelect;
	private Watchlist selectedWatchlist;
	private int selectedRow;
	private View windowView;
	private View view;
	
	public interface OnWatchlistEventListener {
		
		public void watchlistSelectedEvent(Watchlist watchList);
		
		public void watchlistUpdatedEvent(String watchlistName, String watchlistId, boolean isNew);
		
		public void watchlistDeletedEvent(String watchlistId);
		
		public void showKeyboardEvent();
		
		public void hideKeyboardEvent(EditText editText);
		
	}
	
	public void positionSector() {
		
		ScrollView watchlistScroll = (ScrollView) windowView.findViewById(R.id.watchlistScroll);
		
		int index = 1;
		for (Iterator itr=watchlists.iterator(); itr.hasNext(); ) {
			Watchlist compare = (Watchlist) itr.next();
			if (compare.getId().equals(selectedWatchlist.getId())) {
				break;
			}
			index++;
		}
		int scrollHeight = (index * 35) - 80;
		if (scrollHeight>0) {
			watchlistScroll.scrollTo(0, scrollHeight);
		}
		TableLayout tableLayout = (TableLayout) windowView.findViewById(R.id.watchlistTable);
		TableRow tableRow = (TableRow) tableLayout.getChildAt(index-1);
		TextView textLabel = (TextView) tableRow.getChildAt(0);
		textLabel.setBackgroundColor(Color.DKGRAY);
		tableRow.setBackgroundColor(Color.DKGRAY);
		textLabel.setTextColor(Color.WHITE);
  		selectedRow = index - 1;
		
	}
	
	
	public boolean isShowing() { 
		return isShowing; 
	}
		
	public void setHideOnSelect(boolean doHideOnSelect) { 
		hideOnSelect = doHideOnSelect; 
	} 
	
	public synchronized void setWatchlists(List watchlists) throws Exception {
		if (isShowing) {
			throw new Exception("Menu list may not be modified while menu is displayed.");
		}
		this.watchlists = watchlists;
	}
	
	public WatchListView(Context context, OnWatchlistEventListener listener, LayoutInflater inflater) {
		this.listener = listener;
		watchlists = new ArrayList();
		this.context = context;
		layoutInflater = inflater;
	}
	
	public synchronized void show(View view, Watchlist watchlist) {
		
		this.selectedWatchlist = watchlist;
		this.view = view;
		isShowing = true;
		boolean isLandscape = false;
		int itemCount = watchlists.size();
		if (itemCount<1) {
			return; 
		}
		if (popupWindow != null) {
			return; 
		}
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		if (display.getWidth() > display.getHeight()) {
			isLandscape = true;
		}
		
		windowView = layoutInflater.inflate(R.layout.watchlist_table_window, null);

        this.constructTableView();
              
		popupWindow = new PopupWindow(windowView, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, false);
        popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
        popupWindow.setWidth(display.getWidth());
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        
        windowView.post(new Runnable() {
        	public void run() {
        		WatchListView.this.positionSector();
        	}
        });
        		
        Button newButton = (Button) windowView.findViewById(R.id.newButton);
        newButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(CustomWindow.idleThread != null && ! CustomWindow.isShowingIdleAlert)
					CustomWindow.idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				WatchListView.this.hide();
				WatchListView.this.enterWatchlistName("0", true);
				listener.showKeyboardEvent();
				 
			}
		});
	}
	
	/*  Mary@20130828 - Fixes_Request-20130711, ReqNo.17
	private void enterWatchlistName(final String watchlistId, final boolean isNew) {
	*/
	public void enterWatchlistName(final String watchlistId, final boolean isNew) {
	// Added by Mary@20121224-Fixes_Request-20121221, ReqNo.15 - START
		enterWatchlistName(watchlistId, isNew, "");
	}	
	
	private void enterWatchlistName(final String watchlistId, final boolean isNew, @NonNull final String strInputName) {
		
		// Added by Mary@20121224-Fixes_Request-20121221, ReqNo.15 - END
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			// Added by Mary@20130828 - Change_Request-20130724, ReqNo.10 - START
			if( isNew && AppConstants.brokerConfigBean.isImposeMaxWatchlistCreate()
					& watchlists.size() >= AppConstants.brokerConfigBean.getIntMaxNoOfWatchlistCreate() ) {
				AlertDialog warning = new AlertDialog.Builder(context)
					//.setIcon(android.R.attr.alertDialogStyle)	//Commented out by Thinzar, Fixes_Request-20140820,ReqNo.23
					.setTitle("Reach Maximum Number of Watchlist")
					.setMessage("We regret to inform that we are unable to create your watchlist as you have reached your maximum number of watchlists.")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							dialog.dismiss();
						}
					})
					.create();
				
				if( ! ( (Activity)context ).isFinishing() )
					warning.show();
				
				return;
			}else{
			// Added by Mary@20130828 - Change_Request-20130724, ReqNo.10 - END
				final EditText input = new EditText(context);
				input.setText(strInputName);
				
				AlertDialog.Builder alert = new AlertDialog.Builder(context)
					//.setIcon(android.R.attr.alertDialogStyle) //Commented out by Thinzar, Fixes_Request-20140820,ReqNo.23
					.setTitle("Watchlist Name")
					.setMessage("Please enter new name:")
					.setView(input)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							final String name = input.getText().toString().trim();
							
							// Added by Mary@20121224-Fixes_Request-20121221, ReqNo.17 - START
							if(name.length() == 0){
								AlertDialog warning = new AlertDialog.Builder(context)
				                //.setIcon(android.R.attr.alertDialogStyle) //Commented out by Thinzar, Fixes_Request-20140820,ReqNo.23
				                .setTitle("Invalid Watchlist Name")
				                .setMessage("Please make sure that you did not leave the name blank.")
				                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
				                    public void onClick(DialogInterface dialog, int whichButton) {
				                    	dialog.dismiss();
				                    	//Added by Thinzar, Fixes_Request-20140820,ReqNo.23
				                    	IBinder token = input.getWindowToken();
				                    	( ( InputMethodManager ) context.getSystemService( Context.INPUT_METHOD_SERVICE ) ).hideSoftInputFromWindow( token, 0 );
				                    	enterWatchlistName(watchlistId, isNew, name);
				                    }
				                })
				                .create();
								// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
								if( ! ( (Activity)context ).isFinishing() )
									warning.show();
								
								return;
							}
							// Added by Mary@20121224-Fixes_Request-20121221, ReqNo.17 - END
							
							// Added by Mary@20121224-Fixes_Request-20121221, ReqNo.15 - START
							if(name.length() > 20){
								AlertDialog warning = new AlertDialog.Builder(context)
				                //.setIcon(android.R.attr.alertDialogStyle) //Commented out by Thinzar, Fixes_Request-20140820,ReqNo.23
				                .setTitle("Invalid Watchlist Name")
				                .setMessage("Please make sure the name that you type does not exceed 20 characters.")
				                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
				                    public void onClick(DialogInterface dialog, int whichButton) {
				                    	dialog.dismiss();
				                    	//Added by Thinzar, Fixes_Request-20140820,ReqNo.23
				                    	IBinder token = input.getWindowToken();
				                    	( ( InputMethodManager ) context.getSystemService( Context.INPUT_METHOD_SERVICE ) ).hideSoftInputFromWindow( token, 0 );
				                    	enterWatchlistName(watchlistId, isNew, name);
				                    }
				                })
				                .create();
								
								// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
								if( ! ( (Activity)context ).isFinishing() )
									warning.show();
								
								return;
							}
							// Added by Mary@20121224-Fixes_Request-20121221, ReqNo.15 - END
							
							// Added by Mary@20121226 - Fixes_Request-20121221, ReqNo.14 - START
							Pattern p 			= Pattern.compile("[^a-z0-9 _-]", Pattern.CASE_INSENSITIVE);
							Matcher m 			= p.matcher(name);
							boolean isInvalid 	= m.find();
							if(isInvalid){
								AlertDialog warning = new AlertDialog.Builder(context)
				                //.setIcon(android.R.attr.alertDialogStyle)
				                .setTitle("Invalid Watchlist Name")
				                .setMessage("Please use only letters (a-z or A-Z), numbers (0-9) and symbol (space-_) in this field.")
				                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
				                    public void onClick(DialogInterface dialog, int whichButton) {
				                    	dialog.dismiss();
				                    	//Added by Thinzar, Fixes_Request-20140820,ReqNo.23
				                    	IBinder token = input.getWindowToken();
				                    	( ( InputMethodManager ) context.getSystemService( Context.INPUT_METHOD_SERVICE ) ).hideSoftInputFromWindow( token, 0 );
				                    	enterWatchlistName(watchlistId, isNew, name);
				                    }
				                })
				                .create();
								
								// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
								if( ! ( (Activity)context ).isFinishing() )
									warning.show();
								
								return;
							}
							// Added by Mary@20121226 - Fixes_Request-20121221, ReqNo.14 - END
							
							boolean isExists = false;
							for (Iterator<Watchlist> itr=watchlists.iterator(); itr.hasNext(); ) {
								Watchlist watchlist = itr.next();
								if( name.equalsIgnoreCase( watchlist.getName() ) 
										|| name.equalsIgnoreCase( watchlist.getName().replaceAll("_", " ") )
								){
									isExists = true;
									break;
								}
							}
							
							if (isExists) {
								listener.hideKeyboardEvent(input);
								AlertDialog warning = new AlertDialog.Builder(context)
				                //.setIcon(android.R.attr.alertDialogStyle)
				                // Mary@20121224-Fixes_Request-20121221, ReqNo.18
				                .setTitle("Duplicate Watchlist")
				                /* Mary@20130426 - Fixes_Request-20130419, ReqNo.4
				                .setMessage("Watchlist with the name \"" + name + "\" is already existed.")
				                */
				                .setMessage("Watchlist with the name \"" + name + "\" already exist")
				                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
				                    public void onClick(DialogInterface dialog, int whichButton) {
				                    	WatchListView.this.show(view, WatchListView.this.selectedWatchlist);
				                    	dialog.dismiss();
				                    	//Added by Thinzar, Fixes_Request-20140820,ReqNo.23
				                    	IBinder token = input.getWindowToken();
				                    	( ( InputMethodManager ) context.getSystemService( Context.INPUT_METHOD_SERVICE ) ).hideSoftInputFromWindow( token, 0 );
				                    }
				                })
				                .create();
								
								// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
								if( ! ( (Activity)context ).isFinishing() )
									warning.show();
								
								return;
							}
							listener.watchlistUpdatedEvent(name, watchlistId, isNew);
							//Added by Thinzar, Fixes_Request-20140820,ReqNo.23
	                    	IBinder token = input.getWindowToken();
	                    	( ( InputMethodManager ) context.getSystemService( Context.INPUT_METHOD_SERVICE ) ).hideSoftInputFromWindow( token, 0 );
						}
					})
					.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								listener.hideKeyboardEvent(input);
								WatchListView.this.show(view, WatchListView.this.selectedWatchlist);
								dialog.dismiss();
								//Added by Thinzar, Fixes_Request-20140820,ReqNo.23
		                    	IBinder token = input.getWindowToken();
		                    	( ( InputMethodManager ) context.getSystemService( Context.INPUT_METHOD_SERVICE ) ).hideSoftInputFromWindow( token, 0 );
							}
						});
				
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ( (Activity)context ).isFinishing() )
					alert.show();
			}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	
	private void constructTableView() {
		
		final TableLayout tableLayout = (TableLayout) windowView.findViewById(R.id.watchlistTable);
   	 	tableLayout.removeAllViews();
   	 	
        int id = 0;
        for (Iterator itr=watchlists.iterator(); itr.hasNext(); ) {
        	Watchlist watchlist = (Watchlist) itr.next();
       	 
            final TableRow tableRow = new TableRow(context);   
            tableRow.setId(id);
            tableRow.setLayoutParams(new TableLayout.LayoutParams(
           		 TableLayout.LayoutParams.MATCH_PARENT,
           		 TableLayout.LayoutParams.WRAP_CONTENT));
            
            tableRow.setWeightSum(1);
            
            final TextView textLabel = (TextView) layoutInflater.inflate(R.layout.sector_list_item, null);
            textLabel.setText(watchlist.getName());
                        
            tableRow.addView(textLabel);
            
            tableRow.setOnClickListener(new OnClickListener() {
           	 
	           	 public void onClick(View view) {
	           		 
	           		 final TableRow previousRow = (TableRow) tableLayout.getChildAt(selectedRow);
	           		 TextView previousLabel = (TextView) previousRow.findViewById(R.id.sectorLabel);
	           		 previousLabel.setBackgroundDrawable(null);
	           		 previousRow.setBackgroundDrawable(null);
	           		 previousLabel.setTextColor(Color.BLACK);
	           		 
	           		 selectedWatchlist = (Watchlist) watchlists.get(tableRow.getId());
	           		 selectedRow = tableRow.getId();
	           		 
	           		 textLabel.setBackgroundColor(Color.DKGRAY);
	           		 tableRow.setBackgroundColor(Color.DKGRAY);
	           		 textLabel.setTextColor(Color.WHITE);
	           		 listener.watchlistSelectedEvent(selectedWatchlist);
	           		 if (hideOnSelect) {
	           			 WatchListView.this.hide();
	           		 }
	           		 
	           	 }
            });
            
            tableRow.setOnLongClickListener(new OnLongClickListener() {
				
				public boolean onLongClick(View v) {
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					try{
						final CharSequence[] items = {context.getResources().getString(R.string.watchlist_rename),
								context.getResources().getString(R.string.watchlist_delete)};
						
						final Watchlist watchlist = (Watchlist) watchlists.get(tableRow.getId());
						
						AlertDialog.Builder builder = new AlertDialog.Builder(context);
						builder.setTitle(context.getResources().getString(R.string.watchlist_action) + " "
								+ watchlist.getName());
						builder.setItems(items, new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog, int item) {
						    	if (item==0) {
						    		WatchListView.this.enterWatchlistName(watchlist.getId(), false, watchlist.getName());
						    		
						    	} else if (item==1) {
						    		WatchListView.this.hide();
						    		listener.watchlistDeletedEvent(watchlist.getId());
						    		
						    	}
						        
						    }
						});
						AlertDialog alert = builder.create();
						
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ( (Activity)context ).isFinishing() )
							alert.show();
						
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
					}catch(Exception e){
						e.printStackTrace();
					}
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
					return true;
				}
			});
            
            
            tableLayout.addView(tableRow, new TableLayout.LayoutParams(
           		 TableRow.LayoutParams.FILL_PARENT,
           		 TableRow.LayoutParams.WRAP_CONTENT));  
            id++;
       }
      
	}
	
	
	public synchronized void hide() {
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
		try{
			isShowing = false;
			if (popupWindow != null && popupWindow.isShowing() ) {
				popupWindow.dismiss();
				popupWindow = null;
			}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
		return;
	}
}
