package com.n2nconnect.android.stock.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.Cipher;

import org.apache.commons.lang.StringUtils;
import org.jose4j.base64url.Base64;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CustomizedBase64;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.model.E2EEInfo;
//import com.sun.istack.internal.Nullable;
import android.support.annotation.Nullable;
import com.sunnic.e2ee.client.SncE2EEClient;

public class FormatUtil {
	public static final int DECIMAL_PLACE = 2;
	public static final String NEWSDATE_FORMAT_1 = "yyyy-MM-dd HH:mm:ss";
	public static final String NEWSDATE_FORMAT_2 = "yyyy-MM-dd HH:mm:ss";
	public static final String NEWSDATE_FORMAT_3 = "yyyy/MM/dd hh:mm:ss";
	public static final String format_yyyyMMdd	= "yyyy-MM-dd";
	public static final String format_ddMyyyy	= "dd M yyyy";
	
	public static int[] m_arrBT = { 13, 129, -1, 30, -2, 10 };
	public static int[] m_arrUTFFID = { 130 };

	public static Date parseDateString(String dateString, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);

		try {
			Date date = dateFormat.parse(dateString);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String formatDateString(Date date, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		String dateString = dateFormat.format(date);

		return dateString;
	}

	public static String formatTimeStamp(String timestamp) {
		// Added by Mary@20120823 - avoid StringIndexOutOfBound Exception -
		// START
		if (timestamp.length() < 14)
			return "";
		// Added by Mary@20120823 - avoid StringIndexOutOfBound Exception - END

		String time1 = timestamp.substring(0, timestamp.length() - 3);
		String time2 = timestamp.substring(14, timestamp.length());

		return time1 + "." + time2;
	}

	public static PublicKey loadPublicKey(String Key) throws Exception {
		try {
			byte[] buffer = CustomizedBase64.decode(Key);
			String hex = asHex(buffer);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
			return (RSAPublicKey) keyFactory.generatePublic(keySpec);
		} catch (FileNotFoundException e) {
			/*
			 * Mary@20130705 - public key error throw new Exception(
			 * "unable to find public key");
			 */
			throw new FileNotFoundException(ErrorCodeConstants.MISSING_PUBLIC_KEY);
		} catch (IOException e) {
			/*
			 * Mary@20130705 - public key error throw new Exception(
			 * "public key file read error");
			 */
			throw new IOException(ErrorCodeConstants.FAIL_TO_READ_PB_KEY);
		} catch (NoSuchAlgorithmException e) {
			/*
			 * Mary@20130705 - public key error throw new Exception(
			 * "no such algorithm");
			 */
			throw new NoSuchAlgorithmException(ErrorCodeConstants.NO_SUCH_ALGORITHM);
		} catch (InvalidKeySpecException e) {
			/*
			 * Mary@20130705 - public key error throw new Exception(
			 * "Invalid public key");
			 */
			throw new InvalidKeySpecException(ErrorCodeConstants.INVALID_KEY_SPEC_EXCEPTION);
		}
	}

	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
	public static PublicKey loadRSAPublicKey(String strKey, String strModulus_Base64, String strExponent_Base64)
			throws Exception {
		try {
			String hexKey = asHex(CustomizedBase64.decode(strKey));
			String strHexModulus = asHex(CustomizedBase64.decode(strModulus_Base64));
			String strHexExponent = asHex(CustomizedBase64.decode(strExponent_Base64));

			BigInteger biModulus = new BigInteger(strHexModulus, 16);
			BigInteger biExponent = new BigInteger(strHexExponent, 16);

			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec keySpec = new RSAPublicKeySpec(biModulus, biExponent);

			// X509EncodedKeySpec keySpec= new X509EncodedKeySpec(buffer);
			return (RSAPublicKey) keyFactory.generatePublic(keySpec);
		} catch (FileNotFoundException e) {
			throw new Exception("unable to find public key");
		} catch (IOException e) {
			throw new Exception("public key file read error");
		} catch (NoSuchAlgorithmException e) {
			throw new Exception("no such algorithm");
		} catch (InvalidKeySpecException e) {
			throw new Exception("Invalid public key");
		}
	}

	public static String encryptWithRSA(PublicKey pbRSAKey, String strInputToEncrypt) {
		String strRSAEncryptedOutput = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, pbRSAKey);

			byte[] inputBytes = strInputToEncrypt.getBytes(AppConstants.STRING_ENCODING_FORMAT);
			byte[] cipherBytes = new byte[cipher.getOutputSize(inputBytes.length)];
			int ctLength = cipher.update(inputBytes, 0, inputBytes.length, cipherBytes, 0);
			ctLength += cipher.doFinal(cipherBytes, ctLength);

			strRSAEncryptedOutput = asHex(cipherBytes);
			// strRSAEncryptedOutput = Base64.encodeBytes(cipherBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return strRSAEncryptedOutput;
	}

	public static String HashSHA256Generator(String value) {
		MessageDigest md;
		StringBuffer sb = null;
		byte[] digest = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(value.getBytes());
			digest = md.digest();

			sb = new StringBuffer();
			for (int i = 0; i < digest.length; i++) {
				sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END

	public static String asHex(byte buf[]) {
		StringBuffer strbuf = new StringBuffer(buf.length * 2);

		for (int i = 0; i < buf.length; i++) {
			if (((int) buf[i] & 0xff) < 0x10)
				strbuf.append("0");
			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}
		return strbuf.toString();
	}

	public static StringBuffer uncompress(byte[] buf, String sSTX, String sETX)
			throws java.io.IOException, java.util.zip.DataFormatException {
		int nCnt = 0;
		int nBeginCon = 0;
		// int nBeginTag = -1;
		// int nIndex = -1;
		int nLen = 0;
		int nVal = -1;
		boolean bCompress = false;
		boolean bUnCompressStart = false;
		boolean bUnCompressEnd = false;
		boolean bUnCompressAdded = false;
		String sTmp = "";
		// boolean bDone = false;
		// byte[] buf;
		byte[] buf2;
		byte[] buf3;
		StringBuffer strBuf = new StringBuffer();
		java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();

		buf2 = buf;

		for (int i = 0; i < buf2.length; i++) {
			nVal = (int) (buf2[i] & 0xff);

			if (m_arrBT[0] == nVal) {
				if (i + 1 < buf2.length && m_arrBT[nCnt + 1] == (int) (buf2[i + 1] & 0xff)) {
					if (!bCompress) {
						buf3 = new byte[i];
						for (int j = 0; j < buf3.length; j++) {
							buf3[j] = buf2[j];
						}
						strBuf.append(convertUtfData(buf3, sSTX, sETX));
					}

					sTmp = "";
					bCompress = true;
					i++;
					nCnt += 2;
				}
			} else if (m_arrBT[nCnt] == -1) { // content length
				if (m_arrBT[nCnt + 1] == nVal) { // end content length
					sTmp = sTmp.trim();
					sTmp = decompress(sTmp);
					sTmp = addDecimal(sTmp);

					nLen = Integer.valueOf(sTmp).intValue();
					nBeginCon = i + 1;

					i += nLen;
					if (i >= buf2.length) {
						strBuf.delete(0, strBuf.length());
						break;
					}
					nCnt += 2;
				} else {
					sTmp += (char) (nVal) + "";
				}
			} else if (m_arrBT[nCnt] == -2) { // content
				if (m_arrBT[nCnt + 1] == nVal) { // end content
					buf3 = uncompressByte(buf2, nBeginCon, nLen);
					strBuf.append(convertUtfData(buf3, sSTX, sETX));
				} else {
					for (int j = 0; j < buf2.length; j++) {
						if (j == i) {
						}
					}
				}
				nCnt = 0;
			}
		}
		return strBuf;
	}

	public static String addDecimal(String s) {
		int nDotPos;
		String sBegin, sEnd;
		String sTmp = s.trim();

		if (sTmp.compareTo("") == 0)
			return sTmp;

		try {
			// nDotPos = (int)s.charAt(s.length()-1);
			nDotPos = Integer.parseInt(sTmp.substring(sTmp.length() - 1));
			sTmp = sTmp.substring(0, sTmp.length() - 1);
			if (nDotPos == 0) {
				// check is infront is 0
				// for input 00, return as 0
				if (sTmp.length() > 1)
					while (sTmp.indexOf('0') == 0)
						sTmp = sTmp.substring(1, sTmp.length());

				return sTmp;
			}
			while (sTmp.length() <= nDotPos)
				sTmp = "0" + sTmp;

			sBegin = sTmp.substring(0, sTmp.length() - nDotPos);

			sEnd = sTmp.substring(sTmp.length() - nDotPos);
			sTmp = sBegin + "." + sEnd;
			while (sTmp.indexOf('.') > 1) {
				if (sTmp.indexOf('0') == 0)
					sTmp = sTmp.substring(1, sTmp.length());
				else
					break;
			}

			return sTmp;
		} catch (StringIndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "0";
	}

	public static String decompress(String s) {
		boolean isNegative = false;
		int i;
		int iTmpByte;
		char cChar;
		String sResult = "";
		for (i = 0; i < s.length(); i++) {
			iTmpByte = ~((byte) s.charAt(i));
			cChar = (char) (((iTmpByte >> 4) & 0xf) + 48);

			if (!Character.isDigit(cChar) && (byte) cChar == 58) {
				isNegative = true;
				cChar = ' ';
			} else if (!Character.isDigit(cChar) && !((byte) cChar == 58)) {
				cChar = ' ';
				sResult += String.valueOf(cChar);
			} else
				sResult += String.valueOf(cChar);

			cChar = (char) ((iTmpByte & 0x0F) + 48);
			if (!Character.isDigit(cChar) && (byte) cChar == 58) {
				isNegative = true;
				cChar = ' ';
			} else if (!Character.isDigit(cChar) && !((byte) cChar == 58)) {
				cChar = ' ';
				sResult += String.valueOf(cChar);
			} else
				sResult += String.valueOf(cChar);

		}
		if (isNegative)
			return ("-" + sResult);
		else
			return sResult;
	}

	public static byte[] uncompressByte(byte[] b, int off, int len)
			throws java.io.IOException, java.util.zip.DataFormatException {

		java.util.zip.Inflater in = new java.util.zip.Inflater();
		in.setInput(b, off, len);

		// Create an expandable byte array to hold the decompressed data
		java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();

		// Decompress the data
		byte[] buf2 = new byte[1024];
		while (!in.finished()) {
			int count = in.inflate(buf2);
			bos.write(buf2, 0, count);
		}
		bos.close();
		in.end();

		// Get the decompressed data
		b = bos.toByteArray();

		return b;
	}

	public static String convertUtfData(byte[] abyData, String sSTX, String sETX) {
		int iIndex = 0;
		int iChar;
		int nPos = 0;
		int[] arrnUTFPos = null;
		// char cSep;
		boolean bConvertUTF = false;
		boolean bCheckIType = false;
		boolean bIsNewLine = false;
		// String sResult = "";
		StringBuffer sResult = new StringBuffer();
		byte[] abyTemp = null;
		abyTemp = new byte[4096];

		if (sETX.equals("a") && sSTX.length() > 0) {
			java.util.StringTokenizer token = new java.util.StringTokenizer(sSTX, ",");
			arrnUTFPos = new int[token.countTokens()];

			for (int i = 0; i < arrnUTFPos.length; i++) {
				try {
					arrnUTFPos[i] = Integer.valueOf(token.nextToken()).intValue();
				} catch (NumberFormatException e) {
					arrnUTFPos[i] = -1;
				}
			}

			sSTX = "";
			sETX = "2c";
		}

		try {
			for (int i = 0; i < abyData.length; i++) {
				iChar = (int) (abyData[i] & 0xff);

				if (iChar == 0)
					continue;

				if (bConvertUTF) {
					if (Integer.toHexString(iChar).compareTo("1e") == 0
							|| Integer.toHexString(iChar).compareTo(sETX) == 0) {
						try {
							sResult.append(new String(abyTemp, 0, iIndex, "UTF8"));
							sResult.append(String.valueOf(((char) iChar)));
							bConvertUTF = false;
							iIndex = 0;
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					} else {
						abyTemp[iIndex] = abyData[i];
						iIndex++;
					}
				} else {
					// sResult += String.valueOf(((char)iChar));
					sResult.append(String.valueOf(((char) iChar)));
				}
				if (bCheckIType) {
					// if(iChar==130/* || (arrnUTFPos != null &&
					// arrnUTFPos[nPos] == 130)*/){
					if (isUTFFieldID(iChar)) {
						bConvertUTF = true;
					}
					bCheckIType = false;
				}

				if (Integer.toHexString(iChar).compareTo(sSTX) == 0
						|| Integer.toHexString(iChar).compareTo("1e") == 0) {
					bCheckIType = true;
				}

				if (arrnUTFPos != null) {
					if (iChar == 13) {
						bIsNewLine = true;
					} else if (iChar == 10 && bIsNewLine) {
						bIsNewLine = false;
						nPos = 0;
					} else if (Integer.toHexString(iChar).compareTo(sETX) == 0) {
						nPos++;
					}

					// if (nPos < arrnUTFPos.length && arrnUTFPos[nPos] == 130){
					if (nPos < arrnUTFPos.length && isUTFFieldID(arrnUTFPos[nPos])) {
						bConvertUTF = true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sResult.toString();
	}

	public static boolean isUTFFieldID(int vnFID) {
		boolean bIsUTF = false;

		for (int i = 0; i < m_arrUTFFID.length; i++) {
			if (vnFID == m_arrUTFFID[i]) {
				bIsUTF = true;
				break;
			}
		}

		return bIsUTF;
	}

	public static String formatPrice(float number) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatPrice(number, null);
	}

	public static String formatPrice(float number, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null)
			return strRepZero;

		if (number < 1000) {
			DecimalFormat formatter = new DecimalFormat("###,##0.000");
			return formatter.format(number);
		} else {
			DecimalFormat formatter = new DecimalFormat("###,##0.00");
			return formatter.format(number);
		}
	}

	// Added by Thinzar@20141112, Change_Request-20141101, ReqNo. 8 - START
	/*
	 * formatPrice : returns 3 decimal if <1000, 2 decimal if >1000 formatPrice2
	 * : returns no decimal, specifically for JK and JKD formatPrice4 : returns
	 * 4 decimal places for PH and PHD
	 */
	public static String formatPrice(float number, @Nullable String strRepZero, String exchangeCode) {
		if (exchangeCode.equalsIgnoreCase("JK") || exchangeCode.equalsIgnoreCase("JKD")) {

			return formatPrice2(number, strRepZero);
		} else if (exchangeCode.equalsIgnoreCase("PH") || exchangeCode.equalsIgnoreCase("PHD")) {

			return formatPrice4(number, strRepZero);
		} else {

			return formatPrice(number, strRepZero);
		}
	}

	public static String formatDoubleByExchange(double number, @Nullable String strRepZero, String exchangeCode) {
		String strFormat = null;

		if (number == 0 && strRepZero != null)
			return strRepZero;
		else {
			if (exchangeCode.equalsIgnoreCase("JK") || exchangeCode.equalsIgnoreCase("JKD")) {
				strFormat = "###,##0";
			} else if (exchangeCode.equalsIgnoreCase("PH") || exchangeCode.equalsIgnoreCase("PHD")) {
				if (number > 0 && number < 1000000)
					strFormat = "###,##0.0000";
				else if (number >= 1000000)
					strFormat = "###,##0.00";
				else
					strFormat = "###,##0.0000";
			} else {
				strFormat = "###,##0.00";
			}

			DecimalFormat formatter = new DecimalFormat(strFormat);

			if (number >= 1000000) {
				double formatFigure = number / 1000000;
				String value = formatter.format(formatFigure);
				return value + "M";
			} else {
				return formatter.format(number);
			}

		}
	}
	// Added by Thinzar@20141112, Change_Request-20141101, ReqNo. 8 - END

	public static String formatPrice2(float number) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatPrice2(number, null);
	}

	public static String formatPrice2(float number, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null)
			return strRepZero;

		DecimalFormat formatter = new DecimalFormat("###,##0");
		return formatter.format(number);
	}

	public static String formatPrice3(float number) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatPrice3(number, null);
	}

	public static String formatPrice3(float number, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null)
			return strRepZero;

		DecimalFormat formatter = new DecimalFormat("###,###.###");
		return formatter.format(number);

	}

	public static String formatPrice4(float number) {
		return formatPrice4(number, null);
	}

	public static String formatPrice4(float number, @Nullable String strRepZero) {

		if (number == 0 && strRepZero != null)
			return strRepZero;

		DecimalFormat formatter = new DecimalFormat("###,##0.0000");
		return formatter.format(number);

	}

	public static String formatPriceChange(float number) {
		DecimalFormat formatter = new DecimalFormat("###,##0.000");
		if (number >= 0) {
			String value = formatter.format(number);
			if (number != 0) {
				return "+" + value;
			} else {
				return value;
			}
		} else {
			float positive = Math.abs(number);
			String value = formatter.format(positive);
			return "-" + value;
		}
	}

	public static String formatPriceChange2(float number) {
		DecimalFormat formatter = new DecimalFormat("###,##0");
		if (number >= 0) {
			String value = formatter.format(number);
			if (number != 0) {
				return "+" + value;
			} else {
				return value;
			}
		} else {
			float positive = Math.abs(number);
			String value = formatter.format(positive);
			return "-" + value;
		}
	}

	public static String formatPriceChange(float number, String format) {
		DecimalFormat formatter = new DecimalFormat(format);
		if (number >= 0) {

			String value = formatter.format(number);
			if (number != 0) {
				return "+" + value;
			} else {
				return value;
			}
		} else {
			float positive = Math.abs(number);
			String value = formatter.format(positive);
			return "-" + value;
		}
	}

	// default 3 decimal
	public static String formatPriceChangeByExchange(float number, String exchangeCode) {
		String format;
		if (exchangeCode.equalsIgnoreCase("JK") || exchangeCode.equalsIgnoreCase("JKD")) {
			format = "###,##0";
		} else if (exchangeCode.equalsIgnoreCase("PH") || exchangeCode.equalsIgnoreCase("PHD")) {
			format = "###,##0.0000";
		} else {
			format = "###,##0.000"; // Edited 20150422, Fixes_Request-20150401,
									// ReqNo. 8
		}

		DecimalFormat formatter = new DecimalFormat(format);
		if (number >= 0) {

			String value = formatter.format(number);
			if (number != 0) {
				return "+" + value;
			} else {
				return value;
			}
		} else {
			float positive = Math.abs(number);
			String value = formatter.format(positive);
			return "-" + value;
		}
	}

	// default 2 decimal
	public static String formatIndicesChangeByExchange(float number, String exchangeCode) {
		String format;
		if (exchangeCode.equalsIgnoreCase("JK") || exchangeCode.equalsIgnoreCase("JKD")) {
			format = "###,##0";
		} else if (exchangeCode.equalsIgnoreCase("PH") || exchangeCode.equalsIgnoreCase("PHD")) {
			format = "###,##0.0000";
		} else {
			format = "###,##0.00";
		}

		DecimalFormat formatter = new DecimalFormat(format);
		if (number >= 0) {

			String value = formatter.format(number);
			if (number != 0) {
				return "+" + value;
			} else {
				return value;
			}
		} else {
			float positive = Math.abs(number);
			String value = formatter.format(positive);
			return "-" + value;
		}
	}

	public static String formatPricePercent(float number) {
		number *= 100;
		DecimalFormat formatter = new DecimalFormat("##0.00");
		if (number >= 0) {
			String value = formatter.format(number);
			if (number != 0) {
				return "+" + value + "%";
			} else {
				return value + "%";
			}
		} else {
			float positive = Math.abs(number);
			String value = formatter.format(positive);
			return "-" + value + "%";
		}
	}

	public static String formatInteger(int number) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatInteger(number, null);
	}

	public static String formatInteger(int number, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null) {
			return strRepZero;
		}
		DecimalFormat formatter = new DecimalFormat("###,###");
		return formatter.format(number);
	}

	public static String formatFloat(float number) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatFloat(number, null);
	}

	public static String formatFloat(float number, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null) {
			return strRepZero;
		}
		DecimalFormat formatter = new DecimalFormat("###,###0.00");
		return formatter.format(number);
	}

	public static String formatFloat2(float number) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatFloat2(number, null);
	}

	public static String formatFloat2(float number, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null) {
			return strRepZero;
		}
		DecimalFormat formatter = new DecimalFormat("###,###0");
		return formatter.format(number);
	}

	public static String formatLong(long number) {
		if (AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK")
				| AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD")) {
			return formatLong(number, false);
		} else {
			return formatLong(number, true);
		}
	}

	// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
	public static String formatLong(long number, @Nullable String strRepZero) {
		if (AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK")
				|| AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD")) {
			return formatLong(number, false, strRepZero);
		} else {
			return formatLong(number, true, strRepZero);
		}
	}
	// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END

	public static String formatLong(long number, boolean decimal) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatLong(number, decimal, null);
	}

	public static String formatLong(long number, boolean decimal, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null) {
			return strRepZero;
		}

		if (number >= 1000000) {
			DecimalFormat formatter = null;
			if (decimal) {
				formatter = new DecimalFormat("###,###.##");
			} else {
				formatter = new DecimalFormat("###,###");
			}
			double convert = Long.valueOf(number).doubleValue();
			double formatFigure = convert / 1000000;
			String value = formatter.format(formatFigure);
			return value + "M";
		} else {
			DecimalFormat formatter = new DecimalFormat("###,###");
			return formatter.format(number);
		}
	}

	// Thinzar@20160315, return comma separated long value withoug million
	// rounding for quantity
	public static String formatQty(long number) {
		DecimalFormat formatter = new DecimalFormat("###,###");

		return formatter.format(number);
	}

	// Added by Mary@20130627 - Fixes_Request-20130523, ReqNo.19 - START
	public static String formatDoubleFull(double number, @Nullable String strRepZero) {
		if (number == 0 && strRepZero != null) {
			return strRepZero;
		}

		return new DecimalFormat("###,###,###,##0.00").format(number);
	}
	// Added by Mary@20130627 - Fixes_Request-20130523, ReqNo.19 - END

	public static String formatDouble(double number) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatDouble(number, null);
	}

	public static String formatDouble(double number, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null) {
			return strRepZero;
		}

		DecimalFormat formatter = new DecimalFormat("###,##0.00");

		if (number >= 1000000) {
			double formatFigure = number / 1000000;
			String value = formatter.format(formatFigure);
			return value + "M";
		} else {
			return formatter.format(number);
		}
	}

	public static String formatDouble3(double number) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatDouble3(number, null);
	}

	public static String formatDouble3(double number, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null) {
			return strRepZero;
		}
		/*
		 * Mary@20130102 - Fixes_Request-20121102, ReqNo.11 DecimalFormat
		 * formatter = new DecimalFormat("###,###.###");
		 */
		DecimalFormat formatter = new DecimalFormat("###,##0.00#");

		if (number >= 1000000) {
			double formatFigure = number / 1000000;
			String value = formatter.format(formatFigure);
			return value + "M";
		} else {
			return formatter.format(number);
		}

	}

	public static String formatDouble2(double number) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - START
		return formatDouble2(number, null);
	}

	public static String formatDouble2(double number, @Nullable String strRepZero) {
		// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo.15 - END
		/*
		 * Mary@20121001 - Fixes_Request-20120815, ReqNo.15 if (number == 0) {
		 * return "-"; }
		 */
		if (number == 0 && strRepZero != null) {
			return strRepZero;
		}
		DecimalFormat formatter = new DecimalFormat("###,##0");

		if (number >= 1000000) {
			double formatFigure = number / 1000000;
			String value = formatter.format(formatFigure);
			return value + "M";
		} else {
			return formatter.format(number);
		}

	}

	// Added by Mary@20130117 - Enhance Maintainability - START
	public static char convertAsciiToChar(int intAsciiValue) {
		return (char) intAsciiValue;
	}
	// Added by Mary@20130117 - Enhance Maintainability - END

	// Added by Mary@20131008 - Change_Request-20130711, ReqNo.14 - START
	public static String encryptWithE2EE(E2EEInfo e2eeInfo, String strPassword) {
		String strE2EEEncryptedPwd = null;
		try {
			SncE2EEClient e2eeClient = new SncE2EEClient();
			strE2EEEncryptedPwd = e2eeClient.encryptPIN1(e2eeInfo.getStrE2EEPublicKey(),
					e2eeInfo.getStrE2EERandomNumber(), strPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return strE2EEEncryptedPwd;
	}
	// Added by Mary@20131008 - Change_Request-20130711, ReqNo.14 - END

	// Added by Sonia@20131002 - Change_Request-20130225, ReqNo.5 - START
	public static String formatPercent(double number) {
		DecimalFormat formatter = new DecimalFormat("##0.00");
		String value = null;
		if (number >= 0) {
			if (number == 0.0 || number == 100.0) {
				value = String.valueOf(Math.round(number));
			} else {
				value = formatter.format(number);
			}
		}
		return value;
	}
	// Added by Sonia@20131002 - Change_Request-20130225, ReqNo.5 - END

	public static String formatFloatToString(float floatValue) {
		DecimalFormat dec = new DecimalFormat("###,###,##0.00");

		return dec.format(floatValue);
	}

	public static RSAPrivateKey loadPrivateKey(String Key) throws Exception {
		Base64 based64 = new Base64();

		try {
			byte[] buffer = based64.decode(Key);
			String hex = asHex(buffer);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
			return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
		} catch (NoSuchAlgorithmException e) {
			/*
			 * Mary@20130705 - public key error throw new Exception(
			 * "no such algorithm");
			 */
			throw new NoSuchAlgorithmException(ErrorCodeConstants.NO_SUCH_ALGORITHM);
		} catch (InvalidKeySpecException e) {
			/*
			 * Mary@20130705 - public key error throw new Exception(
			 * "Invalid public key");
			 */
			throw new InvalidKeySpecException(ErrorCodeConstants.INVALID_KEY_SPEC_EXCEPTION);
		}
	}

	public PublicKey getPublicKey(String pubKeyStr) {
		// String pubKeyStr = SP.getString("PublicKey", "");
		byte[] sigBytes = Base64.decode(pubKeyStr);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(sigBytes);
		KeyFactory keyFact = null;
		try {
			keyFact = KeyFactory.getInstance("RSA", "BC");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		try {
			return keyFact.generatePublic(x509KeySpec);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return null;
	}

	public PrivateKey getPrivateKey(String privKeyStr) {
		// String privKeyStr = SP.getString("PrivateKey", "");
		byte[] sigBytes = Base64.decode(privKeyStr);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(sigBytes);
		KeyFactory keyFact = null;
		try {
			keyFact = KeyFactory.getInstance("RSA", "BC");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		try {
			return keyFact.generatePrivate(x509KeySpec);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String[] generateKeys() {
		String[] keys = new String[2];
		try {

			KeyPairGenerator generator;
			try {
				generator = KeyPairGenerator.getInstance("RSA", "BC");
				generator.initialize(256, new SecureRandom());
				KeyPair pair = generator.generateKeyPair();
				PublicKey pubKey = pair.getPublic();
				PrivateKey privKey = pair.getPrivate();
				byte[] publicKeyBytes = pubKey.getEncoded();
				String pubKeyStr = new String(Base64.encode(publicKeyBytes));
				byte[] privKeyBytes = privKey.getEncoded();
				String privKeyStr = new String(Base64.encode(privKeyBytes));

				keys[0] = pubKeyStr;
				keys[1] = privKeyStr;

			} catch (NoSuchProviderException e) {
				e.printStackTrace();
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return keys;
	}
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.5
	public static String formatLastUpdateDate(String lastUpdateDate){
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date	=null;
		SimpleDateFormat df2	=null;
		
		try {
			date = df.parse(lastUpdateDate);
			df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return df2.format(date).toString();
	}
	
	public static String customFormatDate(String strDate, String originalFormat, String newFormat){
		DateFormat df = new SimpleDateFormat(originalFormat);
		Date date	=null;
		SimpleDateFormat df2	=null;
		
		try {
			date = df.parse(strDate);
			df2 = new SimpleDateFormat(newFormat);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return df2.format(date).toString();
	}
	
	public static Date parseStringToDate(String dateString, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);

		try {
			Date date = dateFormat.parse(dateString);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String parseDateToString(Date formattedDate, String format){
		
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(formattedDate);
	}

	public static String maskString(String originalString, int lengthToUnmask){
	 	if(originalString.length() >= lengthToUnmask) {

			String subStrToMask = originalString.substring(0, originalString.length() - lengthToUnmask);
			String maskedSubString = StringUtils.repeat("x", subStrToMask.length());
			String maskedString = originalString.replace(subStrToMask, maskedSubString);

			return maskedString;
		}else{
			return originalString;
		}
	}

	public static String parseSymbolCodeToRealTimeFeed(String feedSymbolCode){
		String symbolCodeRT = feedSymbolCode;

		return ( symbolCodeRT.length() > 1 && symbolCodeRT.toUpperCase().endsWith("D") ) ? symbolCodeRT.substring(0, symbolCodeRT.length() - 1) : symbolCodeRT;
	}
}