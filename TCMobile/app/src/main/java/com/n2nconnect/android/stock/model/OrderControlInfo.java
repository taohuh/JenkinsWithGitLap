package com.n2nconnect.android.stock.model;

import java.util.ArrayList;
import java.util.List;

public class OrderControlInfo {
	
	private String strOrderType;
	private List<String> lstAvailableValidity;
	private List<String> lstValidityThatEnableDisclosedQtyField;
	private List<String> lstValidityThatEnableMinQtyField;
	private boolean isEnablePriceField;
	private boolean isEnableQuantityField;
	private boolean isEnableGTDDateField;
	private boolean isEnableStopLimitPriceField;
	private boolean isEnableMinQtyField;
	private boolean isEnableDisclosedQtyField;
	
	//Added By Thinzar@20140814 - Change_Request-20140801, ReqNo.4
	private boolean isEnableTriggerType			= false;
	private boolean isEnableTriggerDirection	= false;
	
	public OrderControlInfo(){
		lstAvailableValidity					= new ArrayList<String>();
		lstValidityThatEnableDisclosedQtyField	= new ArrayList<String>();
		lstValidityThatEnableMinQtyField		= new ArrayList<String>();
	}
	
	public String getStrOrderType(){
		return this.strOrderType;
	}
	public void setStrOrderType(String strOrderType){
		this.strOrderType	= strOrderType;
	}

	public List<String> getLstAvailableValidity() {
		return lstAvailableValidity;
	}

	public void addStrAvailableValidity(String strAvailableValidity) {
		this.lstAvailableValidity.add(strAvailableValidity);
	}

	public List<String> getLstValidityThatEnableDisclosedQtyField() {
		return lstValidityThatEnableDisclosedQtyField;
	}

	public void addStrValidityThatEnableDisclosedQtyField(String strValidityThatEnableDisclosedQtyField) {
		this.lstValidityThatEnableDisclosedQtyField.add(strValidityThatEnableDisclosedQtyField);
	}
	
	public List<String> getLstValidityThatEnableMinQtyField() {
		return lstValidityThatEnableMinQtyField;
	}

	public void addStrValidityThatEnableMinQtyField(String strValidityThatEnableMinQtyField) {
		this.lstValidityThatEnableMinQtyField.add(strValidityThatEnableMinQtyField);
	}

	public boolean isEnablePriceField() {
		return isEnablePriceField;
	}

	public void setEnablePriceField(boolean isEnablePriceField) {
		this.isEnablePriceField = isEnablePriceField;
	}

	public boolean isEnableQuantityField() {
		return isEnableQuantityField;
	}

	public void setEnableQuantityField(boolean isEnableQuantityField) {
		this.isEnableQuantityField = isEnableQuantityField;
	}

	public boolean isEnableGTDDateField() {
		return isEnableGTDDateField;
	}

	public void setEnableGTDDateField(boolean isEnableGTDDateField) {
		this.isEnableGTDDateField = isEnableGTDDateField;
	}

	public boolean isEnableStopLimitPriceField() {
		return isEnableStopLimitPriceField;
	}

	public void setEnableStopLimitPriceField(boolean isEnableStopLimitPriceField) {
		this.isEnableStopLimitPriceField = isEnableStopLimitPriceField;
	}

	public boolean isEnableMinQtyField() {
		return isEnableMinQtyField;
	}

	public void setEnableMinQtyField(boolean isEnableMinQtyField) {
		this.isEnableMinQtyField = isEnableMinQtyField;
	}

	public boolean isEnableDisclosedQtyField() {
		return isEnableDisclosedQtyField;
	}

	public void setEnableDisclosedQtyField(boolean isEnableDisclosedQtyField) {
		this.isEnableDisclosedQtyField = isEnableDisclosedQtyField;
	}

	//Added by Thinzar@20140814 - Change_Request-20140801, ReqNo.4 - START
	public boolean isEnableTriggerType() {
		return isEnableTriggerType;
	}

	public void setEnableTriggerType(boolean isEnableTriggerType) {
		this.isEnableTriggerType = isEnableTriggerType;
	}

	public boolean isEnableTriggerDirection() {
		return isEnableTriggerDirection;
	}

	public void setEnableTriggerDirection(boolean isEnableTriggerDirection) {
		this.isEnableTriggerDirection = isEnableTriggerDirection;
	}
	//Change_Request-20140801, ReqNo.4 - END
}
