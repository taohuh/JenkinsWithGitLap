package com.n2nconnect.android.stock.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.zip.DataFormatException;

import javax.crypto.Cipher;

import org.apache.http.util.EncodingUtils;

import com.n2nconnect.android.stock.AES256Encryption;
import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.CustomizedBase64;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.InvalidPasswordException;
import com.n2nconnect.android.stock.ParamConstants;
//import com.sun.istack.internal.NotNull;
import android.support.annotation.NonNull;

import android.os.Build;
import android.util.Log;

public class AtpConnectUtil {
	/* Mary@20130515 - Fixes_Request-20130424, ReqNo.9
	private static char startText = (char)2;
	private static char endText = (char)3;
	*/
	private final static char startText	= (char)2;
	private final static char endText	= (char)3;
	
	//Added by Thinzar, to support both http and https, Change_Request-20160101, ReqNo.11 - START
	public static String getAtpProtocol(){
		if(AppConstants.brokerConfigBean.isATPServerSSL() && AppConstants.isAppVersionSupportSSL)
			return "https";
		else 
			return "http";
	}
	
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
	public static String tradeGetE2EEParams() throws Exception{
		URL url			= null;
		byte[] bytes	= null;
		String response	= null;
		try {
			/* Mary@20130704 - Fixes_Request-20130523, ReqNo.28
			url			= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetE2EEParams");
			*/
			url			= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetE2EEParams?" +  FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S") );
			DefinitionConstants.Debug("[AtpConnectUtil][tradeGetE2EEParams]Url address: " + url.toString() );
			bytes		= readUrlData(url);
			response	= new String(bytes, AppConstants.STRING_ENCODING_FORMAT);
			DefinitionConstants.Debug("[AtpConnectUtil][tradeGetE2EEParams]response: " + response );
			
			if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) ){
				throw new Exception(response);
			}else if( ErrorCodeConstants.isBackendExceptionMsg(response) ){
				if( response.contains("500") )
					throw new IOException(response);
				else
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			}
			
			return response;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
			throw ioe;
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			bytes		= null;
			url			= null;
		}
		return null;
	}
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
	
	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	public static String tradeGetSession() throws FailedAuthenicationException {
	*/
	public static String tradeGetSession() throws Exception {
		URL url				= null;
		byte[] bytes		= null;
		String userParamKey	= null;
		try {
			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/TradeGetSession");
			/* Mary@20130704 - Fixes_Request-20130523, ReqNo.28
			url			= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetSession");
			*/
			url			= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetSession?" +  FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S") );
			DefinitionConstants.Debug("[AtpConnectUtil][tradeGetSession]Url address: " + url.toString() );
			bytes		= readUrlData(url);
			userParamKey= SystemUtil.byteToHex(bytes, true);
			DefinitionConstants.Debug("[AtpConnectUtil][tradeGetSession]Server response: "+ userParamKey);
			
			if( ErrorCodeConstants.isATPBackendResponseErrMsg(userParamKey) )
				throw new Exception(userParamKey);
			else if( ErrorCodeConstants.isBackendExceptionMsg(userParamKey) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			
			return userParamKey;
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
		}
		return null;
		 */
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			bytes		= null;
			url			= null;
		}
		return null;
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static byte[] tradeGetSession2(String[] AESKey) throws FailedAuthenicationException {
		byte[] mKey = null;

		try {

			String stx = Character.toString(startText);
			String etx = Character.toString(endText);

			mKey = AES256Encryption.HashSHA256Generator(AESKey[0]);

			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/TradeGetSession2?"+AESKey[1]);
			URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/TradeGetSession2?"+AESKey[1] );
			DefinitionConstants.Debug("[AtpConnectUtil][tradeGetSession2]Url address:" + url.toString() );

			byte[] bytes = readUrlData(url);

			String response;
			try {
				response = new String(bytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String userParamKey=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				byte[] arr = null;

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes());

					arr = AES256Encryption.deencryptData(decData, mKey);

					return arr;
				}

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return null;
	}
	*/
	public static byte[] tradeGetSession2(String[] AESKey) throws Exception {
		URL url 		= null;
		byte[] bytes	= null;
		String response	= null; 
		byte[] mKey 	= null;

		try {
			String stx 			= Character.toString(startText);
			String etx 			= Character.toString(endText);
			mKey 				= AES256Encryption.HashSHA256Generator(AESKey[0]);

			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/TradeGetSession2?"+AESKey[1]);
			url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetSession2?"+ URLEncoder.encode(AESKey[1]));	//Edited 20160905, URLEncode based on ATP team's suggestion
			DefinitionConstants.Debug("[AtpConnectUtil][tradeGetSession2]Url address:" + url.toString() );
			bytes				= readUrlData(url);
			response			= new String(bytes, AppConstants.STRING_ENCODING_FORMAT);
			
			if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			
			String BeginData	= "10|"+stx;
			String EndData 		= etx+"E";
			int startIdx 		= -1;
			int endIdx 			= -1;

			try{
				startIdx	= response.indexOf(BeginData) + BeginData.length();
				endIdx 		= response.indexOf(EndData);
			}catch (Exception e){
				e.printStackTrace();
			}

			if(startIdx != -1 && endIdx != -1){
				String sSubData	= response.substring(startIdx, endIdx);
				byte[] decData	= CustomizedBase64.decode( sSubData.getBytes() );

				return AES256Encryption.deencryptData(decData, mKey);
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			response= null;
			bytes	= null;
			url 	= null;
			mKey 	= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String UpdateClientTermsAndCondition(Map parameters) throws FailedAuthenicationException{

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}     
		String userName = (String) parameters.get(ParamConstants.USERNAME_TAG);
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);

		try{

			URL url = null;

			if(AppConstants.EnableEncryption==false){
				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/["+userParam+"]InsCliTCInfo?+="+senderCode+"|,="+userName);
				url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT,  "/["+userParam+"]InsCliTCInfo?+="+senderCode+"|,="+userName);
				DefinitionConstants.Debug("[AtpConnectUtil][UpdateClientTermsAndCondition]Url address: "+ url.toString() );

				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Response server: " + response);
			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "+="+senderCode+"|,="+userName;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/["+userParam+"]InsCliTCInfo?"+encodedURL);
				url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT,  "/["+userParam+"]InsCliTCInfo?"+encodedURL);
				DefinitionConstants.Debug( "[AtpConnectUtil][UpdateClientTermsAndCondition]Url address: " + url.toString() );

				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		}catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	*/
	public static String UpdateClientTermsAndCondition(Map<String, Object> parameters) throws Exception{
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		
		String userParam		= null;
		byte[] userParamInByte 	= null;
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String userName		= (String) parameters.get(ParamConstants.USERNAME_TAG);
		String senderCode	= (String) parameters.get(ParamConstants.SENDERCODE_TAG);

		try{
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/["+userParam+"]InsCliTCInfo?+="+senderCode+"|,="+userName);
				url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(),  "/["+userParam+"]InsCliTCInfo?+="+senderCode+"|,="+userName);
				DefinitionConstants.Debug("[AtpConnectUtil][UpdateClientTermsAndCondition]Url address: "+ url.toString() );
				dataBytes	= readUrlData(url);
				response	= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][UpdateClientTermsAndCondition]Response server : " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String base64EncodedUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey 					= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam 				= "+=" + senderCode + "|,=" + userName;
				byte[] EData					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|" + stx + base64EncodedUserParam + "|" + encryptedInputParam + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd, AppConstants.STRING_ENCODING_FORMAT);

				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/["+userParam+"]InsCliTCInfo?"+encodedURL);
				//url 							= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(),  "/["+userParam+"]InsCliTCInfo?"+encodedURL);
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(),  "/[0]InsCliTCInfo?"+encodedURL);	//Edited by Thinzar@20140923, Fixes_Request-20140820,ReqNo.31
				DefinitionConstants.Debug( "[AtpConnectUtil][UpdateClientTermsAndCondition]Url address: " + url.toString() );
				dataBytes						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug( "[AtpConnectUtil][UpdateClientTermsAndCondition]response: " + response );

				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";

				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				}catch (Exception e){
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData	= response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}				
				DefinitionConstants.Debug("[AtpConnectUtil][UpdateClientTermsAndCondition]Server decrypted response: " + decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte = null;
		}

		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String CheckSingleLogon(Map parameters) throws FailedAuthenicationException{

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}     
		String userName = (String) parameters.get(ParamConstants.USERNAME_TAG);

		try{

			URL url = null;

			if(AppConstants.EnableEncryption==false){

				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/["+userParam+"]CheckSingleLogon?+="+userName);
				url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/["+userParam+"]CheckSingleLogon?+="+userName);
				DefinitionConstants.Debug( "[AtpConnectUtil][CheckSingleLogon]Url address: " + url.toString() );

				byte[] bytes = readUrlData(url);

				DefinitionConstants.Debug("Server Response: "+new String(bytes));

				return new String(bytes);
			}else{

				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "+="+userName;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData;

				EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]CheckSingleLogon?"+encodedURL);
				url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[0]CheckSingleLogon?"+encodedURL);
				DefinitionConstants.Debug( "[AtpConnectUtil][CheckSingleLogon]Url address: " + url.toString() );

				
				DefinitionConstants.Debug( "[AtpConnectUtil][CheckSingleLogon]Url address: " + url.toString() );

				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		}catch (MalformedURLException e) {
			e.printStackTrace();
		}catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}
		return null;
	}
	*/
	public static String CheckSingleLogon(Map<String, Object> parameters) throws Exception{
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte	= null;

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
			
		String userName	= (String) parameters.get(ParamConstants.USERNAME_TAG);

		try{		
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/["+userParam+"]CheckSingleLogon?+="+userName);
				url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/["+userParam+"]CheckSingleLogon?+="+userName);
				DefinitionConstants.Debug( "[AtpConnectUtil][CheckSingleLogon]Url address: " + url.toString() );
				dataBytes	= readUrlData(url);
				response	= new String(dataBytes);
				DefinitionConstants.Debug("[AtpConnectUtil][CheckSingleLogon]Server Response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return response;
			}else{
				String InputParam 				= "+=" + userName;
				String encryptionKey			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String stx						= Character.toString(startText);
				String etx						= Character.toString(endText);
				byte[] EData					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncodedUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				String urlAdd 					= "10|" + stx+base64EncodedUserParam + "|" + encryptedInputParam + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd, AppConstants.STRING_ENCODING_FORMAT);

				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]CheckSingleLogon?"+encodedURL);
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]CheckSingleLogon?"+encodedURL);
				DefinitionConstants.Debug( "[AtpConnectUtil][CheckSingleLogon]Url address: " + url.toString() );

				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug( "[AtpConnectUtil][CheckSingleLogon]response: " + response );
				
				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData 				= "10|" + stx;
				String EndData					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try{
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx 	= response.indexOf(EndData);
				}catch (Exception e){
					e.printStackTrace();
				}

				if(startIdx != -1 && endIdx != -1){
					String sSubData	= response.substring(startIdx, endIdx);
					byte[] decData 	= null;

					try {
						decData = CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					byte[] arr		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString	= new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][CheckSingleLogon]Server response: " + decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				*/
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte = null;
		}

		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static void tradeAlive(Map parameters) throws FailedAuthenicationException{

		String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);  

		try{

			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://" + AppConstants.ATP_SERVER_IP +"/[" +userParam+"]TradeAlive");
			URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[" +userParam+"]TradeAlive");
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeAlive]Url address: " + url.toString() );
			
			byte[] bytes = readUrlData(url);

		}catch (MalformedURLException e) {
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}
	}
	*/
	public static boolean tradeAlive(Map<String, String> parameters) throws Exception{
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= parameters.get(ParamConstants.USERPARAM_TAG);  

		try{
			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://" + AppConstants.ATP_SERVER_IP +"/[" +userParam+"]TradeAlive");
			
			url			= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[" +userParam+"]TradeAlive");
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeAlive]Url address: " + url.toString() );
			
			dataBytes	= readUrlData(url);
			response	= new String(dataBytes);
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeAlive]response: " + response);
			
			if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			
		// NOTE : as this just work as heartbeat to server so no exception throw but boolean is return to have retry when heartbeat sending fail
			return true;
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			url				= null;
			dataBytes		= null;
			response		= null;
			userParam		= null;
		}
		
		return false;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static PublicKey tradeGetPK() throws FailedAuthenicationException {

		try{
			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://"+ AppConstants.ATP_SERVER_IP + "/TradeGetPK");
			URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/TradeGetPK");
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetPK]Url address: " + url.toString() );	

			byte[] bytes = readUrlData(url);

			PublicKey PubKey =  FormatUtil.loadPublicKey(new String(bytes));

			return PubKey;

		}catch (MalformedURLException e) {
			e.printStackTrace();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	*/
	public static PublicKey tradeGetPK() throws Exception {
		URL url				= null;
		byte[] dataBytes	= null;
		String response		= null;
		PublicKey PubKey	= null;
		try{
			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://"+ AppConstants.ATP_SERVER_IP + "/TradeGetPK");
			/* Mary@20130704 - Fixes_Request-20130523, ReqNo.28
			url 		= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetPK");
			*/
			url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetPK?" + FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S") );
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetPK]Url address: " + url.toString() );	
			dataBytes	= readUrlData(url);
			response	= new String(dataBytes);
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetPK]response: " + response);	
			
			if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			
			PubKey 	= FormatUtil.loadPublicKey(response);

			return PubKey;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		// Mary@20130705 - public key error - START
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (NoSuchAlgorithmException nsae) {
			throw nsae;
		} catch (InvalidKeySpecException ikse) {
			throw ikse;
		// Mary@20130705 - public key error - END
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
			response	= null;
		}

		return null;
	}
	
	/*
	 * Added by Thinzar, Change_Request-20150901, ReqNo.4
	 * This function will replace tradeGetPK() and tradeGetPK_LoadBalance()
	 */
	public static PublicKey tradeGetPK_LBSupport() throws Exception {
		URL url				= null;
		byte[] dataBytes	= null;
		String response		= null;
		PublicKey PubKey	= null;
		try{
			//Edited by Thinzar, Change_Request_20160101, ReqNo.13
			//url 		= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetPK?+=1|" + FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S") );
			String strURLEncoded	= URLEncoder.encode("+=1|" + FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S"), AppConstants.STRING_ENCODING_FORMAT);
			url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "TradeGetPK?" + strURLEncoded);
			
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetPK_LBSupport]Url address: " + url.toString() );	
			dataBytes	= readUrlData(url);
			response	= new String(dataBytes);
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetPK_LBSupport]response: " + response);	
			
			if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			
			Map<String, String> loadBalanceMap	= AtpMessageParser.parseTradeGetPKLaodBalance(response);
			String public_key_data		= loadBalanceMap.get(ParamConstants.PUBLIC_KEY_TAG);
			String public_ip_data		= loadBalanceMap.get(ParamConstants.PUBLIC_IP_TAG);
			String private_ip_data		= loadBalanceMap.get(ParamConstants.PRIVATE_IP_TAG);
			String secondary_port_data 	= null;
			if(loadBalanceMap.get(ParamConstants.SECONDARY_PORT_TAG) != null)
				secondary_port_data	= loadBalanceMap.get(ParamConstants.SECONDARY_PORT_TAG);
				
			byte[] decData				= CustomizedBase64.decode(public_ip_data.getBytes(AppConstants.STRING_ENCODING_FORMAT));
			String decrypted_public_ip	= new String(decData, AppConstants.STRING_ENCODING_FORMAT);
			
			//Added by Thinzar, sometimes ATP return empty public IP
			if(decrypted_public_ip == null || decrypted_public_ip.length() == 0){
				decrypted_public_ip		= AppConstants.brokerConfigBean.getStrAtpIP();
			}
			
			byte[] decData2				= CustomizedBase64.decode(private_ip_data.getBytes(AppConstants.STRING_ENCODING_FORMAT));
			String decrypted_private_ip	= new String(decData2, AppConstants.STRING_ENCODING_FORMAT);
				
			DefinitionConstants.Debug("decrypted public IP: " + decrypted_public_ip);
				
			if(secondary_port_data != null){
				byte[] decData3					= CustomizedBase64.decode(secondary_port_data.getBytes(AppConstants.STRING_ENCODING_FORMAT));
				String decrypted_secondary_port	= new String(decData3, AppConstants.STRING_ENCODING_FORMAT);
				AppConstants.brokerConfigBean.setIntAtpPort(Integer.parseInt(decrypted_secondary_port));
				DefinitionConstants.Debug("decrypted secondary port:" + decrypted_secondary_port);
			}
				
			AppConstants.brokerConfigBean.setStrAtpIP(decrypted_public_ip);
			PubKey	= FormatUtil.loadPublicKey(public_key_data);
			
			return PubKey;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		// Mary@20130705 - public key error - START
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (NoSuchAlgorithmException nsae) {
			throw nsae;
		} catch (InvalidKeySpecException ikse) {
			throw ikse;
		// Mary@20130705 - public key error - END
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
			response	= null;
		}

		return null;
	}
	
	//Added by Thinzar@20140917, Change_Request-20140901, ReqNo.6 - START
	public static PublicKey tradeGetPK_LoadBalance() throws Exception {
		
		URL url				= null;
		byte[] dataBytes	= null;
		String response		= null;
		PublicKey PubKey	= null;
		
		try{
			//Edited by Thinzar, Change_Request_20160101, ReqNo.13
			//url 		= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetPK?+=1|" + FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S") );
			String strURLEncoded	= URLEncoder.encode("+=1|" + FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S"), AppConstants.STRING_ENCODING_FORMAT);
			url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "TradeGetPK?" + strURLEncoded);
			
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetPK]Url address: " + url.toString() );	
			dataBytes	= readUrlData(url);
			response	= new String(dataBytes);
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetPK]response: " + response);	
			
			if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			
			Map<String, String> loadBalanceMap	= AtpMessageParser.parseTradeGetPKLaodBalance(response);
			String public_key_data		= loadBalanceMap.get(ParamConstants.PUBLIC_KEY_TAG);
			String public_ip_data		= loadBalanceMap.get(ParamConstants.PUBLIC_IP_TAG);
			String private_ip_data		= loadBalanceMap.get(ParamConstants.PRIVATE_IP_TAG);
			String secondary_port_data 	= null;
			if(loadBalanceMap.get(ParamConstants.SECONDARY_PORT_TAG) != null)
				secondary_port_data	= loadBalanceMap.get(ParamConstants.SECONDARY_PORT_TAG);
			
			byte[] decData				= CustomizedBase64.decode(public_ip_data.getBytes(AppConstants.STRING_ENCODING_FORMAT));
			String decrypted_public_ip	= new String(decData, AppConstants.STRING_ENCODING_FORMAT);
			
			byte[] decData2				= CustomizedBase64.decode(private_ip_data.getBytes(AppConstants.STRING_ENCODING_FORMAT));
			String decrypted_private_ip	= new String(decData2, AppConstants.STRING_ENCODING_FORMAT);

			DefinitionConstants.Debug("decrypted public IP: " + decrypted_public_ip);
			
			if(secondary_port_data != null){
				byte[] decData3					= CustomizedBase64.decode(secondary_port_data.getBytes(AppConstants.STRING_ENCODING_FORMAT));
				String decrypted_secondary_port	= new String(decData3, AppConstants.STRING_ENCODING_FORMAT);
				AppConstants.brokerConfigBean.setIntAtpPort(Integer.parseInt(decrypted_secondary_port));
				DefinitionConstants.Debug("decrypted secondary port:" + decrypted_secondary_port);
			}
			
			AppConstants.brokerConfigBean.setStrAtpIP(decrypted_public_ip);
			PubKey	= FormatUtil.loadPublicKey(public_key_data);
			
			return PubKey;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		// Mary@20130705 - public key error - START
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (NoSuchAlgorithmException nsae) {
			throw nsae;
		} catch (InvalidKeySpecException ikse) {
			throw ikse;
		// Mary@20130705 - public key error - END
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
			response	= null;
		}

		return null;
	}
	//Added by Thinzar@20140917, Change_Request-20140901, ReqNo.6 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradeGetKey() throws FailedAuthenicationException {

		try {
			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/TradeGetKey");
			URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/TradeGetKey");
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetKey]Url address: " + url.toString() );

			byte[] bytes = readUrlData(url);

			String Key = null;
			Key = N2NFeedUtil.decompress(bytes);

			String substrKey = null;

			substrKey = Key.substring(1, Key.length()-1);

			return substrKey;

		} catch (MalformedURLException e) {
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}

		return null;
	}
	*/
	public static String tradeGetKey() throws Exception {
		URL url				= null;
		byte[] dataBytes	= null;
		String response		= null;
		try {
			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/TradeGetKey");
			/* Mary@20130704 - Fixes_Request-20130523, ReqNo.28
			url 		= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetKey");
			*/
			url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetKey?" + FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S") );
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetKey]Url address: " + url.toString() );
			dataBytes	= readUrlData(url);
			response	= N2NFeedUtil.decompress(dataBytes);
			
			if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

			return response.substring(1, response.length()-1);
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
		}

		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String[] tradeGetKey2(PublicKey key) throws FailedAuthenicationException{

		byte[] cipherText;
		Cipher cipher;

		try{
			String RandomKey = UUID.randomUUID().toString();

			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(RandomKey.getBytes());

			String sha256Hash = HashSHA256Generator(RandomKey);
			String sha256Hash2 = HashSHA256Generator(sha256Hash);	

			String base64EncodedText = Base64.encodeBytes(cipherText);

			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://"+ AppConstants.ATP_SERVER_IP + "/TradeGetKey2?"+base64EncodedText);
			URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/TradeGetKey2?"+base64EncodedText);
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetKey2]Url address: " + url.toString() );

			byte[] bytes = readUrlData(url);
			
			DefinitionConstants.Debug("Server response: " + new String(bytes) );
			
			byte[] decodedResult = Base64.decode(bytes);

			String result = FormatUtil.asHex(decodedResult);

			if(result.equals(sha256Hash2)){
				String arr[] = {RandomKey, base64EncodedText};
				return arr;
			}else{
				return null;
			}


		}catch (MalformedURLException e) {
			e.printStackTrace();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	*/
	public static String[] tradeGetKey2(PublicKey key) throws Exception{
		URL url				= null;
		byte[] dataBytes	= null;
		String response		= null;
		byte[] cipherText	= null;
		Cipher cipher		= null;
		try{
			String RandomKey 		= UUID.randomUUID().toString();
			cipher 					= Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText 				= cipher.doFinal(RandomKey.getBytes());
			String sha256Hash 		= FormatUtil.HashSHA256Generator(RandomKey);
			String sha256Hash2 		= FormatUtil.HashSHA256Generator(sha256Hash);	
			String base64EncodedText= CustomizedBase64.encodeBytes(cipherText);

			// Mary@20120718 - avoid Malformed ipv6 address error
			// URL url = new URL("http://"+ AppConstants.ATP_SERVER_IP + "/TradeGetKey2?"+base64EncodedText);
			url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetKey2?" + URLEncoder.encode(base64EncodedText));	//2016-09-05, added URLEncode as requested by ATP
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetKey2]Url address: " + url.toString() );
			dataBytes 	= readUrlData(url);
			response	= new String(dataBytes);
			DefinitionConstants.Debug("[AtpConnectUtil][tradeGetKey2] Server response: " + response);
			
			if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			
			byte[] decodedResult	= CustomizedBase64.decode(dataBytes);

			String result 			= FormatUtil.asHex(decodedResult);

			if(result.equals(sha256Hash2)){
				String arr[] = {RandomKey, base64EncodedText};
				return arr;
			}else{
				return null;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
			response	= null;
			cipherText	= null;
			cipher		= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	public static String tradeLogout(Map<String, String> parameters) throws FailedAuthenicationException {
		String userParam	= parameters.get(ParamConstants.USERPARAM_TAG);

		try {
			/* Mary@20120718 - avoid Malformed ipv6 address error - START
			String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
					+ "]TradeLogout";

			DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam + "]TradeLogout" );

			URL url = new URL(urlAddress);
			*/
			URL url			= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[" + userParam+ "]TradeLogout");
			DefinitionConstants.Debug("[AtpConnectUtil][tradeLogout]Url address: " + url.toString() );
			// Mary@20120718 - avoid Malformed ipv6 address error - END
			
			byte[] dataBytes= readUrlData(url);
			String response = new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);

			DefinitionConstants.Debug("[AtpConnectUtil][tradeLogout]Server response: "+ response);
			
			return response;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradeLogin(Map parameters, StringBuffer buffer, StringBuffer paramBuffer) 
			throws InvalidPasswordException, FailedAuthenicationException {

		String userParamKey = null;
		byte[] userParamKeyInByte = null;
		String username = (String) parameters.get(ParamConstants.USERNAME_TAG);
		String password = (String) parameters.get(ParamConstants.PASSWORD_TAG);

		if(AppConstants.EnableEncryption==false){
			userParamKey = (String) parameters.get(ParamConstants.USERPARAM_KEY_TAG);
		}else{
			userParamKeyInByte = (byte[])parameters.get(ParamConstants.USERPARAM_KEY_INBYTE_TAG);
		}


		String userParam = null;

		try {

			URL url = null;

			if(AppConstants.EnableEncryption == false){

				

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				if(AppConstants.atpCompress){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + userParamKey +"]TradeLogin?Username="
							+ username + "|Password=" + password 
							+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0|PullCompress=1");
				}else{
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + userParamKey +"]TradeLogin?Username="
							+ username + "|Password=" + password 
							+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0");
				}

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + userParamKey +"]TradeLogin?Username="
						+ username + "|Password=" + password 
						+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0");
				/
				String strFileName 	= "/[" + userParamKey +"]TradeLogin?Username=" + username + "|Password=" + password
										+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0";
				
				if(AppConstants.atpCompress)
					strFileName += "|PullCompress=1";
				
				url 				= new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, strFileName);
				DefinitionConstants.Debug( "[AtpConnectUtil][tradeLogin]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);

				String response = new String(dataBytes, AppConstants.STRING_ENCODING);

				DefinitionConstants.Debug("Server response: "+response);

				int pos = response.indexOf("[UserParam]=") + "[UserParam]=".length();
				int end = response.indexOf("\r", pos);

				buffer.append(response);

				if (response.startsWith(AppConstants.ERROR_STRING)) {
					String errorCode = SystemUtil.extractErrorCode(response);
					throw new InvalidPasswordException(errorCode, response);
				}

				if (paramBuffer != null) {
					String paramString = response.substring(pos, end);			
					paramBuffer.append(paramString);
				}

				userParam = SystemUtil.extractUserParam(dataBytes, "[UserParam]=", false);

			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;
				String EncryptedUsername = encryptATPString(username);
				String EncryptedPassword = encryptATPString(password);

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = null; 

				if(AppConstants.atpCompress){
					InputParam = "Username="
							+ EncryptedUsername + "|Password=" + EncryptedPassword 
							+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=1|Encryption=0|PullCompress=1";
				}else{
					InputParam = "Username="
							+ EncryptedUsername + "|Password=" + EncryptedPassword 
							+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=1|Encryption=0";
				}

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String encodedUserParamInByte = Base64.encodeBytes(userParamKeyInByte);
				String urlAdd = "10|"+stx+ encodedUserParamInByte+"|"+encryptedInputParam+etx+"E";
				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeLogin?"+encodedURL);
				url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[0]TradeLogin?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeLogin]Url address: " + url.toString() );

				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				byte[] arr = null;

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes());

					arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr);

				}

				DefinitionConstants.Debug("Server response: "+decryptedString);

				int pos = decryptedString.indexOf("[UserParam]=") + "[UserParam]=".length();
				int end = decryptedString.indexOf("\r", pos);

				buffer.append(decryptedString);

				if (decryptedString.startsWith(AppConstants.ERROR_STRING)) {
					String errorCode = SystemUtil.extractErrorCode(decryptedString);
					throw new InvalidPasswordException(errorCode, decryptedString);
				}

				if (paramBuffer != null) {
					String paramString = decryptedString.substring(pos, end);			
					paramBuffer.append(paramString);
				}

				userParam = SystemUtil.extractUserParam(arr, "[UserParam]=", true);
				byte[] userParamInByte = SystemUtil.extractUserParamInByte(arr, "[UserParam]=", true);
				AppConstants.userParamInByte = userParamInByte;

			}

			return userParam;

		} catch (IOException e) {
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
			
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		} 
		return null;
	}
	*/
	public static String tradeLogin(Map<String, Object> parameters, StringBuffer buffer, StringBuffer paramBuffer) throws Exception{
		URL url						= null;
		byte[] dataBytes			= null;
		String response				= null;
		String userParamKey 		= null;
		byte[] userParamKeyInByte 	= null;
		String username 			= (String) parameters.get(ParamConstants.USERNAME_TAG);
		String password 			= (String) parameters.get(ParamConstants.PASSWORD_TAG);
		String brokerId				= (String) parameters.get(ParamConstants.BROKER_ID_TAG);	//Added by Thinzar@20111111, Change_Request-20141101, ReqNo. 3

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParamKey 		= (String) parameters.get(ParamConstants.USERPARAM_KEY_TAG);
		else
			userParamKeyInByte 	= (byte[])parameters.get(ParamConstants.USERPARAM_KEY_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamKeyInByte 	= (byte[])parameters.get(ParamConstants.USERPARAM_KEY_INBYTE_TAG);
		else
			userParamKey 		= (String) parameters.get(ParamConstants.USERPARAM_KEY_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
		String strRSAPassword		= (String) parameters.get(ParamConstants.RSA_PASSWORD_TAG);
		String strE2EERandomNum		= (String) parameters.get(ParamConstants.E2EE_RANDOM_NUM_TAG);
		// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
		
		// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - START
		String strDeviceOSType		= (String) parameters.get(ParamConstants.PARAM_STR_DEVICE_OS_TYPE);
		String strDeviceOSVersion	= (String) parameters.get(ParamConstants.PARAM_STR_DEVICE_OS_VERSION);
		String strDeviceModelName	= (String) parameters.get(ParamConstants.PARAM_STR_DEVICE_MODEL);
		String strDeviceConnectivity= (String) parameters.get(ParamConstants.PARAM_STR_DEVICE_CONNECTIVITY);
		String strSimOperatorName	= (String) parameters.get(ParamConstants.PARAM_STR_SIM_OPERATOR_NAME);
		String strExtraInfo			= (String) parameters.get(ParamConstants.PARAM_STR_EXTRA_INFO);
		String strDeviceScreenSize	= (String) parameters.get(ParamConstants.PARAM_STR_SCREEN_SIZE);
		// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - END
		
		//Added by Thinzar, Change_Request20160722, ReqNo.11
		String strLoginType			= (String) parameters.get(ParamConstants.PARAM_STR_LOGIN_TYPE);
		
		String userParam 			= null;
		
		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				if(AppConstants.atpCompress){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + userParamKey +"]TradeLogin?Username="
							+ username + "|Password=" + password 
							+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0|PullCompress=1");
				}else{
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + userParamKey +"]TradeLogin?Username="
							+ username + "|Password=" + password 
							+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0");
				}

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + userParamKey +"]TradeLogin?Username="
						+ username + "|Password=" + password 
						+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0");
				*/
				
				/* Mary@20130102 - Fixes_Request-20121102, ReqNo.14 - START
				String strFileName 	= "/[" + userParamKey +"]TradeLogin?Username=" + username + "|Password=" + password
										+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0";
				
				if(AppConstants.atpCompress)
					strFileName += "|PullCompress=1";
				
				url 				= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), strFileName);
				DefinitionConstants.Debug( "[AtpConnectUtil][tradeLogin]Url address: " + url.toString() );
				*/
				
				/* Added by Sonia@20130516 - Change_Request-20130424, ReqNo.2 - START
				String strFileName 	= "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0";
				*/
				String strFileName 	= "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0|AppCode=MA";
				// Added by Sonia@20130516 - Change_Request-20130424, ReqNo.2 - END
				
				if(AppConstants.atpCompress)
					strFileName += "|PullCompress=1";
				
				// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
				if( AppConstants.brokerConfigBean.isEnableE2EEEncryption() ){
					strFileName = "[@RSAEnabled@]Username=" + username + "|Password=" + password 
									+ "|RSAPassword=" + strRSAPassword + "|E2EERandomNumber=" + strE2EERandomNum + strFileName;
				}else{
					strFileName = "Username=" + username + "|Password=" + password + strFileName;
				}
				// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
				
				// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - START
				strFileName		+= "|OSName=" + strDeviceOSType + "|Version=" + strDeviceOSVersion + "|Model=" + strDeviceModelName + "|NetType=" + strDeviceConnectivity
									+ "|Provider=" + strSimOperatorName + "|Content=" + strExtraInfo + "|ScreenSize=" + strDeviceScreenSize;
				// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - END
				
				//Change_Request-20160722, ReqNo.11
				if(strLoginType != null && strLoginType.trim().length() > 0){
					strFileName		+= "|LoginType=" + strLoginType;
				}
				
				//Added by Thinzar, Change_Request-20161101, ReqNo.4
				strFileName			+= "|ClientIP=" + SystemUtil.getIPAddress(true);

				String strURLEncoded= URLEncoder.encode(strFileName, AppConstants.STRING_ENCODING_FORMAT);
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[" + userParamKey +"]TradeLogin?" + strURLEncoded);
				DefinitionConstants.Debug( "[AtpConnectUtil][tradeLogin]Url address: " + url.toString() );
				// Mary@20130102 - Fixes_Request-20121102, ReqNo.14 - END
				
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeLogin]Server response: "+response);
				
				/* Mary@20121227 - StartWith is case sensitive
				if (response.startsWith(AppConstants.ERROR_STRING)) {
				*/
				if (response.toUpperCase().startsWith(AppConstants.ERROR_STRING)) {
					String errorCode = SystemUtil.extractErrorCode(response);
					throw new InvalidPasswordException(errorCode, response);
				}

				buffer.append(response);

				int pos 			= response.indexOf("[UserParam]=") + "[UserParam]=".length();
				int end 			= response.indexOf("\r", pos);
				if (paramBuffer != null) {
					String paramString = response.substring(pos, end);			
					paramBuffer.append(paramString);
				}

				userParam = SystemUtil.extractUserParam(dataBytes, "[UserParam]=", false);
			}else{
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String encryptionKey			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam				= null;
				
				// Added by Mary@20130114 - Change_Request-20130104, ReqNo.1 - START
				if( AppConstants.brokerConfigBean.isEnableE2EEEncryption() ){
					InputParam 				= "[@RSAEnabled@]Username=" + username + "|Password=" + password 
												+ "|RSAPassword=" + strRSAPassword + "|E2EERandomNumber=" + strE2EERandomNum
												+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0";
					
				}else{
				// Added by Mary@20130114 - Change_Request-20130104, ReqNo.1 - END
					String EncryptedUsername= encryptATPString(username);
					String EncryptedPassword= encryptATPString(password);
	
					InputParam 				= "Username=" + EncryptedUsername + "|Password=" + EncryptedPassword 
												+ "|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=1|Encryption=0";
				}
				InputParam						= AppConstants.atpCompress ? InputParam + "|PullCompress=1" : InputParam;
				// Added by Sonia@20130516 - Change_Request-20130424, ReqNo.2
				InputParam 						+= "|AppCode=MA";
				// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - START
				InputParam						+= "|OSName=" + strDeviceOSType + "|Version=" + strDeviceOSVersion 
													+ "|Model=" + strDeviceModelName + "|NetType=" + strDeviceConnectivity
													+ "|Provider=" + strSimOperatorName + "|Content=" + strExtraInfo + "|ScreenSize=" + strDeviceScreenSize;
				// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - END
				
				//Change_Request-20160722, ReqNo.11
				if(strLoginType != null && strLoginType.trim().length() > 0){
					InputParam						+= "|LoginType=" + strLoginType;
				}
				
				//Added by Thinzar@20141111, Change_Request-20141101, ReqNo. 3
				if(brokerId != null && brokerId.trim().length() > 0){
					InputParam					+= "|BHCode=" + brokerId;
				}
				
				//Added by Thinzar, Change_Request-20161101, ReqNo.4
				InputParam						+= "|ClientIP=" + SystemUtil.getIPAddress(true);
				
				DefinitionConstants.Debug("[AtpConnectUtil][tradeLogin]Params:" + InputParam);
				
				byte[] EData 					= AES256Encryption.encryptData(InputParam.getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String encodedUserParamInByte 	= CustomizedBase64.encodeBytes(userParamKeyInByte);
				String urlAdd 					= "10|" + stx + encodedUserParamInByte + "|" + encryptedInputParam + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd, AppConstants.STRING_ENCODING_FORMAT);
				
				// Mary@20120718 - avoid Malformed ipv6 address error
				// url = new URL("http://" + AppConstants.brokerConfigBean.getStrAtpIP() + "/[0]TradeLogin?"+encodedURL);
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradeLogin?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeLogin]Url address: " + url.toString() );
				
				dataBytes						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeLogin]response: " + response );

				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";

				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;

				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx 	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				byte[] arr 					= null;
				if (startIdx != -1 && endIdx != -1) {
					String sSubData	= response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes());
					arr 			= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradeLogin]Server response after decrypt : "+decryptedString);
				
				if( decryptedString == null )
					throw new Exception(ErrorCodeConstants.FAIL_TRADE_LOGIN);

				int pos 					= decryptedString.indexOf("[UserParam]=") + "[UserParam]=".length();
				int end 					= decryptedString.indexOf("\r", pos);
				buffer.append(decryptedString);

				/* Mary@20121227 - StartWith is case sensitive
				if (decryptedString.startsWith(AppConstants.ERROR_STRING)) {
				*/
				if(decryptedString.toUpperCase().startsWith(AppConstants.ERROR_STRING)) {
					String errorCode = SystemUtil.extractErrorCode(decryptedString);
					throw new InvalidPasswordException(errorCode, decryptedString);
				}
				
				// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isOwnException(decryptedString) )
					throw new Exception(decryptedString);
				
				if(pos == ("[UserParam]=".length() + -1 ) )
					throw new Exception(ErrorCodeConstants.FAIL_TRADE_LOGIN);
				// ADded by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END

				if(paramBuffer != null){
					String paramString = decryptedString.substring(pos, end);			
					paramBuffer.append(paramString);
				}

				userParam 					= SystemUtil.extractUserParam(arr, "[UserParam]=", true);
				byte[] userParamInByte 		= SystemUtil.extractUserParamInByte(arr, "[UserParam]=", true);
				AppConstants.userParamInByte= userParamInByte;

			}

			return userParam;
		// NOTE : No retry so whenever have exception just throw as to avoid suspension when correct msg not capture
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			url					= null;
			dataBytes			= null;
			response			= null;
			userParamKey 		= null;
			userParamKeyInByte 	= null;
			username 			= null;
			password 			= null;
		}
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradeTicket(Map parameters) throws FailedAuthenicationException {

		int i=136;
		char c =(char)i;
		String MA = Character.toString(c);

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}     
		String ticketNo = (String) parameters.get(ParamConstants.TICKETNO_TAG);       
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);

		String brokerCode = (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String clientCode = (String) parameters.get(ParamConstants.CLIENTCODE_TAG);
		String accountNo = (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String actionType = (String) parameters.get(ParamConstants.ACTIONCODE_TAG);
		String symbolCode = (String) parameters.get(ParamConstants.SYMBOLCODE_TAG);
		String orderTimestamp = (String) parameters.get(ParamConstants.ORDER_TIMESTAMP_TAG);
		String quantity = (String) parameters.get(ParamConstants.ORDER_QUANTITY_TAG);
		String orderSource = (String) parameters.get(ParamConstants.ORDER_SOURCE_TAG);
		String branchCode = (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);

		String encodePin = (String) parameters.get(ParamConstants.TRADINGPIN_TAG);
		String price = (String) parameters.get(ParamConstants.ORDER_PRICE_TAG);
		String orderType = (String) parameters.get(ParamConstants.ORDER_TYPE_TAG);
		String validity = (String) parameters.get(ParamConstants.ORDER_VALIDITY_TAG);
		String paymentType = (String) parameters.get(ParamConstants.PAYMENT_TYPE_TAG);
		String currency = (String) parameters.get(ParamConstants.CURRENCY_TAG);
		String stopLimit = (String) parameters.get(ParamConstants.ORDER_STOPLIMIT_TAG);

		// Added by Mary@20120717 - to confirm is Short Sell Activity proceed
		boolean isProceedShortSell	= ( (Boolean)parameters.get(ParamConstants.SHORT_SELL_IS_PROCEED) ).booleanValue();
		
		try {
			String urlAddress = null;

			if(AppConstants.EnableEncryption==false){

				String encodedPin = SystemUtil.encryptATPString(encodePin);
				
				/*  Mary@20120718 - avoid Malformed ipv6 address error - START
				if (actionType.equals("Revise") || actionType.equals("Cancel")) {
					String orderNo = (String) parameters.get(ParamConstants.ORDER_NO_TAG);
					String orderFixedNo = (String) parameters.get(ParamConstants.ORDER_FIXEDNO_TAG);
					long matchedQuantity = ((Long) parameters.get(ParamConstants.ORDER_MATCHEDQTY_TAG)).longValue();

					if (actionType.equals("Cancel")) {
						urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeTicket?*={" 
								+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp 
								+ "|;=" + quantity + "|8=" + orderSource + "|M=" + branchCode + "|P=" + exchangeCode + "|R=" + encodedPin
								+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					} else if (actionType.equals("Revise")) {
						urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeTicket?*={" 
								+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp + "|?=" + stopLimit 
								+ "|;=" + quantity + "|E=" + String.valueOf(matchedQuantity) + "|8=" + orderSource + "|M=" + branchCode 
								+ "|P=" + exchangeCode + "|R=" + encodedPin + "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					}
				} else {

					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeTicket?*={" 
							+ ticketNo + "}|2=" + senderCode + "|3=" + brokerCode + "|/=" + clientCode + "|.=" + accountNo
							+ "|5=" + symbolCode + "|7=" + orderTimestamp + "|;=" + quantity + "|?=" + stopLimit 
							+ "|8=" + orderSource + "|M=" + branchCode + "|F=" + paymentType + "|P=" + exchangeCode + "|R=" + encodedPin
							+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType +"|B=" +currency;

					String expiryDate = (String) parameters.get(ParamConstants.ORDER_EXPIRYDATE_TAG);

					if (expiryDate != null) {
						urlAddress += "|:=" + expiryDate;
					}
				}
				// Added by Mary@20120717 - to confirm is Short Sell Activity proceed - START
				urlAddress	= isProceedShortSell ? urlAddress + "|L=1" : urlAddress;
				DefinitionConstants.Debug("[AtpConnectUtil][TradeTicket]Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				if (actionType.equals("Revise") || actionType.equals("Cancel")) {
					String orderNo = (String) parameters.get(ParamConstants.ORDER_NO_TAG);
					String orderFixedNo = (String) parameters.get(ParamConstants.ORDER_FIXEDNO_TAG);
					long matchedQuantity = ((Long) parameters.get(ParamConstants.ORDER_MATCHEDQTY_TAG)).longValue();

					if (actionType.equals("Cancel")) {
						urlAddress = "/[" + userParam +"]TradeTicket?*={" 
								+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp 
								+ "|;=" + quantity + "|8=" + orderSource + "|M=" + branchCode + "|P=" + exchangeCode + "|R=" + encodedPin
								+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					} else if (actionType.equals("Revise")) {
						urlAddress = "/[" + userParam +"]TradeTicket?*={" 
								+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp + "|?=" + stopLimit 
								+ "|;=" + quantity + "|E=" + String.valueOf(matchedQuantity) + "|8=" + orderSource + "|M=" + branchCode 
								+ "|P=" + exchangeCode + "|R=" + encodedPin + "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					}
				} else {

					urlAddress = "/[" + userParam +"]TradeTicket?*={" 
							+ ticketNo + "}|2=" + senderCode + "|3=" + brokerCode + "|/=" + clientCode + "|.=" + accountNo
							+ "|5=" + symbolCode + "|7=" + orderTimestamp + "|;=" + quantity + "|?=" + stopLimit 
							+ "|8=" + orderSource + "|M=" + branchCode + "|F=" + paymentType + "|P=" + exchangeCode + "|R=" + encodedPin
							+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType +"|B=" +currency;

					String expiryDate = (String) parameters.get(ParamConstants.ORDER_EXPIRYDATE_TAG);

					if (expiryDate != null) {
						urlAddress += "|:=" + expiryDate;
					}
				}
				
				// Added by Mary@20120712 - to confirm is Short Sell Activity proceed - START
				urlAddress	= isProceedShortSell ? urlAddress + "|L=1" : urlAddress;
				
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket]Url address: " + url.toString() );			
				// Mary@20120718 - avoid Malformed ipv6 address error - END	
		
				byte[] bytes = null;

				bytes = readUrlData(url);
				String response = new String(bytes, AppConstants.STRING_ENCODING);

				// Added by Mary@20120717 - additional handling for error code 502 - START
				if( response.toUpperCase().contains("ERROR 502") )
					response		= response.split("\r\n")[0];
				// Added by Mary@20120717 - additional handling for error code 502 - END
				DefinitionConstants.Debug("Server response: " + response);

				return response;
			}else{

				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);
				String InputParam = null;

				String encodedPin = encryptATPString(encodePin);

				if (actionType.equals("Revise") || actionType.equals("Cancel")) {
					String orderNo = (String) parameters.get(ParamConstants.ORDER_NO_TAG);
					String orderFixedNo = (String) parameters.get(ParamConstants.ORDER_FIXEDNO_TAG);
					long matchedQuantity = ((Long) parameters.get(ParamConstants.ORDER_MATCHEDQTY_TAG)).longValue();

					if (actionType.equals("Cancel")) {
						InputParam = "*={" + ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp 
								+ "|;=" + quantity + "|8=" + orderSource + "|M=" + branchCode + "|P=" + exchangeCode + "|R=" + encodedPin
								+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					}else if (actionType.equals("Revise")){
						InputParam = "*={" 
								+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp + "|?=" + stopLimit
								+ "|;=" + quantity + "|E=" + String.valueOf(matchedQuantity) + "|8=" + orderSource + "|M=" + branchCode 
								+ "|P=" + exchangeCode + "|R=" + encodedPin + "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					}
				}else{
					InputParam = "*={" 
							+ ticketNo + "}|2=" + senderCode + "|3=" + brokerCode + "|/=" + clientCode + "|.=" + accountNo
							+ "|5=" + symbolCode + "|7=" + orderTimestamp + "|;=" + quantity + "|?=" + stopLimit
							+ "|8=" + orderSource + "|M=" + branchCode + "|F=" + paymentType + "|P=" + exchangeCode + "|R=" + encodedPin
							+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType +"|B=" +currency;
				}
				
				// Added by Mary@20120717 - to confirm is Short Sell Activity proceed - START
				InputParam	= isProceedShortSell ? InputParam + "|L=1" : InputParam;
				DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket]Have Encryption - InputParam: " + InputParam);
				// Added by Mary@20120717 - to confirm is Short Sell Activity proceed - END

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				// Mary@20120718 - avoid Malformed ipv6 address error
				// URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeTicket?"+encodedURL);
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[0]TradeTicket?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket]Url address: " + url.toString() );
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				// Added by Mary@20120717 - additional handling for error code 502 - START
				if( decryptedString.toUpperCase().contains("ERROR 502") ){
					decryptedString		= decryptedString.split("\r\n")[0];
				}
				// Added by Mary@20120717 - additional handling for error code 502 - END
				DefinitionConstants.Debug("Server response: "+decryptedString);

				return decryptedString;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		} 
		return null;
	}
	*/

	public static String tradeTicket(Map<String, Object> parameters) throws Exception {
		URL url						= null;
		byte[] dataBytes			= null;
		String response				= null;
		String urlAddress 			= null;
		String MA 					= Character.toString( FormatUtil.convertAsciiToChar(136) );
		String userParam 			= null;
		byte[] userParamInByte 		= null;

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if(AppConstants.EnableEncryption==false){
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}     
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String ticketNo 			= (String) parameters.get(ParamConstants.TICKETNO_TAG);       
		String senderCode 			= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String brokerCode 			= (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String clientCode 			= (String) parameters.get(ParamConstants.CLIENTCODE_TAG);
		String accountNo 			= (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String actionType 			= (String) parameters.get(ParamConstants.ACTIONCODE_TAG);
		String symbolCode 			= (String) parameters.get(ParamConstants.SYMBOLCODE_TAG);
		String orderTimestamp 		= (String) parameters.get(ParamConstants.ORDER_TIMESTAMP_TAG);
		String quantity 			= (String) parameters.get(ParamConstants.ORDER_QUANTITY_TAG);
		String orderSource 			= (String) parameters.get(ParamConstants.ORDER_SOURCE_TAG);
		String branchCode			= (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String exchangeCode 		= (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 START
		String encodePin 			= AppConstants.selectedDeviceID.equals("")?(String) parameters.get(ParamConstants.TRADINGPIN_TAG):"123456";
		// Added by Mary@20130320 - Fixes_Request-20130314, ReqNo.5
		encodePin					= encodePin == null && !AppConstants.brokerConfigBean.isEnablePinFeatures() ? "123456" : encodePin;
		//String encodePin 			="123456";
				
		String deviceID				= AppConstants.selectedDeviceID;
		String otpValue				= AppConstants.otpValue;
		//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 END
		
		String price 				= (String) parameters.get(ParamConstants.ORDER_PRICE_TAG);
		String orderType 			= (String) parameters.get(ParamConstants.ORDER_TYPE_TAG);
		String validity 			= (String) parameters.get(ParamConstants.ORDER_VALIDITY_TAG);
		String paymentType 			= (String) parameters.get(ParamConstants.PAYMENT_TYPE_TAG);
		// Added by Mary@20131011 - Fixes_Request-20130711, ReqNo.33
		paymentType					= paymentType == null ? "" : paymentType;
		String currency 			= (String) parameters.get(ParamConstants.CURRENCY_TAG);
		String stopLimit 			= (String) parameters.get(ParamConstants.ORDER_STOPLIMIT_TAG);
		
		// Added by Mary@20120717 - to confirm is Short Sell Activity proceed
		boolean isProceedShortSell	= ( (Boolean)parameters.get(ParamConstants.SHORT_SELL_IS_PROCEED) ).booleanValue();
		//Kw@20130404 - Fixes_Request-20130314, ReqNo.11 
		boolean isProceedExceed5Bid =( (Boolean)parameters.get(ParamConstants.EXCEED_5_BID_IS_PROCEED)).booleanValue();
		// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1 - START
		String strMinQty			= (String) parameters.get(ParamConstants.MINIMUM_QUANTITY_TAG);
		String strDisclosedQty		= (String) parameters.get(ParamConstants.DISCLOSED_QUANTITY_TAG);
		// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1 - END
		
		// Added by Mary@20130321 - Fixes_Request-20130319, ReqNo.7
		boolean isAwareShortSell	= ( (Boolean)parameters.get(ParamConstants.SHORT_SELL_IS_AWARE) ).booleanValue();
		
		//Added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4
		String triggerPriceType 	 = (String) parameters.get(ParamConstants.TRIGGER_PRICE_TYPE_TAG);
		String triggerPriceDirection = (String) parameters.get(ParamConstants.TRIGGER_PRICE_DIRECTION_TAG); 
		
		//Change_Request-20161101, ReqNo.2
		boolean isPrivateOrder		= false;
		if(parameters.containsKey(ParamConstants.IS_PRIVATE_ORDER)) 
			isPrivateOrder			= ((Boolean)parameters.get(ParamConstants.IS_PRIVATE_ORDER)).booleanValue(); 
		
		//Change_Request-20170119, ReqNo.4
		String externalOrderSource	= null;
		if(parameters.containsKey(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG))
			externalOrderSource	= ((String) parameters.get(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG)).trim();
		
		// Added by Mary@20131011 - Fixes_Request-20130711, ReqNo.33 - START
		try{
			ticketNo		= ticketNo.trim();
			senderCode		= senderCode.trim();
			brokerCode		= brokerCode.trim();
			clientCode		= clientCode.trim();
			accountNo		= accountNo.trim();
			actionType		= actionType.trim();
			symbolCode		= symbolCode.trim();
			orderTimestamp	= orderTimestamp.trim();
			quantity		= quantity.trim();
			orderSource		= orderSource.trim();
			branchCode		= branchCode.trim();
			exchangeCode	= exchangeCode.trim();
			encodePin		= encodePin.trim();
			price			= price.trim();
			orderType		= orderType.trim();
			validity		= validity.trim();
			paymentType		= paymentType.trim();
			currency		= currency.trim();
			stopLimit		= stopLimit.trim();
			strMinQty		= strMinQty.trim();
			strDisclosedQty	= strDisclosedQty.trim();
		}catch(Exception e){
			throw new Exception(ErrorCodeConstants.INVALID_ATP_REQUEST_DATA);
		}
		// Added by Mary@20131011 - Fixes_Request-20130711, ReqNo.33 - END
		
		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				
				//Kw@20130104 Change Request- Request No.2 -START
				//String encodedPin =	AppConstants.selectedDeviceID.equals("")?SystemUtil.encryptATPString(encodePin):encodePin;
				String encodedPin   =    SystemUtil.encryptATPString(encodePin);
				SystemUtil.encryptATPString(otpValue);
				//Kw@20130104 Change Request- Request No.2 -END
				
				/*  Mary@20120718 - avoid Malformed ipv6 address error - START
				if (actionType.equals("Revise") || actionType.equals("Cancel")) {
					String orderNo = (String) parameters.get(ParamConstants.ORDER_NO_TAG);
					String orderFixedNo = (String) parameters.get(ParamConstants.ORDER_FIXEDNO_TAG);
					long matchedQuantity = ((Long) parameters.get(ParamConstants.ORDER_MATCHEDQTY_TAG)).longValue();

					if (actionType.equals("Cancel")) {
						urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeTicket?*={" 
								+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp 
								+ "|;=" + quantity + "|8=" + orderSource + "|M=" + branchCode + "|P=" + exchangeCode + "|R=" + encodedPin
								+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					} else if (actionType.equals("Revise")) {
						urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeTicket?*={" 
								+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp + "|?=" + stopLimit 
								+ "|;=" + quantity + "|E=" + String.valueOf(matchedQuantity) + "|8=" + orderSource + "|M=" + branchCode 
								+ "|P=" + exchangeCode + "|R=" + encodedPin + "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					}
				} else {

					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeTicket?*={" 
							+ ticketNo + "}|2=" + senderCode + "|3=" + brokerCode + "|/=" + clientCode + "|.=" + accountNo
							+ "|5=" + symbolCode + "|7=" + orderTimestamp + "|;=" + quantity + "|?=" + stopLimit 
							+ "|8=" + orderSource + "|M=" + branchCode + "|F=" + paymentType + "|P=" + exchangeCode + "|R=" + encodedPin
							+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType +"|B=" +currency;

					String expiryDate = (String) parameters.get(ParamConstants.ORDER_EXPIRYDATE_TAG);

					if (expiryDate != null) {
						urlAddress += "|:=" + expiryDate;
					}
				}
				// Added by Mary@20120717 - to confirm is Short Sell Activity proceed - START
				urlAddress	= isProceedShortSell ? urlAddress + "|L=1" : urlAddress;
				DefinitionConstants.Debug("[AtpConnectUtil][TradeTicket]Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/
				if ( actionType.toUpperCase().equals("REVISE") || actionType.toUpperCase().equals("CANCEL") ) {
					String orderNo 		= (String) parameters.get(ParamConstants.ORDER_NO_TAG);
					String orderFixedNo = (String) parameters.get(ParamConstants.ORDER_FIXEDNO_TAG);
					long matchedQuantity= ((Long) parameters.get(ParamConstants.ORDER_MATCHEDQTY_TAG)).longValue();
					if ( actionType.toUpperCase().equals("CANCEL") ) {
						urlAddress = "/[" + userParam +"]TradeTicket?*={" 
									+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
									+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp 
									+ "|;=" + quantity + "|8=" + orderSource + "|M=" + branchCode + "|P=" + exchangeCode + "|R=" + encodedPin
									+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					}else if( actionType.toUpperCase().equals("REVISE") ) {
						//edited by Thinzar@20140819, Change_Request_20140801, ReqNo.4&5 - stopLimit FID changed from 63 to 159
						urlAddress = "/[" + userParam +"]TradeTicket?*={" 
									+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
									+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp + "|" + FormatUtil.convertAsciiToChar(159) + "=" + stopLimit 
									+ "|;=" + quantity + "|E=" + String.valueOf(matchedQuantity) + "|8=" + orderSource + "|M=" + branchCode 
									+ "|P=" + exchangeCode + "|R=" + encodedPin + "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
						
						String expiryDate 	= (String) parameters.get(ParamConstants.ORDER_EXPIRYDATE_TAG);

						if (expiryDate != null)
							urlAddress += "|:=" + expiryDate;
						
						//added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 - START
						if(triggerPriceType != null)		
							urlAddress += "|" +  FormatUtil.convertAsciiToChar(154) + "=" + triggerPriceType;
						if(triggerPriceDirection != null)	
							urlAddress += "|" + FormatUtil.convertAsciiToChar(155) + "=" + triggerPriceDirection;
						//added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 - END
						
					}
				} else {
					urlAddress 			= "/[" + userParam +"]TradeTicket?*={" 
											+ ticketNo + "}|2=" + senderCode + "|3=" + brokerCode + "|/=" + clientCode + "|.=" + accountNo
											+ "|5=" + symbolCode + "|7=" + orderTimestamp + "|;=" + quantity + "|" + FormatUtil.convertAsciiToChar(159) + "=" + stopLimit 
											+ "|8=" + orderSource + "|M=" + branchCode + "|F=" + paymentType + "|P=" + exchangeCode + "|R=" + encodedPin
											+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType +"|B=" +currency+"|"+MA+"=MA";

					String expiryDate 	= (String) parameters.get(ParamConstants.ORDER_EXPIRYDATE_TAG);

					if (expiryDate != null)
						urlAddress += "|:=" + expiryDate;
					
					//added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 - START
					if(triggerPriceType != null)		
						urlAddress += "|" +  FormatUtil.convertAsciiToChar(154) + "=" + triggerPriceType;
					if(triggerPriceDirection != null)	
						urlAddress += "|" + FormatUtil.convertAsciiToChar(155) + "=" + triggerPriceDirection;
					//added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 - END
					
					//Added by Thinzar, Change_Request-20161101, ReqNo.2
					if(isPrivateOrder)
						urlAddress	+= "|" + FormatUtil.convertAsciiToChar(168) + "=1";		//not sending if false
					
					// Added by Mary@20130320 - Fixes_Request-20130319, ReqNo.7
					urlAddress			= isAwareShortSell && actionType.toUpperCase().equals("SELL") ? urlAddress + "|" + FormatUtil.convertAsciiToChar(129) + "=262144" : urlAddress;
					
					//Change_Request_20170119, ReqNo.4
					if(externalOrderSource != null && externalOrderSource.length() > 0){
						urlAddress	+= "|" + FormatUtil.convertAsciiToChar(193) + "=" + externalOrderSource;		
					}
				}
				
				if(!(strMinQty.equals("")&&strDisclosedQty.equals(""))){
					// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1
					urlAddress += "|@=" + strMinQty + "|A=" + strDisclosedQty;
				}
				
				// Added by Mary@20120712 - to confirm is Short Sell Activity proceed - START
				//Kw@20130404 - Fixes_Request-20130314, ReqNo.11 
				urlAddress		= (isProceedShortSell||isProceedExceed5Bid) ? urlAddress + "|L=1" : urlAddress;
				//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 START
				if(!AppConstants.selectedDeviceID.equals("")){
					urlAddress +="|"+FormatUtil.convertAsciiToChar(140)+"=" +otpValue + "|"+FormatUtil.convertAsciiToChar(142)+"=" +deviceID;
				}
				
				//urlAddress += "|=" +deviceID +"|=" +otpValue;
				//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 END
				url 			= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket]Url address: " + url.toString() );			
				// Mary@20120718 - avoid Malformed ipv6 address error - END	
				
				dataBytes		= readUrlData(url);
				response 		= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);

				// Added by Mary@20120717 - additional handling for error code 502 - START & //Kw@20130404 - Fixes_Request-20130314, ReqNo.11
				if( response.toUpperCase().contains("ERROR 502") || response.toUpperCase().contains("ERROR 509") ){
					response		= response.split("\r\n")[0];
				}	
				// Added by Mary@20120717 - additional handling for error code 502 - END
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				
				DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket]Server response: " + response);

				return response;
			}else{

				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);
				String InputParam = null;

				String encodedPin = encryptATPString(encodePin);
				//Kw@20130104 Change Request- Request No.2 -START
				SystemUtil.encryptATPString(otpValue);
				//Kw@20130104 Change Request- Request No.2 -END

				if(actionType.toUpperCase().equals("REVISE") || actionType.toUpperCase().equals("CANCEL") ){
					String orderNo = (String) parameters.get(ParamConstants.ORDER_NO_TAG);
					String orderFixedNo = (String) parameters.get(ParamConstants.ORDER_FIXEDNO_TAG);
					long matchedQuantity = ((Long) parameters.get(ParamConstants.ORDER_MATCHEDQTY_TAG)).longValue();

					if( actionType.toUpperCase().equals("CANCEL") ){ 
						InputParam = "*={" + ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp 
								+ "|;=" + quantity + "|8=" + orderSource + "|M=" + branchCode + "|P=" + exchangeCode + "|R=" + encodedPin
								+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
					}else if( actionType.toUpperCase().equals("REVISE") ){
						InputParam = "*={" 
								+ ticketNo + "}|+=" + orderNo + "|,=" + orderFixedNo + "|2=" + senderCode + "|3=" + brokerCode 
								+ "|/=" + clientCode + "|.=" + accountNo + "|5=" + symbolCode + "|7=" + orderTimestamp + "|" + FormatUtil.convertAsciiToChar(159) + "=" + stopLimit
								+ "|;=" + quantity + "|E=" + String.valueOf(matchedQuantity) + "|8=" + orderSource + "|M=" + branchCode 
								+ "|P=" + exchangeCode + "|R=" + encodedPin + "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType+"|"+MA+"=MA";
						
						String expiryDate 	= (String) parameters.get(ParamConstants.ORDER_EXPIRYDATE_TAG);

						if (expiryDate != null)
							InputParam += "|:=" + expiryDate;
						//added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 - START
						if(triggerPriceType != null)		InputParam += "|" +  FormatUtil.convertAsciiToChar(154) + "=" + triggerPriceType;
						if(triggerPriceDirection != null)	InputParam += "|" + FormatUtil.convertAsciiToChar(155) + "=" + triggerPriceDirection;
						//added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 - END
					}
				}else{
					InputParam = "*={" 
							+ ticketNo + "}|2=" + senderCode + "|3=" + brokerCode + "|/=" + clientCode + "|.=" + accountNo
							+ "|5=" + symbolCode + "|7=" + orderTimestamp + "|;=" + quantity + "|" + FormatUtil.convertAsciiToChar(159) + "=" + stopLimit
							+ "|8=" + orderSource + "|M=" + branchCode + "|F=" + paymentType + "|P=" + exchangeCode + "|R=" + encodedPin
							+ "|<=" + price + "|6=" + orderType + "|9=" + validity + "|4=" + actionType +"|B=" +currency+"|"+MA+"=MA";
					
					String expiryDate 	= (String) parameters.get(ParamConstants.ORDER_EXPIRYDATE_TAG);

					if (expiryDate != null)
						InputParam += "|:=" + expiryDate;
					
					//added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 - START
					if(triggerPriceType != null)		
						InputParam += "|" +  FormatUtil.convertAsciiToChar(154) + "=" + triggerPriceType;
					if(triggerPriceDirection != null)	
						InputParam += "|" + FormatUtil.convertAsciiToChar(155) + "=" + triggerPriceDirection;
					//added by Thinzar@20140815 - Change_Request-20140801, ReqNo.4 - END
					
					//Added by Thinzar, Change_Request-20161101, ReqNo.2
					if(isPrivateOrder)
						InputParam	+= "|" + FormatUtil.convertAsciiToChar(168) + "=1";		//not sending if "0"
					
					//Change_Request_20170119, ReqNo.4
					if(externalOrderSource != null && externalOrderSource.length() > 0){
						InputParam	+= "|" + FormatUtil.convertAsciiToChar(193) + "=" + externalOrderSource;		
					}
					
					// Added by Mary@20130320 - Fixes_Request-20130319, ReqNo.7
					InputParam			= isAwareShortSell && actionType.toUpperCase().equals("SELL") ? InputParam + "|" + FormatUtil.convertAsciiToChar(129) + "=262144" : InputParam;
				}
				
				if(!(strMinQty.equals("")&&strDisclosedQty.equals(""))){
					// Added by Mary@20121126 - Change_Request-20121106, ReqNo.1
					InputParam += "|@=" + strMinQty + "|A=" + strDisclosedQty;
				}
				//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 - START
				if(!AppConstants.selectedDeviceID.equals("")){
					InputParam += "|"+Character.toString(( char ) 142)+"=" +deviceID +"|"+Character.toString(( char ) 140)+"=" +otpValue;
				}
				// Added by Kw@20130116 - Change_Request20130104, ReqNo.2 - END
				// Added by Mary@20120717 - to confirm is Short Sell Activity proceed - START
				InputParam	= isProceedShortSell ? InputParam + "|L=1" : InputParam;
				DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket]Have Encryption - InputParam: " + InputParam);
				// Added by Mary@20120717 - to confirm is Short Sell Activity proceed - END

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);

				String encryptedInputParam = CustomizedBase64.encodeBytes(EData);
				String base64EncodedUserParam = CustomizedBase64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				// Mary@20120718 - avoid Malformed ipv6 address error
				// URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeTicket?"+encodedURL);
				url = new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradeTicket?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket]Url address: " + url.toString() );
				
				dataBytes = readUrlData(url);
				response = new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket] Server response: "+response);
				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				/* Mary@20130717 - Fixes_Request-20130108, ReqNo.16
				String decryptedString=null;
				*/
				String decryptedString			= response;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				
				// Added by Mary@20120717 - additional handling for error code 502 - START && //Kw@20130404 - Fixes_Request-20130314, ReqNo.11
				if( decryptedString.toUpperCase().contains("ERROR 502") || decryptedString.toUpperCase().contains("ERROR 509")){
					decryptedString		= decryptedString.split("\r\n")[0];
					DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket] decryptedString : " + decryptedString);
				}
			// Added by Mary@20120717 - additional handling for error code 502 - END
				DefinitionConstants.Debug("[AtpConnectUtil][tradeTicket] Server decrypted response: "+decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);

				return decryptedString;
			}

		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url					= null;
			dataBytes			= null;
			urlAddress 			= null;
			MA 					= null;
			userParam 			= null;
			userParamInByte 	= null;
    
			ticketNo 			= null;       
			senderCode 			= null;
			brokerCode 			= null;
			clientCode 			= null;
			accountNo 			= null;
			actionType 			= null;
			symbolCode 			= null;
			orderTimestamp 		= null;
			quantity 			= null;
			orderSource 		= null;
			branchCode			= null;
			exchangeCode 		= null;
			encodePin 			= null;
			price 				= null;
			orderType 			= null;
			validity 			= null;
			paymentType 		= null;
			currency 			= null;
			stopLimit 			= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	/* Mary@20130117 - should throw Exception to avoid unexpected crash
	public static String getHintInfo(Map parameters) throws FailedAuthenicationException {
	*/
	public static String getHintInfo(Map<String, Object> parameters) throws Exception {
		String adminKey			= (String) parameters.get(ParamConstants.ADMINKEY_TAG);
		String userName 		= (String) parameters.get(ParamConstants.USERNAME_TAG);
		String senderCode 		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);

		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				String urlAddress 	= null;
				
				/* Mary@20120718 - avoid Malformed ipv6 address error
				if(userName==null){
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]GetHintInfo?+="+senderCode;
				}else{
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]GetHintInfo?.="+userName;
				}

				DefinitionConstants.Debug("Url address: " +urlAddress);

				URL url = new URL(urlAddress);
				 */
				urlAddress		= "/[" + adminKey + "]GetHintInfo?";
				urlAddress		+= userName==null? "+="+senderCode : ".="+userName;

				URL url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][getHintInfo]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes	= readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING_FORMAT);

				DefinitionConstants.Debug("[AtpConnectUtil][getHintInfo]Response: "+ response);

				return response;
			}else{
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String inputParam 				= null;

				if(senderCode==null && userName != null)
					inputParam = ".="+userName;
				else if(senderCode!=null && userName == null)
					inputParam = "+="+senderCode;

				/* Mary@20121227 - Fixes_Request-20121102, ReqNo.12 - START
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] userParamInByte 			= (byte[])parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
				*/
				String[] AESKey					= null;
				String encryptionKey			= null;
				if(AppConstants.getEncryptedKey() == null){
					//AESKey			= tradeGetKey2( tradeGetPK() );		//Commented and replaced with following line, Fixes_Request-20170103, ReqNo.8
					//Change_Request-20170601, ReqNo.14 - START
					if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
						AESKey = tradeGetKey2A(tradeGetPK2A());
					}
					//Change_Request-20170601, ReqNo.14 - END
					else {
						AESKey = tradeGetKey2(tradeGetPK_LoadBalance());
					}

					encryptionKey	= AESKey[0];
					AppConstants.setEncryptedKey(encryptionKey);
				}else{
					encryptionKey	= AppConstants.getEncryptedKey();
				}
				
				byte[] userParamInByte			= null;
				if( parameters.get(ParamConstants.USERPARAM_INBYTE_TAG) != null ){
					userParamInByte 			= (byte[])parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
				}else{
					if(AppConstants.userParamInByte == null){
						//userParamInByte				= tradeGetSession2(AESKey);
						//Change_Request-20170601, ReqNo.14 - START
						if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
							userParamInByte = tradeGetSession2A(AESKey);
						}
						//Change_Request-20170601, ReqNo.14 - END
						else {
							userParamInByte = tradeGetSession2(AESKey);
						}
						AppConstants.userParamInByte= userParamInByte;
					}else{
						userParamInByte				= AppConstants.userParamInByte;
					}
				}
				// Mary@20121227 - Fixes_Request-20121102, ReqNo.12 - END
				
				byte[] mKey 					= AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData 					= AES256Encryption.encryptData(inputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				
				String base64EncodedUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				String urlAdd 					= "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				/* Mary@20121227 - Fixes_Request-20121102, ReqNo.12
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);
				*/

				/* Mary@20120718 - avoid Malformed ipv6 address error
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[0]GetHintInfo?"+encodedURL;
				DefinitionConstants.Debug("Url address: " +urlAddress);
				URL url = new URL(urlAddress);
				*/
				/* Mary@20121227 - Fixes_Request-20121102, ReqNo.12
				URL url 						= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]GetHintInfo?"+encodedURL);
				*/
				URL url 						= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]GetHintInfo?"+urlAdd);
				DefinitionConstants.Debug("[AtpConnectUtil][getHintInfo]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes 				= readUrlData(url);
				String response 				= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][getHintInfo]response: " + response);
				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";

				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;

				try {
					startIdx	= response.indexOf(BeginData) + BeginData.length();
					endIdx 		= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					
					byte[] decData 	= null;
					try {
						decData = CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][GetHintInfo]Server decrypted response: " + decryptedString);
				
				// Added by Mary@20130529 - Fixes_Request-20130523, ReqNo.4 - START
				if( ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) ){
					AppConstants.setEncryptedKey(null);
					AppConstants.userParamInByte = null;
				}
				// Added by Mary@20130529 - Fixes_Request-20130523, ReqNo.4 - END
					
				return decryptedString;
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}
		
		/*  Mary@20130117 - should throw Exception to avoid unexpected crash
		return null;
		 */
	}

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String getAccountInfo2(Map parameters) throws FailedAuthenicationException {

		String adminKey = (String) parameters.get(ParamConstants.ADMINKEY_TAG);
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);

		try {
			/* Mary@20120718 - avoid Malformed ipv6 address error
			String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
					+ "]GetAccountInfo?+=" + senderCode;

			DefinitionConstants.Debug("Url address: " + urlAddress);


			if(AppConstants.EnableEncryption==false){

				URL url = new URL(urlAddress);
			/
			if(AppConstants.EnableEncryption==false){
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[" + adminKey + "]GetAccountInfo?+=" + senderCode);
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo2]Url address: " + url.toString() );
			// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);

				return response;
			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "+=" + senderCode;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);

				String urlAdd = "10|"+stx+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				DefinitionConstants.Debug("Url address: " + "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo2]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END

				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();	
		}
		return null;
	}
	*/
	public static String getAccountInfo2(Map<String, Object> parameters) throws Exception {
		URL url				= null;
		byte[] dataBytes	= null;
		String response		= null;
		String adminKey 	= (String) parameters.get(ParamConstants.ADMINKEY_TAG);
		String senderCode 	= (String) parameters.get(ParamConstants.SENDERCODE_TAG);

		try {
			/* Mary@20120718 - avoid Malformed ipv6 address error
			String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
					+ "]GetAccountInfo?+=" + senderCode;

			DefinitionConstants.Debug("Url address: " + urlAddress);


			if(AppConstants.EnableEncryption==false){

				URL url = new URL(urlAddress);
			*/
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[" + adminKey + "]GetAccountInfo?+=" + senderCode);
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo2]Url address: " + url.toString() );
			// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes	= readUrlData(url);
				response 	= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo2]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return response;
			}else{
				/* Mary@20121227 - Fixes_Request-20121102, ReqNo.12
				String encryptionKey 		= AppConstants.getEncryptedKey();
				byte[] mKey 				= AES256Encryption.HashSHA256Generator(encryptionKey);
				
				String InputParam 			= "+=" + senderCode;
				byte[] EData 				= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);
				
				String stx 					= Character.toString(startText);
				String etx 					= Character.toString(endText);
				String urlAdd 				= "10|" + stx + Base64.encodeBytes(EData) + etx + "E";
				String encodedURL 			= URLEncoder.encode(urlAdd, AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				DefinitionConstants.Debug("Url address: " + "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				/
				url = new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo2]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END

				dataBytes = readUrlData(url);
				response = new String(dataBytes,AppConstants.STRING_ENCODING);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo2]Server response: " + decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return decryptedString;
				*/
				return getHintInfo(parameters);
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
			adminKey 	= null;
			senderCode 	= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String getAccountInfo(Map parameters) throws FailedAuthenicationException {

		String adminKey = (String) parameters.get(ParamConstants.ADMINKEY_TAG);
		String userName = (String) parameters.get(ParamConstants.USERNAME_TAG);

		try {
			/* Mary@20120718 - avoid Malformed ipv6 address error - START
			String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
					+ "]GetAccountInfo?.=" + userName;

			DefinitionConstants.Debug("Url address: " + urlAddress);

			if(AppConstants.EnableEncryption==false){
				URL url = new URL(urlAddress);
			/
			if( ! AppConstants.EnableEncryption ){
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[" + adminKey + "]GetAccountInfo?.=" + userName);
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo]Url address: " + url.toString() );
			// Mary@20120718 - avoid Malformed ipv6 address error - END

				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);

				return response;
			}else{
				String encryptionKey = tradeGetKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);


				String InputParam = ".=" + userName;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);

				String urlAdd = "10|"+stx+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				DefinitionConstants.Debug("Url address: " + "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}
		return null;
	}
	*/
	public static String getAccountInfo(Map<String, Object> parameters) throws Exception {
		URL url			= null;
		byte[] dataBytes= null;
		String response	= null;
		String adminKey = (String) parameters.get(ParamConstants.ADMINKEY_TAG);
		String userName = (String) parameters.get(ParamConstants.USERNAME_TAG);

		try {
			/* Mary@20120718 - avoid Malformed ipv6 address error - START
			String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
					+ "]GetAccountInfo?.=" + userName;

			DefinitionConstants.Debug("Url address: " + urlAddress);

			if(AppConstants.EnableEncryption==false){
				URL url = new URL(urlAddress);
			*/
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[" + adminKey + "]GetAccountInfo?.=" + userName);
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo]Url address: " + url.toString() );
			// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				dataBytes 	= readUrlData(url);
				response 	= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return response;
			}else{
				/* Mary@20121227 - Fixes_Request-20121102, ReqNo.12
				String encryptionKey 			= tradeGetKey2(tradeGetPK())[0];
				byte[] mKey 					= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam 				= ".=" + userName;
				byte[] EData 					= AES256Encryption.encryptData(InputParam.getBytes(AppConstants.STRING_ENCODING), mKey);
				String encryptedInputParam 		= Base64.encodeBytes(EData);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|" + stx + encryptedInputParam + etx + "E";
				String encodedURL				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);
				
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				DefinitionConstants.Debug("Url address: " + "http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]GetAccountInfo?"+encodedURL);
				/
				url 						= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[" + adminKey + "]GetAccountInfo?"+encodedURL);

				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				String BeginData 			= "10|"+stx;
				String EndData 				= etx+"E";
				int startIdx 				= -1;
				int endIdx					= -1;
				String decryptedString		= null;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= null;
					try {
						decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
					} catch (IOException e) {
						e.printStackTrace();
					}

					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr, AppConstants.STRING_ENCODING);
				}
				DefinitionConstants.Debug("[AtpConnectUtil][getAccountInfo]Server response: " + decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
				*/
				return getHintInfo(parameters);
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
			adminKey 	= null;
			userName 	= null;
		}
		
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradeClientLimit(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}

		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String accountNo = (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String clientCode = (String) parameters.get(ParamConstants.CLIENTCODE_TAG);
		String brokerCode = (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String branchCode = (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);

		try {
			/* Mary@20120718 - avoid Malformed ipv6 address error - START
			String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeClientLimit?'=" 
					+ senderCode + "|.=" + accountNo + "|/=" + clientCode + "|3=" + brokerCode + "|M=" + branchCode
					+ "|P=" + exchangeCode;

			DefinitionConstants.Debug("Url address: " + urlAddress);

			if(AppConstants.EnableEncryption==false){
				URL url = new URL(urlAddress);
			/	
			String strFileName	= "/[" + userParam +"]TradeClientLimit?'=" + senderCode + "|.=" + accountNo + "|/=" + clientCode + "|3=" + brokerCode 
									+ "|M=" + branchCode + "|P=" + exchangeCode;
			if( ! AppConstants.EnableEncryption ){
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClientLimit]Url address: " + url.toString() );
			// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);

				return response; 
			}else{

				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "'=" + senderCode + "|.=" + accountNo + "|/=" + clientCode + "|3=" + brokerCode + "|M=" + branchCode
						+ "|P=" + exchangeCode;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeClientLimit?"+encodedURL);
				DefinitionConstants.Debug("Url address: " + "http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeClientLimit?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[0]TradeClientLimit?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClientLimit]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		} catch (MalformedURLException e) {			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}
		return null;
	}
	*/
	public static String tradeClientLimit(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if(AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode 		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String accountNo 		= (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String clientCode 		= (String) parameters.get(ParamConstants.CLIENTCODE_TAG);
		String brokerCode 		= (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String branchCode 		= (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String exchangeCode 	= (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		
		//Added by Thinzar@20140806 - Change_Request-20140701, ReqNo9 - START
		String settlementMode 	= (String) parameters.get(ParamConstants.PAYMENT_TYPE_TAG);
		String settlementCurrency = (String) parameters.get(ParamConstants.PAYMENT_CURRENCY_TAG);
		//Added by Thinzar@20140806 - END

		try {
			/* Mary@20120718 - avoid Malformed ipv6 address error - START
			String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeClientLimit?'=" 
					+ senderCode + "|.=" + accountNo + "|/=" + clientCode + "|3=" + brokerCode + "|M=" + branchCode
					+ "|P=" + exchangeCode;

			DefinitionConstants.Debug("Url address: " + urlAddress);

			if(AppConstants.EnableEncryption==false){
				URL url = new URL(urlAddress);
			*/	
			/*Commented out by Thinzar@20140806
			String strFileName	= "/[" + userParam +"]TradeClientLimit?'=" + senderCode + "|.=" + accountNo + "|/=" + clientCode + "|3=" + brokerCode 
									+ "|M=" + branchCode + "|P=" + exchangeCode;
			 */
			String strFileName	= "/[" + userParam +"]TradeClientLimit?'=" + senderCode + "|.=" + accountNo + "|/=" + clientCode + "|3=" + brokerCode
					+ "|M=" + branchCode + "|P=" + exchangeCode;
			
			//Added by Thinzar@20140806 - Change_Request-20140701, ReqNo 9 - START
			if(settlementMode != null && settlementMode.equalsIgnoreCase("CUT")){
				strFileName += "|"+ FormatUtil.convertAsciiToChar(137) + "=" + settlementMode +"|" + FormatUtil.convertAsciiToChar(138) + "=" + settlementCurrency;
			}
			//Added by Thinzar@20140806 - END
			
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				DefinitionConstants.Debug("strFileName:" + strFileName);
				url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClientLimit]Url address: " + url.toString() );
			// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes	 = readUrlData(url);
				response 	= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClientLimit]Server response: " + response);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response; 
			}else{
				String encryptionKey		= AppConstants.getEncryptedKey();
				byte[] mKey 				= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam 			= "'=" + senderCode + "|.=" + accountNo + "|/=" + clientCode + "|3=" + brokerCode + "|M=" + branchCode
												+ "|P=" + exchangeCode;
				
				//Added by Thinzar@20140806 - Change_Request-20140701, ReqNo 9 - START
				if(settlementMode != null && settlementMode.equalsIgnoreCase("CUT")){
					InputParam += "|"+ FormatUtil.convertAsciiToChar(137) + "=" + settlementMode +"|" + FormatUtil.convertAsciiToChar(138) + "=" + settlementCurrency;
				}
				//Added by Thinzar@20140806 - END
				
				byte[] EData 				= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam	= CustomizedBase64.encodeBytes(EData);
				
				String base64EncodedUserParam = CustomizedBase64.encodeBytes(userParamInByte);
				String stx = Character.toString(startText);
				String etx = Character.toString(endText);
				
				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeClientLimit?"+encodedURL);
				DefinitionConstants.Debug("Url address: " + "http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeClientLimit?"+encodedURL);
				*/
				url = new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradeClientLimit?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClientLimit]InputParam: " + InputParam );
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClientLimit]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				dataBytes = readUrlData(url);
				response = new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				
				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				/* Mary@20130717 - Fixes_Request-20130108, ReqNo.16
				String decryptedString=null;
				*/
				String decryptedString			= response;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClientLimit]Server response: " + decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam 		= null;
			userParamInByte = null;
			senderCode 		= null;
			accountNo 		= null;
			clientCode 		= null;
			brokerCode 		= null;
			branchCode 		= null;
			exchangeCode 	= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static void deleteFavListStock(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}

		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId = (String) parameters.get(ParamConstants.FAVORATEID_TAG);
		String symbolCode = (String) parameters.get(ParamConstants.SYMBOLCODE_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		String repexchgCode=null;

		if(exchangeCode.contains("D")){
			exchangeCode = exchangeCode.substring(0,2);
		}

		try {

			if(AppConstants.EnableEncryption==false){

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=DelFavListStk|+=" + senderCode + "|-=" + favorateId 
						+ "|1=" + symbolCode + "|2=" + exchangeCode;

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				String strFileName	= "/[" + userParam + "]Favorate?*=DelFavListStk|+=" + senderCode + "|-=" + favorateId 
										+ "|1=" + symbolCode + "|2=" + exchangeCode;
				URL url 			= new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavListStock]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END

				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);
			}else{

				String encryptionKey = tradeGetKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);


				String InputParam = "*=DelFavListStk|+=" + senderCode + "|-=" + favorateId 
						+ "|1=" + symbolCode + "|2=" + exchangeCode;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);
				
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = null;

				url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeForgetPasswd?"+encodedURL);

				DefinitionConstants.Debug("Url address: " + "http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeForgetPasswd?"+encodedURL);
				 /
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/[0]TradeForgetPasswd?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavListStock]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}    	
	}
	*/
	public static boolean deleteFavListStock(Map<String, Object> parameters) throws Exception{
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte	= null;

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode 		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId 		= (String) parameters.get(ParamConstants.FAVORATEID_TAG);
		String symbolCode 		= (String) parameters.get(ParamConstants.SYMBOLCODE_TAG);
		String exchangeCode 	= (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);

		if( exchangeCode.contains("D") )
			exchangeCode = exchangeCode.substring(0,2);

		try{
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=DelFavListStk|+=" + senderCode + "|-=" + favorateId 
						+ "|1=" + symbolCode + "|2=" + exchangeCode;

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/
				String strFileName	= "/[" + userParam + "]Favorate?*=DelFavListStk|+=" + senderCode + "|-=" + favorateId 
										+ "|1=" + symbolCode + "|2=" + exchangeCode;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavListStock]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavListStock]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			}else{
				/* Mary@20121226-Fixes_Request-20121102, ReqNo.8
				String encryptionKey 			= tradeGetKey();
				*/
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				
				String InputParam 				= "*=DelFavListStk|+=" + senderCode + "|-=" + favorateId + "|1=" + symbolCode + "|2=" + exchangeCode;
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
			
				String stx						= Character.toString(startText);
				String etx						= Character.toString(endText);
				
				String urlAdd 					= "10|" + stx + CustomizedBase64.encodeBytes(userParamInByte) + "|" + CustomizedBase64.encodeBytes(EData) + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd, AppConstants.STRING_ENCODING_FORMAT);
				
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = null;

				url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeForgetPasswd?"+encodedURL);

				DefinitionConstants.Debug("Url address: " + "http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeForgetPasswd?"+encodedURL);
				 */
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavListStock]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavListStock]response: " + response);
				
				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData 				= "10|" + stx;
				String EndData					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavListStock]decrypted response: " + decryptedString);
				
				/* Mary@20130719 - Fixes_Request-20121102,ReqNo.8
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
				if(decryptedString == null)
					return true;
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
				*/
				if(decryptedString != null){
					if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
						throw new FailedAuthenicationException(decryptedString);
					else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
						throw new Exception(decryptedString);
					else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
						throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				}
			}
			
			return true;
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			response		= null;
			userParam 		= null;
			userParamInByte	= null;
			senderCode 		= null;
			favorateId 		= null;
			symbolCode 		= null;
			exchangeCode 	= null;
		}
		
		return false;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradeChgPasswd(Map parameters, int passwordType) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String username = (String) parameters.get(ParamConstants.USERNAME_TAG);
		String oldPassword = (String) parameters.get(ParamConstants.OLD_PASSWORD_TAG);
		String newPassword = (String) parameters.get(ParamConstants.NEW_PASSWORD_TAG);

		String EncryptedUsername = encryptATPString(username);
		String EncryptedOldPassword = encryptATPString(oldPassword);
		String EncryptedNewPassword = encryptATPString(newPassword);

		String urlAddress = null;

		/* Mary@20120718 - avoid Malformed ipv6 address error - START
		if (passwordType == AppConstants.CHANGE_PASSWORD_TYPE) {

			urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
					+ "]TradeChgPasswd?'=" + senderCode + "|*=" + username 
					+ "|+=" + oldPassword + "|,=" + newPassword;
		} else if (passwordType == AppConstants.CHANGE_PIN_TYPE) {
			urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
					+ "]TradeChgPIN?'=" + senderCode + "|*=" + username 
					+ "|-=" + oldPassword + "|.=" + newPassword;
		}
		/
		if (passwordType == AppConstants.CHANGE_PASSWORD_TYPE) {
			urlAddress = "/[" + userParam + "]TradeChgPasswd?'=" + senderCode + "|*=" + username + "|+=" + oldPassword + "|,=" + newPassword;
		} else if (passwordType == AppConstants.CHANGE_PIN_TYPE) {
			urlAddress = "/[" + userParam + "]TradeChgPIN?'=" + senderCode + "|*=" + username + "|-=" + oldPassword + "|.=" + newPassword;
		}
		// Mary@20120718 - avoid Malformed ipv6 address error - END

		try {

			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeChgPasswd]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END

				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);

				return response;
			}else{

				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;


				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam;

				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					InputParam = "'=" + senderCode + "|*=" + EncryptedUsername + "|+=" + EncryptedOldPassword + "|,=" + EncryptedNewPassword;
				}else{
					InputParam = "'=" + senderCode + "|*=" + EncryptedUsername + "|-=" + EncryptedOldPassword + "|.=" + EncryptedNewPassword;
				}

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = null;

				if (passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeChgPasswd?"+encodedURL);

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeChgPasswd?"+encodedURL);

				}else if (passwordType == AppConstants.CHANGE_PIN_TYPE) {
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeChgPIN?"+encodedURL);

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeChgPIN?"+encodedURL);
				}
				/
				String strFileName = "";
				if (passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
					strFileName = "/[0]TradeChgPasswd?"+encodedURL;
				else if (passwordType == AppConstants.CHANGE_PIN_TYPE)
					strFileName = "/[0]TradeChgPIN?"+encodedURL;
				
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeChgPasswd]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END	

				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END			
		}    	
		return null;
	}
	*/
	public static String tradeChgPasswd(Map<String, Object> parameters, int intPwdType) throws Exception {
		URL url						= null;
		byte[] dataBytes			= null;
		String response				= null;
		String userParam			= null;
		byte[] userParamInByte		= null;
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if( AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode			= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String username 			= (String) parameters.get(ParamConstants.USERNAME_TAG);
		String oldPassword 			= (String) parameters.get(ParamConstants.OLD_PASSWORD_TAG);
		String newPassword 			= (String) parameters.get(ParamConstants.NEW_PASSWORD_TAG);
		
		String EncryptedUsername 	= encryptATPString(username);
		String EncryptedOldPassword = encryptATPString(oldPassword);
		String EncryptedNewPassword = encryptATPString(newPassword);

		try{
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				String urlAddress 			= null;
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				if (passwordType == AppConstants.CHANGE_PASSWORD_TYPE) {

					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeChgPasswd?'=" + senderCode + "|*=" + username 
							+ "|+=" + oldPassword + "|,=" + newPassword;
				} else if (passwordType == AppConstants.CHANGE_PIN_TYPE) {
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeChgPIN?'=" + senderCode + "|*=" + username 
							+ "|-=" + oldPassword + "|.=" + newPassword;
				}
				
				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/
				/* Mary@20130102 - Fixes_Request-20121102, ReqNo.11 - START
				if (intPwdType == AppConstants.CHANGE_PASSWORD_TYPE)
					urlAddress	= "/[" + userParam + "]TradeChgPasswd?'=" + senderCode + "|*=" + username + "|+=" + oldPassword + "|,=" + newPassword;
				else if (intPwdType == AppConstants.CHANGE_PIN_TYPE)
					urlAddress	= "/[" + userParam + "]TradeChgPIN?'=" + senderCode + "|*=" + username + "|-=" + oldPassword + "|.=" + newPassword;
				*/
				if (intPwdType == AppConstants.CHANGE_PASSWORD_TYPE)
					urlAddress	= "/[" + userParam + "]TradeChgPasswd?" + URLEncoder.encode("'=" + senderCode + "|*=" + username + "|+=" + oldPassword + "|,=" + newPassword, AppConstants.STRING_ENCODING_FORMAT);
				else if (intPwdType == AppConstants.CHANGE_PIN_TYPE)
					urlAddress	= "/[" + userParam + "]TradeChgPIN?" + URLEncoder.encode("'=" + senderCode + "|*=" + username + "|-=" + oldPassword + "|.=" + newPassword, AppConstants.STRING_ENCODING_FORMAT);
				// Mary@20130102 - Fixes_Request-20121102, ReqNo.11 - END
				
				url			= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeChgPasswd]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes				= readUrlData(url);
				response 				= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeChgPasswd]Server response: " + response);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String encryptionKey			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);

				String InputParam;
				if(intPwdType == AppConstants.CHANGE_PASSWORD_TYPE)
					InputParam = "'=" + senderCode + "|*=" + EncryptedUsername + "|+=" + EncryptedOldPassword + "|,=" + EncryptedNewPassword;
				else
					InputParam = "'=" + senderCode + "|*=" + EncryptedUsername + "|-=" + EncryptedOldPassword + "|.=" + EncryptedNewPassword;
				
				byte[] EData 					= AES256Encryption.encryptData(InputParam.getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				
				String urlAdd 					= "10|" + stx + CustomizedBase64.encodeBytes(userParamInByte) + "|" + CustomizedBase64.encodeBytes(EData) + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = null;

				if (passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeChgPasswd?"+encodedURL);

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeChgPasswd?"+encodedURL);

				}else if (passwordType == AppConstants.CHANGE_PIN_TYPE) {
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeChgPIN?"+encodedURL);

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeChgPIN?"+encodedURL);
				}
				*/
				String strFileName 				= "";
				if (intPwdType == AppConstants.CHANGE_PASSWORD_TYPE)
					strFileName = "/[0]TradeChgPasswd?"+encodedURL;
				else if (intPwdType == AppConstants.CHANGE_PIN_TYPE)
					strFileName = "/[0]TradeChgPIN?"+encodedURL;
				
				url 						= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeChgPasswd]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END	
				
				dataBytes 					= readUrlData(url);
				response 					= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeChgPasswd]response : " + response );
				
				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData 				= "10|"+stx;
				String EndData 					= etx+"E";
				int startIdx 					= -1;
				int endIdx						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try{
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				}catch (Exception e){
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradeChgPasswd]Server response: " + decryptedString);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(decryptedString);
					//throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS); //Edited by Thinzar, Fixes_Request-20151001-ReqNo.1 - START
					
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
			/*Commented out by Thinzar, Fixes_Request-20151001-ReqNo.1 - START
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
				*/
		}finally{
			url					= null;
			dataBytes			= null;
			userParam			= null;
			userParamInByte		= null;
			senderCode			= null;
			username 			= null;
			oldPassword 		= null;
			newPassword 		= null;
			EncryptedUsername 	= null;
			EncryptedOldPassword= null;
			EncryptedNewPassword= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String RegisterMail(Map parameters, int passwordType) throws FailedAuthenicationException{

		String atpAdminKey = AppConstants.ADMIN_KEY;
		String mailtype = "P";
		String messageType = "E";
		String templateId = null;

		if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
			templateId = "20";
		}else if(passwordType == AppConstants.CHANGE_PIN_TYPE){
			templateId = "30";
		}

		String NewPasswordOrPin = (String) parameters.get(ParamConstants.TRADEFORGET_NEWPWD_TAG);
		String UserEmail = (String)parameters.get(ParamConstants.HINT_EMAIL_TAG);
		/* Mary@20120718 - avoid Malformed ipv6 address error - START
		String urlAddress = "http://"+AppConstants.ATP_SERVER_IP+"/["+atpAdminKey+"]RegisterMail?+="+mailtype+"|,="+messageType+
				"|/="+templateId+"|0="+NewPasswordOrPin+"^|.="+UserEmail;
		/
		String urlAddress = "/["+atpAdminKey+"]RegisterMail?+="+mailtype+"|,="+messageType+"|/="+templateId+"|0="+NewPasswordOrPin+"^|.="+UserEmail;
		// Mary@20120718 - avoid Malformed ipv6 address error - END

		try {

			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][RegisterMail]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);

				return response;
			}else{
				String encryptionKey = tradeGetKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "+="+mailtype+"|,="+messageType+"|/="+templateId+"|0="+NewPasswordOrPin+"^|.="+UserEmail;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);

				String urlAdd = "10|"+stx+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error
				URL url = new URL("http://"+AppConstants.ATP_SERVER_IP+"/["+atpAdminKey+"]RegisterMail?"+encodedURL);

				DefinitionConstants.Debug("Url address: " + "http://"+AppConstants.ATP_SERVER_IP+"/["+atpAdminKey+"]RegisterMail?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, "/["+atpAdminKey+"]RegisterMail?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][RegisterMail]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END			
		}    	

		return null;
	}
	*/
	public static String RegisterMail(Map<String, String> parameters, int passwordType) throws Exception{
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String atpAdminKey		= AppConstants.ADMIN_KEY;
		String mailtype 		= "P";
		String messageType 		= "E";
		String templateId 		= null;

		if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
			templateId = "20";
		else if(passwordType == AppConstants.CHANGE_PIN_TYPE)
			templateId = "30";

		String NewPasswordOrPin = parameters.get(ParamConstants.TRADEFORGET_NEWPWD_TAG);
		String UserEmail 		= parameters.get(ParamConstants.HINT_EMAIL_TAG);
		/* Mary@20120718 - avoid Malformed ipv6 address error - START
		String urlAddress = "http://"+AppConstants.ATP_SERVER_IP+"/["+atpAdminKey+"]RegisterMail?+="+mailtype+"|,="+messageType+
				"|/="+templateId+"|0="+NewPasswordOrPin+"^|.="+UserEmail;
		*/
		String urlAddress		= "/["+atpAdminKey+"]RegisterMail?+="+mailtype+"|,="+messageType+"|/="+templateId+"|0="+NewPasswordOrPin+"^|.="+UserEmail;
		// Mary@20120718 - avoid Malformed ipv6 address error - END

		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/
				url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][RegisterMail]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 	= readUrlData(url);
				response 	= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][RegisterMail]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return response;
			}else{
				/* Mary@20130726-Fixes_Request-20121102, ReqNo.8
				String encryptionKey 		= tradeGetKey();
				*/
				String encryptionKey 		= AppConstants.getEncryptedKey();
				String stx 					= Character.toString(startText);
				String etx 					= Character.toString(endText);
				String InputParam 			= "+=" + mailtype + "|,=" + messageType + "|/=" + templateId + "|0=" + NewPasswordOrPin + "^|.=" + UserEmail;
				byte[] mKey					= AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData 				= AES256Encryption.encryptData(InputParam.getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				/* Mary@20130726 - Fixes_Request-201300711, ReqNo.10 - START
				String encryptedInputParam 	= CustomizedBase64.encodeBytes(EData);
				String urlAdd 				= "10|" + stx + encryptedInputParam + etx + "E";
				*/
				String urlAdd 				= "10|" + stx + CustomizedBase64.encodeBytes(AppConstants.userParamInByte) + "|" + CustomizedBase64.encodeBytes(EData) + etx + "E";
				// Mary@20130726 - Fixes_Request-201300711, ReqNo.10 - END
				String encodedURL 			= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				/* Mary@20120718 - avoid Malformed ipv6 address error
				URL url = new URL("http://"+AppConstants.ATP_SERVER_IP+"/["+atpAdminKey+"]RegisterMail?"+encodedURL);

				DefinitionConstants.Debug("Url address: " + "http://"+AppConstants.ATP_SERVER_IP+"/["+atpAdminKey+"]RegisterMail?"+encodedURL);
				*/
				url 					= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/["+atpAdminKey+"]RegisterMail?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][RegisterMail]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 				= readUrlData(url);
				response 				= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][RegisterMail]Server response: " + response);

				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData		= "10|"+stx;
				String EndData 			= etx+"E";
				int startIdx 			= -1;
				int endIdx 				= -1;
				/* Mary@20130726 - Fixes_Request-20130108, ReqNo.16
				String decryptedString	= null;
				*/
				String decryptedString	= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				}catch (Exception e){
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData	= response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][RegisterMail]Server decrypted response: " + decryptedString);
				
				/* Mary@20130726 - Fixes_Request-20121102,ReqNo.8
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
				if(decryptedString == null)
					return null;
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
				*/
				if(decryptedString != null){
				// Mary@20130726 - Fixes_Request-20121102,ReqNo.8
					if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
						throw new FailedAuthenicationException(decryptedString);
					else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
						throw new Exception(decryptedString);
					else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
						throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				}
					
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			atpAdminKey		= null;
			mailtype 		= null;
			messageType 	= null;
			templateId 		= null;
			NewPasswordOrPin= null;
			UserEmail 		= null;
			urlAddress		= null;
		}

		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END


	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradeForgetPasswd2(Map parameters, int passwordType)
			throws FailedAuthenicationException{

		String username = (String) parameters.get(ParamConstants.USERNAME_TAG);
		String adminKey = (String) parameters.get(ParamConstants.ADMINKEY_TAG);
		String hintQuestion = (String) parameters.get(ParamConstants.HINT_QUESTION_TAG);
		String hintAnswer = (String) parameters.get(ParamConstants.HINT_ANSWER_TAG);

		try {

			String urlAddress = null;

			if(AppConstants.EnableEncryption==false){

				String userParam = (String)parameters.get(ParamConstants.USERPARAM_TAG);
				
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeForgetPasswd2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0";

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeForgetPasswd2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0" );

				}else if(passwordType == AppConstants.CHANGE_PIN_TYPE){
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeForgetPIN2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0";

					DefinitionConstants.Debug("http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeForgetPIN2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0");
				}

				URL url = new URL(urlAddress);
				/
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
					urlAddress = "/[" + userParam + "]TradeForgetPasswd2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0";
				else if(passwordType == AppConstants.CHANGE_PIN_TYPE)
					urlAddress = "/[" + userParam + "]TradeForgetPIN2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0";
				
				URL url = new URL("http", AppConstants.ATP_SERVER_IP,  AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd2]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);

				DefinitionConstants.Debug("Server response: "+response);

				return response;
			}else{

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;
				byte[] userParamInByte = (byte[])parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);

				String inputParam = "*="+username+"|,="+hintQuestion+"|-="+hintAnswer+"|.=0";

				byte[] EData = AES256Encryption.encryptData(inputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeForgetPasswd2?"+encodedURL;

					DefinitionConstants.Debug("Url address: "+urlAddress);

				}else if(passwordType == AppConstants.CHANGE_PIN_TYPE){
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeForgetPIN2?"+encodedURL;

					DefinitionConstants.Debug("Url address: "+urlAddress);
				}

				URL url = new URL(urlAddress);
				/
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
					urlAddress = "/[0]TradeForgetPasswd2?"+encodedURL;
				else if(passwordType == AppConstants.CHANGE_PIN_TYPE)
					urlAddress = "/[0]TradeForgetPIN2?"+encodedURL;
				
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd2]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				if(response.indexOf("10")!=-1){

					try {
						startIdx = response.indexOf(BeginData) + BeginData.length();
						endIdx = response.indexOf(EndData);
					} catch (Exception e) {
						e.printStackTrace();
					}

					if (startIdx != -1 && endIdx != -1) {
						String sSubData = response.substring(startIdx, endIdx);

						byte[] decData = null;

						try {
							decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						byte[] arr = AES256Encryption.deencryptData(decData, mKey);

						decryptedString = new String(arr,AppConstants.STRING_ENCODING);

					}

					DefinitionConstants.Debug("Server response: "+decryptedString);

					return decryptedString;
				}else{
					return response;
				}
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END			
		}

		return null;
	}
	*/
	public static String tradeForgetPasswd2(Map<String, Object> parameters, int passwordType) throws Exception{
		URL url				= null;
		byte[] dataBytes	= null;
		String response		= null;
		String urlAddress 	= null;
		String username 	= (String) parameters.get(ParamConstants.USERNAME_TAG);
		/* Mary@20130506 - comment unused variable
		String adminKey 	= (String) parameters.get(ParamConstants.ADMINKEY_TAG);
		*/
		String hintQuestion = (String) parameters.get(ParamConstants.HINT_QUESTION_TAG);
		String hintAnswer 	= (String) parameters.get(ParamConstants.HINT_ANSWER_TAG);

		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				String userParam = (String)parameters.get(ParamConstants.USERPARAM_TAG);
	
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeForgetPasswd2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0";

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeForgetPasswd2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0" );

				}else if(passwordType == AppConstants.CHANGE_PIN_TYPE){
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeForgetPIN2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0";

					DefinitionConstants.Debug("http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
							+ "]TradeForgetPIN2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0");
				}

				URL url = new URL(urlAddress);
				*/
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
					urlAddress = "/[" + userParam + "]TradeForgetPasswd2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0";
				else if(passwordType == AppConstants.CHANGE_PIN_TYPE)
					urlAddress = "/[" + userParam + "]TradeForgetPIN2?*="+username+",="+hintQuestion+"-="+hintAnswer+".=0";
				
				url			= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd2]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 	= readUrlData(url);
				response 	= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd2]Server response: "+response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return response;
			}else{
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String encryptionKey 			= AppConstants.getEncryptedKey();
				/* Mary@20120103 - Fixes_Request-20121102, ReqNo.12 - START
				byte[] userParamInByte 			= (byte[])parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
				*/
				byte[] userParamInByte			= null;
				if( parameters.get(ParamConstants.USERPARAM_INBYTE_TAG) != null )
					userParamInByte 			= (byte[])parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
				else
					userParamInByte				= AppConstants.userParamInByte;
				// Mary@20120103 - Fixes_Request-20121102, ReqNo.12 - END
				
				byte[] mKey 					= AES256Encryption.HashSHA256Generator(encryptionKey);
				String inputParam 				= "*=" + username + "|,=" + hintQuestion + "|-=" + hintAnswer + "|.=0";
				byte[] EData 					= AES256Encryption.encryptData(inputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncodedUserParam	= CustomizedBase64.encodeBytes(userParamInByte);
				String urlAdd 					= "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeForgetPasswd2?"+encodedURL;

					DefinitionConstants.Debug("Url address: "+urlAddress);

				}else if(passwordType == AppConstants.CHANGE_PIN_TYPE){
					urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeForgetPIN2?"+encodedURL;

					DefinitionConstants.Debug("Url address: "+urlAddress);
				}

				URL url = new URL(urlAddress);
				*/
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
					urlAddress = "/[0]TradeForgetPasswd2?"+encodedURL;
				else if(passwordType == AppConstants.CHANGE_PIN_TYPE)
					urlAddress = "/[0]TradeForgetPIN2?"+encodedURL;
				
				url 						= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd2]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 					= readUrlData(url);
				response 					= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				
				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData				= "10|" + stx;
				String EndData					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;

				if(response.indexOf("10")!=-1){
					try{
						startIdx= response.indexOf(BeginData) + BeginData.length();
						endIdx 	= response.indexOf(EndData);
					}catch (Exception e){
						e.printStackTrace();
					}

					if(startIdx != -1 && endIdx != -1){
						String sSubData = response.substring(startIdx, endIdx);
						byte[] decData 	= null;
						try {
							decData = CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
						decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
						// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
						decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
					}
					DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd2]Server response: "+decryptedString);

					if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
						throw new FailedAuthenicationException(decryptedString);
					else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
						throw new Exception(decryptedString);
					
					return decryptedString;
				}else{					
					return response;
				}
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
			urlAddress 	= null;
			username 	= null;
			/* Mary@20130506 - comment unused variable
			adminKey 	= null;
			*/
			hintQuestion= null;
			hintAnswer 	= null;
		}

		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradeForgetPasswd(Map parameters, int passwordType) 
			throws FailedAuthenicationException {

		String username = (String) parameters.get(ParamConstants.USERNAME_TAG);
		String adminKey = (String) parameters.get(ParamConstants.ADMINKEY_TAG);
		String hintQuestion = (String) parameters.get(ParamConstants.HINT_QUESTION_TAG);
		String hintAnswer = (String) parameters.get(ParamConstants.HINT_ANSWER_TAG);
		String hintEmail = (String) parameters.get(ParamConstants.HINT_EMAIL_TAG);

		try {

			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = null;
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]TradeForgetPasswd");

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]TradeForgetPasswd");

				}else if(passwordType == AppConstants.CHANGE_PIN_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]TradeForgetPIN");

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]TradeForgetPIN"); 
				}
				/
				String strFileName	= "";
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
					strFileName ="/[" + adminKey + "]TradeForgetPasswd";
				else if(passwordType == AppConstants.CHANGE_PIN_TYPE)
					strFileName ="/[" + adminKey + "]TradeForgetPIN";
				
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				URLConnection connection = url.openConnection();
				HttpURLConnection httpPost = (HttpURLConnection) connection;
				httpPost.setDoInput(true);
				httpPost.setDoOutput(true);
				httpPost.setRequestMethod("POST");
				httpPost.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				DataOutputStream dos = new DataOutputStream(httpPost.getOutputStream());
				String output = "Request=*=" + username + "|+=" + hintEmail + "|,=" + hintQuestion
						+ "|-=" + hintAnswer + "|.=0";
				
				byte[] bytes = output.getBytes();
				dos.write(bytes); 
				dos.flush();
				dos.close();

				byte[] dataBytes = null;
				int length = 0;
				int size = 1024;

				InputStream inputStream = httpPost.getInputStream();

				if (inputStream instanceof ByteArrayInputStream) {
					size = inputStream.available();
					dataBytes = new byte[size];
					length = inputStream.read(dataBytes, 0, size);
				} else {
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					dataBytes = new byte[size];
					while ((length = inputStream.read(dataBytes, 0, size)) != -1) {
						outputStream.write(dataBytes, 0, length);
					}
					dataBytes = outputStream.toByteArray();
				}
				inputStream.close();

				DefinitionConstants.Debug(new String(dataBytes, AppConstants.STRING_ENCODING));

				return new String(dataBytes, AppConstants.STRING_ENCODING);
			}else{
				String encryptionKey = tradeGetKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);


				String InputParam = "*=" + username + "|+=" + hintEmail + "|,=" + hintQuestion + "|-=" + hintAnswer + "|.=0";

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);

				String urlAdd = "10|"+stx+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = null;

				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]TradeForgetPasswd?"+encodedURL);

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]TradeForgetPasswd?"+encodedURL);

				}else if(passwordType == AppConstants.CHANGE_PIN_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]TradeForgetPIN?"+encodedURL);

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]TradeForgetPIN?"+encodedURL);
				}
				/
				String strFileName	= "";
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
					strFileName = "/[" + adminKey + "]TradeForgetPasswd?"+encodedURL;
				else if(passwordType == AppConstants.CHANGE_PIN_TYPE)
					strFileName = "/[" + adminKey + "]TradeForgetPIN?"+encodedURL;

				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END

				byte[] dataBytes = readUrlData(url);

				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String decryptedString=null;

				if(response.indexOf("10")!=-1){

					String BeginData = "10|"+stx;
					String EndData = etx+"E";

					int startIdx = -1;
					int endIdx = -1;

					try {
						startIdx = response.indexOf(BeginData) + BeginData.length();
						endIdx = response.indexOf(EndData);
					} catch (Exception e) {
						e.printStackTrace();
					}

					if (startIdx != -1 && endIdx != -1) {
						String sSubData = response.substring(startIdx, endIdx);

						byte[] decData = null;

						decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

						byte[] arr = AES256Encryption.deencryptData(decData, mKey);

						decryptedString = new String(arr,AppConstants.STRING_ENCODING);

					}

					DefinitionConstants.Debug("Server response: " + decryptedString);

					return decryptedString;
				}else{
					return response;
				}
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		} 
		return null;
	}
	*/
	public static String tradeForgetPasswd(Map<String, Object> parameters, int passwordType) throws Exception {
		URL url						= null;
		byte[] dataBytes			= null;
		String response				= null;
		URLConnection connection	= null;
		HttpURLConnection httpPost	= null;
		String username				= (String)parameters.get(ParamConstants.USERNAME_TAG);
		String adminKey 			= (String)parameters.get(ParamConstants.ADMINKEY_TAG);
		String hintQuestion 		= (String)parameters.get(ParamConstants.HINT_QUESTION_TAG);
		String hintAnswer 			= (String)parameters.get(ParamConstants.HINT_ANSWER_TAG);
		String hintEmail 			= (String)parameters.get(ParamConstants.HINT_EMAIL_TAG);

		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = null;
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]TradeForgetPasswd");

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]TradeForgetPasswd");

				}else if(passwordType == AppConstants.CHANGE_PIN_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]TradeForgetPIN");

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey 
							+ "]TradeForgetPIN"); 
				}
				*/
				String strFileName			= "";
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
					strFileName ="/[" + adminKey + "]TradeForgetPasswd";
				else if(passwordType == AppConstants.CHANGE_PIN_TYPE)
					strFileName ="/[" + adminKey + "]TradeForgetPIN";
				
				url 					= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				connection 					= url.openConnection();
				httpPost					= (HttpURLConnection) connection;
				httpPost.setDoInput(true);
				httpPost.setDoOutput(true);
				httpPost.setRequestMethod("POST");
				httpPost.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				DataOutputStream dos 		= new DataOutputStream(httpPost.getOutputStream());
				String output 				= "Request=*=" + username + "|+=" + hintEmail + "|,=" + hintQuestion
												+ "|-=" + hintAnswer + "|.=0";
				
				byte[] bytes 				= output.getBytes();
				dos.write(bytes); 
				dos.flush();
				dos.close();

				int length 					= 0;
				int size					= 1024;
				InputStream inputStream 	= httpPost.getInputStream();
				if(inputStream instanceof ByteArrayInputStream){
					size		= inputStream.available();
					dataBytes 	= new byte[size];
					length 		= inputStream.read(dataBytes, 0, size);
				}else{
					ByteArrayOutputStream outputStream	= new ByteArrayOutputStream();
					dataBytes 							= new byte[size];
					while ( ( length = inputStream.read(dataBytes, 0, size) ) != -1 ) {
						outputStream.write(dataBytes, 0, length);
					}
					dataBytes 							= outputStream.toByteArray();
				}
				inputStream.close();
				
				response					= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug( "[AtpConnectUtil][tradeForgetPasswd]Response : " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return response;
			}else{
				/* Mary@20130103 - Fixes_Request-20121102, ReqNo.12
				String encryptionKey 		= tradeGetKey();
				byte[] mKey					= AES256Encryption.HashSHA256Generator(encryptionKey);
				String stx 					= Character.toString(startText);
				String etx					= Character.toString(endText);
				String InputParam 			= "*=" + username + "|+=" + hintEmail + "|,=" + hintQuestion + "|-=" + hintAnswer + "|.=0";
				byte[] EData 				= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);
				String encryptedInputParam 	= Base64.encodeBytes(EData);
				String urlAdd				= "10|" + stx + encryptedInputParam + etx +"E";
				String encodedURL 			= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = null;

				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]TradeForgetPasswd?"+encodedURL);

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]TradeForgetPasswd?"+encodedURL);

				}else if(passwordType == AppConstants.CHANGE_PIN_TYPE){
					url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]TradeForgetPIN?"+encodedURL);

					DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[" + adminKey + "]TradeForgetPIN?"+encodedURL);
				}
				/
				String strFileName			= "";
				if(passwordType == AppConstants.CHANGE_PASSWORD_TYPE)
					strFileName = "/[" + adminKey + "]TradeForgetPasswd?"+encodedURL;
				else if(passwordType == AppConstants.CHANGE_PIN_TYPE)
					strFileName = "/[" + adminKey + "]TradeForgetPIN?"+encodedURL;

				url 						= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes					= readUrlData(url);
				response 					= new String(dataBytes,AppConstants.STRING_ENCODING);

				String decryptedString		= null;
				if( response.indexOf("10") != -1 ){
					String BeginData= "10|" + stx;
					String EndData	= etx + "E";
					int startIdx 	= -1;
					int endIdx 		= -1;

					try {
						startIdx= response.indexOf(BeginData) + BeginData.length();
						endIdx	= response.indexOf(EndData);
					} catch (Exception e) {
						e.printStackTrace();
					}

					if (startIdx != -1 && endIdx != -1) {
						String sSubData = response.substring(startIdx, endIdx);
						byte[] decData 	= Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));
						byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
						decryptedString = new String(arr,AppConstants.STRING_ENCODING);
					}
					DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd]Server response: " + decryptedString);

					if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
						throw new FailedAuthenicationException(decryptedString);
					else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
						throw new Exception(decryptedString);
					else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
						throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
					
					return decryptedString;
				}else{
					DefinitionConstants.Debug("[AtpConnectUtil][tradeForgetPasswd]Server response: " + response);

					if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
						throw new FailedAuthenicationException(response);
					else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
						throw new Exception(response);
					else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
						throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
					
					return response;
				}
				*/
				return tradeForgetPasswd2(parameters, passwordType);
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			if(httpPost != null){
				httpPost.disconnect();
				httpPost	= null;
			}
			
			if(connection != null)
				connection = null;
			
			url			= null;
			dataBytes	= null;
			username	= null;
			adminKey 	= null;
			hintQuestion= null;
			hintAnswer 	= null;
			hintEmail 	= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static void deleteFavList(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId = (String) parameters.get(ParamConstants.FAVORATEID_TAG);

		try {

			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=DelFavList|+=" + senderCode + "|-=" + favorateId;

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				String strFileName	= "/[" + userParam + "]Favorate?*=DelFavList|+=" + senderCode + "|-=" + favorateId;
				URL url 			= new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavList]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);
			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "*=DelFavList|+=" + senderCode + "|-=" + favorateId;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT,  "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavList]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 

	}
	*/
	public static boolean deleteFavList(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;
		
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId		= (String) parameters.get(ParamConstants.FAVORATEID_TAG);

		try{
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=DelFavList|+=" + senderCode + "|-=" + favorateId;

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/
				String strFileName	= "/[" + userParam + "]Favorate?*=DelFavList|+=" + senderCode + "|-=" + favorateId;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavList]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes			= readUrlData(url);
				response			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavList]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam 				= "*=DelFavList|+=" + senderCode + "|-=" + favorateId;
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam		= CustomizedBase64.encodeBytes(EData);
				String base64EncodedUserParam	= CustomizedBase64.encodeBytes(userParamInByte);
				
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|" + stx + base64EncodedUserParam + "|" + encryptedInputParam + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				*/
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(),  "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavList]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavList]response: " + response);
				
				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData				= "10|" + stx;
				String EndData 					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData 	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][deleteFavList]Server decrypted response: " + decryptedString);
				
				/* Mary@20130719 - Fixes_Request-20121102,ReqNo.8
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
				if(decryptedString == null)
					return true;
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
				*/
				if(decryptedString != null){
					if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
						throw new FailedAuthenicationException(decryptedString);
					else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
						throw new Exception(decryptedString);
					else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
						throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				}
			}
			
			return true;
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			response		= null;
			userParam 		= null;
			userParamInByte = null;
			senderCode		= null;
			favorateId		= null;
		}
		
		return false;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	
/*	public static void updateFavListName(Map parameters) throws FailedAuthenicationException {

//    	String userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId = (String) parameters.get(ParamConstants.FAVORATEID_TAG);
		String favorateName = (String) parameters.get(ParamConstants.FAVORATENAME_TAG);
//		String paramString = (String) parameters.get(ParamConstants.PARAMSTRING_TAG);

    	try {


    		URL url = new URL("http://203.116.91.169/TradeGetSession");
			byte[] bytes = readUrlData(url);

			String userParamKey = SystemUtil.byteToHex(bytes, true);
			url = new URL("http://203.116.91.169/[" + userParamKey +"]TradeLogin?Username=test25|Password=abc123|PullMode=1|AppName=M|Language=EN|Compress=0|EncryptedUP=0|Encryption=0");

			byte[] dataBytes = readUrlData(url);
			String response = new String(dataBytes, "ISO-8859-1");
			int pos = response.indexOf("[UserParam]=") + "[UserParam]=".length();
			int end = response.indexOf("\r", pos);

			String paramString = response.substring(pos, end);	

    		 url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/Favorate");

            HttpURLConnection httpPost = (HttpURLConnection) url.openConnection();
            httpPost.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpPost.setUseCaches(false);
            httpPost.setDoOutput(true);
            httpPost.setDoInput(true);
            httpPost.setRequestMethod("POST");
            OutputStreamWriter writer = new OutputStreamWriter(httpPost.getOutputStream());

		    String output = "*=UpdFavListName|+=" + senderCode + "|-=" + favorateId + "|.="
    			+ favorateName + "|/=0";
		    output = "Request=" + URLEncoder.encode(output) + "&Userparams=" + paramString;

		    String test = "Request=*%3DUpdFavListName%7C%2B%3DA00025%7C-%3D2%7C.%3DFAV+2%7C%2F%3D0%7C0%3D&Userparams=" + paramString;

		    writer.write(test);
		    writer.flush();

		    int responseCode = httpPost.getResponseCode();
            String responseMessage = httpPost.getResponseMessage();
            DefinitionConstants.Debug(responseCode);
            if (responseCode == 200) {
                InputStream is = httpPost.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                StringBuffer buffer = new StringBuffer();

                int ch = 0;
				while ((ch = isr.read()) != -1) {
                    buffer.append((char) ch);
                }

                String data = buffer.toString();
            } else if(responseCode == 401) {
                DefinitionConstants.Debug();
            }
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

    }*/

	
	/*  Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static void updateFavListName(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId = (String) parameters.get(ParamConstants.FAVORATEID_TAG);
		String favorateName = (String) parameters.get(ParamConstants.FAVORATENAME_TAG);

		favorateName = favorateName.replaceAll(" ", "_");

		try {

			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=UpdFavListName|+=" + senderCode + "|-=" + favorateId + "|.="
						+ favorateName + "|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101";

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				String urlAddress	= "/[" + userParam + "]Favorate?*=UpdFavListName|+=" + senderCode + "|-=" + favorateId + "|.="
										+ favorateName + "|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101";
				URL url 			= new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][updateFavListName]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);
			}else{

				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "*=UpdFavListName|+=" + senderCode + "|-=" + favorateId + "|.="
						+ favorateName + "|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101";

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][updateFavListName]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}    	
	}
	*/
	// use in Rename and create New watchlist
	public static boolean updateFavListName(Map<String, Object> parameters) throws Exception{
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;
		
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId 		= (String) parameters.get(ParamConstants.FAVORATEID_TAG);
		String favorateName 	= (String) parameters.get(ParamConstants.FAVORATENAME_TAG);
		/* Mary@20121224 - Fixes_Request-20121221, ReqNo.16
		favorateName 			= favorateName.replaceAll(" ", "_");
		*/
		
		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=UpdFavListName|+=" + senderCode + "|-=" + favorateId + "|.="
						+ favorateName + "|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101";

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/
				/* Mary@20130104 - Fixes_Request-20121102, ReqNo.13 - START
				String urlAddress	= "/[" + userParam + "]Favorate?*=UpdFavListName|+=" + senderCode + "|-=" + favorateId + "|.="
										+ favorateName + "|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101";
										
				url 				= new URL("http", AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][updateFavListName]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 			= readUrlData(url);
				*/
				String urlParameters="*=UpdFavListName|+=" + senderCode + "|-=" + favorateId + "|.=" + favorateName + "|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101";
				url					= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/Favorate");
				DefinitionConstants.Debug("[AtpConnectUtil][updateFavListName]Url address: " + url.toString() );
				
				dataBytes			= readUrlPostData(url, urlParameters, userParam);
				// Mary@20130104 - Fixes_Request-20121102, ReqNo.13 - END
				response			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][updateFavListName]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			}else{
				String encryptionKey			= AppConstants.getEncryptedKey();
				byte[] mKey 					= AES256Encryption.HashSHA256Generator(encryptionKey);
				
				String InputParam 				= "*=UpdFavListName|+=" + senderCode + "|-=" + favorateId + "|.="
													+ favorateName + "|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101";
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|" + stx + CustomizedBase64.encodeBytes(userParamInByte) + "|" + CustomizedBase64.encodeBytes(EData) + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd, AppConstants.STRING_ENCODING_FORMAT);
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				*/
				url 						= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][updateFavListName]InputParam: " + InputParam);
				DefinitionConstants.Debug("[AtpConnectUtil][updateFavListName]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 				= readUrlData(url);
				response 				= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				
				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try{
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				}catch(Exception e){
					e.printStackTrace();
				}

				if(startIdx != -1 && endIdx != -1){
					String sSubData	= response.substring(startIdx, endIdx);
					byte[] decData 	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][updateFavListName]Server response: " + decryptedString);
				
				/* Mary@20130719 - Fixes_Request-20121102,ReqNo.8
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
				if(decryptedString == null)
					return true;
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
				*/
				if(decryptedString != null){
					if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
						throw new FailedAuthenicationException(decryptedString);
					else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
						throw new Exception(decryptedString);
					else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
						throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				}
			}
			return true;
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			Log.e("[AtpConnectUtil]","[updateFavListName] e.getMessage() : " + e.getMessage() );
			Log.e("[AtpConnectUtil]","[updateFavListName] ErrorCodeConstants.isOwnException( e.getMessage() ) : " + ErrorCodeConstants.isOwnException( e.getMessage() ) );
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			response		= null;
			userParam 		= null;
			userParamInByte = null;
			senderCode		= null;
			favorateId 		= null;
			favorateName 	= null;
		}
		
		return false;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	/*  Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static void addFavListStock(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId = (String) parameters.get(ParamConstants.FAVORATEID_TAG);
		String symbolCode = (String) parameters.get(ParamConstants.SYMBOLCODE_TAG);
		String exchangeCode = (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);

		String repexchgCode=null;

		if(exchangeCode.contains("D")){
			exchangeCode = exchangeCode.substring(0,2);
		}

		try {

			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=UpdFavListStk|+=" + senderCode + "|-=" + favorateId + "|.=A|/=0"
						+ "|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101|1=" + symbolCode + "|2=" + exchangeCode;

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				String urlAddress 	= "/[" + userParam + "]Favorate?*=UpdFavListStk|+=" + senderCode + "|-=" + favorateId + "|.=A|/=0"
										+ "|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101|1=" + symbolCode + "|2=" + exchangeCode;
				URL url 			= new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][addFavListStock]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);
			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "*=UpdFavListStk|+=" + senderCode + "|-=" + favorateId + "|.=A|/=0"
						+ "|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101|1=" + symbolCode + "|2=" + exchangeCode;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][addFavListStock]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}    	
	}
	*/
	// use in add stock to watchlist
	public static boolean addFavListStock(Map<String, Object> parameters) throws Exception{
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam		= null;
		byte[] userParamInByte	= null;
		
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END

		String senderCode		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId		= (String) parameters.get(ParamConstants.FAVORATEID_TAG);
		String symbolCode		= (String) parameters.get(ParamConstants.SYMBOLCODE_TAG);
		String exchangeCode		= (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);

		if( exchangeCode.contains("D") )
			exchangeCode = exchangeCode.substring(0,2);

		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=UpdFavListStk|+=" + senderCode + "|-=" + favorateId + "|.=A|/=0"
						+ "|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101|1=" + symbolCode + "|2=" + exchangeCode;

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/
				String urlAddress 	= "/[" + userParam + "]Favorate?*=UpdFavListStk|+=" + senderCode + "|-=" + favorateId + "|.=A|/=0"
										+ "|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101|1=" + symbolCode + "|2=" + exchangeCode;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][addFavListStock]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);

				DefinitionConstants.Debug("[AtpConnectUtil][addFavListStock]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
			}else{
				String encryptionKey			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String InputParam 				= "*=UpdFavListStk|+=" + senderCode + "|-=" + favorateId + "|.=A|/=0"
													+ "|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101|1=" + symbolCode + "|2=" + exchangeCode;
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncodedUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				String urlAdd 					= "10|" + stx+base64EncodedUserParam + "|" + encryptedInputParam + etx + "E";
				String encodedURL				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				*/
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][addFavListStock]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][addFavListStock]response: " + response);

				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData				= "10|"+stx;
				String EndData					= etx+"E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx 	= response.indexOf(EndData);
				}catch(Exception e){
					e.printStackTrace();
				}

				if(startIdx != -1 && endIdx != -1){
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData 	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][addFavListStock]Server decrypted response: " + decryptedString);
				
				/* Mary@20130719 - Fixes_Request-20121102,ReqNo.8
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
				if(decryptedString == null)
					return true;
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
				*/
				if(decryptedString != null){
					if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
						throw new FailedAuthenicationException(decryptedString);
					else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
						throw new Exception(decryptedString);
					else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
						throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				}
			}
			return true;
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			response		= null;
			userParam		= null;
			userParamInByte	= null;
			senderCode		= null;
			favorateId		= null;
			symbolCode		= null;
			exchangeCode	= null;
		}
		
		return false;	
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String getFavListStockCode(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId = (String) parameters.get(ParamConstants.FAVORATEID_TAG);

		try {

			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=GetFavListStkCodeCache|+=" + senderCode + "|-=" + favorateId;

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				String urlAddress 	= "/[" + userParam + "]Favorate?*=GetFavListStkCodeCache|+=" + senderCode + "|-=" + favorateId;
				URL url				= new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListStockCode]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);

				return response;
			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "*=GetFavListStkCodeCache|+=" + senderCode + "|-=" + favorateId;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListStockCode]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}    	
		return null;
	}
	*/
	public static String getFavListStockCode(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;
		
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String favorateId		= (String) parameters.get(ParamConstants.FAVORATEID_TAG);
		
		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=GetFavListStkCodeCache|+=" + senderCode + "|-=" + favorateId;

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/
				String urlAddress 	= "/[" + userParam + "]Favorate?*=GetFavListStkCodeCache|+=" + senderCode + "|-=" + favorateId;
				url					= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListStockCode]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				
				String InputParam 				= "*=GetFavListStkCodeCache|+=" + senderCode + "|-=" + favorateId;
				byte[] EData					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				
				String urlAdd					= "10|" + stx + CustomizedBase64.encodeBytes(userParamInByte) + "|" + CustomizedBase64.encodeBytes(EData) + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				*/
				url 						= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListStockCode]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 					= readUrlData(url);
				response 					= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try{
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				}catch(Exception e){
					e.printStackTrace();
				}

				if(startIdx != -1 && endIdx != -1){
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData 	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListStockCode]Server response: " + decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam 		= null;
			userParamInByte = null;
			senderCode		= null;
			favorateId		= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String getFavListInfo(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);

		try {

			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=GetFavListInfo|+=" + senderCode + "|,=1";

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				String urlAddress	= "/[" + userParam + "]Favorate?*=GetFavListInfo|+=" + senderCode + "|,=1";
				URL url 			= new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListInfo]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);

				return response;
			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "*=GetFavListInfo|+=" + senderCode + "|,=1";

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncryptedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncryptedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListInfo]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}    	
		return null;
	}
	*/
	public static String getFavListInfo(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam		= null;
		byte[] userParamInByte	= null;
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte	= (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode 		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);

		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+ "]Favorate?*=GetFavListInfo|+=" + senderCode + "|,=1";

				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/

				String urlAddress	= "/[" + userParam + "]Favorate?*=GetFavListInfo|+=" + senderCode + "|,=1";
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListInfo]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListInfo]Server response: " + response);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam 				= "*=GetFavListInfo|+=" + senderCode + "|,=1";
				byte[] EData					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncryptedUserParam = CustomizedBase64.encodeBytes(userParamInByte);
				String urlAdd 					= "10|" + stx + base64EncryptedUserParam + "|" + encryptedInputParam + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]Favorate?"+encodedURL);
				*/
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]Favorate?" + encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListInfo]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListInfo]response: " + response);

				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try{
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				}catch(Exception e){
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][getFavListInfo]Server decrypted response: " + decryptedString);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte	= null;
			senderCode 		= null;
		}
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END


	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradePortfolio(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String brokerCode = (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String accountNo = (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String branchCode = (String) parameters.get(ParamConstants.BRANCHCODE_TAG);

		try {
			/* Mary@20120718 - avoid Malformed ipv6 address error - START
			String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
					+ "]TradePortfolio?'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode 
					+ "|3=" + brokerCode + "|Z=1";
			DefinitionConstants.Debug("Url address: " + urlAddress);
			/
			String urlAddress = "/[" + userParam + "]TradePortfolio?'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode + "|3=" + brokerCode + "|Z=1";
			// Mary@20120718 - avoid Malformed ipv6 address error - END

			if(AppConstants.EnableEncryption==false){
				// Mary@20120718 - avoid Malformed ipv6 address error - START
				// URL url = new URL(urlAddress);
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolio]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = new String(bytes, AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);

				return response;
			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode + "|3=" + brokerCode + "|Z=1";

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncryptedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncryptedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradePortfolio?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]TradePortfolio?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, "/[0]TradePortfolio?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolio]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr,AppConstants.STRING_ENCODING);

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}    	
		return null;
	}
	*/
	public static String tradePortfolio(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption ){
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode 		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String brokerCode 		= (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String accountNo 		= (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String branchCode		= (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27
		int intPortfolioType	= ( (Integer) parameters.get(ParamConstants.PARAM_PORTFOLIOTYPE_TAG) ).intValue();

		try {
			/* Mary@20120718 - avoid Malformed ipv6 address error - START
			String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
					+ "]TradePortfolio?'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode 
					+ "|3=" + brokerCode + "|Z=1";
			DefinitionConstants.Debug("Url address: " + urlAddress);
			*/
			// Mary@20120718 - avoid Malformed ipv6 address error - END

			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				// Mary@20120718 - avoid Malformed ipv6 address error - START
				// URL url = new URL(urlAddress);
				// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START
				String urlAddress	= "/[" + userParam + "]TradePortfolio?'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode + "|3=" + brokerCode
										/* Mary@20130704 - Fixes_Request-20130523, ReqNo.27
										+ "|Z="+ParamConstants.PORTFOLIOTYPE_TAG;     //1";
										*/
										+ "|Z=" + intPortfolioType;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolio]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolio]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam				= "'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode + "|3=" + brokerCode
													/* Mary@20130704 - Fixes_Request-20130523, ReqNo.27
													+ "|Z="+ParamConstants.PORTFOLIOTYPE_TAG; //1";
													*/
													+ "|Z=" + intPortfolioType;
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncryptedUserParam = CustomizedBase64.encodeBytes(userParamInByte);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|"+stx+base64EncryptedUserParam+"|"+encryptedInputParam+etx+"E";
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradePortfolio?"+encodedURL);

				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]TradePortfolio?"+encodedURL);
				*/
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradePortfolio?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolio]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolio]response: " + response);

				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData 				= "10|" + stx;
				String EndData					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				String decryptedString=null;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolio] Server decrypted response: " + decryptedString);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte	= null;
			senderCode 		= null;
			brokerCode 		= null;
			accountNo 		= null;
			branchCode		= null;
		}  	
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	public static String tradePortfolioSummaryRpt(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;

		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		
		String brokerCode 		= (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String accountNo 		= (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String branchCode		= (String) parameters.get(ParamConstants.BRANCHCODE_TAG);

		try {
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				String urlAddress	= "/[" + userParam + "]TradePrtfSummRpt?" + ".=" + accountNo + "|3=" + brokerCode + "|M=" + branchCode ;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSummaryRpt]Url address: " + url.toString() );
				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSummaryRpt]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam				= ".=" + accountNo + "|3=" + brokerCode + "|M=" + branchCode;
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncryptedUserParam = CustomizedBase64.encodeBytes(userParamInByte);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|"+stx+base64EncryptedUserParam+"|"+encryptedInputParam+etx+"E";
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradePrtfSummRpt?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSummaryRpt]Url address: " + url.toString() );
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSummaryRpt]response: " + response);

				String BeginData 				= "10|" + stx;
				String EndData					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				String decryptedString=null;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSummaryRpt] Server decrypted response: " + decryptedString);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte	= null;
			brokerCode 		= null;
			accountNo 		= null;
			branchCode		= null;
		}  	
		return null;
	}
	
	public static String tradePortfolioSubDetailRpt(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;

		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		
		String senderCode		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String brokerCode 		= (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String accountNo 		= (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String branchCode		= (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String exchangeCode			= (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);

		try {
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				String urlAddress	= "/[" + userParam + "]TradePrtfSubDtlRpt?" + "'=" + senderCode+"|.=" + accountNo + "|3=" + brokerCode + "|M=" + branchCode 
										+ "|c=" + exchangeCode + "|e=D";
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSubDetailRpt]Url address: " + url.toString() );
				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSubDetailRpt]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam				="'=" + senderCode+"|.=" + accountNo + "|3=" + brokerCode + "|M=" + branchCode + "|c=" + exchangeCode + "|e=D";
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncryptedUserParam = CustomizedBase64.encodeBytes(userParamInByte);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|"+stx+base64EncryptedUserParam+"|"+encryptedInputParam+etx+"E";
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradePrtfSubDtlRpt?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSubDetailRpt]Url address: " + url.toString() );
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSubDetailRpt]response: " + response);

				String BeginData 				= "10|" + stx;
				String EndData					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				String decryptedString=null;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioSubDetailRpt] Server decrypted response: " + decryptedString);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte	= null;
			brokerCode 		= null;
			accountNo 		= null;
			branchCode		= null;
		}  	
		return null;
	}
	
	public static String tradePortfolioDetailRpt(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;

		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		
		String senderCode 		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String accountNo 		= (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String brokerCode 		= (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String branchCode		= (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String accExchangeCode	= (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		String stockCode 		= (String) parameters.get(ParamConstants.STOCK_CODE_TAG);
		String exchangeCode = null;
		if (stockCode!=null){
			exchangeCode = stockCode.substring(stockCode.indexOf(".")+1);
			stockCode = stockCode.substring(0, stockCode.indexOf("."));
		}
		String mode = (String) parameters.get(ParamConstants.MODE_TAG);

		try {
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				String urlAddress	= "/[" + userParam + "]TradePrtfDtlRpt?" + "'=" + senderCode + "|.=" + accountNo + "|3=" + brokerCode + "|M=" + branchCode 
						+ "|c=" + accExchangeCode + "|e=D" + "|+=" + stockCode + "|b=" + exchangeCode + "|g=" + mode;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioDetailRpt]Url address: " + url.toString() );
				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioDetailRpt]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam				= "'=" + senderCode + "|.=" + accountNo + "|3=" + brokerCode + "|M=" + branchCode + "|c=" + accExchangeCode + "|e=D" + "|+=" + stockCode + "|b=" + exchangeCode + "|g=" + mode; 
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncryptedUserParam = CustomizedBase64.encodeBytes(userParamInByte);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|"+stx+base64EncryptedUserParam+"|"+encryptedInputParam+etx+"E";
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradePrtfDtlRpt?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioDetailRpt]Url address: " + url.toString() );
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioDetailRpt]response: " + response);

				String BeginData 				= "10|" + stx;
				String EndData					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				String decryptedString=null;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradePortfolioDetailRpt] Server decrypted response: " + decryptedString);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte	= null;
			brokerCode 		= null;
			accountNo 		= null;
			branchCode		= null;
		}  	
		return null;
	}
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradeStatus(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String accountNo = (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String branchCode = (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String timeStamp = (String) parameters.get(ParamConstants.TIMESTAMP_TAG);


		try {
			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+"]TradeStatus?'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode + "|L="+timeStamp;
				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/
				String urlAddress 	= "/[" + userParam +"]TradeStatus?'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode + "|L="+timeStamp;
				URL url 			= new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeStatus]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = null;
				
				if(AppConstants.atpCompress){
					try {
						response = FormatUtil.uncompress(bytes, "", "a").toString();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
				}else{
					response = EncodingUtils.getString(bytes, AppConstants.STRING_ENCODING);
				}
				//new String(bytes, "ISO-8859-1");
				//response.getBytes(AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("Server response: " + response);
				return response;
			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String InputParam = "'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode  + "|L="+timeStamp;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodedUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				DefinitionConstants.Debug("Url address: "+"http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeStatus?"+encodedURL);

				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeStatus?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, "/[0]TradeStatus?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeStatus]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);
					
					if(AppConstants.atpCompress){
						try {
							decryptedString = FormatUtil.uncompress(arr, "", "a").toString();
						} catch (DataFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
					}else{
						decryptedString = new String(arr,AppConstants.STRING_ENCODING);
					}
				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}    	
		return null;
	}
	*/
	
	public static String tradeStatus(Map<String, Object> parameters) throws Exception {
		
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode 		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String accountNo 		= (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String branchCode 		= (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String timeStamp 		= (String) parameters.get(ParamConstants.TIMESTAMP_TAG);
		//Added by Thinzar, Fixes_Request-20140820, ReqNo.1
		String brokerCode		= (String) parameters.get(ParamConstants.BROKERCODE_TAG);

		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam 
						+"]TradeStatus?'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode + "|L="+timeStamp;
				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/

				String urlAddress 	= "/[" + userParam +"]TradeStatus?'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode + "|L="+timeStamp;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeStatus]Url address: " + url.toString() );
				DefinitionConstants.Debug("[AtpConnectUtil][tradeStatus]param:" + urlAddress);
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 			= readUrlData(url);
				
				if(AppConstants.atpCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
				}else{
					response = EncodingUtils.getString(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				}
				//new String(bytes, "ISO-8859-1");
				//response.getBytes(AppConstants.STRING_ENCODING);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeStatus]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam 				= "'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode  + "|L="+timeStamp;
				if(brokerCode != null) 	InputParam						+= "|3=" + brokerCode;	//Added by Thinzar, Fixes_request-20140820, ReqNo.1
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncodedUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|" + stx + base64EncodedUserParam + "|" + encryptedInputParam + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeStatus]param:" + InputParam);
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				DefinitionConstants.Debug("Url address: "+"http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeStatus?"+encodedURL);

				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeStatus?"+encodedURL);
				*/
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradeStatus?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeStatus]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeStatus]response: " + response);

				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					
					if(AppConstants.atpCompress){
						try {
							decryptedString = FormatUtil.uncompress(arr, "", "a").toString();
							// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
							decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
						} catch (DataFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
					}else{
						decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
						// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
						decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
					}
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradeStatus]Server decrypted response: " + decryptedString);
				
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				decryptedString	= decryptedString == null ? "" : decryptedString;
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte	= null;
			senderCode 		= null;
			accountNo 		= null;
			branchCode		= null;
			timeStamp		= null;
		}   	
		return null;
	}
	//  Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	

	public static String encryptATPString(String s) {
		int length		= s.length();
		String result 	= "";
		// Added by Mary@20130605 - avoid IndexOutOfBoundException
		if(length > 0){
			int cl = length / 2 + length % 2;
			for(int i = 0; i < cl; i++) {
				int s1 = s.charAt(i);
				int s2;
				if(cl + i > length - 1)
					s2 = 0;
				else
					s2 = s.charAt(cl + i);
				int c1 = (s1 & 0xf0 | s2 & 0xf) ^ 0xf8;
				int c2 = (s2 & 0xf0 | s1 & 0xf) ^ 0xf8;
				result = (new StringBuilder(String.valueOf(result))).append((char)c1).toString();
				result = (new StringBuilder(String.valueOf(result))).append((char)c2).toString();
			}
		}
	
		return result;
	}

	public static String decryptATPString(String s) {
		String result 	= "";
		int length		= s.length();
		// Added by Mary@20130605 - avoid IndexOutOfBoundException
		if(length > 0){
			/* Mary@20130605 - comment unused variable
			String res = null;
			*/
			int cl = 0;
			if(length%2 > 0 ){
				cl = length/2 + 1;
			}else{
				cl = length/2;
			}
	
			String[] result1 = new String[length+1];
	
			int s2 = 0;
			int s1 = 0;
	
			int i = 0;
			int n = 1;
	
			while(i<length){
				s1 = s.charAt(i) ^ 0xf8;
				int im = i+1;
				if(im > length){
					s2 = 0 ^ 0xf8;
				}else{
					s2 = s.charAt(i+1) ^ 0xf8;
				}
	
				int c1 = (s1 & 0xf0) | (s2 & 0xf);
				int c2 = (s2 & 0xf0) | (s1 & 0xf);
	
				int naddcl = n+cl;
	
				result1[n] = Character.toString((char)c1);
				result1[n+cl] = Character.toString((char)c2);
				result1[0] = Character.toString((char)naddcl);
	
				n++; i+=2;
			}
	
			for(int j=0; j<result1.length-1; j++){
				result = result + result1[j+1];
			}
		}

		return filter(result);

	}

	public static String filter(String str) {
		StringBuilder filtered = new StringBuilder(str.length());
		for (int i = 0; i < str.length(); i++) {
			char current = str.charAt(i);
			if (current >= 0x20 && current <= 0x7e) {
				filtered.append(current);
			}
		}
		return filtered.toString();
	}

	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	public static String tradeClient(Map parameters) throws FailedAuthenicationException {

		String userParam = null;
		byte[] userParamInByte = null;

		if(AppConstants.EnableEncryption==false){
			userParam = (String) parameters.get(ParamConstants.USERPARAM_TAG);
		}else{
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		}
		String senderCode = (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		try {

			if(AppConstants.EnableEncryption==false){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeClient?'=" + senderCode;
				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				/ 
				String urlAddress 	= "/[" + userParam +"]TradeClient?'=" + senderCode;
				URL url 			= new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClient]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				
				byte[] bytes = readUrlData(url);

				String response = null;

				if(AppConstants.atpCompress){
					try {
						response = FormatUtil.uncompress(bytes, "", "a").toString();
					} catch (DataFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					response = new String(bytes, AppConstants.STRING_ENCODING);
				}
				DefinitionConstants.Debug("Server response: " + response);

				return response;
			}else{
				String encryptionKey = AppConstants.getEncryptedKey();
				byte[] mKey;

				String stx = Character.toString(startText);
				String etx = Character.toString(endText);


				String InputParam = "'="+senderCode;

				mKey = AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData = AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING), mKey);

				String encryptedInputParam = Base64.encodeBytes(EData);
				String base64EncodeUserParam = Base64.encodeBytes(userParamInByte);

				String urlAdd = "10|"+stx+base64EncodeUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeClient?"+encodedURL);
				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeClient?"+encodedURL);
				/
				URL url = new URL("http", AppConstants.ATP_SERVER_IP, AppConstants.ATP_SERVER_PORT, "/[0]TradeClient?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClient]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END

				byte[] dataBytes = readUrlData(url);
				String response = new String(dataBytes,AppConstants.STRING_ENCODING);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString=null;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					decData = Base64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING));

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);
					
					if(AppConstants.atpCompress){

						try {
							decryptedString = FormatUtil.uncompress(arr, "", "a").toString();
						} catch (DataFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else{
						decryptedString = new String(arr,AppConstants.STRING_ENCODING);
					}

				}

				DefinitionConstants.Debug("Server response: " + decryptedString);

				return decryptedString;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}    	
		return null;
	}
	*/
	public static String tradeClient(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam		= null;
		byte[] userParamInByte	= null;
		
		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
		if( ! AppConstants.EnableEncryption )
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		else
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		*/
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
		
		String senderCode		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		
		try {
			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if( ! AppConstants.EnableEncryption ){
			*/
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				String urlAddress = "http://" + AppConstants.ATP_SERVER_IP + "/[" + userParam +"]TradeClient?'=" + senderCode;
				DefinitionConstants.Debug("Url address: " + urlAddress);

				URL url = new URL(urlAddress);
				*/ 

				String urlAddress 	= "/[" + userParam +"]TradeClient?'=" + senderCode;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClient]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 			= readUrlData(url);
				response			= null;
				if(AppConstants.atpCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (DataFormatException e) {
						e.printStackTrace();
					}
				}else{
					response = new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClient]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam 				= "'=" + senderCode;
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncodeUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				String stx 						= Character.toString(startText);
				String etx					 	= Character.toString(endText);
				String urlAdd 					= "10|" + stx + base64EncodeUserParam + "|" + encryptedInputParam + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				/* Mary@20120718 - avoid Malformed ipv6 address error - START
				URL url = new URL("http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeClient?"+encodedURL);
				DefinitionConstants.Debug("Url address: http://" + AppConstants.ATP_SERVER_IP + "/[0]TradeClient?"+encodedURL);
				*/
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradeClient?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClient]Url address: " + url.toString() );
				// Mary@20120718 - avoid Malformed ipv6 address error - END
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClient]response: " + response);

				/* Mary@20121226 - Fixes_Request-20121102,ReqNo.8
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				*/
				
				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData	= response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr		= AES256Encryption.deencryptData(decData, mKey);
					
					if(AppConstants.atpCompress){
						try {
							decryptedString = FormatUtil.uncompress(arr, "", "a").toString();
							// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
							decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
						} catch (DataFormatException e) {
							e.printStackTrace();
						}
					}else{
						decryptedString	= new String(arr,AppConstants.STRING_ENCODING_FORMAT);
						// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
						decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
					}
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradeClient]Server decrypted response: " + decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte	= null;
			senderCode 		= null;
		}   	   	
		return null;
	}
	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	// Added by Mary@20130121 - Change_Request-20130104, ReqNo.3 - START
	public static String updateRDSStatus(Map<String, Object> parameters) throws Exception {
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;

		if(AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		
		String senderCode 		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		/* Mary@20130605 - comment unused variable
		String accountNo 		= (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String exchangeCode 	= (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		*/
		String brokerCode 		= (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String branchCode 		= (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String strRDSVersionNo 	= (String) parameters.get(ParamConstants.RDS_VERSIONNO_TAG);
		boolean isAcknowledge 	= ( (Boolean) parameters.get(ParamConstants.RDS_AGREE_OR_DISAGREE_TAG) ).booleanValue();

		try {			
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				String strFileName	= "/[" + userParam +"]UpdateRDSStatus?+=" + senderCode + "|,=|-=" + brokerCode 
										+ "|.=" + branchCode + "|/=|0=" + (isAcknowledge ? "1" : "0") + "|1=" + strRDSVersionNo + "|2=Electronic|5=MA";
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), strFileName);
				DefinitionConstants.Debug("[AtpConnectUtil][updateRDSStatus]Url address: " + url.toString() );
				dataBytes	 		= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][updateRDSStatus]Server response: " + response);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response; 
			}else{
				String encryptionKey			= AppConstants.getEncryptedKey();
				byte[] mKey 					= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam 				= "+=" + senderCode + "|,=|-=" + brokerCode 
													+ "|.=" + branchCode + "|/=|0=" + (isAcknowledge ? "1" : "0") + "|1=" + strRDSVersionNo + "|2=Electronic|5=MA";
				byte[] EData 					= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam		= CustomizedBase64.encodeBytes(EData);
				
				String base64EncodedUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				String stx					 	= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]UpdateRDSStatus?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][updateRDSStatus]Url address: " + url.toString() );
				
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][updateRDSStatus]response: " + response);
				
				String BeginData 				= "10|"+stx;
				String EndData 					= etx+"E";

				int startIdx 					= -1;
				int endIdx 						= -1;
				/* Mary@20130227 - Fixes_Request-20130108, ReqNo.16
				String decryptedString			= null;
				*/
				String decryptedString			= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx 	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData	= response.substring(startIdx, endIdx);
					byte[] decData	= null;

					try {
						decData = CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					} catch (IOException e) {
						e.printStackTrace();
					}

					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString = new String(arr,AppConstants.STRING_ENCODING_FORMAT);
					// Added by Mary@20130328 - Fixes_Request-20130314, ReqNo.6
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][updateRDSStatus]Server decrypted response: " + decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam 		= null;
			userParamInByte = null;
			senderCode 		= null;
			/* Mary@20130605 - comment unused variable
			accountNo 		= null;
			exchangeCode 	= null;
			*/
			brokerCode 		= null;
			branchCode 		= null;
		}
		return null;
	}
	// Added by Mary@20130121 - Change_Request_20130104, ReqNo.3 - END
	
	// Added by Mary@20130919 - Change_Request-20130711, ReqNo.14 - START
	public static String ValidateOneFA(Map<String, Object> parameters) throws Exception {
		URL url						= null;
		byte[] dataBytes			= null;
		String response				= null;
		String userParam 			= null;
		byte[] userParamInByte 		= null;
		
		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		
		String strUserName 	= (String) parameters.get(ParamConstants.USERNAME_TAG);
		String strRandomNum	= (String) parameters.get(ParamConstants.E2EE_RANDOM_NUM_TAG);
		String strE2EEPwd	= (String) parameters.get(ParamConstants.PASSWORD_TAG);
		String strRSAPwd	= (String) parameters.get(ParamConstants.RSA_PASSWORD_TAG);
		
		try{
			String strInputParam	= "+=" + strUserName  + "|,=" + strRSAPwd + "|-=" + strE2EEPwd + "|.="+ strRandomNum;
			DefinitionConstants.Debug("[AtpConnectUtil][ValidateOneFA]InputParam: " + strInputParam + ";" );
			
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[" + userParam + "]ValidateOneFA?" + strInputParam);
				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOneFA]Url address: " + url.toString() );
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
		
				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOneFA]Server response: " + response);
				
				return response;
			}else{
				
				String encryptionKey			= AppConstants.getEncryptedKey();
				byte[] mKey 					= AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData 					= AES256Encryption.encryptData(strInputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam		= CustomizedBase64.encodeBytes(EData);
				
				String base64EncodedUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				
				String urlAdd 					= "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL 				= URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]ValidateOneFA?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOneFA]Url address: " + url.toString() );
				
				
				dataBytes						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				
				String BeginData 				= "10|"+stx;
				String EndData 					= etx+"E";

				int startIdx 					= -1;
				int endIdx 						= -1;
				String decryptedString			= response;

				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData	= response.substring(startIdx, endIdx);

					byte[] decData	= null;
					try {
						decData = CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					} catch (IOException e) {
						e.printStackTrace();
					}

					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					decryptedString	= new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOneFA]Server response: " + decryptedString);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
			
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
		}
		
		return null;
	}
	// Added by Mary@20130919 - Change_Request-20130711, ReqNo.14 - END

	/*  Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	public static byte[] readUrlData(URL url) throws FailedAuthenicationException {
	*/
	public static byte[] readUrlData(URL url) throws Exception {
		int length						= 0;
		int size 						= 1024;
		byte[] dataBytes 				= null;
		InputStream inputStream 		= null;
		HttpURLConnection urlConnection	= null;

		try {
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - START 
			if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO)
			     System.setProperty("http.keepAlive", "false");
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - END
						
			urlConnection = (HttpURLConnection) url.openConnection();
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - START
			urlConnection.setRequestMethod("GET");
			/* Mary@20130925 - Change_Request-20130724, ReqNo.16
			urlConnection.setConnectTimeout(5000);
			*/
			urlConnection.setConnectTimeout(8000);
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - END
			
			/* Mary@20120717 - additional exception code with error message appended
			if(urlConnection.getResponseCode()==500){
			*/
			/* Mary@20121227 - handle httpURLConnection IndexOutOfBound exception
			if(urlConnection.getResponseCode()==500 || urlConnection.getResponseCode()==502){
			*/
			int intRespCode;
			try{
				intRespCode	= urlConnection.getResponseCode();
				DefinitionConstants.Debug("readUrlData - response code: " + intRespCode);
			}
			//Added by Thinzar, Change_Request-20150901, ReqNo.1 - START
			catch(SocketTimeoutException stoe){
				AppConstants.timeoutExceptionCount++;
				DefinitionConstants.Debug("Catching socket time out from readUrlData=" + AppConstants.timeoutExceptionCount);
				stoe.printStackTrace();
				intRespCode = 0;
					
			}catch(ConnectException cne){
				AppConstants.connectExceptionCount++;
				DefinitionConstants.Debug("Catching connection exception from readUrlData=" + AppConstants.connectExceptionCount);
				cne.printStackTrace();
				intRespCode = 0;
				
			}
			//Added by Thinzar, Change_Request-20150901, ReqNo.1 - END
			catch(java.lang.IndexOutOfBoundsException IOOBE){
				IOOBE.printStackTrace();
				intRespCode = 0;
			}
			//Kw@20130404 - Fixes_Request-20130314, ReqNo.11  added intRespCode==509	
			if(intRespCode==500 || intRespCode==502 || intRespCode==509 || intRespCode==513 || intRespCode == 510){	//Added 513, Fixes_Request-20160101, ReqNo.10
				try{
					inputStream = urlConnection.getInputStream();
				}catch(Exception e){
					inputStream = urlConnection.getErrorStream();
				}
			}else{
				inputStream = urlConnection.getInputStream();
			}
			
			if (inputStream instanceof ByteArrayInputStream) {
				size 		= inputStream.available();
				dataBytes 	= new byte[size];
				length 		= inputStream.read(dataBytes, 0, size);
			} else {
				ByteArrayOutputStream outputStream 	= new ByteArrayOutputStream();
				dataBytes 							= new byte[size];
				while( ( length = inputStream.read(dataBytes, 0, size) ) != -1 ){
					outputStream.write(dataBytes, 0, length);
				}
				dataBytes 							= outputStream.toByteArray();
			}
			
			if(inputStream!=null)
				inputStream = null;

			return dataBytes;
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}catch(EOFException eof){
			eof.printStackTrace();
			throw new EOFException(ErrorCodeConstants.FAIL_COMMUNICATION_EOF);
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}catch (IOException ioe){
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			throw new FailedAuthenicationException("Session is expired");
			 */
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) ){
				throw new FailedAuthenicationException("Session is Expired");
			}else{
				throw new IOException(ErrorCodeConstants.FAIL_COMMUNICATION_IOE);
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}catch(NullPointerException npe){
			npe.printStackTrace();
			throw new NullPointerException(ErrorCodeConstants.FAIL_COMMUNICATION_NPE);
		}catch (Exception e){
			e.printStackTrace();
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			throw new FailedAuthenicationException("Network is unreachable");
			*/
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new FailedAuthenicationException(e.getMessage());
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		}finally{
			if(inputStream != null){
				inputStream.close();
				inputStream = null;
			}
			if(urlConnection != null){
				urlConnection.disconnect();
				urlConnection	= null;
			}
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}

	}
	
	// Added by Mary@20130104 - Fixes_Request-20121102, ReqNo.13 - START
	public static byte[] readUrlPostData(@NonNull URL url, @NonNull String strURLParameters, @NonNull String strUserParam) throws Exception{
		int length						= 0;
		int size 						= 1024;
		byte[] dataBytes 				= null;
		InputStream inputStream 		= null;
		HttpURLConnection urlConnection	= null;

		try {
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - START 
			if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO)
			     System.setProperty("http.keepAlive", "false");
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16 - END
						
			strURLParameters	= "Request=" + URLEncoder.encode(strURLParameters) + "&UserParams=" + strUserParam;
			
			urlConnection 		= (HttpURLConnection) url.openConnection();
			urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			urlConnection.setRequestProperty( "Content-Length", Integer.toString( strURLParameters.length() ) );
			urlConnection.setUseCaches(false);
			urlConnection.setDoOutput(true);
			urlConnection.setDoInput(true);
			urlConnection.setRequestMethod("POST");
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.16
			/* Mary@20130925 - Change_Request-20130724, ReqNo.16
			urlConnection.setConnectTimeout(5000);
			*/
			urlConnection.setConnectTimeout(8000);
			
			DataOutputStream dos= new DataOutputStream( urlConnection.getOutputStream() );
			byte[] bytes 		= strURLParameters.getBytes(AppConstants.STRING_ENCODING_FORMAT) ;
			dos.write(bytes); 
			dos.flush();
			dos.close();
			
			int intRespCode;
			try{
				intRespCode	= urlConnection.getResponseCode();
			}catch(java.lang.IndexOutOfBoundsException IOOBE){
				IOOBE.printStackTrace();
				intRespCode = 0;
			}
			
			if(intRespCode==500 || intRespCode==502){
				try{
					inputStream = urlConnection.getInputStream();
				}catch(Exception e){
					inputStream = urlConnection.getErrorStream();
				}

				if (inputStream instanceof ByteArrayInputStream) {
					size 		= inputStream.available();
					dataBytes 	= new byte[size];
					length 		= inputStream.read(dataBytes, 0, size);
				} else {
					ByteArrayOutputStream outputStream 	= new ByteArrayOutputStream();
					dataBytes							= new byte[size];
					while( ( length = inputStream.read(dataBytes, 0, size) ) != -1 )
						outputStream.write(dataBytes, 0, length);

					dataBytes = outputStream.toByteArray();
				}
			}else{
				inputStream = urlConnection.getInputStream();

				if(inputStream == null){
					dataBytes	= urlConnection.getResponseMessage().getBytes();
				}else{
					if (inputStream instanceof ByteArrayInputStream) {
						size 		= inputStream.available();
						dataBytes 	= new byte[size];
						length 		= inputStream.read(dataBytes, 0, size);
					} else {
						ByteArrayOutputStream outputStream 	= new ByteArrayOutputStream();
						dataBytes 							= new byte[size];
						while( ( length = inputStream.read(dataBytes, 0, size) ) != -1 )
							outputStream.write(dataBytes, 0, length);
						
						dataBytes 							= outputStream.toByteArray();
					}
				}
			}
			
			if(inputStream!=null)
				inputStream = null;

			return dataBytes;
		}catch(EOFException eof){
			eof.printStackTrace();
			throw new EOFException(ErrorCodeConstants.FAIL_COMMUNICATION_EOF);
		}catch (IOException ioe){
			ioe.printStackTrace();
			if( ErrorCodeConstants.isIOAutheticationNull( ioe.getMessage() ) ){
				throw new FailedAuthenicationException("Session is Expired");
			}else{
				throw new IOException(ErrorCodeConstants.FAIL_COMMUNICATION_IOE);
			}
		}catch(NullPointerException npe){
			npe.printStackTrace();
			throw new NullPointerException(ErrorCodeConstants.FAIL_COMMUNICATION_NPE);
		}catch (Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new FailedAuthenicationException(e.getMessage());
		}finally{
			if(inputStream != null){
				inputStream.close();
				inputStream = null;
			}
			if(urlConnection != null){
				urlConnection.disconnect();
				urlConnection	= null;
			}
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}

	}
	// Added by Mary@20130104 - Fixes_Request-20121102, ReqNo.13 - END
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.7 - START
	public static String tradeHistory(Map<String, Object> parameters) throws Exception{
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;
		int statusHistory		= 1;

		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_TAG);
		
		String senderCode 		= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String accountNo 		= (String) parameters.get(ParamConstants.ACCOUNTNO_TAG);
		String branchCode 		= (String) parameters.get(ParamConstants.BRANCHCODE_TAG);
		String brokerCode		= (String) parameters.get(ParamConstants.BROKERCODE_TAG);
		String exchangeCode		= (String) parameters.get(ParamConstants.EXCHANGECODE_TAG);
		String fromDate			= (String) parameters.get(ParamConstants.DATE_FROM_TAG);
		String toDate			= (String) parameters.get(ParamConstants.DATE_TO_TAG);
		 
		String urlParam 		= "'=" + senderCode + "|.=" + accountNo + "|M=" + branchCode + "|P=" + exchangeCode
				+ "|[="+ statusHistory + "|^=" + fromDate + "|_=" + toDate;
		if(brokerCode != null) 	
			urlParam			+= "|3=" + brokerCode;	
		
		try {
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){

				String urlAddress 	= "/[" + userParam +"]TradeHistory?" + urlParam;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeHistory]Url address: " + url.toString() );
				DefinitionConstants.Debug("[AtpConnectUtil][tradeHistory]param:" + urlAddress);

				dataBytes 			= readUrlData(url);
				
				if(AppConstants.atpCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (DataFormatException e) {
						e.printStackTrace();
					} 
				}else{
					response = EncodingUtils.getString(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				}
				
				DefinitionConstants.Debug("[AtpConnectUtil][tradeHistory]Server response: " + response);
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData 					= AES256Encryption.encryptData(urlParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncodedUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|" + stx + base64EncodedUserParam + "|" + encryptedInputParam + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeHistory]param:" + urlParam);
				
				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]TradeHistory?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeHistory]Url address: " + url.toString() );
				
				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][tradeHistory]response: " + response);

				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;

				String decryptedString			= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);
					
					if(AppConstants.atpCompress){
						try {
							decryptedString = FormatUtil.uncompress(arr, "", "a").toString();
							decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
						} catch (DataFormatException e) {
							e.printStackTrace();
						} 
					}else{
						decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
						decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
					}
				}
				DefinitionConstants.Debug("[AtpConnectUtil][tradeHistory]Server decrypted response: " + decryptedString);
				decryptedString	= decryptedString == null ? "" : decryptedString;
				
				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);
				
				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte	= null;
			senderCode 		= null;
			accountNo 		= null;
			branchCode		= null;
			exchangeCode	= null;
			fromDate		= null;
			toDate			= null;
		}   	
		return null;
	}
	//Added by Thinzar, Change_Request-20160101, ReqNo.7 - END
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.8 - START
	public static String ValidatePreLogin(Map<String, Object> parameters) throws Exception{
		URL url					= null;
		byte[] dataBytes		= null;
		String response			= null;
		String userParam 		= null;
		byte[] userParamInByte 	= null;

		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_KEY_INBYTE_TAG);
		else
			userParam 		= (String) parameters.get(ParamConstants.USERPARAM_KEY_TAG);

		String username				= (String) parameters.get(ParamConstants.USERNAME_TAG);
		String strRSAPassword		= (String) parameters.get(ParamConstants.RSA_PASSWORD_TAG);
		String strE2EERandomNum		= (String) parameters.get(ParamConstants.E2EE_RANDOM_NUM_TAG);
		String deviceID 			= (String) parameters.get(ParamConstants.DEVICE_ID_TAG);
		String senderCode 			= (String) parameters.get(ParamConstants.SENDERCODE_TAG);
		String strPassword			= (String) parameters.get(ParamConstants.PASSWORD_TAG);

		String urlParam 	= "+=" + username 
				+ "|"+ Character.toString( FormatUtil.convertAsciiToChar(47) ) + "=" + deviceID 
				+ "|"+ Character.toString( FormatUtil.convertAsciiToChar(48) ) + "=" + AppConstants.TCANDROID_APP_ID
				+ "|"+ Character.toString( FormatUtil.convertAsciiToChar(49) ) + "=" + senderCode;

		if (AppConstants.brokerConfigBean.isEnableE2EEEncryption()) {
			urlParam 		+= "|,=" + strRSAPassword
					+ "|-=" + strPassword
					+ "|.=" + strE2EERandomNum;
		} else{
			String encryptedPasswd	= encryptATPString(strPassword);
			urlParam 					+= "|,=" + encryptedPasswd;
		}

		try {
			if ( ! AppConstants.brokerConfigBean.isEnableEncryption() ){

				String urlAddress 	= "/[" + userParam +"]ValidatePreLogin?" + urlParam;
				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), urlAddress);
				DefinitionConstants.Debug("[AtpConnectUtil][ValidatePreLogin]Url address: " + url.toString() );
				DefinitionConstants.Debug("[AtpConnectUtil][ValidatePreLogin]param:" + urlAddress);

				dataBytes 			= readUrlData(url);

				if(AppConstants.atpCompress){
					try {
						response = FormatUtil.uncompress(dataBytes, "", "a").toString();
					} catch (DataFormatException e) {
						e.printStackTrace();
					} 
				}else{
					response = EncodingUtils.getString(dataBytes, AppConstants.STRING_ENCODING_FORMAT);
				}

				DefinitionConstants.Debug("[AtpConnectUtil][ValidatePreLogin]Server response: " + response);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
					throw new FailedAuthenicationException(response);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
					throw new Exception(response);
				else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return response;
			}else{
				String encryptionKey 			= AppConstants.getEncryptedKey();
				byte[] mKey						= AES256Encryption.HashSHA256Generator(encryptionKey);
				byte[] EData 					= AES256Encryption.encryptData(urlParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam 		= CustomizedBase64.encodeBytes(EData);
				String base64EncodedUserParam 	= CustomizedBase64.encodeBytes(userParamInByte);
				String stx 						= Character.toString(startText);
				String etx 						= Character.toString(endText);
				String urlAdd 					= "10|" + stx + base64EncodedUserParam + "|" + encryptedInputParam + etx + "E";
				String encodedURL 				= URLEncoder.encode(urlAdd, AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][ValidatePreLogin]param:" + urlParam);

				url 							= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]ValidatePreLogin?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][ValidatePreLogin]Url address: " + url.toString() );

				dataBytes 						= readUrlData(url);
				response 						= new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);
				DefinitionConstants.Debug("[AtpConnectUtil][ValidatePreLogin]response: " + response);

				String BeginData 				= "10|" + stx;
				String EndData 					= etx + "E";
				int startIdx 					= -1;
				int endIdx 						= -1;

				String decryptedString			= response;
				try {
					startIdx= response.indexOf(BeginData) + BeginData.length();
					endIdx	= response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);
					byte[] decData	= CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					byte[] arr 		= AES256Encryption.deencryptData(decData, mKey);

					if(AppConstants.atpCompress){
						try {
							decryptedString = FormatUtil.uncompress(arr, "", "a").toString();
							decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
						} catch (DataFormatException e) {
							e.printStackTrace();
						} 
					}else{
						decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
						decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
					}
				}
				DefinitionConstants.Debug("[AtpConnectUtil][ValidatePreLogin]Server decrypted response: " + decryptedString);
				decryptedString	= decryptedString == null ? "" : decryptedString;

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return decryptedString;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url				= null;
			dataBytes		= null;
			userParam		= null;
			userParamInByte	= null;
			senderCode 		= null;
			username		= null;
			strRSAPassword	= null;
			strE2EERandomNum= null;
			deviceID		= null;
			strPassword		= null;
			senderCode		= null;
		}   	

		return null;
	}
	//Added by Thinzar, Change_Request-20160722, ReqNo.8 - END

	public static String otpValidate(Map<String, Object> parameters) throws Exception {
		URL url						= null;
		byte[] dataBytes			= null;
		String response				= null;
		String urlAddress 			= null;
		String userParam 			= null;
		byte[] userParamInByte 		= null;

		if (AppConstants.brokerConfigBean.isEnableEncryption() )
			userParamInByte = (byte[]) parameters.get(ParamConstants.USERPARAM_INBYTE_TAG);
		else
			userParam		= (String) parameters.get(ParamConstants.USERPARAM_TAG);

		String clientCode 			= (String) parameters.get(ParamConstants.CLIENTCODE_TAG);
		String deviceID 			= (String) parameters.get(ParamConstants.DEVICE_ID_TAG);
		String OTPValue 			=  parameters.get(ParamConstants.OTP_VALUE_TAG).toString();

		//Added for SMS OTP enhancements
		String deviceType			= (String) parameters.get(ParamConstants.DEVICE_TYPE_2FA_TAG);
		String isTriggerSMS 		= (String) parameters.get(ParamConstants.IS_TRIGGER_SMS_TAG);
		String challengeCode 		= (String) parameters.get(ParamConstants.CHALLENGE_CODE_2FA_TAG);

		try{
			if( ! AppConstants.brokerConfigBean.isEnableEncryption() ){
				urlAddress			= "'=" + clientCode + "|+=" + deviceID  + "|,=" + OTPValue;

				//Change_Request-20170601, ReqNo.10
				if(deviceType != null) {
					urlAddress += "|1=" + deviceType;
					if (deviceType.equals(AppConstants.DEV_TYPE_2FA_SMS)) {
						urlAddress += "|2=" + isTriggerSMS;

						if (isTriggerSMS != null && isTriggerSMS.equalsIgnoreCase("N")) {
							urlAddress += "|4=" + challengeCode;
						}
					}
				}

				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOTP]urlAddress: " + urlAddress);

				url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(), AppConstants.brokerConfigBean.getIntAtpPort(), "/[" + userParam + "]ValidateOTP?" + urlAddress);//encodedURL);

				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOTP]Url address: " + url.toString() );

				dataBytes 			= readUrlData(url);
				response 			= new String(dataBytes, AppConstants.STRING_ENCODING_FORMAT);

				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOTP]Server response: " + response);

				return response;
			}else{

				String encryptionKey		= AppConstants.getEncryptedKey();
				byte[] mKey 				= AES256Encryption.HashSHA256Generator(encryptionKey);
				String InputParam 			= "'=" + clientCode + "|+=" + deviceID  + "|,=" + OTPValue;

				//Change_Request-20170601, ReqNo.10
				if(deviceType != null) {
					InputParam += "|1=" + deviceType;
					if (deviceType.equals(AppConstants.DEV_TYPE_2FA_SMS)) {
						InputParam += "|2=" + isTriggerSMS;

						if (isTriggerSMS != null && isTriggerSMS.equalsIgnoreCase("N")) {
							InputParam += "|4=" + challengeCode;
						}
					}
				}

				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOTP]InputParam: " + InputParam );
				byte[] EData 				= AES256Encryption.encryptData(InputParam.trim().getBytes(AppConstants.STRING_ENCODING_FORMAT), mKey);
				String encryptedInputParam	= CustomizedBase64.encodeBytes(EData);

				String base64EncodedUserParam = CustomizedBase64.encodeBytes(userParamInByte);
				String stx = Character.toString(startText);
				String etx = Character.toString(endText);

				String urlAdd = "10|"+stx+base64EncodedUserParam+"|"+encryptedInputParam+etx+"E";

				String encodedURL = URLEncoder.encode(urlAdd,AppConstants.STRING_ENCODING_FORMAT);

				url = new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/[0]ValidateOTP?"+encodedURL);
				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOTP]Url address: " + url.toString() );

				dataBytes = readUrlData(url);
				response = new String(dataBytes,AppConstants.STRING_ENCODING_FORMAT);

				String BeginData = "10|"+stx;
				String EndData = etx+"E";

				int startIdx = -1;
				int endIdx = -1;
				String decryptedString			= response;

				try {
					startIdx = response.indexOf(BeginData) + BeginData.length();
					endIdx = response.indexOf(EndData);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (startIdx != -1 && endIdx != -1) {
					String sSubData = response.substring(startIdx, endIdx);

					byte[] decData = null;

					try {
						decData = CustomizedBase64.decode(sSubData.getBytes(AppConstants.STRING_ENCODING_FORMAT));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] arr = AES256Encryption.deencryptData(decData, mKey);

					decryptedString = new String(arr, AppConstants.STRING_ENCODING_FORMAT);
					decryptedString	= response.indexOf(BeginData) > 0 ? response.substring( 0,  response.indexOf(BeginData) ) + decryptedString : decryptedString;
				}
				DefinitionConstants.Debug("[AtpConnectUtil][ValidateOTP]Server response: " + decryptedString);

				if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(decryptedString)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(decryptedString) )
					throw new FailedAuthenicationException(decryptedString);
				else if( ErrorCodeConstants.isATPBackendResponseErrMsg(decryptedString) )
					throw new Exception(decryptedString);
				else if( ErrorCodeConstants.isBackendExceptionMsg(decryptedString) )
					throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

				return decryptedString;
			}

		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
		}

		return null;
	}

	//Change_Request-20170601, ReqNo.14 - START
	public static PublicKey tradeGetPK2A() throws Exception {
		URL url				= null;
		byte[] dataBytes	= null;
		String response		= null;
		PublicKey PubKey	= null;
		try{

			String strURLEncoded	= URLEncoder.encode("+=1|" + FormatUtil.formatDateString(new Date(), "yyyyMMddhhmmss.S"), AppConstants.STRING_ENCODING_FORMAT);
			url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "TradeGetPK2A?" + strURLEncoded);

			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetPK2A]Url address: " + url.toString() );
			dataBytes	= readUrlData(url);
			response	= new String(dataBytes);
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetPK2A]response: " + response);

			if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

			Map<String, String> loadBalanceMap	= AtpMessageParser.parseTradeGetPK2A(response);
			String public_key_data		= loadBalanceMap.get(ParamConstants.PUBLIC_KEY_TAG);
			String public_ip_data		= loadBalanceMap.get(ParamConstants.PUBLIC_IP_TAG);
			//String private_ip_data	= loadBalanceMap.get(ParamConstants.PRIVATE_IP_TAG);
			String secondary_port_data   = loadBalanceMap.get(ParamConstants.SECONDARY_PORT_TAG);

			String decryptedPublicIP    = decodeBase64Data(public_ip_data);
			String secondaryPort 		= decodeBase64Data(secondary_port_data);

			if(decryptedPublicIP != null && decryptedPublicIP.length() > 0){
				DefinitionConstants.Debug("DecryptedPublicIP:" + decryptedPublicIP);
				AppConstants.brokerConfigBean.setStrAtpIP(decryptedPublicIP);
			}

			if(secondaryPort != null && secondaryPort.length() > 0){
				DefinitionConstants.Debug("Decrypted secondary port:" + secondaryPort);
				AppConstants.brokerConfigBean.setIntAtpPort(Integer.parseInt(secondaryPort));
			}

			PubKey	= FormatUtil.loadPublicKey(public_key_data);

			return PubKey;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
			// Mary@20130705 - public key error - START
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (NoSuchAlgorithmException nsae) {
			throw nsae;
		} catch (InvalidKeySpecException ikse) {
			throw ikse;
			// Mary@20130705 - public key error - END
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
			response	= null;
		}

		return null;
	}

	public static String decodeBase64Data(String encryptedString) throws IOException {

		String decryptedString = null;
		byte[] decData;

		try {
			decData = CustomizedBase64.decode(encryptedString.getBytes(AppConstants.STRING_ENCODING_FORMAT));
			decryptedString = new String(decData, AppConstants.STRING_ENCODING_FORMAT);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return decryptedString;
	}

	public static String[] tradeGetKey2A(PublicKey key) throws Exception{
		URL url				= null;
		byte[] dataBytes	= null;
		String response		= null;
		byte[] cipherText	= null;
		Cipher cipher		= null;
		try{
			String RandomKey 		= UUID.randomUUID().toString();
			cipher 					= Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText 				= cipher.doFinal(RandomKey.getBytes());
			String sha256Hash 		= FormatUtil.HashSHA256Generator(RandomKey);
			String sha256Hash2 		= FormatUtil.HashSHA256Generator(sha256Hash);
			String base64EncodedText= CustomizedBase64.encodeBytes(cipherText);

			url 		= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetKey2A?" + URLEncoder.encode(base64EncodedText));
			DefinitionConstants.Debug( "[AtpConnectUtil][tradeGetKey2A]Url address: " + url.toString() );
			dataBytes 	= readUrlData(url);
			response	= new String(dataBytes);
			DefinitionConstants.Debug("[AtpConnectUtil][tradeGetKey2A] Server response: " + response);

			if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

			byte[] decodedResult	= CustomizedBase64.decode(dataBytes);

			String result 			= FormatUtil.asHex(decodedResult);

			if(result.equals(sha256Hash2)){
				String arr[] = {RandomKey, base64EncodedText};
				return arr;
			}else{
				return null;
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			url			= null;
			dataBytes	= null;
			response	= null;
			cipherText	= null;
			cipher		= null;
		}
		return null;
	}

	public static byte[] tradeGetSession2A(String[] AESKey) throws Exception {
		URL url 		= null;
		byte[] bytes	= null;
		String response	= null;
		byte[] mKey 	= null;

		try {
			String stx 			= Character.toString(startText);
			String etx 			= Character.toString(endText);
			mKey 				= AES256Encryption.HashSHA256Generator(AESKey[0]);

			url 				= new URL(getAtpProtocol(), AppConstants.brokerConfigBean.getStrAtpIP(),  AppConstants.brokerConfigBean.getIntAtpPort(), "/TradeGetSession2A?"+ URLEncoder.encode(AESKey[1]));
			DefinitionConstants.Debug("[AtpConnectUtil][tradeGetSession2A]Url address:" + url.toString() );
			bytes				= readUrlData(url);
			response			= new String(bytes, AppConstants.STRING_ENCODING_FORMAT);

			if ( ErrorCodeConstants.isInvalidATPSessionErrMsg(response)  || ErrorCodeConstants.isInvalidATPEncryptionKeyErrMsg(response) )
				throw new FailedAuthenicationException(response);
			else if( ErrorCodeConstants.isATPBackendResponseErrMsg(response) )
				throw new Exception(response);
			else if( ErrorCodeConstants.isBackendExceptionMsg(response) )
				throw new Exception(ErrorCodeConstants.FAIL_BACKEND_PROCESS);

			String BeginData	= "10|"+stx;
			String EndData 		= etx+"E";
			int startIdx 		= -1;
			int endIdx 			= -1;

			try{
				startIdx	= response.indexOf(BeginData) + BeginData.length();
				endIdx 		= response.indexOf(EndData);
			}catch (Exception e){
				e.printStackTrace();
			}

			if(startIdx != -1 && endIdx != -1){
				String sSubData	= response.substring(startIdx, endIdx);
				byte[] decData	= CustomizedBase64.decode( sSubData.getBytes() );

				return AES256Encryption.deencryptData(decData, mKey);
			}
		}catch(FailedAuthenicationException fae) {
			fae.printStackTrace();
			throw fae;
		}catch(EOFException eof){
			eof.printStackTrace();
		}catch(MalformedURLException mue) {
			mue.printStackTrace();
			throw new MalformedURLException(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
		}catch (UnsupportedEncodingException uee){
			uee.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
			if( ErrorCodeConstants.isOwnException( e.getMessage() ) )
				throw e;
			else
				throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
		}finally{
			response= null;
			bytes	= null;
			url 	= null;
			mKey 	= null;
		}
		return null;
	}
	//Change_Request-20170601, ReqNo.14 - END
}