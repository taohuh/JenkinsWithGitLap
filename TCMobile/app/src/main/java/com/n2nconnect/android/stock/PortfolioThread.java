package com.n2nconnect.android.stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.n2nconnect.android.stock.model.StockHolding;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;

public class PortfolioThread extends Thread {
	
	private String senderCode;
	private TradeAccount account;
	private OnPortfolioListener registeredListener;
	private String userParam;
	private String atpuserParam;
	private Context context;
	private int refreshRate;
	private volatile boolean stopRequested; 
	private Thread runThread;
	private List<StockHolding> stockHoldings = null;
	private boolean mPaused;
	// Added by Mary@20130705 - Fixes_Request-20130523, ReqNo.27
	private int intPortfolioType;
	
	//  Added by Mary@20130705 - Fixes_Request-20130523, ReqNo.27 - START
	public int getIntPortfolioType(){
		return intPortfolioType;
	}
	public void setIntPortfolioType(int intPortfolioType){
		this.intPortfolioType	= intPortfolioType;
	}
	//  Added by Mary@20130705 - Fixes_Request-20130523, ReqNo.27 - END
		
	public int getRefreshRate() {
		return refreshRate;
	}

	public void setRefreshRate(int refreshRate) {
		this.refreshRate = refreshRate;
	}

	public OnPortfolioListener getRegisteredListener() {
		return registeredListener;
	}

	public void setRegisteredListener(OnPortfolioListener registeredListener) {
		this.registeredListener = registeredListener;
	}

	public String getUserParam() {
		return userParam;
	}
	
	public void setTradeAccount(TradeAccount account){
		this.account = account;
	}

	public void setUserParam(String userParam) {
		this.userParam = userParam;
	}
	public interface OnPortfolioListener {
		public void refreshStockEvent(List<StockHolding> symbolsData, List<StockSymbol> portfolioData);
		public void atpsessionExpired();
	}
	
	/* Mary@20130705 - Fixes_Request-20130523, ReqNo.27
	public PortfolioThread(String senderCode, TradeAccount account, String atpuserParam, String userParam, int refreshRate) {
	*/
	public PortfolioThread(String senderCode, TradeAccount account, String atpuserParam, String userParam, int refreshRate, int intPortfolioType) {
		mPaused 				= false;
		this.senderCode 		= senderCode;
		this.account 			= account;
		this.atpuserParam 		= atpuserParam;
		this.userParam 			= userParam;
		this.refreshRate 		= refreshRate;
		this.stopRequested 		= false;
		// Added by Mary@20130705 - Fixes_Request-20130523, ReqNo.27
		this.intPortfolioType 	= intPortfolioType;
	}
	
	public void run() {
		
		while (!stopRequested) {
			
			synchronized (this) {
				while (mPaused) {
					try {
						wait();
					} catch (InterruptedException e) {
					}
				}
			}
			
			try {
				
				runThread = Thread.currentThread();
				
				if (refreshRate > 0) {
					long waitTime = refreshRate * 1000;
					Thread.sleep(waitTime);
				} else if (refreshRate == 0) {
					Thread.sleep(AppConstants.DEFAULT_REFRESH_RATE * 1000);
					continue;
				} else {				
					Thread.sleep(AppConstants.DEFAULT_REFRESH_RATE * 1000);
				}
				
				Map<String, Object> parameters = new HashMap<String, Object>();
				
				/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3 - START
				if(AppConstants.EnableEncryption==false){
					parameters.put(ParamConstants.USERPARAM_TAG, atpuserParam);
				}else{
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				}
				*/
				/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
				if(AppConstants.EnableEncryption)
				*/
				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, atpuserParam);
				// Mary@20121005 - Fixes_Request-20120815, ReqNo.3 - END
				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
				parameters.put(ParamConstants.BROKERCODE_TAG, account.getBrokerCode());
				parameters.put(ParamConstants.ACCOUNTNO_TAG, account.getAccountNo());
				parameters.put(ParamConstants.BRANCHCODE_TAG, account.getBranchCode());
				// Added by Mary@20130705 - Fixes_Request-20130523, ReqNo.27
				parameters.put(ParamConstants.PARAM_PORTFOLIOTYPE_TAG, intPortfolioType);

				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - NOTE : not perform retry for thread
				try {
					String response = AtpConnectUtil.tradePortfolio(parameters);
					stockHoldings = AtpMessageParser.parseTradePortfolio(response);

				} catch (FailedAuthenicationException e) {
					e.printStackTrace();
					if (registeredListener != null) {
						registeredListener.atpsessionExpired();
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				String[] symbolCodes = new String[stockHoldings.size()];

				int index = 0;

				for(Iterator<StockHolding> itr=stockHoldings.iterator(); itr.hasNext();){
					StockHolding counter 	= itr.next();
					//symbolCodes[index] 	= counter.getSymbolCode();
					String strTemp			= counter.getSymbolCode();
					/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
					String strTrdExchgCode	= ( strTemp.split("\\.") )[\1];
					*/
					String strTrdExchgCode	= strTemp.substring( (strTemp.lastIndexOf(".") + 1), strTemp.length() );
					for(int i=0; i<DefinitionConstants.getTrdExchangeCode().size(); i++){
						String strCurrTrdExchgCode	= DefinitionConstants.getTrdExchangeCode().get(i);
						if( strTrdExchgCode.equals(strCurrTrdExchgCode) ){
							String strCurrQCExchgCode	= DefinitionConstants.getValidQCExchangeCodes().get(i);
							strTemp						= strTemp.replace("." + strTrdExchgCode, "." + strCurrQCExchgCode);
							break;
						}
					}
					counter.setSymbolCode(strTemp);
					symbolCodes[index] 		= strTemp;
					
					index++;
				}
				
				List refreshStocks = null;

				if (symbolCodes.length != 0) {
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					parameters = new HashMap();
					parameters.put(ParamConstants.USERPARAM_TAG, userParam);
					parameters.put(ParamConstants.SYMBOLCODES_TAG, symbolCodes);
					
					String response = QcHttpConnectUtil.imageQuote(parameters, CommandConstants.IMAGE_QUOTE_REFRESH);  
					*/
					String response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_REFRESH);  // NOTE : no retry for function executed in thread
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					
					if(response!=null){
						refreshStocks = QcMessageParser.parseStockRefresh(response);
					}
					
					// Added by Mary@20120919 - Change_Request-20120719, ReqNo.12 - START
					for( Iterator itr = stockHoldings.iterator(); itr.hasNext(); ){
						StockHolding counter	= (StockHolding) itr.next();
						for( Iterator itr2 = refreshStocks.iterator(); itr2.hasNext(); ){
							StockSymbol stock	= (StockSymbol) itr2.next();
							if( counter.getSymbolCode().equals( stock.getSymbolCode() ) ){
								counter.setIntLotSize( stock.getIntLotSize() );
								break;
							}
						}
					}
					// Added by Mary@20120919 - Change_Request-20120719, ReqNo.12 - END
				} else {
					refreshStocks = new ArrayList();
				}
				
				if (registeredListener != null) {
					registeredListener.refreshStockEvent(stockHoldings,refreshStocks);
				}
		        
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (FailedAuthenicationException e) {				
				e.printStackTrace();
				try {
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				 	List<String> data = QcHttpConnectUtil.relogin();
					userParam = data.get(0);
					*/
					AppConstants.QCdata	= QcHttpConnectUtil.relogin();
					userParam	= AppConstants.QCdata.getStrUserParam();
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}catch(Exception e2){
					e2.printStackTrace();
		        }
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public void stopRequest() {
        stopRequested = true;

        if (runThread != null ) {
            runThread.interrupt();
        }
    }
	
	public void pauseRequest() {
		synchronized (this){
			mPaused = true;
		}
	}

	public void resumeRequest() {
		synchronized (this){
			mPaused = false;
			notify();
		}
	}
	
}
