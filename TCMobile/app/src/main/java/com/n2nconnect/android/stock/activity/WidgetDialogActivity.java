package com.n2nconnect.android.stock.activity;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.custom.QustomDialogBuilder;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.Window;

public class WidgetDialogActivity extends Activity {
	private String strTitle ;
	private String strMsg;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		Intent intent= getIntent();
		if(intent.getExtras()!=null){
			strTitle =intent.getStringExtra("title");
			strMsg 	=intent.getStringExtra("message");
		}

		String appName= getApplicationContext().getResources().getString(R.string.app_name);
		WidgetDialogActivity.this.setFinishOnTouchOutside(false);

		//ContextThemeWrapper ctw = new ContextThemeWrapper(WidgetDialogActivity.this, R.style.AlertDialogCustom);
		/*
		AlertDialog.Builder builder	= new AlertDialog.Builder(WidgetDialogActivity.this);

		builder.setTitle(appName)
		//.setIcon(R.drawable.app_icon_push)			//32x32
		.setMessage(strTitle + " " + strMsg)
		.setPositiveButton("Open App", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,	int id) {
				try{
					dialog.dismiss();
					String packageName = getApplicationContext().getPackageName();
					Intent LaunchIntent = getApplicationContext().getPackageManager()
							.getLaunchIntentForPackage(packageName);
					LaunchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(LaunchIntent);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		})
		.setNegativeButton("Close", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,	int id) {
				try{
					dialog.dismiss();
					SharedPreferences	sharedPreferences = WidgetDialogActivity.this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
					boolean isLogout= sharedPreferences.getBoolean(PreferenceConstants.IS_APP_EXIT, false);
					if(isLogout==true){
						Intent intents = new Intent(Intent.ACTION_MAIN);
						intents.addCategory(Intent.CATEGORY_HOME);
						intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						intents.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
						intents.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						intents.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intents);	
					}else{
						String packageName = getApplicationContext().getPackageName();
						Intent LaunchIntent = getApplicationContext().getPackageManager()
								.getLaunchIntentForPackage(packageName);
						LaunchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(LaunchIntent);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});

		AlertDialog alert	= builder.create();
		
		alert.show();
		 */
		if(strMsg.contains("rejected"))
			strMsg = strMsg.replaceAll("rejected", "<b>"+"rejected"+"</b>");
		System.out.println("strMsg:" + strMsg);

		@SuppressLint("ResourceType")	//to temporarily fix the apk signing error "expected resource of type string"
		QustomDialogBuilder qustomDialogBuilder = (QustomDialogBuilder) new QustomDialogBuilder(WidgetDialogActivity.this)
		.setTitle(appName)
		.setTitleColor(getResources().getString(R.color.primary_theme_color))
		.setDividerColor(getResources().getString(R.color.primary_theme_color))
		.setMessage(Html.fromHtml(strTitle + " " + strMsg))
		.setPositiveButton(getResources().getString(R.string.dialog_button_open_app), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,	int id) {
				try{
					dialog.dismiss();
					String packageName = getApplicationContext().getPackageName();
					Intent LaunchIntent = getApplicationContext().getPackageManager()
							.getLaunchIntentForPackage(packageName);
					LaunchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(LaunchIntent);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		})
		.setNegativeButton(getResources().getString(R.string.dialog_button_close), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,	int id) {
				try{
					dialog.dismiss();
					SharedPreferences	sharedPreferences = WidgetDialogActivity.this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
					boolean isLogout= sharedPreferences.getBoolean(PreferenceConstants.IS_APP_EXIT, false);
					if(isLogout==true){
						Intent intents = new Intent(Intent.ACTION_MAIN);
						intents.addCategory(Intent.CATEGORY_HOME);
						intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						intents.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
						intents.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						intents.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intents);	
					}else{
						String packageName = getApplicationContext().getPackageName();
						Intent LaunchIntent = getApplicationContext().getPackageManager()
								.getLaunchIntentForPackage(packageName);
						LaunchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(LaunchIntent);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		
		qustomDialogBuilder.show();
	}

	@Override
	public void onStop() {
		super.onStop();
		System.gc();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}