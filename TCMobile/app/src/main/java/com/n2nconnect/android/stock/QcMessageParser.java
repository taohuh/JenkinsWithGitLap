package com.n2nconnect.android.stock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.WordUtils;

import com.n2nconnect.android.stock.model.BusinessDone;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.MarketDepth;
import com.n2nconnect.android.stock.model.MarketSummary;
import com.n2nconnect.android.stock.model.NewsCategory;
import com.n2nconnect.android.stock.model.Sector;
import com.n2nconnect.android.stock.model.StockInfo;
import com.n2nconnect.android.stock.model.StockNews;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TimeSales;
import com.n2nconnect.android.stock.util.FormatUtil;

public class QcMessageParser {

	// Added by Mary@20130517 - Fixes_Request-20130424, ReqNo.9 - START
	public static String parseGetNewKey(String strRawInput){
		String strUserParamKey	= null;

		StringTokenizer tokens  = new StringTokenizer(strRawInput, "\r\n");
		while( tokens.hasMoreTokens() ){
			String strTemp 	= tokens.nextToken();
			if(strTemp != null && strTemp.trim().length() > 0
					&& strTemp.indexOf("[]") == -1
					&& strTemp.indexOf("[END]") == -1
					){
				strUserParamKey	= strTemp.trim();
				break;
			}
		}

		return strUserParamKey;
	}

	// Added by Mary@20130517 - Fixes_Request-20130424, ReqNo.9 - END

	public static Map<String, ExchangeInfo> parseExchangeSector(String message) {

		Map<String, ExchangeInfo> exchangeInfos = new HashMap<String, ExchangeInfo>();

		StringTokenizer tokens = new StringTokenizer(message, "\r\n");

		while (tokens.hasMoreTokens()) {
			String lineItem = tokens.nextToken();
			String[] columns = lineItem.split("=");
			String key = columns[0];
			if (columns.length > 1) {
				String value = columns[1];

				if (key.equals("[Exchange]")) { // e.g. [Exchange]=KL,MY
					StringTokenizer items = new StringTokenizer(value, ",");

					while (items.hasMoreTokens()) {
						String exchangeCode = items.nextToken();
						ExchangeInfo exchange = new ExchangeInfo();
						exchange.setQCExchangeCode(exchangeCode);
						exchangeInfos.put(exchangeCode, exchange);
					}
				} else if (key.equals("[Sector]")) {		// e.g. [Sector]=KL,10,[All Stocks]
					StringTokenizer items 	= new StringTokenizer(value, ",");
					int index 				= 0;
					ExchangeInfo info 		= null;
					Sector sector 			= new Sector();

					while (items.hasMoreTokens()) {
						String item = items.nextToken();

						if (index == 0) {
							info = (ExchangeInfo) exchangeInfos.get(item);
						} else if (index == 1) {
							sector.setSectorCode(item);
						} else if (index==2) {
							String name = item.replace("[", "").replace("]", "");
							sector.setSectorName(name.toUpperCase());
						}
						index++;
					}
					if (info != null) {
						info.addSector(sector);
					}
				}else if (key.equals("[Indices]")) {		// e.g. [Indices]=KL,020000000.KL
					StringTokenizer items 	= new StringTokenizer(value, ",");
					if (items.countTokens() == 2) {
						ExchangeInfo info 	= null;
						int index 			= 0;
						while (items.hasMoreTokens()) {
							String item = items.nextToken();
							if (index == 0)
								info = (ExchangeInfo) exchangeInfos.get(item);
							else if (index == 1){
								//Edited by Thinzar, if Feed doesn't return the exchange, this could lead to exception
								if(info != null)
									info.setIndiceSymbol(item);
							}

							index++;
						}
					}
				}
			}
		}

		return exchangeInfos;
	}

	public static List<Sector> parseSector(String message) {
		List<Sector> sectors	= new ArrayList<Sector>();
		StringTokenizer tokens	= new StringTokenizer(message, "\r\n");


		while (tokens.hasMoreTokens()) {
			String sectorData 		= tokens.nextToken();
			StringTokenizer columns = new StringTokenizer(sectorData, ",");

			Sector sector			= new Sector();
			int index 				= 0;
			/* Mary@20130927 - Fixes_Request-20130711, ReqNo.19
			boolean toExclude 		= false;
			*/

			while (columns.hasMoreTokens()) {
				String column = columns.nextToken();

				/* Mary@20130927 - Fixes_Request-20130711, ReqNo.19
				if (index == 0 && column.equals("10000")) {
					toExclude = true;
					// continue;
				}
				*/

				switch (index) {
					case 0:
						sector.setSectorCode(column); break;
					case 1:
						sector.setSectorName(column); break;
					case 2:
						sector.setParentCode(column); break;
					//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
					case 3:
						sector.setIndexCount((Integer.parseInt(column)));break;
					//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
				}
				index++;
			}

			/* Mary@20130927 - Fixes_Request-20130711, ReqNo.19
			if (!toExclude)
			*/
			//If filter added by Thinzar, Fixes_Request20150401, ReqNo. 5, to remove "TOTAL" from sector	//this casuses "pull to refresh" malfunction, undo for the time being
			//if(Integer.parseInt(sector.getSectorCode()) != 10000)
			sectors.add(sector);

		}

		Map<String, Sector> parents = new HashMap<String, Sector>();

		for (Iterator<Sector> itr=sectors.iterator(); itr.hasNext(); ) {
			Sector sector = itr.next();
			if (sector.getParentCode().equals("0")) {
				parents.put(sector.getSectorCode(), sector);
			}
		}
		for (Iterator<Sector> itr=sectors.iterator(); itr.hasNext(); ) {
			Sector sector = itr.next();
			if (!sector.getParentCode().equals("0")) {
				Sector parent = parents.get(sector.getParentCode());
				if (parent != null) {
					// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.4 - START
					if( parent.getChildSectors().size() == 0 ){
						Sector defaultSector = new Sector();
						defaultSector.setSectorCode( parent.getSectorCode() );
						defaultSector.setSectorName("All");
						defaultSector.setParentCode( parent.getSectorCode() );
						//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
						defaultSector.setIndexCount(parent.getIndexCount() );
						//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
						parent.addChildSector(defaultSector);
						defaultSector.setParentSector(parent);
					}
					// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.4 - END

					parent.addChildSector(sector);
					sector.setParentSector(parent);
				}
			}
		}

		List<Sector> sectorList = new ArrayList<Sector>();
		sectorList.addAll(parents.values());
		Collections.sort(sectorList);

		return sectorList;
	}

	public static List<StockSymbol> parseSymbolSearch(String message) {
		List<StockSymbol> symbols	= new ArrayList<StockSymbol>();
		StringTokenizer tokens 		= new StringTokenizer(message, "\r\n");

		while (tokens.hasMoreTokens()) {
			String stockData 		= tokens.nextToken();
			StringTokenizer columns = new StringTokenizer(stockData, ",");
			StockSymbol symbol 		= new StockSymbol();

			int index = 0;
			while (columns.hasMoreTokens()) {
				String column = columns.nextToken();

				switch (index) {
					case 0: symbol.setSymbolCode(column); break;
					case 1: symbol.setStockCode(column); break;
				}

				index++;
			}

			symbols.add(symbol);
		}

		return symbols;
	}

	public static MarketSummary parseMarketSummary(String message) {
		message= message.lastIndexOf(",") == (message.length() - 1) ? message + " " : message;
		message= message.indexOf(",") == 0 ? " " + message : message;
		message= message.replaceAll(",,", ", ,");
		/* Mary@20130604 - comment unused variable
		List symbols = new ArrayList();
		*/

		StringTokenizer tokens = new StringTokenizer(message, "\r\n");
		String marketInsert = null;
		String marketSummary = null;
		MarketSummary summary = new MarketSummary();

		while (tokens.hasMoreTokens()) {

			String stockData = tokens.nextToken();
			if (stockData.equals("[INSERT]")) {
				marketInsert = tokens.nextToken();
			}
			if (stockData.equals("[SUMMARY]")) {
				marketSummary = tokens.nextToken();
			}
		}

		if (marketInsert!= null) {

			StringTokenizer columns = new StringTokenizer(marketInsert, ",");

			int index = 0;
			while (columns.hasMoreTokens()) {
				String column = columns.nextToken();

				switch (index) {
					case 0: summary.setSymbolCode(column); break;
					case 1: summary.setStockCode(column); break;
					case 2: summary.setLACP(Float.parseFloat(column)); break;
					case 3: summary.setLastDone(Float.parseFloat(column)); break;
					case 4: summary.setHigh(Float.parseFloat(column)); break;
					case 5: summary.setLow(Float.parseFloat(column)); break;
				/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
				// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
				case 6: summary.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "1" : column.trim() ) ); break;
				*/
					case 6: summary.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "0" : column.trim() ) ); break;
				}

				index++;
			}
		}

		if (marketSummary!= null) {
			StringTokenizer columns = new StringTokenizer(marketSummary, ",");

			int index = 0;
			while (columns.hasMoreTokens()) {
				String column = columns.nextToken();

				switch (index) {
					case 0: summary.setExchangeCode(column); break;
					case 2: summary.setUpCounter(Integer.parseInt(column)); break;
					case 3: summary.setDownCounter(Integer.parseInt(column)); break;
					case 4: summary.setUnchangeCounter(Integer.parseInt(column)); break;
					case 5: summary.setUntradedCounter(Integer.parseInt(column)); break;
					case 6: summary.setTotalCounter(Integer.parseInt(column)); break;
					case 7: summary.setVolume(Long.parseLong(column)); break;
					case 8: summary.setValue(Double.parseDouble(column)); break;
					case 9: if(!column.equals(" ") && column!=null){summary.setTotalTrade(Long.parseLong(column)); break;}break;
				}

				index++;
			}
		}

		return summary;
	}

	public static List<StockSymbol> parseIndices(String message) {
		message= message.lastIndexOf(",") == (message.length() - 1) ? message + " " : message;
		message= message.indexOf(",") == 0 ? " " + message : message;
		message= message.replaceAll(",,", ", ,");

		List<StockSymbol> symbols = new ArrayList<StockSymbol>();
		StringTokenizer tokens = new StringTokenizer(message, "\r\n");
		//	StringTokenizer tokens = new StringTokenizer("00010000002.KL,CONSUMER.KL,23.3,343.3,3434.43,3434,343.3,9099,", "\r\n");

		while (tokens.hasMoreTokens()) {
			String stockData = tokens.nextToken();

			if (stockData.equals("[SUMMARY]")) {
				break;
			}
			//StringTokenizer columns = new StringTokenizer("00010000002.KL,CONSUMER.KL,23.3,343.3,3434.43,3434,3443,,", ",");

			StringTokenizer columns = new StringTokenizer(stockData, ",");
			StockSymbol symbol = new StockSymbol();

			int index = 0;
			while (columns.hasMoreTokens()) {
				String column = columns.nextToken();
				switch (index) {
					case 0: symbol.setSymbolCode(column); break;
					case 1: symbol.setStockCode(column); break;
					case 2: symbol.setPreviousClose(Float.parseFloat(column)); break;
					case 3: symbol.setLACP(Float.parseFloat(column)); break;
					case 4: symbol.setLastDone(Float.parseFloat(column)); break;
					case 5: symbol.setOpen(Float.parseFloat(column)); break;
					case 6: symbol.setHigh(Float.parseFloat(column)); break;
					case 7: symbol.setLow(Float.parseFloat(column)); break;
					case 8: if(!column.equals(" ") && column!=null){symbol.setTotalTrade(Long.parseLong(column));break;}break;
				}

				index++;
			}

			symbols.add(symbol);
		}

		return symbols;

	}

	public static List<StockSymbol> parseStockSymbol(String message) {
		List<StockSymbol> symbols = new ArrayList<StockSymbol>();
		StringTokenizer tokens = new StringTokenizer(message, "\r\n");

		while (tokens.hasMoreTokens()) {
			try{
				//String stockData = tokens.nextToken();

				String stockSymbol = tokens.nextToken().trim();
				String[] columns = stockSymbol.split(",");
				//	StringTokenizer columns = new StringTokenizer(stockData, ",");
				StockSymbol symbol = new StockSymbol();

				//int index = 0;

				for (int i=0; i<columns.length; i++) {
					String column = columns[i];
					//		while (columns.hasMoreTokens()) {
					//			String column = columns.nextToken();

					switch (i) {
						case 0: symbol.setSymbolCode(column); break;
						case 1: symbol.setStockCode(column); break;
						case 2: symbol.setCompanyName(column); break;
						case 3: symbol.setPreviousClose(Float.parseFloat(column)); break;
						case 4: symbol.setLACP(Float.parseFloat(column)); break;
						case 5: symbol.setOpen(Float.parseFloat(column)); break;
						case 6: symbol.setHigh(Float.parseFloat(column)); break;
						case 7: symbol.setLow(Float.parseFloat(column)); break;
						case 8: symbol.setLastDone(Float.parseFloat(column)); break;
						case 9:
						/* Mary@20130822 - Fixes_Request-20130711, ReqNo.15
						/* Mary@20130620 - To be configurable according to broker
						if(AppConstants.isJavaQC)
						/
						if( AppConstants.brokerConfigBean.isConnectJavaQC() ){
							if(column.indexOf(".")!=-1){
								String[] columnSplit = column.split("\\.");symbol.setVolume(Long.parseLong(columnSplit[0]));
								break;
							}
						}else{
						*/
							if(column!=null && column.trim().length() > 0){
								symbol.setVolume(Long.parseLong(column));
								break;
							}
						case 10:if(column!=null)
							symbol.setValue(Double.parseDouble(column));
						else
							symbol.setValue(0);
							break;
						case 11:if(column!=null && !column.equals(""))
							symbol.setTotalTrade(Integer.parseInt(column));
						else symbol.setTotalTrade(0);
							break;
						case 12:symbol.setCurrency(column); break;
					/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
					// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
					case 13: symbol.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "1" : column.trim() ) ); break;
					*/
						case 13: symbol.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "0" : column.trim() ) ); break;
					}

				}

				symbols.add(symbol);

			}catch (Exception ex) {
				ex.printStackTrace();
				continue;
			}

		}

		return symbols;

	}

	public static List<StockNews> parseNewsUpdate(String message) {
		Map<String, StockNews> newsUpdates	= new HashMap<String, StockNews>();
		StringTokenizer tokens				= new StringTokenizer(message, "\r\n");

		while (tokens.hasMoreTokens()) {
			try {
				String newsItem 	= tokens.nextToken().trim();

				// StringTokenizer columns = new StringTokenizer(newsItem, ",");
				String[] columns	= newsItem.split(",");
				StockNews news 		= new StockNews();

				for (int i=0; i<columns.length; i++) {
					String column = columns[i];

					switch (i) {
						case 0: news.setExchangeCode(column); break;
						case 1: news.setSymbolCode(column); break;
						case 2: news.setNewsId(column); break;
						/* Mary@20130301 - Fixes_Request-20130108, ReqNo.17
						case 3: news.setNewsTitle(column); break;
						*/
						case 3: news.setNewsTitle( WordUtils.capitalizeFully(column) ); break;
						case 4:
							if(column.length() == 22)
								news.setNewsDate( FormatUtil.parseDateString(column, FormatUtil.NEWSDATE_FORMAT_2) );
							else if(column.length() == 19)
								news.setNewsDate( FormatUtil.parseDateString(column, FormatUtil.NEWSDATE_FORMAT_1) );
							break;
						case 7:
							// Added by Mary@20130226 - Change_Request-20130225, ReqNo.2
							column	= column == null? "-1" : (column.trim().length() == 0 ? "-1" : column.trim() );
							news.setCategoryId(column);
							break;
						// Added by Mary@20130910 - Change_Request-20130724, ReqNo.13 - START
						/* Mary@20130924 - Change_Request-20130724, ReqNo.13
						case 10:
						*/
						case 11:
							news.setSymbolName(column); break;
						// Added by Mary@20130910 - Change_Request-20130724, ReqNo.13 - END
					}

				}
				if (news.getNewsDate() != null) {
					// Added by Mary@20130226 - Change_Request-20130225, ReqNo.2 - START
					if(news.getCategoryId() == null || (news.getCategoryId() != null && news.getCategoryId().trim().length() == 0) )
						news.setCategoryId("-1");
					// Added by Mary@20130226 - Change_Request-20130225, ReqNo.2 - END

					newsUpdates.put(news.getNewsId(), news);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				continue;
			}
		}
		List<StockNews> updates = new ArrayList<StockNews>();
		updates.addAll(newsUpdates.values());
		Collections.sort(updates);

		return updates;
	}

	public static StockInfo parseCurrency(String message){

		StringTokenizer columns = new StringTokenizer(message, ",");
		StockInfo info = new StockInfo();

		int index = 0;
		while (columns.hasMoreTokens()) {
			String column = columns.nextToken();
			switch (index) {
				case 1: info.setCurrency(column.trim()); break;
			}
			index++;
		}
		return info;
	}


	public static StockInfo parseStockDetail(String message) {

		// Added by Mary@20130110 - Change_Request-20130104, ReqNo.4
		// temp add return flag for is CPF Enable
		message	+=",0";

		//StringTokenizerEx columns = new StringTokenizerEx(message, ",");
		String[] columns = message.split("\\,");
		StockInfo info = new StockInfo();

		int index = 0;
		while (index<=columns.length-1) {
			String column = columns[index];
			column = column.trim();

			switch (index) {
				case 0: info.setSymbolCode(column); break;
				case 1: info.setStockCode(column); break;
				case 2: info.setCompanyName(column); break;
				case 3:
					if( isEmptyString(column) ){
						info.setPreviousClose(0); break;
					}else if( column.equals("0") ){
						info.setPreviousClose(0); break;
					}else{
						info.setPreviousClose(Float.parseFloat(column)); break;
					}
				case 4:
					if(isEmptyString(column)){
						info.setLACP(0); break;
					}else if(column.equals("0")){
						info.setLACP(0); break;
					}else{
						info.setLACP(Float.parseFloat(column)); break;
					}
				case 5:
					if(isEmptyString(column)){
						info.setOpen(0); break;
					}else if(column.equals("0")){
						info.setOpen(0); break;
					}else{
						info.setOpen(Float.parseFloat(column)); break;
					}
				case 6:
					if(isEmptyString(column)){
						info.setHigh(0); break;
					}else if(column.equals("0")){
						info.setHigh(0); break;
					}else{
						info.setHigh(Float.parseFloat(column)); break;
					}
				case 7:
					if(isEmptyString(column)){
						info.setLow(0); break;
					}else if(column.equals("0")){
						info.setLow(0); break;
					}else{
						info.setLow(Float.parseFloat(column)); break;
					}
				case 8:
					if(isEmptyString(column)){
						info.setLastDone(0); break;
					}else if(column.equals("0")){
						info.setLastDone(0); break;
					}else{
						info.setLastDone(Float.parseFloat(column)); break;
					}
				case 9:
					if(isEmptyString(column)){
						info.setVolume(0); break;
					}else if(column.equals("0")){
						info.setVolume(0); break;
					}else{
						info.setVolume(Long.parseLong(column)); break;
					}
				case 10:
					if(isEmptyString(column)){
						info.setValue(0); break;
					}else if(column.equals("0")){
						info.setValue(0); break;
					}else{
						info.setValue(Double.parseDouble(column)); break;
					}
				case 11:
					if(isEmptyString(column)){
						info.setTotalTrade(0); break;
					}else if(column.equals("0")){
						info.setTotalTrade(0); break;
					}else{
						info.setTotalTrade(Integer.parseInt(column)); break;
					}
				case 12: info.setSectorName(column); break;
				case 13:
					if(isEmptyString(column)){
						info.setSharePerLot(0); break;
					}else if(column.equals("0")){
						info.setSharePerLot(0); break;
					}else{
						info.setSharePerLot(Integer.parseInt(column)); break;
					}
				case 14:
					if(isEmptyString(column)){
						info.setShareIssued(0); break;
					}else if(column.equals("0")){
						info.setShareIssued(0); break;
					}else{
						info.setShareIssued(Long.parseLong(column)); break;
					}
				case 15:
					if(isEmptyString(column)){
						info.setNta(0); break;
					}else if(column.equals("0")){
						info.setNta(0); break;
					}else{
						info.setNta(Float.parseFloat(column)); break;
					}
				case 16:
					if(isEmptyString(column)){
						info.setParValue(0); break;
					}else if(column.equals("0")){
						info.setParValue(0); break;
					}else{
						info.setParValue(Float.parseFloat(column)); break;
					}
				case 17:
					if(isEmptyString(column)){
						info.setEps(0); break;
					}else if(column.equals("0")){
						info.setEps(0); break;
					}else{
						info.setEps(Float.parseFloat(column)); break;
					}
				case 18:
					if(isEmptyString(column)){
						info.setPeRatio(0); break;
					}else if(column.equals("0")){
						info.setPeRatio(0); break;
					}else{
						info.setPeRatio(Float.parseFloat(column)); break;
					}
				case 19:
					if(isEmptyString(column)){
						info.setInstrument(""); break;
					}else{
						info.setInstrument(column); break;
					}
				case 20:
					if(isEmptyString(column)){
						info.setFloorPrice(0); break;
					}else if(column.equals("0")){
						info.setFloorPrice(0); break;
					}else{
						info.setFloorPrice(Float.parseFloat(column)); break;
					}
				case 21:
					if(isEmptyString(column)){
						info.setCeilingPrice(0); break;
					}else if(column.equals("0")){
						info.setCeilingPrice(0); break;
					}else{
						info.setCeilingPrice(Float.parseFloat(column)); break;
					}
				case 22:
					/* Mary@20130605 - avoid IndexOutOfBoundException
					if( ! column.equals("") ){
					*/
					if( ! column.equals("") && column.length() > 1){
						/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3 - START
						char status = column.charAt(1);
						if(status == 'A'){
							info.setStatus("(A) ACTIVE"); break;
						}else if(status == 'E'){
							info.setStatus("(E) EXPIRED"); break;
						}else if(status == 'D'){
							info.setStatus("(D) DELISTED"); break;
						}else if(status == 'S'){
							info.setStatus("(S) SUSPENDED"); break;
						}else if(status == 'H'){
							info.setStatus("(H) HALTED"); break;
						}else if(status == 'B'){
							info.setStatus("(B) RESTRICTED TO BUY IN"); break;
						}else if(status == 'N'){
							info.setStatus("(N) NEW"); break;
						}else if(status == 'W'){
							info.setStatus("(W) NEW WITH INVESTOR ALERT"); break;
						}else if(status == 'T'){
							info.setStatus("(T) ACTIVE WITH INVESTOR ALERT"); break;
						}else if(status == 'Q'){
							info.setStatus("(Q) DESIGNATED"); break;
						}else if(status == 'G'){
							info.setStatus("(G) FROZEN"); break;
						}else if(status == 'V'){
							info.setStatus("(V) RESERVE");
						}else if(status == 'P'){
							info.setStatus("(P) SUSPEND WITH INVESTOR ALERT"); break;
						}else if(status == 'R'){
							info.setStatus("(R) READY"); break;
						}else if(status == 'Y'){
							info.setStatus("(Y) DELAYED"); break;
						}else if(status == 'X'){
							info.setStatus("(X) SHUTTING DOWN"); break;
						}else if(status == 'I'){
							info.setStatus("(I) IMMEDIATE DELIVERY"); break;
						}else if(status == 'L'){
							info.setStatus("(L) PENDING LISTING"); break;
						}else if(status == 'M'){
							info.setStatus("(M) PENDING ANNOUNCEMENT"); break;
						}else if(status == 'Z'){
							info.setStatus("(Z) WHEN ISSUE"); break;
						}else if(status == 'C'){
							info.setStatus("(C) CONDITIONAL TRADING"); break;
						}else if(status == 'U'){
							info.setStatus("(U) CONDITIONAL WHEN ISSUE"); break;
						}
						*/
						//Added by Thinzar@20140808 - Change_Request-20140801, ReqNo. 1 - START
						if(column.charAt(4) == 'X')	info.setCorporateActionNews(true);
						else info.setCorporateActionNews(false);
						//Added by Thinzar@20140808 - END

						//Added by Thinzar@20141113, Change_Request-20141101, ReqNo. 6 - START
						if(column.charAt(8) == 'Y')			info.setForeignLimit("YES");
						else if(column.charAt(8) == 'N')	info.setForeignLimit("NO");
						else if(column.charAt(8) == '.')	info.setForeignLimit("-");

						switch( column.charAt(1) ){
							case 'A' :
								info.setStatus("(A) ACTIVE"); break;
							case 'E' :
								info.setStatus("(E) EXPIRED"); break;
							case 'D' :
								info.setStatus("(D) DELISTED"); break;
							case 'S' :
								info.setStatus("(S) SUSPENDED"); break;
							case 'H' :
								info.setStatus("(H) HALTED"); break;
							case 'B' :
								info.setStatus("(B) RESTRICTED TO BUY IN"); break;
							case 'N' :
								info.setStatus("(N) NEW"); break;
							case 'W' :
								info.setStatus("(W) NEW WITH INVESTOR ALERT"); break;
							case 'T' :
								info.setStatus("(T) ACTIVE WITH INVESTOR ALERT"); break;
							case 'Q' :
								info.setStatus("(Q) DESIGNATED"); break;
							case 'G' :
								info.setStatus("(G) FROZEN"); break;
							case 'V' :
								info.setStatus("(V) RESERVE"); break;
							case 'P' :
								info.setStatus("(P) SUSPEND WITH INVESTOR ALERT"); break;
							case 'R' :
								info.setStatus("(R) READY"); break;
							case 'Y' :
								info.setStatus("(Y) DELAYED"); break;
							case 'X' :
								info.setStatus("(X) SHUTTING DOWN"); break;
							case 'I' :
								info.setStatus("(I) IMMEDIATE DELIVERY"); break;
							case 'L' :
								info.setStatus("(L) PENDING LISTING"); break;
							case 'M' :
								info.setStatus("(M) PENDING ANNOUNCEMENT"); break;
							case 'Z' :
								info.setStatus("(Z) WHEN ISSUE"); break;
							case 'C' :
								info.setStatus("(C) CONDITIONAL TRADING"); break;
							case 'U' :
								info.setStatus("(U) CONDITIONAL WHEN ISSUE"); break;
							case 'F' :
								info.setStatus("(F) CIRCUIT BREAKER"); break;
						}
						// Mary@20121005 - Fixes_Request-20120815, ReqNo.3 - END
					}
					// Added by Mary@20121005 - Fixes_request-20120815, ReqNo.3
					break;
				case 23:
					if( isEmptyString(column) )
						info.setSellVolume(0);
					else if( column.equals("0") )
						info.setSellVolume(0);
					else
						info.setSellVolume( Long.parseLong(column) );

					break;
				case 24:
					if( isEmptyString(column) )
						info.setBuyVolume(0);
					else if( column.equals("0") )
						info.setBuyVolume(0);
					else
						info.setBuyVolume(Long.parseLong(column));

					break;
				case 25:
					if( isEmptyString(column) )
						info.setBuyTrans(0);
					else if( column.equals("0") )
						info.setBuyTrans(0);
					else
						info.setBuyTrans(Long.parseLong(column));

					break;
				case 26:
					if( isEmptyString(column) )
						info.setSellTrans(0);
					else if( column.equals("0") )
						info.setSellTrans(0);
					else
						info.setSellTrans(Long.parseLong(column));

					break;
				case 27:
					info.setCurrency(column); break;
				// Added by Sonia@20130820 - Change_Request-20130225, ReqNo.5 - START
				case 28:
					info.setLastTradeNo(Integer.parseInt(column));
					break;
				// Added by Sonia@20130820 - Change_Request-20130225, ReqNo.5 - END

				//Added by Thinzar, Change_Request-20161101, ReqNo.3 - START
				case 29:
					info.setTotalShortSellVolume(Long.parseLong(column));
					break;
				//Added by Thinzar, Change_Request-20161101, ReqNo.3 - END

				//Change_Request-20170119, ReqNo.12
				case 30:
					info.setTheorecticalPrice(Float.parseFloat(column));
					break;

				//Added by Thinzar, Change_Request-20160722, ReqNo.2 - START
				case 31:

					if(column.length() > 0)
						info.setTheScreenerStarRating(Integer.parseInt(column));
					else
						info.setTheScreenerStarRating(999);	//999 is dummy variable not to get default as zero
					break;
				case 32:

					if(column.length() > 0)
						info.setTheScreenerRiskLevel(Integer.parseInt(column));
					else
						info.setTheScreenerRiskLevel(999);
					break;
				//Added by Thinzar, Change_Request-20160722, ReqNo.2 - END

				// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4 - START
				case 33:

					// Added by Mary@20130110 - Change_Request-20130104, ReqNo.4
					// as ATP will preform the validation so always set payable with CPF
					info.setPayableWithCPF(true); break; // mary test
				// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4 - END
			}

			index++;
		}

		return info;
	}

	private static boolean isEmptyString(String column){
		/* Mary@20121005 - Fixes_request-20120815, ReqNo.3
		if(column.trim().equals("")){
			return true;
		}else{
			return false;
		}
		*/
		return ( ( column.trim().equals("") ) ? true : false );
	}

	//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 START	
	public static List<MarketDepth> parseMarketDepth4(String message) {
		List<MarketDepth> marketDepths	= new ArrayList<MarketDepth>();
		StringTokenizer columns			= new StringTokenizer(message, "\r\n");

		while( columns.hasMoreTokens() ){
			String column		= columns.nextToken();
			String[] columns2	= column.split(",");

			MarketDepth depth	= new MarketDepth();
			//Edited by Thinzar@20140918, Fixes_Request-20140820, ReqNo.24 (some test data doesn't return all columns, handle Array IndexOutofBound)
			if(columns2.length > 6){
				depth.setBidSize(Long.parseLong(columns2[2]));
				depth.setBidPrice(Float.parseFloat(columns2[3]));
				depth.setAskPrice(Float.parseFloat(columns2[4]));
				depth.setAskSize(Long.parseLong(columns2[5]));
			} else {
				depth.setBidSize(0);
				depth.setBidPrice(0);
				depth.setAskPrice(0);
				depth.setAskSize(0);
			}

			marketDepths.add(depth);
		}

		return marketDepths;
	}

	public static List<MarketDepth> parseMarketDepth3(String message, int indicator) {
		List<MarketDepth> marketDepths	= new ArrayList<MarketDepth>();
		StringTokenizer columns 		= new StringTokenizer(message, ",");
		MarketDepth depth 				= new MarketDepth();
		int k 							= 1;
		int index 						= 0;

		while (columns.hasMoreTokens() && k<=indicator) {
			String column = columns.nextToken();
			//	System.out.println("DEPTH COLUMNS?:"+column+"  INDEX:"+index+" K:"+k);

			switch(index-(4*(k-1))){
				case 1:depth.setBidSize(Long.parseLong(column));break;// depth[].setBidSize(Long.parseLong(column));System.out.println("DEPTH switch 1"+(k-1)+" :"+column);break;
				case 2:depth.setBidPrice(Float.parseFloat(column));break; //System.out.println("DEPTH switch 2"+(k-1)+" :"+column);break;
				case 3:depth.setAskSize(Long.parseLong(column));break;//depth[k-1].setAskSize(Long.parseLong(column));System.out.println("DEPTH switch 3"+(k-1)+" :"+column);break;
				case 4:depth.setAskPrice(Float.parseFloat(column));marketDepths.add(depth);depth = new MarketDepth();k++;break;//depth[k-1].setAskPrice(Float.parseFloat(column));System.out.println("DEPTH switch 4"+(k-1)+" :"+column);marketDepths.add(depth[k-1]);k++;break;
				default:break;
			}

			index++;
		}

		return marketDepths;
	}

	//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 END
	public static List<MarketDepth> parseMarketDepth(String message) {
		List<MarketDepth> marketDepths 	= new ArrayList<MarketDepth>();
		StringTokenizer columns 		= new StringTokenizer(message, ",");

		MarketDepth depth 				= new MarketDepth();
		MarketDepth depth2 				= new MarketDepth();
		MarketDepth depth3 				= new MarketDepth();
		MarketDepth depth4 				= new MarketDepth();
		MarketDepth depth5 				= new MarketDepth();
		int index 						= 0;

		while (columns.hasMoreTokens()) {
			String column = columns.nextToken();
			switch (index) {
				case 1: depth.setBidSize(Long.parseLong(column)); break;
				case 2: depth.setBidPrice(Float.parseFloat(column)); break;
				case 3: depth.setAskSize(Long.parseLong(column)); break;
				case 4: depth.setAskPrice(Float.parseFloat(column)); break;
				case 5: depth2.setBidSize(Long.parseLong(column)); break;
				case 6: depth2.setBidPrice(Float.parseFloat(column)); break;
				case 7: depth2.setAskSize(Long.parseLong(column)); break;
				case 8: depth2.setAskPrice(Float.parseFloat(column)); break;
				case 9: depth3.setBidSize(Long.parseLong(column)); break;
				case 10: depth3.setBidPrice(Float.parseFloat(column)); break;
				case 11: depth3.setAskSize(Long.parseLong(column)); break;
				case 12: depth3.setAskPrice(Float.parseFloat(column)); break;
				case 13: depth4.setBidSize(Long.parseLong(column)); break;
				case 14: depth4.setBidPrice(Float.parseFloat(column)); break;
				case 15: depth4.setAskSize(Long.parseLong(column)); break;
				case 16: depth4.setAskPrice(Float.parseFloat(column)); break;
				case 17: depth5.setBidSize(Long.parseLong(column)); break;
				case 18: depth5.setBidPrice(Float.parseFloat(column)); break;
				case 19: depth5.setAskSize(Long.parseLong(column)); break;
				case 20: depth5.setAskPrice(Float.parseFloat(column)); break;
			}
			index++;
		}

		marketDepths.add(depth);
		marketDepths.add(depth2);
		marketDepths.add(depth3);
		marketDepths.add(depth4);
		marketDepths.add(depth5);

		return marketDepths;
	}

	public static List<MarketDepth> parseMarketDepth2(StockSymbol symbol){
		List<MarketDepth> marketDepths 			= new ArrayList<MarketDepth>();

		final List<Float> marketDepthAskPrice 	= symbol.getMarketDepthAskPrice();
		final List<Long> marketDepthAskSize 	= symbol.getMarketDepthAskSize();
		final List<Float> marketDepthBidPrice 	= symbol.getMarketDepthBidPrice();
		final List<Long> marketDepthBidSize 	= symbol.getMarketDepthBidSize();

		for(int i=0; i<marketDepthAskPrice.size(); i++){
			MarketDepth depth = new MarketDepth();
			depth.setAskPrice(marketDepthAskPrice.get(i));
			depth.setAskSize(marketDepthAskSize.get(i));
			depth.setBidPrice(marketDepthBidPrice.get(i));
			depth.setBidSize(marketDepthBidSize.get(i));

			marketDepths.add(depth);
		}

		return marketDepths;
	}

	public static List<StockSymbol> parseStockRefresh(String message) {
		List<StockSymbol> symbols = new ArrayList<StockSymbol>();

		try{
			message= message.lastIndexOf(",") == (message.length() - 1) ? message + " " : message;
			message= message.indexOf(",") == 0 ? " " + message : message;
			message= message.replaceAll(",,", ", ,");
			StringTokenizer tokens = new StringTokenizer(message, "\r\n");

			StockSymbol.MarketDepthBidPrice.clear();
			StockSymbol.MarketDepthBidSize.clear();
			StockSymbol.MarketDepthAskPrice.clear();
			StockSymbol.MarketDepthAskSize.clear();

			while (tokens.hasMoreTokens()) {
				String stockData = tokens.nextToken();

				StringTokenizer columns = new StringTokenizer(stockData, ",");
				StockSymbol symbol = new StockSymbol();

				int index = 0;
				while (columns.hasMoreTokens()) {
					String column = columns.nextToken();

					switch (index) {
						case 0: symbol.setSymbolCode(column); break;
						case 1: symbol.setHigh(Float.parseFloat(column)); break;
						case 2: symbol.setLow(Float.parseFloat(column)); break;
						case 3: symbol.setLastDone(Float.parseFloat(column)); break;
						case 4: symbol.setVolume(Long.parseLong(column)); break;
						case 5: symbol.setValue(Double.parseDouble(column)); break;
						case 6:
							if(!column.equals(" ") && column!=null)
								symbol.setTotalTrade(Integer.parseInt(column) );
							break;
						case 7:
							symbol.setLastBidSize(Long.parseLong(column));
							symbol.setMarketDepthBidSize(Long.parseLong(column));
							break;
						case 8:
							symbol.setLastBidPrice(Float.parseFloat(column));
							symbol.setMarketDepthBidPrice(Float.parseFloat(column));
							break;
						case 9:
							symbol.setLastAskSize(Long.parseLong(column));
							symbol.setMarketDepthAskSize(Long.parseLong(column));
							break;
						case 10:
							symbol.setLastAskPrice(Float.parseFloat(column));
							symbol.setMarketDepthAskPrice(Float.parseFloat(column));
							break;
						case 11:
							symbol.setPreviousClose(Float.parseFloat(column)); break;
						case 12:
							symbol.setLACP(Float.parseFloat(column)); break;
						case 13:
							symbol.setLastBidSize2(Long.parseLong(column));
							symbol.setMarketDepthBidSize(Long.parseLong(column)); break;
						case 14:
							symbol.setLastBidPrice2(Float.parseFloat(column));
							symbol.setMarketDepthBidPrice(Float.parseFloat(column)); break;
						case 15:
							symbol.setLastAskSize2(Long.parseLong(column));
							symbol.setMarketDepthAskSize(Long.parseLong(column)); break;
						case 16:
							symbol.setLastAskPrice2(Float.parseFloat(column));
							symbol.setMarketDepthAskPrice(Float.parseFloat(column)); break;
						case 17:
							symbol.setLastBidSize3(Long.parseLong(column));
							symbol.setMarketDepthBidSize(Long.parseLong(column)); break;
						case 18:
							symbol.setLastBidPrice3(Float.parseFloat(column));
							symbol.setMarketDepthBidPrice(Float.parseFloat(column)); break;
						case 19:
							symbol.setLastAskSize3(Long.parseLong(column));
							symbol.setMarketDepthAskSize(Long.parseLong(column)); break;
						case 20:
							symbol.setLastAskPrice3(Float.parseFloat(column));
							symbol.setMarketDepthAskPrice(Float.parseFloat(column)); break;
						case 21: symbol.setMarketDepthBidSize(Long.parseLong(column)); break;
						case 22: symbol.setMarketDepthBidPrice(Float.parseFloat(column)); break;
						case 23: symbol.setMarketDepthAskSize(Long.parseLong(column)); break;
						case 24: symbol.setMarketDepthAskPrice(Float.parseFloat(column)); break;
						case 25: symbol.setMarketDepthBidSize(Long.parseLong(column)); break;
						case 26: symbol.setMarketDepthBidPrice(Float.parseFloat(column)); break;
						case 27: symbol.setMarketDepthAskSize(Long.parseLong(column)); break;
						case 28: symbol.setMarketDepthAskPrice(Float.parseFloat(column)); break;
						case 29: symbol.setInstrument(column); break;
					/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
					// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
					case 30: symbol.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "1" : column.trim() ) ); break;
					*/
						case 30: symbol.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "0" : column.trim() ) ); break;
						// Added by Sonia@20130820 - Change_Request-20130225, ReqNo.5 - START
						case 31: symbol.setLastTrade_No(Integer.parseInt(column)); break;
						// Added by Sonia@20130820 - Change_Request-20130225, ReqNo.5 - END

						case 32: symbol.setDerivativeStockType(column); break;
					}
					index++;
				}

				symbols.add(symbol);
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		return symbols;
	}

	public static StockSymbol parseStockTrade(String message){
		if(message != null){
			message = message.lastIndexOf(",") == (message.length() - 1) ? message + " " : message;
			message = message.indexOf(",") == 0 ? " " + message : message;
			/* Mary@20130315 - Fixes_Request-20130314, ReqNo.4
			message= message.replaceAll(",,", ", ,");
			*/
			
			/* Mary@20130315 - Fixes_Request-20130314, ReqNo.4
			StringTokenizer columns = new StringTokenizer(message, ",");
			*/
			String[] columns	= message.split(",");
			StockSymbol symbol	= new StockSymbol();
			/* Mary@20130315 - Fixes_Request-20130314, ReqNo.4 - START
			int index = 0;

			while (columns.hasMoreTokens()) {
				String column = columns.nextToken();
			*/
			for(int index=0; index<columns.length; index++){
				String column = columns[index];
				// Mary@20130315 - Fixes_Request-20130314, ReqNo.4 - END
				switch (index) {
					case 0: symbol.setSymbolCode(column); break;
					case 1: symbol.setStockCode(column); break;
					case 2: symbol.setCompanyName(column); break;
					case 3: symbol.setPreviousClose(Float.parseFloat(column)); break;
					case 4: symbol.setLACP(Float.parseFloat(column)); break;
					case 5: symbol.setHigh(Float.parseFloat(column)); break;
					case 6: symbol.setLow(Float.parseFloat(column)); break;
					case 7: symbol.setLastDone(Float.parseFloat(column)); break;
					case 8: symbol.setVolume(Long.parseLong(column)); break;
					case 9: symbol.setValue(Double.parseDouble(column)); break;
					case 10: if(!column.equals(" ") && column!=null){symbol.setTotalTrade(Integer.parseInt(column));break;} break;
					case 11: symbol.setLastBidSize(Long.parseLong(column)); break;
					case 12: symbol.setLastBidPrice(Float.parseFloat(column)); break;
					case 13: symbol.setLastAskSize(Long.parseLong(column)); break;
					case 14: symbol.setLastAskPrice(Float.parseFloat(column)); break;
					case 15: symbol.setLastBidSize2(Long.parseLong(column)); break;
					case 16: symbol.setLastBidPrice2(Float.parseFloat(column)); break;
					case 17: symbol.setLastAskSize2(Long.parseLong(column)); break;
					case 18: symbol.setLastAskPrice2(Float.parseFloat(column)); break;
					case 19: symbol.setLastBidSize3(Long.parseLong(column)); break;
					case 20: symbol.setLastBidPrice3(Float.parseFloat(column)); break;
					case 21: symbol.setLastAskSize3(Long.parseLong(column)); break;
					case 22: symbol.setLastAskPrice3(Float.parseFloat(column)); break;
					case 23: symbol.setCurrency(column); break;
					case 24: symbol.setInstrument(column); break;
				/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
				// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
				case 25: symbol.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "1" : column.trim() ) ); break;
				*/
					case 25: symbol.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "0" : column.trim() ) ); break;

					//Added by Thinzar, Fixes_Request-20160101, ReqNo.6
					case 26: symbol.setSectorName(column == null? "-":column.trim().toString());break;
				}
				/* Mary@20130315 - Fixes_Request-20130314, ReqNo.4
				index++;
				*/
			}

			return symbol;
		}else{
			return null;
		}
	}

	// Added by Sonia@20130705 - Change_Request-20130225, ReqNo.5 - START
	public static List<TimeSales> parseTimeSales(String message) {

		List<TimeSales> timeSales = new ArrayList<TimeSales>();

		try {
			StringTokenizer tokens = new StringTokenizer(message, "\r\n");
			int tokenCount = 0;

			while (tokens.hasMoreTokens()) {
				String stockData = tokens.nextToken();
				if (stockData.equals("[END]")){
					break;
				}

				if (tokenCount > 1){
					if (stockData.length() > 1){

						StringTokenizer columns = new StringTokenizer(stockData, ",");
						TimeSales sales = new TimeSales();

						int index = 0;
						while (columns.hasMoreTokens()) {
							String column = columns.nextToken();

							switch (index) {
								case 0:
									sales.setTime(FormatUtil.parseDateString(column, "HHmmss"));
									break;
								case 1:
									sales.setTradedat(column.charAt(0));
									break;
								case 2:
									sales.setPrice(Float.parseFloat(column));
									break;
								case 3:
									sales.setVolume(Long.parseLong(column));
									break;
								case 4:
									sales.setValue(Integer.parseInt(column));
									break;
								case 5:
									sales.setRecordsNo(Integer.parseInt(column));
									break;
							}

							index++;
						}
						timeSales.add(sales);
					}
				}
				tokenCount ++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return timeSales;
	}
	// Added by Sonia@20130705 - Change_Request-20130225, ReqNo.5 - END

	// Added by Sonia@20130918 - Change_Request-20130225, ReqNo.5 - START
	public static List<TimeSales> parseTimeSalesPH(String message) {

		List<TimeSales> timeSales = new ArrayList<TimeSales>();

		try {
			StringTokenizer tokens = new StringTokenizer(message, "\r\n");
			int tokenCount = 0;

			while (tokens.hasMoreTokens()) {
				String stockData = tokens.nextToken();
				if (stockData.equals("[END]")){
					break;
				}

				if (tokenCount > 1){
					if (stockData.length() > 1){

						StringTokenizer columns = new StringTokenizer(stockData, ",");
						TimeSales sales = new TimeSales();

						//Change_Request-20161101, ReqNo.1, set default in case Feed doesn't return BBH and SBH
						sales.setForeign('-');
						sales.setBuyerBrokerHouse("-");
						sales.setDomestic('-');
						sales.setSellerBrokerHouse("-");
						//Change_Request-20161101, ReqNo.1 - END

						int index = 0;
						while (columns.hasMoreTokens()) {
							String column = columns.nextToken();

							switch (index) {
								case 0:
									sales.setTime(FormatUtil.parseDateString(column, "HHmmss"));
									break;
								case 1:
									sales.setTradedat(column.charAt(0));
									break;
								case 2:
									sales.setPrice(Float.parseFloat(column));
									break;
								case 3:
									sales.setVolume(Long.parseLong(column));
									break;
								case 4:
									sales.setValue(Integer.parseInt(column));
									break;
								case 5:
									sales.setRecordsNo(Integer.parseInt(column));
									break;
								case 6:
									if(column != null && column.length() > 0)
										sales.setForeign(column.charAt(0));
									break;
								case 7:
									if(column != null && column.length() > 0)
										sales.setBuyerBrokerHouse(column);
									break;
								case 8:
									if(column != null && column.length() > 0)
										sales.setDomestic(column.charAt(0));
									break;
								case 9:
									if(column != null && column.length() > 0)
										sales.setSellerBrokerHouse(column);
									break;
							}

							index++;
						}

						timeSales.add(sales);
					}
				}
				tokenCount ++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return timeSales;
	}
	// Added by Sonia@20130918 - Change_Request-20130225, ReqNo.5 - END

	// Added by Sonia@20131002 - Change_Request-20130225, ReqNo.5 - START
	public static List<BusinessDone> parseBusinessDone(String message) {

		List<BusinessDone> businessDone = new ArrayList<BusinessDone>();

		try {
			StringTokenizer tokens = new StringTokenizer(message, "\r\n");
			int tokenCount = 0;

			while (tokens.hasMoreTokens()) {
				String stockData = tokens.nextToken();
				if (stockData.equals("[END]")){
					break;
				}

				if (tokenCount > 0){
					if (stockData.length() > 1){

						BusinessDone done = new BusinessDone();

						if (stockData.equals("[TOTAL]")){
							break;
						}else if(stockData.equals("[SUMMARY]")){
							break;
						}else{
							StringTokenizer columns = new StringTokenizer(stockData, ",");

							int index = 0;
							while (columns.hasMoreTokens()) {
								String column = columns.nextToken();

								switch (index) {
									case 0:
										done.setPrice(Float.parseFloat(column));
										break;
									case 1:
										done.setSVolume(Long.parseLong(column));
										break;
									case 2:
										done.setBVolume(Long.parseLong(column));
										break;
									case 3:
										done.setTotalVolume(Long.parseLong(column));
										break;
									case 4:
										done.setTotalValue(Long.parseLong(column));
										break;
								}
								index++;
							}
							businessDone.add(done);
						}
					}
				}
				tokenCount ++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return businessDone;
	}
	// Added by Sonia@20131002 - Change_Request-20130225, ReqNo.5 - END

	//Aadded by Thinzar@20141113, Change_Request-20141101, ReqNo. 6 - START
	/*
	 * sometimes UAT feed returns multiple rows, prod feed returns one row, edited the code to work under any condition
	 */
	public static StockInfo parseStockDetailImage6(String message, String symbolCode){
		String qcExchangeCode	= symbolCode.substring( (symbolCode.lastIndexOf(".") + 1), symbolCode.length() );
		StockInfo stockInfo	= new StockInfo();

		//initialize the object (in case returns empty string)
		stockInfo.setPrice52WeekHi("-");
		stockInfo.setPrice52WeekLo("-");
		stockInfo.setDynamicHi("-");
		stockInfo.setDynamicLo("-");
		stockInfo.setForeignOwnershipLimit("-");

		StringTokenizer tokens = new StringTokenizer(message, "\r\n");
		while (tokens.hasMoreTokens()) {
			String itemRow		= tokens.nextToken();
			String [] columns	= itemRow.split("\\,");
			if(symbolCode.equalsIgnoreCase(columns[0])){

				int index			= 0;
				while(index <= columns.length - 1){
					String column	= columns[index].trim();

					switch(index){
						case 0:
							stockInfo.setSymbolCode(column);
							break;
						case 1:
							if(isEmptyString(column))
								stockInfo.setPrice52WeekHi("-");
							else
								stockInfo.setPrice52WeekHi(FormatUtil.formatPrice(Float.parseFloat(column), "-", qcExchangeCode));
							break;
						case 2:
							if(isEmptyString(column))
								stockInfo.setPrice52WeekLo("-");
							else
								stockInfo.setPrice52WeekLo(FormatUtil.formatPrice(Float.parseFloat(column), "-", qcExchangeCode));
							break;
						case 3:
							if(isEmptyString(column))
								stockInfo.setDynamicHi("-");
							else
								stockInfo.setDynamicHi(FormatUtil.formatPrice(Float.parseFloat(column), "-", qcExchangeCode));
							break;
						case 4:
							if(isEmptyString(column))
								stockInfo.setDynamicLo("-");
							else
								stockInfo.setDynamicLo(FormatUtil.formatPrice(Float.parseFloat(column), "-", qcExchangeCode));
							break;
						case 5:
							if(isEmptyString(column))
								stockInfo.setForeignOwnershipLimit("-");
							else
								stockInfo.setForeignOwnershipLimit(FormatUtil.formatDoubleByExchange(Float.parseFloat(column), "-", qcExchangeCode));
							break;
						default:
							break;
					}
					index++;
				}
				break;		//break while loop
			} else{
				continue;
			}
		}

		return stockInfo;
	}
	//Added by Thinzar@20141113, Change_Request-20141101, ReqNo. 6 - END

	//Change_Request-20170601, ReqNo.2
	public static List<NewsCategory> parseNewsCategory(String message){

		List<NewsCategory> newsCategories = new ArrayList<NewsCategory>();

		if(message.length()>0) {
			StringTokenizer tokens = new StringTokenizer(message, "\r\n");
			while (tokens.hasMoreTokens()) {
				String itemRow = tokens.nextToken();
				String[] columns = itemRow.split("\\|");
				NewsCategory newsCat = new NewsCategory();

				if (columns.length > 0) {
					int index = 0;

					while (index < columns.length - 1) {
						String column = columns[index];

						switch (index) {

							case 0:
								newsCat.setSource(column);
								break;
							case 1:
								if (column.indexOf('.') != -1) {
									String[] subColumns = column.split("\\.");

									newsCat.setParentCategory(subColumns[0]);
									newsCat.setCategory(subColumns[1]);
								} else {
									newsCat.setCategory(column);
								}
								break;
							case 2:
								if (column.indexOf('.') != -1) {
									String[] subColumns = column.split("\\.");
									newsCat.setName(subColumns[1]);
								} else {
									newsCat.setName(column);
								}
								break;
							case 3:
								newsCat.setSeq(Float.parseFloat(column));
								break;
						}
						index++;
					}
				}
				newsCategories.add(newsCat);
			}
		}

		return newsCategories;
	}

	public static int getTotalNewsSize(String response){
		int total = 0;
		final String totalParam = "[TOTAL]";

		StringTokenizer tokens = new StringTokenizer(response, "\r\n");

		while(tokens.hasMoreTokens()){
			String itemRow = tokens.nextToken();

			if(itemRow.contains(totalParam)){
				String[] columns = itemRow.split("\\=");
				if(columns.length > 1){
					total = Integer.parseInt(columns[1]);
				}
				break;
			}
		}

		return total;
	}

	public static ArrayList<StockNews> parseStockNewsV2(String response){
		ArrayList<StockNews> stockNewsArrayList = new ArrayList<StockNews>();

		StringTokenizer tokens = new StringTokenizer(response, "\r\n");
		while(tokens.hasMoreTokens()){
			String itemRow = tokens.nextToken();
			StockNews news = new StockNews();

			String[] columns = itemRow.split("\\,", -1);	//Note: need to add this to avoid skipping empty values

			if(columns.length > 2){
				int index = 0;
				while(index < columns.length - 1){
					String column = columns[index];

					switch(index){
						case 0:
							news.setNewsId(column);
							break;
						case 1:
							//TODO: parse later when needed
							break;
						case 2:
							news.setDateTime(column);
							break;
						case 3:
							news.setCategoryId(column);
							break;
						case 4:
							if(column.length() > 0)
								news.setSymbolCode(column);
							break;
						case 5:
							news.setNewsTitle(column);
							break;
						case 6:
							//TODO: parse later when needed
							break;
						case 7:
							//TODO: parse later when needed
							break;
						case 8:
							if(column.length()>0)
								news.setNewsSource(column.toUpperCase());
							break;
						case 9:
							if(column.length()>0)
								news.setSymbolName(column);
							break;
					}

					index++;
				}

				stockNewsArrayList.add(news);
			}
		}

		return stockNewsArrayList;
	}

	//Change_Request-20170601, ReqNo.12
	public static String parseStockMapping(String response){
		String strReutersStkCode	= null;

		StringTokenizer tokens  = new StringTokenizer(response, "\r\n");
		while( tokens.hasMoreTokens() ){
			String strTemp 	= tokens.nextToken();
			if(strTemp != null && strTemp.trim().length() > 0
					&& strTemp.indexOf("[]") == -1
					&& strTemp.indexOf("[END]") == -1
					){
				strReutersStkCode	= strTemp.trim();
				break;
			}
		}
		return strReutersStkCode;
	}
}