package com.n2nconnect.android.stock.model;

import java.util.ArrayList;

public class Derivative {
	private int state;
	
	private ArrayList<DervTradePrtfSubDetailRpt> dervTradePrtfSubDetailRptList;
	private DervTradePrtfSummRpt dervTradePrtfSummRpt;
	

	
	
	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public ArrayList<DervTradePrtfSubDetailRpt> getDerivativeTradePortfolioSubDetailRptList() {
		return dervTradePrtfSubDetailRptList;
	}

	public void setDerivativeTradePortfolioSubDetailRptList(ArrayList<DervTradePrtfSubDetailRpt> dervTradePrtfSubDetailRptList) {
		this.dervTradePrtfSubDetailRptList = dervTradePrtfSubDetailRptList;
	}

	public DervTradePrtfSummRpt getDerivativeTradePortfolioSummaryRpt() {
		return dervTradePrtfSummRpt;
	}

	public void setDerivativeTradePortfolioSummaryRpt(DervTradePrtfSummRpt dervTradePrtfSummRpt) {
		this.dervTradePrtfSummRpt = dervTradePrtfSummRpt;
	}
	
	public void calculate(){
		
		for(DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt:dervTradePrtfSubDetailRptList){
			dervTradePrtfSubDetailRpt.calculateNettPosition();
			dervTradePrtfSubDetailRpt.calculateUnrealisedPL();
			dervTradePrtfSubDetailRpt.calculateTotalPL();
		}
		
		//current balance
		dervTradePrtfSummRpt.calculateCurrentBalance();
		
		//unrealised G/L
		dervTradePrtfSummRpt.calculateUnrealisedPL(dervTradePrtfSubDetailRptList);
		
		//equity
		dervTradePrtfSummRpt.calculateEquity();
		
		//netLiquidation
		dervTradePrtfSummRpt.calculateNetLiquidation();
		
		//excess/shortfall
		dervTradePrtfSummRpt.calculateExcessOrShortfall();
		
		//eligiblity
		dervTradePrtfSummRpt.calculateEligibility();
		
		//grossBuy
		dervTradePrtfSummRpt.calculateGrossBuy(dervTradePrtfSubDetailRptList);
		
		//grossSell
		dervTradePrtfSummRpt.calculateGrossSell(dervTradePrtfSubDetailRptList);
	
	}
	
}
