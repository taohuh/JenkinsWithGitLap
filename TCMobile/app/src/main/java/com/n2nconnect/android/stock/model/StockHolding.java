package com.n2nconnect.android.stock.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang.ArrayUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.n2nconnect.android.stock.ParamConstants;

public class StockHolding implements Comparable<StockHolding> {
	private String symbolCode;
	private String stockCode;
	private double marketValue;
	private double gainLossAmount;
	private float gainLossPercent;
	private long quantityOnHand;
	private float avgPurchasePrice;
	private float lastDone;
	private float lacp; // Fixes_Request-20160101, ReqNo.8
	private long quantityAvailable;
	private long quantitySellQueued;
	private boolean isExpand;
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
	private int intLotSize;
	// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27
	private int intPortfolioType = ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED;

	// Added by Thinzar@20140805 - Change_Request-20140701, ReqNo. 9
	private String settlementCurrency;
	private String investmentPortfolio;

	// Added by KW@20120827 - Change request-20120827, ReqNo.11 - START
	private float aggrBuyPrice;
	private float aggrSellPrice;
	private long totalQtyFromHolding;
	private long totalQtyShort;
	private long totalQtySold;
	private float totalBrokerage;
	private double realiseGainLossAmount;
	private float realiseGainLossPercent;

	// Added by Thinzar, Change_Request-20160101, ReqNo.9 - variables for unrealized portfolio sorting
	private double calculatedMarketValue = 0;
	private double calculatedUnrealizeGL = 0;
	private float calculatedPercentPL = 0;

	// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27 - START
	public int getIntPortfolioType() {
		return intPortfolioType;
	}

	public void setIntPortfolioType(int intPortfolioType) {
		this.intPortfolioType = intPortfolioType;
	}
	// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27 - END

	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12 - START
	public int getIntLotSize() {
		return intLotSize;
	}

	public void setIntLotSize(int intLotSize) {
		/*
		 * Mary@20130314 - Fixes_Request-20130314, ReqNo.2 this.intLotSize =
		 * intLotSize == 0 ? 1 : intLotSize;
		 */
		this.intLotSize = intLotSize;
	}

	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12 - END
	public boolean isExpand() {
		return isExpand;
	}

	public void setExpand(boolean isExpand) {
		this.isExpand = isExpand;
	}

	public String getSymbolCode() {
		return symbolCode;
	}

	public void setSymbolCode(String symbolCode) {
		this.symbolCode = symbolCode;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public double getMarketValue() {
		return marketValue;
	}

	public void setMarketValue(double marketValue) {
		this.marketValue = marketValue;
	}

	public double getGainLossAmount() {
		return gainLossAmount;
	}

	public void setGainLossAmount(double gainLossAmount) {
		this.gainLossAmount = gainLossAmount;
	}

	public float getGainLossPercent() {
		return gainLossPercent;
	}

	public void setGainLossPercent(float gainLossPercent) {
		this.gainLossPercent = gainLossPercent;
	}

	public long getQuantityOnHand() {
		return quantityOnHand;
	}

	public void setQuantityOnHand(long quantityOnHand) {
		this.quantityOnHand = quantityOnHand;
	}

	public float getAvgPurchasePrice() {
		return avgPurchasePrice;
	}

	public void setAvgPurchasePrice(float avgPurchasePrice) {
		this.avgPurchasePrice = avgPurchasePrice;
	}

	public float getLastDone() {
		return lastDone;
	}

	public void setLastDone(float lastDone) {
		this.lastDone = lastDone;
	}

	public long getQuantityAvailable() {
		return quantityAvailable;
	}

	public void setQuantityAvailable(long quantityAvailable) {
		this.quantityAvailable = quantityAvailable;
	}

	public long getQuantitySellQueued() {
		return quantitySellQueued;
	}

	public void setQuantitySellQueued(long quantitySellQueued) {
		this.quantitySellQueued = quantitySellQueued;
	}

	// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START
	public float getAggrBuyPrice() {
		return aggrBuyPrice;
	}

	public void setAggrBuyPrice(float aggrBuyPrice) {
		this.aggrBuyPrice = aggrBuyPrice;
	}

	public float getAggrSellPrice() {
		return aggrSellPrice;
	}

	public void setAggrSellPrice(float aggrSellPrice) {
		this.aggrSellPrice = aggrSellPrice;
	}

	public long getTotalQtyFromHolding() {
		return totalQtyFromHolding;
	}

	public void setTotalQtyFromHolding(long totalQtyFromHolding) {
		this.totalQtyFromHolding = totalQtyFromHolding;
	}

	public long getTotalQtyShort() {
		return totalQtyShort;
	}

	public void setTotalQtyShort(long totalQtyShort) {
		this.totalQtyShort = totalQtyShort;
	}

	public long getTotalQtySold() {
		return totalQtySold;
	}

	public void setTotalQtySold(long totalQtySold) {
		this.totalQtySold = totalQtySold;
	}

	public float getTotalBrokerage() {
		return totalBrokerage;
	}

	public void setTotalBrokerage(float totalBrokerage) {
		this.totalBrokerage = totalBrokerage;
	}

	public double getRealiseGainLossAmount() {
		return realiseGainLossAmount;
	}

	public void setRealiseGainLossAmount(double realiseGainLossAmount) {
		this.realiseGainLossAmount = realiseGainLossAmount;
	}

	public float getRealiseGainLossPercent() {
		return realiseGainLossPercent;
	}

	public void setRealiseGainLossPercent(float realiseGainLossPercent) {
		this.realiseGainLossPercent = realiseGainLossPercent;
	}
	// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END

	// Added by Thinzar@20140805 - Change_Request-20140701, ReqNo. 9 - START
	public String getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(String settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public String getInvestmentPortfolio() {
		return investmentPortfolio;
	}

	public void setInvestmentPortfolio(String investmentPortfolio) {
		this.investmentPortfolio = investmentPortfolio;
	}
	// Added by Thinzar@20140805 - END

	// Fixes_Request-20160101, ReqNo.8 - START
	public float getLacp() {
		return lacp;
	}

	public void setLacp(float lacp) {
		this.lacp = lacp;
	}
	// Fixes_Request-20160101, ReqNo.8 - END

	// Added by Thinzar, Change_Request-20160101, ReqNo.9 - START
	public double getCalculatedMarketValue() {
		return calculatedMarketValue;
	}

	public void setCalculatedMarketValue(double calculatedMarketValue) {
		this.calculatedMarketValue = calculatedMarketValue;
	}

	public double getCalculatedUnrealizeGL() {
		return calculatedUnrealizeGL;
	}

	public void setCalculatedUnrealizeGL(double calculatedUnrealizeGL) {
		this.calculatedUnrealizeGL = calculatedUnrealizeGL;
	}

	public float getCalculatedPercentPL() {
		return calculatedPercentPL;
	}

	public void setCalculatedPercentPL(float calculatedPercentPL) {
		this.calculatedPercentPL = calculatedPercentPL;
	}
	
	@Override
	public int compareTo(StockHolding s) {

		return this.symbolCode.compareTo(s.symbolCode);
	}

	public static class orderByStockCodeASC implements Comparator<StockHolding> {

		@Override
		public int compare(StockHolding s1, StockHolding s2) {
			return s1.stockCode.compareToIgnoreCase(s2.stockCode);
		}
	}

	public static class orderByStockCodeDESC implements Comparator<StockHolding> {

		@Override
		public int compare(StockHolding s1, StockHolding s2) {
			return s2.stockCode.compareToIgnoreCase(s1.stockCode);
		}
	}

	public static class orderByRealiseGainLossPercentASC implements Comparator<StockHolding> {
		@Override
		public int compare(StockHolding s1, StockHolding s2) {
			return s1.realiseGainLossPercent > s2.realiseGainLossPercent ? 1
					: (s1.realiseGainLossPercent < s2.realiseGainLossPercent ? -1 : 0);
		}
	}

	public static class orderByRealiseGainLossPercentDESC implements Comparator<StockHolding> {
		@Override
		public int compare(StockHolding s1, StockHolding s2) {
			return s2.realiseGainLossPercent > s1.realiseGainLossPercent ? 1
					: (s2.realiseGainLossPercent < s1.realiseGainLossPercent ? -1 : 0);
		}
	}
	
	//private double calculatedUnrealizeGL = 0;
	//private float calculatedPercentPL = 0;
	
	public static class orderByCalculatedUnrealizeGLASC implements Comparator<StockHolding>{
		
		@Override
		public int compare(StockHolding s1, StockHolding s2){
			return s1.getCalculatedUnrealizeGL() > s2.getCalculatedUnrealizeGL() ? 1
					: (s1.getCalculatedUnrealizeGL() < s2.getCalculatedUnrealizeGL() ? -1 : 0);
		}
	}
	
	public static class orderByCalculatedUnrealizeGLDESC implements Comparator<StockHolding>{
		
		@Override
		public int compare(StockHolding s1, StockHolding s2){
			return s2.getCalculatedUnrealizeGL() > s1.getCalculatedUnrealizeGL() ? 1
					: (s2.getCalculatedUnrealizeGL() < s1.getCalculatedUnrealizeGL() ? -1 : 0);
		}
	}
	
	public static class orderByCalculatedPercentPLASC implements Comparator<StockHolding>{
		
		@Override
		public int compare(StockHolding s1, StockHolding s2){
			return s1.getCalculatedPercentPL() > s2.getCalculatedPercentPL() ? 1
					: (s1.getCalculatedPercentPL() < s2.getCalculatedPercentPL() ? -1 : 0);
		}
	}
	
	public static class orderByCalculatedPercentPLDESC implements Comparator<StockHolding>{
		
		@Override
		public int compare(StockHolding s1, StockHolding s2){
			return s2.getCalculatedPercentPL() > s1.getCalculatedPercentPL() ? 1
					: (s2.getCalculatedPercentPL() < s1.getCalculatedPercentPL() ? -1 : 0);
		}
	}
	// Added by Thinzar, Change_Request-20160101, ReqNo.9 - END
}