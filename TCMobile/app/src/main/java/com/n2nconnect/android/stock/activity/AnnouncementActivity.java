package com.n2nconnect.android.stock.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class AnnouncementActivity extends CustomWindow {

	WebView webView;
	ProgressDialog dialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.announcement);
		
		this.title.setText(getResources().getString(R.string.announcement_module_title));
		this.icon.setImageResource(AppConstants.brokerConfigBean.getIntAnnouncementMenuIconBgImg());
		
		if(this.getIntent() != null){
			Bundle bundle = this.getIntent().getExtras();
			//To check calling activity is login screen.
			isFromLogin = bundle.getBoolean(ParamConstants.FROM_LOGIN_TAG, false);
			if(isFromLogin) menuButton.setVisibility(View.INVISIBLE);
		}
		
		webView = (WebView)findViewById(R.id.webview);
		
		//Added by Thinzar, Fixes_Request-20140820, Req#2 - START
		dialog = new ProgressDialog(this);
		webView.setWebChromeClient(new WebChromeClient(){

			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				if(!AnnouncementActivity.this.isFinishing()){
					dialog.show();
				}
				
				if(newProgress == 100 && dialog.isShowing())
					dialog.dismiss();
			}

		});
		//Added by Thinzar, Fixes_Request-20140820, Req#2 - START
		
		webView.loadUrl(AppConstants.brokerConfigBean.getStrAnnouncementURL());
	}
}