package com.n2nconnect.android.stock.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class DMAActivity extends CustomWindow implements OnDoubleLoginListener{

	private Button btnAgree;
	private WebView DMAView;
	private ProgressDialog dialog;

	private String senderCode;
	private String userParam;
	private String loginId;
	private DoubleLoginThread loginThread;

	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	private int intRetryCtr = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			super.onCreate(savedInstanceState);

			if (this.getIntent() != null) {
				Bundle bundle = this.getIntent().getExtras();
				//To check calling activity is login screen, to prompt user for logout when back button.
				senderCode = bundle.getString(ParamConstants.SENDERCODE_TAG);
				userParam = bundle.getString(ParamConstants.USERPARAM_TAG);
				loginId = bundle.getString(ParamConstants.USERNAME_TAG);
			}

			AppConstants.showMenu = false;

			this.title.setText("Terms And Conditions");
			this.icon.setVisibility(View.GONE);

			loginThread = ((StockApplication) DMAActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(loginThread != null)
				loginThread.setRegisteredListener(DMAActivity.this);

			setContentView(R.layout.dma_view);

			//Edited By Thinzar@20140827, Change_Request-20140801, ReqNo.6 - START
			//String DMAUrl = "http://einvestuat.hdbsib.com/gcHDBS/tnc.html";
			String DMAUrl 	= AppConstants.brokerConfigBean.getDMAWebViewURL();
			//Edited By Thinzar@20140827, Change_Request-20140801, ReqNo.6 - END

			btnAgree = (Button)findViewById(R.id.btnRegister);
			btnAgree.setText("Agree");
			this.menuButton.setVisibility(View.GONE);
			dialog = new ProgressDialog(this);

			DMAView = (WebView)findViewById(R.id.DMAWeb);
			DMAView.setHorizontalScrollBarEnabled(false);

			//Fixes_Request-20170701, ReqNo.15
			DMAView.setWebChromeClient(new WebChromeClient(){

				@Override
				public void onProgressChanged(WebView view, int newProgress) {
					if( ! DMAActivity.this.isFinishing() ){
						dialog.setMessage("Loading...");
						dialog.show();
					}

					if(newProgress == 100 && dialog.isShowing())
						dialog.dismiss();
				}

			});
			DMAView.loadUrl(DMAUrl);

			btnAgree.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					//Fixes_Request-20170701, ReqNo.15
					new UpdateClientTermsAndConditionTask().execute();
				}
			});
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	//Fixes_Request-20170701, ReqNo.15
	private class UpdateClientTermsAndConditionTask extends AsyncTask<Void, Void, Exception>{
		private String strResponse		= null;
		private Exception ex;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if(dialog != null && !DMAActivity.this.isFinishing()){
				dialog.setMessage("updating...");
				dialog.show();
			}
		}

		@Override
		protected Exception doInBackground(Void... arg0) {
			Map<String, Object> parameters 			= new HashMap<String, Object>();

			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, userParam);

			parameters.put(ParamConstants.USERNAME_TAG, loginId);
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);


			try{
				strResponse	= AtpConnectUtil.UpdateClientTermsAndCondition(parameters);
				while(intRetryCtr < AppConstants.intMaxRetry && strResponse == null){
					intRetryCtr++;
					strResponse	= AtpConnectUtil.UpdateClientTermsAndCondition(parameters);
				}
				intRetryCtr = 0;


			}catch(Exception e){
				ex = e;
			}
			// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			return ex;
		}

		@Override
		protected void onPostExecute(Exception exception) {
			super.onPostExecute(exception);

			if(!DMAActivity.this.isFinishing() && dialog.isShowing())
				dialog.dismiss();

			if(exception != null){
				displayLogoutAlert(exception.getMessage());

			}else{
				if(strResponse != null){
					Intent intent = new Intent();
					intent.putExtra(ParamConstants.FROM_LOGIN_TAG, true);
					intent.putExtra("IsDMAExists", true);
					intent.setClass(DMAActivity.this, QuoteActivity.class);
					DMAActivity.this.startActivity(intent);
					finish();
				}else{
					displayLogoutAlert(ErrorCodeConstants.FAIL_UPDATE_AGREEMENT);
				}
			}
		}
	}

	private void displayLogoutAlert(String message){

		AlertDialog alert = new AlertDialog.Builder(DMAActivity.this)
				.setMessage(message)
				.setPositiveButton("LOGOUT", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						new logout().execute();
						dialog.dismiss();
					}
				})
				.create();

		if( ! DMAActivity.this.isFinishing() )
			alert.show();
	}

	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onResume(){
		try{
			super.onResume();

			currContext	= DMAActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);

			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(DMAActivity.this);
				loginThread.resumeRequest();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void onPause(){
		try{
			super.onPause();

			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END

	@Override
	public void onBackPressed() {
		return;
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler.obtainMessage();
		message.obj = response;
		handler.sendMessage(message);
	}

	final Handler handler = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();

				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					DMAActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private class logout extends AsyncTask<Void,Void,String>{

		@Override
		protected String doInBackground(Void... params) {
			String atpUserParam = sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);

			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			try {
				AtpConnectUtil.tradeLogout(parameters);
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String str){

			logout(DMAActivity.this);
		}

	}
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
}