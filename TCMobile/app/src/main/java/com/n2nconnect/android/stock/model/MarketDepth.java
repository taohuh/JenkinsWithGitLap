package com.n2nconnect.android.stock.model;

public class MarketDepth {
	
	private float bidPrice;
	private long bidSize;
	private float askPrice;
	private long askSize;
	
	public float getBidPrice() {
		return bidPrice;
	}
	public void setBidPrice(float bidPrice) {
		this.bidPrice = bidPrice;
	}
	public long getBidSize() {
		return bidSize;
	}
	public void setBidSize(long bidSize) {
		this.bidSize = bidSize;
	}
	public float getAskPrice() {
		return askPrice;
	}
	public void setAskPrice(float askPrice) {
		this.askPrice = askPrice;
	}
	public long getAskSize() {
		return askSize;
	}
	public void setAskSize(long askSize) {
		this.askSize = askSize;
	}
	
}
