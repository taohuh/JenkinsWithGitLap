package com.n2nconnect.android.stock.activity;

import java.io.IOException;

import org.json.JSONObject;
//import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.gcm.StockAlertPubKey;
import com.n2nconnect.android.stock.gcm.StockAlertUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;

public class SettingsPushNotiActivity extends CustomWindow {
	
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST 	= 9000;
	private static final int REQUEST_PHONE_STATE = 1;
	private final String stockAlertTAG							= "StockAlert";
	private static final String toggleTAG	= "toggleTAG";
	private String gcmRegId;
	private String jwtToken;
	
	private ToggleButton toggleNotiBtn;
	private ProgressDialog mDialog;
	
	private String strCurrentUsername;
	private String brokerCode;
	private String senderCode;
	private String screenSize;
	
	private String pushInfoUsername;
	private boolean hasDeviceRegistered;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.title.setText(getResources().getString(R.string.settings_module_title));
		this.icon.setImageResource( AppConstants.brokerConfigBean.getIntAccSettingMenuIconBgImg() );

		setContentView(R.layout.activity_push_noti_setting);

		toggleNotiBtn	= (ToggleButton)findViewById(R.id.toggle_notification);
		toggleNotiBtn.setTag(toggleTAG);
		
		mDialog			= new ProgressDialog(SettingsPushNotiActivity.this);
		
		SharedPreferences sharedPreferences 	= getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		strCurrentUsername	= sharedPreferences.getString(PreferenceConstants.USER_NAME, "");
		senderCode 			= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
		brokerCode			= sharedPreferences.getString(PreferenceConstants.BROKER_CODE, "");
		screenSize			= SystemUtil.getDeviceScreenSize(SettingsPushNotiActivity.this);

		SharedPreferences pushInfoPref 	= getSharedPreferences(PreferenceConstants.GCM_PREF_FILE, MODE_PRIVATE);
		pushInfoUsername		= pushInfoPref.getString(PreferenceConstants.GCM_USERNAME, "");
		hasDeviceRegistered 	= pushInfoPref.getBoolean(PreferenceConstants.HAS_DEVICE_REGISTERED, false);
	}
	
	private void setToggleListener(){
		toggleNotiBtn.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton mButton, boolean isChecked) {
				if(toggleNotiBtn.getTag() != null){
					if(isChecked){
	
						AlertDialog.Builder pushAlertBuilder	= new AlertDialog.Builder(SettingsPushNotiActivity.this);
						AlertDialog enablePushAlert = pushAlertBuilder.setTitle(getResources().getString(R.string.settings_push_noti_alert_title))
								.setMessage(getResources().getString(R.string.settings_push_noti_alert_msg))
								.setPositiveButton(getResources().getString(R.string.dialog_button_yes), new DialogInterface.OnClickListener() {
	
									@Override
									public void onClick(DialogInterface dialog, int arg1) {
										
										SettingsPushNotiActivity.this.startPushNotificationRegisterProcess();
										dialog.dismiss();
									}
								})
								.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
	
									@Override
									public void onClick(DialogInterface dialog, int which) {
										toggleNotiBtn.setTag(null);		//not to trigger listener
										toggleNotiBtn.setChecked(false);
										dialog.dismiss();
										toggleNotiBtn.setTag(toggleTAG);
									}
								})
								.setCancelable(false)
								.create();
	
						enablePushAlert.show();
					}else{
	
						AlertDialog.Builder pushAlertBuilder	= new AlertDialog.Builder(SettingsPushNotiActivity.this);
						AlertDialog disablePushAlert = pushAlertBuilder.setTitle(getResources().getString(R.string.settings_push_noti_disable_alert_title))
								.setMessage(getResources().getString(R.string.settings_push_noti_disable_alert_msg))
								.setPositiveButton(getResources().getString(R.string.dialog_button_yes), new DialogInterface.OnClickListener() {
	
									@Override
									public void onClick(DialogInterface dialog, int arg1) {
										
										new UnRegisterDeviceTask().execute();
										dialog.dismiss();
									}
								})
								.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
	
									@Override
									public void onClick(DialogInterface dialog, int which) {
										toggleNotiBtn.setTag(null);
										toggleNotiBtn.setChecked(true);
										dialog.dismiss();
										toggleNotiBtn.setTag(toggleTAG);
									}
								})
								.setCancelable(false)
								.create();
	
						disablePushAlert.show();
					}
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
	
		if(hasDeviceRegistered  && strCurrentUsername.equals(pushInfoUsername)){
			new CheckDeviceStatusTask().execute();
		}else{
			toggleNotiBtn.setChecked(false);
			//set toggle listeners
			setToggleListener();
		}
	}

	private void startPushNotificationRegisterProcess(){

		if(!AppConstants.isAppMultilogin && AppConstants.brokerConfigBean.isEnableStockAlert()){
			// Check for the phone_state permission before accessing the camera.  If the permission is not granted yet, request permission.
			final int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
			if (rc == PackageManager.PERMISSION_GRANTED) {
				regGcm();
			} else {
				checkForPhoneStatePermission();
			}
		} 
	}

	private void regGcm(){
		//GCMRegistrar.checkDevice(getApplicationContext());
		//GCMRegistrar.checkManifest(getApplicationContext());

		// Check device for Play Services APK. If check succeeds, proceed with GCM registration.
		if (checkPlayServices()) {
			new GetGcmRegistrationIdTask().execute();
		} else {
			DefinitionConstants.Debug("No valid Google Play Services APK found.");
		}
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, SettingsPushNotiActivity.this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				showAlert("This device is not supported for Push Notification.");
			}
			return false;
		}
		return true;
	}

	private class GetGcmRegistrationIdTask extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if(mDialog != null && !mDialog.isShowing()){
				mDialog.setMessage(getResources().getString(R.string.stock_alert_progress_register_gcm));
				mDialog.show();
			}
		}

		@Override
		protected String doInBackground(Void... params) {
			String exceptionMsg = null;

			try {
				gcmRegId = StockAlertUtil.getGcmRegistrationId(SettingsPushNotiActivity.this);
				DefinitionConstants.Debug(stockAlertTAG + ": Device registered, registration ID=" + gcmRegId);

			} catch (IOException ex) {
				exceptionMsg = ex.getMessage();
			}

			return exceptionMsg;
		}

		@Override
		protected void onPostExecute(String msg) {
			if(mDialog!= null && mDialog.isShowing())
				mDialog.dismiss();

			if(msg != null){
				showAlert(msg);
			}else{

				AppConstants.hasRegisteredStockAlert	= true;				
				StockAlertUtil.saveGcmInfoToPref(SettingsPushNotiActivity.this, strCurrentUsername, gcmRegId);

				//get public key
				new GetPublicKeyTask().execute();
			}
		}
	}

	//getPublicKey and generate JWT
	private class GetPublicKeyTask extends AsyncTask<Void, Void, Exception> {
		StockAlertPubKey stockAlertObj;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(mDialog != null)
				mDialog.setMessage(getResources().getString(R.string.stock_alert_progress_get_public_key));

			if(!mDialog.isShowing())
				mDialog.show();
		}

		@Override
		protected Exception doInBackground(Void... params) {

			try{
				stockAlertObj = StockAlertUtil.getPublicKey(brokerCode);

				if(stockAlertObj == null){
					throw new Exception("Error getting public key for stock alert.");
				}else{
					JSONObject claimJson = StockAlertUtil.getJsonForRegister(SettingsPushNotiActivity.this, senderCode, brokerCode, screenSize);
					jwtToken = StockAlertUtil.generateJWTToken(stockAlertObj, SettingsPushNotiActivity.this, brokerCode, claimJson);

					if(jwtToken == null){
						throw new Exception("Failed to generate JWT Token");
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				return e;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Exception ex) {
			super.onPostExecute(ex);
			if(mDialog != null && mDialog.isShowing())
				mDialog.dismiss();

			if (ex != null) {
				DefinitionConstants.Debug("GetPublicKeyTask, Exception:" + ex.getMessage());
				showAlert(getResources().getString(R.string.quote_stock_alert_register_failed));
			}else{
				new RegisterDeviceTask().execute();
			} 
		}
	}

	private class RegisterDeviceTask extends AsyncTask<Void, Void, Exception> {

		private Exception ex = null;
		private String status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if(mDialog != null)
				mDialog.setMessage(getResources().getString(R.string.stock_alert_progress_register));

			if(!mDialog.isShowing())
				mDialog.show();
		}

		@Override
		protected Exception doInBackground(Void... params) {
			Log.i(stockAlertTAG, "SettingsPushNotiActivity.RegisterDeviceTask");

			try{
				status = StockAlertUtil.registerDevice(jwtToken, brokerCode);

				if(status == null || !status.equals("200"))
					throw new Exception("Failed register device");
			}
			catch(Exception e){
				ex = e;
			}

			return ex;
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);
			if(mDialog != null && mDialog.isShowing()){
				mDialog.dismiss();

				if (result != null) {
					//failed to register, turn toggle off
					toggleNotiBtn.setTag(null);		
					toggleNotiBtn.setChecked(false);
					toggleNotiBtn.setTag(toggleTAG);
					
					showAlert(result.getMessage());
				}else{
					StockAlertUtil.saveDeviceRegisterFlag(SettingsPushNotiActivity.this);
					showAlert(getResources().getString(R.string.stock_alert_enabled_msg));
				}
			}
		}
	}

	private class UnRegisterDeviceTask extends AsyncTask<Void, Void, Exception> {

		private Exception ex = null;
		private String status;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(mDialog != null){
				mDialog.setMessage(getResources().getString(R.string.stock_alert_progress_deactivate));
			}
			if(!mDialog.isShowing())
				mDialog.show();
		}

		@Override
		protected Exception doInBackground(Void... params) {
			Log.i(stockAlertTAG, "SettingsPushNotiActivity.UnRegisterDeviceTask");

			try{
				StockAlertPubKey stockAlertPubKeyObj = StockAlertUtil.getPublicKey(brokerCode);

				if(stockAlertPubKeyObj == null){
					throw new Exception("Error getting public key for stock alert.");
				}else{
					JSONObject unRegClaimJson = StockAlertUtil.getUnregisterJson(SettingsPushNotiActivity.this, brokerCode, senderCode, strCurrentUsername, screenSize);
					DefinitionConstants.Debug("Unregister JSON:" + unRegClaimJson);
					jwtToken = StockAlertUtil.generateJWTToken(stockAlertPubKeyObj, SettingsPushNotiActivity.this, brokerCode, unRegClaimJson);

					if(jwtToken == null){
						throw new Exception("Failed to generate JWT Token");
					}else{
						status = StockAlertUtil.registerDevice(jwtToken, brokerCode);

						if(status == null || !status.equals("200"))
							throw new Exception("Failed to unregister device");
						
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				ex = e;
			}

			return ex;
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);
			if(mDialog != null && mDialog.isShowing()){
				mDialog.dismiss();

				if (result != null) {
					//failed to unregister, turn toggle back on
					toggleNotiBtn.setTag(null);		
					toggleNotiBtn.setChecked(true);
					toggleNotiBtn.setTag(toggleTAG);
					
					showAlert(result.getMessage());
				}else{
					
					StockAlertUtil.clearGcmInfoFromPref(SettingsPushNotiActivity.this);
					//set flag not to ask user again to register
					StockAlertUtil.saveDoNotAskAgainForPushNotification(SettingsPushNotiActivity.this);
					
					showAlert(getResources().getString(R.string.stock_alert_deactivated_msg));
					
					new GCMUnregisterTask().execute();
				}
			}
		}
	}
	
	private class GCMUnregisterTask extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				/*GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(SettingsPushNotiActivity.this);
				gcm.unregister();*/
				InstanceID.getInstance(SettingsPushNotiActivity.this).deleteInstanceID();
			}catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	private class CheckDeviceStatusTask extends AsyncTask<Void, Void, Exception> {

		private Exception ex = null;
		private String status;
		private boolean isDeviceRegisteredServerStatus = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(mDialog != null){
				mDialog.setMessage(getResources().getString(R.string.stock_alert_check_device_status));
			}
			if(!mDialog.isShowing())
				mDialog.show();
		}

		@Override
		protected Exception doInBackground(Void... params) {
			Log.i(stockAlertTAG, "SettingsPushNotiActivity.CheckDeviceStatusTask");

			try{
				StockAlertPubKey stockAlertPubKeyObj = StockAlertUtil.getPublicKey(brokerCode);

				if(stockAlertPubKeyObj == null){
					throw new Exception("Error getting public key for stock alert.");
				}else{
					JSONObject checkStatusClaimJson = StockAlertUtil.getCheckStatusJson(SettingsPushNotiActivity.this, brokerCode, senderCode, strCurrentUsername, screenSize);
					DefinitionConstants.Debug("Unregister JSON:" + checkStatusClaimJson);
					jwtToken = StockAlertUtil.generateJWTToken(stockAlertPubKeyObj, SettingsPushNotiActivity.this, brokerCode, checkStatusClaimJson);

					if(jwtToken == null){
						throw new Exception("Failed to generate JWT Token");
					}else{
						status = StockAlertUtil.registerDevice(jwtToken, brokerCode);

						if(status == null || !status.equals("200"))
							throw new Exception("Failed to unregister device");
						
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				ex = e;
			}

			return ex;
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);
			if(mDialog != null && mDialog.isShowing()){
				mDialog.dismiss();

				if (result != null) {
					isDeviceRegisteredServerStatus = false;
					toggleNotiBtn.setChecked(false);
					
				}else{
					isDeviceRegisteredServerStatus = true;
					
					if(hasDeviceRegistered  && strCurrentUsername.equals(pushInfoUsername)){
						toggleNotiBtn.setChecked(true);
					}else{
						toggleNotiBtn.setChecked(false);
					}
				}
				
				//set toggle listeners
				setToggleListener();
			}
		}
	}

	//READ_PHONE_STATE is considered a dangerous permission in Android M (6.0) you need to request the permission at runtime
	private void checkForPhoneStatePermission() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

			if (ContextCompat.checkSelfPermission(SettingsPushNotiActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
				if (ActivityCompat.shouldShowRequestPermissionRationale(SettingsPushNotiActivity.this, Manifest.permission.READ_PHONE_STATE)) {

					showPermissionMessage();
				} else {
					// No explanation needed, we can request the permission.
					ActivityCompat.requestPermissions(SettingsPushNotiActivity.this,
							new String[] { Manifest.permission.READ_PHONE_STATE }, REQUEST_PHONE_STATE);
				}
			}
		}
	}

	private void showPermissionMessage(){
		new AlertDialog.Builder(this)
		.setTitle("Read phone state")
		.setMessage("This app requires the permission to read phone state to continue")
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ActivityCompat.requestPermissions(SettingsPushNotiActivity.this,
						new String[]{Manifest.permission.READ_PHONE_STATE},
						REQUEST_PHONE_STATE);
			}
		}).create().show();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		switch (requestCode) {
		case REQUEST_PHONE_STATE:

			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				regGcm();
			} else {
				String phone_state_permission_denied	= "Unable to register push notification without granting permission";
				showAlert(phone_state_permission_denied);
			}
			break;
		}
	}

	public void showAlert(String message) {

		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(message)
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
				}
			});
			AlertDialog alert = builder.create();

			if( ! this.isFinishing() )
				alert.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}