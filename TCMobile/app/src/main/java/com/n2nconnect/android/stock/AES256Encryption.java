package com.n2nconnect.android.stock;

import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;

public class AES256Encryption {
	
	public static String asHex (byte buf[]) {
		StringBuffer strbuf = new StringBuffer(buf.length * 2);
		int i;

		for (i = 0; i < buf.length; i++) {
			if (((int) buf[i] & 0xff) < 0x10)
				strbuf.append("0");

			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}

		return strbuf.toString();
	}

	public static byte[] asByte(char[] hex){
		int length = hex.length / 2;
		byte[] raw = new byte[length];
		for (int i = 0; i < length; i++) {
			int high = Character.digit(hex[i * 2], 16);
			int low = Character.digit(hex[i * 2 + 1], 16);
			int value = (high << 4) | low;
			if (value > 127)
				value -= 256;
			raw[i] = (byte) value;
		}
		return raw;
	}

	@SuppressWarnings("finally")
	public static byte[] encryptData(byte[] data, byte[] key) {
		byte[] encrypted = null;

		Key skey = new javax.crypto.spec.SecretKeySpec(key, "AES");

		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding");
			cipher.init(Cipher.ENCRYPT_MODE, skey);

			encrypted = new byte[cipher.getOutputSize(data.length)];
			int ctLength = cipher.update(data, 0, data.length, encrypted, 0);
			ctLength += cipher.doFinal(encrypted, ctLength);
		} catch (Exception e) {
			e.getMessage();
		} finally {
			return encrypted;
		}
	}

	@SuppressWarnings("finally")
	public static byte[] deencryptData(byte[] data, byte[] key) {
		byte[] deencrypted = null;

		Key skey = new javax.crypto.spec.SecretKeySpec(key, "AES");

		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding");
			cipher.init(Cipher.DECRYPT_MODE, skey);

			deencrypted = cipher.doFinal(data);

		} catch (Exception e) {
			DefinitionConstants.Debug("e: " + e.getMessage());
		} finally {
			return deencrypted;
		}
	}

	public static byte[] HashSHA256Generator(String value){
		MessageDigest md;
		StringBuffer sb = null;
		byte[] digest = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(value.getBytes());
			digest = md.digest();

			sb = new StringBuffer();
			for (int i = 0; i < digest.length; i++) {
				sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return digest;

	}
}
