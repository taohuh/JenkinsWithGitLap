package com.n2nconnect.android.stock.custom;


import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.TextView;

import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.activity.SettingsActivity;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;
/**
 * 
 * @author KW   
 * @remark credit to Nono
 * 20120817 - Change Request-20120719, ReqNo.9
 *
 */
public class TryRefreshableView extends LinearLayout {

	private static final int TAP_TO_REFRESH = 1; 
	private static final int PULL_TO_REFRESH = 2; 
	private static final int RELEASE_TO_REFRESH = 3; 
	private static final int REFRESHING = 4; 
	public int mRefreshState;
	public int mfooterRefreshState;
	public Scroller scroller;
	public ScrollView sv;
	private View refreshView;
	public View mfooterView;
	public TextView mfooterViewText;
	private ImageView refreshIndicatorView;
	public ImageView refreshIndicatorView2;
	public ImageView information;
	public int refreshTargetTop;
//	private int refreshTargetTop= -35|42-LayoutParams.WRAP_CONTENT;
	public int refreshFooter;
	private ProgressBar bar;
	public ProgressBar bar2;
	public TextView headTotalCountView;
	public TextView TotalCountView;
	private TextView downTextView;
	private boolean isvalidate = false;

	private RefreshListener refreshListener;
	public int totalCount;
	private int lastY;

	private RotateAnimation mFlipAnimation;
	private RotateAnimation mReverseFlipAnimation;
	public int nowpull = -1;

	private boolean isRecord;
	private Context mContext;

	public TryRefreshableView(Context context) {
		super(context);
		mContext = context;

	}

	public TryRefreshableView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		init();

	}
	
	// Added by Kw@20121023 - Fixes_Request-20121016, ReqNo.01 - START
	//return the right topmargin value base on the density of the display	
	/* commented out by Thinzar@20140710 - this doesn't work on screen sizes higher than 400ppi
	public static int getTopMargin(TryRefreshableView tryRefreshableView){
		int i = 0;
		Resources resources = tryRefreshableView.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
			 
		if(metrics.densityDpi<200)
			return -25;
		else if(metrics.densityDpi<240)
			return -46;
	 	else if(metrics.densityDpi<280)
			return -50;
		else if(metrics.densityDpi>=280)
			return -62;
		else
			return 0;	 
	}
	*/
	
	// Replace above function by Thinzar@20140710 - Change_Request-20140701, Req#3 - START
	public static int getTopMargin(TryRefreshableView tryRefreshableView){
		int heightInPixel;
		Resources resources = tryRefreshableView.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		 
		//Let's say the topMargin is 33dp, convert that to pixel according to screen density
		int assumedHeightInDp = 34;
		heightInPixel = (-1) * Math.round(assumedHeightInDp * (metrics.densityDpi / 160f));	//convert dp to pixel
		
		//DEBUG
		System.out.println("getTopMargin: " + heightInPixel + " on screen dpi of " + metrics.densityDpi);
		
		return heightInPixel;
	}
	
	// Added by Kw@20121023 - Fixes_Request-20121016, ReqNo.01 - END

	private void init() {
		mFlipAnimation = new RotateAnimation(0, -180, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		mFlipAnimation.setInterpolator(new LinearInterpolator());
		mFlipAnimation.setDuration(250);
		mFlipAnimation.setFillAfter(true);

		mReverseFlipAnimation = new RotateAnimation(-180, 0, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		mReverseFlipAnimation.setInterpolator(new LinearInterpolator());
		mReverseFlipAnimation.setDuration(250);
		mReverseFlipAnimation.setFillAfter(true);
		scroller = new Scroller(mContext);

		refreshView = LayoutInflater.from(mContext).inflate(R.layout.pull_to_refresh_header, null);
		refreshIndicatorView = (ImageView) refreshView.findViewById(R.id.head_arrowImageView);
		//bar = (ProgressBar) refreshView.findViewById(R.id.head_progressBar);
		information = (ImageView)refreshView.findViewById(R.id.information_imageview);
		downTextView = (TextView) refreshView.findViewById(R.id.head_tipsTextView);
	
		// Added by Kw@20121023 - Fixes_Request-20121016, ReqNo.01 - START
		refreshTargetTop = getTopMargin(this);
		// Added by Kw@20121023 - Fixes_Request-20121016, ReqNo.01 - START
		refreshView.setMinimumHeight(30);
		
		LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		lp.topMargin = refreshTargetTop;
		lp.gravity = Gravity.CENTER;
		addView(refreshView, lp);
		
		isRecord = false;
		//mfooterView.
		mRefreshState = TAP_TO_REFRESH;
		mfooterRefreshState = TAP_TO_REFRESH;
	}

	public boolean onTouchEvent(MotionEvent event) {

		int y = (int) event.getRawY();

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (isRecord == false) {
				//Log.i("moveY", "lastY:" + y);
				lastY = y;
				isRecord = true;
			}
			break;

		case MotionEvent.ACTION_MOVE:

			//Log.i("TAG", "ACTION_MOVE");
			//Log.i("moveY", "ACTION_MOVE - lastY:" + lastY);
			//Log.i("moveY", "y:" + y);
			int m = y - lastY;
			if(-10<m && m <10){
				return false;
			}
			
			doMovement(m);
			lastY = y;

			break;

		case MotionEvent.ACTION_UP:
			//Log.i("TAG", "ACTION_UP");

			fling();
			mfooterView.setVisibility(GONE);

			isRecord = false;
			break;
		}
		
		return true;
	}

	private void fling() {
		if (nowpull == 0 && mRefreshState != REFRESHING) {
			LinearLayout.LayoutParams lp = (LayoutParams) refreshView
					.getLayoutParams();
			//Log.i("TAG", "fling()" + lp.topMargin);
			if (lp.topMargin > 0) {
				refresh();
				
			} else {
				returnInitState();
				//finishRefresh();
			}
		} else if (nowpull == 1 && mfooterRefreshState != REFRESHING) {
		

			if (refreshFooter >= 40	&& mfooterRefreshState == RELEASE_TO_REFRESH) {
				mfooterRefreshState = REFRESHING;
				FooterPrepareForRefresh(); 
				onRefresh(); 
			} else {
				if (refreshFooter>=0)
					resetFooterPadding();
				else {
					resetFooterPadding();
					mfooterRefreshState = TAP_TO_REFRESH;
					TryPullToRefreshScrollView.ScrollToPoint(sv, sv.getChildAt(0),-refreshFooter);
				}
			}
		}
	}

	public void onRefresh() {

		if (refreshListener != null) {
			refreshListener.onRefresh();
		}
	}

	private void returnInitState() {
		// TODO Auto-generated method stub
		mRefreshState = TAP_TO_REFRESH;
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
		int i = lp.topMargin;
		scroller.startScroll(0, i, 0, refreshTargetTop);

		invalidate();
	}

	private void refresh() {
		// TODO Auto-generated method stub
		mRefreshState = REFRESHING;
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
		int i = lp.topMargin;
		refreshIndicatorView.setVisibility(View.GONE);
		refreshIndicatorView.setImageDrawable(null);
		information.setVisibility(View.VISIBLE);
		//bar.setVisibility(View.VISIBLE);
		downTextView.setText("refreshing");
		scroller.startScroll(0, i, 0, (0 - i)+10);
	//	scroller.startScroll(0, i, 0, refreshTargetTop);
		invalidate();


		if (refreshListener != null) {

			refreshListener.onRefresh();
		}

	}

	private void resetFooterPadding() {
		//LayoutParams svlp = (LayoutParams) sv.getLayoutParams();
		//svlp.bottomMargin=0;
		
		//sv.setLayoutParams(svlp);
		LayoutParams rf = (LayoutParams) mfooterView.getLayoutParams();
		//rf.height=0;
		rf.bottomMargin = rf.bottomMargin/2;
		
		mfooterView.setVisibility(GONE);
		mfooterView.setLayoutParams(rf);
		TryPullToRefreshScrollView.ScrollToPoint(sv, sv.getChildAt(0),0);

	}

	public void FooterPrepareForRefresh() {
		resetFooterPadding();
		refreshIndicatorView2.setVisibility(View.GONE);
		refreshIndicatorView2.setImageDrawable(null);
		bar2.setVisibility(View.VISIBLE);
		mfooterViewText.setText("Loading...");
		mfooterRefreshState = REFRESHING;
	}

	/**
	 * 
	 */
	@Override
	public void computeScroll() {
		// TODO Auto-generated method stub

		if (scroller.computeScrollOffset()) {
			int i = this.scroller.getCurrY();
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
			int k = Math.max(i, refreshTargetTop);
			lp.topMargin = k;
			this.refreshView.setLayoutParams(lp);
			this.refreshView.invalidate();
			invalidate();
		}
	}
	
	public void doMovement(int moveY) {
		// TODO Auto-generated method stub
		double lastpage = Math.ceil((double)(totalCount/20));
		if(isvalidate){
			returnInitState();
			isvalidate =false;
			
		}
		
		LinearLayout.LayoutParams lp = (LayoutParams) refreshView.getLayoutParams();
		if(sv.getScrollY() == 0 && moveY > 0&&refreshFooter<=0)
		{
			nowpull=0;
		}
		if(sv.getChildAt(0).getMeasuredHeight()-sv.getMeasuredHeight() == sv.getScrollY()  && moveY < 0){
			nowpull = 1;
			
		}
		
		
		if (nowpull == 0 && mRefreshState != REFRESHING && ParamConstants.PAGE_NUMBER > 0) {
			
			float f1 = lp.topMargin;
			float f2 = f1 + moveY * 0.3F;
			int i = (int) f2;
			lp.topMargin = i;
			if(lp.topMargin>=refreshTargetTop)
				refreshView.setLayoutParams(lp);
			
			
			refreshView.invalidate();
			invalidate();

			
			downTextView.setVisibility(View.VISIBLE);

			refreshIndicatorView.setVisibility(View.VISIBLE);
			information.setVisibility(View.GONE);
			//bar.setVisibility(View.GONE);
			if (lp.topMargin > 20 && mRefreshState != RELEASE_TO_REFRESH) {
				//
				if(ParamConstants.PAGE_NUMBER>0)
					downTextView.setText("Release to Go to Previous Page ["+(1+(ParamConstants.PAGE_NUMBER-1)*20)+"-"+(20+(ParamConstants.PAGE_NUMBER-1)*20)+"]");
				else
					downTextView.setText("Release to Go to Previous Page ");
				
				// refreshIndicatorView.setImageResource(R.drawable.goicon);
				refreshIndicatorView.clearAnimation();
				refreshIndicatorView.startAnimation(mFlipAnimation);
				mRefreshState = RELEASE_TO_REFRESH;

			} else if (lp.topMargin <= 20 && mRefreshState != PULL_TO_REFRESH) {
				downTextView.setText("Pull Down to Go to Previous Page");
				// refreshIndicatorView.setImageResource(R.drawable.goicon);
				if (mRefreshState != TAP_TO_REFRESH) {
					refreshIndicatorView.clearAnimation();
					refreshIndicatorView.startAnimation(mReverseFlipAnimation);

				}
				mRefreshState = PULL_TO_REFRESH;

			} 
		} 
		else if (nowpull == 1 && mfooterRefreshState != REFRESHING && ParamConstants.PAGE_NUMBER <lastpage) {

		/*	LayoutParams svlp = (LayoutParams) sv.getLayoutParams();
			svlp.bottomMargin=svlp.bottomMargin-moveY;
			Log.i("other","svlp.bottomMargin::"+svlp.bottomMargin);
			//svlp.bottomMargin = 50; 
			refreshFooter=svlp.bottomMargin;
			refreshFooter=moveY;
			sv.setLayoutParams(svlp);
			TryPullToRefreshScrollView.ScrollToPoint(sv, sv.getChildAt(0),0);
			refreshIndicatorView.setVisibility(View.VISIBLE); */
			
			
			LayoutParams svlp = (LayoutParams) sv.getLayoutParams();
			LayoutParams rf = (LayoutParams)mfooterView.getLayoutParams();
			
			
			mfooterView.setVisibility(VISIBLE);
		//	rf.height = (int)(moveY);
		//	Log.i("bottomMargin","t1.rf.INITIALbottomMargin"+rf.bottomMargin);
			rf.bottomMargin += (int)(-moveY*0.2F);
			//rf.height =30;
			refreshFooter=rf.bottomMargin;
		//	Log.i("other can scroll","t1.rf.height:"+rf.height);
		//	Log.i("bottomMargin","t1.rf.bottomMargin"+rf.bottomMargin);
			
		//	if(lp.topMargin>=refreshTargetTop)
			mfooterView.setLayoutParams(rf);
			TryPullToRefreshScrollView.ScrollToPoint(sv, sv.getChildAt(0),0);
			

			
			refreshIndicatorView.setVisibility(View.VISIBLE);
			
			if (rf.bottomMargin >= 50 && mfooterRefreshState != RELEASE_TO_REFRESH) {
				refreshIndicatorView2.clearAnimation();
				refreshIndicatorView2.startAnimation(mFlipAnimation);
				 
					if(totalCount%20 >0 && lastpage == ParamConstants.PAGE_NUMBER+1)
						mfooterViewText.setText("Release to Go to Next Page["+(1+(ParamConstants.PAGE_NUMBER+1)*20)+"-"+(((1+ParamConstants.PAGE_NUMBER)*20)+totalCount%20)+"]");
					else if (lastpage == ParamConstants.PAGE_NUMBER)
						mfooterViewText.setText("Release to Go to Next Page");
					else
						mfooterViewText.setText("Release to Go to Next Page["+(1+(ParamConstants.PAGE_NUMBER+1)*20)+"-"+(20+(1+ParamConstants.PAGE_NUMBER)*20)+"]");
					
				mfooterRefreshState = RELEASE_TO_REFRESH;
				
			} else if (rf.bottomMargin < 50 && mfooterRefreshState != PULL_TO_REFRESH) {
				refreshIndicatorView2.clearAnimation();
				refreshIndicatorView2.startAnimation(mReverseFlipAnimation);
				mfooterViewText.setText("Pull Up to Go to Next Page");
				mfooterRefreshState = PULL_TO_REFRESH;
			}
		} 
	}

	
	public void setRefreshListener(RefreshListener listener) {
		this.refreshListener = listener;
	}
	
	public void validate(){
		if (mRefreshState != TAP_TO_REFRESH) {
			mRefreshState = TAP_TO_REFRESH; 
			isvalidate =true;
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
			int i = lp.topMargin;
			refreshIndicatorView.setImageResource(R.drawable.pull_down);
			refreshIndicatorView.clearAnimation();
			information.setVisibility(View.GONE);
		//	bar.setVisibility(View.GONE);
		}
	}

	
	public void finishRefresh() {
		if (mRefreshState != TAP_TO_REFRESH) {
			mRefreshState = TAP_TO_REFRESH; 
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
			int i = lp.topMargin;
			refreshIndicatorView.setImageResource(R.drawable.pull_down);
			refreshIndicatorView.clearAnimation();
			information.setVisibility(View.GONE);
			//bar.setVisibility(View.GONE);
			
	//		downTextView.setText("tap to refresh");
		scroller.startScroll(0, 0, 0, refreshTargetTop*2);
		//		scroller.startScroll(0, 0, 0, 0-i);
		invalidate();
		}
		if (mfooterRefreshState != TAP_TO_REFRESH) {
			resetFooter();
		}
	}

	public void resetFooter() {

		bar2.setVisibility(View.GONE);
		refreshIndicatorView2.setVisibility(View.VISIBLE);
		mfooterRefreshState = TAP_TO_REFRESH;
		resetFooterPadding();
		//mfooterViewText.setText("Tap to refresh..");
		refreshIndicatorView2.setImageResource(R.drawable.pull_up);
		refreshIndicatorView2.clearAnimation();
		TryPullToRefreshScrollView.ScrollToInitialPoint(sv);
		LayoutParams rf = (LayoutParams)mfooterView.getLayoutParams();
		rf.bottomMargin = 0;
		mfooterView.setLayoutParams(rf);
	//	TryPullToRefreshScrollView.ScrollToPoint(sv, sv.getChildAt(0),0);
	
		
	//	scroller.startScroll(0, sv.getScrollY(), 0, -sv.getHeight());
	}

	public void HideFooter() {
		LayoutParams mfvlp = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
	//	mfvlp.bottomMargin = refreshFooter;
		mfvlp.bottomMargin = 0;
		mfooterView.setLayoutParams(mfvlp);
		mfooterRefreshState = TAP_TO_REFRESH;
	}

	
	/*
	 * @see
	 * android.view.ViewGroup#onInterceptTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onInterceptTouchEvent(MotionEvent e) {
		// TODO Auto-generated method stub
		int action = e.getAction();
		int y = (int) e.getRawY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			// lastY = y;
			if (isRecord == false) {
				// Log.i("moveY", "lastY:" + y);
				lastY = y;
				isRecord = true;
			}
			break;

		case MotionEvent.ACTION_MOVE:
			int m = y - lastY;
			if (canScroll(m)) {
				return true;
			}
			break;
		case MotionEvent.ACTION_UP:
	
			isRecord = false;
			break;

		case MotionEvent.ACTION_CANCEL:

			break;
		}
		return false;
	} 

	private boolean canScroll(int diff) {
		// TODO Auto-generated method stub
		LinearLayout.LayoutParams lp = (LayoutParams) refreshView.getLayoutParams();
		View childView;
		childView = this.getChildAt(1);
	//	Log.i("other", "cs.mRefreshState:" + mRefreshState);
	//	Log.i("Scroll Y","cs.Scroll Y:"+((ScrollView) childView).getScrollY());
	//	Log.i("MEASURE HEIGHT","cs.getmeasuredheightCHILD"+((ScrollView) childView).getChildAt(0).getMeasuredHeight());
	//	Log.i("MEASURE HEIGHT","cs.getmeasuredHeight"+((ScrollView) childView).getMeasuredHeight());
		if(-10<diff && diff <10){
			return false;
		}
	
		if(isvalidate){
			returnInitState();
			isvalidate =false;
		}


		if (mRefreshState == REFRESHING || mfooterRefreshState == REFRESHING) {
			return true;
		}
		if (getChildCount() > 1) {
			childView = this.getChildAt(1);
			if (childView instanceof ListView){
				int top = ((ListView) childView).getChildAt(0).getTop();
				int pad = ((ListView) childView).getListPaddingTop();
				if ((Math.abs(top - pad)) < 3 && ((ListView) childView).getFirstVisiblePosition() == 0) {
					return true;
				}else{
					return false;
				}
			
			}else if (childView instanceof ScrollView){
				if (((ScrollView) childView).getScrollY() == 0 && diff > 0) {
					nowpull = 0;
					return true;
				}else if (((ScrollView) childView).getChildAt(0).getMeasuredHeight()-sv.getMeasuredHeight() == sv.getScrollY()){
			//	 {
					if((int)Math.ceil((double)(totalCount/20)) == ParamConstants.PAGE_NUMBER)
						return false;
					
					//sv.getChildAt(0).getMeasuredHeight()-sv.getMeasuredHeight() == sv.getScrollY()  && moveY < 0
					nowpull = 1;
					return true;
				}/* else if((((ScrollView) childView).getChildAt(0).getMeasuredHeight() <= ((ScrollView) childView).getScrollY() + getHeight() && diff < 0)){
					System.out.print("t6.HERE 4");
					nowpull = 1;
					
					return true;
				} */else {
					return false;
				}
					
			}
	}
		
		return false;
	}


	public interface RefreshListener {
		public void onRefresh();
		
	}
}

