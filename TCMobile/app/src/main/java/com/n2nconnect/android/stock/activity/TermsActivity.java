package com.n2nconnect.android.stock.activity;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/*
 * Terms and Conditions doesn't work every time after logged out
 * a temporarily solution to fix it
 */

public class TermsActivity extends CustomWindow {

	private ArrayAdapter<String> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.terms_conditions);
		this.title.setText(getResources().getString(R.string.terms_n_conditions_title));
		this.icon.setImageResource(R.drawable.menuicon_acc_setting);

		Bundle bundle				= this.getIntent().getExtras();
		boolean isFromLogin 		= bundle.getBoolean(ParamConstants.FROM_LOGIN_TAG, false);
		menuButton.setVisibility(isFromLogin ? View.GONE : View.VISIBLE);

		//Added by Thinzar, Change_Request-20160722, ReqNo.8
		final boolean isProceedTouchID	= bundle.getBoolean(ParamConstants.IS_PROCEED_TOUCHID_TAG, false);
		Button btnAgreeTerms	= (Button)findViewById(R.id.btnAgreeTerms);

		( (TextView) findViewById(R.id.tvTitle)).setVisibility(View.GONE);
		adapter = new ArrayAdapter<String>(this, R.layout.list_item_1, R.id.tvItem, AppConstants.brokerConfigBean.getALTermsAndConditions()) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View rowView = convertView;
				if (rowView == null) {
					LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					rowView = vi.inflate(R.layout.list_item_1, null);
				}
				TextView textView = (TextView) rowView.findViewById(R.id.tvItem);
				if (textView != null) {

					textView.setText(AppConstants.brokerConfigBean.getALTermsAndConditions().get(position));
				}

				return rowView;
			}
		};

		((ListView) findViewById(R.id.lvTermsNConditions)).setAdapter(adapter);

		//Added by Thinzar, Change_Request-20160722, ReqNo.8 - START
		btnAgreeTerms.setVisibility(View.VISIBLE);

		btnAgreeTerms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isProceedTouchID) {
					Intent i = new Intent();
					i.setClass(TermsActivity.this, ActivateTouchIDActivity.class);
					startActivity(i);
				} else {
					TermsActivity.this.finish();
				}
			}
		});
		//Added by Thinzar, Change_Request-20160722, ReqNo.8 - END
	}
}