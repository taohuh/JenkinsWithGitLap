package com.n2nconnect.android.stock.model;

import java.io.Serializable;
import java.util.Date;

public class StockNews implements Comparable<StockNews>, Serializable {
	
	private static final long serialVersionUID = 1453000918058885147L;
	private String exchangeCode;
	private String symbolCode;
	private String newsId;
	private String newsTitle;
	private Date newsDate;
	private String categoryId;
	
	// Added by Mary@20130910 - Change_Request-20130724, ReqNo.13
	private String symbolName;
	
	//Change_Request-20170601, ReqNo.2 (Additional parameters required for elastic news)
	private String dateTime;
	private String newsSource;
	
	public String getExchangeCode() {
		return exchangeCode;
	}
	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}
	public String getSymbolCode() {
		return symbolCode;
	}
	public void setSymbolCode(String symbolCode) {
		this.symbolCode = symbolCode;
	}
	public String getNewsId() {
		return newsId;
	}
	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}
	public String getNewsTitle() {
		return newsTitle;
	}
	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}
	public Date getNewsDate() {
		return newsDate;
	}
	public void setNewsDate(Date newsDate) {
		this.newsDate = newsDate;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	
	// Added by Mary@20130910 - Change_Request-20130724, ReqNo.13 - START
	public String getSymbolName() {
		return symbolName;
	}
	public void setSymbolName(String symbolName) {
		this.symbolName = symbolName;
	}
	// Added by Mary@20130910 - Change_Request-20130724, ReqNo.13 - END
	
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getNewsSource() {
		return newsSource;
	}
	public void setNewsSource(String newsSource) {
		this.newsSource = newsSource;
	}
	
	public int compareTo(StockNews compare) {			
		if (this.getNewsDate().after(compare.getNewsDate())) {
			return -1;
		} else if (this.getNewsDate().before(compare.getNewsDate())) {
			return 1;
		} else {
			return 0;
		}
	}	
}
