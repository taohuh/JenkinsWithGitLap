package com.n2nconnect.android.stock.activity;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.StockInfo;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class OrderDetailActivity extends CustomWindow implements OnDoubleLoginListener, OnRefreshStockListener{

	private int orderType;
	private Map<String, String> orderDetails;
	private List<String> orderSequences;

	private String symbolCode;
	private StockSymbol stockSymbol;
	private String orderNo;
	private String orderFixedNo;
	private String paymentType;
	private float orderPrice;
	private float stopLimit;
	private long orderQuantity;
	private long matchedQuantity;
	// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.5
	private long unmatchQuantity;
	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
	private long lngDisclosedQty;
	private long lngMinQty;

	private String ordType;
	private String validity;
	private String goToDate;
	private String paymentCurrency;
	private String lotSize;
	private String stockCode;
	private ProgressDialog dialog;
	private DoubleLoginThread loginThread;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr	= 0;
	private AlertDialog alertException;

	// Added by Mary@20130108 - Fixes_Request-20121102, ReqNo.17
	private String strOriAction;
	
	// Added by Mary@20130226 - Change_Request-20130104, ReqNo.3 - START
	private String splitTrdExchgCode;
	private ExchangeInfo exchgInfoMatch;

	//Added by Thinzar@20140819 - Change_Request-20140801, ReqNo.4&5 - START
	private String triggerPriceType;
	private String triggerPriceDirection;
	
	private Button submitButton;

	//Fixes_Request-20170701, ReqNo.16
	private String orderTicketNo;
	
	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.5
		try{
			super.onCreate(savedInstanceState);
			
			Bundle bundle 	= this.getIntent().getExtras();
			dialog			= new ProgressDialog(this);
			symbolCode 		= bundle.getString(ParamConstants.SYMBOLCODE_TAG);
			orderType 		= bundle.getInt(ParamConstants.ORDER_DETAILTYPE_TAG);
			stockCode		= bundle.getString(ParamConstants.STOCK_CODE_TAG);		//Fixes_Request-20160101, ReqNo.6
			orderDetails 	= (Map<String, String>) bundle.getSerializable(ParamConstants.ORDER_DETAILS_TAG);
			orderSequences 	= (List<String>) bundle.getSerializable(ParamConstants.ORDER_DISPLAYSEQ_TAG);

			loginThread = ((StockApplication) OrderDetailActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) OrderDetailActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(this);
					
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(OrderDetailActivity.this)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			//Change_Request-20170601, ReqNo. 17 (to map ATP exchange code to Feed exchange code)
			symbolCode = SystemUtil.convertSymbolMapSubscribeExchg(symbolCode);

			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				new retrieveStockData().execute();
			
			//Added by Thinzar@20141110, Change_Request-20141101, ReqNo. 2 - START
			if (orderType == AppConstants.ORDER_DETAIL_SUBMIT && AppConstants.brokerConfigBean.isMsgPromptForStopNIfTouchOrdType()) {
				String m_ordType			= orderDetails.get("Order Type").toString();
				String m_action				= orderDetails.get("Action").toString();

				boolean isShowAlertDialog	= false;
				String alertMsg				= null;
				
				if(m_ordType.equalsIgnoreCase("Stop") || m_ordType.equalsIgnoreCase("StopLimit")){
					isShowAlertDialog		= true;
					if(m_action.equalsIgnoreCase("Buy"))	
						alertMsg	= ErrorCodeConstants.STOP_BUY_WARNING_MSG;
					else if(m_action.equalsIgnoreCase("Sell"))
						alertMsg	= ErrorCodeConstants.STOP_SELL_WARNING_MSG;
					
				} else if(m_ordType.equalsIgnoreCase("LimitIfTouched") || m_ordType.equalsIgnoreCase("MarketIfTouched")){
					isShowAlertDialog	= true;
					if(m_action.equalsIgnoreCase("Buy"))	
						alertMsg	= ErrorCodeConstants.IFTOUCH_BUY_WARNING_MSG;
					else if(m_action.equalsIgnoreCase("Sell"))
						alertMsg	= ErrorCodeConstants.IFTOUCH_SELL_WARNING_MSG;
				}
				
				if(isShowAlertDialog){
					new AlertDialog.Builder(this)
							.setMessage(alertMsg)
							.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
												dialog.cancel();
										}
									}).show();
				}
			}
			//Added by Thinzar@20141110, Change_Request-20141101, ReqNo. 2 - END
			
			if (orderType == AppConstants.ORDER_DETAIL_REVISE) {

				lotSize				= String.valueOf( bundle.getInt(ParamConstants.LOT_SIZE_TAG) );
				orderNo				= bundle.getString(ParamConstants.ORDER_NO_TAG);
				orderFixedNo 		= bundle.getString(ParamConstants.ORDER_FIXEDNO_TAG);
				orderPrice 			= bundle.getFloat(ParamConstants.ORDER_PRICE_TAG);
				orderQuantity 		= bundle.getLong(ParamConstants.ORDER_QUANTITY_TAG);
				matchedQuantity 	= bundle.getLong(ParamConstants.ORDER_MATCHEDQTY_TAG);

				// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.5
				unmatchQuantity		= bundle.getLong(ParamConstants.ORDER_UNMATCHQTY_TAG);
				paymentType 		= bundle.getString(ParamConstants.PAYMENT_TYPE_TAG);
				ordType 			= bundle.getString(ParamConstants.ORDER_TYPE_TAG);
				validity			= bundle.getString(ParamConstants.ORDER_VALIDITY_TAG);
				goToDate 			= bundle.getString(ParamConstants.GO_TO_DATE_TAG);
				paymentCurrency 	= bundle.getString(ParamConstants.PAYMENT_CURRENCY_TAG);

				stockCode 			= bundle.getString(ParamConstants.STOCK_CODE_TAG);
				stopLimit 			= bundle.getFloat(ParamConstants.ORDER_STOPLIMIT_TAG);
				String statusCode	= bundle.getString(ParamConstants.ORDER_STATUS_TAG);

				// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
				lngDisclosedQty 	= bundle.getLong(ParamConstants.DISCLOSED_QUANTITY_TAG);
				lngMinQty 			= bundle.getLong(ParamConstants.MINIMUM_QUANTITY_TAG);

				// Added by Mary@20130108 - Fixes_Request-20121102, 17
				strOriAction		= bundle.getString( ParamConstants.ORI_ACTION_TYPE);
				
				//Added by Thinzar@20140819 - Change_Request-20140801, ReqNo,4
				triggerPriceType 	= bundle.getString(ParamConstants.TRIGGER_PRICE_TYPE_TAG);
				triggerPriceDirection= bundle.getString(ParamConstants.TRIGGER_PRICE_DIRECTION_TAG);

				//Fixes_Request-20170701, ReqNo.16
				orderTicketNo 		= bundle.getString(ParamConstants.TICKETNO_TAG);

				setContentView(R.layout.order_detail_view);
	
				this.title.setText(getResources().getString(R.string.order_detail_module_title));

				this.icon.setImageResource( AppConstants.brokerConfigBean.getIntOrdBookMenuIconBgImg() );
	
				// Added by Mary@20130226 - Change_Request-20130104, ReqNo.3 - START
				splitTrdExchgCode			= symbolCode.substring( (symbolCode.lastIndexOf(".") + 1), symbolCode.length() );
				StockApplication application= (StockApplication) OrderDetailActivity.this.getApplication();
				exchgInfoMatch				= null;
				for( Iterator<ExchangeInfo> itr2=application.getExchangeInfos().iterator(); itr2.hasNext(); ){
					ExchangeInfo exchgInfo 	= itr2.next();

					if( exchgInfo.getTrdExchgCode() != null && ( splitTrdExchgCode.equals( exchgInfo.getQCExchangeCode() ) || splitTrdExchgCode.equals( exchgInfo.getTrdExchgCode() ) ) ){
						splitTrdExchgCode	= exchgInfo.getTrdExchgCode();
						exchgInfoMatch		= exchgInfo;
						break;
					}
				}
				// Added by Mary@20130226 - Change_Request-20130104, ReqNo.3 - END
				
				submitButton = (Button) this.findViewById(R.id.submitButton);
				
				if( statusCode.equals("8") || statusCode.equals("2") || statusCode.equals("4") || statusCode.equals("C") )
					submitButton.setVisibility(View.GONE);
				
				submitButton.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
						try{
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
							if(idleThread != null && ! isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
							
							// Added by Mary@20130225 - Change_Request-20130104, ReqNo.3 - START
							if( ( AppConstants.getRDSInfo().isBlockByRDS() && ! AppConstants.getRDSInfo().getStrLocalExchangeCode().contains(splitTrdExchgCode) )  ){
								alertException.setTitle(getResources().getString(R.string.trade_outbound_block_title));
								alertException.setMessage(getResources().getString(R.string.trade_outbound_block_msg));
								alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,	int id) {
										dialog.dismiss();
									}
								});
								// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
								if( ! OrderDetailActivity.this.isFinishing() )
									alertException.show();
								
								return;
							}else{

								// Added by Mary@20130225 - Change_Request-20130104, ReqNo.3 - END
								Intent intent = new Intent();
								intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
								intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_REVISE);
								intent.putExtra(ParamConstants.ORDER_NO_TAG, orderNo);
								intent.putExtra(ParamConstants.ORDER_FIXEDNO_TAG, orderFixedNo);
								intent.putExtra(ParamConstants.ORDER_PRICE_TAG, orderPrice);
								intent.putExtra(ParamConstants.ORDER_QUANTITY_TAG, orderQuantity);
								intent.putExtra(ParamConstants.ORDER_MATCHEDQTY_TAG, matchedQuantity);
								// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.5
								intent.putExtra(ParamConstants.ORDER_UNMATCHQTY_TAG, unmatchQuantity);
								intent.putExtra(ParamConstants.PAYMENT_TYPE_TAG, paymentType);
								intent.putExtra(ParamConstants.ORDER_TYPE_TAG, ordType);
								intent.putExtra(ParamConstants.ORDER_VALIDITY_TAG, validity);
								intent.putExtra(ParamConstants.GO_TO_DATE_TAG, goToDate);
								intent.putExtra(ParamConstants.PAYMENT_CURRENCY_TAG, paymentCurrency);
								
								//Added by Thinzar@20141104, Fixes_Request-20140901, ReqNo. 13
								if (orderType == AppConstants.ORDER_DETAIL_REVISE)	
									lotSize	= String.valueOf(stockSymbol.getIntLotSize());
								
								intent.putExtra(ParamConstants.LOT_SIZE_TAG, lotSize);
								
								intent.putExtra(ParamConstants.ORDER_STOPLIMIT_TAG, stopLimit);
								intent.putExtra(ParamConstants.INSTRUMENT_TAG,stockSymbol.getInstrument());

								// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
								intent.putExtra(ParamConstants.DISCLOSED_QUANTITY_TAG, lngDisclosedQty);
								intent.putExtra(ParamConstants.MINIMUM_QUANTITY_TAG, lngMinQty);

								// Added by Mary@20130108 - Fixes_Request-20121102, 17
								intent.putExtra( ParamConstants.ORI_ACTION_TYPE, strOriAction );

								// Added by Mary@20130925 - Change_Request-20130724, ReqNo.18
								intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, orderType);
								
								//Added by Thinzar@20140819 - Change_Request_20140801,ReqNo.4 - START
								intent.putExtra(ParamConstants.TRIGGER_PRICE_TYPE_TAG, triggerPriceType);
								intent.putExtra(ParamConstants.TRIGGER_PRICE_DIRECTION_TAG, triggerPriceDirection);

								//Fixes_Request-20170701, ReqNo.16
								intent.putExtra(ParamConstants.TICKETNO_TAG, orderTicketNo);

								intent.setClass(OrderDetailActivity.this, TradeActivity.class);
								startActivity(intent);
								OrderDetailActivity.this.finish();
							}
						// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
					}
				});
			} else if (orderType == AppConstants.ORDER_DETAIL_SUBMIT) {
				setContentView(R.layout.order_confirm_view);
				this.title.setText("");

				this.icon.setImageResource( AppConstants.brokerConfigBean.getIntHomeMenuIconBgImg() );
	
				submitButton = (Button) this.findViewById(R.id.submitButton);
				submitButton.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						// Added by Mary@20120803 - Fixes_Request-20120724, ReqNo.15 - START
						view.setEnabled(false);
						view.setVisibility(View.GONE);

						OrderDetailActivity.this.setResult(RESULT_OK); 
						OrderDetailActivity.this.finish();
					}
				});
	
			} else if (orderType == AppConstants.PORTFOLIO_DETAIL_TRADE) {
				setContentView(R.layout.order_detail_view);
	
				this.title.setText(getResources().getString(R.string.portfolio_detail_module_title));
				this.icon.setImageResource( AppConstants.brokerConfigBean.getIntPortfolioMenuIconBgImg() );

				lotSize				= bundle.getString(ParamConstants.LOT_SIZE_TAG);
				
				// Added by Mary@20130502 - Fixes_Request-20130419, ReqNo.8 - START
				splitTrdExchgCode			= symbolCode.substring( (symbolCode.lastIndexOf(".") + 1), symbolCode.length() );
				StockApplication application= (StockApplication) OrderDetailActivity.this.getApplication();
				exchgInfoMatch				= null;
				for( Iterator<ExchangeInfo> itr2=application.getExchangeInfos().iterator(); itr2.hasNext(); ){
					ExchangeInfo exchgInfo 	= itr2.next();

					if( exchgInfo.getTrdExchgCode() != null && ( splitTrdExchgCode.equals( exchgInfo.getQCExchangeCode() ) || splitTrdExchgCode.equals( exchgInfo.getTrdExchgCode() ) ) ){
						splitTrdExchgCode	= exchgInfo.getTrdExchgCode();
						exchgInfoMatch		= exchgInfo;
						break;
					}
				}
				// Added by Mary@20130502 - Fixes_Request-20130419, ReqNo.8 - END

				submitButton = (Button) this.findViewById(R.id.submitButton);
				submitButton.setText(getResources().getString(R.string.trade_btn));
					
				submitButton.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
						try {
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
							if (idleThread != null && !isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130605 - Change_Request-20130424,ReqNo.5 - END

							// Added by Mary@20130225 - Change_Request-20130104,ReqNo.3 - START
							if ((AppConstants.getRDSInfo().isBlockByRDS() && !AppConstants.getRDSInfo()
									.getStrLocalExchangeCode().contains(splitTrdExchgCode))) {
								alertException.setTitle(getResources().getString(R.string.trade_outbound_block_title));
								alertException.setMessage(getResources().getString(R.string.trade_outbound_block_msg));
								alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
									}
								});
								// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
								if (!OrderDetailActivity.this.isFinishing())
									alertException.show();
								return;
							} else {
								// Added by Mary@20130225 - Change_Request-20130104, ReqNo.3 - END
								Intent intent = new Intent();
								intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
								intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
								// Added by Mary@20130925 - Change_Request-20130724, ReqNo.18
								intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, orderType);
								intent.putExtra(ParamConstants.LOT_SIZE_TAG, lotSize);

								intent.setClass(OrderDetailActivity.this, TradeActivity.class);
								startActivity(intent);
								OrderDetailActivity.this.finish();
							}
							// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
						} catch (Exception e) {
							e.printStackTrace();
						}
						// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
					}
				});
				
				
				// Added by Mary@20130226 - Fixes_Request-20130108, ReqNo.14 - START
				if( ! DefinitionConstants.getTrdExchangeCode().contains(splitTrdExchgCode) || exchgInfoMatch == null){
					submitButton.setVisibility(View.GONE);
				}else{
					submitButton.setVisibility(View.VISIBLE);
				}
				// Added by Mary@20130226 - Fixes_Request-20130108, ReqNo.14 - END
			}
			
			// Added by Mary@20121114 - Fixes_Request-20120719, ReqNo.12
			if(splitTrdExchgCode != null && !splitTrdExchgCode.isEmpty())	//Condition Added, Fixes_Request-20160101, ReqNo.16
				AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, splitTrdExchgCode);
		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.5 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.5 - END
	}
	
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
	@Override
	public void onResume(){
		System.out.println("tradeDebug==OrderDetailActivity, onResume()");
		// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.5
		try{
			super.onResume();
			
			currContext	= OrderDetailActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(OrderDetailActivity.this);
				loginThread.resumeRequest();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	
	private class retrieveStockData extends AsyncTask<Void,Void,String>{
		// Added by  Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! OrderDetailActivity.this.isFinishing() )
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(Void... params) {

			try{
				OrderDetailActivity.this.retrieveStockData();
			}catch(Exception e){
				exception = e;
			}
			return null;
			//  Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			try{
				if(dialog != null && dialog.isShowing() )
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
					dialog.dismiss();
				
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							finish();
						}
					});
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! OrderDetailActivity.this.isFinishing() )
						alertException.show();
					exception	= null;
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				if (orderType == AppConstants.ORDER_DETAIL_REVISE)
					OrderDetailActivity.this.prepareOrderDetailHeader();
				else if(orderType == AppConstants.ORDER_DETAIL_SUBMIT)
					OrderDetailActivity.this.prepareOrderConfirmHeader();
				else if(orderType == AppConstants.PORTFOLIO_DETAIL_TRADE)
					OrderDetailActivity.this.prepareOrderDetailHeader();
				
				OrderDetailActivity.this.prepareOrderDetailView();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}		
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if(this.mainMenu.isShowing()){
				this.mainMenu.hide();

			}else{
				finish();
			}
			// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}

	/*public String getLotSize(){
		StockInfo stockInfo = null;

		try {
			String[] symbolCodes = {symbolCode};
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START

			String response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
			while(intRetryCtr < AppConstants.intMaxRetry && response == null){
				intRetryCtr++;
				response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
			}
			intRetryCtr	= 0;

			if(response == null)				
				throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
			// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			stockInfo = QcMessageParser.parseStockDetail(response);

			return Integer.toString(stockInfo.getSharePerLot());

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}*/

	public void retrieveStockData() throws Exception{
		String response = null;
		try {
			String[] symbolCodes = {symbolCode};

			response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_TRADE);
			while(intRetryCtr < AppConstants.intMaxRetry && response == null){
				intRetryCtr++;
				response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_TRADE);
			}
			intRetryCtr	= 0;
			
			if(response == null)				
				throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);

			stockSymbol = QcMessageParser.parseStockTrade(response);
		}catch(Exception e){
			throw e;
		}
	}

	private void prepareOrderDetailHeader() {
		TextView stockNameLabel		= (TextView) this.findViewById(R.id.stockNameLabel);
		TextView companyName		= (TextView) this.findViewById(R.id.companyNameLabel);
		TextView priceLabel			= (TextView) this.findViewById(R.id.priceLabel);
		ImageView arrowImage		= (ImageView) this.findViewById(R.id.arrowImage);
		TextView priceChangeLabel	= (TextView) this.findViewById(R.id.priceChangeLabel);
		TextView percentChangeLabel	= (TextView) this.findViewById(R.id.pricePercentLabel);
		TextView sectorLabel		= (TextView) this.findViewById(R.id.sectorLabel);		//Fixes_Request-20160101, ReqNo.6
		
		if(stockSymbol!=null){
			if(stockSymbol.getStockCode()!=null){

				if(stockSymbol.getStockName(true).length() > 10){
					stockNameLabel.setTextSize(17);
					stockNameLabel.setGravity(Gravity.CENTER_VERTICAL);
				}

				//Fixes_Request-20160101, ReqNo.6
				if(orderType == AppConstants.PORTFOLIO_DETAIL_TRADE){
					stockNameLabel.setText(stockSymbol.getStockName(false));
					sectorLabel.setVisibility(View.VISIBLE);
					sectorLabel.setText(stockSymbol.getSectorName());
				}
				else 
					stockNameLabel.setText(stockSymbol.getStockName(true));
			}else{
				//Edited by Thinzar, to fix empty name at portfolio details
				String tmpStkName	= (stockCode != null)? stockCode:symbolCode;
				if(tmpStkName.length()>15)
					stockNameLabel.setTextSize(12);
				stockNameLabel.setText(tmpStkName);
				
				//Added by Thinzar, to grey out the submit button if stockSymbol is null
				submitButton.setBackgroundResource(R.drawable.button_trd_disabled);
				submitButton.setEnabled(false);
			}

			companyName.setText( stockSymbol.getCompanyName() == null ? "" : stockSymbol.getCompanyName() );
			priceLabel.setText(FormatUtil.formatPrice(stockSymbol.getLastDone(), null, splitTrdExchgCode));		//( FormatUtil.formatPrice(stockSymbol.getLastDone(), null) );

			if(stockSymbol.getPriceChange() > 0)
				arrowImage.setImageResource(R.drawable.arrow_up);
			else if(stockSymbol.getPriceChange() < 0)
				arrowImage.setImageResource(R.drawable.arrow_down);
			else
				arrowImage.setImageDrawable(null);
			
			//priceChangeLabel.setText( FormatUtil.formatPriceChange( stockSymbol.getPriceChange() ) );
			priceChangeLabel.setText(FormatUtil.formatPriceChangeByExchange(stockSymbol.getPriceChange(), splitTrdExchgCode));
			percentChangeLabel.setText(FormatUtil.formatPricePercent(stockSymbol.getChangePercent()));

			int intStatusColor	= getResources().getColor(R.color.NoStatus_FgColor);
			if(stockSymbol.getPriceChange() > 0)
				intStatusColor	= getResources().getColor(R.color.UpStatus_BgColor);
			else if(stockSymbol.getPriceChange() < 0)
				intStatusColor	= getResources().getColor(R.color.DownStatus_BgColor);
			
			priceLabel.setTextColor(intStatusColor);
			priceChangeLabel.setTextColor(intStatusColor);
			percentChangeLabel.setTextColor(intStatusColor);
			// Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - END
		}else{

			stockNameLabel.setText(orderType == AppConstants.ORDER_DETAIL_REVISE  ? stockCode : symbolCode);
			
			arrowImage.setImageDrawable(null);
			companyName.setText("Null");
			priceLabel.setText("0.000");
			priceLabel.setTextColor(Color.BLACK);
			priceChangeLabel.setTextColor(Color.BLACK);
			percentChangeLabel.setTextColor(Color.BLACK);
			priceChangeLabel.setText("0.000");
			percentChangeLabel.setText("0.00%");
		}
	}

	private void prepareOrderConfirmHeader() {
		TextView stockNameLabel	= (TextView) this.findViewById(R.id.stockNameLabel);

		stockNameLabel.setText( stockSymbol.getStockName(true) );
		
		TextView companyName	= (TextView) this.findViewById(R.id.companyNameLabel);
		companyName.setText(stockSymbol.getCompanyName());
	}

	private void prepareOrderDetailView() {
		TableLayout keyTableLayout	= (TableLayout) this.findViewById(R.id.orderKeyTable);
		TableLayout valueTableLayout= (TableLayout) this.findViewById(R.id.orderValueTable);

		int id 						= 0;
		for (Iterator<String> itr=orderSequences.iterator(); itr.hasNext(); ) {
			String orderKey		= itr.next();
			String orderValue	= orderDetails.get(orderKey);

			TableRow keyRow		= new TableRow(this);
			TableRow valueRow	= new TableRow(this);

			keyRow.setId(id);
			keyRow.setLayoutParams(new TableLayout.LayoutParams( TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
			keyRow.setGravity(Gravity.RIGHT);		//added by Thinzar, new design for TCMobile 2.0

			valueRow.setId(id);
			valueRow.setLayoutParams(new TableLayout.LayoutParams( TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );

			View keyView 		= this.getLayoutInflater().inflate(R.layout.order_list_item, null);
			View valueView 		= this.getLayoutInflater().inflate(R.layout.order_list_item, null);

			TextView keyLabel 	= (TextView) keyView.findViewById(R.id.orderItemLabel);
			TextView valueLabel = (TextView) valueView.findViewById(R.id.orderItemLabel);
			
			
			String[] keys = orderKey.split(" ");
				
			//	Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
			
			if(keys.length > 3){
				id = addrows(keys,orderValue,id,(keys.length%3==0)?(int)Math.ceil(keys.length /3)-1:(int)Math.ceil(keys.length /3));
			}else{ 
				keyLabel.setText(orderKey + ":");

				keyLabel.setGravity(Gravity.RIGHT);		//added by Thinzar, new design for TCMobile 2.0
				keyRow.addView(keyLabel);

				valueLabel.setText(orderValue);
				if (orderKey.equalsIgnoreCase("action")) {

					valueLabel.setTextColor( getResources().getColor(orderValue.equalsIgnoreCase("Buy") ? R.color.BuyActionColor : R.color.SellActionColor) );
					valueLabel.setTypeface(null, Typeface.BOLD);		//Added by Thinzar, Change_Request-20160303, ReqNo.3
				}

				valueRow.addView(valueLabel);
				
				keyTableLayout.addView(getHorizontalLine());
				keyTableLayout.addView( keyRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
				
				valueTableLayout.addView(getHorizontalLine());
				valueTableLayout.addView( valueRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,	TableRow.LayoutParams.WRAP_CONTENT) );  
				
				id++;
			}
		}
		
	}
	
	private View getHorizontalLine(){
		View v = new View(this);
		v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1));
		v.setBackgroundColor(getResources().getColor(R.color.separator_view_color));
		
		return v;
	}
	
	private int addrows(String[] keys,String orderValue,int id,int counter){
		if(counter > 0)
			addrows(keys,orderValue,id,counter-1);

		TableLayout keyTableLayout = (TableLayout) this.findViewById(R.id.orderKeyTable);
		TableLayout valueTableLayout = (TableLayout) this.findViewById(R.id.orderValueTable);
		
		TableRow keyRow = new TableRow(this);
		TableRow valueRow = new TableRow(this);
		View keyView = this.getLayoutInflater().inflate(R.layout.order_list_item, null);
		View valueView = this.getLayoutInflater().inflate(R.layout.order_list_item, null);
		
		keyRow.setId(id);
		keyRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
	
		valueRow.setId(id);
		valueRow.setLayoutParams( new TableLayout.LayoutParams( TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT ) );

		TextView keyLabel = (TextView) keyView.findViewById(R.id.orderItemLabel);

		for(int i=0+(3*counter); i<keys.length && i<(2+(3*counter))+1;i++){
			if(i==(keys.length-1))
				keyLabel.setText(keyLabel.getText()+keys[i]);
			else
				keyLabel.setText(keyLabel.getText()+keys[i]+" ");
		
		}
	
		TextView valueLabel = (TextView) valueView.findViewById(R.id.orderItemLabel);
		if(counter==((keys.length%3==0)?((int)Math.ceil(keys.length /3))-1:((int)Math.ceil(keys.length /3)))){
			keyLabel.setText(keyLabel.getText()+":");	
			valueLabel.setText(orderValue);		

		}
		keyRow.addView(keyLabel);
		valueRow.addView(valueLabel);
		
		keyTableLayout.addView(keyRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));  

		valueTableLayout.addView(valueRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)); 
		return id++;
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message = handler3.obtainMessage();
		message.obj		= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;

			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if( OrderDetailActivity.this.mainMenu.isShowing() )
					OrderDetailActivity.this.mainMenu.hide();
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				
				if( !isFinishing() )
					OrderDetailActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {

	}
}