package com.n2nconnect.android.stock.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class ViewPreferenceActivity extends CustomWindow 
	implements OnDoubleLoginListener{

	private ProgressDialog dialog;
	private DoubleLoginThread loginThread;
	private RadioGroup rgViewQtyMeasure;
	private TextView tvViewQtyMeasureDesc;
	private RadioButton rbViewQtyInLot;
	private RadioButton rbViewQtyInUnit;

	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.view_preferences);
	
			this.title.setText(getResources().getString(R.string.view_pref_module_title));
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntAccSettingMenuIconBgImg() );
			
			loginThread				= ((StockApplication) ViewPreferenceActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) ViewPreferenceActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(ViewPreferenceActivity.this);
			
			dialog 					= new ProgressDialog(ViewPreferenceActivity.this);
			
			tvViewQtyMeasureDesc	= (TextView) findViewById(R.id.tvViewQtyMeasurementDesc);
			rbViewQtyInLot			= (RadioButton) findViewById(R.id.rbViewQtyInLot);
			rbViewQtyInUnit			= (RadioButton) findViewById(R.id.rbViewQtyInUnit);
			
			rgViewQtyMeasure		= (RadioGroup) findViewById(R.id.rgViewQtyMeasurement);
			rgViewQtyMeasure.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
	            public void onCheckedChanged(RadioGroup group, int checkedId) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					AppConstants.brokerConfigBean.setIntViewQtyMeasurement(checkedId == R.id.rbViewQtyInLot ? AppConstants.INT_TRADE_QTY_IN_LOT : AppConstants.INT_TRADE_QTY_IN_UNIT);
					
					toggleViewQtySection();
				}
			});
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}
	
	public void onResume(){
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			super.onResume();
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= ViewPreferenceActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(ViewPreferenceActivity.this);
				loginThread.resumeRequest();
			}
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
			
			if( dialog != null && dialog.isShowing() )
				dialog.dismiss();
			
			dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
			/* Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
			*/
			if( (idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) ) && ( ! ViewPreferenceActivity.this.isFinishing() ) )
				dialog.show();
			
			reinitPage();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			if(dialog != null && dialog.isShowing() )
				dialog.dismiss();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	
	public void onPause(){
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onPause();
			
			cacheViewPreferenceSetting();
		
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
			else
				finish();
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public void reinitPage(){
		/* Mary@20130604 - comment unused variable
		SharedPreferences sharedPreferences	= null;
		String strTemp						= null;
		*/
		try{
			/* Mary@20130604 - comment unused variable
			sharedPreferences			= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);			
			this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
			*/
			
			toggleViewQtySection();
		}catch(Exception e){
			e.printStackTrace();
		/* Mary@20130604 - comment unused variable	
		}finally{
			sharedPreferences	= null;
			strTemp				= null;
		*/
		}
	}
	
	public void toggleViewQtySection(){
		/* a) Qty Measurement */
		if(AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT){
			tvViewQtyMeasureDesc.setText(R.string.view_qty_in_lot_desc);
			rbViewQtyInLot.setChecked(true);
		}else{
			tvViewQtyMeasureDesc.setText(R.string.view_qty_in_unit_desc);
			rbViewQtyInUnit.setChecked(true);
		}
	}
	
	public void cacheViewPreferenceSetting(){
		SharedPreferences sharedPreferences			= null;
		Map<String, String> mapBrokerUserPreference	= new HashMap<String, String>();
		try{
			sharedPreferences					= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
			
			AppConstants.brokerConfigBean.setIntViewQtyMeasurement( ( (RadioButton)findViewById(R.id.rbViewQtyInLot) ).isChecked() ? AppConstants.INT_TRADE_QTY_IN_LOT : AppConstants.INT_TRADE_QTY_IN_UNIT);
			mapBrokerUserPreference.put(PreferenceConstants.VIEW_QUANTITY_MEASUREMENT, String.valueOf( AppConstants.brokerConfigBean.getIntViewQtyMeasurement() ) );
			
			AppConstants.cacheBrokerUserDetails(ViewPreferenceActivity.this, sharedPreferences, mapBrokerUserPreference);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			sharedPreferences		= null;
			mapBrokerUserPreference.clear();
			mapBrokerUserPreference	= null;
		}
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(ViewPreferenceActivity.this.mainMenu.isShowing()){
					ViewPreferenceActivity.this.mainMenu.hide();
				}
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					ViewPreferenceActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};
}