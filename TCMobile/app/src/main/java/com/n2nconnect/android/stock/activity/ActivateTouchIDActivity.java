package com.n2nconnect.android.stock.activity;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidParameterSpecException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.InvalidPasswordException;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.E2EEInfo;
import com.n2nconnect.android.stock.touchid.FingerprintEncryptDialogFragment;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

/*
 * Created by Thinzar, Change_Request-20160722, ReqNo.8
 */
public class ActivateTouchIDActivity extends CustomWindow {

	private EditText nameField;
	private EditText passwordField;
	private Button btnActivateTouchID;
	private LinearLayout layout_activate;
	private LinearLayout layout_success;
	private Button btnProceed;
	private ProgressDialog dialog;

	private String username;
	private String password;
	private String senderCode;
	private String strRSAPassword;
	private String strE2EERandomNumber;
	private int intRetryCtr = 0;
	private String encryptedKey = null;
	private String userParamKey = null;
	private byte[] userParamKeyInByte = null;
	private StringBuffer paramBuffer;
	private String results;
	private String userParam;
	public static String brokerId	= null;
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;

	//variables for fingerprint
	private static final String DIALOG_FRAGMENT_TAG = "myFragment";
	private Cipher mEncryptCipher;
	private KeyStore mKeyStore;
	private KeyGenerator mKeyGenerator;
	private static final String TAG = ActivateTouchIDActivity.class.getSimpleName();
	private String strPasswordToEncrypt;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_activate_touchid);

		this.title.setText("Activate TouchID");
		this.icon.setImageResource(R.drawable.menuicon_acc_setting);
		menuButton.setVisibility(View.GONE);

		nameField			= (EditText)findViewById(R.id.nameField);
		passwordField		= (EditText)findViewById(R.id.passwordField);
		btnActivateTouchID	= (Button)findViewById(R.id.btnActivateTouchID);
		layout_activate		= (LinearLayout)findViewById(R.id.layout_activate);
		layout_success		= (LinearLayout)findViewById(R.id.layout_success);
		btnProceed			= (Button)findViewById(R.id.btnProceed);

		dialog				= new ProgressDialog(ActivateTouchIDActivity.this);
		sharedPreferences 	= ActivateTouchIDActivity.this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		editor 				= sharedPreferences.edit();

		btnActivateTouchID.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(nameField.getText().toString().trim().length() == 0){
					showAlertAndFocus("User ID must not be blank", nameField);

					return;
				} else if(passwordField.getText().toString().trim().length() == 0){
					showAlertAndFocus("Password must not be blank", passwordField);

					return;
				}else{
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(nameField.getWindowToken(), 0);

					username 		= nameField.getText().toString().trim();
					password 		= passwordField.getText().toString().trim();
					strPasswordToEncrypt	= password;

					passwordField.setText("");
					if ( AppConstants.brokerConfigBean.isEnableE2EEEncryption() )
						new E2EEELogin().execute(password);
					else
						new ATPLogin().execute();
				}
			}
		});

		btnProceed.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				ActivateTouchIDActivity.this.finish();
			}
		});

		layout_success.setVisibility(View.GONE);
	}

	private void showAlertAndFocus(String errorMsg, final EditText fieldToFocus){
		AlertDialog.Builder builder = new AlertDialog.Builder(ActivateTouchIDActivity.this);
		builder.setMessage(errorMsg)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
						fieldToFocus.requestFocus();
					}
				});
		AlertDialog alert = builder.create();

		if( ! ActivateTouchIDActivity.this.isFinishing() )
			alert.show();
	}

	private class E2EEELogin extends AsyncTask<String, Void, Exception> {
		String response = null;
		String strInputPassword = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			try{
				dialog.setMessage(getResources().getString(R.string.touchid_activate_progress1));

				if( ! ActivateTouchIDActivity.this.isFinishing() )
					dialog.show();

			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected Exception doInBackground(String... params) {
			try {
				response = AtpConnectUtil.tradeGetE2EEParams();

				if (response == null){
					throw new Exception(ErrorCodeConstants.FAIL_GET_E2EE_PARAM);
				}else{ 
					/* catch invalid response, e.g. ")=||||||"  - START */
					if( ( response.split("\r\n") ).length < 2)
						throw new Exception(ErrorCodeConstants.FAIL_GET_VALID_E2EE_PARAM);

					String strTemp	= response.split("\r\n")[1];
					strTemp			= strTemp.substring( strTemp.indexOf(DefinitionConstants.RECORD_HEADER) + DefinitionConstants.RECORD_HEADER.length(), strTemp.length() );
					if(strTemp.split("\\|").length == 0)
						throw new Exception(ErrorCodeConstants.FAIL_GET_VALID_E2EE_PARAM);

					strInputPassword= params[0];
				}
			} catch (InvalidPasswordException e) {
				e.printStackTrace();
				return e;
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();

				return new IOException(ErrorCodeConstants.FAIL_AUTHENTICATION_IOE);
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);

			try {
				if (result != null) {
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();

					ActivateTouchIDActivity.this.showMainThreadAlert(result);
					return;
				}

				E2EEInfo e2eeInfo	= AtpMessageParser.parseE2EEParams(response);

				password 			= FormatUtil.encryptWithE2EE(e2eeInfo, strInputPassword);
				strE2EERandomNumber = e2eeInfo.getStrE2EERandomNumber();

				// RSA Encryption
				PublicKey pkE2EERSAPbKey = FormatUtil.loadRSAPublicKey( e2eeInfo.getStrRSAPublicKey(), e2eeInfo.getStrRSAModulus_Base64(), e2eeInfo.getStrRSAExponent_Base64() );
				strRSAPassword = FormatUtil.encryptWithRSA(pkE2EERSAPbKey, strInputPassword);

				new ATPLogin().execute();

			} catch (Exception e) {
				e.printStackTrace();
				ActivateTouchIDActivity.this.showMainThreadAlert(e);
				return;
			}
		}
	}

	private class ATPLogin extends AsyncTask<Void, Void, Exception> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			try{
				dialog.setMessage(getResources().getString(R.string.touchid_activate_progress2));

				if( ! ActivateTouchIDActivity.this.isFinishing() && !dialog.isShowing())
					dialog.show();

			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected Exception doInBackground(Void... params) {
			PublicKey RSAPublicKey = null;
			try {

				if (AppConstants.brokerConfigBean.isEnableEncryption()) {
					//Change_Request-20170601, ReqNo.14 - START
					if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
						//1. Get RSA Public Key
						RSAPublicKey = AtpConnectUtil.tradeGetPK2A();

						intRetryCtr = 0;
						while (ActivateTouchIDActivity.this.intRetryCtr < AppConstants.intMaxRetry && RSAPublicKey == null) {
							ActivateTouchIDActivity.this.intRetryCtr++;
							RSAPublicKey = AtpConnectUtil.tradeGetPK2A();
						}
						intRetryCtr = 0;

						if(RSAPublicKey == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_PUBLIC_KEY);

						String[] AESKey = AtpConnectUtil.tradeGetKey2A(RSAPublicKey);
						while (ActivateTouchIDActivity.this.intRetryCtr < AppConstants.intMaxRetry && AESKey == null) {
							ActivateTouchIDActivity.this.intRetryCtr++;
							AESKey = AtpConnectUtil.tradeGetKey2A(RSAPublicKey);
						}
						intRetryCtr = 0;

						if (AESKey != null) {
							encryptedKey = AESKey[0];
							userParamKeyInByte = AtpConnectUtil.tradeGetSession2A(AESKey);

							while (ActivateTouchIDActivity.this.intRetryCtr < AppConstants.intMaxRetry && userParamKeyInByte == null) {
								ActivateTouchIDActivity.this.intRetryCtr++;
								userParamKeyInByte = AtpConnectUtil.tradeGetSession2A(AESKey);
							}
							intRetryCtr = 0;

							if (userParamKeyInByte == null)
								throw new Exception(ErrorCodeConstants.FAIL_GET_ENCRYPTED_SESSION);
						} else {
							throw new Exception(ErrorCodeConstants.FAIL_GET_ENCRYPTED_KEY);
						}
					}
					//Change_Request-20170601, ReqNo.14 - END
					else {
						intRetryCtr = 0;

						RSAPublicKey = AtpConnectUtil.tradeGetPK_LBSupport();
						while (ActivateTouchIDActivity.this.intRetryCtr < AppConstants.intMaxRetry && RSAPublicKey == null) {
							ActivateTouchIDActivity.this.intRetryCtr++;
							RSAPublicKey = AtpConnectUtil.tradeGetPK_LBSupport();
						}
						intRetryCtr = 0;

						if(RSAPublicKey == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_PUBLIC_KEY);

						String[] AESKey = AtpConnectUtil.tradeGetKey2(RSAPublicKey);

						while (ActivateTouchIDActivity.this.intRetryCtr < AppConstants.intMaxRetry && AESKey == null) {
							ActivateTouchIDActivity.this.intRetryCtr++;
							AESKey = AtpConnectUtil.tradeGetKey2(RSAPublicKey);
						}
						intRetryCtr = 0;

						if (AESKey != null) {
							encryptedKey = AESKey[0];
							userParamKeyInByte = AtpConnectUtil.tradeGetSession2(AESKey);

							while (ActivateTouchIDActivity.this.intRetryCtr < AppConstants.intMaxRetry && userParamKeyInByte == null) {
								ActivateTouchIDActivity.this.intRetryCtr++;
								userParamKeyInByte = AtpConnectUtil.tradeGetSession2(AESKey);
							}
							intRetryCtr = 0;

							if (userParamKeyInByte == null)
								throw new Exception(ErrorCodeConstants.FAIL_GET_ENCRYPTED_SESSION);
						} else {
							throw new Exception(ErrorCodeConstants.FAIL_GET_ENCRYPTED_KEY);
						}
					}

				} else {
					userParamKey = AtpConnectUtil.tradeGetSession();

					while (ActivateTouchIDActivity.this.intRetryCtr < AppConstants.intMaxRetry && userParamKey == null) {
						ActivateTouchIDActivity.this.intRetryCtr++;
						userParamKey = AtpConnectUtil.tradeGetSession();
					}
					intRetryCtr = 0;

					if(userParamKey == null)
						throw new Exception(ErrorCodeConstants.FAIL_GET_SESSION);
				}

				return null;
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				return new IOException(ErrorCodeConstants.FAIL_AUTHENTICATION_IOE);
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);

			try{
				if (result != null) {

					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
					ActivateTouchIDActivity.this.showMainThreadAlert(result);
					return;
				}

				if (AppConstants.brokerConfigBean.isEnableEncryption()) {
					if (userParamKeyInByte == null)
						return;
				} else {
					if (userParamKey == null)
						return;
				}

				if(encryptedKey != null)
					AppConstants.setEncryptedKey(encryptedKey);

				new tradeLogin().execute();

			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	private class tradeLogin extends AsyncTask<Void, Void, Exception> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(dialog.isShowing())
				dialog.setMessage(getResources().getString(R.string.touchid_activate_progress3));
		}

		@Override
		protected Exception doInBackground(Void... params) {
			Map<String, Object> parameters = new HashMap<String, Object>();

			if (AppConstants.brokerConfigBean.isEnableEncryption())
				parameters.put(ParamConstants.USERPARAM_KEY_INBYTE_TAG, userParamKeyInByte);
			else
				parameters.put(ParamConstants.USERPARAM_KEY_TAG, userParamKey);

			parameters.put(ParamConstants.USERNAME_TAG, username);
			parameters.put(ParamConstants.PASSWORD_TAG, password);

			if (AppConstants.brokerConfigBean.isEnableE2EEEncryption()) {
				parameters.put(ParamConstants.E2EE_RANDOM_NUM_TAG, strE2EERandomNumber);
				parameters.put(ParamConstants.RSA_PASSWORD_TAG, strRSAPassword);
			}

			parameters.put( ParamConstants.PARAM_STR_DEVICE_OS_TYPE, SystemUtil.getDeviceOSType() );
			parameters.put( ParamConstants.PARAM_STR_DEVICE_OS_VERSION, SystemUtil.getDeviceOSVersion() );
			parameters.put( ParamConstants.PARAM_STR_DEVICE_MODEL, SystemUtil.getDeviceModelName() );
			parameters.put( ParamConstants.PARAM_STR_DEVICE_CONNECTIVITY, SystemUtil.getDeviceConnectivity(ActivateTouchIDActivity.this) );
			parameters.put( ParamConstants.PARAM_STR_SIM_OPERATOR_NAME, SystemUtil.getSIMOperatorName(ActivateTouchIDActivity.this) );
			parameters.put( ParamConstants.PARAM_STR_EXTRA_INFO, getResources().getString(R.string.version_tag) );
			parameters.put( ParamConstants.PARAM_STR_SCREEN_SIZE, SystemUtil.getDeviceScreenSize(ActivateTouchIDActivity.this) );
			parameters.put(ParamConstants.BROKER_ID_TAG, brokerId);
			parameters.put(ParamConstants.PARAM_STR_LOGIN_TYPE, "B");

			StringBuffer response 	= new StringBuffer(25600);
			paramBuffer 			= new StringBuffer();

			userParam 				= null;
			try {
				userParam = AtpConnectUtil.tradeLogin(parameters, response, paramBuffer);
			} catch (InvalidPasswordException e) {
				return e;
			} catch (FailedAuthenicationException e) {

				return new IOException(ErrorCodeConstants.FAIL_AUTHENTICATION_IOE);
			} catch (Exception e) {
				return e;
			}

			results 				= response.toString();

			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);

			super.onPostExecute(result);

			if (result != null) {
				try{

					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();

				}catch(Exception e){
					e.printStackTrace();
				}

				ActivateTouchIDActivity.this.showMainThreadAlert(result);
				return;
			}

			Properties properties 	= new Properties();
			try {
				properties.load( new ByteArrayInputStream( results.getBytes(AppConstants.STRING_ENCODING_FORMAT) ) );
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				ActivateTouchIDActivity.this.showMainThreadAlert(e);
				e.printStackTrace();
			}

			senderCode = properties.getProperty(AppConstants.ATP_SENDER_CODE);

			if (userParam != null)
				new ValidatePreLogin().execute();
		}
	}

	private class ValidatePreLogin extends AsyncTask<Void, Void, Exception> {

		private String deviceId		= null;
		String response 			= null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			try{
				deviceId			= SystemUtil.getDeviceIMEI(ActivateTouchIDActivity.this);

				if(dialog.isShowing())
					dialog.setMessage(getResources().getString(R.string.touchid_activate_progress4));

			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected Exception doInBackground(Void... arg0) {
			Map<String, Object> parameters	= new HashMap<String, Object>();

			try{

				if (AppConstants.brokerConfigBean.isEnableEncryption())
					parameters.put(ParamConstants.USERPARAM_KEY_INBYTE_TAG, userParamKeyInByte);
				else
					parameters.put(ParamConstants.USERPARAM_KEY_TAG, userParamKey);

				parameters.put(ParamConstants.USERNAME_TAG, username);

				if (AppConstants.brokerConfigBean.isEnableE2EEEncryption()) {
					parameters.put(ParamConstants.E2EE_RANDOM_NUM_TAG, strE2EERandomNumber);
					parameters.put(ParamConstants.RSA_PASSWORD_TAG, strRSAPassword);
				}
				parameters.put(ParamConstants.PASSWORD_TAG, password);
				parameters.put(ParamConstants.DEVICE_ID_TAG, deviceId);
				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);

				response	= AtpConnectUtil.ValidatePreLogin(parameters);

				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response = AtpConnectUtil.ValidatePreLogin(parameters);
				}
				intRetryCtr	= 0;

				return null;
			}catch(Exception e){
				e.printStackTrace();

				return e;
			}
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);

			try{
				if (result != null) {

					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
					ActivateTouchIDActivity.this.showMainThreadAlert(result);
					return;
				} else{
					if(dialog.isShowing() && !ActivateTouchIDActivity.this.isFinishing())
						dialog.dismiss();

					if(response != null && !response.isEmpty()){
						Map<String, String> responseMap	= AtpMessageParser.parseValidatePreLogin(response);

						String preLoginToken	= responseMap.get(ParamConstants.PRE_LOGIN_TOKEN_TAG);
						String statusCode		= responseMap.get(ParamConstants.STATUS_CODE_TAG);
						String statusMsg		= responseMap.get(ParamConstants.STATUS_MSG_TAG);

						if(statusCode.equals("0")){
							//save preLoginToken to sharedPreferences
							editor.putString(PreferenceConstants.VALIDATE_PRE_LOGIN_TOKEN, preLoginToken);
							editor.commit();

							//TODO: ATP not sending back mobile token

							//SUCCESS: start fingerprint encryption
							try {
								mKeyStore = KeyStore.getInstance("AndroidKeyStore");
							} catch (KeyStoreException e) {
								throw new RuntimeException("Failed to get an instance of KeyStore", e);
							}
							try {
								mKeyGenerator = KeyGenerator
										.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
							} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
								throw new RuntimeException("Failed to get an instance of KeyGenerator", e);
							}

							if (initEncryptCipher()) {
								FingerprintEncryptDialogFragment mFragment = new FingerprintEncryptDialogFragment();
								Bundle bundle = new Bundle();
								bundle.putString(ParamConstants.FINGERPRINT_DIALOG_TITLE_TAG, getResources().getString(R.string.pager_title_activate_touchid));
								mFragment.setArguments(bundle);

								mFragment.setEncryptCipher(mEncryptCipher);
								mFragment.setPasswordToEncrypt(strPasswordToEncrypt);
								mFragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
							}
						} else{

							editor.putBoolean(PreferenceConstants.IS_PRE_LOGIN_VALIDATED, false);	//overwrite if exist previously
							editor.commit();

							showAlertAndFocus(statusMsg, passwordField);
						}
					}
				}

			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public void showMainThreadAlert(Exception ex) {
		Message message = handler.obtainMessage();
		message.obj = ex;
		handler.sendMessage(message);
	}

	final Handler handler = new Handler() {
		public void handleMessage(Message message) {
			Exception ex = (Exception) message.obj;
			SystemUtil.showExceptionAlert(ex, ActivateTouchIDActivity.this);
		}
	};

	private boolean initEncryptCipher() {

		mEncryptCipher = getCipher(Cipher.ENCRYPT_MODE);
		if (mEncryptCipher == null) {
			// try again after recreating the keystore
			createKey();
			mEncryptCipher = getCipher(Cipher.ENCRYPT_MODE);
		}
		return (mEncryptCipher != null);

	}

	public Cipher getCipher(int mode) {
		Cipher cipher;

		try {
			mKeyStore.load(null);
			byte[] iv;
			cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
					+ KeyProperties.BLOCK_MODE_CBC + "/"
					+ KeyProperties.ENCRYPTION_PADDING_PKCS7);
			IvParameterSpec ivParams;
			if (mode == Cipher.ENCRYPT_MODE) {
				cipher.init(mode, getKey());
			}

			return cipher;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@TargetApi(23)
	@SuppressLint("NewApi")
	private SecretKey createKey() {
		try {

			// Set the alias of the entry in Android KeyStore where the key will appear
			// and the constrains (purposes) in the constructor of the Builder
			mKeyGenerator.init(new KeyGenParameterSpec.Builder(PreferenceConstants.KEY_NAME,
					KeyProperties.PURPOSE_DECRYPT | KeyProperties.PURPOSE_ENCRYPT)
					.setBlockModes(KeyProperties.BLOCK_MODE_CBC)		 	// Require the user to authenticate with a fingerprint to authorize every use of the key
					.setUserAuthenticationRequired(true)
					.setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
					.build());
			return mKeyGenerator.generateKey();

		} catch (Exception e) {

		}
		return null;
	}

	private SecretKey getKey() {
		try {
			mKeyStore.load(null);
			SecretKey key = (SecretKey) mKeyStore.getKey(PreferenceConstants.KEY_NAME, null);
			if (key != null) return key;
			return createKey();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean tryEncrypt(String secert) {
		try {

			byte[] encryptedPassword = mEncryptCipher.doFinal(secert.getBytes());

			IvParameterSpec ivParams = mEncryptCipher.getParameters().getParameterSpec(IvParameterSpec.class);
			String iv = Base64.encodeToString(ivParams.getIV(), Base64.DEFAULT);

			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean(PreferenceConstants.IS_PRE_LOGIN_VALIDATED, true);
			editor.putString(PreferenceConstants.KEY_PASSWORD, Base64.encodeToString(encryptedPassword, Base64.DEFAULT));
			editor.putString(PreferenceConstants.KEY_USERNAME, username);
			editor.putString(PreferenceConstants.KEY_PASSWORD_IV, iv);
			editor.commit();
			return true;


		} catch (BadPaddingException | IllegalBlockSizeException e) {
			Toast.makeText(this, "Failed to encrypt the data with the generated key. "
					+ "Retry the purchase", Toast.LENGTH_LONG).show();
			Log.e(TAG, "Failed to encrypt the data with the generated key." + e.getMessage());
		} catch (InvalidParameterSpecException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void onEncryptAuthenSuccess(boolean bEncrypt) {
		if(bEncrypt){
			System.out.println("Fingerprint encryption success!");
			layout_activate.setVisibility(View.GONE);
			layout_success.setVisibility(View.VISIBLE);
		}else{
			//TODO:show alert later
			System.out.println("Fingerprint encryption error!");
		}
	}
}