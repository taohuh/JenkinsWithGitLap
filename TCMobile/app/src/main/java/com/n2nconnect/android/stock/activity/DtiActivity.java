package com.n2nconnect.android.stock.activity;

import java.io.IOException;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.DtiAdapter;
import com.n2nconnect.android.stock.model.DtiResult;
import com.n2nconnect.android.stock.util.HttpHelper;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class DtiActivity extends CustomWindow {
	
	private String dtiFullLink;
	private HttpHelper httpHelper;
	private ProgressDialog dialog;
	private ListView lvDti;
	private TextView tvNoDtiMsg;
	private TextView tvDtiDate;
	
	ArrayList<DtiResult> dtiResultArrayListFromIntent;
	private final int DTI_TERMS_INTENT_ID	= 1001;
	//private final int dtiTotalRetry			= 3;		//if no dti available on Monday, try the day before
	private int retryCycle	= 0;
	
	//private String todayDateString;
	private String dtiDateString;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dti);
		
		this.title.setText(getResources().getString(R.string.dti_module_title));
		this.icon.setImageResource(R.drawable.menuicon_dti);
		
		httpHelper	= new HttpHelper();
		dialog		= new ProgressDialog(DtiActivity.this);
		lvDti		= (ListView)findViewById(R.id.lv_dti);
		tvDtiDate	= (TextView)findViewById(R.id.tv_dti_date);
		tvNoDtiMsg	= (TextView)findViewById(R.id.tv_no_dti_msg);
		tvNoDtiMsg.setVisibility(View.GONE);
		
		dtiResultArrayListFromIntent	= new ArrayList<DtiResult>();
		/*commented by Thinzar, API change
		todayDateString		= SystemUtil.getTodayDate(FormatUtil.format_yyyyMMdd);
		
		Date todayDate	= Calendar.getInstance().getTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(todayDate);
		
		if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY){
			dtiDateString		= SystemUtil.getNDaysFromDate(todayDateString, -3);
		}else{
			dtiDateString		= SystemUtil.getNDaysFromDate(todayDateString, -1);
		}
		*/
		Bundle bundle	= this.getIntent().getExtras();
		if(bundle != null && bundle.containsKey(ParamConstants.DTI_RESULT_ARRAYLIST_TAG)){
			dtiResultArrayListFromIntent	= bundle.getParcelableArrayList(ParamConstants.DTI_RESULT_ARRAYLIST_TAG);
			
			if(!AppConstants.hasDtiTermsShown){
				Intent dtiTermsIntent	= new Intent();
				dtiTermsIntent.putExtra(ParamConstants.IS_FROM_DTI_TAG, true);
				dtiTermsIntent.setClass(DtiActivity.this, DtiTermsActivity.class);
				startActivityForResult(dtiTermsIntent, DTI_TERMS_INTENT_ID);
			}else{
				prepareDtiArrayAdapter(dtiResultArrayListFromIntent);
			}
		}else{
			
			//dtiFullLink	= AppConstants.getDtiApiFullUrl(dtiDateString);
			dtiFullLink		= AppConstants.brokerConfigBean.getDtiApiUrl();
			new FetchDtiTask().execute(dtiFullLink);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
		case 1001:
			if(resultCode == RESULT_CANCELED){
				DtiActivity.this.finish();
			}else{
				AppConstants.hasDtiTermsShown	= true;
				prepareDtiArrayAdapter(dtiResultArrayListFromIntent);
			} 
			break;
		}
	}

	private class FetchDtiTask extends AsyncTask<String, Void, Exception>{
		private Exception ex	= null;
		private String dtiResultString	= null;
		private ArrayList<DtiResult> dtiResultArrayListFromServer;
		//private String dtiLogUrl;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			//for DTI logging purpose
			/*dtiLogUrl	= "https://api.recognia.com/api/logusage?cid=207&uid=admin&pwd=tradeCONTROL&tuid=" 
						+ AppConstants.userSenderName + "&page=dti_listing_mobile";
			*/
			retryCycle++;
			dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
			
			if(!dialog.isShowing() && !DtiActivity.this.isFinishing())
				dialog.show();
		}
		
		@Override
		protected Exception doInBackground(String... params) {
			String url	= params[0];
			DefinitionConstants.Debug("[DTI] API:" + url);
			
			try{
				dtiResultString	= httpHelper.doGet(url);
				dtiResultArrayListFromServer	= SystemUtil.parseDtiResult(dtiResultString);
				
				/*
				//DTI logging purpose
				String logDetails = httpHelper.doGet(dtiLogUrl);
				*/
			}catch(IOException e){
				ex = e;
				e.printStackTrace();
			} catch(Exception e){
				ex = e;
				e.printStackTrace();
			}
			
			return ex;
		}
		
		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);
			
			if(dialog.isShowing() && !DtiActivity.this.isFinishing())
				dialog.dismiss();
			
			if(result != null){
				displayAlertDialog(result.getMessage().toString());
			}else{
				prepareDtiArrayAdapter(dtiResultArrayListFromServer);
			}
		}
	}
	
	private void prepareDtiArrayAdapter(final ArrayList<DtiResult> mDtiResultArrayList){
		if(mDtiResultArrayList.size() > 0){	
			//tvDtiDate.setText(FormatUtil.formatDateString(FormatUtil.parseStringToDate(dtiDateString, FormatUtil.format_yyyyMMdd), "dd MMM yyyy"));
			tvDtiDate.setVisibility(View.GONE);
			
			DtiAdapter dtiAdapter	= new DtiAdapter(DtiActivity.this, R.layout.custom_row_dti, mDtiResultArrayList);
			lvDti.setAdapter(dtiAdapter);
			
			lvDti.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					DtiResult dti	= mDtiResultArrayList.get(position);

					Intent dtiDetailIntent	= new Intent();
					dtiDetailIntent.setClass(DtiActivity.this, DtiDetailsActivity.class);
					dtiDetailIntent.putExtra(ParamConstants.DTI_ID_TAG, dti.getId());
					dtiDetailIntent.putExtra(ParamConstants.DTI_INSTRUMENT_SYM_SOMMON_TAG, dti.getInstrumentSymbolCommon());
					dtiDetailIntent.putExtra(ParamConstants.DTI_INSTRUMENT_EXCHG_TAG, dti.getInstrumentExchange());
					dtiDetailIntent.putExtra(ParamConstants.DTI_INSTRUMENT_ISIN_TAG, dti.getInstrumentIsin());
					dtiDetailIntent.putExtra(ParamConstants.DTI_TRADE_TYPE_TAG, dti.getTradeType());

					startActivity(dtiDetailIntent);
				}
			});
			
		}else{
			
			tvNoDtiMsg.setVisibility(View.VISIBLE);
			lvDti.setVisibility(View.GONE);
			/*
			if(retryCycle < dtiTotalRetry){
				//dtiDateString		= SystemUtil.getNDaysFromDate(dtiDateString, -1);
				//dtiFullLink			= AppConstants.getDtiApiFullUrl(dtiDateString);
				dtiFullLink	= AppConstants.brokerConfigBean.getDtiApiUrl();
				
				new FetchDtiTask().execute(dtiFullLink);
			}else{
				tvNoDtiMsg.setVisibility(View.VISIBLE);
				lvDti.setVisibility(View.GONE);
			}
			*/
		}
	}
	
	private void displayAlertDialog(String errorMsg){
		AlertDialog.Builder builder	= new AlertDialog.Builder(DtiActivity.this);
		builder.setMessage(errorMsg)
		.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				dialog.dismiss();
				DtiActivity.this.finish();
			}
		});
		AlertDialog alert	= builder.create();
		
		alert.show();
	}
}