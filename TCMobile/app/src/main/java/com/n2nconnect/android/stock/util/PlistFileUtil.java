package com.n2nconnect.android.stock.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Map;

import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ParamConstants;

import xmlwise.Plist;
import xmlwise.XmlParseException;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
/*
 * Thinzar@20140624 
 * - request by KT, to move out config files to server
 * - based on DefaultConfigBean.java and plistFile
 */
public class PlistFileUtil {
	
	//public static String LMS_JSP_PATH = "http://lms-my.asiaebroker.com/ebcServlet/LMSGetSetting.jsp";	//moved to AppConstants.java, Change_Request-20140901, ReqNo. 3
	public static String ICONS_FOLDER_NAME = "icons";
	public static String PLIST_FOLDER_NAME = "plist";	//folder name in /data/data
	public static String ASSETS_PLIST_FOLDER_NAME = "plist";
	public static final String TAG = "PlistFileUtil";
	
	public static boolean copyAssets(Context mCtx, String oriFilePath, String destFilePath) {
		//Log.d(TAG, "copying assets");
	    AssetManager assetManager = mCtx.getAssets();
	    boolean isSuccess = false;
	   
	    Log.d(TAG, "copying file: " + oriFilePath);
		DefinitionConstants.Debug("copying file: " + oriFilePath);
	       
	    InputStream in = null;
	    OutputStream out = null;
	    try {
	    	in = assetManager.open(oriFilePath);
	        File outFile = new File(destFilePath);
	        out = new FileOutputStream(outFile);
	        copyFile(in, out);
	        in.close();
	        in = null;
	        out.flush();
	        out.close();
	        out = null;
	        
	        isSuccess = true;
	    } catch(IOException e) {
	    	Log.e(TAG, "Failed to copy asset file: " + oriFilePath + " to " + destFilePath, e);
	    	e.printStackTrace();
	    	isSuccess = false;
	    }           
	    
	    return isSuccess;
	}//end copyAssets
	
	public static void copyFile(InputStream in, OutputStream out) throws IOException {
	    byte[] buffer = new byte[1024];
	    int read;
	    while((read = in.read(buffer)) != -1){
	      out.write(buffer, 0, read);
	    }
	} //end copyFile
	
	public static BitmapDrawable getImgFrmPath(String url, int scaledWidth, int scaledHeight){
		BitmapDrawable d = null;
		
		Bitmap icon = BitmapFactory.decodeFile(url);
		icon = Bitmap.createScaledBitmap(icon, scaledWidth, scaledHeight, true);
		
		d = new BitmapDrawable(icon);
		
		return d;
	}
	
	public static boolean writeToExternalStorage(InputStream in, String path, String filename){
		Log.d(TAG, "writing file to external sd, " + filename);
		
		try {
			StringBuilder sb = inputStreamToStringBuilder(in);
			
			File file = new File(path, filename);
		    if(file.exists()) file.delete();
		    BufferedWriter writer = new BufferedWriter(new FileWriter(file));
	        writer.write(sb.toString());
	        writer.flush();
	        
	        return true;
		} catch (IOException e1) {
			e1.printStackTrace();
			
			return false;
		}
	}
	
	public static boolean checkPlistIsMultiLogin(Context mCtx, String path, String filename){
		
		Map<String, Object> properties	= null;
		boolean isMultiLogin = false;
		
		try{
			properties = readPlistFile(path, filename);
		} catch (Exception ex){
			ex.printStackTrace();
		}
		
		//Added by Thinzar, GoogleCrashreport-20141001, ReqNo.2 - START
		if(properties == null || !properties.containsKey("Version")){
			String oriFilePath	= PlistFileUtil.ASSETS_PLIST_FOLDER_NAME + "/" + filename;
			String destFilePath	= mCtx.getFilesDir().getPath() + "/" + PlistFileUtil.PLIST_FOLDER_NAME + "/" + filename;
			PlistFileUtil.copyAssets(mCtx, oriFilePath, destFilePath);
			
			properties = readPlistFile(path, filename);
		}
		//Added by Thinzar, GoogleCrashreport-20141001, ReqNo.2 - END
		
		isMultiLogin = properties.containsKey("BrokerList");
		Log.d("PlistFileUtil", "isMulitLogin: " + isMultiLogin);
		
		return isMultiLogin;
	}
	
	public static String checkMinVersionAndIsMultiLogin(Context mCtx, String path, String filename, String currentVersion){
		Map<String, Object> properties	= null;
		String result		 	= null;
		String minVersion;
		boolean isMultiLogin 	= false;
		
		try{
			properties = readPlistFile(path, filename);
		} catch (Exception ex){
			ex.printStackTrace();
		}
		
		//Added by Thinzar, GoogleCrashreport-20141001, ReqNo.2 - START
		if(properties == null || !properties.containsKey("Version")){
			String oriFilePath	= PlistFileUtil.ASSETS_PLIST_FOLDER_NAME + "/" + filename;
			String destFilePath	= mCtx.getFilesDir().getPath() + "/" + PlistFileUtil.PLIST_FOLDER_NAME + "/" + filename;
			PlistFileUtil.copyAssets(mCtx, oriFilePath, destFilePath);
					
			properties = readPlistFile(path, filename);
		}
		//Added by Thinzar, GoogleCrashreport-20141001, ReqNo.2 - END
		
		if(properties != null && properties.containsKey("MinVersion")){
			minVersion	= properties.get("MinVersion").toString();
		} else {
			minVersion	= currentVersion;
		}
			
		if(!isCurrentVersionNewer(minVersion, currentVersion)){
			result	= ParamConstants.MIN_VERSION_NOT_MET_TAG;
		} else{
			isMultiLogin 	= properties.containsKey("BrokerList");
			if(isMultiLogin){
				result		= ParamConstants.MULTI_LOGIN_TAG;
			} else
				result 		= ParamConstants.SINGLE_LOGIN_TAG;
		}
	
		return result;
	}
	
	public static Map<String, Object> readPlistFile(String path, String filename){
		Map<String, Object> properties = null;
		File file = new File(path, filename);
		
		if(file.exists()){	
			try {
				InputStream in = new FileInputStream(file);
				StringBuilder sb = inputStreamToStringBuilder(in);
				
				properties = Plist.fromXml(sb.toString());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException ioe){
				ioe.printStackTrace();
			} catch (XmlParseException e) {
				e.printStackTrace();
			} 
		} else {
			Log.d(TAG, path + "/" + filename + " not exists.");
		}
		
		return properties;
	}
	
	public static StringBuilder inputStreamToStringBuilder(InputStream in){
		
		StringBuilder sb = new StringBuilder();
		String line = "";
		
		try{
			InputStreamReader is = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(is);
			
			while((line = br.readLine()) != null){
				sb.append(line);
			}
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
		
		return sb;
	}
	
	public static String getPlistFileNameFormat(String pkgName){
		String plistFileNameFormat = "config_" + pkgName + ".plist";
		
		return plistFileNameFormat;
	}
	/* This code block doesn't return correct message for 0.2.0 vs 1.0.0, commented out by Thinzar@20160315 and replaced with enhanced code
	private static boolean isCurrentVersionNewer(String minVersion, String currentVer){
	 	String[] minVerArray		= minVersion.split("\\.");
	  	String[] currentVerArray	= currentVer.split("\\.");
	  	
	  	for(int i=0;i<minVerArray.length;i++){
	  		if(currentVerArray[i].compareTo(minVerArray[i]) < 0) return false;
	  	}
	  	
	  	return true;
	  }
	*/
	private static boolean isCurrentVersionNewer(String minVersion, String currentVer) {
		String[] minVerArray = minVersion.split("\\.");
		String[] currentVerArray = currentVer.split("\\.");

		for (int i = 0; i < minVerArray.length; i++) {
			if (currentVerArray[i].compareTo(minVerArray[i]) > 0) {
				return true;
			} else if (currentVerArray[i].compareTo(minVerArray[i]) < 0) {
				return false;
			}
		}
		return true;
	}
}