package com.n2nconnect.android.stock.imageslideshow.utils;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.activity.ShowInfoActivity;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;


public class ImageSlideAdapter extends PagerAdapter {
	int [] products;
	String []title;
	String []  desc;
	 Context ctx;
	public ImageSlideAdapter(Context ctx, int[] tutorialImage,String []title,String []  desc) {
		this.products = tutorialImage;
		this.ctx=ctx;
		this.title = title;
		this.desc = desc;
	}

	@Override
	public int getCount() {
		return products.length;
	}

	@Override
	public View instantiateItem(ViewGroup container, final int position) {
		LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.vp_image, container, false);

		ImageView mImageView = (ImageView) view.findViewById(R.id.image_display);
		mImageView.setImageResource(products[position]);
		
		Button btnSubscribe =(Button)view.findViewById(R.id.btn_subscribe);
		if(position==4){
			
			if(AppConstants.brokerConfigBean.getStrLMSSubscriptionUrl()!=null && !AppConstants.brokerConfigBean.getStrLMSSubscriptionUrl().equals("")){
				btnSubscribe.setVisibility(View.VISIBLE);
				btnSubscribe.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent intent=new Intent(ctx, ShowInfoActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
						intent.putExtra(ParamConstants.FROM_LOGIN_TAG, true);
						intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_SUBSCRIBE_SERVICE_TYPE);
						ctx.getApplicationContext().startActivity(intent);
					}
				}); 
			}else{
				btnSubscribe.setVisibility(View.GONE);
			}
			
		}else{
			btnSubscribe.setVisibility(View.GONE);
		}
		
		
		
		TextView tvTitle =(TextView)view.findViewById(R.id.tv_title);
		tvTitle.setText(title[position]);
		TextView tvDesc =(TextView)view.findViewById(R.id.tv_desc);
		tvDesc.setText(desc[position]);
		container.addView(view);
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
}