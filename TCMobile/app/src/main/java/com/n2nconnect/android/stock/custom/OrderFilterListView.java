package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class OrderFilterListView {

	private List orderfiltertype;
	private OnOrderFilterSelectedListener listener;
	private Context context;
	private LayoutInflater layoutInflater;
	private PopupWindow popupWindow;
	private boolean isShowing;
	private boolean hideOnSelect;
	private int selectedRow;
	private View windowView;
	private String selectedFilter;

	public void positionFilterOrder() {

		ScrollView filterScroll = (ScrollView) windowView.findViewById(R.id.orderfilterScroll);
		
		String selected = selectedFilter;

		int index = 1;

		for (int i=0; i<orderfiltertype.size(); i++) {
			String compare = (String) orderfiltertype.get(i);
			if (compare.equals(selected)){
				break;
			}
			index++;
		}

		int scrollHeight = (index * 35) - 80;
		if (scrollHeight>0) {
			filterScroll.scrollTo(0, scrollHeight);
		}
		TableLayout tableLayout = (TableLayout) windowView.findViewById(R.id.orderfilterTable);
		TableRow tableRow = (TableRow) tableLayout.getChildAt(index-1);
		TextView textLabel = (TextView) tableRow.getChildAt(0);
		textLabel.setBackgroundColor(Color.DKGRAY);
		tableRow.setBackgroundColor(Color.DKGRAY);
		textLabel.setTextColor(Color.WHITE);
		selectedRow = index - 1;
	}

	public interface OnOrderFilterSelectedListener {
		public void orderFilterSelectedEvent(String OrderFilter);
	}

	public boolean isShowing() { 
		return isShowing; 
	}

	public void setHideOnSelect(boolean doHideOnSelect) { 
		hideOnSelect = doHideOnSelect; 
	} 

	public synchronized void setOrderFilterList(List orderfiltertype) throws Exception {
		if (isShowing) {
			throw new Exception("Menu list may not be modified while menu is displayed.");
		}
		this.orderfiltertype = orderfiltertype;
	}

	public OrderFilterListView(Context context, OnOrderFilterSelectedListener listener, LayoutInflater inflater) {
		this.listener = listener;
		orderfiltertype = new ArrayList();
		this.context = context;
		layoutInflater = inflater;
	}

	public OrderFilterListView(OnClickListener onClickListener,
			OnClickListener onClickListener2, LayoutInflater layoutInflater2) {
		// TODO Auto-generated constructor stub
	}

	public synchronized void show(View view, String orderType) {

		this.selectedFilter = orderType;
		isShowing = true;
		boolean isLandscape = false;
		int itemCount = orderfiltertype.size();
		if (itemCount<1) {
			return; 
		}
		if (popupWindow != null) {
			return; 
		}
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		if (display.getWidth() > display.getHeight()) {
			isLandscape = true;
		}

		windowView = layoutInflater.inflate(R.layout.orderfilter_table_window, null);

		this.constructTableView();

		Button doneButton = (Button) windowView.findViewById(R.id.filterdoneButton);
		doneButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(CustomWindow.idleThread != null && ! CustomWindow.isShowingIdleAlert)
					CustomWindow.idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				OrderFilterListView.this.hide();
				listener.orderFilterSelectedEvent(selectedFilter);
			}
		});

		popupWindow = new PopupWindow(windowView, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, false);
		popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
		popupWindow.setWidth(display.getWidth());
		popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

		windowView.post(new Runnable() {
			public void run() {
				OrderFilterListView.this.positionFilterOrder();
			}
		});

	}

	private void constructTableView() {

		final TableLayout tableLayout = (TableLayout) windowView.findViewById(R.id.orderfilterTable);

		int id = 0;
		for (int i=0; i<orderfiltertype.size(); i++) {
			
			if(selectedFilter==null){
				if(i==0){
					selectedFilter = (String) orderfiltertype.get(0);
				}
			}
			
			String filter = (String) orderfiltertype.get(i);

			final TableRow tableRow = new TableRow(context);   
			tableRow.setId(id);
			tableRow.setLayoutParams(new TableLayout.LayoutParams(
					TableLayout.LayoutParams.MATCH_PARENT,
					TableLayout.LayoutParams.WRAP_CONTENT));

			tableRow.setWeightSum(1);

			final TextView textLabel = (TextView) layoutInflater.inflate(R.layout.ordefilter_list_item, null);
			textLabel.setText(filter);

			tableRow.addView(textLabel);

			tableRow.setOnClickListener(new OnClickListener() {

				public void onClick(View view) {

					final TableRow previousRow = (TableRow) tableLayout.getChildAt(selectedRow);
					TextView previousLabel = (TextView) previousRow.findViewById(R.id.orderFilterLabel);
					previousLabel.setBackgroundDrawable(null);
					previousRow.setBackgroundDrawable(null);
					previousLabel.setTextColor(Color.BLACK);

					selectedRow = tableRow.getId();
					selectedFilter = (String)orderfiltertype.get(selectedRow);
					textLabel.setBackgroundColor(Color.DKGRAY);
					tableRow.setBackgroundColor(Color.DKGRAY);
					textLabel.setTextColor(Color.WHITE);

				}
			});

			// Add the TableRow to the TableLayout
			tableLayout.addView(tableRow, new TableLayout.LayoutParams(
					TableRow.LayoutParams.FILL_PARENT,
					TableRow.LayoutParams.WRAP_CONTENT));  
			id++;
		}


	}

	public synchronized void hide() {
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
		try{
			isShowing = false;
			if(popupWindow != null && popupWindow.isShowing() ){
				popupWindow.dismiss();
				popupWindow = null;
			}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
		return;
	}



}
