package com.n2nconnect.android.stock.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class ChangePasswordActivity extends CustomWindow implements OnDoubleLoginListener, OnRefreshStockListener {

	private ProgressDialog dialog;
	private EditText currentPwdField;
	private EditText confirmPwdField;
	private int passwordType;
	private String currentPwd;
	private String confirmPwd;
	private DoubleLoginThread loginThread;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr = 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	//Fixes_Request-20161101, ReqNo.15
	private TextView txtChangePasswordRule;
	
	//Change_Request-20170119, ReqNo.11
	private TextView tvMessage;
	private String strPasswordMessage	= "";

	public void onCreate(Bundle savedInstanceState) {		
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.change_password);
	
			this.title.setText(getResources().getString(R.string.settings_change_passwd_title));
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(R.drawable.menuicon_acc_setting);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntAccSettingMenuIconBgImg() );
			Bundle bundle	= this.getIntent().getExtras();
			passwordType	= bundle.getInt(ParamConstants.PASSWORD_TYPE_TAG);
			//Change_Request-20170119, ReqNo.11
			if(bundle.containsKey(ParamConstants.STATUS_MSG_TAG))
				strPasswordMessage	= bundle.getString(ParamConstants.STATUS_MSG_TAG);
	
			loginThread 	= ((StockApplication) ChangePasswordActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(loginThread != null)
				loginThread.setRegisteredListener(ChangePasswordActivity.this);
			
			dialog 			= new ProgressDialog(ChangePasswordActivity.this);
			//Change_Request-20170119, ReqNo.11
			tvMessage		= (TextView)this.findViewById(R.id.tv_message);
			if(strPasswordMessage.length() > 0){
				tvMessage.setVisibility(View.VISIBLE);
				tvMessage.setText(strPasswordMessage);
			}
			
			txtChangePasswordRule	= (TextView) this.findViewById(R.id.tv_change_password_rule); 	//Fixes_Request-20161101, ReqNo.15
	
			if (passwordType == AppConstants.CHANGE_PIN_TYPE) {    
				this.title.setText(getResources().getString(R.string.settings_change_pin_title));
				TextView currentPwdLabel = (TextView) this.findViewById(R.id.currentPwdLabel);
				currentPwdLabel.setText(getResources().getString(R.string.current_pin));
				
				TextView newPwdLabel = (TextView) this.findViewById(R.id.newPwdLabel);
				newPwdLabel.setText(getResources().getString(R.string.new_pin));
				
				TextView confirmPwdLabel = (TextView) this.findViewById(R.id.confirmPwdLabel);
				confirmPwdLabel.setText(getResources().getString(R.string.confirm_pin));
				
				currentPwdField = (EditText) this.findViewById(R.id.currentPwdField);
				//currentPwdField.setInputType(InputType.TYPE_CLASS_NUMBER);
				currentPwdField.setTransformationMethod(PasswordTransformationMethod.getInstance());
				
				EditText newPwdField = (EditText) this.findViewById(R.id.newPwdField);
				newPwdField.setInputType(InputType.TYPE_CLASS_NUMBER);
				newPwdField.setTransformationMethod(PasswordTransformationMethod.getInstance());
				
				confirmPwdField = (EditText) this.findViewById(R.id.confirmPwdField);
				confirmPwdField.setInputType(InputType.TYPE_CLASS_NUMBER);
				confirmPwdField.setTransformationMethod(PasswordTransformationMethod.getInstance());
				
			// Added by Mary@20121224 - Fixes_Request-20121221, ReqNo.4 - START
				InputFilter[] FilterArray	= new InputFilter[1];
				FilterArray[0] 				= new InputFilter.LengthFilter(6);
				currentPwdField.setFilters(FilterArray);
				newPwdField.setFilters(FilterArray);
				confirmPwdField.setFilters(FilterArray);
				
				txtChangePasswordRule.setVisibility(View.GONE);
			}else{
				InputFilter[] FilterArray	= new InputFilter[1];
				FilterArray[0] 				= new InputFilter.LengthFilter(20);
				
				currentPwdField				= (EditText) this.findViewById(R.id.currentPwdField);
				currentPwdField.setFilters(FilterArray);
				
				EditText newPwdField 		= (EditText) this.findViewById(R.id.newPwdField);
				newPwdField.setFilters(FilterArray);
			
				confirmPwdField 			= (EditText) this.findViewById(R.id.confirmPwdField);
				confirmPwdField.setFilters(FilterArray);
				
				//Fixes_Request-20161101, ReqNo.15
				txtChangePasswordRule.setVisibility(View.VISIBLE);
				
			// Added by Mary@20121224 - Fixes_Request-20121221, ReqNo.4 - END
			}
	
			Button submitButton = (Button) this.findViewById(R.id.submitButton);
			submitButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view){
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					ChangePasswordActivity.this.changePasswordEvent(passwordType);
				}
			});
	
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(ChangePasswordActivity.this)
			//.setIcon(android.R.attr.alertDialogStyle)
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,	int id) {
					// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
					try{
						dialog.dismiss();
					// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
					}catch(Exception e){
						e.printStackTrace();
					}
					// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
				}
			})
			.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}
	
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
	@Override
	public void onResume(){		
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onResume();
			
			currContext	= ChangePasswordActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
		
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(ChangePasswordActivity.this);
				loginThread.resumeRequest();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onPause(){	
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{		
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END

	public void changePasswordEvent(int passwordType) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			EditText currentPwdField	= (EditText) this.findViewById(R.id.currentPwdField);
			EditText newPwdField 		= (EditText) this.findViewById(R.id.newPwdField);
			EditText confirmPwdField 	= (EditText) this.findViewById(R.id.confirmPwdField);
	
			if (currentPwdField.getText().toString().trim().length() == 0
					|| newPwdField.getText().toString().trim().length() == 0
					|| confirmPwdField.getText().toString().trim().length() == 0) {
				String message = (passwordType == AppConstants.CHANGE_PASSWORD_TYPE) ? 
						// Mary@20121224 - Fixes_request-20121221, ReqNo.7
						getResources().getString(R.string.password_blank_msg) : getResources().getString(R.string.pin_blank_msg);
	
				AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this);
				builder
				//.setIcon(android.R.attr.alertDialogStyle)
				.setTitle(passwordType == AppConstants.CHANGE_PASSWORD_TYPE? getResources().getString(R.string.invalid_password_title) : getResources().getString(R.string.invalid_pin_title))
				.setMessage(message)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
				AlertDialog alert = builder.create();
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ChangePasswordActivity.this.isFinishing() )
					alert.show();
				
				return;
			}
	
			if (!newPwdField.getText().toString().equals(confirmPwdField.getText().toString())) {
				String message = (passwordType == AppConstants.CHANGE_PASSWORD_TYPE) ? 
						// Mary@20121224 - Fixes_request-20121221, ReqNo.7
						getResources().getString(R.string.confirm_password_errormsg) : getResources().getString(R.string.confirm_pin_errormsg);
	
				AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this);
				builder
				//.setIcon(android.R.attr.alertDialogStyle)
				// Mary@20121224 - Fixes_request-20121221, ReqNo.7
				.setTitle(passwordType == AppConstants.CHANGE_PASSWORD_TYPE? getResources().getString(R.string.invalid_password_title) : getResources().getString(R.string.invalid_pin_title))
				.setMessage(message)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
				AlertDialog alert = builder.create();
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ChangePasswordActivity.this.isFinishing() )
					alert.show();
				
				return;
			}
	
			/* Mary@20130905 - Fixes_Request-20130711, ReqNo.18
			if (currentPwdField.getText().toString().equals(confirmPwdField.getText().toString())) {
				String message = (passwordType == AppConstants.CHANGE_PASSWORD_TYPE) ? 
						// Mary@20121224 - Fixes_request-20121221, ReqNo.7
						"New Password must not same as the Old Passoword." : "New PIN must not same as the Old PIN.";
	
				AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this);
				builder.setIcon(android.R.attr.alertDialogStyle)
				// Mary@20121224 - Fixes_request-20121221, ReqNo.7
				.setTitle(passwordType == AppConstants.CHANGE_PASSWORD_TYPE? "Invalid Password" : "Invalid PIN")
				.setMessage(message)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
				AlertDialog alert = builder.create();
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ChangePasswordActivity.this.isFinishing() )
					alert.show();
				
				return;
			}
			*/
			
			currentPwd = currentPwdField.getText().toString().trim();
			confirmPwd = confirmPwdField.getText().toString().trim();
	
			new ChangePassword().execute();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}
	

	private class ChangePassword extends AsyncTask<Void,Void,String>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				dialog.setMessage(getResources().getString(R.string.processing_request));
				
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ChangePasswordActivity.this.isFinishing() )
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try{
				Map<String, Object> parameters 		= new HashMap<String, Object>();
				parameters.put( ParamConstants.SENDERCODE_TAG, sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null) );
				parameters.put( ParamConstants.USERNAME_TAG, sharedPreferences.getString(PreferenceConstants.USER_NAME, null) );
				parameters.put(ParamConstants.OLD_PASSWORD_TAG, currentPwd);
				parameters.put(ParamConstants.NEW_PASSWORD_TAG, confirmPwd);
	
				/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
				if(AppConstants.EnableEncryption)
				*/
				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
					parameters.put( ParamConstants.USERPARAM_TAG, sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null) );
	
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				try {
					final String response = AtpConnectUtil.tradeChgPasswd(parameters, passwordType);
					return response;
				} catch (FailedAuthenicationException e) {
					e.printStackTrace();
					DefinitionConstants.stopAllThread();
				}
				*/
				try {
					String response = AtpConnectUtil.tradeChgPasswd(parameters, passwordType);
	
					while(intRetryCtr < AppConstants.intMaxRetry && response == null){
						intRetryCtr++;
						response = AtpConnectUtil.tradeChgPasswd(parameters, passwordType);
					}
					intRetryCtr	= 0;
	
					if(response == null)			
						throw new Exception(ErrorCodeConstants.FAIL_CHANGE_PASSWORD);
					
					return response;
				}catch(FailedAuthenicationException e){        	
					DefinitionConstants.stopAllThread();
				} catch (Exception e) {
					e.printStackTrace();
					exception	= e;
				}
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				return null;
			}catch(Exception e){
				e.printStackTrace();
				exception	= e;
				return null;
			}
		}

		@Override
		protected void onPostExecute(final String response){
			super.onPostExecute(response);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			try{
				if(dialog != null && dialog.isShowing() )
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
					dialog.dismiss();
	
				if(response!=null){
					AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this);
					builder.setMessage(response)			       
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							if (response.indexOf("1710") != -1 || response.indexOf("1803") != -1) {
								/* Mary@20130110 - Fixes_Request-20121221, ReqNo.6
								ChangePasswordActivity.this.finish();
								 */
								// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - START
								if( passwordType == AppConstants.CHANGE_PIN_TYPE){
									if( AppConstants.hasResetPin() )
										ChangePasswordActivity.this.setResult( RESULT_OK, new Intent() );
									
									// Added by Mary@20131002 - Change_Request-20130724, ReqNo.19
									( (StockApplication) ChangePasswordActivity.this.getApplication() ).setRememberPin(false);
									
									ChangePasswordActivity.this.finish();
								}else if( passwordType == AppConstants.CHANGE_PASSWORD_TYPE ){
									if( AppConstants.hasResetPin() ){
										Intent intent	= new Intent();
										intent.putExtra(ParamConstants.PASSWORD_TYPE_TAG, AppConstants.CHANGE_PIN_TYPE);
						    			intent.setClass(ChangePasswordActivity.this.getApplicationContext(), ChangePasswordActivity.class);
						    			ChangePasswordActivity.this.startActivityForResult(intent, AppConstants.CHANGE_PIN_TYPE);
									}else{
										ChangePasswordActivity.this.finish();
									}
								}
								// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - END
							}
						}
					});
					AlertDialog alert = builder.create();
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! ChangePasswordActivity.this.isFinishing() )
						alert.show();
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				}else if( response == null || exception != null){
					alertException.setMessage( exception.getMessage() );
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! ChangePasswordActivity.this.isFinishing() )
						alertException.show();
					exception	= null;
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - START
	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if(keyCode == KeyEvent.KEYCODE_BACK && ( AppConstants.hasResetPin() || AppConstants.hasResetPwd() ) ){
			finish();
			
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {	
		if(requestCode == AppConstants.CHANGE_PIN_TYPE)
			ChangePasswordActivity.this.finish();
	}
	// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - END

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if( ChangePasswordActivity.this.mainMenu.isShowing() )
					ChangePasswordActivity.this.mainMenu.hide();
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
				if(loginThread != null)
					loginThread.stopRequest();
				
				DefinitionConstants.stopAllThread();
				if( ! isFinishing() )
					ChangePasswordActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		// TODO Auto-generated method stub

	}
}