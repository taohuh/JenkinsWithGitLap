package com.n2nconnect.android.stock.model;

import java.util.Map;
import android.os.Parcel;
import android.os.Parcelable;

import com.n2nconnect.android.stock.DefinitionConstants;

public class DtiResult implements Parcelable {
	private String id;
	private String classType;
	private String eventTypeName;
	private Double expectedPercentageMove;

	//instrument
	private String instrumentExchange;
	private String instrumentCurrency;
	private int instrumentId;
	private String instrumentIsin;
	private String instrumentName;
	private String instrumentSymbolCommon;
	
	//pricing
	private String pricingHigh;
	private String pricingClose;
	private String pricingVolume;
	
	private char tradeType;	//L=Bearish, others=Bullish
	private char tradingHorizon; //S-Short-Term, I-Intermediate Term, L-Long-Term
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public String getEventTypeName() {
		return eventTypeName;
	}

	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}

	public Double getExpectedPercentageMove() {
		return expectedPercentageMove;
	}

	public void setExpectedPercentageMove(Double expectedPercentageMove) {
		this.expectedPercentageMove = expectedPercentageMove;
	}

	public String getInstrumentExchange() {
		return instrumentExchange;
	}

	public void setInstrumentExchange(String instrumentExchange) {
		this.instrumentExchange = instrumentExchange;
	}

	public String getInstrumentCurrency() {
		return instrumentCurrency;
	}

	public void setInstrumentCurrency(String instrumentCurrency) {
		this.instrumentCurrency = instrumentCurrency;
	}

	public int getInstrumentId() {
		return instrumentId;
	}

	public void setInstrumentId(int instrumentId) {
		this.instrumentId = instrumentId;
	}

	public String getInstrumentIsin() {
		return instrumentIsin;
	}

	public void setInstrumentIsin(String instrumentIsin) {
		this.instrumentIsin = instrumentIsin;
	}

	public String getInstrumentName() {
		return instrumentName;
	}

	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}

	public String getInstrumentSymbolCommon() {
		return instrumentSymbolCommon;
	}

	public void setInstrumentSymbolCommon(String instrumentSymbolCommon) {
		this.instrumentSymbolCommon = instrumentSymbolCommon;
	}

	public String getPricingHigh() {
		return pricingHigh;
	}

	public void setPricingHigh(String pricingHigh) {
		if(pricingHigh == null)
			this.pricingHigh = "0";
		else
			this.pricingHigh = pricingHigh;
	}

	public char getTradeType() {
		return tradeType;
	}

	public void setTradeType(char tradeType) {
		this.tradeType = tradeType;
	}
	
	public String getTradeTypeDescription() {
		if(this.tradeType == 'L'){
			return "Bullish Event";
		}else{
			return "Bearish Event";
		}
	}
	
	public String getTradeTypeShortDescr() {
		if(this.tradeType == 'L'){
			return "Bullish";
		}else{
			return "Bearish";
		}
	}
	
	//"DTI_COUNTRY_TAG", "DTI_BG_RESID_TAG", "MAPPED_EXCHG_TAG"
	public String getInfo(String hashMapIndex){
		Map<String, String> dtiExchangeMap	= DefinitionConstants.getDtiExchangeMap(this.getInstrumentExchange());
		
		return dtiExchangeMap.get(hashMapIndex);
	}
	
	public String getPricingClose() {
		return pricingClose;
	}

	public void setPricingClose(String pricingClose) {
		if(pricingClose == null)
			this.pricingClose = "0";
		else
			this.pricingClose = pricingClose;
	}
	
	public String getPricingVolume() {
		return pricingVolume;
	}

	public void setPricingVolume(String pricingVolume) {
		if(pricingVolume == null)
			this.pricingVolume = "0";
		else
			this.pricingVolume = pricingVolume;
	}
	
	public char getTradingHorizon() {
		return tradingHorizon;
	}

	public void setTradingHorizon(char tradingHorizon) {
		this.tradingHorizon = tradingHorizon;
	}
	
	public String getTradingHorizonDetails(){
		String trdHorizon	= "";
		if(this.tradingHorizon == 'S'){
			trdHorizon	= "a Short-Term";
		}else if(this.tradingHorizon == 'I'){
			trdHorizon	= "an Intermediate-Term";
		}else if(this.tradingHorizon == 'L'){
			trdHorizon	= "a Long-Term";
		}
		
		return trdHorizon;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int arg1) {
		dest.writeString(id);
		dest.writeString(classType);
		dest.writeString(eventTypeName);
		dest.writeDouble(expectedPercentageMove);
		dest.writeString(instrumentExchange);
		dest.writeString(instrumentCurrency);
		dest.writeInt(instrumentId);
		dest.writeString(instrumentIsin);
		dest.writeString(instrumentName);
		dest.writeString(instrumentSymbolCommon);
		dest.writeString(pricingHigh);
		dest.writeString(pricingClose);
		dest.writeString(pricingVolume);
		dest.writeString(Character.toString(tradeType));
		dest.writeString(Character.toString(tradingHorizon)); 
	}
	
	public DtiResult(Parcel in) {
		id	= in.readString();
		classType			= in.readString();
		eventTypeName		= in.readString();
		expectedPercentageMove	= in.readDouble();
		instrumentExchange	= in.readString();
		instrumentCurrency	= in.readString();
		instrumentId		= in.readInt();
		instrumentIsin		= in.readString();
		instrumentName		= in.readString();
		instrumentSymbolCommon	= in.readString();
		pricingHigh			= in.readString();
		pricingClose		= in.readString();
		pricingVolume		= in.readString();
		tradeType			= in.readString().charAt(0);
		tradingHorizon		= in.readString().charAt(0);

	}
	
	public DtiResult(){
		
	}
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public DtiResult createFromParcel(Parcel in) {
            return new DtiResult(in);
        }

        public DtiResult[] newArray(int size) {
            return new DtiResult[size];
        }
    };
}