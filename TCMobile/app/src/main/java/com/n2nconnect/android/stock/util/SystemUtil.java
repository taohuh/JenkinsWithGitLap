package com.n2nconnect.android.stock.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.WordUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager.BadTokenException;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CustomizedBase64;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.model.DtiResult;
import com.n2nconnect.android.stock.model.StockSymbol;

public class SystemUtil {

	private static HttpClient instance = null;

	public static HttpClient getHttpClient() {
		if (instance==null) {
			instance = new DefaultHttpClient();
		}	
		return instance;
	}

	public static boolean convertFlag(String flag) {
		if (flag.equals("1")) {
			return true;
		} else if (flag.equals("0")) {
			return false;
		} else {
			return false;
		}
	}

	public static void showExceptionAlert(Exception ex, Context context) {
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setMessage(ex.getMessage())
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
					return;
				}
			});
			AlertDialog alert = builder.create();

			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! ( (Activity)context ).isFinishing() )
				alert.show();
		}catch(BadTokenException bte){
			bte.printStackTrace();
		}
	}

	public static String[] getSymbolCodes(List<StockSymbol> stockSymbols) {
		// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
		if(stockSymbols == null)
			return new String[0];
		// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END

		String[] symbolCodes= new String[stockSymbols.size()];
		int index 			= 0;
		for (Iterator<StockSymbol> itr = stockSymbols.iterator(); itr.hasNext(); ) {
			StockSymbol symbol = itr.next();
			symbolCodes[index] = symbol.getSymbolCode();
			index++;
		}
		return symbolCodes;
	}

	public static void showFailedAuthenicationAlert(Activity activity) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			builder.setMessage("HTTP Error 401 Unauthorized.")
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();


			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! activity.isFinishing() )
				alert.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	public static String extractErrorCode(String message) {
		int pos = message.indexOf("ERROR,") + "ERROR,".length();
		int end = message.indexOf(" ", pos);

		return message.substring(pos, end);

	}

	public static String extractUserParam(byte[] bytes, String keyword, boolean isEncrypt) {
		String response = null;
		try {
			response = new String(bytes,AppConstants.STRING_ENCODING_FORMAT);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int pos = response.indexOf(keyword) + keyword.length();
		int end = response.indexOf("\r", pos);

		byte[] userBytes = new byte[end-pos];
		System.arraycopy(bytes, pos, userBytes, 0, end-pos);

		String userParam = null;

		userParam = SystemUtil.byteToHex(userBytes, true);

		return userParam;

	}

	public static String extractUserParam2(byte[] bytes, String keyword, boolean isEncrypt) {
		String response = null;
		try {
			response = new String(bytes,AppConstants.STRING_ENCODING_FORMAT);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int pos = response.indexOf(keyword) + keyword.length();
		int end = response.indexOf("\r", pos);

		byte[] userBytes = new byte[end-pos];
		System.arraycopy(bytes, pos, userBytes, 0, end-pos);

		/* Mary@20130620 - To be configurable according to broker
		String userParam = null;

		if(AppConstants.isJavaQC == false){
			userParam = SystemUtil.byteToHex(userBytes, true);
		else
			userParam = new String(userBytes);

		return userParam;
		 */
		return AppConstants.brokerConfigBean.isConnectJavaQC() ? new String(userBytes) : SystemUtil.byteToHex(userBytes, true);
	}

	public static String extractQCTimeOut(byte[] bytes, String keyword, boolean isEncrypt) {
		String response = null;
		try {
			response = new String(bytes,AppConstants.STRING_ENCODING_FORMAT);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int pos = response.indexOf(keyword) + keyword.length();
		int end = response.indexOf("\r", pos);

		byte[] userBytes = new byte[end-pos];
		System.arraycopy(bytes, pos, userBytes, 0, end-pos);

		String QCTimeOut = null;

		try {
			QCTimeOut = decompress(new String(userBytes, AppConstants.STRING_ENCODING_FORMAT));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//QCTimeOut = Integer.toString(hex2decimal(QCTimeOut));
		QCTimeOut = QCTimeOut.substring(0, QCTimeOut.length() - 1);

		return QCTimeOut.trim();

	}

	public static String extractQCTimeOut2(String response, String keyword, boolean isEncrypt) {

		int pos = response.indexOf(keyword) + keyword.length();
		int end = response.indexOf("\r", pos);

		byte[] userBytes = new byte[end-pos];
		try {
			System.arraycopy(response.getBytes(AppConstants.STRING_ENCODING_FORMAT), pos, userBytes, 0, end-pos);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String QCTimeOut = null;

		try {
			QCTimeOut = decompress(new String(userBytes, AppConstants.STRING_ENCODING_FORMAT));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		QCTimeOut = QCTimeOut.substring(0, QCTimeOut.length() - 1);

		return QCTimeOut.trim();

	}

	public static String decompress(String s)
	{
		boolean isNegative = false;
		int i;
		int iTmpByte;
		char cChar;
		String sResult = "";
		for(i=0 ; i<s.length(); i++){
			iTmpByte = ~((byte)s.charAt(i));
			cChar = (char)(((iTmpByte >> 4) & 0xf) + 48);

			if( !Character.isDigit(cChar) && (byte)cChar == 58 ){
				isNegative = true;
				cChar = ' ';
			}else if( !Character.isDigit(cChar) && !( (byte)cChar == 58 ) ){
				cChar = ' ';
				sResult += String.valueOf(cChar);                     
			}else              
				sResult += String.valueOf(cChar);


			cChar = (char)((iTmpByte & 0x0F) + 48);         
			if (!Character.isDigit(cChar)&& (byte)cChar == 58){
				isNegative = true;
				cChar = ' ';
			}
			else if(!Character.isDigit(cChar) && !((byte)cChar == 58)){
				cChar = ' ';
				sResult += String.valueOf(cChar);                     
			}
			else
				sResult += String.valueOf(cChar);


		} 
		if(isNegative) return ("-" + sResult);
		else return sResult;    
	}


	public static String extractUserParam3(String response, String keyword, boolean isEncrypt) {

		int pos = response.indexOf(keyword) + keyword.length();
		int end = response.indexOf("\r", pos);

		byte[] userBytes = new byte[end-pos];
		System.arraycopy(response.getBytes(), pos, userBytes, 0, end-pos);

		/* Mary@20130620 - To be configurable according to broker
		String userParam = null;

		if(AppConstants.isJavaQC == false){
			userParam = SystemUtil.byteToHex(userBytes, true);
		}else{
			userParam = new String(userBytes);
		}

		return userParam;
		 */
		return AppConstants.brokerConfigBean.isConnectJavaQC() ? new String(userBytes) : SystemUtil.byteToHex(userBytes, true);
	}

	public static byte[] extractUserParamInByte(byte[] bytes, String keyword, boolean isEncrypt) {
		String response = null;
		try {
			response = new String(bytes,AppConstants.STRING_ENCODING_FORMAT);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int pos = response.indexOf(keyword) + keyword.length();
		int end = response.indexOf("\r", pos);

		byte[] userBytes = new byte[end-pos];
		System.arraycopy(bytes, pos, userBytes, 0, end-pos);

		return userBytes;

	}

	public static String encryptATPString(String s) {
		int length 		= s.length();
		String result 	= "";
		// Added by Mary@20130605 - avoid IndexOutOfBoundException
		if(length > 0){
			int cl = length / 2 + length % 2;
			for(int i = 0; i < cl; i++) {
				int s1 = s.charAt(i);
				int s2;
				if(cl + i > length - 1)
					s2 = 0;
				else
					s2 = s.charAt(cl + i);
				int c1 = (s1 & 0xf0 | s2 & 0xf) ^ 0xf8;
				int c2 = (s2 & 0xf0 | s1 & 0xf) ^ 0xf8;
				result = (new StringBuilder(String.valueOf(result))).append((char)c1).toString();
				result = (new StringBuilder(String.valueOf(result))).append((char)c2).toString();
			}
		}
		return encodeUserParams(result);
	}

	public static String encodeUserParams(String param) {

		String sTmp = "";
		byte bytes[] = new byte[0];
		try {
			bytes = param.getBytes(AppConstants.STRING_ENCODING_FORMAT);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		byte abyte0[];
		int j = (abyte0 = bytes).length;
		for(int i = 0; i < j; i++) {
			byte b = abyte0[i];
			if(b > 64 && b < 91 || b > 96 && b < 123)
				sTmp = (new StringBuilder(java.lang.String.valueOf(sTmp))).append((char)b).toString();
			else
				sTmp = (new StringBuilder(java.lang.String.valueOf(sTmp))).append("%").append(java.lang.String.format("%1$02X", new java.lang.Object[] {
						java.lang.Byte.valueOf(b)}
						)).toString();
		}
		return sTmp;
	}

	public static String byteToHex (byte buf[], boolean forURL) {

		StringBuffer strbuf = new StringBuffer(buf.length * 2);
		int i;

		for (i = 0; i < buf.length; i++) {
			if (forURL) {
				strbuf.append("%");
			}

			if (((int) buf[i] & 0xff) < 0x10)
				strbuf.append("0");

			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}

		return strbuf.toString();
	}


	public static String generateAESKey(byte[] abtKey) throws Exception {

		try {
			int iLength = abtKey.length;
			int iStartPos = -1;
			int iNLFound = 0;

			for (int i = 0; i < iLength; i++) {
				if (abtKey[i] == 13) {
					iNLFound++;
				} else if (abtKey[i] == 10) {
					if (iNLFound != 0) {
						iNLFound++;
						if (iNLFound == 4) {
							iStartPos = i + 1;
							break;
						}
					}
				} else {
					iNLFound = 0;
				}
			}
			byte[] abtTemp;
			if (iStartPos != -1) {
				iLength = iLength - iStartPos;
				abtTemp = new byte[iLength];
				System.arraycopy(abtKey, iStartPos, abtTemp, 0, iLength);
			} else {
				abtTemp = new byte[iLength];
				System.arraycopy(abtKey, 0, abtTemp, 0, iLength);
			}
			String sOriginal = decompress(abtTemp);
			if (sOriginal != null) {
				sOriginal = sOriginal.substring(1, sOriginal.length() - 1);
				String sDateKey = sOriginal.substring(0, 8);
				String sKey = sDateKey + sDateKey;
				sOriginal += "00";
				return byteToHex(encrypt(sKey.getBytes(), sOriginal));
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new Exception("N2NUtil:generateAESKey:" + e.toString());
		}
	}

	public static String decompress(byte[] s) {
		boolean isNegative = false;
		int i;
		int iTmpByte;
		byte bCharByte;
		char cChar;
		String sResult = "";
		for (i = 0; i < s.length; i++) {
			iTmpByte = ~(s[i]);

			cChar = (char) (((iTmpByte >> 4) & 0xf) + 48);

			if (!Character.isDigit(cChar) && (byte) cChar == 58) {
				isNegative = true;
				cChar = ' ';
			} else if (!Character.isDigit(cChar)) {
				cChar = ' ';
				sResult += String.valueOf(cChar);
			} else {
				sResult += String.valueOf(cChar);
			}

			cChar = (char) ((iTmpByte & 0x0F) + 48);


			if (!Character.isDigit(cChar) && (byte) cChar == 58) {
				isNegative = true;
				cChar = ' ';
			} else if (!Character.isDigit(cChar)) {
				cChar = ' ';
				sResult += String.valueOf(cChar);
			} else {
				sResult += String.valueOf(cChar);
			}
		}

		if (isNegative) {
			return ("-" + sResult);
		} else {
			return sResult;
		}
	}

	private static String byteToHex(byte buf[]) {

		StringBuffer strbuf = new StringBuffer(buf.length * 2);
		int i;

		for (i = 0; i < buf.length; i++) {
			if (((int) buf[i] & 0xff) < 0x10) {
				strbuf.append("0");
			}
			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}
		return strbuf.toString();
	}	

	public static byte[] encrypt(byte[] key, String plainText) throws Exception {

		SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

		Cipher cipher = Cipher.getInstance("AES/ECB/NOPADDING");

		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

		byte[] encrypted = cipher.doFinal(plainText.getBytes());

		return encrypted;
	}

	// Added by Mary@20130620 - Change_Request-20130424, ReqNo.9 - START
	/**
	 * Byte is read from left to right, bit is read from right to left. access right is encoded as Hex
	 * @param strHex
	 * @param bitIdx
	 * @return
	 */
	public static boolean isBitOn_Hex(String strHex, int bitIdx){
		int vIdx	= bitIdx / 8; 			// 1 byte == 8 bits
		vIdx 		*= 2; 					// 2 character 1 byte;

		int vPos	= 1 << (bitIdx % 8);           

		int hexLen	= strHex.length();

		if(hexLen > vIdx){
			// high bit
			int byteData = Character.digit(strHex.charAt(vIdx), 16) << 4;

			if(hexLen+1>vIdx)
				// low bit
				byteData |= Character.digit(strHex.charAt(vIdx+1), 16);

			byteData		&= 0xFF; // trim left bit

			return (byteData & vPos) == vPos;
		}

		return false;
	}

	public static boolean isBitOn_Base64(String strBase64, int bitIdx) throws Exception{
		int vIdx		= (bitIdx / 8);
		int vPos 		= 1 << (bitIdx % 8);
		//byte[] data 	= org.apache.commons.codec.binary.Base64.decodeBase64(strBase64);
		byte[] data		= CustomizedBase64.decode(strBase64);

		if( data.length > vIdx){
			int b	= data[vIdx];
			b 		&= 0xFF;

			return (b & vPos) == vPos;
		}

		return false;
	}
	// Added by Mary@20130620 - Change_Request-20130424, ReqNo.9 - END

	// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - START
	public static String getDeviceModelName(){
		String manufacturer = Build.MANUFACTURER;
		String model 		= Build.MODEL;
		model				= model.startsWith(manufacturer) ? WordUtils.capitalize(model) :  WordUtils.capitalize(manufacturer) + " " + model;

		return model;
	}

	public static String getDeviceOSType(){
		return "ANDROID";
	}

	public static String getDeviceOSVersion(){
		return android.os.Build.VERSION.RELEASE;
	}

	public static String getDeviceConnectivity(Context context){
		ConnectivityManager connectivity= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		// NetworkInfo wifiInfo 			= connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		// NetworkInfo mobileInfo 			= connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		String strConnection			= connectivity.getActiveNetworkInfo().getTypeName();

		String strExtraInfo				= connectivity.getActiveNetworkInfo().getExtraInfo();
		if(strExtraInfo == null || (strExtraInfo != null && strExtraInfo.equalsIgnoreCase("null") ) )
			strExtraInfo	= "";

		// Added by Mary@20130819 - Changes_Request-20130724, ReqNo.4
		if( strExtraInfo.length() > 0 
				|| ( connectivity.getActiveNetworkInfo().getSubtypeName() != null && connectivity.getActiveNetworkInfo().getSubtypeName().trim().length() > 0 )
				)
			strConnection					+= "[" + strExtraInfo + " - "+ connectivity.getActiveNetworkInfo().getSubtypeName() + "]";

		return strConnection;
	}

	/*
  	public static String getSimDetails(Context context){
  		TelephonyManager telephonyManager	= (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
  		return "Device ID : " + telephonyManager.getDeviceId() + ", Sim Serial Number : " + telephonyManager.getSimSerialNumber()
  				+ ", Sim Operator Name : " + telephonyManager.getSimOperatorName();		
  	}
	 */

	public static String getSIMOperatorName(Context context){
		TelephonyManager telephonyManager	= (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getSimOperatorName();		
	}

	public static String getDeviceScreenSize(Activity activity){
		Display display = activity.getWindowManager().getDefaultDisplay();
		/* Mary@20130819 - Change_Request-20130724, ReqNo.4
  		return display.getWidth() + " x " + display.getHeight();
		 */
		return display.getWidth() + "x" + display.getHeight();
	}
	// Added by Mary@20130802 - Change_Request-20130724, ReqNo.4 - END

	//added by Melissa
	public static boolean isCall(String stockSymbol){
		String callChar = stockSymbol.substring(stockSymbol.indexOf(".")-1,stockSymbol.indexOf("."));
		if(callChar.equalsIgnoreCase("C")){
			return true;
		}else if(callChar.equalsIgnoreCase("P")){
			return false;
		}else{
			return false;
		}
	}

	//Added by Thinzar@20140709 - Change_Request-20170701 - Req #2,3,4 - START
	public static float convertDpToPixel(float dp, Context context){
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}
	//Added by Thinzar@20140709 - Change_Request-20170701 - Req #2,3,4 - END

	//Added by Thinzar@20140903 - Fixes_Request-20140820, ReqNo.13 - START
	public static boolean isDerivative(String exchangeCode){
		for(String code: AppConstants.getListDerivativesExchgCode()){
			if(exchangeCode.equalsIgnoreCase(code))	
				return true;
		}

		return false;
	}
	//Added by Thinzar@20140903 - Fixes_Request-20140820, ReqNo.13 - END

	//Change_Request-20160722, ReqNo.4 (to solve KL delayed feed not getting timeSales)
	public static String changeDelayFeedCodeToRealTime(String symbolCode){
		String[] items 	= symbolCode.split("\\.");
		symbolCode		= items[0].concat(".KL");

		return symbolCode;
	}

	//added by Thinzar, 20160906, for theScreener
	public static String getTrdExchgCode(String qcExchgCode){
		String trdExchgCode	= null;

		final List<String> validQCExchangeCodes 		= DefinitionConstants.getValidQCExchangeCodes();
		final List<String> validTradeExchangeCodes		= DefinitionConstants.getTrdExchangeCode();

		for(int i=0; i<validQCExchangeCodes.size();i++){
			if(validQCExchangeCodes.get(i).equalsIgnoreCase(qcExchgCode)){

				trdExchgCode	= validTradeExchangeCodes.get(i);
				break;
			}
		}

		return trdExchgCode;
	}

	public static String getQcExchangeCode(String trdExchgCode){
		String qcExchgCode	= null;

		final List<String> validQCExchangeCodes 		= DefinitionConstants.getValidQCExchangeCodes();
		final List<String> validTradeExchangeCodes		= DefinitionConstants.getTrdExchangeCode();

		for(int i=0; i<validTradeExchangeCodes.size();i++){

			if(validTradeExchangeCodes.get(i).equalsIgnoreCase(trdExchgCode)){
				if(i < validQCExchangeCodes.size()){
					qcExchgCode	= validQCExchangeCodes.get(i);
					break;
				}
			}
		}

		return qcExchgCode;
	}

	//Edited by Thinzar, WiFi tablets doesn't have telephonyManager
    public static String getDeviceIMEI(Context context){
        String identifier   = null;
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        
        if(telephonyManager != null)
            identifier      = telephonyManager.getDeviceId();
        
        if(identifier == null || identifier.length() == 0)
            identifier  = android.os.Build.SERIAL; //Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
            
        return identifier;
    }

	public static int getSdkVersion(){
		int version = Build.VERSION.SDK_INT;
		return version;
	}
	// Added by Diyana, Change_Request_20160624, ReqNo.2 -END

	//Fixes_Request-20161101, ReqNo.3 - START
	public static boolean isExchangeSupportModulusChart(String exchgCode){
		
		boolean isSupportModulusChart	= false;
		String mExchgCode	= (exchgCode.length() > 1 && exchgCode.endsWith("D"))? exchgCode.substring(0, exchgCode.length() - 1) : exchgCode;
		
		List<String> arrExchgSupportModulus	= AppConstants.brokerConfigBean.getArrExchgSupportModulus();
		
		if(arrExchgSupportModulus.size() == 0){
			isSupportModulusChart	= false;
			
		}else{
			if(arrExchgSupportModulus.contains(mExchgCode)){
				isSupportModulusChart	= true;
			}else{
				isSupportModulusChart	= false;
			}
		}
		
		return isSupportModulusChart;
	}
	//Fixes_Request-20161101, ReqNo.3 - END
	
	 /**
     * Get IP address from first non-localhost interface
     * @param ipv4  true=return ipv4, false=return ipv6
     * @return  address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4) 
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { 
        	ex.printStackTrace();
        } 
        
        return "";
    }
    
    //Change_Request-20170119, ReqNo.5
    public static ArrayList<DtiResult> parseDtiResult(String dtiResultString) throws Exception{
    	ArrayList<DtiResult> dtiResultArrayList	= new ArrayList<DtiResult>();
    	
    	try{
	    	JSONObject jsonObject = new JSONObject(dtiResultString);
	    	
			if(jsonObject.has(ParamConstants.DTI_PRI_TAG)){
				JSONObject priObject	= jsonObject.getJSONObject(ParamConstants.DTI_PRI_TAG);
				
				if(priObject.has(ParamConstants.DTI_STATUS_TAG)){
					String status	= priObject.getString(ParamConstants.DTI_STATUS_TAG);
					
					if(status.equalsIgnoreCase("ok")){
						if(priObject.has(ParamConstants.DTI_LIST_TAG) && priObject.get(ParamConstants.DTI_LIST_TAG) != JSONObject.NULL){
							JSONArray jsonListArray	= priObject.getJSONArray(ParamConstants.DTI_LIST_TAG);

							for(int i=0; i<jsonListArray.length();i++){

								JSONObject dtiObject	= jsonListArray.getJSONObject(i);
								DtiResult dti	= new DtiResult();
								dti.setId(dtiObject.getString(ParamConstants.DTI_ID_TAG));
								dti.setClassType(dtiObject.getString(ParamConstants.DTI_CLASS_TAG));
								dti.setEventTypeName(dtiObject.getString(ParamConstants.DTI_EVENT_TYPE_NAME_TAG));
								try{
									dti.setExpectedPercentageMove(dtiObject.getDouble(ParamConstants.DTI_EXP_PERCENT_MOVE_TAG));
								}catch(JSONException e){
									dti.setExpectedPercentageMove(0.0);
								}

								dti.setTradeType(dtiObject.getString(ParamConstants.DTI_TRADE_TYPE_TAG).charAt(0));
								dti.setTradingHorizon(dtiObject.getString(ParamConstants.DTI_TRADE_HORIZON).charAt(0));

								JSONObject instrumentObject	= dtiObject.getJSONObject(ParamConstants.DTI_INSTRUMENT_TAG);
								dti.setInstrumentExchange(instrumentObject.getString(ParamConstants.DTI_INSTRUMENT_EXCHG_TAG));
								dti.setInstrumentId(instrumentObject.getInt(ParamConstants.DTI_INSTRUMENT_ID_TAG));
								dti.setInstrumentCurrency(instrumentObject.getString(ParamConstants.DTI_INSTRUMENT_CURRENCY_TAG));
								dti.setInstrumentIsin(instrumentObject.getString(ParamConstants.DTI_INSTRUMENT_ISIN_TAG));
								dti.setInstrumentName(instrumentObject.getString(ParamConstants.DTI_INSTRUMENT_NAME_TAG));
								dti.setInstrumentSymbolCommon(instrumentObject.getString(ParamConstants.DTI_INSTRUMENT_SYM_SOMMON_TAG));

								JSONObject pricingObject	= dtiObject.getJSONObject(ParamConstants.DTI_PRICING_TAG);
								//set default value, sometimes DTI doesn't send back full JSON
								dti.setPricingClose("0");
								dti.setPricingHigh("0");
								dti.setPricingVolume("0");
								
								if(pricingObject.has(ParamConstants.DTI_PRICING_CLOSE_TAG)) 
									dti.setPricingClose(pricingObject.getString(ParamConstants.DTI_PRICING_CLOSE_TAG));
								
								if(pricingObject.has(ParamConstants.DTI_PRICING_HIGH_TAG))
									dti.setPricingHigh(pricingObject.getString(ParamConstants.DTI_PRICING_HIGH_TAG));
								
								if(pricingObject.has(ParamConstants.DTI_PRICING_VOL_TAG))
									dti.setPricingVolume(pricingObject.getString(ParamConstants.DTI_PRICING_VOL_TAG));

								dtiResultArrayList.add(dti);
							}
						}
					}else{
						throw new Exception("Error parsing data, status from server: " + status);
					}
				}
			}
    	}catch(JSONException ex){
    		ex.printStackTrace();
    		throw new Exception("Error parsing data. Please try again later.");
    	}
	    	
    	return reArrangeArrayList(dtiResultArrayList);
    }
    
    //CIMB want us to sort accordingly: MY, SG, HK, TH, ID & US
    private static ArrayList<DtiResult> reArrangeArrayList(final ArrayList<DtiResult> dtiResultArrayList){
    	ArrayList<DtiResult> sortedArrayList	= new ArrayList<DtiResult>();
    	
    	String[] exchangeOrderToSort	= {"MYX", "SGX-ST", "HKG", "SET", "JKT", "NYSE", "NASDAQ", "AMEX"};
    	
    	for(int i=0;i<exchangeOrderToSort.length;i++){
    		String currentExchange	= exchangeOrderToSort[i];
    		
    		for(int k=0;k<dtiResultArrayList.size();k++){
    			DtiResult d	= dtiResultArrayList.get(k);
    			if(d.getInstrumentExchange().equalsIgnoreCase(currentExchange))
    				sortedArrayList.add(d);
    		}
    	}
    	
    	return sortedArrayList;
    }
    
    public static String getItradeExchgCode(String dtiExchangeCode){
		String iTradeValidExchgCode	= null;
		
		Map<String, String> dtiMap	= DefinitionConstants.getDtiExchangeMap(dtiExchangeCode);
		String itradeEquivalentExchange	= dtiMap.get(ParamConstants.MAPPED_EXCHG_TAG);
		iTradeValidExchgCode	= SystemUtil.getQcExchangeCode(itradeEquivalentExchange);	//to support delay feed
		//System.out.println("debug==iTradeValidExchgCode:" + iTradeValidExchgCode);
		return iTradeValidExchgCode;
	}
    
    public static String getText(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                    connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null) 
            response.append(inputLine + "\r\n");

        in.close();

        return response.toString();
    }
	
	public static String getNDaysFromDate(String originalDateStr, int n){
		Date originalDate;
		Date finalDate;
		
		originalDate 	= FormatUtil.parseStringToDate(originalDateStr, FormatUtil.format_yyyyMMdd);
		Calendar cal	= Calendar.getInstance();
		cal.setTime(originalDate);
		cal.add(Calendar.DATE, n);
		finalDate = cal.getTime();

		return FormatUtil.parseDateToString(finalDate, FormatUtil.format_yyyyMMdd);
	}
	
	public static String getTodayDate(String format)
	{
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date today = new Date();

		return FormatUtil.parseDateToString(today, format);
	}
	
	public static String getAppVersionName(Context ctx) {
		try {
			PackageInfo packageInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
			
			return packageInfo.versionName;
		} catch (NameNotFoundException e) {
			return "";
		}
	}

	public static String getCurrentTimeStamp(){

		long timeInMs = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Date resultdate = new Date(timeInMs);

		return sdf.format(resultdate).toString();
	}

	public static String getSubscribedSgExchange(){
		final List<String> qcExchangeCodes 		= DefinitionConstants.getValidQCExchangeCodes();

		if(qcExchangeCodes.contains("SID"))
			return "SID";
		else if(qcExchangeCodes.contains("SI"))
			return "SI";
		else return null;
	}

	//Change_Request-20170601, ReqNo. 17
	public static String convertSymbolMapSubscribeExchg(String mSymbolCode){
		String symbolStockCode 	= mSymbolCode.substring(0, mSymbolCode.lastIndexOf("."));
		String symbolExchgCode	= mSymbolCode.substring((mSymbolCode.lastIndexOf(".") + 1), mSymbolCode.length());
		String mappedExchg 		= symbolExchgCode;	//in case unable to map, return original code

		final List<String> QCExchangeCodes 				= DefinitionConstants.getValidQCExchangeCodes();
		final List<String> tradeExchangeCodes			= DefinitionConstants.getTrdExchangeCode();
		final List<String> lstPortfolioDelayFeedExchg 	= AppConstants.brokerConfigBean.getLstPortfolioDelayFeedExchg();

		if(AppConstants.brokerConfigBean.isShowPortfolioDelayFeed() &&
				lstPortfolioDelayFeedExchg.contains(symbolExchgCode))
			mappedExchg		= symbolExchgCode + "D";
		else {
			//check exchange based on LMS subscription
			for (int i = 0; i < tradeExchangeCodes.size(); i++) {
				if(symbolExchgCode.equalsIgnoreCase(tradeExchangeCodes.get(i))) {
					mappedExchg = QCExchangeCodes.get(i);
					break;
				}
			}
		}

		return symbolStockCode + "." + mappedExchg;
	}
}