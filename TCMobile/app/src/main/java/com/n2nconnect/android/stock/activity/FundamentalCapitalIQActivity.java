package com.n2nconnect.android.stock.activity;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.util.FundamentalCapitalConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;


public class FundamentalCapitalIQActivity extends CustomWindow{
    private int intSelectedOption;
    private String strStockCode;
    private String strSourceCode;
    private String strExchCode;
    private String strCategory;
    private String strReportType;
    private String strReportPeriod;
    private String strBrokerCode;		//Added by Thinzar, Change_Request-20150401, ReqNo. 3
    private FundamentalCapitalIQTask fundamentalCapitalIQTask;
    private ProgressDialog dialog;
    private AlertDialog alertDialog;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {    	       
        // Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
        try{
        	super.onCreate(savedInstanceState);
        	
	        final Context context = this;
	        setContentView(R.layout.fundamental_capital_iq_webview);
	        	
	        Bundle extras			= this.getIntent().getExtras();
	        
	        //Added by Thinzar, Change_Request-20170119, ReqNo.3 - START
	        if(AppConstants.brokerConfigBean.getNewFundamentalSourceCode().length() > 0){
	        	if (extras != null) {
	        		this.title.setText(AppConstants.brokerConfigBean.getNewFundamentalNewsLabel());
	        		
	        		String symbolCode	= extras.getString(ParamConstants.STOCK_CODE_TAG);
	        		String brokerCode	= extras.getString(ParamConstants.BROKERCODE_TAG);
	        		String userName		= extras.getString(ParamConstants.USERNAME_TAG);
	        		String packageId	= extras.getString(ParamConstants.PACKAGE_ID_TAG);
	        		
	        		String fundamentalFullUrl	= FundamentalCapitalConnectUtil.getNewFundamentalReportUrl(symbolCode, brokerCode, userName, packageId);
	        		DefinitionConstants.Debug("Fundamental URL:" + fundamentalFullUrl);
	        		
	        		WebView webFundamental = (WebView) findViewById(R.id.wvFundamentalCapitalIQ);
	        		dialog	= new ProgressDialog(this);
	        		dialog.setMessage("Loading");
	        		
	        		webFundamental.getSettings().setJavaScriptEnabled(true);
	        		webFundamental.setWebViewClient(new WebViewClient() {    			 
						public boolean shouldOverrideUrlLoading(WebView view, String url) {              
							view.loadUrl(url);
							return true;
						}
	        		});
	        		
	        		webFundamental.setWebChromeClient(new WebChromeClient(){

		    			@Override
		    			public void onProgressChanged(WebView view, int newProgress) {
		    				if( ! FundamentalCapitalIQActivity.this.isFinishing() ){	
			    				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
			    				dialog.show();
		    				}
		    				
		    				if(newProgress == 100 && dialog.isShowing())
		    					dialog.dismiss();
		    			}
		    		});
	        		
	        		webFundamental.loadUrl(fundamentalFullUrl);
	        	}
	        }
	      //Added by Thinzar, Change_Request-20170119, ReqNo.3 - END
	        else{
				
				if (extras != null) {
					intSelectedOption 	= extras.getInt(ParamConstants.PARAM_FC_SELECTED_VIEW);
					strExchCode			= extras.getString(ParamConstants.EXCHANGECODE_TAG);
					
					strStockCode		= extras.getString(ParamConstants.SYMBOLCODE_TAG);
					if(strStockCode != null && strStockCode.indexOf(".") != -1){
					/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20 - START
						strStockCode	= ( strStockCode.split("\\.") )[0];
					*/
						strStockCode	= strStockCode.substring(0, strStockCode.lastIndexOf(".") );
					}
					
					strSourceCode		= extras.getString(ParamConstants.PARAM_FC_SOURCE_CODE);
					if(strSourceCode == null)
						strSourceCode	= AppConstants.strDefaultSourceCode;
					
					strReportType		= extras.getString(ParamConstants.PARAM_FC_REPORT_TYPE);
					if(strReportType == null)
						strReportType	= AppConstants.strDefaultReportType;
					
					strReportPeriod		= extras.getString(ParamConstants.PARAM_FC_REPORT_PERIOD);
					if(strReportPeriod == null){
						//strReportPeriod	= AppConstants.strDefaultReportPeriod;
						strReportPeriod		= AppConstants.brokerConfigBean.getFundamentalDefaultReportCode();	//Edited by Thinzar, Change_Request-20150401, ReqNo. 3
					}
					
					strCategory			= extras.getString(ParamConstants.PARAM_FC_CATEGORY);
				}
				
				dialog 					= new ProgressDialog(this);
				alertDialog 			= new AlertDialog.Builder(this).create();
				
				//Edited by Thinzar@20150522, Change_Request-20150401, ReqNo. 3
				//this.title.setText("S&P Capital IQ");
				this.title.setText(AppConstants.brokerConfigBean.getFundamentalNewsLabel());
				strBrokerCode	= sharedPreferences.getString(PreferenceConstants.BROKER_CODE, null);
				
				/* Mary@20120814 - Change_Request-20120719, ReqNo.4
				this.icon.setImageResource(LayoutSettings.menu_home_icn);
				*/
				this.icon.setImageResource( AppConstants.brokerConfigBean.getIntHomeMenuIconBgImg() );
						
				fundamentalCapitalIQTask= new FundamentalCapitalIQTask(FundamentalCapitalIQActivity.this, intSelectedOption);
				fundamentalCapitalIQTask.execute();
				// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
	        }
        }catch(Exception e){
        	e.printStackTrace();
        }
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
    }
    
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
    @Override
    public void onResume(){
    	// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
    	try{
    		super.onResume();
    		
			currContext	= FundamentalCapitalIQActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
    }
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
    
    public class FundamentalCapitalIQTask extends AsyncTask<String, Void, String>{
    	private Context context;
    	private int intSelectedOption;
    	private WebView webView;
    	private AsyncTask<String, Void, String> updateTask;
    	
    	public FundamentalCapitalIQTask(FundamentalCapitalIQActivity activity, int intSelectedOption){
    		this.context			= activity;
    		this.intSelectedOption	= intSelectedOption;
    	}
    	
    	@Override
    	protected void onPreExecute(){
    		super.onPreExecute();
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    		try{
	    		this.updateTask	= this;
				updateTask 		= this;
				/* Commented out by Thinzar, Change_Request-20150401, ReqNo. 4
				FundamentalCapitalIQActivity.this.dialog.setMessage("Loading...");
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! FundamentalCapitalIQActivity.this.isFinishing() )
					FundamentalCapitalIQActivity.this.dialog.show();
				 */
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
    	}
    	
    	@Override
		protected String doInBackground(String... arg0){
    		Map<String, String> map	= new HashMap<String, String>();
    		map.put(ParamConstants.EXCHANGECODE_TAG, strExchCode);
    		map.put(ParamConstants.SYMBOLCODE_TAG, strStockCode);
    		map.put(ParamConstants.PARAM_FC_SOURCE_CODE, strSourceCode);
    		map.put(PreferenceConstants.BROKER_CODE, strBrokerCode);//Added by Thinzar, Change_Request-20150401, ReqNo. 3
    		
    		String strUrl	= "";
    		switch(intSelectedOption){
	    		case ParamConstants.PARAM_INT_VIEW_FC_FUNDAMENTAL_INFO :
	    			map.put(ParamConstants.PARAM_FC_CATEGORY, strCategory);
	    			strUrl	= FundamentalCapitalConnectUtil.getFundamentalInfoUrl(map);
	    			break;
	    		case ParamConstants.PARAM_INT_VIEW_FC_FINANCIAL_REPORT :
	    			map.put(ParamConstants.PARAM_FC_REPORT_TYPE, strReportType);
	        		map.put(ParamConstants.PARAM_FC_REPORT_PERIOD, strReportPeriod);
	    			strUrl	= FundamentalCapitalConnectUtil.getFinancialReportUrl(map);
	    			break;
    		}
    			
			return strUrl;
		}
		
    	@Override
		protected void onPostExecute(final String strUrl){
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    		try{
	    		webView = (WebView) findViewById(R.id.wvFundamentalCapitalIQ);
	    		webView.getSettings().setJavaScriptEnabled(true);
	    		webView.setWebViewClient(new WebViewClient() {    			 
					public boolean shouldOverrideUrlLoading(WebView view, String url) {              
						view.loadUrl(url);
						return true;
					}
	
					public void onPageFinished(WebView view, String url) {
						if( FundamentalCapitalIQActivity.this.dialog != null && FundamentalCapitalIQActivity.this.dialog.isShowing() )
							FundamentalCapitalIQActivity.this.dialog.dismiss();
					}
	              
					public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
						Log.e("[FundamentalCapitalIQActivity]", "[FundamentalCapitalIQTask][onPostExecute][onReceivedError] Error : " + description);
						FundamentalCapitalIQActivity.this.alertDialog.setTitle(getResources().getString(R.string.dialog_title_error));
						FundamentalCapitalIQActivity.this.alertDialog.setMessage(description);
						FundamentalCapitalIQActivity.this.alertDialog.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								FundamentalCapitalIQActivity.this.finish();
							}
						});
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! FundamentalCapitalIQActivity.this.isFinishing() )
							FundamentalCapitalIQActivity.this.alertDialog.show();
					}
	    		});
	    		
	    		//Added by Thinzar, Change_Requst-20150401, ReqNo. 4 - START
	    		webView.setWebChromeClient(new WebChromeClient(){

	    			@Override
	    			public void onProgressChanged(WebView view, int newProgress) {
	    				if( ! FundamentalCapitalIQActivity.this.isFinishing() ){	
		    				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
		    				dialog.show();
	    				}
	    				
	    				if(newProgress == 100 && dialog.isShowing())
	    					dialog.dismiss();
	    			}
	    		});
	    		//Added by Thinzar, Change_Requst-20150401, ReqNo. 4 - END
	    		
	    		webView.loadUrl(strUrl);
	    		
	    		/*Commented out by Thinzar, Change_Requst-20150401, ReqNo. 4
	    		if( FundamentalCapitalIQActivity.this.dialog != null && FundamentalCapitalIQActivity.this.dialog.isShowing() )
	    			FundamentalCapitalIQActivity.this.dialog.dismiss();
	    		*/
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    		}catch(Exception e){
    			e.printStackTrace();
    		}
	    	// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
    	}
    }
}