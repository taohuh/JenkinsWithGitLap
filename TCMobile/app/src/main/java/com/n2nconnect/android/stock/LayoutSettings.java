package com.n2nconnect.android.stock;

import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class LayoutSettings {
	
	//Color
	/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
	public static final int colorGreen = Color.parseColor("#33CC33");
	public static final int colorRed = Color.parseColor("#CC3333");
	public static final int colorYellow = Color.parseColor("#FFD700");
	*/
	
	//Button
	public static final int btnRed = R.drawable.buttonchg_red70x28;
	public static final int btnGreen = R.drawable.buttonchg_grn70x28;
	
	//Indicator Arrow
	public static final int indicatorArrow = R.drawable.indicator_arrow;
	
	//Up and Down Arrow
	public static final int upArrow = R.drawable.arrow_up;
	public static final int downArrow = R.drawable.arrow_down;
	
	//Menu icon
	/* Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
	public static final int menu_home_icn = R.drawable.menuicon_home;
	public static final int menu_stock_icn = R.drawable.menuicon_stkinfo;
	public static final int menu_marketSummary_icn = R.drawable.menuicon_summary;
	public static final int menu_search_icn = R.drawable.menuicon_search;
	public static final int menu_ordbook_icn = R.drawable.menuicon_ord_book;
	public static final int menu_portfolio_icn = R.drawable.menuicon_portfolio;
	public static final int menu_trade_icn = R.drawable.menuicon_trade;
	public static final int menu_indices_icn = R.drawable.menuicon_indices;
	public static final int menu_news_icn = R.drawable.menuicon_news;
	public static final int menu_watchlist_icn = R.drawable.menuicon_watchlist;
	public static final int menu_orddetail_icn = R.drawable.menuicon_ord_book;
	*/
	
	// Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	
	//table row background
	//public static final int tablerow_light = R.drawable.tablerow_light;	//change_Request-20160101, ReqNo.3
	//public static final int tablerow_dark = R.drawable.tablerow_dark;		//change_Request-20160101, ReqNo.3
	public static final int tablerow_light = R.drawable.selector_table_row;
	public static final int tablerow_dark = R.drawable.selector_table_row;
	
	//public static final int tablerow_light_expand = R.drawable.tablerow320x88_light;	//change_Request-20160101, ReqNo.3
	//public static final int tablerow_dark_expand = R.drawable.tablerow320x88_dark;		//change_Request-20160101, ReqNo.3
	public static final int tablerow_light_expand = R.drawable.selector_table_row;
	public static final int tablerow_dark_expand = R.drawable.selector_table_row;
	
	//Loading animation
	public static final int loading_animate = R.drawable.loading;
	
	//Paging Image
	/* Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - START
	public static final int paging1 = R.drawable.page_control_1;
	public static final int paging2 = R.drawable.page_control_2;
	public static final int paging3 = R.drawable.page_control_3;
	public static final int paging4 = R.drawable.page_control_4;
	*/
	
	/* Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - START 
	public static final int paging1of4 = R.drawable.page_control_4_1;
	public static final int paging2of4 = R.drawable.page_control_4_2;
	public static final int paging3of4 = R.drawable.page_control_4_3;
	public static final int paging4of4 = R.drawable.page_control_4_4;
	*/
	/* Sonia@20131001 - Change_Request-20130225, ReqNo.5 - START
	public static final int paging1of5 = R.drawable.page_control_5_1;
	public static final int paging2of5 = R.drawable.page_control_5_2;
	public static final int paging3of5 = R.drawable.page_control_5_3;
	public static final int paging4of5 = R.drawable.page_control_5_4;
	public static final int paging5of5 = R.drawable.page_control_5_5;
	// Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - END
	*/
	/* Commented out for TCMobile 2.0 enhancements
	public static final int paging1of6 = R.drawable.page_control_6_1;
	public static final int paging2of6 = R.drawable.page_control_6_2;
	public static final int paging3of6 = R.drawable.page_control_6_3;
	public static final int paging4of6 = R.drawable.page_control_6_4;
	public static final int paging5of6 = R.drawable.page_control_6_5;
	public static final int paging6of6 = R.drawable.page_control_6_6;
	// Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - END
	
	public static final int paging1of7 = R.drawable.page_control_7_1;
	public static final int paging2of7 = R.drawable.page_control_7_2;
	public static final int paging3of7 = R.drawable.page_control_7_3;
	public static final int paging4of7 = R.drawable.page_control_7_4;
	public static final int paging5of7 = R.drawable.page_control_7_5;
	public static final int paging6of7 = R.drawable.page_control_7_6;
	public static final int paging7of7 = R.drawable.page_control_7_7;
	// Sonia@20131001 - Change_Request-20130225, ReqNo.5 - END
	*/
	//Selected Edge
	public static final int selectedEdge = R.drawable.rounded_edge;
}