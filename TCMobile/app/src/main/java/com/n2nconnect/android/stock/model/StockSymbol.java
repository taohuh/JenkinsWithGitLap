package com.n2nconnect.android.stock.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class StockSymbol implements Comparable<StockSymbol> {

	private String symbolCode;
	private String stockCode;
	private String companyName;
	private String currency;
	private float lastDone;
	private float previousClose;
	private float high;
	private float low;
	private long volume;
	private double value;
	private float open;
	private long totalTrade;
	private float lastBidPrice;
	private long lastBidSize;
	private float lastAskPrice;
	private long lastAskSize;
	private float lastBidPrice2;
	private long lastBidSize2;
	private float lastAskPrice2;
	private long lastAskSize2;
	private float lastBidPrice3;
	private long lastBidSize3;
	private float lastAskPrice3;
	private long lastAskSize3;
	private float LACP;
	private String instrument;
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
	private int intLotSize;
	// Added by Sonia@20130823 - Change_Request-20130225, ReqNo.5
	private int lastTradeNo;
	private String derivativeStockType;
	
	public static List<Float> MarketDepthBidPrice	= new ArrayList<Float>();
	public static List<Long> MarketDepthBidSize 	= new ArrayList<Long>();
	public static List<Float> MarketDepthAskPrice 	= new ArrayList<Float>();
	public static List<Long> MarketDepthAskSize 	= new ArrayList<Long>(); 
	
	private boolean isExpand;
	
	//Added by Thinzar, Fixes_Request-20160101, ReqNo.6
	private String sectorName;
	
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12 - START
	public int getIntLotSize() {
		return intLotSize;
	}

	public void setIntLotSize(int intLotSize) {
		/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
		this.intLotSize = intLotSize == 0 ? 1 : intLotSize;
		*/
		this.intLotSize = intLotSize;
	}
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12 - END
	
	public float getLastBidPrice() {
		return lastBidPrice;
	}

	public float getLastBidPrice2() {
		return lastBidPrice2;
	}
	
	public float getLastBidPrice3() {
		return lastBidPrice3;
	}
	
	public List<Long> getMarketDepthBidSize(){
		return MarketDepthBidSize;
	}
	
	public List<Float> getMarketDepthBidPrice(){
		return MarketDepthBidPrice;
	}
	
	public List<Long> getMarketDepthAskSize(){
		return MarketDepthAskSize;
	}
	
	public List<Float> getMarketDepthAskPrice(){
		return MarketDepthAskPrice;
	}

	public void setLastBidPrice(float lastBidPrice) {
		this.lastBidPrice = lastBidPrice;
	}
	
	public void setLastBidPrice2(float lastBidPrice) {
		this.lastBidPrice2 = lastBidPrice;
	}
	
	public void setLastBidPrice3(float lastBidPrice) {
		this.lastBidPrice3 = lastBidPrice;
	}
	
	public void setCurrency(String currency){
		this.currency = currency;
	}
	
	public void setLACP(float LACP){
		this.LACP = LACP;
	}
	
	public float getLACP(){
		return LACP;
	}

	public long getLastBidSize() {
		return lastBidSize;
	}
	
	public long getLastBidSize2() {
		return lastBidSize2;
	}
	
	public long getLastBidSize3() {
		return lastBidSize3;
	}
	
	public void setMarketDepthBidSize(long BidSize){
		MarketDepthBidSize.add(BidSize); 
	}
	
	public void setMarketDepthBidPrice(float BidPrice){
		MarketDepthBidPrice.add(BidPrice); 
	}
	public void setMarketDepthAskSize(long AskSize){
		MarketDepthAskSize.add(AskSize); 
	}
	
	public void setMarketDepthAskPrice(float AskPrice){
		MarketDepthAskPrice.add(AskPrice); 
	}

	public void setLastBidSize(long lastBidSize) {
		this.lastBidSize = lastBidSize;
	}
	
	public void setLastBidSize2(long lastBidSize) {
		this.lastBidSize2 = lastBidSize;
	}
	
	public void setLastBidSize3(long lastBidSize) {
		this.lastBidSize3 = lastBidSize;
	}

	public float getLastAskPrice() {
		return lastAskPrice;
	}
	
	public float getLastAskPrice2() {
		return lastAskPrice2;
	}
	
	public float getLastAskPrice3() {
		return lastAskPrice3;
	}
	
	public String getCurrency(){
		return currency;
	}

	public void setLastAskPrice(float lastAskPrice) {
		this.lastAskPrice = lastAskPrice;
	}
	
	public void setLastAskPrice2(float lastAskPrice) {
		this.lastAskPrice2 = lastAskPrice;
	}
	
	public void setLastAskPrice3(float lastAskPrice) {
		this.lastAskPrice3 = lastAskPrice;
	}

	public long getLastAskSize() {
		return lastAskSize;
	}
	
	public long getLastAskSize2() {
		return lastAskSize2;
	}
	
	public long getLastAskSize3() {
		return lastAskSize3;
	}

	public void setLastAskSize(long lastAskSize) {
		this.lastAskSize = lastAskSize;
	}
	
	public void setLastAskSize2(long lastAskSize) {
		this.lastAskSize2 = lastAskSize;
	}
	
	public void setLastAskSize3(long lastAskSize) {
		this.lastAskSize3 = lastAskSize;
	}

	public boolean checkDiscrepancy(StockSymbol compare) {
		if (this.getLastDone() != compare.getLastDone() 
				|| this.high != compare.getHigh()
				|| this.low != compare.getLow()
				|| this.totalTrade != compare.getTotalTrade()) {
			return true;
		}
	
		return false;
	}
	
	public float getPriceChange() {
		if(LACP!=0){
			return this.getLastDone() - LACP;
		}else{
			return 0;
		}
	}
	
	public float getChangePercent() {
		if (LACP!=0) {
			return (this.getLastDone() - LACP) / LACP;
		} else {
			return 0;
		}
	}
	
	/* Mary@20130718 - Change_Request-20130424, ReqNo.13 - START
	public String getStockName() {
		if(stockCode.indexOf(".") != -1){
	*/
	public String getStockName(boolean hasExchgCodeRemove) {
		if(hasExchgCodeRemove && stockCode.indexOf(".") != -1){
	// Mary@20130718 - Change_Request-20130424, ReqNo.13 - END
		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
			return stockCode.substring(0, stockCode.indexOf("."));
		*/
			return stockCode.substring(0, stockCode.lastIndexOf("."));
		} else {
			return stockCode;
		}
	}
	public String getSymbolCode() {
		return symbolCode;
	}
	public void setSymbolCode(String symbolCode) {
		this.symbolCode = symbolCode;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public float getLastDone() {
		if (lastDone != 0) {
			return lastDone;
		} else {
			return LACP;
		}
	}
	public void setLastDone(float lastDone) {
		this.lastDone = lastDone;
	}
	public float getPreviousClose() {
		return previousClose;
	}
	public void setPreviousClose(float previousClose) {
		this.previousClose = previousClose;
	}
	public float getHigh() {
		return high;
	}
	public void setHigh(float high) {
		this.high = high;
	}
	public float getLow() {
		return low;
	}
	public void setLow(float low) {
		this.low = low;
	}
	public long getVolume() {
		return volume;
	}
	public void setVolume(long volume) {
		this.volume = volume;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public float getOpen() {
		return open;
	}
	public void setOpen(float open) {
		this.open = open;
	}
	public boolean isExpand() {
		return isExpand;
	}
	public void setExpand(boolean isExpand) {
		this.isExpand = isExpand;
	}
	public long getTotalTrade() {
		return totalTrade;
	}
	public void setTotalTrade(long totalTrade) {
		this.totalTrade = totalTrade;
	}
	public void setInstrument(String instrument){
		this.instrument = instrument;
	}
	public String getInstrument(){
		return instrument;
	}
	// Added by Sonia@20130823 - ChangeRequest-20130225, ReqNo.5 - START
	public int getLastTrade_No(){
		return lastTradeNo;
	}
	public void setLastTrade_No(int lastTradeNo){
		this.lastTradeNo = lastTradeNo;
	}
	// Added by Sonia@20130823 - ChangeRequest-20130225, ReqNo.5 - END

	public String getDerivativeStockType() {
		return derivativeStockType;
	}

	public void setDerivativeStockType(String derivativeStockType) {
		this.derivativeStockType = derivativeStockType;
	}
	
	public String getSectorName() {
		return sectorName;
	}

	public void setSectorName(String sectorName) {
		this.sectorName = sectorName;
	}

	/*** Added by Thinzar@20150515, Change_Request-20150401, ReqNo.2 - START ***/
	@Override
	public int compareTo(StockSymbol s) {
		//set sort by symbolCode as default
		return this.symbolCode.compareTo(s.symbolCode);
	}
	
	//Comparator implementation to Sort Order object based on CompanyName
    public static class orderByCompanyName implements Comparator<StockSymbol> {

        @Override
        public int compare(StockSymbol s1, StockSymbol s2) {
            return s1.companyName.compareTo(s2.companyName);
        }
    }

	
	//Comparator implementation to Sort Order object based on Volume - Movers
    public static class orderByVolume implements Comparator<StockSymbol> {

        @Override
        public int compare(StockSymbol s1, StockSymbol s2) {
            return s2.volume > s1.volume ? 1 : (s2.volume < s1.volume ? -1 : 0);
        }
    }
    
    //Comparator implementation to Sort Order object based on priceChange - Gainers 
    public static class orderByPriceChange implements Comparator<StockSymbol> {

        @Override
        public int compare(StockSymbol s1, StockSymbol s2) {
            return s2.getPriceChange() > s1.getPriceChange() ? 1 : (s2.getPriceChange() < s1.getPriceChange() ? -1 : 0);
        }
    }
    
    //Comparator implementation to Sort Order object based on priceChange Reverse - Losers
    public static class orderByPriceChangeReverse implements Comparator<StockSymbol> {

        @Override
        public int compare(StockSymbol s1, StockSymbol s2) {
            return s1.getPriceChange() > s2.getPriceChange() ? 1 : (s1.getPriceChange() < s2.getPriceChange() ? -1 : 0);
        }
    }
	
    //Comparator implementation to Sort Order object based on priceChangePercent - Gainers Percent
    public static class orderByPriceChangePercent implements Comparator<StockSymbol> {

        @Override
        public int compare(StockSymbol s1, StockSymbol s2) {
            return s2.getChangePercent() > s1.getChangePercent() ? 1 : (s2.getChangePercent() < s1.getChangePercent() ? -1 : 0);
        }
    }
    
    //Comparator implementation to Sort Order object based on priceChangePercent - Losers Percent
    public static class orderByPriceChangePercentReverse implements Comparator<StockSymbol> {

        @Override
        public int compare(StockSymbol s1, StockSymbol s2) {
            return s1.getChangePercent() > s2.getChangePercent() ? 1 : (s1.getChangePercent() < s2.getChangePercent() ? -1 : 0);
        }
    }
	/*** Added by Thinzar@20150515, Change_Request-20150401, ReqNo.2 - END ***/
    
    // Added by Diyana for watchlist sorting - END
	public static final Comparator<StockSymbol> ASCENDING_ORDER = new Comparator<StockSymbol>() {
		public int compare(StockSymbol e1, StockSymbol e2) {
			return e1.getStockName(false).compareToIgnoreCase(e2.getStockName(false));
		}
	};

	public static final Comparator<StockSymbol> DESC_ORDER = new Comparator<StockSymbol>() {
		public int compare(StockSymbol e1, StockSymbol e2) {
			return e2.getStockName(false).compareToIgnoreCase(e1.getStockName(false));
		}
	};
	
	public static void saveCLicked(Context ctx, String value, String keyName) {
		SharedPreferences pref = ctx.getSharedPreferences("MyPref", ctx.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putString(keyName, value);
		editor.commit(); // commit changes
	}

	public static String getSaveCLicked(Context ctx, String keyName) {
		String value = "";
		SharedPreferences pref = ctx.getSharedPreferences("MyPref", ctx.MODE_PRIVATE);
		if (pref.contains(keyName)) {
			value = pref.getString(keyName, "");
		}

		return value;
	}

	// based on vol
	public static class LongComparator implements Comparator<StockSymbol> {
		public int compare(StockSymbol p1, StockSymbol p2) {
			long vol1 = p1.getVolume();
			long vol2 = p2.getVolume();

			if (vol1 == vol2)
				return 0;
			else if (vol1 > vol2)
				return 1;
			else
				return -1;
		}
	}
	// Added by Diyana for watchlist sorting - END		
}