package com.n2nconnect.android.stock.model;

import java.util.ArrayList;
import java.util.List;

//import com.sun.istack.internal.NotNull;
import android.support.annotation.NonNull;

/* Added by Mary@20120814 - Change_Request-20120719, ReqNo.4
 * 
 * TARGET FUNC	:
 * 	- provide a base method/configuration need to be implement for each broker 
 */

public interface BrokerConfigBean {

	/* Button Icon - Name - START */
	public String getStrBrokerHouseBtnName();
	public void setStrBrokerHouseBtnName(String strBrokerHouseBtnName);
	/* Button Icon - Name - END */
	
	/* Button Icon - Image - START */
	public int getIntBrokerHouseBtnImg();
	public void setIntBrokerHouseBtnImg(int intBrokerHouseBtnImg);
	/* Button Icon - Name - END */
	
	/* Login Screen - background image - START */
	public int getIntLoginBgImg();
	public void setIntLoginBgImg(int intLoginBgImg);
	/* Login Screen - background image - END */
	
	/* Login Screen - button background image - START */
	public int getIntLoginBtnBgImg();
	public void setIntLoginBtnBgImg(int intLoginBtnBgImg);
	/* Login Screen - button background image - END */
	
	/* Login Screen - Powered By Statement - START */
	public boolean showPoweredByStatement();
	public void isShowPoweredByStatement(boolean showPoweredByStatement);
	/* Login Screen - Powered By Statement - END */
	
	/* Login Screen - Terms And Conditions - START */
	public boolean hasSignInTermsNCondition();
	public void setHasSignInTermsNCondition(boolean hasSignInTermsNCondition);
	/* Login Screen - Terms And Conditions - END */
	
	/* Login Screen - Remember Me - START */
	public boolean showRememberMe();
	public void isShowRememberMe(boolean showRememberMe);
	/* Login Screen - Remember Me - END */
	
	/* Login - E2EE encryption - START */
	public boolean isEnableE2EEEncryption();
	public void setEnableE2EEEncryption(boolean isEnableE2EEEncryption);
	/* Login - E2EE encryption - END */
	
	/* Text color - START */
	public int getIntTextColor();
	public void setIntTextColor(int intTextColor);
	/* Text color - END */
	
	/* Link text color - START */
	public int getIntLinkTextColor();
	public void setIntLinkTextColor(int intLinkTextColor);
	/* Link text color - END */
	
	/* Top Navigation Bar - background image - START */
	public int getIntTopNavBarBgImg();
	public void setIntTopNavBarBgImg(int intTopNavBarBgImg);
	/* Top Navigation Bar - background image - END */
	
	/* Top Navigation Bar - button background image - START */
	public int getIntTopNavBarBtnBgImg();
	public void setIntTopNavBarBtnBgImg(int intTopNavBarBtnBgImg);
	/* Top Navigation Bar - button background image - END */
	
	/* Menu Icon - background image - START */
	public int getIntAccSettingMenuIconBgImg();
	public void setIntAccSettingMenuIconBgImg(int intAccSettingMenuIconBgImg);
	public int getIntExchgMenuIconBgImg();
	public void setIntExchgMenuIconBgImg(int intExchgMenuIconBgImg);
	public int getIntHomeMenuIconBgImg();
	public void setIntHomeMenuIconBgImg(int intHomeMenuIconBgImg);
	public int getIntIndiciesMenuIconBgImg();
	public void setIntIndiciesMenuIconBgImg(int intIndiciesMenuIconBgImg);
	public int getIntLogoutMenuIconBgImg();
	public void setIntLogoutMenuIconBgImg(int intLogoutMenuIconBgImg);
	public int getIntNewsMenuIconBgImg();
	public void setIntNewsMenuIconBgImg(int intNewsMenuIconBgImg);
	public int getIntOrdBookMenuIconBgImg();
	public void setIntOrdBookMenuIconBgImg(int intOrdBookMenuIconBgImg);
	public int getIntPortfolioMenuIconBgImg();
	public void setIntPortfolioMenuIconBgImg(int intPortfolioMenuIconBgImg);
	public int getIntSearchMenuIconBgImg();
	public void setIntSearchMenuIconBgImg(int intSearchMenuIconBgImg);
	public int getIntStkInfoMenuIconBgImg();
	public void setIntStkInfoMenuIconBgImg(int intStkInfoMenuIconBgImg);
	public int getIntMktSumMenuIconBgImg();
	public void setIntMktSumMenuIconBgImg(int intMktSumMenuIconBgImg);
	public int getIntTradeMenuIconBgImg();
	public void setIntTradeMenuIconBgImg(int intTradeMenuIconBgImg);
	public int getIntWatchlistMenuIconBgImg();
	public void setIntWatchlistMenuIconBgImg(int intWatchlistMenuIconBgImg);
	public int getIntAnnouncementMenuIconBgImg();
	public void setIntAnnouncementMenuIconBgImg(int intWatchlistMenuIconBgImg);
	/* Menu Icon - background image - END */
	
	/* Menu Icon Setting - START*/
	public String getStrAnnouncementURL();
	public void setStrAnnouncementURL(String strAnnouncementURL);
	/* Menu Icon Setting - END*/
	
	/* Page - background image - START */
	public int getIntBgImg();
	public void setIntBgImg(int intBgImg);
	/* Page - background image - END */
	
	/* Has Registration facility - START */
	public boolean hasRegistration();
	public void isHasRegistration(boolean hasRegistration);
	/* Has Registration facility - END */
	
	/* Disclaimer facility - START */
	public boolean showPortfolioDisclaimer();
	public void isShowPortfolioDisclaimer(boolean showPortfolioDisclaimer);
	
	public String getStrPortfolioDisclaimerUrl();
	public void setStrPortfolioDisclaimerUrl(String strPortfolioDisclaimerUrl);
	/* Disclaimer facility - END */
	
	/* Risk Disclosure Statement - START */
	public String getStrRiskDisclosureStatementUrl();
	public void setStrRiskDisclosureStatementUrl(String strRiskDisclosureStatementUrl);
	/* Risk Disclosure Statement - END */
	
	/* LMS Subscription Config - START */
	public String getStrLMSSponsor();
	public void setStrLMSSponsor(@NonNull String strLMSSponsor);
	public String getStrLMSProduct();
	public void setStrLMSProduct(@NonNull String strLMSProduct);
	public String getStrLMSServer();
	public void setStrLMSServer(@NonNull String strLMSserver);
	public String getStrLMSSubscriptionUrl();
	public void setStrLMSSubscriptionUrl(@NonNull String strLMSSubscriptionUrl);
	public String getStrLMSSubscriptionStatusUrl();
	public void setStrLMSSubscriptionStatusUrl(@NonNull String strLMSSubscriptionStatusUrl);
	/* LMS Subscription Config - END */
	
	/* ATP Config - START */
	public String getStrAtpIP();
	public void setStrAtpIP(@NonNull String strAtpIP);
	public int getIntAtpPort();
	public void setIntAtpPort(int intAtpPort);
	/* ATP Config - END */
	
	/* QC(Feed) Config - START */
	public boolean isGetNewKey();
	public void setIsGetNewKey(boolean isGetNewKey);
	/* QC(Feed) Config - END */
	
	/* News Config - START */
	public String getStrNewsIP();
	public void setStrNewsIP(@NonNull String strNewsIP);
	/* News Config - END */
	
	/* Chart Config - START */
	public String getStrChartIP();
	public void setStrChartIP(@NonNull String strChartIP);
	/* Chart Config - END */
	
	/* Application Name - START */
	public String getStrAppName();
	public void setStrAppName(@NonNull String strAppName);
	/* Application Name - END */
	
	/* Application Introduction - START */
	public String getStrAppIntro();
	public void setStrAppIntro(@NonNull String strAppIntro);
	/* Application Introduction - END */
	
	/* Application Conclusion - START */
	public String getStrAppConclude();
	public void setStrAppConclude(@NonNull String strAppConclude);
	/* Application Conclusion - END */
	
	/* Key Features Title - START */
	public String getStrKeyFeaturesTitle();
	public void setStrKeyFeaturesTitle(@NonNull String strKeyFeaturesTitle);
	/* Key Features Title - END */
	
	/* Key Features - START */
	public ArrayList<String> getALKeyFeatures();
	public void setALKeyFeatures(@NonNull ArrayList<String> alKeyFeatures);
	/* Key Features - END */
	
	/* Terms and Conditions - START */
	public ArrayList<String> getALTermsAndConditions();
	public void setALTermsAndConditions(@NonNull ArrayList<String> alTermsNConditions);
	/* Terms and Conditions - END */
	
	/* Terms of Service - START */
	public ArrayList<String> getALTermsOfService();
	public void setALTermsOfService(@NonNull ArrayList<String> alTermsOfService);
	/* Terms of Service - END */
	
	/* Privacy Policy - START */
	public ArrayList<String> getALPrivacyPolicy();
	public void setALPrivacyPolicy(@NonNull ArrayList<String> alPrivacyPolicy);
	/* Privacy Policy - END */
	
	/* Disclaimer of Warranty - START */
	public ArrayList<String> getALDisclaimerOfWarranty();
	public void setALDisclaimerOfWarranty(@NonNull ArrayList<String> alDisclaimerOfWarranty);
	/* Disclaimer of Warranty - END */
	
	/* Trade Preference - Trade Quantity Measurement - START */
	public int getIntTradeQtyMeasurement();
	public void setIntTradeQtyMeasurement(int intTradeQtyMeasurement);
	/* Trade Preference - Trade Quantity Measurement - END */
	
	/* Trade Preference - Auto Fill Quantity - START */
	public boolean autoFillTradeQuantity();
	public void isAutoFillTradeQuantity(boolean isAutoFillTradeQuantity);
	/* Trade Preference - Auto Fill Quantity - END */
	
	/* Trade Preference - Custom Quantity - START */
	public int getIntCustomTradeQuantity();
	public void setIntCustomTradeQuantity(int intCustomTradeQuantity);
	/* Trade Preference - Custom Quantity - END */
	
	/* Trade Preference - Auto Fill Quantity - START */
	public boolean autoFillTradePrice();
	public void isAutoFillTradePrice(boolean isAutoFillTradePrice);
	/* Trade Preference - Auto Fill Quantity - END */
	
	/* Restore Default Config - START */
	public BrokerConfigBean restoreConfigBean();
	/* Restore Default Config - END */
	
	/* Restore Default Trade Preference Config - START */
	public void resetTradePreferenceConfig();
	/* Restore Default Trade Preference Config - END */
	
	/* Base ATP Exchange Code - START */
	public String getStrBaseATPExchangeCode();
	public void setStrBaseATPExchangeCode(String strBaseATPExchangeCode);
	/* Base ATP Exchange Code - END */
	
	/* Base QC Exchange Code - START */
	public String getStrBaseQCExchangeCode();
	public void setStrBaseQCExchangeCode(String strBaseQCExchangeCode);
	/* Base QC Exchange Code - END */
	
	/* Order Pad - Default Payment Type - START */
	public String getStrDefaultPaymentType();
	public void setStrDefaultPaymentType(String strPaymentType);
	/* Order Pad - Default Payment Type - END */
	
	/* Order Pad - Default Currency Type - START */
	public String getStrDefaultCurrencyType();
	public void setStrDefaultCurrencyType(String strCurrencyType);
	/* Order Pad - Default Currency Type - END */
	
	/* Order Pad - Payment Type to be filter - START */
	public List<String> getLstPymtTypeToBeFilter();
	public void setLstPymtTypeToBeFilter(List<String> lstPymtTypeToBeFilter);
	/* Order Pad - Payment Type to be filter - END */
	
	/* Order Pad - Is show limit - START */
	public boolean isShowLimitAtOrderPad();
	public void setShowLimitAtOrderPad(boolean isShowLimitAtOrderPad);
	/* Order Pad - Is show limit - END */
	
	/* Portfolio - Is show limit - START */
	public boolean isShowLimitAtPortfolio();
	public void setShowLimitAtPortfolio(boolean isShowLimitAtPortfolio);
	/* Portfolio - Is show limit - END */
	
	/* Order Book - Is show limit - START */
	public boolean isShowLimitAtOrderBook();
	public void setShowLimitAtOrderBook(boolean isShowLimitAtOrderBook);
	/* Order Book - Is show limit - END */
	
	/* View Preference - View Quantity Measurement - START */
	public int getIntViewQtyMeasurement();
	public void setIntViewQtyMeasurement(int intTradeQtyMeasurement);
	/* View Preference - View Quantity Measurement - END */
	
	/* Request Preference - Enable Encryption - START */
	public boolean isEnableEncryption();
	public void setEnableEncryption(boolean isEnableEncryption);
	/* Request Preference - Enable Encryption - END */
	
	/* Setting - Enable Pin Features - START */
	public boolean isEnablePinFeatures();
	public void setEnablePinFeatures(boolean isEnablePinFeatures);
	/* Setting - Enable Pin Features - END */
	
	/* Setting - Enable Password Features - START */
	public boolean isEnablePasswordFeatures();
	public void setEnablePasswordFeatures(boolean isEnablePasswordFeatures);
	/* Setting - Enable Password Features - END */
	
	/* Setting - About Link - START */
	public String getStrAboutURL();
	public void setAboutURL(String strAboutURL);
	/* Setting - show About - END */
	
	/* Setting - Terms of Service Link - START */
	public String getStrTermsOfServiceURL();
	public void setStrTermsOfServiceURL(String strTermsOfServiceURL);
	/* Setting - Terms Of Service Link - END */
	
	/* Setting - show Privacy Policy - START */
	public String getStrPrivacyPolicyURL();
	public void setStrPrivacyPolicyURL(String strPrivacyPolicyURL);
	/* Setting - show Privacy Policy - END */
	
	/* Setting - show Disclaimer of Warranty - START */
	public String getStrDisclaimerOfWarrantyURL();
	public void setStrDisclaimerOfWarrantyURL(String getStrDisclaimerOfWarrantyURL);
	/* Setting - show Disclaimer of Warranty - END */
	
	/*Multilevel - Marketdepth -START */
	public boolean isMultilevelMarketDepth();
	public void setMultilevelMarketDepth(boolean isMultilevelMarketDepth);
	/*Multilevel - Marketdepth -END */
	
	/* Setting - Enable Trade Features - START */
	public boolean enableTrade();
	public void isEnableTrade(boolean isEnableTrade);
	/* Setting - Enable Trade Features - END */
	
	/* Setting - Connect to Java QC instead of Delphi QC - START */
	public boolean isConnectJavaQC();
	public void setConnectJavaQC(boolean isConnectJavaQC);
	/* Setting - Connect to Java QC instead of Delphi QC - END */
	
	/* Trade Setting - show Skip Confirmation Option - START */
	public boolean isShowSkipConfirmation();
	public void setShowSkipConfirmation(boolean isShowSkipConfirmation);
	/* Trade Setting - show Skip Confirmation Option - END */
	
	/* Trade Setting - show Short Sell Option - START */
	public boolean isShowShortSell();
	public void setShowShortSell(boolean isShowShortSell);
	/* Trade Setting - show Short Sell Option - END */
	
	/* Trade Setting - List of Exchange that show Short Sell Option - START */
	public List<String> lstExchgShowShortSell();
	public void setLstExchgShowSortSell(List<String> lstExchgShowShortSell);
	/* Trade Setting - List of Exchange that show Short Sell Option - END */
	
	/* Trade Setting - Impose Max Input Length On Trade Quantity - START*/
	public boolean isImposeMaxLenOnQty();
	public void setImposeMaxLenOnQty(boolean isImposeMaxLenOnQty);
	/* Trade Setting - Impose Max Input Length On Trade Quantity - END*/
	
	/* Watchlist Setting - Set Maximum No Of Watchlist Create - START */
	public int getIntMaxNoOfWatchlistCreate();
	public void setIntMaxNoOfWatchlistCreate(int intMaxNoOfWatchlistCreate);
	/* Watchlist Setting - Set Maximum No Of Watchlist Create - END */
	
	/* Watchlist Setting - Impose Max No of Watchlist Create - START */
	public boolean isImposeMaxWatchlistCreate();
	public void setImposeMaxWatchlistCreate(boolean isImposeMaxWatchlistCreate);
	/* Watchlist Setting - Impose Max No of Watchlist Create - END */
	
	/* Watchlist Setting - Set Maximum No Of Stock Add Into Watchlist - START */
	public int getIntMaxNoOfStockAddToWatchlist();
	public void setIntMaxNoOfStockAddToWatchlistd(int intMaxNoOfStockAddToWatchlist);
	/* Watchlist Setting - Set Maximum No Of Stock Add Into Watchlist - END */
	
	/* Watchlist Setting - Impose Max No of stock added into Watchlist - START */
	public boolean isImposeMaxStockAddToWatchlist();
	public void setImposeMaxStockAddToWatchlist(boolean isImposeMaxStockAddToWatchlist);
	/* Watchlist Setting - Impose Max No of stock added into Watchlist - END */
	
}
