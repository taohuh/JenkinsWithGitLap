package com.n2nconnect.android.stock.custom;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.iTradeCIMBSG_uat.stock.R;
/*
 * Added by Thinzar@20140731 - Change_Request-20140701, ReqNo. 9
 */
public class PaymentTypeFilterListView {
	private View windowView;
	private String selectedFilter;
	private List paymentFilterList;
	private int selectedRow;
	private boolean isShowing;
	private boolean hideOnSelect;
	private OnPayTypeFilterSelectedListener listener;
	private Context context;
	private LayoutInflater layoutInflater;
	private PopupWindow popupWindow;
	
	public void positionPymtTypeFilter(){
		ScrollView filterScroll = (ScrollView)windowView.findViewById(R.id.payTypeFilterScroll);
		
		String selected = selectedFilter;
		
		int index = 1;
		
		for(int i=0;i<paymentFilterList.size();i++){
			String compare = (String)paymentFilterList.get(i);
			if(compare.equals(selected)){
				break;
			}
			index++;
		}
		
		int scrollHeight = (index * 35) - 80;
		if(scrollHeight > 0){
			filterScroll.scrollTo(0, scrollHeight);
		}
		
		TableLayout tableLayout = (TableLayout)windowView.findViewById(R.id.payTypeFilterTable);
		TableRow tableRow = (TableRow)tableLayout.getChildAt(index -1);
		TextView textLabel = (TextView)tableRow.getChildAt(0);
		textLabel.setBackgroundColor(Color.DKGRAY);
		tableRow.setBackgroundColor(Color.DKGRAY);
		textLabel.setTextColor(Color.WHITE);
		selectedRow = index - 1;
	}
	
	public interface OnPayTypeFilterSelectedListener{
		public void payTypeFilterSelectedEvent(String payTypeFilter);
	}
	
	public boolean isShowing(){
		return isShowing;
	}
	
	public void setHideOnSelect(boolean doHideOnSelect){
		hideOnSelect = doHideOnSelect;
	}
	
	public synchronized void setPayTypeFilterList(List payTypeFilterList) throws Exception{
		if(isShowing){
			throw new Exception("Menu list may not be modified while menu is displayed.");
		}
		this.paymentFilterList = payTypeFilterList;
	}
	
	public PaymentTypeFilterListView(Context context, OnPayTypeFilterSelectedListener listener, LayoutInflater inflater){
		this.listener = listener;
		paymentFilterList = new ArrayList();
		this.context = context;
		this.layoutInflater = inflater;
	}
	
	public PaymentTypeFilterListView(OnClickListener onClickListener,
			OnClickListener onClickListener2, LayoutInflater layoutInflater2) {
		// TODO Auto-generated constructor stub
	}

	public synchronized void show(View view, String paymentType){
		
		this.selectedFilter = paymentType;
		isShowing = true;
		boolean isLandscape = false;
		int itemCount = paymentFilterList.size();
		if(itemCount<1){
			return;
		}
		if(popupWindow != null){
			return;
		}
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		if(display.getWidth() > display.getHeight()){
			isLandscape = true;
		}
		
		windowView = layoutInflater.inflate(R.layout.payment_type_filter_table_window, null);
		
		this.constructTableView();
		
		Button doneButton = (Button)windowView.findViewById(R.id.payTypeFilterDoneButton);
		doneButton.setOnClickListener(new OnClickListener(){
			public void onClick(View view){
				if(CustomWindow.idleThread != null && !CustomWindow.isShowingIdleAlert)
					CustomWindow.idleThread.resetExpiredTime();
				
				PaymentTypeFilterListView.this.hide();
				listener.payTypeFilterSelectedEvent(selectedFilter);
			}
		});
		
		popupWindow = new PopupWindow(windowView, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, false);
		popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
		popupWindow.setWidth(display.getWidth());
		popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
		
		windowView.post(new Runnable(){
			public void run(){
				PaymentTypeFilterListView.this.positionPymtTypeFilter();
			}
		});
	}
	
	private void constructTableView(){
		
		final TableLayout tableLayout = (TableLayout)windowView.findViewById(R.id.payTypeFilterTable);
		
		int id = 0;
		for(int i=0; i<paymentFilterList.size();i++){
			if(selectedFilter == null){
				if(i==0){
					selectedFilter = (String)paymentFilterList.get(0);
				}
			}
			
			String filter = (String)paymentFilterList.get(i);
			
			final TableRow tableRow = new TableRow(context);
			tableRow.setId(id);
			tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
			tableRow.setWeightSum(1);
			
			final TextView textLabel = (TextView)layoutInflater.inflate(R.layout.payment_type_list_item, null);
			textLabel.setText(filter);
			tableRow.addView(textLabel);
			
			tableRow.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View view) {
					
					final TableRow previousRow = (TableRow)tableLayout.getChildAt(selectedRow);
					TextView previousLabel = (TextView)previousRow.findViewById(R.id.payTypeFilterLabel);
					previousLabel.setBackgroundDrawable(null);
					previousRow.setBackgroundDrawable(null);
					previousLabel.setTextColor(Color.BLACK);
					
					selectedRow = tableRow.getId();
					selectedFilter = (String)paymentFilterList.get(selectedRow);
					textLabel.setBackgroundColor(Color.DKGRAY);
					tableRow.setBackgroundColor(Color.DKGRAY);
					textLabel.setTextColor(Color.WHITE);
				}
				
			});
			
			tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
			id++;
		}
		
	}
	
	public synchronized void hide(){
		try{
			isShowing = false;
			if(popupWindow != null && popupWindow.isShowing()){
				popupWindow.dismiss();
				popupWindow = null;
			} 
		} catch (Exception e){
			e.printStackTrace();
		}
		
		return;
	}
}
