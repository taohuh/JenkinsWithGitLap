package com.n2nconnect.android.stock.model;

import com.n2nconnect.android.stock.AppConstants;


public class CurrencyExchangeInfo {
	private String strFromStockCurrency;
	private String strToClientCurrency;
	private double dblBuyRate;
	private double dblSellRate;
	private long lngDenomination;
	
	public CurrencyExchangeInfo(){
		strFromStockCurrency	= AppConstants.getStrBaseCurrencyCode();
		strToClientCurrency		= AppConstants.getStrBaseCurrencyCode();
		dblBuyRate				= 1.00;
		dblSellRate				= 1.00;
		lngDenomination			= 1;
	}
	
	public String getStrFromStockCurrency() {
		return strFromStockCurrency;
	}
	public void setStrFromStockCurrency(String strFromStockCurrency) {
		this.strFromStockCurrency = strFromStockCurrency;
	}
	public String getStrToClientCurrency() {
		return strToClientCurrency;
	}
	public void setStrToClientCurrency(String strToClientCurrency) {
		this.strToClientCurrency = strToClientCurrency;
	}
	public double getDblBuyRate() {
		return dblBuyRate;
	}
	public void setDblBuyRate(double dblBuyRate) {
		this.dblBuyRate = dblBuyRate;
	}
	public double getDblSellRate() {
		return dblSellRate;
	}
	public void setDblSellRate(double dblSellRate) {
		this.dblSellRate = dblSellRate;
	}
	public long getLngDenomination() {
		return lngDenomination;
	}
	public void setLngDenomination(long lngDenomination) {
		this.lngDenomination = lngDenomination;
	}
	
	
}