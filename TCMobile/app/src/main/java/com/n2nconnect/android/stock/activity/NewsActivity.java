package com.n2nconnect.android.stock.activity;

import java.util.List;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class NewsActivity extends CustomWindow implements OnDoubleLoginListener{
	
	private WebView webView;
	private DoubleLoginThread loginThread;
	private ProgressDialog dialog;
	
    public void onCreate(Bundle savedInstanceState) {      
        // Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
        try{
        	super.onCreate(savedInstanceState);
        	
	        setContentView(R.layout.news_web_view);
	        
	        this.title.setText(getResources().getString(R.string.news_module_title));
	        /* Mary@20120814 - Change_Request-20120719, ReqNo.4
	        this.icon.setImageResource(R.drawable.menuicon_home);
	        */
	        this.icon.setImageResource( AppConstants.brokerConfigBean.getIntNewsMenuIconBgImg() );        
	        
	        Bundle bundle = this.getIntent().getExtras();
	        String newsId = bundle.getString(ParamConstants.NEWSID_TAG);
	        
			loginThread = ((StockApplication) NewsActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) NewsActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(NewsActivity.this);
			
			/* Mary@20130104 - Changes_Request-20130110, ReqNo.8
	        String url = "http://" + AppConstants.NEWS_SERVER_IP + "/ebcNews/index.jsp?&id=" + newsId + "EN";
	        */
			dialog	= new ProgressDialog(NewsActivity.this);
			String url = "";
			if(AppConstants.brokerConfigBean.isArchiveNews()==true){
				 url =  "http://" + AppConstants.brokerConfigBean.getStrNewsIP() + "/ebcNews/RN/index.jsp?id=" + newsId + "EN";
			}else{
				 url = "http://" + AppConstants.brokerConfigBean.getStrNewsIP() + "/ebcNews/index.jsp?&id=" + newsId + "EN";
			}
			
			System.out.println("debug News URL: " + url);
	        webView = (WebView) this.findViewById(R.id.newsWebView);
	        webView.getSettings().setJavaScriptEnabled(true);
	        
	        webView.setWebViewClient(new WebViewClient() {    			 
				public boolean shouldOverrideUrlLoading(WebView view, final String url) {  
					//System.out.println("shouldOverrideUrlLoading:" + url);
					
					//Edited by Thinzar, Change_Request-20160101, ReqNo.14 - START
					if (url.contains("pdf.reuters.com") || url.contains("play.google.com")) {
		                try {
		                	AlertDialog dialog = new AlertDialog.Builder(NewsActivity.this)
		                			.setMessage(getResources().getString(R.string.news_external_web_warning))
		                			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener(){

										@Override
										public void onClick(DialogInterface dialog, int which) {
											Uri uriUrl = Uri.parse(url);
						                    Intent intentUrl = new Intent(Intent.ACTION_VIEW, uriUrl);
						                    startActivity(intentUrl);
										}
		                			})
		                			.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which) {
											//do nothing
										}
									})
		                			.create();
		                	dialog.show();
		                	
		                    return true;
		                } catch (Exception e) {
		                    System.out.println(e);
		                    Toast.makeText(NewsActivity.this, getResources().getString(R.string.no_pdf_viewer_found), Toast.LENGTH_LONG).show();
		                }
		            } else{
		            	view.loadUrl(url);
		            }
					//Edited by Thinzar, Change_Request-20160101, ReqNo.14 - END
					
					return true;
				}
				
    		});
    		
    		//Added by Thinzar, Change_Requst-20150401, ReqNo. 4 - START
    		webView.setWebChromeClient(new WebChromeClient(){

    			@Override
    			public void onProgressChanged(WebView view, int newProgress) {
    				if( ! NewsActivity.this.isFinishing() ){	
	    				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
	    				dialog.show();
    				}
    				
    				if(newProgress == 100 && dialog.isShowing())
    					dialog.dismiss();
    			}
    		});
	        webView.loadUrl(url);
	    // Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START  
        }catch(Exception e){
        	e.printStackTrace();
        }
        // Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
    }
    
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
    @Override
    public void onResume(){
    	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
    	try{
    		super.onResume();
    		
			currContext	= NewsActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(NewsActivity.this);
				loginThread.resumeRequest();
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
    }
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
    
    // Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
    @Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
    // Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
	    	// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if(this.mainMenu.isShowing())
	    		this.mainMenu.hide();
			
	    	webView.goBack();
	    	finish();
	    	
	        return true;
	    }else if(keyCode==KeyEvent.KEYCODE_BACK){
	    	// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onDestroy() {
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
		try{
			super.onDestroy();
			
			if(this.mainMenu.isShowing())
				this.mainMenu.hide();
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.refreshThread != null)
					LoginActivity.refreshThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				if(NewsActivity.this.mainMenu.isShowing()){
					NewsActivity.this.mainMenu.hide();
				}
				
				if(!isFinishing()){
					NewsActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};
}
