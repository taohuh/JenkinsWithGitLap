package com.n2nconnect.android.stock.model;

public class E2EEInfo {
	private String strE2EEPublicKey;
	private String strE2EESessionID;
	private String strE2EERandomNumber;
	private String strRSAModulus_Base64;
	private String strRSAExponent_Base64;
	private String strRSAPublicKey;
	
	
	public String getStrE2EEPublicKey() {
		return strE2EEPublicKey;
	}
	public void setStrE2EEPublicKey(String strPublicKey) {
		this.strE2EEPublicKey = strPublicKey;
	}
	
	public String getStrE2EESessionID() {
		return strE2EESessionID;
	}
	public void setStrE2EESessionID(String strSessionID) {
		this.strE2EESessionID = strSessionID;
	}
	
	public String getStrE2EERandomNumber() {
		return strE2EERandomNumber;
	}
	public void setStrE2EERandomNumber(String strRandomNumber) {
		this.strE2EERandomNumber = strRandomNumber;
	}
	
	public String getStrRSAModulus_Base64() {
		return strRSAModulus_Base64;
	}
	public void setStrRSAModulus_Base64(String strRSAModulus_Base64) {
		this.strRSAModulus_Base64 = strRSAModulus_Base64;
	}
	
	public String getStrRSAExponent_Base64() {
		return strRSAExponent_Base64;
	}
	public void setStrRSAExponent_Base64(String strRSAExponent_Base64) {
		this.strRSAExponent_Base64 = strRSAExponent_Base64;
	}
	
	public String getStrRSAPublicKey() {
		return strRSAPublicKey;
	}
	public void setStrRSAPublicKey(String strRSAPublicKey) {
		this.strRSAPublicKey = strRSAPublicKey;
	}
	
}