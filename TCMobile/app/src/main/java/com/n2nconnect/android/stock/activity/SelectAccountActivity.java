package com.n2nconnect.android.stock.activity;

import java.util.Iterator;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.acclistAdapter;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class SelectAccountActivity extends CustomWindow implements OnDoubleLoginListener, OnRefreshStockListener {

	private List<TradeAccount> tradeAccounts;
	/* Mary@20130605 - comment unused variable
	private TableRow selectedRow;
	private boolean toPersist;
	private ImageButton search;
	*/
	private String selectedAccount;
	private ListView lview;
	private DoubleLoginThread loginThread;
	private acclistAdapter adapter;
	private EditText filterText;
	private Button showAll;
	private TextView noresultFound;
	private Button filterOpt;
	private int Selection = 0;

	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.select_account_view2);
			this.title.setText(getResources().getString(R.string.select_acc_module_title));
			this.icon.setImageResource(R.drawable.menuicon_acc_setting);
			
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(R.drawable.menuicon_watchlist);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntWatchlistMenuIconBgImg() );
			
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.2
			showAll 					= (Button)findViewById(R.id.showAllButton);
			
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
			/* Commented for TC
			if(! AppConstants.hasMultiLogin ){
				( (RelativeLayout)findViewById(R.id.rl_sa_TopNavBar) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBgImg() );
				showAll.setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );
			}
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
			 */
			StockApplication application= (StockApplication) this.getApplication();
			
			AppConstants.showMenu 		= true;
			
			/* Mary@20130702 - GooglePlayReport-20130702, CrashNo.2
			showAll 					= (Button)findViewById(R.id.showAllButton);
			*/
			showAll.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					filterText.setText("");
					filterText.clearFocus();
					lview.setSelectionAfterHeaderView();
				}
				
			});
			
			lview 						= (ListView)findViewById(R.id.accountList);
			filterText 					= (EditText)findViewById(R.id.searchField);
			//filterText.setHint("Search by");
			
			/* Mary@20130605 - comment unused variable
			search 						= (ImageButton)findViewById(R.id.searchButton);
			*/
			noresultFound 				= (TextView)findViewById(R.id.noResultsFoundView);
			
			filterOpt 					= (Button)findViewById(R.id.filterOptButton);
			filterOpt.setText(getResources().getString(R.string.select_acc_filterby_name));
			filterOpt.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					try{
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						final String[] items 		= {
								getResources().getString(R.string.select_acc_filterby_name), 
								getResources().getString(R.string.select_acc_filterby_acc_num)
								};
		    			AlertDialog.Builder builder = new AlertDialog.Builder(SelectAccountActivity.this);
		    			builder.setTitle(getResources().getString(R.string.select_acc_filter_title));
		    			builder.setSingleChoiceItems(items, Selection, new DialogInterface.OnClickListener() {
			    			@Override
		    			    public void onClick(DialogInterface dialog, int item) {
			    				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			    				if(idleThread != null && ! isShowingIdleAlert)
			    					idleThread.resetExpiredTime();
			    				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			    				
		    			    	filterOpt.setText(items[item]);
		    		        	Selection = item;
		    		        	adapter.setFilterOption(Selection);
		    		        	// Added by Mary@20121025 - Fixes_Request_20121023, ReqNo.12
		    		        	adapter.getFilter().filter( filterText.getText().subSequence( 0, filterText.length() ) );
		    		        	dialog.dismiss();
		    			    }
		    			});
		    			AlertDialog alert 			= builder.create();
		    			
		    			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! SelectAccountActivity.this.isFinishing() )
							alert.show();
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
					}catch(Exception e){
						e.printStackTrace();
					}
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
				}
			});
			loginThread 				= ((StockApplication) SelectAccountActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) SelectAccountActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(this);
	
			Bundle extras 				= this.getIntent().getExtras();
			selectedAccount 			= extras.getString(ParamConstants.ACCOUNTNO_TAG);
			boolean filterByExchg 		= extras.getBoolean(ParamConstants.FILTER_BY_EXCHG);
			// Added by Mary@20120717 - get input parameter exchange code - START
			String strTrdExchangeCode	= extras.getString(ParamConstants.EXCHANGECODE_TAG);
			
			if(strTrdExchangeCode == null && filterByExchg ){
				for(Iterator<ExchangeInfo> itr = application.getExchangeInfos().iterator(); itr.hasNext(); ){
					ExchangeInfo exchgInfo	= itr.next();
					if( AppConstants.DEFAULT_EXCHANGE_CODE.equals( exchgInfo.getQCExchangeCode() ) ){
						strTrdExchangeCode				= exchgInfo.getTrdExchgCode();
						break;
					}
				}
			}
			if(strTrdExchangeCode == null)
				strTrdExchangeCode	= AppConstants.DEFAULT_EXCHANGE_CODE;
			// Added by Mary@20120717 - get input parameter exchange code - END
	
			if (selectedAccount == null)
				selectedAccount = application.getSelectedAccount().getAccountNo();
			
			/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			if(filterByExchg==true){
				// Mary@20120717 - get input parameter exchange code
				// tradeAccounts = application.getTradeAccounts(AppConstants.DEFAULT_EXCHANGE_CODE);
				tradeAccounts = application.getTradeAccounts(strExchangeCode);
			}else{
				tradeAccounts = application.getTradeAccounts();
			}
			*/
			/* Mary@20130220 - Fixes_Request-20130108, ReqNo.10
			tradeAccounts = filterByExchg ? application.getTradeAccounts(strTrdExchangeCode) : application.getTradeAccounts();
			*/
			tradeAccounts				= application.getTradeAccounts();
			/* Mary@20130605 - comment unused variable
			toPersist 					= false;
			*/
			
			if(tradeAccounts != null)
				this.constructTableView();
			
			filterText.addTextChangedListener(filterTextWatcher);
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
	@Override
	public void onResume(){
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onResume();
			
			currContext	= SelectAccountActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(SelectAccountActivity.this);
				loginThread.resumeRequest();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	
	private TextWatcher filterTextWatcher = new TextWatcher() {

	    public void afterTextChanged(Editable s) {
	    }

	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	    }

	    public void onTextChanged(CharSequence s, int start, int before, int count) {
	        adapter.getFilter().filter(s);
	    }

	};

	private void constructTableView() {
		adapter = new acclistAdapter(SelectAccountActivity.this, tradeAccounts,noresultFound, Selection);
		lview.setAdapter(adapter);
		
		lview.setClickable(true);
		lview.setOnItemClickListener(new AdapterView.OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView parentView, View childView, int position, long id) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				childView.setBackgroundResource(R.drawable.selected_tablerow);

				Intent data 		= new Intent();

				TradeAccount account= (TradeAccount) tradeAccounts.get(position);
				data.putExtra(ParamConstants.ACCOUNTNO_TAG, account.getAccountNo());
				data.putExtra(ParamConstants.BRANCHCODE_TAG, account.getBranchCode());

				if(DefinitionConstants.tradeOrders!=null)
					DefinitionConstants.tradeOrders.clear();

				SelectAccountActivity.this.setResult(RESULT_OK, data); 
				SelectAccountActivity.this.finish();
			}
			
		});
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(SelectAccountActivity.this.mainMenu.isShowing()){
					SelectAccountActivity.this.mainMenu.hide();
				}
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				
				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					SelectAccountActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		// TODO Auto-generated method stub
		
	}
}
