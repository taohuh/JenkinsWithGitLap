package com.n2nconnect.android.stock.activity;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;


public class RiskDisclosureStatementActivity extends CustomWindow{
    private RiskDiclosureStatementTask riskDiclosureStatementTask;
    private ProgressDialog dialog;
    private AlertDialog alertDialog;
    
    private String strATPUserParam;
    private byte[] userParamInByte;
    private String strSenderCode;
    private String strAccountNo;
    private String strBrokerCode;
    private String strBranchCode;
    private String strExchgCode;
    private String strRDSVersionNo;
    private int intRetryCtr	= 3;
    // Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - START
	private String strRDSUrl;
	private boolean isBlockByRDS;
	// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - END
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState){  	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.risk_disclosure_statement_webview);
        
		dialog 					= new ProgressDialog(this);
		
		/* Mary@20130329 - Change_Request-20130104, ReqNo.3
		this.title.setText("Risk Disclosure Statement");
		*/
		this.icon.setImageResource(R.drawable.disclaimer);
		
		if(this.getIntent() != null){
			Bundle bundle 		= this.getIntent().getExtras();
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				userParamInByte = bundle.getByteArray(ParamConstants.USERPARAM_INBYTE_TAG);
			else
				strATPUserParam	= bundle.getString(ParamConstants.USERPARAM_TAG);
			
			strSenderCode	= bundle.getString(ParamConstants.SENDERCODE_TAG);
			strAccountNo	= bundle.getString(ParamConstants.ACCOUNTNO_TAG);
			strBrokerCode	= bundle.getString(ParamConstants.BROKERCODE_TAG);
			strBranchCode	= bundle.getString(ParamConstants.BRANCHCODE_TAG);
			strExchgCode	= bundle.getString(ParamConstants.EXCHANGECODE_TAG);
			strRDSVersionNo	= bundle.getString(ParamConstants.RDS_VERSIONNO_TAG);
			// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - START
			strRDSUrl		= bundle.getString(ParamConstants.RDS_URL_TAG);
			isBlockByRDS	= bundle.getBoolean(ParamConstants.IS_BLOCK_BY_RDS_TAG, false);
			// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - END
		}
		
		// Added by Mary@20130329 - Change_Request-20130104, ReqNo.3
		this.title.setText( isBlockByRDS ? getResources().getString(R.string.rds_disclaimer_module_title) : getResources().getString(R.string.rds_agreement_module_title) );
				
		riskDiclosureStatementTask= new RiskDiclosureStatementTask(RiskDisclosureStatementActivity.this);
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
		if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
			riskDiclosureStatementTask.execute();
		
		// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - START
		LinearLayout llRDSAgreement		= (LinearLayout)findViewById(R.id.llRDSButtonForAgreement);
		LinearLayout llRDSInformation	= (LinearLayout)findViewById(R.id.llRDSButtonForInformation);
		if(isBlockByRDS){
			llRDSInformation.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, 0, 0.1f) );
			llRDSAgreement.setLayoutParams( new LinearLayout.LayoutParams(0, 0, 0f) );
			
			Button btnAwareRDS	= (Button)findViewById(R.id.btnAwareRDS);
			btnAwareRDS.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					RiskDisclosureStatementActivity.this.setResult(RESULT_CANCELED, new Intent()); 	
					RiskDisclosureStatementActivity.this.finish();
				}		
			});
		}else{
			llRDSInformation.setLayoutParams( new LinearLayout.LayoutParams(0, 0, 0f) );
			llRDSAgreement.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, 0, 0.1f) );
		// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - END
			
			Button btnAgreeRDS	= (Button)findViewById(R.id.btnAgreeRDS);
			btnAgreeRDS.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					new updateRDSStatus().execute(true);				
				}		
			});
			Button btnDisagreeRDS	= (Button)findViewById(R.id.btnDisagreeRDS);
			btnDisagreeRDS.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					RiskDisclosureStatementActivity.this.setResult(RESULT_CANCELED, new Intent()); 	
					RiskDisclosureStatementActivity.this.finish();
				}		
			});
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(RiskDisclosureStatementActivity.this);
		alertDialog	= builder.create();
    }
    
    // Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
    @Override
    public void onResume(){
    	// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
    	try{
	    	super.onResume();
			
			currContext	= RiskDisclosureStatementActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
    }
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
    
    private class RiskDiclosureStatementTask extends AsyncTask<String, Void, String>{
    	private Context context;
    	private WebView webView;
    	private AsyncTask<String, Void, String> updateTask;
    	
    	public RiskDiclosureStatementTask(RiskDisclosureStatementActivity activity){
    		this.context			= activity;
    	}
    	
    	@Override
    	protected void onPreExecute(){
    		super.onPreExecute();
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    		try{
	    		this.updateTask	= this;
				updateTask = this;
				RiskDisclosureStatementActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! RiskDisclosureStatementActivity.this.isFinishing() )
					RiskDisclosureStatementActivity.this.dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
    	}
    	
    	@Override
		protected String doInBackground(String... arg0){
    		/* Mary@20130327 - Change_Request-20130104, ReqNo.3
			return AppConstants.brokerConfigBean.getStrRiskDisclosureStatementUrl();
			*/
    		return strRDSUrl;
		}
		
    	@Override
		protected void onPostExecute(final String strUrl){
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    		try{
	    		webView = (WebView) findViewById(R.id.wvRiskDiclosureStatement);
	    		
	    		// Added by Sonia@20130522 - Change_Request-20130424, ReqNo.3 - START
		    	Resources resources = getResources();
		   		DisplayMetrics metrics = resources.getDisplayMetrics();
		   		 if(metrics.densityDpi<200)
		   			webView.setInitialScale(56);
		   		 else if(metrics.densityDpi<240)
		   			webView.setInitialScale(75);
		   		 else if(metrics.densityDpi<280)
		   			webView.setInitialScale(75);
		   		 else if(metrics.densityDpi>=280)
		   			webView.setInitialScale(120);
		   		 else
		   			webView.setInitialScale(75);
		   		
		   		// loads the WebView completely zoomed out
		   		webView.getSettings().setLoadWithOverviewMode(true);
		   		// makes the Webview have a normal viewport (such as a normal desktop browser)
				webView.getSettings().setUseWideViewPort(true);
				webView.getSettings().setBuiltInZoomControls(true);
				// Added by Sonia@20130522 - Change_Request-20130424, ReqNo.3 - END
				
	    		webView.getSettings().setJavaScriptEnabled(true);
	    		webView.setWebViewClient(new WebViewClient() {    			 
					public boolean shouldOverrideUrlLoading(WebView view, String url) {				
						view.loadUrl(url);
						return true;
					}
	
					public void onPageFinished(WebView view, String url) {
						if( RiskDisclosureStatementActivity.this.dialog != null && RiskDisclosureStatementActivity.this.dialog.isShowing() )
							RiskDisclosureStatementActivity.this.dialog.dismiss();
					}
	              
					public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
						RiskDisclosureStatementActivity.this.alertDialog.setTitle(getResources().getString(R.string.dialog_title_error));
						RiskDisclosureStatementActivity.this.alertDialog.setMessage(description);
						RiskDisclosureStatementActivity.this.alertDialog.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								RiskDisclosureStatementActivity.this.finish();
							}
						});
						
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! RiskDisclosureStatementActivity.this.isFinishing() )
							RiskDisclosureStatementActivity.this.alertDialog.show();
					}
	    		});
	    		webView.loadUrl(strUrl);
	    		
	    		if( RiskDisclosureStatementActivity.this.dialog != null && RiskDisclosureStatementActivity.this.dialog.isShowing() )
	    			RiskDisclosureStatementActivity.this.dialog.dismiss();
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
    	}
    }

    private class updateRDSStatus extends AsyncTask<Boolean, Void,String>{
		private boolean isExpired		= false;
		private Exception exception 	= null;
		private boolean isDialogOwner	= false;
		private boolean isAcknowledge	= false;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    		try{
				if( ! RiskDisclosureStatementActivity.this.dialog.isShowing() ){
					RiskDisclosureStatementActivity.this.dialog.setMessage(getResources().getString(R.string.rds_update_progress));
	
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! RiskDisclosureStatementActivity.this.isFinishing() ){
						RiskDisclosureStatementActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		
		@Override
		protected String doInBackground(Boolean... params) {
			this.isAcknowledge = params[0];
			Map<String, Object> parameters 	= new HashMap<String, Object>();
			
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, strATPUserParam);
			
			parameters.put(ParamConstants.SENDERCODE_TAG, strSenderCode);
			parameters.put(ParamConstants.ACCOUNTNO_TAG, strAccountNo);
			parameters.put(ParamConstants.BROKERCODE_TAG, strBrokerCode);
			parameters.put(ParamConstants.BRANCHCODE_TAG, strBranchCode);
			parameters.put(ParamConstants.EXCHANGECODE_TAG, strExchgCode);
			parameters.put(ParamConstants.RDS_VERSIONNO_TAG, strRDSVersionNo);
			parameters.put(ParamConstants.RDS_AGREE_OR_DISAGREE_TAG, isAcknowledge);

			String result = null;
			try {
				result = AtpConnectUtil.updateRDSStatus(parameters);

				while(intRetryCtr < AppConstants.intMaxRetry && result == null){
					intRetryCtr++;
					result = AtpConnectUtil.updateRDSStatus(parameters);
				}
				intRetryCtr	= 0;
				
				if(result == null)
					throw new Exception(ErrorCodeConstants.FAIL_UPD_RDS_STATUS);
			}catch(FailedAuthenicationException e){        	
				isExpired	= true;
				LoginActivity.QCAlive.stopRequest();
				LoginActivity.refreshThread.stopRequest();
				exception	= e;
			} catch (Exception e) {
				exception	= e;
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    		try{
				if( RiskDisclosureStatementActivity.this.dialog!= null && RiskDisclosureStatementActivity.this.dialog.isShowing() && isDialogOwner ){
					RiskDisclosureStatementActivity.this.dialog.dismiss();
					isDialogOwner = false;
				}
	
				if(isExpired){
					RiskDisclosureStatementActivity.this.sessionExpired();
					return;
				}
	
				if(result == null || exception != null){
					alertDialog.setMessage( exception.getMessage() );
					alertDialog.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							RiskDisclosureStatementActivity.this.setResult(RESULT_CANCELED, new Intent()); 
							RiskDisclosureStatementActivity.this.finish();
						}
					});
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! RiskDisclosureStatementActivity.this.isFinishing() )
						alertDialog.show();
					
					exception	= null;
					return;
				}
				
				RiskDisclosureStatementActivity.this.setResult(isAcknowledge ? RESULT_OK : RESULT_CANCELED, new Intent()); 	
				RiskDisclosureStatementActivity.this.finish();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
}