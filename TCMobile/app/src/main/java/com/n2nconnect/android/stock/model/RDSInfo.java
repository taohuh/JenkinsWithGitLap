package com.n2nconnect.android.stock.model;

import java.util.ArrayList;
import java.util.List;

public class RDSInfo {
	private String strRDSVersion;
	private List<String> strLocalExchangeCode;
	private boolean isRDSEnable;
	private boolean isBlockByRDS;
	// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3
	private String strRDSUrl;
	
	public RDSInfo(){
		strRDSVersion 		= "";
		strLocalExchangeCode= new ArrayList<String>();
		isRDSEnable			= false;
		isBlockByRDS		= false;
		// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3
		strRDSUrl			= "";
	}

	
	
	public String getStrRDSVersion() {
		return strRDSVersion;
	}

	public void setStrRDSVersion(String strRDSVersion) {
		this.strRDSVersion = strRDSVersion;
	}

	public List<String> getStrLocalExchangeCode() {
		return strLocalExchangeCode;
	}

	public void setStrLocalExchangeCode(List<String> strLocalExchangeCode) {
		this.strLocalExchangeCode = strLocalExchangeCode;
	}
	
	public void addStrLocalExchangeCode(String strExchgCode){
		this.strLocalExchangeCode.add(strExchgCode);
	}
	
	public void setIsRDSEnable(boolean isRDSEnable){
		this.isRDSEnable = isRDSEnable;
	}
	public boolean isRDSEnable(){
		return this.isRDSEnable;
	}
	
	public void setIsBlockByRDS(boolean isBlockByRDS){
		this.isBlockByRDS = isBlockByRDS;
	}
	public boolean isBlockByRDS(){
		return this.isBlockByRDS;
	}
	
	// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - START
	public void setStrRDSUrl(String strRDSUrl){
		this.strRDSUrl = strRDSUrl;
	}
	public String getStrRDSUrl(){
		return this.strRDSUrl;
	}
	// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - END
}