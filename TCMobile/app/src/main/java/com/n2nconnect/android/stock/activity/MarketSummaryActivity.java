package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.MarketSummaryThread;
import com.n2nconnect.android.stock.MarketSummaryThread.OnMarketSummaryListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.ScreenReceiver;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.PieHelper;
import com.n2nconnect.android.stock.custom.PieView;
import com.n2nconnect.android.stock.model.MarketSummary;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class MarketSummaryActivity extends CustomWindow implements OnDoubleLoginListener
	/* Mary@20130621 - Fixes_Request-20130524, ReqNo.15
	, OnRefreshStockListener
	*/
	, OnMarketSummaryListener{
	
	private String symbolCode;
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	private String userParam;
	 */
	private MarketSummary marketSummary;
	private TextView tradeLabel;
	private TextView volumeLabel;
	private TextView valueLabel;
	private ImageView arrowImage;
	private TextView lastDoneLabel;
	private TextView changeLabel;
	private TextView changePercentLabel;
	// Added by Mary@20130621 - Fixes_Request-20130524, ReqNo.15 - START
	private TextView totalValueTitle;
	private TextView stockNameLabel;
	private final int INT_POS_PIE_CHART		= 2 ;
	private int intCurrPagePos;
	// Added by Mary@20130621 - Fixes_Request-20130524, ReqNo.15 - END
	/* Mary@20130621 - Fixes_Request-20130524, ReqNo.15
	private RefreshStockThread refreshThread;
	*/
	private MarketSummaryThread refreshThread;
	private ImageView chartView;
	private AnimationDrawable loadAnimation;
	private List<View> mView;
	private View layout0 = null;
	private View layout1 = null;
	private View layout2 = null;
	private View layout3 = null;
	//private ImageView imgPage;	//replaced with programmatically generated dots
	private ImageView pagerDot1;	
	private ImageView pagerDot2;
	private LayoutInflater inflater; 
	private MyPagerAdapter adapter;
	private ViewPager myPager;
	private int height;
	private int width;
	private ProgressDialog dialog;
	private String exchangeCode;
	private Map<String, Object> parameters;
	private DoubleLoginThread loginThread;
	private TextView noChartFoundView;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr	= 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	/* Mary@20130605 - comment unused variable
	// Added by Mary@20120803 - show no chart found textview
	private TextView tvNoChartFound;
	*/
	
	private StockApplication application;
	private String selectedExchangeCode;
	private WebView mktSummaryWebView;
	private String chartServerType;
	
	//Change_Request-20160722, ReqNo. 5
	private OrientationEventListener orientationListener;
	private boolean isChartActivityStarted	= false;
	private ImageView imgRotate;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if(this.mainMenu.isShowing()){
				this.mainMenu.hide();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			}else{
				finish();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			}
			// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}

	/* Mary@20130621 - Fixes_Request-20130523, ReqNo.15
	private void refreshStockData(List<StockSymbol> changedSymbols) {
		final StockSymbol symbol	= changedSymbols.get(0);

		/* Mary@20120804 - add filtering condition to avoid nullpointerexception on marketsummary
		if (symbol.getSymbolCode().equals(marketSummary.getSymbolCode())) {
		/
		if ( marketSummary != null && symbol.getSymbolCode().equals( marketSummary.getSymbolCode() ) ) {

			if( symbol.checkDiscrepancy(marketSummary) ){

				if( symbol.getLastDone() != marketSummary.getLastDone() ){
					float gap	= symbol.getLastDone() - marketSummary.getLastDone();
					marketSummary.setLastDone(symbol.getLastDone());
					
					if(height<900 && width<500){
						if( FormatUtil.formatDouble( marketSummary.getLastDone() ).length() > 8 )
							lastDoneLabel.setTextSize(25);
					}

					if( AppConstants.DEFAULT_EXCHANGE_CODE.equals("JKD") || AppConstants.DEFAULT_EXCHANGE_CODE.equals("JK") ){
						/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
						lastDoneLabel.setText(FormatUtil.formatPrice3(marketSummary.getLastDone()));
						/
						lastDoneLabel.setText( FormatUtil.formatPrice3(marketSummary.getLastDone(), "-") );
					}else{
						/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
						lastDoneLabel.setText(FormatUtil.formatPrice(marketSummary.getLastDone()));
						/
						lastDoneLabel.setText( FormatUtil.formatPrice(marketSummary.getLastDone(), "-") );
					}

					changeLabel.setText( FormatUtil.formatPriceChange(marketSummary.getPriceChange(), "###,##0.00") );
					changePercentLabel.setText( FormatUtil.formatPricePercent( marketSummary.getChangePercent() ) );

					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					if (gap > 0) {
						lastDoneLabel.setBackgroundColor(LayoutSettings.colorGreen);
						lastDoneLabel.setTextColor(Color.BLACK);
					} else if (gap < 0) {
						lastDoneLabel.setBackgroundColor(LayoutSettings.colorRed);
						lastDoneLabel.setTextColor(Color.WHITE);
					}
					/
					if(gap > 0){
						lastDoneLabel.setBackgroundColor( getResources().getColor(R.color.UpStatus_BgColor) );
						lastDoneLabel.setTextColor( getResources().getColor(R.color.UpStatus_FgColor) );
					}else if (gap < 0){
						lastDoneLabel.setBackgroundColor( getResources().getColor(R.color.DownStatus_BgColor) );
						lastDoneLabel.setTextColor( getResources().getColor(R.color.DownStatus_FgColor) );
					} 
					// Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - END
					
					lastDoneLabel.postDelayed(new Runnable() {
						public void run() {
							lastDoneLabel.setBackgroundColor(Color.TRANSPARENT);
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - START
							if (symbol.getPriceChange() > 0) {
								lastDoneLabel.setTextColor(LayoutSettings.colorGreen);
								changeLabel.setTextColor(LayoutSettings.colorGreen);
								changePercentLabel.setTextColor(LayoutSettings.colorGreen);
							} else if (symbol.getPriceChange() < 0) {
								lastDoneLabel.setTextColor(LayoutSettings.colorRed);
								changeLabel.setTextColor(LayoutSettings.colorRed);
								changePercentLabel.setTextColor(LayoutSettings.colorRed);
							} else {
								lastDoneLabel.setTextColor(Color.BLACK);
								changeLabel.setTextColor(Color.BLACK);
								changePercentLabel.setTextColor(Color.BLACK);
							}
							/
							int intStatusColor	= getResources().getColor( R.color.NoStatus_FgColor );
							if(symbol.getPriceChange() > 0)
								intStatusColor	= getResources().getColor( R.color.UpStatus_BgColor );
							else if(symbol.getPriceChange() < 0)
								intStatusColor	= getResources().getColor( R.color.DownStatus_BgColor );
							
							lastDoneLabel.setTextColor(intStatusColor);
							changeLabel.setTextColor(intStatusColor);
							changePercentLabel.setTextColor(intStatusColor);
							// Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - END
						}
					}, 1500);

					if(marketSummary.getPriceChange() > 0)
						arrowImage.setImageResource(LayoutSettings.upArrow);
					else if(marketSummary.getPriceChange() < 0)
						arrowImage.setImageResource(LayoutSettings.downArrow);
					else
						arrowImage.setImageDrawable(null);
				}
			}  
		}

	}
	*/
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.market_summary);
	
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
			height = metrics.heightPixels;
			width = metrics.widthPixels;
	
			this.title.setText(getResources().getString(R.string.mkt_summ_module_title));
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(LayoutSettings.menu_marketSummary_icn);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntMktSumMenuIconBgImg() );
	
			inflater = getLayoutInflater();
	
			//imgPage = (ImageView) findViewById(R.id.imgLoginTopLogo);
			pagerDot1	= (ImageView)findViewById(R.id.marketSummaryDot1);
			pagerDot2	= (ImageView)findViewById(R.id.marketSummaryDot2);
			imgRotate	= (ImageView)findViewById(R.id.imgRotate);
			imgRotate.setVisibility(View.VISIBLE);
	
			mView = new ArrayList<View>();
	
			layout0 = inflater.inflate(R.layout.empty_view, null);
			layout1 = inflater.inflate(R.layout.market_line_chart, null);
			layout2 = inflater.inflate(R.layout.market_pie_chart, null);
			layout3 = inflater.inflate(R.layout.empty_view, null);
	
			mView.add(layout0);
			mView.add(layout1);
			mView.add(layout2);
			mView.add(layout3);
			
			chartServerType	= AppConstants.brokerConfigBean.getChartType();
	
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(MarketSummaryActivity.this)
				//.setIcon(android.R.attr.alertDialogStyle)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
						return;
					}
				})
				.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			try {
				userParam = sharedPreferences.getString(PreferenceConstants.QC_USER_PARAM, null);
				if (userParam == null) {
					List<String>data = QcHttpConnectUtil.relogin();
					userParam = data.get(0);
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
					editor.commit();
				}
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}

	public void onResume() {
		try {
			super.onResume();
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= MarketSummaryActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
			/* Mary@20130129 - Fixes_Request-20130110, ReqNo.6
			// Added by Mary@20121116 - Change_Request-20120719, ReqNo.12
			AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, null);
			*/
			
			AppConstants.showMenu = true;

			application 			= (StockApplication) this.getApplication();
			symbolCode 				= application.getSelectedExchange().getIndiceSymbol();
			selectedExchangeCode	= application.getSelectedExchange().getQCExchangeCode();

			if(symbolCode == null)
				symbolCode = AppConstants.DEFAULT_MARKET_CODE;

			//Change_Request-20160722, ReqNo. 5 - START
			orientationListener 		= new OrientationEventListener(this, SensorManager.SENSOR_DELAY_UI) {
				public void onOrientationChanged(int orientation) {
					
					if(orientation > 70 && orientation < 290){
						
						if(!isChartActivityStarted){
							
							isChartActivityStarted		= true;
							ScreenReceiver.wasScreenOn	= false;
							
							Intent intent 				= new Intent();
							intent.putExtra(ParamConstants.CHART_DURATION_TAG, AppConstants.CHART_DURATION_MKT_SUMM);
							intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
							intent.setClass(MarketSummaryActivity.this, ChartActivity.class);
							startActivity(intent);
						}
					}else{
						isChartActivityStarted = false;
					}
				}
			};
			orientationListener.disable();
			
			//Change_Request-20160722, ReqNo. 5 - END
			
			/* Mary@20130621 - Fixes_Request-20130524, ReqNo.15
			String[] symbolCodes = { symbolCode };
			refreshThread = ((StockApplication) this.getApplication()).getRefreshThread();
			if (refreshThread == null) {
				int rate = sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				refreshThread = new RefreshStockThread(userParam, symbolCodes, rate);
				 /
				refreshThread = new RefreshStockThread(AppConstants.QCdata.getStrUserParam(), symbolCodes, rate);
				refreshThread.start();	
				refreshThread.setRegisteredListener(this);
				((StockApplication) this.getApplication()).setRefreshThread(refreshThread);
			} else {
				refreshThread.resumeRequest();
				refreshThread.setSymbolCodes(symbolCodes);
				refreshThread.setRegisteredListener(this);
			}
			*/
			if(refreshThread != null){
				refreshThread.resumeRequest();
				refreshThread.setStrSymbolCode(symbolCode);
			}else{
				int rate 		= sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
				refreshThread 	= new MarketSummaryThread(symbolCode, rate);
				refreshThread.start();
			}
			refreshThread.setRegisteredListener(this);
			
			loginThread 	= ((StockApplication)this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) MarketSummaryActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(MarketSummaryActivity.this);
				loginThread.resumeRequest();
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(this);

			dialog 			= new ProgressDialog(MarketSummaryActivity.this);
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				new stockRefresh().execute();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	protected void onPause() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onPause();
			
			if(this.mainMenu.isShowing())
				this.mainMenu.hide();
	
			if(refreshThread!=null)
				refreshThread.pauseRequest();
			
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}

	private class MyPagerAdapter extends PagerAdapter {
		public int getCount() {
			return 4;
		}

		public Object instantiateItem(View collection, int position) {
			((ViewPager) collection).addView(mView.get(position), 0);

			if(position==1){
				noChartFoundView = (TextView)mView.get(position).findViewById(R.id.noChartFoundView);
				MarketSummaryActivity.this.prepareMarketLineView(mView.get(position));
			/* Mary@20130621 - Fixes_Request-20130523,ReqNo.15 - START
			}else if(position==2){
				MarketSummaryActivity.this.prepareMarketPieView( mView.get(position) );
			*/
			}else if(position ==  INT_POS_PIE_CHART){
				
				MarketSummaryActivity.this.prepareMarketPieView(mView.get(position), marketSummary);
			// Mary@20130621 - Fixes_Request-20130523,ReqNo.15 - END
			}
			// Added by Mary@20130621 - Fixes_Request-20130523,ReqNo.15
			intCurrPagePos	= position;
			
			return mView.get(position);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(mView.get(arg1));
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == ((View) arg1);
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
	}

	private class stockRefresh extends AsyncTask<View, Void, String[]> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;

		private AsyncTask<View, Void, String[]> updateTask = null;
		
		@Override
		protected void onPreExecute() {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			try{
				updateTask = this;
				MarketSummaryActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! MarketSummaryActivity.this.isFinishing() )
					MarketSummaryActivity.this.dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String[] doInBackground(View... params) {
			// TODO Auto-generated method stub
			Map<String, String> parameters	= new HashMap<String, String>();
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			parameters.put(ParamConstants.USERPARAM_TAG, userParam);
			*/
			parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
			
			exchangeCode 					= AppConstants.DEFAULT_EXCHANGE_CODE;
			if(symbolCode.equals(AppConstants.DEFAULT_MARKET_CODE))
				exchangeCode = AppConstants.DEFAULT_EXCHANGE_CODE;
			parameters.put(ParamConstants.EXCHANGECODE_TAG, exchangeCode);

			String response = null;
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			try {
				response = QcHttpConnectUtil.imageMarket(parameters);
			} catch (FailedAuthenicationException e) {
				updateTask.cancel(true);
				new stockRefresh().execute();
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
			try {
				response = QcHttpConnectUtil.imageMarket(parameters);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response = QcHttpConnectUtil.imageMarket(parameters);
				}
				intRetryCtr	= 0;
				
				if(response == null)				
					throw new Exception(ErrorCodeConstants.FAIL_GET_MARKET_DETAIL);
			}catch(Exception e){
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END;

			if(response!=null){
				marketSummary = QcMessageParser.parseMarketSummary(response);
			}
			return null;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String[] symbolCodes) {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( dialog != null && dialog.isShowing() )
					dialog.dismiss();
				
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							finish();
						}
					});
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! MarketSummaryActivity.this.isFinishing() )
						alertException.show();	
					exception = null;
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
				adapter = new MyPagerAdapter();
				myPager = (ViewPager) findViewById(R.id.mypager);
				myPager.setAdapter(adapter);
	
				myPager.setCurrentItem(1);
	
				myPager.setOnPageChangeListener(new OnPageChangeListener(){
					@Override
					public void onPageScrollStateChanged(int status) {
						// TODO Auto-generated method stub
					}
	
					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						// TODO Auto-generated method stub
	
					}
	
					@Override
					public void onPageSelected(int position) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - START
						if(position == 0){
							myPager.setCurrentItem(position + 1);
						}else if(position==1){
							imgPage.setImageResource(R.drawable.page_chart_1);
						}else if(position==2){
							imgPage.setImageResource(R.drawable.page_chart_2);
						}else if(position == 3){
							myPager.setCurrentItem(position - 1);
						}
						*/
						
						switch(position){
							case 0 :
								myPager.setCurrentItem(position + 1);
								orientationListener.disable();	//Change_Request-20160722, ReqNo. 5
								break;
							case 1 :
								//Edited by Thinzar, Change_Request-20160101, ReqNo.3
								//imgPage.setImageResource(R.drawable.page_chart_1);
								setPagerDot(pagerDot1);
								imgRotate.setVisibility(View.VISIBLE);
								orientationListener.enable();	//Change_Request-20160722, ReqNo. 5
								break;
							case 2 :
								//Edited by Thinzar, Change_Request-20160101, ReqNo.3
								//imgPage.setImageResource(R.drawable.page_chart_2);
								setPagerDot(pagerDot2);
								imgRotate.setVisibility(View.GONE);
								orientationListener.disable();	//Change_Request-20160722, ReqNo. 5
								
								break;
							case 3 :
								myPager.setCurrentItem(position - 1);
								orientationListener.disable();	//Change_Request-20160722, ReqNo. 5
								break;
						}
						// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
					}
	
				});
	
				MarketSummaryActivity.this.prepareStockView(null, marketSummary, exchangeCode);	
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.3
	private void setPagerDot(ImageView imgPagerDot) {
		// reset all dots to white first
		pagerDot1.setImageResource(R.drawable.bottom_bar_grey_dot);
		pagerDot2.setImageResource(R.drawable.bottom_bar_grey_dot);

		// set red dot
		imgPagerDot.setImageResource(R.drawable.bottom_bar_white_dot);
	}

	/* Mary@20130621 - Fixes_Request-20130523, ReqNo.15
	private void prepareMarketPieView(View view) {
	*/
	private void prepareMarketPieView(View view, MarketSummary marketSummary) {
		//List<PieItem> pieItems		= new ArrayList<PieItem>();
		try{
			/* Commented by Thinzar, replaced with new chart Library
			PieItem upItem 				= new PieItem();
			
			upItem.setColor( getResources().getColor(R.color.UpStatus_BgColor) );
			upItem.setCount( marketSummary.getUpCounter() );
			pieItems.add(upItem);
	
			PieItem downItem 			= new PieItem();
			
			downItem.setColor( getResources().getColor(R.color.DownStatus_BgColor) );
			downItem.setCount( marketSummary.getDownCounter() );
			pieItems.add(downItem);
	
			PieItem unchangeItem 		= new PieItem();
			unchangeItem.setColor(getResources().getColor(R.color.market_summary_unchanged));
			unchangeItem.setCount (marketSummary.getUnchangeCounter() );
			pieItems.add(unchangeItem);
	
			PieItem untradeItem 		= new PieItem();
			untradeItem.setColor(getResources().getColor(R.color.market_summary_untraded));
			untradeItem.setCount( marketSummary.getUntradedCounter() );
			pieItems.add(untradeItem);
	
			int overlayId 				= R.drawable.cam_overlay_big;
			int size					= 140;
			Bitmap backgroundImage 		= Bitmap.createBitmap(size, size, Bitmap.Config.RGB_565);
	
			PieChartView pieChartView	= new PieChartView(this);
			pieChartView.setLayoutParams(new LayoutParams(size, size));
			pieChartView.setGeometry(size, size, 0, 0, 0, 0, overlayId);
			pieChartView.setData(pieItems, marketSummary.getTotalCounter());
			pieChartView.setSkinParams(Color.WHITE);
			pieChartView.invalidate();
			pieChartView.draw(new Canvas(backgroundImage));
	
			ImageView chartImageView	= (ImageView) view.findViewById(R.id.chartImageView);
			chartImageView.setImageResource(0);
			chartImageView.setImageBitmap(backgroundImage);
			*/
			
			int totalCount				= marketSummary.getTotalCounter();
			float upCounterPercentage		= (float) 100 * ((float)marketSummary.getUpCounter() / (float)totalCount);
			float downCounterPercentage		= (float) 100 * ((float)marketSummary.getDownCounter() / (float)totalCount);
			float unchangeCounterPercentage	= (float) 100 * ((float)marketSummary.getUnchangeCounter() / (float)totalCount);
			float untradedCounterPercentage	= (float) 100 - (upCounterPercentage + downCounterPercentage + unchangeCounterPercentage);
			
			System.out.println("PieValues(%): " + upCounterPercentage + "," + downCounterPercentage + "," + unchangeCounterPercentage + "," + untradedCounterPercentage);
					
			final PieView pieChartView		= (PieView)view.findViewById(R.id.pie_view);
			ArrayList<PieHelper> pieHelperArrayList = new ArrayList<PieHelper>();
	        pieHelperArrayList.add(new PieHelper(upCounterPercentage, getResources().getColor(R.color.UpStatus_BgColor)));
	        pieHelperArrayList.add(new PieHelper(downCounterPercentage, getResources().getColor(R.color.DownStatus_BgColor)));
	        pieHelperArrayList.add(new PieHelper(unchangeCounterPercentage, getResources().getColor(R.color.market_summary_unchanged)));
	        pieHelperArrayList.add(new PieHelper(untradedCounterPercentage, getResources().getColor(R.color.market_summary_untraded)));

	        pieChartView.showPercentLabel(false);
	        pieChartView.setData(pieHelperArrayList);
			
			((TextView) view.findViewById(R.id.upLabel)).setText( String.valueOf( marketSummary.getUpCounter()));
			((TextView) view.findViewById(R.id.downLabel)).setText( String.valueOf( marketSummary.getDownCounter()));
			((TextView) view.findViewById(R.id.unchangedLabel)).setText( String.valueOf( marketSummary.getUnchangeCounter()));
			((TextView) view.findViewById(R.id.untradedLabel)).setText( String.valueOf( marketSummary.getUntradedCounter()));
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			/*
			if(pieItems != null){
				pieItems.clear();
				pieItems = null;
			}
			*/
		}
	}
	
	/* Mary@20130621 - Fixes_Request-20130524, ReqNo.15 - START
	private void prepareStockView(String exchangeCode){
		/* Mary@20130311 - Fixes_Request-20130108, ReqNo.22	
		Map currencies 			= DefinitionConstants.getExchangeCurrencyMapping();
		/
		String currency 		= DefinitionConstants.getCurrencyMapping().get(exchangeCode);

		tradeLabel 				= (TextView) this.findViewById(R.id.tradeLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		tradeLabel.setText(FormatUtil.formatLong(marketSummary.getTotalTrade()));
		/
		tradeLabel.setText( FormatUtil.formatLong(marketSummary.getTotalTrade(), "-") );
		
		volumeLabel 			= (TextView) this.findViewById(R.id.volumeLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		( (TextView) this.findViewById(R.id.volumeLabel) ).setText(FormatUtil.formatLong(marketSummary.getVolume()));
		/
		volumeLabel.setText( FormatUtil.formatLong(marketSummary.getVolume(), "-") );
			
		valueLabel 				= (TextView) this.findViewById(R.id.valueLabel);
		TextView valueTitleLabel= (TextView)this.findViewById(R.id.totalValueLabel);
		valueTitleLabel.setText("Total Value ("+currency+")");
		/* Mary@20120815 - Fixes_Request-20120815, ReqNo.3
		if (currency !=null){
			valueLabel.setText(FormatUtil.formatDouble(marketSummary.getValue()));
		}else{
			valueLabel.setText(FormatUtil.formatDouble(marketSummary.getValue()));
		}
		/
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		valueLabel.setText( FormatUtil.formatDouble( marketSummary.getValue() ) );
		/
		valueLabel.setText( FormatUtil.formatDouble( marketSummary.getValue(), "-" ) );

		TextView nameLabel 	= (TextView) this.findViewById(R.id.stockNameLabel);
		nameLabel.setText(marketSummary.getStockCode());

		arrowImage 			= (ImageView) this.findViewById(R.id.arrowImage);
		if (marketSummary.getPriceChange() > 0)
			arrowImage.setImageResource(LayoutSettings.upArrow);
		else if (marketSummary.getPriceChange() < 0)
			arrowImage.setImageResource(LayoutSettings.downArrow);
		else
			arrowImage.setImageDrawable(null);
		
		lastDoneLabel = (TextView) this.findViewById(R.id.lastDoneLabel);
		if(height<900 && width<500){
			if(FormatUtil.formatDouble(marketSummary.getLastDone()).length()>8)
				lastDoneLabel.setTextSize(25);
		}

		if(AppConstants.DEFAULT_EXCHANGE_CODE.equals("JKD") || AppConstants.DEFAULT_EXCHANGE_CODE.equals("JK")){
			/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
			lastDoneLabel.setText(FormatUtil.formatDouble3(marketSummary.getLastDone()));
			/
			lastDoneLabel.setText(FormatUtil.formatDouble3(marketSummary.getLastDone(), "-"));
		}else{
			/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
			lastDoneLabel.setText(FormatUtil.formatDouble(marketSummary.getLastDone()));
			/
			lastDoneLabel.setText(FormatUtil.formatDouble(marketSummary.getLastDone(), "-"));
		}

		changeLabel = (TextView) this.findViewById(R.id.priceMarketLabel);
		changeLabel.setText(FormatUtil.formatPriceChange(marketSummary.getPriceChange(), "###,##0.00"));

		changePercentLabel = (TextView) this.findViewById(R.id.percentMarketLabel);
		changePercentLabel.setText(FormatUtil.formatPricePercent(marketSummary.getChangePercent()));
		/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - START
		if (marketSummary.getPriceChange() > 0) {
			lastDoneLabel.setTextColor(LayoutSettings.colorGreen);
			changeLabel.setTextColor(LayoutSettings.colorGreen);
			changePercentLabel.setTextColor(LayoutSettings.colorGreen);
		} else if (marketSummary.getPriceChange() < 0) {
			lastDoneLabel.setTextColor(LayoutSettings.colorRed);
			changeLabel.setTextColor(LayoutSettings.colorRed);
			changePercentLabel.setTextColor(LayoutSettings.colorRed);
		} else {
			changePercentLabel.setTextColor(Color.BLACK);
			changePercentLabel.setTextColor(Color.BLACK);
			changePercentLabel.setTextColor(Color.BLACK);
		}
		/
		int intStatusColor	= getResources().getColor(R.color.NoStatus_FgColor);
		
		if(marketSummary.getPriceChange() > 0)
			intStatusColor	= getResources().getColor(R.color.UpStatus_BgColor);
		else if(marketSummary.getPriceChange() < 0)
			intStatusColor	= getResources().getColor(R.color.DownStatus_BgColor);
		
		lastDoneLabel.setTextColor(intStatusColor);
		changeLabel.setTextColor(intStatusColor);
		changePercentLabel.setTextColor(intStatusColor);
		// Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - END
	}
*/
	private void initiateStockView(){
		totalValueTitle			= (TextView)this.findViewById(R.id.totalValueLabel);
		stockNameLabel	 		= (TextView) this.findViewById(R.id.stockNameLabel);
				
		tradeLabel 				= (TextView) this.findViewById(R.id.tradeLabel);
		volumeLabel 			= (TextView) this.findViewById(R.id.volumeLabel);
		valueLabel 				= (TextView) this.findViewById(R.id.valueLabel);
		arrowImage 				= (ImageView) this.findViewById(R.id.arrowImage);
		lastDoneLabel 			= (TextView) this.findViewById(R.id.lastDoneLabel);
		changeLabel 			= (TextView) this.findViewById(R.id.priceMarketLabel);
		changePercentLabel 		= (TextView) this.findViewById(R.id.percentMarketLabel);
	}

	private void prepareStockView(final MarketSummary marketSummaryOld, final MarketSummary marketSummaryNew, String strExchgCode){
		if(marketSummaryOld == null)
			initiateStockView();

		totalValueTitle	.setText("Total Value (" + ( strExchgCode != null ? DefinitionConstants.getCurrencyMapping().get(strExchgCode) : "-" ) + ")");
		stockNameLabel	.setText(marketSummaryNew.getStockCode());
		
		tradeLabel		.setText( FormatUtil.formatLong( marketSummaryNew.getTotalTrade(), "-") );
		volumeLabel		.setText( FormatUtil.formatLong( marketSummaryNew.getVolume(), "-") );
		valueLabel		.setText( FormatUtil.formatDouble( marketSummaryNew.getValue(), "-" ) );

		if (marketSummaryNew.getPriceChange() > 0)
			arrowImage.setImageResource(LayoutSettings.upArrow);
		else if (marketSummaryNew.getPriceChange() < 0)
			arrowImage.setImageResource(LayoutSettings.downArrow);
		else
			arrowImage.setImageDrawable(null);
		
		
		if(height<900 && width<500){
			if( FormatUtil.formatPrice( marketSummaryNew.getLastDone() ).length() > 8 )
				lastDoneLabel.setTextSize(25);
		}
		/*
		if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") )
			lastDoneLabel.setText( FormatUtil.formatPrice3(marketSummaryNew.getLastDone(), null) );
		else
			lastDoneLabel.setText( FormatUtil.formatPrice(marketSummaryNew.getLastDone(), null) );
		changeLabel.setText( FormatUtil.formatPriceChange( marketSummaryNew.getPriceChange(), "###,##0.00") );
		*/
		//Fixes_Request-20160101, ReqNo.3
		lastDoneLabel.setText(FormatUtil.formatPrice(marketSummaryNew.getLastDone(), null, selectedExchangeCode));
		changeLabel.setText(FormatUtil.formatPriceChangeByExchange(marketSummaryNew.getPriceChange(), selectedExchangeCode));
		
		changePercentLabel.setText( FormatUtil.formatPricePercent( marketSummaryNew.getChangePercent() ) );
		
		if( marketSummaryOld != null ){
			float gap	= marketSummaryNew.getLastDone() - marketSummaryOld.getLastDone();
			
			if(gap > 0){
				lastDoneLabel.setBackgroundColor( getResources().getColor(R.color.UpStatus_BgColor) );
				lastDoneLabel.setTextColor( getResources().getColor(R.color.UpStatus_FgColor) );
			}else if (gap < 0){
				lastDoneLabel.setBackgroundColor( getResources().getColor(R.color.DownStatus_BgColor) );
				lastDoneLabel.setTextColor( getResources().getColor(R.color.DownStatus_FgColor) );
			} 
			
			lastDoneLabel.postDelayed(new Runnable() {
				public void run() {
					lastDoneLabel.setBackgroundColor(Color.TRANSPARENT);
					int intStatusColor	= getResources().getColor( R.color.NoStatus_FgColor );
					if(marketSummaryNew.getPriceChange() > 0)
						intStatusColor	= getResources().getColor( R.color.UpStatus_BgColor );
					else if(marketSummaryNew.getPriceChange() < 0)
						intStatusColor	= getResources().getColor( R.color.DownStatus_BgColor );
					
					lastDoneLabel.setTextColor(intStatusColor);
					changeLabel.setTextColor(intStatusColor);
					changePercentLabel.setTextColor(intStatusColor);
				}
			}, 1500);
		}else{
			int intStatusColor	= getResources().getColor(R.color.NoStatus_FgColor);
			
			if(marketSummaryNew.getPriceChange() > 0)
				intStatusColor	= getResources().getColor(R.color.UpStatus_BgColor);
			else if(marketSummaryNew.getPriceChange() < 0)
				intStatusColor	= getResources().getColor(R.color.DownStatus_BgColor);
			
			lastDoneLabel.setTextColor(intStatusColor);
			changeLabel.setTextColor(intStatusColor);
			changePercentLabel.setTextColor(intStatusColor);
		}
	}
	// Mary@20130621 - Fixes_Request-20130524, ReqNo.15 - END
	
	private void prepareMarketLineView(View view) {
		chartView			= (ImageView) view.findViewById(R.id.chartImageView);
		mktSummaryWebView 	= (WebView)view.findViewById(R.id.mktSummaryWebView);
		parameters			= new HashMap<String, Object>();
		parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
		
		if(noChartFoundView.isShown())
			noChartFoundView.setVisibility(View.GONE);
		
		orientationListener.enable();	//Change_Request-20160722, ReqNo.5
		
		new displayChart().execute(parameters);
	}
	
	private class displayChart extends AsyncTask<Map<String, Object>, Void, String>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		@Override
		protected void onPreExecute(){
			chartView.setScaleType(ScaleType.CENTER);
			chartView.setImageResource(LayoutSettings.loading_animate);
			loadAnimation = (AnimationDrawable)chartView.getDrawable();
			loadAnimation.start();
			
			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(Map<String, Object>... params) {
			Map<String, Object> parameters 	= params[0];
			String imageUrl 				= null;
			
			try {
				imageUrl = QcHttpConnectUtil.chartImage(parameters, CommandConstants.CHART_TYPE_MARKET_INTRADAY);
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				while(intRetryCtr < AppConstants.intMaxRetry && imageUrl == null){
					intRetryCtr++;
					imageUrl = QcHttpConnectUtil.chartImage(parameters, CommandConstants.CHART_TYPE_MARKET_INTRADAY);
				}
				intRetryCtr	= 0;
				
				if(imageUrl == null)				
					throw new Exception(ErrorCodeConstants.FAIL_GET_PATH);
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
			}catch(Exception e){
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			return imageUrl;
		}
		
		@Override
		protected void onPostExecute(String url){
			super.onPostExecute(url);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					loadAnimation.stop();
					
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
						}
					});
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! MarketSummaryActivity.this.isFinishing() )
						alertException.show();	
					exception = null;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if (url == null) {
					return;
				}
				Bitmap bitmap = null;
				
				try {
						bitmap = QcHttpConnectUtil.loadBitmap(url);
	
				} catch (FailedAuthenicationException e) {
					// TODO Auto-generated catch block
					new displayChart().execute(parameters); // might cause infinite loop if it is FailedAuthenicationException continuously thrown
					e.printStackTrace();
				}
				*/
				/* Mary@20130925 - Move to Async Task to avoid android.os.NetworkOnMainThreadException
				Bitmap bitmap = null;
				if(url != null){
					try {
						bitmap = QcHttpConnectUtil.loadBitmap(url);
						// NOTE : No retry perform as there is possible that there are null bitmap loaded
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				loadAnimation.stop();
				
				if(bitmap!=null){
					chartView.setImageResource(0);
					chartView.setScaleType(ScaleType.FIT_XY);
					chartView.setImageBitmap(bitmap);
				}else{
					chartView.setImageResource(0);
					/* Mary@20120803 - Fixes_Request-20120724, ReqNo.16
					noChartFoundView.setVisibility(View.GONE);
					/
					noChartFoundView.setVisibility(View.VISIBLE);
				}
				*/
				new loadChartBitmap().execute(url);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		
	}
	
	// Added by Mary@20130925 - Move to Async Task to avoid android.os.NetworkOnMainThreadException - START
	class loadChartBitmap extends AsyncTask<String, Void, Bitmap>{		
		@Override
		protected void onPreExecute(){			
			super.onPreExecute();
		}
		
		@Override
		protected Bitmap doInBackground(String... params) {
			String url		= params[0];
			Bitmap bitmap 	= null;
			
			try {
				if(url != null && url.trim().length() > 0){
					try {
						
						bitmap = QcHttpConnectUtil.loadBitmap(url);
						// NOTE : No retry perform as there is possible that there are null bitmap loaded
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			return bitmap;
		}
		
		@Override
		protected void onPostExecute(Bitmap bitmap){
			super.onPostExecute(bitmap);
			
			try{
				loadAnimation.stop();
				
				if(bitmap!=null){
					chartView.setImageResource(0);
					chartView.setScaleType(ScaleType.FIT_XY);
					chartView.setImageBitmap(bitmap);
				}else{
					chartView.setImageResource(0);
					noChartFoundView.setVisibility(View.VISIBLE);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	// Added by Mary@20130925 - Move to Async Task to avoid android.os.NetworkOnMainThreadException - END

	/* Mary@20130605 - comment unused method
	private void handleFailedAuthentication() {
		//    	SystemUtil.showFailedAuthenicationAlert(this);
		try {
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			List<String>data = QcHttpConnectUtil.relogin();
			userParam = data.get(0);
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
			editor.commit();
			/

			RefreshStockThread refreshThread = ((StockApplication) this.getApplication()).getRefreshThread();

			if (refreshThread != null)
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				refreshThread.setUserParam(userParam);
				/
				refreshThread.setUserParam( AppConstants.QCdata.getStrUserParam() );

			QCAliveThread QCAlive = ((StockApplication) this.getApplication()).getQCAliveThread();

			if (QCAlive !=null)
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				QCAlive.setUserParam(userParam);
				/
				QCAlive.setUserParam( AppConstants.QCdata.getStrUserParam() );
			
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		} catch (FailedAuthenicationException e) {
		/
		}catch(Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(MarketSummaryActivity.this.mainMenu.isShowing()){
					MarketSummaryActivity.this.mainMenu.hide();
				}
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(refreshThread != null)
					refreshThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				if(!isFinishing()){
					MarketSummaryActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	final Handler handler = new Handler() {
		public void handleMessage(Message message) {
			// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.9
			try{
				/* Mary@20130621 - Fixes_Request-20130523, ReqNo.15
				List<StockSymbol> changedSymbols = (List<StockSymbol>) message.obj;
				/* Mary@20130621 - Fixes_Request-20130523, ReqNo.16
				if(changedSymbols.size()!=0)
				/
				if(changedSymbols != null && changedSymbols.size()!=0)
					MarketSummaryActivity.this.refreshStockData(changedSymbols);
				*/
				MarketSummary changedMarketSummary = (MarketSummary)message.obj;
				if(changedMarketSummary != null && marketSummary != null 
					&& changedMarketSummary.getSymbolCode() != null
					&& changedMarketSummary.getSymbolCode().equals( marketSummary.getSymbolCode() )
					&& changedMarketSummary.checkDiscrepancy(marketSummary)
					&& changedMarketSummary.getLastDone() != marketSummary.getLastDone() 
				){
					MarketSummary oldMarketSummary	= marketSummary;
					marketSummary					= changedMarketSummary;
					
					prepareStockView(oldMarketSummary, marketSummary, exchangeCode);
	
					if( intCurrPagePos == (INT_POS_PIE_CHART + 1) )
						prepareMarketPieView( mView.get(INT_POS_PIE_CHART), marketSummary);
				}
				// Mary@20130621 - Fixes_Request-20130523, ReqNo.15 - END
			// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.9 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.9 - END
		}
	};

	/* Mary@20130621 - Fixes_Request-20130524, ReqNo.15
	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		Message message	= handler.obtainMessage();
		message.obj 	= changedSymbols;
		handler.sendMessage(message);
	}
	*/
	@Override
	public void refreshMarketSummaryEvent(MarketSummary marketSummary){
		Message message	= handler.obtainMessage();
		message.obj 	= marketSummary;
		handler.sendMessage(message);
	}

	@Override
	protected void onStop() {
		super.onStop();
		System.out.println("debug22==MarketSummaryActivity onstop");
		orientationListener.disable();
	}
}