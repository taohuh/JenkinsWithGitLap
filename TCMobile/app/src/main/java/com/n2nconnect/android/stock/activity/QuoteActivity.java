package com.n2nconnect.android.stock.activity;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

//import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QCAliveThread.OnQCAliveListener;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.MarketTypeFilterListView;
import com.n2nconnect.android.stock.custom.MarketTypeFilterListView.OnMktTypeFilterSelectedListener;
import com.n2nconnect.android.stock.custom.SectorListView;
import com.n2nconnect.android.stock.custom.SectorListView.OnSectorSelectedListener;
import com.n2nconnect.android.stock.custom.TryRefreshableView;
import com.n2nconnect.android.stock.custom.TryRefreshableView.RefreshListener;
import com.n2nconnect.android.stock.gcm.StockAlertPubKey;
import com.n2nconnect.android.stock.gcm.StockAlertUtil;
import com.n2nconnect.android.stock.model.DtiResult;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.LotMarket;
import com.n2nconnect.android.stock.model.Sector;
import com.n2nconnect.android.stock.model.StockInfo;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.Watchlist;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.HttpHelper;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class QuoteActivity extends CustomWindow 
implements OnSectorSelectedListener, OnMktTypeFilterSelectedListener, OnRefreshStockListener, OnDoubleLoginListener, OnQCAliveListener{

	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	private String userParam;
	*/
	private boolean isTableConstructed;
	private List<Sector> sectors;
	private List<StockSymbol> stockSymbols;
	private Sector selectedSector;
	private SectorListView sectorListView;
	private Button sectorButton;
	private static int imageSortType;
	private boolean isDisplayPercentage;    
	private List<TextView> nameLabels;
	private List<TextView> priceLabels;
	private List<Button> changeButtons;
	/* Mary@20130718 - Change_Request-20130424, ReqNo.13
	// Added by mary@20130715 - Change_Request-20130424, ReqNo.13
	private List<TextView> exchgLabels;
	*/
	private List<TextView> volumeLabels;
	private List<TextView> valueLabels;
	private List<TextView> highLabels;
	private List<TextView> lowLabels;
	private List<TextView> tradeLabels;
	private List<TextView> openLabels;
	private ExchangeInfo selectedExchange;
	private String selectedExchangeCode;
	private TableLayout tableLayout;
	private boolean isFromLogin;
	private RefreshStockThread refreshThread;
	private DoubleLoginThread loginThread;
	/* Mary@20130529 - comment unused variable
	private Map exchangeSectors;
	private List exchangeInfos;
	*/
	private LinearLayout selectedRow;
	private int selectedId;
	private ProgressDialog dialog;
	
	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
	private TryRefreshableView rv;
	private ScrollView sv;
	private LayoutInflater inflate;
	private int totalCount;
	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END

	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr	= 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	// Added by Mary@20131004 - eliminate unnecessary global static variable
	public static final String DEFAULT_SECTOR_NAME		= "ALL STOCKS";
	
	//Added by Thinzar@20141009, Change_Request-20140901,ReqNo.8
	private LinearLayout nameLayout;
	private View imgNameSeparator;
	//private ImageView nameImage;
	
	//Added by Thinzar@20141027, Change_Request-20140901,ReqNo.12
	private Button marketButton;
	private MarketTypeFilterListView mktFilterListView;
	private List<String> mktTypes	= new ArrayList<String>();
	private String strSelectedFilterMktType;
	private String strSelectedFilterMktCode;	//default to "Normal Board Lot"
	private Map<String, String> filterMarketMap;
	
	StockApplication application;
	
	//Added by Thinzar, enhancements for TCMobile 2.0
	private TextView qNameLabel;
	private TextView moverLabel;
	private TextView gainerLabel;
	private TextView loserLabel;
	private TextView gainerPercentLabel;
	private TextView loserPercentLabel;
	private TextView TValueLabel;
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.8
	private List<Watchlist> watchlists;
	private boolean isExpired	 		= false;
	private final int INT_STOCK_CHART_PAGE_POS 		= 3;	//same as variables from StockActivity
	private final int INT_STOCK_NEWS_PAGE_POS 		= 6;	//same as variables from StockActivity
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.8
	private boolean isRedirectiBillionaire		= false;
	private String stockCodeFromiBillionaire	= "";
	private String exchgCodeFromiBillionaire	= "";
	
	private final int TRADE_TASK_ID				= 9001;
	private final int STOCK_DETAILS_TASK_ID		= 9002;
	
	//Fixes_Request-20161101, ReqNo.1
	private boolean isFromExchange				= false;
	
	//Change_Request-20161101, ReqNo.6
	private String verificationCodeFromIBillionaire		= "";
	private boolean isReturnVerificationToiBillionaire	= false;

	//Change_Request-20170119, ReqNo.5
	private Dialog dtiDialog=null;
	
	//Change_Request-20170601, ReqNo.8
	private String sgxAction;
	private String sgxPrice;
	private String sgxQty;
	private String sgxStockCode;
	private boolean isRedirectSgx = false;
	private boolean isMapStockCode = false; //Change_Request-20170601, ReqNo.12
	
	public void onCreate(Bundle savedInstanceState) {
		
		try {
			super.onCreate(savedInstanceState);
			System.out.println("debug==QuoteActivity onCreate()");
			
			if(this.getIntent() != null){
				Bundle bundle = this.getIntent().getExtras();
				//To check calling activity is login screen, to prompt user for logout when back button.
				isFromLogin = bundle.getBoolean(ParamConstants.FROM_LOGIN_TAG, false);
			}
			
			setContentView(R.layout.stock_quote);
			//initialize the menu
			
			//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
			ParamConstants.PAGE_NUMBER			= 0;
			ParamConstants.IMAGESORT			= CommandConstants.IMAGE_SORT_MOVER;		//Edited by Thinzar, Change_Request-20140901, ReqNo.8
			
			//Added by Thinzar@20141009, Change_Request-20140901, ReqNo.8
			if(SystemUtil.isDerivative(AppConstants.DEFAULT_EXCHANGE_CODE))	
				ParamConstants.IMAGESORT		= CommandConstants.IMAGE_SORT_NAME;
			
			inflate 							= getLayoutInflater();
			LinearLayout linear 				= (LinearLayout)findViewById(R.id.pullRefreshView);
			View v 								= inflate.inflate(R.layout.pull_to_refresh_tryscroll,null);
			v.setPadding(0, -20, 0, 0);
			linear.addView(v);
			tableLayout							= (TableLayout) findViewById(R.id.symbolTable);
			sv 									= (ScrollView) findViewById(R.id.trymySv);
			
			rv 									= (TryRefreshableView) findViewById(R.id.trymyRV);
			rv.mfooterView 						= (View) findViewById(R.id.tryrefresh_footer);
			rv.refreshIndicatorView2 			= (ImageView)findViewById(R.id.bottom_arrowImageView);
			rv.bar2 							= (ProgressBar)findViewById(R.id.bottom_progressBar);
			rv.sv 								= sv;
			rv.mfooterViewText 					= (TextView) findViewById(R.id.tryrefresh_footer_text);
			rv.headTotalCountView 				= (TextView)findViewById(R.id.head_totalCount);
			rv.TotalCountView 					= (TextView)findViewById(R.id.totalCount);
			
			rv.setRefreshListener(new RefreshListener(){
				@Override
				public void onRefresh() {
					// TODO Auto-generated method stub
					if (rv.mRefreshState == 4){
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
							ParamConstants.PAGE_NUMBER -= 1;
							new stockRefresh().execute();
							displayCurrentpage();
							rv.finishRefresh();	
					}

					if(rv.mfooterRefreshState==4){
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						ParamConstants.PAGE_NUMBER+=1;
						new stockRefresh().execute();
						displayCurrentpage();
						rv.finishRefresh();
					}
				}
			});
			// Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
			
			// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4
			if(! AppConstants.hasMultiLogin )
				repaintUI();
			
			application		= (StockApplication) this.getApplication();
			
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.1
			this.title.setText("iMSL");
			*/
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
			this.title.setText(R.string.app_name);
			this.icon.setImageResource(LayoutSettings.menu_home_icn);
			*/
			this.title.setText( AppConstants.brokerConfigBean.getStrAppName() );
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntHomeMenuIconBgImg() );
			// Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
			/* Commented by Thinzar, enhancements for TCMobile 2.0
			final ImageView moverImage			= (ImageView) this.findViewById(R.id.moverImage);
			final ImageView gainerImage 		= (ImageView) this.findViewById(R.id.gainerImage);
			final ImageView loserImage 			= (ImageView) this.findViewById(R.id.loserImage);
			final ImageView gainerPercentImage 	= (ImageView) this.findViewById(R.id.gainerPercentImage);
			final ImageView loserPercentImage 	= (ImageView) this.findViewById(R.id.loserPercentImage);
			final ImageView tradeValueImage 	= (ImageView) this.findViewById(R.id.TValueImage);
			nameImage							= (ImageView) this.findViewById(R.id.nameImage);		//Added by Thinzar@20141009, Change_Request-20140901,ReqNo.8
			*/
			
			//Added by Thinzar, enhancements for TCMobile 2.0
			qNameLabel			= (TextView)this.findViewById(R.id.qNameLabel);
			moverLabel			= (TextView)this.findViewById(R.id.moverLabel);
			gainerLabel			= (TextView)this.findViewById(R.id.gainerLabel);
			loserLabel			= (TextView)this.findViewById(R.id.loserLabel);
			gainerPercentLabel	= (TextView)this.findViewById(R.id.gainerPercentLabel);
			loserPercentLabel	= (TextView)this.findViewById(R.id.loserPercentLabel);
			TValueLabel			= (TextView)this.findViewById(R.id.TValueLabel);
			
			//Added by Thinzar@20141009, Change_Request-20140901,ReqNo.8 - START
			nameLayout 			= (LinearLayout) this.findViewById(R.id.nameLayout);
			imgNameSeparator	= (View) this.findViewById(R.id.imgNameSeparator);
			if(ParamConstants.IMAGESORT == CommandConstants.IMAGE_SORT_MOVER){
				/* 
				moverImage.setImageResource(LayoutSettings.indicatorArrow);
				nameImage.setImageDrawable(null);
				*/
				setSelectedSortBackground(moverLabel);	//Added by Thinzar, enhancements for TCMobile2.0
			} else if(ParamConstants.IMAGESORT == CommandConstants.IMAGE_SORT_NAME){
				/*
				nameImage.setImageResource(LayoutSettings.indicatorArrow);
				moverImage.setImageDrawable(null);
				*/
				setSelectedSortBackground(qNameLabel);	//Added by Thinzar, enhancements for TCMobile2.0
			}
			
			nameLayout.setOnClickListener(
				new OnClickListener() {
					public void onClick(View view) {
						
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						
						setSelectedSortBackground(qNameLabel);	//Added by Thinzar, enhancements for TCMobile2.0
						
						QuoteActivity.this.sortImageByType(CommandConstants.IMAGE_SORT_NAME);						
						imageSortType 							= CommandConstants.IMAGE_SORT_NAME;
						ParamConstants.IMAGESORT 				= imageSortType;
						QuoteActivity.this.isDisplayPercentage 	= false;
						QuoteActivity.this.setDisplayButtonPrice();
					}
				}
			);
			//Added by Thinzar@20141009, Change_Request-20140901,ReqNo.8 - END
			
			LinearLayout moverLayout = (LinearLayout) this.findViewById(R.id.moverLayout);
			moverLayout.setOnClickListener(
				new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						setSelectedSortBackground(moverLabel);	//Added by Thinzar, enhancements for TCMobile2.0
						
						QuoteActivity.this.sortImageByType(CommandConstants.IMAGE_SORT_MOVER);						
						imageSortType 							= CommandConstants.IMAGE_SORT_MOVER;
						// Added by Kw@20120817 - Change Request-20120719, ReqNo.9
						ParamConstants.IMAGESORT 				= imageSortType;
						QuoteActivity.this.isDisplayPercentage 	= false;
						QuoteActivity.this.setDisplayButtonPrice();
					}
				}
			);

			LinearLayout gainerLayout = (LinearLayout) this.findViewById(R.id.gainerLayout);
			gainerLayout.setOnClickListener(
				new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						setSelectedSortBackground(gainerLabel);	//Added by Thinzar, enhancements for TCMobile2.0
						
						QuoteActivity.this.sortImageByType(CommandConstants.IMAGE_SORT_GAINER);
						imageSortType 							= CommandConstants.IMAGE_SORT_GAINER;
						// Added by Kw@20120817 - Change Request-20120719, ReqNo.9
						ParamConstants.IMAGESORT 				= imageSortType;
						QuoteActivity.this.isDisplayPercentage 	= false;
						QuoteActivity.this.setDisplayButtonPrice();
					}
				}
			);

			LinearLayout loserLayout = (LinearLayout) this.findViewById(R.id.loserLayout);
			loserLayout.setOnClickListener(
				new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						setSelectedSortBackground(loserLabel);	//Added by Thinzar, enhancements for TCMobile2.0
						
						QuoteActivity.this.sortImageByType(CommandConstants.IMAGE_SORT_LOSER);
						imageSortType 							= CommandConstants.IMAGE_SORT_LOSER;
						// Added by Kw@20120817 - Change Request-20120719, ReqNo.9
						ParamConstants.IMAGESORT 				= imageSortType;
						QuoteActivity.this.isDisplayPercentage 	= false;
						QuoteActivity.this.setDisplayButtonPrice();
					}
				}
			);

			LinearLayout gainerPercentLayout = (LinearLayout) this.findViewById(R.id.gainerPercentLayout);
			gainerPercentLayout.setOnClickListener(
				new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						setSelectedSortBackground(gainerPercentLabel);	//Added by Thinzar, enhancements for TCMobile2.0
						
						QuoteActivity.this.sortImageByType(CommandConstants.IMAGE_SORT_GAINER_PERCENT);
						imageSortType 							= CommandConstants.IMAGE_SORT_GAINER_PERCENT;
						// Added by Kw@20120817 - Change Request-20120719, ReqNo.9
						ParamConstants.IMAGESORT				= imageSortType;
						QuoteActivity.this.isDisplayPercentage 	= true;
						QuoteActivity.this.setDisplayButtonPercentage();
					}
				}
			);

			LinearLayout loserPercentLayout = (LinearLayout) this.findViewById(R.id.loserPercentLayout);
			loserPercentLayout.setOnClickListener(
				new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						setSelectedSortBackground(loserPercentLabel);	//Added by Thinzar, enhancements for TCMobile2.0
						
						QuoteActivity.this.sortImageByType(CommandConstants.IMAGE_SORT_LOSER_PERCENT);
						imageSortType 							= CommandConstants.IMAGE_SORT_LOSER_PERCENT;
						// Added by Kw@20120817 - Change Request-20120719, ReqNo.9
						ParamConstants.IMAGESORT 				= imageSortType;
						QuoteActivity.this.isDisplayPercentage 	= true;
						QuoteActivity.this.setDisplayButtonPercentage();
					} 
				}
			);

			//Added by Thinzar, Change_Request-20141101, ReqNo. 4, configurable label for PSE
			TValueLabel.setText(AppConstants.brokerConfigBean.getTradeValueLabelOnQuoteScreen());
			
			//Change_Request-20170119, ReqNo.6configurable label for PSE
			moverLabel.setText(AppConstants.brokerConfigBean.getMoversLabelOnQuoteScreen());
			
			LinearLayout tradeValueLayout = (LinearLayout)this.findViewById(R.id.TValueLayout);
			tradeValueLayout.setOnClickListener(
				new OnClickListener(){
					@Override
					public void onClick(View arg0) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						setSelectedSortBackground(TValueLabel);	//Added by Thinzar, enhancements for TCMobile2.0
						
						QuoteActivity.this.sortImageByType(CommandConstants.IMAGE_SORT_TRADE_VALUE);
						imageSortType 							= CommandConstants.IMAGE_SORT_TRADE_VALUE;
						// Added by Kw@20120817 - Change Request-20120719, ReqNo.9
						ParamConstants.IMAGESORT 				= imageSortType;
						QuoteActivity.this.isDisplayPercentage 	= false;
						QuoteActivity.this.setDisplayButtonPrice();
					}
				}
			);

			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			userParam = sharedPreferences.getString(PreferenceConstants.QC_USER_PARAM, null);
			if (userParam == null) {
				List<String>data = QcHttpConnectUtil.relogin();
				userParam = data.get(0);
				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
				editor.commit();
			}
			*/
			
			sectorListView				= new SectorListView(this, this, getLayoutInflater());
			sectorListView.setHideOnSelect(true);

			sectorButton 				= (Button) this.findViewById(R.id.sectorButton);
			sectorButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
					
					// Added by Kw@20120817 - Change Request-20120719, ReqNo.9
					ParamConstants.PAGE_NUMBER=0;
					QuoteActivity.this.doSector();				
				}        	
			});
			
			//Added by Thinzar@20141024, Change-Request-20140901, ReqNo 12
			marketButton	= (Button) findViewById(R.id.marketButton);

			changeButtons				= new ArrayList<Button>();
			nameLabels 					= new ArrayList<TextView>();
			priceLabels 				= new ArrayList<TextView>();
			/* Mary@20130718 - Change_Request-20130424, ReqNo.13
			// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13
			exchgLabels					= new ArrayList<TextView>();
			*/
			volumeLabels 				= new ArrayList<TextView>();
			valueLabels 				= new ArrayList<TextView>();
			highLabels 					= new ArrayList<TextView>();
			lowLabels 					= new ArrayList<TextView>();
			tradeLabels 				= new ArrayList<TextView>();
			openLabels 					= new ArrayList<TextView>();

			imageSortType 				= ParamConstants.IMAGESORT; //CommandConstants.IMAGE_SORT_MOVER;	//Edited by Thinzar@20141009, Change_Request-20140901,ReqNo.8

			this.dialog 				= new ProgressDialog(QuoteActivity.this);
			
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			this.alertException 		= new AlertDialog.Builder(QuoteActivity.this)
			//.setIcon(android.R.attr.alertDialogStyle)
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,	int id) {
					dialog.dismiss();
					return;
				}
			})
			.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			//codes moved from onResume(), Fixes_Request-20161101, ReqNo.1 - START
			selectedExchange 					= application.getSelectedExchange();
			selectedExchangeCode				= selectedExchange.getQCExchangeCode();
			//codes moved from onResume(), Fixes_Request-20161101, ReqNo.1 - END
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private void setSelectedSortBackground(TextView txtSelectedSort){
		
		qNameLabel.setTextColor(getResources().getColor(R.color.quote_market_label_unselected));
		moverLabel.setTextColor(getResources().getColor(R.color.quote_market_label_unselected));
		gainerLabel.setTextColor(getResources().getColor(R.color.quote_market_label_unselected));
		loserLabel.setTextColor(getResources().getColor(R.color.quote_market_label_unselected));
		gainerPercentLabel.setTextColor(getResources().getColor(R.color.quote_market_label_unselected));
		loserPercentLabel.setTextColor(getResources().getColor(R.color.quote_market_label_unselected));
		TValueLabel.setTextColor(getResources().getColor(R.color.quote_market_label_unselected));
		
		//txtSelectedSort.setBackgroundColor(getResources().getColor(R.color.light_grey_bg));
		txtSelectedSort.setTextColor(getResources().getColor(android.R.color.black));
		
	}
	
	//Added by Thinzar, Fixes_Request-20161101, ReqNo.1 - START
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		System.out.println("debug==onNewIntent()");
		
		if(intent != null){
			Bundle bundle = intent.getExtras();
			System.out.println("debug==getting bundle extras");
			//To check calling activity is login screen, to prompt user for logout when back button.
			if(bundle != null) isFromExchange	= bundle.getBoolean(ParamConstants.FROM_EXCHANGE_TAG, false);		//Fixes_Request-20161101, ReqNo.1
		}
		
		if(isFromExchange){
			selectedExchange 		= application.getSelectedExchange();
			selectedExchangeCode	= selectedExchange.getQCExchangeCode();
		}
	}
	//Added by Thinzar, Fixes_Request-20161101, ReqNo.1 - END

	public void onResume() {
		
		// Added by Mary@20130319 - Fixes_Request-20130319, ReqNo.2
		try{
			super.onResume();
			System.out.println("debug==QuoteActivity onResume()");
			
			//Added by Thinzar, Change_Request-20160722, ReqNo.8 - START (verify iBillionaire URL scheme)
			/* 
			 * itradeappsg://iBillionaire/stk:AAPL:O/ 
			 * itradeappsg://ibillionaire/verify/51djjpi3ev
			 * itradeappsg://sgxtrademobile/buysell?action=buy&price=7.410&quantity=2350&stockcode=S68 
			 */
			//reset variables everytime onResume() is called
			isRedirectiBillionaire	= false;
			isRedirectSgx			= false;
			stockCodeFromiBillionaire	= "";
			exchgCodeFromiBillionaire	= "";
			sgxAction				= null;
			sgxPrice				= null;
			sgxQty					= null;
			sgxStockCode			= null;
			
			String externalIntentUri	 = application.getExternalIntentUri();
			System.out.println("debug==onResume() externalIntentUri:" + externalIntentUri);
			
			if(externalIntentUri != null && externalIntentUri.length() > 0){
				if(externalIntentUri.toLowerCase().contains(AppConstants.iBillionaireToken.toLowerCase())){
					if(AppConstants.brokerConfigBean.isEnableIBillionaire){
						
						String iBillionaireAppLinkData		= externalIntentUri;
						if(iBillionaireAppLinkData.length() > 0){
							String[] appLinkArray	= iBillionaireAppLinkData.split("/");
							
							if(appLinkArray.length < 2){
								
								showAlert(getResources().getString(R.string.invalid_data_format_ibillionaire));
								
							}else{
								String token		= appLinkArray[0];
								String data			= appLinkArray[1];
								
								if(!token.equalsIgnoreCase(AppConstants.iBillionaireToken)){
									showAlert(getResources().getString(R.string.unrecognized_token_ibillionaire));
								}else{
									//handle verification
									if(data.equalsIgnoreCase(AppConstants.iBillionaireVerifyToken)){
										if(appLinkArray.length < 3){
											showAlert(getResources().getString(R.string.invalid_verification_code_ibillionaire));
										} else{
											verificationCodeFromIBillionaire			= appLinkArray[2].trim();
											
											if(verificationCodeFromIBillionaire.length() > 0)
												isReturnVerificationToiBillionaire	= true;
											else
												showAlert(getResources().getString(R.string.invalid_verification_code_ibillionaire));
										}
									}
									//handle trade
									else{
										String[] dataArray	= data.split(":");
										
										if(dataArray.length < 3){
											showAlert(getResources().getString(R.string.invalid_data_format_ibillionaire));
										} else{
											stockCodeFromiBillionaire		= dataArray[1];
											exchgCodeFromiBillionaire		= dataArray[2];
											isRedirectiBillionaire			= true;
										}
									}
								}
							}
						}
					}
				}
				//Change_Request-20170601, ReqNo.8
				else if(externalIntentUri.toLowerCase().contains(AppConstants.SGX_APP_ADDRESS.toLowerCase())){
					//e.g., sgxtrademobile/buysell?action=buy&price=7.410&quantity=2350&stockcode=S68
					int startIdx 		= externalIntentUri.indexOf(AppConstants.SGX_APP_ADDRESS) + AppConstants.SGX_APP_ADDRESS.length() + 1; 
					String sgxParams 	= externalIntentUri.substring(startIdx, externalIntentUri.length());
					DefinitionConstants.Debug("sgxParams: " + sgxParams);
					
					String[] sgxParamsArray = sgxParams.split("\\&");
					int index = 0;
					while(index < sgxParamsArray.length){
						String column = sgxParamsArray[index];
						String[] columnArray = column.split("\\=");
						
						if(columnArray.length > 1){
							String titleColumn = columnArray[0];
							String paramColumn = columnArray[1];
							
							if(titleColumn.equalsIgnoreCase(AppConstants.SGX_ACTION_PARAM)){
								sgxAction = paramColumn;
							}else if(titleColumn.equalsIgnoreCase(AppConstants.SGX_PRICE_PARAM)){
								sgxPrice = paramColumn;
							}else if(titleColumn.equalsIgnoreCase(AppConstants.SGX_QTY_PARAM)){
								sgxQty 	= paramColumn;
							}else if(titleColumn.equalsIgnoreCase(AppConstants.SGX_STK_CODE_PARAM)){
								sgxStockCode = paramColumn;
							}
						}
						
						index++;
					}
					
					System.out.println("debugSgx==action:" + sgxAction + ", price:" + sgxPrice
							+ ", quantity: " + sgxQty + ", stockCode:" + sgxStockCode);
					
					if(sgxStockCode==null || sgxStockCode.length() == 0){
						showAlert(getResources().getString(R.string.sgx_stock_code_empty));
					}else{
						//Change_Request-20170601, ReqNo.12
						if(externalIntentUri.toLowerCase().contains("itradeappmy"))
							isMapStockCode = true;

						isRedirectSgx = true;
					}
				}
			}
			//Added by Thinzar, Change_Request-20160722, ReqNo.8 - END
						    
			// Added by Mary@20121116 - Change_Request-20120719, ReqNo.4
			AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, null);
			
			// Added by Kw@20120817 - Change Request-20120719, ReqNo.9
			imageSortType	 					= ParamConstants.IMAGESORT;
			AppConstants.showMenu 				= true;
			//ParamConstants.PAGE_NUMBER			= 0;	//Commented for Fixes_Request-20161101, ReqNo.1
			
			loginThread 						= ((StockApplication) QuoteActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) QuoteActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(QuoteActivity.this);
				loginThread.resumeRequest();
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(QuoteActivity.this);
	
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			userParam = sharedPreferences.getString(PreferenceConstants.QC_USER_PARAM, null);
			*/
			
			if(selectedRow!=null){
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				if( (selectedId + 1) % 2 != 0)
					selectedRow.setBackgroundResource(LayoutSettings.tablerow_dark);
				else
					selectedRow.setBackgroundResource(LayoutSettings.tablerow_light);
				*/
				//selectedRow.setBackgroundResource( ( (selectedId + 1) % 2 != 0) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);		//Change_Request-20160101, ReqNo.3
	
				StockSymbol symbol	= stockSymbols.get(selectedId);
				if( symbol.isExpand() ){
					int childCount = selectedRow.getChildCount();
					if(childCount!=1)
						selectedRow.removeViewAt(1);
	
					symbol.setExpand(false);
					/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
					if( (selectedId + 1) % 2 != 0)
						selectedRow.setBackgroundResource(LayoutSettings.tablerow_dark);
					else
						selectedRow.setBackgroundResource(LayoutSettings.tablerow_light);
					*/
					//selectedRow.setBackgroundResource( ( (selectedId + 1) % 2 != 0) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light );		//Change_Request-20160101, ReqNo.3
				}
			}
	
			//final StockApplication application	= (StockApplication) this.getApplication();	//commented by Thinzar, unused variable
			
			// Added by Mary@20130221 - Fixes_Request-20130108, ReqNo.12 - START
			List<Sector> selectedSectors		= selectedExchange.getSectors();

			if( selectedSectors == null || ( selectedSectors != null && selectedSectors.size() == 0 ) ){
				if( QuoteActivity.this.dialog != null && QuoteActivity.this.dialog.isShowing() )
					QuoteActivity.this.dialog.dismiss();
				
				alertException.setTitle(getResources().getString(R.string.quote_imcomplete_exchange));
				// Mary@20130307 - avoid nullpointer exception when selectedexchange is not available
				/* Mary@20130429 - Fixes_Request-20130419, ReqNo.4
				alertException.setMessage("Fail to get " + selectedExchange.getExchnageName() == null ? "" : selectedExchange.getExchnageName().toUpperCase() + " sector(s) details.");
				*/
				alertException.setMessage(getResources().getString(R.string.quote_fail_to_get1) 
						+ " " + ( selectedExchange.getExchnageName() == null ? "" : selectedExchange.getExchnageName().toUpperCase() ) 
						+ " " + getResources().getString(R.string.quote_fail_to_get2));
				alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						dialog.dismiss();
						if(application.getExchangeInfos().size() > 1){
							Intent intent	= new Intent();
							intent.setClass(QuoteActivity.this, ExchangeActivity.class);
							startActivity(intent);
						}else{
							new logout().execute();
						}
					}
				});
				
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! QuoteActivity.this.isFinishing() )
					alertException.show();
				
				return;
			}
			// Added by Mary@20130221 - Fixes_Request-20130108, ReqNo.12 - END
			
			/* Mary@20130312 - Fixes_Request-20130108, ReqNo.19
			if( AppConstants.DEFAULT_EXCHANGE_CODE.equals("MY") ){
			*/
			
			if(isFromLogin || isFromExchange){	//Edited by Thinzar, Fixes_Request-20161101, ReqNo.1
				
				//set both variable back to false, to be consumed only once
				isFromLogin					= false;		
				isFromExchange				= false;
				ParamConstants.PAGE_NUMBER	= 0;
				
				if( AppConstants.getListDerivativesExchgCode().contains(AppConstants.DEFAULT_EXCHANGE_CODE) ){
					selectedSector 	= selectedExchange.getSectors().get(0);
					sectors 		= selectedExchange.getSectors();
					
					try {
						sectorListView.setSectorList(sectors);
					}catch (Exception e) {
						e.printStackTrace();
					}
					
					//Added by Thinzar@20141009, Change_Request-20140901, ReqNo. 8
					nameLayout.setVisibility(View.VISIBLE);
					imgNameSeparator.setVisibility(View.VISIBLE);
					
					// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
					if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
						new stockRefresh().execute();
				}else{
					populateSectorList();
					
					//Added by Thinzar@20141009, Change_Request-20140901, ReqNo. 8
					nameLayout.setVisibility(View.GONE);
					imgNameSeparator.setVisibility(View.GONE);
				}
				
				populateFilterMarketButton();
				
				// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
				currContext	= QuoteActivity.this;
				
				//reset back the selected market code, Added by Thinzar, Change_Request-20150901, ReqNo.5
				AppConstants.SELECTED_FEED_MARKET_CODE	= AppConstants.DEFAULT_FEED_MARKET_CODE;
				
			} else{
				resumeRefreshStockThread();
				this.handleExternalIntentsRedirect();
			}
			
			if(idleThread == null)
				initiateIdleThread();
			else if( idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
			// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.22 - START
			if(refreshThread != null){
				refreshThread.setSymbolCodes( SystemUtil.getSymbolCodes(stockSymbols) );
				refreshThread.setRegisteredListener(QuoteActivity.this);
				
				if( refreshThread.isPaused() ){
					/* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
					refreshThread.setRegisteredListener(QuoteActivity.this);
					*/
					refreshThread.resumeRequest();
				}
			}
			// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.22 - END
			
			//Added by Thinzar, Change_Request-20170119, ReqNo.5
			if(AppConstants.brokerConfigBean.isDtiEnabled() && AppConstants.brokerConfigBean.isShowDtiPopUp()
					&& !AppConstants.hasCheckedDti){
				String doNotShowDtiDate	= sharedPreferences.getString(PreferenceConstants.DTI_DO_NOT_SHOW_DATE, "");
				String todayDateString	= SystemUtil.getTodayDate(FormatUtil.format_yyyyMMdd);
				
				if(!doNotShowDtiDate.equals(todayDateString)){
					String dtiFullLink	= AppConstants.brokerConfigBean.getDtiApiUrl();
					/* commented dut to API changes
					Date todayDate	= Calendar.getInstance().getTime();
					Calendar cal = Calendar.getInstance();
					cal.setTime(todayDate);
					
					if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY){
						String dtiDateString		= SystemUtil.getNDaysFromDate(todayDateString, -3);
						dtiFullLink	= AppConstants.getDtiApiFullUrl(dtiDateString);
					}else{
						dtiFullLink	= AppConstants.getDtiApiFullUrl(SystemUtil.getNDaysFromDate(todayDateString, -1));
					}
					*/
					DefinitionConstants.Debug("[DTI] API:" + dtiFullLink);
					new FetchDtiTask().execute(dtiFullLink);
					
				}else{
					//Change_Request-20170119, ReqNo.15 
					checkStatusForPushRegistration();
				}
			}else{
				//Change_Request-20170119, ReqNo.15 
				checkStatusForPushRegistration();
			}
			
		// Added by Mary@20130319 - Fixes_Request-20130319, ReqNo.2 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130319 - Fixes_Request-20130319, ReqNo.2 - END
	}
	
	@Override
	protected void onPause() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		try{
			super.onPause();
			
			// Mary@20120724 - avoid NullPointerException
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
			
			// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.22 - START
			if(refreshThread != null)
				refreshThread.pauseRequest();
			// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.22 - END
		
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	
	// Added by Mary@20130606 - Fixes_Request-20130523, ReqNo.6 - START
	@Override
	protected void onStop(){
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		try{
			super.onStop();
			
			if(alertException != null)
				alertException.cancel();
			
			if(dialog != null)
				dialog.cancel();
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	// Added by Mary@20130606 - Fixes_Request-20130523, ReqNo.6 - END
	
	public void onSaveInstanceState(Bundle savedInstanceState) {
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		savedInstanceState.putString("userParam", userParam);
		*/
		// Added by Mary@20130521 - Fixes_Reqeust-20130424, ReqNo.10
		if(AppConstants.QCdata != null)
			savedInstanceState.putString("userParam", AppConstants.QCdata.getStrUserParam() );
		
		super.onSaveInstanceState(savedInstanceState);    
	}

	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		userParam = savedInstanceState.getString("userParam");
		*/
	}
	
	@Override
	public void onBackPressed() {
		// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
		
		if( this.mainMenu.isShowing() )
			this.mainMenu.hide();
	}

	
	
	
	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		Message message = handler.obtainMessage();
		message.obj		= changedSymbols;
		handler.sendMessage(message);
	}

	final Handler handler = new Handler() {
		public void handleMessage(Message message) {
			List<StockSymbol> changedSymbols = (List<StockSymbol>) message.obj;
			QuoteActivity.this.refreshStockData(changedSymbols);
		}
	};

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			//if (keyCode == KeyEvent.KEYCODE_BACK && this.isFromLogin) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {			//Edited by Thinzar, Fixes_Request-20161101, ReqNo.1
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
				
				if(this.mainMenu.isShowing()){
					this.mainMenu.hide();
					return true;
				}else if(sectorListView.isShowing()){
					sectorListView.hide();
					return true;
				}else if(mktFilterListView != null && mktFilterListView.isShowing()){
					mktFilterListView.hide();
				}
				else{
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(getResources().getString(R.string.quote_logout_question))	    			
					.setPositiveButton(getResources().getString(R.string.dialog_button_yes), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
							try{
								dialog.dismiss();
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
							}catch(Exception e){
								e.printStackTrace();
							}
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
							
							/* Mary@20120803 - encapsulate logout execution and enhance cache clean
							AppConstants.selectedAcc = null;
	
							((StockApplication) QuoteActivity.this.getApplication()).setPortfolioThread(null);
							((StockApplication) QuoteActivity.this.getApplication()).setRefreshThread(null);
							((StockApplication) QuoteActivity.this.getApplication()).setDoubleLoginThread(null);
							((StockApplication) QuoteActivity.this.getApplication()).setTradeStatusThread(null);
							((StockApplication) QuoteActivity.this.getApplication()).setQCAliveThread(null);
	
							loginThread.stopRequest();
							DefinitionConstants.stopAllThread();
	
							SharedPreferences.Editor editor = sharedPreferences.edit();
							editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
							editor.commit();
	
							new logout().execute();
	
							AppConstants.mapQCIp.clear();
							Intent intent = new Intent();
							intent.putExtra(ParamConstants.REVERT_LOGIN_TAG, true);
							intent.setClass(QuoteActivity.this, LoginActivity.class);
							startActivity(intent);
							*/
							new logout().execute();
						}
					})
					.setNegativeButton(getResources().getString(R.string.dialog_button_no), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
					AlertDialog alert = builder.create();
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! this.isFinishing() )
						alert.show();
					
					return true; 
				}
			}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		return super.onKeyDown(keyCode, event);
	} 
	
	private class logout extends AsyncTask<Void,Void,String>{
		@Override
		protected String doInBackground(Void... params) {
			// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.10
			AppConstants.hasDisclaimerShown = false;
			String atpUserParam 			= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			
			Map<String, String> parameters	= new HashMap<String, String>();
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			try {
				AtpConnectUtil.tradeLogout(parameters);
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		// Added by Mary@20120803 - encapsulate logout execution and enhance cache clean - START
		@Override
		protected void onPostExecute(String param){
			logout(QuoteActivity.this);
		}
		// Added by Mary@20120803 - encapsulate logout execution and enhance cache clean - END
	}

	private void refreshStockData(List<StockSymbol> changedSymbols) {
		System.out.println("debug==refreshStockData()");
		//Added by Thinzar@20140723 - Change_Request-20140701, Req #6 - START
		if(tableLayout.getChildCount() == 0 && changedSymbols != null) {	
			if(!isTableConstructed)
				QuoteActivity.this.constructTableView();
			else
				QuoteActivity.this.refreshTableView();
		}
		//Added by Thinzar@20140723 - END
		
		try {
			// Added by Mary@20120807 - avoid nullpointerException
			/* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
			if(stockSymbols == null || changedSymbols == null)
			*/
			if(stockSymbols == null || changedSymbols == null || (stockSymbols != null && changedSymbols != null && stockSymbols.size() != changedSymbols.size() ) )
				return;
			
			int id = 0;
			for (Iterator<StockSymbol> itr=changedSymbols.iterator(); itr.hasNext(); ) {
				final StockSymbol symbol	= itr.next();
				StockSymbol current			= stockSymbols.get(id);
				// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
				int intLotSize				= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? symbol.getIntLotSize() : 1;

				/* Mary@20121003 - Fixes_Request-20120815, ReqNo.3
				if( symbol.getSymbolCode().equals( current.getSymbolCode() ) ){
					if( symbol.checkDiscrepancy(current) ){
				*/
				if( symbol.getSymbolCode().equals( current.getSymbolCode() ) && symbol.checkDiscrepancy(current) ){
					
					if( symbol.getLastDone() != current.getLastDone() || symbol.getTotalTrade()!=current.getTotalTrade() ){
						final TextView priceLabel 	= priceLabels.get(id);
						Button changeButton 		= changeButtons.get(id);
						
						/* SET TEXT */
						//Fixes_Request-20160101, ReqNo.3
						/*
						if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK")|| AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
							priceLabel.setText( FormatUtil.formatPrice2(symbol.getLastDone(), "-") );
						else
							priceLabel.setText( FormatUtil.formatPrice(symbol.getLastDone(), "-") );
						 */
						priceLabel.setText(FormatUtil.formatPrice(symbol.getLastDone(), "-", selectedExchangeCode));
						
						if(isDisplayPercentage){
							changeButton.setText(FormatUtil.formatPricePercent(symbol.getChangePercent()));								
						}else{
							//Fixes_Request-20160101, ReqNo.3
							/*
							if(AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK")|| AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD")){
								changeButton.setText(FormatUtil.formatPriceChange2(symbol.getPriceChange()));
							}else{
								changeButton.setText(FormatUtil.formatPriceChange(symbol.getPriceChange()));
							}
							*/
							changeButton.setText(FormatUtil.formatPriceChangeByExchange(symbol.getPriceChange(), selectedExchangeCode));
						}
						
						/* SET COLOR */
						float gap 					= symbol.getLastDone() - current.getLastDone();
						if(gap > 0){
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							priceLabel.setBackgroundColor(LayoutSettings.colorGreen);
							priceLabel.setTextColor(Color.BLACK);
							*/
							priceLabel.setBackgroundColor( getResources().getColor(R.color.UpStatus_BgColor) );
							priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_FgColor) );
						}else if (gap < 0){
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							priceLabel.setBackgroundColor(LayoutSettings.colorRed);
							priceLabel.setTextColor(Color.WHITE);
							*/
							priceLabel.setBackgroundColor( getResources().getColor(R.color.DownStatus_BgColor) );
							priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_FgColor) );
						/* Mary@20120905 - Fixes_Request-20120815, ReqNo.8
						}else if( gap == 0 && symbol.getTotalTrade() != current.getTotalTrade() ){
						*/
						}else if( gap == 0 && symbol.getVolume() != current.getVolume() ){
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							priceLabel.setBackgroundColor(LayoutSettings.colorYellow);
							priceLabel.setTextColor(Color.BLACK);
							*/
							priceLabel.setBackgroundColor( getResources().getColor(R.color.DiffTtlStatus_BgColor) );
							priceLabel.setTextColor( getResources().getColor(R.color.DiffTtlStatus_FgColor) );
						}

						/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
						if(symbol.getPriceChange()<0)
							changeButton.setBackgroundResource(LayoutSettings.btnRed);
						else
							changeButton.setBackgroundResource(LayoutSettings.btnGreen);
						*/
						changeButton.setBackgroundResource( (symbol.getPriceChange()<0) ? LayoutSettings.btnRed : LayoutSettings.btnGreen );
						
						/* SET UPDATER */
						priceLabel.postDelayed(new Runnable() {
							public void run() {
								priceLabel.setBackgroundColor(Color.TRANSPARENT);
								if(symbol.getPriceChange() > 0)
									/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
									priceLabel.setTextColor(LayoutSettings.colorGreen);
									*/
									priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
								else if (symbol.getPriceChange() < 0)
									/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
									priceLabel.setTextColor(LayoutSettings.colorRed);
									*/
									priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
								else
									/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
									priceLabel.setTextColor(Color.BLACK);
									*/
									priceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
							}
						}, 1500);
					}
					current.setLastDone(symbol.getLastDone());
					
					
					if( symbol.getVolume() != current.getVolume() ){
						current.setVolume(symbol.getVolume());
						TextView volumeLabel = volumeLabels.get(id);
						/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
						/* Mary@20120911 - Change_Request-20120719, ReqNo.12
						volumeLabel.setText(FormatUtil.formatLong(symbol.getVolume()));
						/
						/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
						volumeLabel.setText( FormatUtil.formatLong( symbol.getVolume() / intLotSize ) );
						/
						volumeLabel.setText( FormatUtil.formatLong( symbol.getVolume() / intLotSize, "-" ) );
						*/
						volumeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (symbol.getVolume() / intLotSize), "-" ) );
					}
					
					
					if( symbol.getValue() != current.getValue() ){
						current.setValue(symbol.getValue());
						TextView valueLabel = valueLabels.get(id);
						/*
						if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK")|| AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
							valueLabel.setText(FormatUtil.formatDouble2(symbol.getValue(), "-"));
						else
							*/
						valueLabel.setText(FormatUtil.formatDouble(symbol.getValue(), "-"));
					}
					
					
					if( symbol.getHigh() != current.getHigh() ){
						current.setHigh(symbol.getHigh());
						TextView highLabel = highLabels.get(id);
						/*
						if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK")|| AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
							highLabel.setText(FormatUtil.formatFloat2(symbol.getHigh(), "-"));
						else
							highLabel.setText(FormatUtil.formatFloat(symbol.getHigh(), "-"));
						*/
						highLabel.setText( FormatUtil.formatPrice(symbol.getHigh(), "-", selectedExchangeCode));
					}
					
					
					if( symbol.getLow() != current.getLow() ){
						current.setLow(symbol.getLow());
						TextView lowLabel = lowLabels.get(id);
						/*
						if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK")|| AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
							lowLabel.setText(FormatUtil.formatFloat2(symbol.getLow(), "-"));
						else
							lowLabel.setText(FormatUtil.formatFloat(symbol.getLow(), "-"));
						*/
						lowLabel.setText(FormatUtil.formatPrice(symbol.getLow(), "-", selectedExchangeCode));
					}
					
					
					if( symbol.getTotalTrade()!=current.getTotalTrade() ){
						current.setTotalTrade(symbol.getTotalTrade());
						TextView tradeLabel = tradeLabels.get(id);
						/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
						tradeLabel.setText(FormatUtil.formatLong(symbol.getTotalTrade()));
						*/
						tradeLabel.setText(FormatUtil.formatLong(symbol.getTotalTrade(), "-"));
					}
				}  
				id++;
			}
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
	public void repaintUI(){
		( (RelativeLayout)findViewById(R.id.rl_sq_TopNavBar) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBgImg() );
		//( (Button)findViewById(R.id.sectorButton) ).setBackgroundResource(  AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );	//commented out by Thinzar, Change_Request-20160101, ReqNo.3
	}
	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END

	public void sectorSelectedEvent(Sector selection) {
		selectedSector = selection;

		sectorButton.setText((selectedSector.getParentSector()==null)? selectedSector.getSectorName() : selectedSector.getParentSector().getSectorName() + "-" + selectedSector.getSectorName());

		this.sortImageByType(ParamConstants.IMAGESORT);
		
		//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
		displayTotalCount();
		/* Mary@20130927 - Move to Async Task to avoid NetworkOnMainThreadException
		displayCurrentpage();
		*/
		//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
		
		//Added by Thinzar, Change_Request-20140901, ReqNo. 12
		this.strSelectedFilterMktCode	= null;
		this.strSelectedFilterMktType	= null;
		this.resetMarketButton();
	}

	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
	private void displayCurrentpage(){
		int lastpage 	= totalCount/20;
		int intFromPage	= 1 + ParamConstants.PAGE_NUMBER * 20;
		int intToPage	= ParamConstants.PAGE_NUMBER*20;
		if(totalCount%20 >0 && lastpage == ParamConstants.PAGE_NUMBER){
			intToPage += totalCount%20;
		}else{
			intToPage += 20;
		 }
		rv.headTotalCountView.setText(getResources().getString(R.string.pull_refresh_current_record) + ":[" + intFromPage + "-" + intToPage + "]" + getResources().getString(R.string.pull_refresh_total_record) + ":" + totalCount);
		rv.TotalCountView.setText(getResources().getString(R.string.pull_refresh_current_record) + ":[" + intFromPage  + "-" + intToPage + "]" + getResources().getString(R.string.pull_refresh_total_record) + ":" + totalCount);
	}

	private void displayTotalCount(){
		/* Mary@20130312 - Fixes_Request-20130108, ReqNo.19
		//if(selectedSector.getSectorCode().equals("10")||AppConstants.DEFAULT_EXCHANGE_CODE.equals("MY")){
		if( AppConstants.alAllSectorCode.contains( selectedSector.getSectorCode() ) || AppConstants.DEFAULT_EXCHANGE_CODE.equals("MY") ){
		*/
		/* Mary@20130926 - Fixes_Request-20130711, ReqNo.19
		if( AppConstants.alAllSectorCode.contains( selectedSector.getSectorCode() ) || AppConstants.getListDerivativesExchgCode().contains( AppConstants.DEFAULT_EXCHANGE_CODE) ){
		*/
		if( AppConstants.getListDerivativesExchgCode().contains( AppConstants.DEFAULT_EXCHANGE_CODE) ){
			/* Mary@20130927 - Move to Async Task to avoid NetworkOnMainThreadException
			Map<String, String> parameters = new HashMap<String, String>();
			try {
				parameters.put(ParamConstants.SECTORCODE_TAG, selectedSector.getSectorCode());
				parameters.put(ParamConstants.EXCHANGECODE_TAG, AppConstants.DEFAULT_EXCHANGE_CODE);
				
				/* Mary@20130312 - Fixes_Request-20120724, ReqNo.2 - START
				int response2 	= QcHttpConnectUtil.imageCount(parameters);
				/
				String response =  QcHttpConnectUtil.imageCount(parameters);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response =  QcHttpConnectUtil.imageCount(parameters);
				}
				intRetryCtr	= 0;
				
				if(response == null)
					response = "0";
				
				int response2	= Integer.parseInt(response);
				// Mary@20130312 - Fixes_Request-20120724, ReqNo.2 - END
				totalCount		= response2;
				rv.totalCount 	= response2;
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				if( parameters != null)
					parameters.clear();
				parameters = null;
			}
			*/
			new getSectorImageCount().execute();
		}else{
			totalCount 		= selectedSector.getIndexCount();
			rv.totalCount 	= selectedSector.getIndexCount();
			
			// Added by Mary@20130927 - Move to Async Task to avoid NetworkOnMainThreadException
			displayCurrentpage();
		}

	}
	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
	
	// Added by Mary@20130927 - Move to Async Task to avoid NetworkOnMainThreadException - START
	public class getSectorImageCount extends AsyncTask<Void, Void, String>{
		private AsyncTask<Void, Void, String> updateTask;
		
		@Override
		protected void onPreExecute(){
			try{
				super.onPreExecute();
				
				updateTask		= this;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		@Override 
		protected String doInBackground(Void... voids){
			Map<String, String> parameters = new HashMap<String, String>();
			try {
				parameters.put(ParamConstants.SECTORCODE_TAG, selectedSector.getSectorCode());
				parameters.put(ParamConstants.EXCHANGECODE_TAG, AppConstants.DEFAULT_EXCHANGE_CODE);
				
				String strResp =  QcHttpConnectUtil.imageCount(parameters);
				while(intRetryCtr < AppConstants.intMaxRetry && strResp == null){
					intRetryCtr++;
					strResp =  QcHttpConnectUtil.imageCount(parameters);
				}
				intRetryCtr	= 0;
				
				if(strResp == null)
					strResp = "0";
				
				return strResp;
			} catch (Exception e) {
				e.printStackTrace();
				
				return "0";
			}finally{
				if( parameters != null)
					parameters.clear();
				parameters = null;
			}
		}
		
		@Override
		protected void onPostExecute(String strResp){
			try{
				super.onPostExecute(strResp);
				
				int intResp	= Integer.parseInt( (strResp == null || (strResp != null && strResp.trim().length() == 0) ) ? "0" : strResp);
				
				totalCount		= intResp;
				rv.totalCount 	= intResp;
				
				displayCurrentpage();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	// Added by Mary@20130927 - Move to Async Task to avoid NetworkOnMainThreadException - END
	
	
	private void doSector() {
		if (sectorListView.isShowing()) {
			sectorListView.hide();
		} else {
			if( mainMenu.isShowing() )
				mainMenu.hide();
			sectorListView.show(findViewById(R.id.moverLayout), selectedSector);
		}
	}

	protected void onMenuShownEvent() {
		if( sectorListView.isShowing() )
			sectorListView.hide();
		
		if(mktFilterListView != null && mktFilterListView.isShowing())
			mktFilterListView.hide();
	}

	private void populateSectorList() {
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
		if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
			new populateSector().execute();
	}
	
	private class populateSector extends AsyncTask<Void, Void, String>{
		private AsyncTask<Void, Void, String> updateTask; 
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
			System.out.println("debug==populateSector()");
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				updateTask = this;
				QuoteActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! QuoteActivity.this.isFinishing() )
					QuoteActivity.this.dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		
		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try{
				String exchangeCode = null;

				if( AppConstants.DEFAULT_EXCHANGE_CODE.indexOf("D") > 0 )
					exchangeCode = AppConstants.DEFAULT_EXCHANGE_CODE.substring(0, AppConstants.DEFAULT_EXCHANGE_CODE.indexOf("D") );
				else
					exchangeCode = AppConstants.DEFAULT_EXCHANGE_CODE;
				
				if( selectedExchange.getTrdExchgCode().equals(exchangeCode) ){
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					Map parameters = new HashMap();
					parameters.put(ParamConstants.USERPARAM_TAG, userParam);
					String response = QcHttpConnectUtil.sectorFast(parameters, AppConstants.DEFAULT_EXCHANGE_CODE);
					sectors = QcMessageParser.parseSector(response);
					 */
					String response = QcHttpConnectUtil.sectorFast(AppConstants.DEFAULT_EXCHANGE_CODE);
					while(intRetryCtr < AppConstants.intMaxRetry && response == null){
						intRetryCtr++;
						response = QcHttpConnectUtil.sectorFast(AppConstants.DEFAULT_EXCHANGE_CODE);
					}
					intRetryCtr	= 0;

					// NOTE : during test found there is possible of empty string return and activity are not effected, so no need to throw error message even after retry still null
					if(response != null){
						sectors = QcMessageParser.parseSector(response);
					}else{ 
						if(sectors == null)
							sectors	= new ArrayList<Sector>();
					}
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					
					List<Sector> loginSectors	= selectedExchange.getSectors();
					/* Mary@20130401 - Fixes_Request-20130314, ReqNo.8 - START
					Sector allStock 	= (Sector) loginSectors.get(0);
					*/
					Sector allStock				= new Sector();
					if( loginSectors == null || ( loginSectors != null && loginSectors.size() == 0 ) ){
						allStock.setSectorCode( AppConstants.DEFAULT_SECTOR_CODE );
						/* Mary@20131004 - eliminate unnecessary global static variable
						allStock.setSectorName( AppConstants.DEFAULT_SECTOR_NAME );
						*/
						allStock.setSectorName( DEFAULT_SECTOR_NAME );
					}else{
						allStock = loginSectors.get(0);
					}
					// Mary@20130401 - Fixes_Request-20130314, ReqNo.8 - END
					
					// Added by Mary@20130927 - Fixes_Request-20130711, ReqNo.19 - START
					for(int i=0; i<sectors.size(); i++){
						Sector tempSector			= sectors.get(i);
						if( tempSector != null && tempSector.getSectorCode() != null 
								&& ( tempSector.getSectorCode().equalsIgnoreCase("10000") || tempSector.getSectorCode().equals(AppConstants.DEFAULT_SECTOR_CODE) )
								&& ( tempSector.getIndexCount() > allStock.getIndexCount()  )
						){
							sectors.remove(i);
							
							allStock.setParentCode( tempSector.getParentCode() );
							allStock.setIndexCount( tempSector.getIndexCount() );
							
							tempSector = null;
							
							break;
						}
					}
					// Added by Mary@20130927 - Fixes_Request-20130711, ReqNo.19 - END
					
					sectors.add(0, allStock);
				}else{
					sectors = selectedExchange.getSectors();
				}
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				updateTask.cancel(true);
				new populateSector().execute();		// NOTE : might cause infinite loop if FailedAuthenicationException countinuously thrown
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			*/
			}catch(Exception e){
				e.printStackTrace();
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			return null;
		}
		
		@Override
		protected void onPostExecute(String data){					
			super.onPostExecute(data);
			
			/* Kw@20120817 - Change Request-20120719, ReqNo.9
			selectedSector = (Sector) selectedExchange.getSectors().get(0);
			*/
			try {
				selectedSector = sectors.get(0);
				
				sectorListView.setSectorList(sectors);
				
				//Added by Thinzar@20141024, Change-Request-20140901, ReqNo 12, to reset when switched exchange
				strSelectedFilterMktType 		= null;
				strSelectedFilterMktCode		= null;
				populateFilterMarketButton();
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				new stockRefresh().execute();
		}
	}

	public void sortImageByType(int imageSortType) {
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
		if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) ){
			// Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
			ParamConstants.PAGE_NUMBER=0;
			displayCurrentpage(); 
			// Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
			new sortImageByType().execute(imageSortType);
		}
	}

	private class sortImageByType extends AsyncTask<Integer, Void, String>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		/* Mary@20130529 - comment unused variable
		private AsyncTask<Integer, Void, String> updateTask = null;
		*/
		
		@Override
		protected void onPreExecute(){
			/* Mary@20130529 - comment unused variable
			updateTask 					= this;
			*/
			QuoteActivity.this.dialog 	= ProgressDialog.show(QuoteActivity.this, "", getResources().getString(R.string.dialog_progress_loading), true);
		}

		@Override
		protected String doInBackground(Integer... params) {
			try{
				int imageSortType	= params[0];
	
				Map<String, String> parameters		= new HashMap<String, String>();
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				parameters.put(ParamConstants.USERPARAM_TAG, userParam);
				*/
				parameters.put(ParamConstants.SECTORCODE_TAG, selectedSector.getSectorCode());
				/* Mary@20130529 - comment unused variable
				StockApplication application = (StockApplication) QuoteActivity.this.getApplication();
				*/
				parameters.put(ParamConstants.EXCHANGECODE_TAG, AppConstants.DEFAULT_EXCHANGE_CODE);
				
				//Added by Thinzar@20141029, Change_Request-20140901, ReqNo.12
				parameters.put(ParamConstants.MARKETCODE_TAG, strSelectedFilterMktCode);
				
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				try {
					String response = QcHttpConnectUtil.imageSort(parameters, imageSortType);
					if (response == null) {
						return null;
					}else{
						return response;
					}
				} catch (FailedAuthenicationException e) {
					e.printStackTrace();
					updateTask.cancel(true);
					QuoteActivity.this.sortImageByType(imageSortType); // might caused infinite loop if FailedAuthenicationException continuously thrown
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				*/
				try {
					boolean isDerivativeExchg	= SystemUtil.isDerivative(AppConstants.DEFAULT_EXCHANGE_CODE);		//Added by Thinzar, Change_Request-20150401, ReqNo.2
					String response = QcHttpConnectUtil.imageSort(parameters, imageSortType, isDerivativeExchg);	//Edited by Thinzar, Change_Request-20150401, ReqNo.2
					while(intRetryCtr < AppConstants.intMaxRetry && response == null){
						intRetryCtr++;
						response = QcHttpConnectUtil.imageSort(parameters, imageSortType, isDerivativeExchg);		//Edited by Thinzar, Change_Request-20150401, ReqNo.2
					}
					intRetryCtr	= 0;
					
					if(response == null){				
						//edited by Thinzar@20140721 - Change_Request-20140701, Req #6 - START
						if(!isNetworkAvailable())	throw new Exception(ErrorCodeConstants.NETWORK_UNAVAILABLE);
						else 						throw new Exception(ErrorCodeConstants.FAIL_GET_SORTED_DETAIL);
						//edited by Thinzar@20140721 - END
					}
					else if (response == "")
						throw new Exception(ErrorCodeConstants.NO_DATA_MSG);
					
					return response;
				}catch(Exception e){
					exception = e;
				}
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END;
			// Added by Mary@20130606 - app process error - START
			}catch(Exception e){
				exception = new Exception(ErrorCodeConstants.FAIL_PROCESS_ERROR);
			}
			// Added by Mary@20130606 - app process error - END

			return null;
		}

		@Override 
		protected void onPostExecute(String response){
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				if(response!=null)
					stockSymbols = QcMessageParser.parseStockSymbol(response);
				else
					stockSymbols = null;
				*/
				stockSymbols = (response!=null) ? QcMessageParser.parseStockSymbol(response) : null;
				
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
				if(QuoteActivity.this.dialog != null && QuoteActivity.this.dialog.isShowing() )
					QuoteActivity.this.dialog.dismiss();
	
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					if( idleThread != null && idleThread.isExpired() )
						IdleEvent(false);
					
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							// Added by Mary@20121128 - Fixes_Request-20121102, ReqNo.5 - START
							if(isTableConstructed)
								tableLayout.removeAllViews();
							// Added by Mary@20121128 - Fixes_Request-20121102, ReqNo.5 - END
						}
					});
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! QuoteActivity.this.isFinishing() )
						alertException.show();
					
					exception = null;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
				/* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
				refreshThread = ((StockApplication) QuoteActivity.this.getApplication()).getRefreshThread();
				*/
			
				if(stockSymbols != null) {
					// Added by Mary@20121025 - Fixes_Request-20121023, ReqNo.2 - START
					//Edited by Thinzar@20150515, Change_Request-20150401, ReqNo. 2
					if( AppConstants.DEFAULT_EXCHANGE_CODE.equals("MY") )
						stockSymbols	= AppConstants.sortImageBaseOnDerivativeRules(stockSymbols, imageSortType);
					// Added by Mary@20121025 - Fixes_Request-20121023, ReqNo.2 - END
							
					if(!isTableConstructed)
						QuoteActivity.this.constructTableView();
					else
						QuoteActivity.this.refreshTableView();
				}else{
					if(isTableConstructed)
						tableLayout.removeAllViews();
				}
				
				// Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
				if(refreshThread == null)
					refreshThread = ((StockApplication) QuoteActivity.this.getApplication()).getRefreshThread();
				
				refreshThread.setSymbolCodes( SystemUtil.getSymbolCodes(stockSymbols) );
				refreshThread.setRegisteredListener(QuoteActivity.this);
				
				if( refreshThread.isPaused() )
					refreshThread.resumeRequest();
				// Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END
				
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	//Fixes_Request-20161101, ReqNo.1 - START
	private void resumeRefreshStockThread(){
		// Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
		if(refreshThread == null)
			refreshThread = ((StockApplication) QuoteActivity.this.getApplication()).getRefreshThread();
		
		refreshThread.setSymbolCodes( SystemUtil.getSymbolCodes(stockSymbols) );
		refreshThread.setRegisteredListener(QuoteActivity.this);
		
		if( refreshThread.isPaused() )
			refreshThread.resumeRequest();
		// Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END
	}
	//Fixes_Request-20161101, ReqNo.1 - END

	private class stockRefresh extends AsyncTask<View, Void, String> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		/* Mary@20130529 - comment unused variable
		private AsyncTask<View, Void, String> updateTask = null;
		*/
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && idleThread.isExpired() )
					this.cancel(true);
				// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
				/* Mary@20130529 - comment unused variable
				updateTask = this;
				*/
				// Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
				QuoteActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! QuoteActivity.this.isFinishing() )
					QuoteActivity.this.dialog.show();
				// Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(View... params) {
			String response	= null;
			
			try{
				Map<String, String> parameters	= new HashMap<String, String>();
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				parameters.put(ParamConstants.USERPARAM_TAG, userParam);
				*/
				parameters.put(ParamConstants.SECTORCODE_TAG, selectedSector.getSectorCode());
				parameters.put(ParamConstants.EXCHANGECODE_TAG, AppConstants.DEFAULT_EXCHANGE_CODE);
				//parameters.put(ParamConstants.MARKETCODE_TAG, AppConstants.DEFAULT_FEED_MARKET_CODE);			//Added by thinzar@20141027, Change_Request-20140901, ReqNo.12
				parameters.put(ParamConstants.MARKETCODE_TAG, strSelectedFilterMktCode);			//Edited, Fixes_Request-20161101, ReqNo.1
				
				try {
					boolean isDerivativeExchg	= SystemUtil.isDerivative(AppConstants.DEFAULT_EXCHANGE_CODE);		//Added by Thinzar, Change_Request-20150401, ReqNo.2
					response = QcHttpConnectUtil.imageSort(parameters, imageSortType, isDerivativeExchg);			//Edited by Thinzar, Change_Request-20150401, ReqNo.2
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					while(intRetryCtr < AppConstants.intMaxRetry && response == null){
						intRetryCtr++;
						response = QcHttpConnectUtil.imageSort(parameters, imageSortType, isDerivativeExchg);		//Edited by Thinzar, Change_Request-20150401, ReqNo.2
					}
					intRetryCtr	= 0;
					
					if(response == null){			
						//edited by Thinzar@20140721 - Change_Request-20140701, Req #6 - START
						if(!isNetworkAvailable())	throw new Exception(ErrorCodeConstants.NETWORK_UNAVAILABLE);
						else 						throw new Exception(ErrorCodeConstants.FAIL_GET_SORTED_DETAIL);
						//edited by Thinzar@20140721 - END
					}
					else if(response.trim() == "")
						throw new Exception(ErrorCodeConstants.NO_DATA_MSG);
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				} catch (FailedAuthenicationException e) {
					// TODO Auto-generated catch block
					updateTask.cancel(true);
					new stockRefresh().execute(); // might caused infinite loop if FailedAuthenicationException continuously thrown
					e.printStackTrace();
				}
				*/
				}catch(Exception e){
					exception = e;
				}
			// Added by Mary@20130606 - app process error - START
			}catch(Exception e){
				exception = new Exception(ErrorCodeConstants.FAIL_PROCESS_ERROR);
			}
			// Added by Mary@20130606 - app process error - END
			
			return response;
		}

		@Override
		protected void onPostExecute(final String response) {
			super.onPostExecute(response);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( QuoteActivity.this.dialog != null && QuoteActivity.this.dialog.isShowing() )
					QuoteActivity.this.dialog.dismiss();
	
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					if( idleThread != null && idleThread.isExpired() )
						IdleEvent(false);
					
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							// Added by Mary@20121128 - Fixes_Request-20121102, ReqNo.5 - START
							if(isTableConstructed)
								tableLayout.removeAllViews();
							// Added by Mary@20121128 - Fixes_Request-20121102, ReqNo.5 - END
						}
					});
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! QuoteActivity.this.isFinishing() )
						alertException.show();	
					
					exception = null;
					
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				//imageSortType 			= CommandConstants.IMAGE_SORT_MOVER;
	
				/* Mary@20121127 - Fixes_Request-20120815, ReqNo.3
				if(response!=null)
					stockSymbols = QcMessageParser.parseStockSymbol(response);
				*/
				stockSymbols = (response!=null) ? QcMessageParser.parseStockSymbol(response) : null;
	
				if (stockSymbols != null) {
					// Added by Mary@20121025 - Fixes_Request-20121023, ReqNo.2 - START
					/* Mary@20130312 - Fixes_Request-20130108, ReqNo.19
					if( AppConstants.DEFAULT_EXCHANGE_CODE.equals("MY") )
					*/
					//Amended by Thinzar@20150515, Change_Request-20150401, ReqNo.2
					if( AppConstants.getListDerivativesExchgCode().contains(AppConstants.DEFAULT_EXCHANGE_CODE) )
						stockSymbols	= AppConstants.sortImageBaseOnDerivativeRules(stockSymbols, imageSortType);
					// Added by Mary@20121025 - Fixes_Request-20121023, ReqNo.2 - END
					
					if(!isTableConstructed)
						QuoteActivity.this.constructTableView();
					else
						QuoteActivity.this.refreshTableView();
				}else{
					if(isTableConstructed)
						/* Mary@20120807 - avoid nullpointerException
						int rowCount = tableLayout.getChildCount();
						if (rowCount != stockSymbols.size()) {
							tableLayout.removeAllViews();
						}
						*/
						tableLayout.removeAllViews();
				}
	
				/* Kw@20120817 - Change Request-20120719, ReqNo.9
				ScrollView scroll		= (ScrollView) findViewById(R.id.scrollQuote);
				scroll.fullScroll(ScrollView.FOCUS_UP);
				*/
	
				sectorButton.setText((selectedSector.getParentSector()==null)? selectedSector.getSectorName() :	selectedSector.getParentSector().getSectorName() + "-" + selectedSector.getSectorName());
				String[] symbolCodes	= SystemUtil.getSymbolCodes(stockSymbols);
	
				refreshThread 			= ((StockApplication) QuoteActivity.this.getApplication()).getRefreshThread();
				if (refreshThread == null) {
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
					if(AppConstants.hasLogout){
						finish();
					}else{
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
						int rate		= sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
						/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
						refreshThread = new RefreshStockThread(userParam, symbolCodes, rate);
						*/
						refreshThread	= new RefreshStockThread( AppConstants.QCdata.getStrUserParam(), symbolCodes, rate);
						refreshThread.start();	
						((StockApplication) QuoteActivity.this.getApplication()).setRefreshThread(refreshThread);
					}
				/* Mary@20130924 - Fixes_Request-20130711, ReqNo.22
				}else{
				*/
				/* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
				}else if( refreshThread.isPaused() ){
					refreshThread.resumeRequest();
					refreshThread.setSymbolCodes(symbolCodes);
				*/
				}
				refreshThread.setSymbolCodes(symbolCodes);
				refreshThread.setRegisteredListener(QuoteActivity.this);
				
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
				if( refreshThread.isPaused() ){
					refreshThread.resumeRequest();
				}
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END
				
				// Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
				displayTotalCount();
				/* Mary@20130927 - Move to Async Task to avoid NetworkOnMainThreadException
				displayCurrentpage();
				*/
				// Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
			
			handleExternalIntentsRedirect();
		}
	}
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.8 - START
	private void handleExternalIntentsRedirect(){
		
		if(AppConstants.brokerConfigBean.isEnableIBillionaire){
			if(isReturnVerificationToiBillionaire){
				isReturnVerificationToiBillionaire	= false;
				
				showAlertVerifiedIBillionaire(getResources().getString(R.string.verified_msg_ibillionaire));
				
			}else if(isRedirectiBillionaire && stockCodeFromiBillionaire.length() > 0 && exchgCodeFromiBillionaire.length() > 0){
				if(AppConstants.noTradeClient){
        			showAlert(getResources().getString(R.string.ibillionaire_no_trading_account));
        			
        		}else if(!DefinitionConstants.getTrdExchangeCode().contains(exchgCodeFromiBillionaire)){
        			showAlert(getResources().getString(R.string.ibillionaire_exchange_not_subscribed) + stockCodeFromiBillionaire + "." + exchgCodeFromiBillionaire);
        		}
        		else{
					String qcExchangeCode			= SystemUtil.getQcExchangeCode(exchgCodeFromiBillionaire);	//to support delay feed
					String symbolFromiBillionaire	= stockCodeFromiBillionaire + "." + qcExchangeCode;
					
					String[] parameters	= {symbolFromiBillionaire, Integer.toString(TRADE_TASK_ID)};
					//isRedirectiBillionaire = false;
					new FetchStockInfoAndDoNextTask().execute(parameters);
        		}
			}
		}
		
		if(AppConstants.brokerConfigBean.isEnableSgxAppIntegration() && isRedirectSgx){
			if(AppConstants.noTradeClient){
    			showAlert(getResources().getString(R.string.ibillionaire_no_trading_account));    			
    		}else{
    			String symbolFromSGX = sgxStockCode + "." + "SG";
    			String[] parameters	= {symbolFromSGX, Integer.toString(TRADE_TASK_ID)};
    			
				new FetchStockInfoAndDoNextTask().execute(parameters);
    		}
		}
		
		application.setExternalIntentUri(null);
	}
	//Added by Thinzar, Change_Request-20160722, ReqNo.8 - END

	private void refreshTableView() {
		// Added by Mary@20120807 - avoid NullPointerException
		if(stockSymbols == null)
			return;
		
		int rowCount = tableLayout.getChildCount();
		if (rowCount != stockSymbols.size()) {
			tableLayout.removeAllViews();
			this.constructTableView();
		}else{
			int id = 0;
			for (Iterator<StockSymbol> itr=stockSymbols.iterator(); itr.hasNext(); ) {
				StockSymbol symbol 	= itr.next();
				// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
				int intLotSize		= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? symbol.getIntLotSize() : 1;
				
				TableRow tableRow 	= (TableRow) tableLayout.getChildAt(id);
				View rowView 		= tableRow.getChildAt(0);

				TextView nameLabel 	= nameLabels.get(id);
				/* Mary@20130718 - Change_Request-20130424, ReqNo.13
				nameLabel.setText(symbol.getStockName().trim());
				*/
				nameLabel.setText( symbol.getStockName(false).trim() );

				TextView priceLabel = priceLabels.get(id);
				/*
				if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
					priceLabel.setText( FormatUtil.formatPrice2(symbol.getLastDone(), "-") );
				else
					priceLabel.setText( FormatUtil.formatPrice(symbol.getLastDone(), "-") );
				 */
				priceLabel.setText(FormatUtil.formatPrice(symbol.getLastDone(), "-", selectedExchangeCode));
				
				if (symbol.getPriceChange() > 0) 
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					priceLabel.setTextColor(LayoutSettings.colorGreen);
					*/
					priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
				else if (symbol.getPriceChange() < 0)
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					priceLabel.setTextColor(LayoutSettings.colorRed);
					*/
					priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
				else
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					priceLabel.setTextColor(Color.BLACK);
					*/
					priceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
				

				Button changeButton = changeButtons.get(id);            
				if(isDisplayPercentage){
					changeButton.setText(FormatUtil.formatPricePercent(symbol.getChangePercent()));								
				}else{
					//Fixes_Request-20160101, ReqNo.3
					/*
					if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
						changeButton.setText(FormatUtil.formatPriceChange2(symbol.getPriceChange()));
					else
						changeButton.setText(FormatUtil.formatPriceChange(symbol.getPriceChange()));
					*/
					changeButton.setText(FormatUtil.formatPriceChangeByExchange(symbol.getPriceChange(), selectedExchangeCode));
				}

				/* Mary@20120912 - Fixes_Request-20120815, ReqNo.3
				if (symbol.getPriceChange()<0) {
					changeButton.setBackgroundResource(LayoutSettings.btnRed);
				} else {
					changeButton.setBackgroundResource(LayoutSettings.btnGreen);
				}
				*/
				changeButton.setBackgroundResource( symbol.getPriceChange()<0 ? LayoutSettings.btnRed : LayoutSettings.btnGreen );

				LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.rowLayout);
				
				if(rowLayout.getChildCount() == 2)
					rowLayout.removeViewAt(1);

				/* Mary@20120912 - Fixes_Request-20120815, ReqNo.3
				if ((tableRow.getId()+1)%2 != 0) { 
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
				} else {
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
				}
				*/
				//rowLayout.setBackgroundResource( (tableRow.getId() + 1) % 2 != 0 ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light );	//commented for Change_Request-20160101, ReqNo.3
				
				/* Mary@20130718 - Change_Request-20130424, ReqNo.13
				// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - START
				TextView exchgLabel	= exchgLabels.get(id);
				exchgLabel.setText( AppConstants.DEFAULT_EXCHANGE_CODE );
				// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - END
				*/
				
				TextView volumeLabel = volumeLabels.get(id);
				/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
				/* Mary@20120911 - Change_Request20120719, ReqNo.12
				volumeLabel.setText(FormatUtil.formatLong(symbol.getVolume()));
				/
				/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
				volumeLabel.setText( FormatUtil.formatLong(symbol.getVolume() / intLotSize) );
				/
				volumeLabel.setText( FormatUtil.formatLong(symbol.getVolume() / intLotSize, "-") );
				*/
				volumeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (symbol.getVolume() / intLotSize), "-") );

				TextView valueLabel = valueLabels.get(id);
				/*
				if( AppConstants.DEFAULT_EXCHANGE_CODE.equals("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equals("JKD") )
					valueLabel.setText( FormatUtil.formatDouble2( symbol.getValue(), "-" ) );
				else
					valueLabel.setText( FormatUtil.formatDouble( symbol.getValue(), "-" ) );
				*/
				valueLabel.setText( FormatUtil.formatDouble( symbol.getValue(), "-" ) );

				TextView openLabel = openLabels.get(id);
				//Fixes_Request-20160101, ReqNo.3
				/*
				if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
					openLabel.setText( FormatUtil.formatPrice2( symbol.getOpen(), "-" ) );
				else
					openLabel.setText( FormatUtil.formatPrice( symbol.getOpen(), "-" ) );
				*/
				openLabel.setText(FormatUtil.formatPrice(symbol.getOpen(), "-", selectedExchangeCode));
				
				TextView highLabel = highLabels.get(id);
				/*
				if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
					highLabel.setText( FormatUtil.formatPrice2( symbol.getHigh(), "-" ) );
				else
					highLabel.setText( FormatUtil.formatPrice( symbol.getHigh(), "-" ) );
				 */
				highLabel.setText( FormatUtil.formatPrice(symbol.getHigh(), "-", selectedExchangeCode));
				
				TextView lowLabel = lowLabels.get(id);
				/*
				if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
					lowLabel.setText( FormatUtil.formatPrice2( symbol.getLow(), "-" ) );
				else
					lowLabel.setText( FormatUtil.formatPrice( symbol.getLow(), "-" ) );
				*/
				lowLabel.setText(FormatUtil.formatPrice(symbol.getLow(), "-", selectedExchangeCode));
				
				TextView tradeLabel = tradeLabels.get(id);
				/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
				tradeLabel.setText(FormatUtil.formatLong(symbol.getTotalTrade()));
				*/
				tradeLabel.setText( FormatUtil.formatLong(symbol.getTotalTrade(), "-") );

				//            this.setupTableRowOnClick(tableRow, rowLayout);

				id++;
			}
		}
	}

	private void constructTableView() {
		if(changeButtons!=null){
			if(changeButtons.size()!=0)
				changeButtons.clear();
		}else{
			changeButtons = new ArrayList<Button>();
		}

		if(nameLabels!=null){
			if(nameLabels.size()!=0)
				nameLabels.clear();
		}else{
			nameLabels = new ArrayList<TextView>();
		}

		if(priceLabels!=null){
			if(priceLabels.size()!=0)
				priceLabels.clear();
		}else{
			priceLabels = new ArrayList<TextView>();
		}

		/* Mary@20130718 - Change_Request-20130424, ReqNo.13
		// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - START
		if(exchgLabels != null){
			if(exchgLabels.size() != 0)
				exchgLabels.clear();
		}else{
			exchgLabels = new ArrayList<TextView>();
		}
		// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - END
		*/
		
		if(volumeLabels!=null){
			if(volumeLabels.size()!=0)
				volumeLabels.clear();
		}else{
			volumeLabels = new ArrayList<TextView>();
		}
		
		if(valueLabels!=null){
			if(valueLabels.size()!=0)
				valueLabels.clear();
		}else{
			valueLabels = new ArrayList<TextView>();
		}
		
		if(highLabels!=null){
			if(highLabels.size()!=0)
				highLabels.clear();
		}else{
			highLabels = new ArrayList<TextView>();
		}
		
		if(lowLabels!=null){
			if(lowLabels.size()!=0)
				lowLabels.clear();
		}else{
			lowLabels = new ArrayList<TextView>();
		}

		if(tradeLabels!=null){
			if(tradeLabels.size()!=0)
				tradeLabels.clear();
		}else{
			tradeLabels = new ArrayList<TextView>();
		}
		
		if(openLabels!=null){
			if(openLabels.size()!=0)
				openLabels.clear();
		}else{
			openLabels = new ArrayList<TextView>();
		}
		//Commented by Diyana - It caused nullpointerexception
		//tableLayout = (TableLayout) findViewById(R.id.symbolTable);

		int id = 0;

		if(stockSymbols != null) { //Fixes_Request-20170701, ReqNo.8
			for (Iterator<StockSymbol> itr = stockSymbols.iterator(); itr.hasNext(); ) {
				StockSymbol symbol = itr.next();

				final TableRow tableRow = new TableRow(this);
				tableRow.setId(id);
				tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
				tableRow.setWeightSum(1);

				View rowView = this.getLayoutInflater().inflate(R.layout.symbol_row, null);
				final LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.rowLayout);

				TextView nameLabel = (TextView) rowView.findViewById(R.id.nameLabel);
				/* Mary@20130718 - Change_Request-20130424, ReqNo.13
				nameLabel.setText(symbol.getStockName());
				*/
				nameLabel.setText(symbol.getStockName(false));
				nameLabels.add(nameLabel);

				final TextView priceLabel = (TextView) rowView.findViewById(R.id.priceLabel);
				/*
				if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
					priceLabel.setText( FormatUtil.formatPrice2( symbol.getLastDone(), "-" ) );
				else
					priceLabel.setText( FormatUtil.formatPrice( symbol.getLastDone(), "-" ) );
				 */
				//Fixes_Request-20160101, ReqNo.3
				priceLabel.setText(FormatUtil.formatPrice(symbol.getLastDone(), "-", selectedExchangeCode));

				if (symbol.getPriceChange() > 0)
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					priceLabel.setTextColor(LayoutSettings.colorGreen);
					*/
					priceLabel.setTextColor(getResources().getColor(R.color.UpStatus_BgColor));
				else if (symbol.getPriceChange() < 0)
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					priceLabel.setTextColor(LayoutSettings.colorRed);
					*/
					priceLabel.setTextColor(getResources().getColor(R.color.DownStatus_BgColor));
				else
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					priceLabel.setTextColor(Color.BLACK);
					*/
					priceLabel.setTextColor(getResources().getColor(R.color.NoStatus_FgColor));

				priceLabel.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if (idleThread != null && !isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END

						//rowLayout.setBackgroundDrawable(null);	//Change_Request-20160101, ReqNo.3
						selectedRow = rowLayout;

						//rowLayout.setBackgroundColor(Color.CYAN); 	//Change_Request-20160101, ReqNo.3

						int id = tableRow.getId();
						selectedId = id;

						StockSymbol stockSymbol = stockSymbols.get(id);
						//DefinitionConstants.Debug("symbolCode: "+stockSymbol.getSymbolCode());

						Intent intent = new Intent();
						intent.putExtra(ParamConstants.SYMBOLCODE_TAG, stockSymbol.getSymbolCode());

						// Added by Mary - Fixes_Request-20121023, ReqNo.4
						intent.putExtra(ParamConstants.PARAM_STR_RESTART_ACTIVITY, true);
						intent.setClass(QuoteActivity.this, StockActivity.class);
						startActivity(intent);
					}
				});
				priceLabels.add(priceLabel);

				final Button changeButton = (Button) rowView.findViewById(R.id.changeButton);
				if (isDisplayPercentage) {
					changeButton.setText(FormatUtil.formatPricePercent(symbol.getChangePercent()));
				} else {
					//Fixes_Request-20160101, ReqNo.3
					/*
					if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
						changeButton.setText( FormatUtil.formatPriceChange2(symbol.getPriceChange() ) );
					else
						changeButton.setText( FormatUtil.formatPriceChange(symbol.getPriceChange() ) );
					*/
					changeButton.setText(FormatUtil.formatPriceChangeByExchange(symbol.getPriceChange(), selectedExchangeCode));
				}

				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				if(symbol.getPriceChange()<0)
					changeButton.setBackgroundResource(LayoutSettings.btnRed);
				else
					changeButton.setBackgroundResource(LayoutSettings.btnGreen);
				*/
				changeButton.setBackgroundResource((symbol.getPriceChange() < 0) ? LayoutSettings.btnRed : LayoutSettings.btnGreen);
				changeButton.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if (idleThread != null && !isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END

						QuoteActivity.this.changeButtonPercentage();
					}
				});
				changeButtons.add(changeButton);

				tableRow.addView(rowView);
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				if( (id + 1) % 2 != 0 )
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
				else
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
				*/
				//rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light );	//commented for Change_Request-20160101, ReqNo.3
				final View rowExpandView = QuoteActivity.this.getLayoutInflater().inflate(R.layout.symbol_expand_row, null);
				rowExpandView.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if (idleThread != null && !isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END

						priceLabel.performClick();
					}
				});
				populateExpandViewData(rowExpandView, symbol);

				tableRow.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if (idleThread != null && !isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END

						StockSymbol symbol = stockSymbols.get(tableRow.getId());
						if (!symbol.isExpand()) {
							rowLayout.addView(rowExpandView);
							/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
							if( ( tableRow.getId() + 1) % 2 != 0 )
								rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark_expand);
							else
								rowLayout.setBackgroundResource(LayoutSettings.tablerow_light_expand);
							*/
							//rowLayout.setBackgroundResource( ( ( tableRow.getId() + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark_expand : LayoutSettings.tablerow_light_expand );	//commented for Change_Request-20160101, ReqNo.3

							symbol.setExpand(true);
						} else {
							int childCount = rowLayout.getChildCount();
							if (childCount > 1)
								rowLayout.removeViewAt(1);

							symbol.setExpand(false);
							/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
							if( (tableRow.getId() + 1) % 2 != 0 )
								rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
							else
								rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
							*/
							//rowLayout.setBackgroundResource( ( (tableRow.getId() + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);	//commented for Change_Request-20160101, ReqNo.3
						}
					}
				});

				//Change_Request-20160101, ReqNo. 7 - START
				ArrayList<String> menuList = new ArrayList<String>();
				menuList.add(getResources().getString(R.string.longclick_view_stock_news));
				menuList.add(getResources().getString(R.string.longclick_view_stock_chart));
				menuList.add(getResources().getString(R.string.longclick_add_watchlist));
				if (!AppConstants.noTradeClient && AppConstants.brokerConfigBean.enableTrade()) {
					menuList.add(getResources().getString(R.string.longclick_trade));
				}

				final CustomAdapter adapter = new CustomAdapter(this, menuList);


				tableRow.setOnLongClickListener(new OnLongClickListener() {
					@Override
					public boolean onLongClick(View v) {
						final StockSymbol symbol = stockSymbols.get(tableRow.getId());

						AlertDialog.Builder longClickBuilder = new AlertDialog.Builder(QuoteActivity.this);
						longClickBuilder.setTitle(symbol.getStockName(false))
								.setAdapter(adapter, new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int item) {


										switch (item) {
											case 0:
												startStockActivityIntent(symbol.getSymbolCode(), INT_STOCK_NEWS_PAGE_POS);
												break;
											case 1:
												startStockActivityIntent(symbol.getSymbolCode(), INT_STOCK_CHART_PAGE_POS);
												break;
											case 2:
												new FetchWatchlistTask().execute(symbol.getSymbolCode());
												break;
											case 3:
												if (AppConstants.brokerConfigBean.getLstSectorNoTrading().contains(AppConstants.SELECTED_FEED_MARKET_CODE)) {
													List<LotMarket> lotMarketList = selectedExchange.processAndGetMarketInfo();
													String lotMarketName = "";

													for (LotMarket market : lotMarketList) {
														if (market.getCode().equalsIgnoreCase(AppConstants.SELECTED_FEED_MARKET_CODE)) {
															lotMarketName = market.getName();
															break;
														}
													}
													alertException.setMessage(getResources().getString(R.string.sector_no_trading_msg) + lotMarketName + ".");
													alertException.show();

												} else {
													String[] params = {symbol.getSymbolCode(), Integer.toString(TRADE_TASK_ID)};
													new FetchStockInfoAndDoNextTask().execute(params);
												}
												break;
										}
									}
								})
								.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								});
						longClickBuilder.show();
						return false;
					}
				});
				//Change_Request-20160101, ReqNo. 7 - END

				// Add the TableRow to the TableLayout
				tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

				id++;
			}
			isTableConstructed = true;
		}
	}
	
	private class CustomAdapter extends ArrayAdapter<String> {
		
		private Context c;
		private ArrayList<String> forceTouchMenuList;

		private class ViewHolder {
			ImageView imgForceTouchMenu;
	        TextView txtForceTouchMenu;
	    }
		
		
		public CustomAdapter(Context context, ArrayList<String> forceTouchMenuList) {
			super(context, 0, forceTouchMenuList);
			
			this.forceTouchMenuList	= forceTouchMenuList;
			this.c					= context;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder = null;
			StockSymbol s	= stockSymbols.get(position);
			
		    if (convertView == null) {
		    	
		    	LayoutInflater inflater = LayoutInflater.from(getContext());
		    	convertView = inflater.inflate(R.layout.custom_row_force_touch_menu, null, true);
				viewHolder = new ViewHolder();
		    	
				
				viewHolder.imgForceTouchMenu	= (ImageView)convertView.findViewById(R.id.imgForceTouchMenu);
		    	viewHolder.txtForceTouchMenu	= (TextView)convertView.findViewById(R.id.txtForceTouchMenu);
		    	
		    	viewHolder.txtForceTouchMenu.setText(forceTouchMenuList.get(position));
				
				//set image
				switch(position){
				case 0: viewHolder.imgForceTouchMenu.setImageResource(R.drawable.icon_forcetouch_news); break;
				case 1: viewHolder.imgForceTouchMenu.setImageResource(R.drawable.icon_forcetouch_chart); break;
				case 2: viewHolder.imgForceTouchMenu.setImageResource(R.drawable.icon_forcetouch_watchlist); break;
				case 3: viewHolder.imgForceTouchMenu.setImageResource(R.drawable.icon_forcetouch_trade); break;

				}
		    	
		    	convertView.setTag(viewHolder);
		    } else {
		   
		    	viewHolder = (ViewHolder) convertView.getTag();
		    }
		    
			return convertView;
		}
		
	}
	
	private void startStockActivityIntent(String symbolCode, int pagerId){
		Intent stockIntent	= new Intent();
		stockIntent.setClass(QuoteActivity.this, StockActivity.class);
		stockIntent.putExtra(ParamConstants.STOCK_PAGER_ID_TAG, pagerId);
		stockIntent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
		stockIntent.putExtra(ParamConstants.PARAM_STR_RESTART_ACTIVITY, true);
		
		startActivity(stockIntent);
	}
	
	private void setDisplayButtonPercentage(){
		int index = 0;
		
		// Mary@20120810 - avoid NullPointerException
		if(stockSymbols == null)
			return;

		for (Iterator<Button> itr=changeButtons.iterator(); itr.hasNext(); ) {
			Button changeButton = itr.next();
			StockSymbol symbol	= stockSymbols.get(index);

			changeButton.setText(FormatUtil.formatPricePercent(symbol.getChangePercent()));
			index++;
		}
	}

	private void setDisplayButtonPrice(){
		int index = 0;

		// Mary@20120807 - avoid nullpointerException
		if(stockSymbols != null){
			for (Iterator<Button> itr=changeButtons.iterator(); itr.hasNext(); ) {
				Button changeButton	= itr.next();
				StockSymbol symbol	= stockSymbols.get(index);
				
				//Fixes_Request-20160101, ReqNo.3
				//changeButton.setText(FormatUtil.formatPriceChange(symbol.getPriceChange()));
				changeButton.setText(FormatUtil.formatPriceChangeByExchange(symbol.getPriceChange(), selectedExchangeCode));
				
				index++;
			}
		}
	}

	private void changeButtonPercentage() {
		int index = 0;

		// Mary@20120807 - avoid nullpointerException
		if(stockSymbols != null){
			for (Iterator<Button> itr=changeButtons.iterator(); itr.hasNext(); ) {
				Button changeButton	= itr.next();
				StockSymbol symbol	= stockSymbols.get(index);
				if(isDisplayPercentage){
					//Fixes_Request-20160101, ReqNo.3
					/*
					if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
						changeButton.setText(FormatUtil.formatPriceChange2(symbol.getPriceChange()));
					else
						changeButton.setText(FormatUtil.formatPriceChange(symbol.getPriceChange()));
					 */
					changeButton.setText(FormatUtil.formatPriceChangeByExchange(symbol.getPriceChange(), selectedExchangeCode));
				}else{
					changeButton.setText(FormatUtil.formatPricePercent(symbol.getChangePercent()));
				}
				index++;
			}
			/* Mary@20120924 - Fixes_Request-20120815, ReqNo.3
			if(isDisplayPercentage)
				isDisplayPercentage = false;
			else
				isDisplayPercentage = true;
			*/
			isDisplayPercentage	= !isDisplayPercentage;
		}
	}

	private void populateExpandViewData(View expandView, StockSymbol symbol){
		// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
		int intLotSize				= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? symbol.getIntLotSize() : 1;
		
		/* Mary@20130718 - Change_Request-20130424, ReqNo.13
		// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - START
		TextView exchgLabel 		= (TextView) expandView.findViewById(R.id.tvFieldExchgType);
		exchgLabel.setText(AppConstants.DEFAULT_EXCHANGE_CODE);
		exchgLabels.add(exchgLabel);
		// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - END
		*/
		
		TextView volumeLabel 		= (TextView) expandView.findViewById(R.id.volumeLabel);
		/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
		/* Mary@20120911 - Change_Request-20120719, ReqNo.12 
		volumeLabel.setText(FormatUtil.formatLong(symbol.getVolume()));
		/
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		volumeLabel.setText( FormatUtil.formatLong( symbol.getVolume() / intLotSize ) );
		/
		volumeLabel.setText( FormatUtil.formatLong( symbol.getVolume() / intLotSize, "-" ) );
		*/
		volumeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (symbol.getVolume() / intLotSize), "-" ) );
		volumeLabels.add(volumeLabel);
		
		TextView valueLabel 		= (TextView) expandView.findViewById(R.id.valueLabel);
		/*
		if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
			
			valueLabel.setText( FormatUtil.formatDouble2(symbol.getValue(), "-") );
		else
		*/
		valueLabel.setText( FormatUtil.formatDouble(symbol.getValue(), "-") );
		valueLabels.add(valueLabel);
		
		TextView openLabel 			= (TextView) expandView.findViewById(R.id.openLabel);
		//Fixes_Request-20160101, ReqNo.3
		/*
		if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
			openLabel.setText( FormatUtil.formatPrice2(symbol.getOpen(), "-") );
		else
			openLabel.setText( FormatUtil.formatPrice(symbol.getOpen(), "-") );
		*/
		openLabel.setText( FormatUtil.formatPrice(symbol.getOpen(), "-", selectedExchangeCode) );
		openLabels.add(openLabel);
		
		TextView highLabel 			= (TextView) expandView.findViewById(R.id.highLabel);
		//Fixes_Request-20160101, ReqNo.3
		/*
		if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
			highLabel.setText( FormatUtil.formatPrice2(symbol.getHigh(), "-") );
		else
			highLabel.setText( FormatUtil.formatPrice(symbol.getHigh(), "-") );
		*/
		highLabel.setText(FormatUtil.formatPrice(symbol.getHigh(), "-", selectedExchangeCode));
		highLabels.add(highLabel);
		
		TextView lowLabel 		= (TextView) expandView.findViewById(R.id.lowLabel);
		//Fixes_Request-20160101, ReqNo.3
		/*
		if( AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JK") || AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("JKD") )
			lowLabel.setText( FormatUtil.formatPrice2(symbol.getLow(), "-") );
		else
			lowLabel.setText( FormatUtil.formatPrice(symbol.getLow(), "-") );
		*/
		lowLabel.setText(FormatUtil.formatPrice(symbol.getLow(), "-", selectedExchangeCode));
		
		lowLabels.add(lowLabel);
		
		TextView tradeLabel 	= (TextView) expandView.findViewById(R.id.tradeLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		tradeLabel.setText(FormatUtil.formatLong(symbol.getTotalTrade()));
		*/
		tradeLabel.setText( FormatUtil.formatLong(symbol.getTotalTrade(), "-") );
		tradeLabels.add(tradeLabel);
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message	= handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if( response.get(2).equals("1102") ){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(refreshThread != null)
					refreshThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				if( QuoteActivity.this.mainMenu.isShowing() )
					QuoteActivity.this.mainMenu.hide();
				
				if( sectorListView.isShowing() )
					sectorListView.hide();
				
				if(mktFilterListView != null && mktFilterListView.isShowing())
					mktFilterListView.hide();
					
				if( !isFinishing() )
					QuoteActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};


	@Override
	public void QCAliveEvent(boolean isResume) {
	}
	
	//Added by Thinzar@20140721 - Change_Request-20140701, Req #6 - START
	private boolean isNetworkAvailable() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	//Added by Thinzar@20140721 - Change_Request-20140701 - END
	
	//Added by Thinzar@20141027 - Change_Request-20140901, ReqNo.12 - START
	private void resetMarketButton(){
		
		//to reset market button when switch exchange, or when filtering sector
		if(strSelectedFilterMktType == null)	strSelectedFilterMktType		= AppConstants.DEFAULT_FEED_MARKET_TYPE;
		if(strSelectedFilterMktCode == null)	strSelectedFilterMktCode		= AppConstants.DEFAULT_FEED_MARKET_CODE;
		marketButton.setText(strSelectedFilterMktType);	
		
	}
	private void populateFilterMarketButton(){
		System.out.println("debug==populateFilterMarketButton()");
		this.resetMarketButton();
		
		if(SystemUtil.isDerivative(selectedExchange.getQCExchangeCode())){
			marketButton.setVisibility(View.GONE);
		} else{
			
			List<LotMarket> lotMarketList	= selectedExchange.processAndGetMarketInfo();
			filterMarketMap					= new HashMap<String, String>();
			mktTypes.clear();
			
			for(int i=0;i<lotMarketList.size();i++){
				LotMarket lm = lotMarketList.get(i);
				mktTypes.add(lm.getName());
				filterMarketMap.put(lm.getName(), lm.getCode());
			}
			
			mktFilterListView	= new MarketTypeFilterListView(this, this, getLayoutInflater());
			try{
				mktFilterListView.setMktTypeFilterList(mktTypes);
			} catch(Exception ex){
				ex.printStackTrace();
			}
			marketButton.setOnClickListener(marketFilter);
			
			marketButton.setVisibility(View.VISIBLE);
		}
	}
	
	View.OnClickListener marketFilter = new OnClickListener(){

		@Override
		public void onClick(View v) {
			if(idleThread != null && !isShowingIdleAlert)
				idleThread.resetExpiredTime();
				
			QuoteActivity.this.doFilterMarket();
		}
			
	};
		
	private void doFilterMarket(){
			
		if(mktFilterListView.isShowing()){
			mktFilterListView.hide();
		} else{
			if(mainMenu.isShowing()){
				mainMenu.hide();
			}
			mktFilterListView.show(findViewById(R.id.moverLayout), strSelectedFilterMktType);
		}
	}
		
	@Override
	public void mktTypeFilterSelectedEvent(String mktTypeFilter) {
		this.strSelectedFilterMktType = mktTypeFilter;
		this.strSelectedFilterMktCode	= filterMarketMap.get(mktTypeFilter);
		
		//Added by Thinzar, Change_Request-20150901, ReqNo.5
		AppConstants.SELECTED_FEED_MARKET_CODE	= this.strSelectedFilterMktCode;
		
		if(mktTypeFilter != null){
			marketButton.setText(mktTypeFilter);
		}
		
		this.sortImageByType(ParamConstants.IMAGESORT);
		
		//reset the sector selection
		selectedSector = sectors.get(0);
		sectorButton.setText(selectedSector.getSectorName());
		
		displayTotalCount(); //to get the total count from ALL STOCKS's TOTAL
	}
	//Added by Thinzar@20140731 - Change_Request-20140701, ReqNo.9 - END
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.7 - START
	private StockInfo stockInfo;
	
	private class FetchStockInfoAndDoNextTask extends AsyncTask<String, Void, String[]> {
		private Exception exception 						= null;
		private int nextTaskId;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			//TODO: start a progress dialog
			if(!QuoteActivity.this.isFinishing()){
				dialog.setMessage(getResources().getString(R.string.fetching_stock_info_msg));
				dialog.show();
			}
		}

		@Override
		protected String[] doInBackground(String... params) {
			
			String[] symbolCode 	= { params[0] };
			nextTaskId				= Integer.parseInt(params[1]);

			String response = null;
			try {
				//Change_Request-20170601, ReqNo.12 - START
				if(isMapStockCode){
					isMapStockCode = false;
					String code	= symbolCode[0].substring(0, (symbolCode[0].lastIndexOf(".")));

					String subscribedSgExchange = SystemUtil.getSubscribedSgExchange();

					if(subscribedSgExchange == null){
						throw new Exception(getResources().getString(R.string.ibillionaire_exchange_not_subscribed));
					}else {
						response = QcHttpConnectUtil.getStockMapping(code, subscribedSgExchange);
						symbolCode = new String[]{response};

						if(response == null)
							throw new Exception("Failed to get stock mapping.");
					}
				}
				//Change_Request-20170601, ReqNo.12 - END

				intRetryCtr = 0;
				
				response = QcHttpConnectUtil.imageQuote(symbolCode, CommandConstants.IMAGE_QUOTE_DETAIL);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = QcHttpConnectUtil.imageQuote(symbolCode, CommandConstants.IMAGE_QUOTE_DETAIL);
				}
				intRetryCtr = 0;

				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);

			} catch (Exception e) {
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			if(response != null)
				stockInfo = QcMessageParser.parseStockDetail(response);

			return symbolCode;
		}

		@Override
		protected void onPostExecute(String[] result) {
			super.onPostExecute(result);

			if (dialog != null && dialog.isShowing())
				dialog.dismiss();
			
			if (exception != null) {
				alertException.setMessage(exception.getMessage());

				if (!QuoteActivity.this.isFinishing())
					alertException.show();
				return;
			} else{
				String strSymbolCode	= stockInfo.getSymbolCode();
				
				if(strSymbolCode == null || strSymbolCode.isEmpty()){
				
					showAlert(getResources().getString(R.string.stock_not_found_ibillionaire));
				}
				else{
					switch(nextTaskId){
					case TRADE_TASK_ID:
						Intent tradeIntent 	= new Intent();
						
						//tradeIntent.putExtra( ParamConstants.BID_ASK_QTY_TAG, getCumulativeBidAskQty(lstNewMarketDepths, strTemp, INT_BID_RELATED) );
						tradeIntent.putExtra(ParamConstants.SYMBOLCODE_TAG, stockInfo.getSymbolCode());
						tradeIntent.putExtra(ParamConstants.LOT_SIZE_TAG, Integer.toString( stockInfo.getSharePerLot() ) );
						tradeIntent.putExtra(ParamConstants.INSTRUMENT_TAG, stockInfo.getInstrument() );
						tradeIntent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stockInfo.isPayableWithCPF());
						tradeIntent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
						tradeIntent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
						
						//Change_Request-20170119, ReqNo. 4
						if(isRedirectiBillionaire){
							tradeIntent.putExtra(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG, ParamConstants.PARAM_ORDER_SOURCE_IB);
							
							//reset variables
							isRedirectiBillionaire		= false;
							stockCodeFromiBillionaire	= "";
							exchgCodeFromiBillionaire	= "";
						}
						
						if(isRedirectSgx){
							//tradeIntent.putExtra(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG, ParamConstants.PARAM_ORDER_SOURCE_SGX);
							
							if(sgxAction != null){
								tradeIntent.putExtra(ParamConstants.ACTIONCODE_TAG, sgxAction);
							}
							
							if(sgxPrice != null){
								tradeIntent.putExtra(ParamConstants.BID_ASK_PRICE_TAG, sgxPrice);
							}
							
							if(sgxQty != null){
								tradeIntent.putExtra(ParamConstants.ORDER_QUANTITY_TAG, sgxQty);
							}
							
							isRedirectSgx = false;
						}else{
							tradeIntent.putExtra( ParamConstants.BID_ASK_PRICE_TAG, Float.toString(stockInfo.getLastDone()));
						}
						
						tradeIntent.setClass(QuoteActivity.this, TradeActivity.class);
						
						startActivity(tradeIntent);
						
						break;
						
					case STOCK_DETAILS_TASK_ID:

						Intent stockIntent = new Intent();
						stockIntent.putExtra(ParamConstants.SYMBOLCODE_TAG, stockInfo.getSymbolCode());
						stockIntent.putExtra(ParamConstants.PARAM_STR_RESTART_ACTIVITY, true);
						stockIntent.setClass(QuoteActivity.this, StockActivity.class);

						startActivity(stockIntent);

						break;
					}
				}
			}
		}
	}
	//Added by Thinzar, Change_Request-20160101, ReqNo.7 - END
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.8 - START
	private class FetchWatchlistTask extends AsyncTask<String, Void, Exception>{
		String atpUserParam 								= null;
		String senderCode									= null;
		private boolean isDialogOwner						= false;
		private AsyncTask<Void, Void, Exception> updateTask = null;
		private Exception exception							= null;
		private String mStockCode							= null;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			try{
				atpUserParam 	= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
				senderCode		= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
				watchlists 		= new ArrayList<Watchlist>();
				
				if( ! QuoteActivity.this.dialog.isShowing() ){
					QuoteActivity.this.dialog.setMessage(getResources().getString(R.string.retrieving_watchlist_msg));
					
					if( ! QuoteActivity.this.isFinishing() ){
						QuoteActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		@Override
		protected Exception doInBackground(String... params) {
			mStockCode	= params[0];
			
			Map<String, Object> parameters 	= new HashMap<String, Object>();
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			try {
				String response = AtpConnectUtil.getFavListInfo(parameters);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = AtpConnectUtil.getFavListInfo(parameters);
				}
				intRetryCtr = 0;
	
				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_WATCHLIST);
				
				watchlists = AtpMessageParser.parseFavListInfo(response);
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				return e;
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Void... params) {
			try{
				if( ! QuoteActivity.this.dialog.isShowing() ){
					QuoteActivity.this.dialog.setMessage(getResources().getString(R.string.retrieving_watchlist_msg));
					
					if( ! QuoteActivity.this.isFinishing() ){
						QuoteActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected void onPostExecute(Exception exception) {
			super.onPostExecute(exception);

			try{
				if (exception != null) {
					if( QuoteActivity.this.dialog != null && QuoteActivity.this.dialog.isShowing() && isDialogOwner){
						QuoteActivity.this.dialog.dismiss();
						isDialogOwner = false;
					}
					
					if(exception instanceof FailedAuthenicationException){
						if (refreshThread != null)
							refreshThread.stopRequest();

						if (loginThread != null)
							loginThread.stopRequest();

						if (LoginActivity.QCAlive != null)
							LoginActivity.QCAlive.stopRequest();

						QuoteActivity.this.sessionExpired();
					}else{
						alertException.setMessage(exception.getMessage());
						alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								finish();
							}
						});
						
						if( ! QuoteActivity.this.isFinishing() )
							alertException.show();
					}
					
					exception = null;
					return;
				}
				
				if( QuoteActivity.this.dialog != null && QuoteActivity.this.dialog.isShowing() && isDialogOwner){
					QuoteActivity.this.dialog.dismiss();
					isDialogOwner = false;
				}
				
				if (watchlists.size() == 0) {
					alertException.setMessage(getResources().getString(R.string.no_watchlist_found));
					if( ! QuoteActivity.this.isFinishing() )
						alertException.show();
					return;
				} else {

					final String[] items = new String[watchlists.size()];
					int index = 0;
					for (Iterator<Watchlist> itr = watchlists.iterator(); itr.hasNext();) {
						Watchlist watchlist = itr.next();
						items[index] = watchlist.getName();
						index++;
					}

					AlertDialog.Builder builder = new AlertDialog.Builder(QuoteActivity.this);
					builder.setTitle("Add to watchlist");
					builder.setItems(items, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int position) {
							Watchlist w = watchlists.get(position);

							new AddWatchlistStock().execute(w.getId(), mStockCode);
						}
					});
					builder.show();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}	
	
	private class AddWatchlistStock extends AsyncTask<String, Void, String> {
		
		private Exception exception = null;
		String strWatchlistID;
		String strStockSymbolCode;
		boolean isSuccess	= false;
		
		@Override
		protected String doInBackground(String... item) {
			strWatchlistID		= item[0];
			strStockSymbolCode	= item[1];
			
			
			String atpUserParam				= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			String senderCode 				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
			String strExchangeCode			= application.getSelectedExchange().getQCExchangeCode();
			
			try {
				//check for duplicate and maxStocksInWatchList
				try{
					String symbolCode	= strStockSymbolCode.substring(0, strStockSymbolCode.lastIndexOf("."));
					isSuccess = checkWatchlistSymbols(strExchangeCode, symbolCode, strWatchlistID, atpUserParam, senderCode, QuoteActivity.this);
					
				} catch(Exception e){
					System.out.println("caught exception from checkWatchlistSymbols");
					throw e;
				}
				
				Map<String, Object> parameters	= new HashMap<String, Object>();
				
				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);

				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
				parameters.put(ParamConstants.FAVORATEID_TAG, strWatchlistID);
				
				int index 					= strStockSymbolCode.lastIndexOf(".");
				if(index != -1){
					parameters.put( ParamConstants.SYMBOLCODE_TAG, strStockSymbolCode.substring(0, index) );
					strExchangeCode	= strStockSymbolCode.substring( (index + 1), strStockSymbolCode.length() );
				}else{
					parameters.put(ParamConstants.SYMBOLCODE_TAG, strStockSymbolCode);
				}
				
				strExchangeCode				= (strExchangeCode.length() > 1 && strExchangeCode.toUpperCase().endsWith("D") ) ? strExchangeCode.substring(0, (strExchangeCode.length() - 1) )  : strExchangeCode;
				parameters.put(ParamConstants.EXCHANGECODE_TAG, strExchangeCode);
				
				try {
					boolean isSuccess = AtpConnectUtil.addFavListStock(parameters);
					while (intRetryCtr < AppConstants.intMaxRetry && !isSuccess) {
						intRetryCtr++;
						isSuccess = AtpConnectUtil.addFavListStock(parameters);
					}
					intRetryCtr = 0;

					if (!isSuccess)
						throw new Exception(ErrorCodeConstants.FAIL_ADD_STOCK_INTO_WATCHLIST);
				} catch (FailedAuthenicationException e) {
					isExpired = true;
				
					if(refreshThread != null)
						refreshThread.stopRequest();
				
					if(loginThread != null)
						loginThread.stopRequest();
			
					if(LoginActivity.QCAlive != null)
						LoginActivity.QCAlive.stopRequest();
					throw e;
				} catch (Exception e) {
					throw e;
				}
			} catch (Exception e) {
				exception = e;
			}
			return item[1];

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			try{
				if (isExpired) {
					QuoteActivity.this.sessionExpired();
					return;
				}
	
				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					
					if( ! QuoteActivity.this.isFinishing() )
						alertException.show();
					
					return;
				}
				
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.added_to_watchlist) + " " + result, Toast.LENGTH_SHORT).show();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public static boolean checkWatchlistSymbols(String feedExchgCode, String symbolCode, String watchlistId, String atpUserParam, String senderCode, Context mContext) throws Exception {
		int intRetryCtr	= 0;
		boolean isSuccess	= true;
		
		Map<String, Object> parameters 	= new HashMap<String, Object>();
		if( AppConstants.brokerConfigBean.isEnableEncryption() )
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
		
		parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
		parameters.put(ParamConstants.FAVORATEID_TAG, watchlistId);

		String response = null;
		try {
			response = AtpConnectUtil.getFavListStockCode(parameters);
			while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
				intRetryCtr++;
				response = AtpConnectUtil.getFavListStockCode(parameters);
			}
			intRetryCtr = 0;

			if (response == null){
				isSuccess	= false;
				throw new Exception(ErrorCodeConstants.FAIL_GET_WATCHLIST_STOCK);
			}
			
			String[] mSymbols	= AtpMessageParser.parseFavListStockCode(response);
			
			if(AppConstants.brokerConfigBean.isImposeMaxStockAddToWatchlist() && 
					mSymbols.length >= AppConstants.brokerConfigBean.getIntMaxNoOfStockAddToWatchlist()){
				
				isSuccess	= false;
				throw new Exception(mContext.getResources().getString(R.string.max_stock_per_watchlist_msg) + ":" + AppConstants.brokerConfigBean.getIntMaxNoOfStockAddToWatchlist());
			} else {
				
				if(Arrays.asList(mSymbols).contains(symbolCode.concat("." + feedExchgCode))){
					isSuccess	= false;
					throw new Exception(mContext.getResources().getString(R.string.counter_exist_in_watchlist));
				} 
			}
			
		} catch (FailedAuthenicationException e) {
			isSuccess	= false;
			throw e;
		} catch (Exception e) {
			isSuccess	= false;
			throw e;
		}

		return isSuccess;
	}
	//Added by Thinzar, Change_Request-20160101, ReqNo.8 - END
	
	public void showAlert(String message) {
		
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(message)
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
						}
					});
			AlertDialog alert = builder.create();
		
			if( ! this.isFinishing() )
				alert.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void showAlertVerifiedIBillionaire(String message) {
		
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(message)
			.setTitle(getResources().getString(R.string.verified_title_ibillionaire))
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							String appLinkFull	= "";
							
							try {
								String senderCode				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
								final String iBillionaireAppLink	= AppConstants.iBillionaireAppLinkMap.get(getApplicationContext().getPackageName());
								String appLinkDataString		= AppConstants.iBillionaireVerifySuccessToken + "/" + verificationCodeFromIBillionaire + "/" + senderCode;
								String encryptedAppLinkData		= Base64.encodeToString(TheScreenerActivity.encrypt(AppConstants.iBillionaireSecretkey, appLinkDataString), Base64.NO_WRAP);
								
								appLinkFull		= iBillionaireAppLink + "://" + URLEncoder.encode(encryptedAppLinkData, AppConstants.STRING_ENCODING_FORMAT);
								System.out.println("appLinkFull: " + appLinkFull);
								
								startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appLinkFull)));
								
							} catch (Exception e) {
								e.printStackTrace();
								Toast.makeText(QuoteActivity.this, getResources().getString(R.string.feature_not_available) + appLinkFull, Toast.LENGTH_LONG).show();
							}
						}
					})
					.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							dialog.dismiss();
						}
					});
			
			AlertDialog alert = builder.create();
		
			if( ! this.isFinishing() )
				alert.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//Change_Request-20170119, ReqNo.5 - START
	private class FetchDtiTask extends AsyncTask<String, Void, Exception>{
		private Exception ex	= null;
		private String dtiResultString	= null;
		private ArrayList<DtiResult> dtiResultArrayList;
		//private String dtiLogUrl;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			/* Commented by Thinzar, remove logging since not getting from Recognia
			//for DTI logging purpose
			dtiLogUrl	= "https://api.recognia.com/api/logusage?cid=207&uid=admin&pwd=tradeCONTROL&tuid=" 
						+ AppConstants.userSenderName + "&page=dti_listing_mobile";
			 */
		}
		
		@Override
		protected Exception doInBackground(String... params) {
			String url	= params[0];
			
			try{
				HttpHelper httpHelper	= new HttpHelper();
				dtiResultArrayList		= new ArrayList<DtiResult>();
			
				dtiResultString	= httpHelper.doGet(url);
				/*
				//DTI logging purpose
				String logDetails = httpHelper.doGet(dtiLogUrl);
				*/
				dtiResultArrayList	= SystemUtil.parseDtiResult(dtiResultString);
				
			}catch(IOException e){
				ex = e;
				e.printStackTrace();
			} catch(Exception e){
				ex = e;
				e.printStackTrace();
			}
			
			return ex;
		}
		
		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);
			
			AppConstants.hasCheckedDti	= true;
			if(dtiResultArrayList.size() > 0){
				
				//CIMB insists to have hyperlinks in the dialog, so need to use the custom designed Dialog
				dtiDialog= new Dialog(QuoteActivity.this);
				dtiDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dtiDialog.setContentView(R.layout.dti_alert_layout);
				
				final Button btnTotalDti	= (Button)dtiDialog.findViewById(R.id.btn_total_dti);
				btnTotalDti.setText(dtiResultArrayList.size() + " Daily Trading Ideas"); 
				btnTotalDti.setPaintFlags(btnTotalDti.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
				btnTotalDti.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						startDtiIntent(dtiResultArrayList);
						dtiDialog.dismiss();
					}
				});
				
				final TextView tvGreeting	= (TextView)dtiDialog.findViewById(R.id.tv_greeting);
				tvGreeting.setText("Good day " + AppConstants.userSenderName);
				
				final Button btnCloseDti	= (Button)dtiDialog.findViewById(R.id.btn_close_dti);
				btnCloseDti.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						dtiDialog.dismiss();
						
						//Change_Request-20170119, ReqNo.15 
						checkStatusForPushRegistration();
					}
				});
				
				final String todayDateStr	= SystemUtil.getTodayDate(FormatUtil.format_yyyyMMdd);
				
				final CheckBox chkDoNotShowDti	= (CheckBox)dtiDialog.findViewById(R.id.chk_not_show_dti);
				chkDoNotShowDti.setOnCheckedChangeListener(new OnCheckedChangeListener(){

					@Override
					public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
						SharedPreferences.Editor editor = sharedPreferences.edit();
						
						if(isChecked){
							editor.putString(PreferenceConstants.DTI_DO_NOT_SHOW_DATE, todayDateStr);
							editor.commit();
						}else{
							editor.remove(PreferenceConstants.DTI_DO_NOT_SHOW_DATE);
							editor.commit();
						}
					}
				});
				
				final Button btnMYStock1	= (Button)dtiDialog.findViewById(R.id.btn_my_stock1);
				final Button btnMYStock2	= (Button)dtiDialog.findViewById(R.id.btn_my_stock2);
				final TextView tvMYVolume1	= (TextView)dtiDialog.findViewById(R.id.tv_my_volume1);
				final TextView tvMYVolume2	= (TextView)dtiDialog.findViewById(R.id.tv_my_volume2);
				final TextView tvTitleMYTotal = (TextView)dtiDialog.findViewById(R.id.tv_title_total_volume_my);
				final LinearLayout layoutMktVolume1 = (LinearLayout)dtiDialog.findViewById(R.id.layout_market_volume1);
				final LinearLayout layoutMktVolume2 = (LinearLayout)dtiDialog.findViewById(R.id.layout_market_volume2);
				
				btnMYStock1.setPaintFlags(btnTotalDti.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
				btnMYStock2.setPaintFlags(btnTotalDti.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
				
				int currentMYStockCount	= 0;
				for(int i=0;i<dtiResultArrayList.size();i++){
					final DtiResult mDti	= dtiResultArrayList.get(i);
					
					if(mDti.getInstrumentExchange().equalsIgnoreCase("MYX")){
						currentMYStockCount++;
						
						String stkName	= mDti.getInstrumentName().split(" ")[0];
						String stkNameString	= stkName + " (" + mDti.getInstrumentSymbolCommon() + ")";
						String volumeString		= FormatUtil.formatInteger(Integer.parseInt(mDti.getPricingVolume()));
						
						if(currentMYStockCount == 1){
							btnMYStock1.setText(stkNameString);
							tvMYVolume1.setText(" " + volumeString);
							
							btnMYStock1.setOnClickListener(new OnClickListener(){

								@Override
								public void onClick(View v) {
									startDtiDetailsIntent(mDti);
									dtiDialog.dismiss();
								}
							});
						}else if(currentMYStockCount == 2){
							btnMYStock2.setText(stkNameString);
							tvMYVolume2.setText(" " + volumeString);
							
							btnMYStock2.setOnClickListener(new OnClickListener(){

								@Override
								public void onClick(View v) {
									startDtiDetailsIntent(mDti);
									dtiDialog.dismiss();
								}
							});
						}
					}
				}
				
				if(currentMYStockCount == 0){
					tvTitleMYTotal.setVisibility(View.GONE);
					layoutMktVolume1.setVisibility(View.GONE);
					layoutMktVolume2.setVisibility(View.GONE);
				}
				
				dtiDialog.show();
			}
		}
	}
	
	private void startDtiIntent(ArrayList<DtiResult> mDtiResultArrayList){
		Intent dtiIntent	= new Intent();
		dtiIntent.setClass(QuoteActivity.this, DtiActivity.class);
		dtiIntent.putParcelableArrayListExtra(ParamConstants.DTI_RESULT_ARRAYLIST_TAG, mDtiResultArrayList);
		startActivity(dtiIntent);
	}
	
	private void startDtiDetailsIntent(DtiResult mDti){
		Intent dtiDetailIntent	= new Intent();
		dtiDetailIntent.setClass(QuoteActivity.this, DtiDetailsActivity.class);
		dtiDetailIntent.putExtra(ParamConstants.DTI_ID_TAG, mDti.getId());
		dtiDetailIntent.putExtra(ParamConstants.DTI_INSTRUMENT_SYM_SOMMON_TAG, mDti.getInstrumentSymbolCommon());
		dtiDetailIntent.putExtra(ParamConstants.DTI_INSTRUMENT_EXCHG_TAG, mDti.getInstrumentExchange());
		dtiDetailIntent.putExtra(ParamConstants.DTI_INSTRUMENT_ISIN_TAG, mDti.getInstrumentIsin());
		dtiDetailIntent.putExtra(ParamConstants.DTI_TRADE_TYPE_TAG, mDti.getTradeType());

		startActivity(dtiDetailIntent);
	}
	//Change_Request-20170119, ReqNo.5 - END
	
	//Change_Request-20170119, ReqNo.15 - START
	private ProgressDialog mDialog;
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST 	= 9000;
	private String gcmRegId;
	private String previousGcmRegId;
	private String jwtToken;
	private String strCurrentUsername;
	private static final int REQUEST_PHONE_STATE = 1;
	
	private void checkStatusForPushRegistration(){
		if(!AppConstants.hasCheckedGCM && AppConstants.brokerConfigBean.isEnableStockAlert()){
			AppConstants.hasCheckedGCM = true;
		
			SharedPreferences pushInfoPref 	= getSharedPreferences(PreferenceConstants.GCM_PREF_FILE, MODE_PRIVATE);
			
			String currentUsername	= sharedPreferences.getString(PreferenceConstants.USER_NAME, "");
			boolean isSetDoNotAsk	= sharedPreferences.getBoolean(PreferenceConstants.DO_NOT_ASK_PUSH_NOTI, false);
			String pushInfoUsername			= pushInfoPref.getString(PreferenceConstants.GCM_USERNAME, "");
			boolean hasDeviceRegistered 	= pushInfoPref.getBoolean(PreferenceConstants.HAS_DEVICE_REGISTERED, false);
			String registeredAppVersion		= pushInfoPref.getString(PreferenceConstants.GCM_REG_APP_VERSION, "0");
			String registeredOSVersion		= pushInfoPref.getString(PreferenceConstants.GCM_REG_OS_VERSION, "0");
			
			if( !pushInfoUsername.equals(currentUsername)){
				System.out.println("debug==pushInfoUser and currentUsername not same");
				StockAlertUtil.clearGcmInfoFromPref(QuoteActivity.this);
				//without this, the prompt will keep showing
				SharedPreferences.Editor pushInfoEditor = pushInfoPref.edit();
				pushInfoEditor.putString(PreferenceConstants.GCM_USERNAME, currentUsername);
				pushInfoEditor.commit();
				
				isSetDoNotAsk = false;
			}
			System.out.println("debug==isSetDoNotAsk:" + isSetDoNotAsk);
			
			if(!isSetDoNotAsk){
					AlertDialog.Builder pushAlertBuilder	= new AlertDialog.Builder(QuoteActivity.this);
					AlertDialog enablePushAlert = pushAlertBuilder.setTitle(getResources().getString(R.string.settings_push_noti_alert_title))
					.setMessage(getResources().getString(R.string.settings_push_noti_alert_msg))
					.setPositiveButton(getResources().getString(R.string.dialog_button_yes), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int arg1) {
							QuoteActivity.this.startPushNotificationRegisterProcess();
							dialog.dismiss();
						}
					})
					.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					})
					.create();
					
					enablePushAlert.show();
					StockAlertUtil.saveDoNotAskAgainForPushNotification(QuoteActivity.this);
					
			}
			
			//silent update GCM_ID if os version changes or app version changes.
			System.out.println("debug==registeredAppVersion:" + registeredAppVersion 
					+ ", currentVersion:" + SystemUtil.getAppVersionName(QuoteActivity.this));
			if(pushInfoUsername.equals(currentUsername) && hasDeviceRegistered){
				if(!registeredAppVersion.equals(SystemUtil.getAppVersionName(QuoteActivity.this))
						|| !registeredOSVersion.equals(SystemUtil.getDeviceOSVersion())){
					mDialog					= new ProgressDialog(QuoteActivity.this);
					previousGcmRegId		= StockAlertUtil.getGcmRegIdFromPref(QuoteActivity.this);
					new GcmSilentCheckTask().execute();
				}
			}
		}
	}
	
	private class GcmSilentCheckTask extends AsyncTask<Void, Void, Exception>{
		private Exception ex = null;
		
		@Override
		protected Exception doInBackground(Void... arg0) {
			System.out.println("GcmSilentCheckTask");
			try {
				gcmRegId = StockAlertUtil.getGcmRegistrationId(QuoteActivity.this);
				
			} catch (IOException e) {
				e.printStackTrace();
				ex = e;
			}
			return ex;
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);
			if(result == null){
				if(!previousGcmRegId.equals(gcmRegId)){
					System.out.println("GcmSilentCheckTask.onPostExecute, different gcmRegId");
					strCurrentUsername 		= sharedPreferences.getString(PreferenceConstants.USER_NAME, null);
					StockAlertUtil.saveGcmInfoToPref(QuoteActivity.this, strCurrentUsername, gcmRegId);

					//get public key
					new GetPublicKeyTask().execute();
				}else{
					System.out.println("GcmSilentCheckTask.onPostExecute, same gcmRegId");
				}
			}
		}
	}
	
	private void startPushNotificationRegisterProcess(){
		
		mDialog					= new ProgressDialog(QuoteActivity.this);
		
		if(!AppConstants.isAppMultilogin && AppConstants.brokerConfigBean.isEnableStockAlert()){
			// Check for the phone_state permission before accessing the camera.  If the permission is not granted yet, request permission.
	        final int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
	        if (rc == PackageManager.PERMISSION_GRANTED) {
	        	regGcm();
	        } else {
	        	checkForPhoneStatePermission();
	        }
		} 
	}
	
	private void regGcm(){
		//GCMRegistrar.checkDevice(getApplicationContext());
		//GCMRegistrar.checkManifest(getApplicationContext());

		// Check device for Play Services APK. If check succeeds, proceed with GCM registration.
		if (checkPlayServices()) {
			new GetGcmRegistrationIdTask().execute();
		} else {
			DefinitionConstants.Debug("No valid Google Play Services APK found.");
		}
	}
	
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, QuoteActivity.this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				showAlert("This device is not supported for Push Notification.");
			}
			return false;
		}
		return true;
	}
	
	private class GetGcmRegistrationIdTask extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if(mDialog != null && !mDialog.isShowing()){
				mDialog.setMessage(getResources().getString(R.string.stock_alert_progress_register_gcm));
				mDialog.show();
			}
		}

		@Override
		protected String doInBackground(Void... params) {
			String exceptionMsg = null;

			try {
				gcmRegId = StockAlertUtil.getGcmRegistrationId(QuoteActivity.this);
				DefinitionConstants.Debug("QuoteActivity: Device registered, registration ID=" + gcmRegId);

			} catch (IOException ex) {
				exceptionMsg = ex.getMessage();
			}
			
			return exceptionMsg;
		}

		@Override
		protected void onPostExecute(String msg) {
			if(mDialog!= null && mDialog.isShowing())
				mDialog.dismiss();

			if(msg != null){
				showAlert(msg);
			}else{

				AppConstants.hasRegisteredStockAlert	= true;				
				strCurrentUsername 		= sharedPreferences.getString(PreferenceConstants.USER_NAME, null);
				StockAlertUtil.saveGcmInfoToPref(QuoteActivity.this, strCurrentUsername, gcmRegId);

				//get public key
				new GetPublicKeyTask().execute();
			}
		}
	}

	//getPublicKey and generate JWT
	private class GetPublicKeyTask extends AsyncTask<Void, Void, Exception> {
		StockAlertPubKey stockAlertObj;
		String senderCode;
		String brokerCode;
		String screenSize;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			senderCode 	= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
			brokerCode	= sharedPreferences.getString(PreferenceConstants.BROKER_CODE, "");
			screenSize	= SystemUtil.getDeviceScreenSize(QuoteActivity.this);
			
			
			if(mDialog != null)
				mDialog.setMessage(getResources().getString(R.string.stock_alert_progress_get_public_key));
			
			if(!mDialog.isShowing())
				mDialog.show();
		}

		@Override
		protected Exception doInBackground(Void... params) {
			
			try{
				stockAlertObj = StockAlertUtil.getPublicKey(brokerCode);
				
				if(stockAlertObj == null){
					throw new Exception("Error getting public key for stock alert.");
				}else{
					JSONObject claimJson = StockAlertUtil.getJsonForRegister(QuoteActivity.this, senderCode, brokerCode, screenSize);
					jwtToken = StockAlertUtil.generateJWTToken(stockAlertObj, QuoteActivity.this, brokerCode, claimJson);
					
					if(jwtToken == null){
						throw new Exception("Failed to generate JWT Token");
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				return e;
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Exception ex) {
			super.onPostExecute(ex);
			if(mDialog != null && mDialog.isShowing())
				mDialog.dismiss();
			
			if (ex != null) {
				DefinitionConstants.Debug("GetPublicKeyTask, Exception:" + ex.getMessage());
				showAlert(getResources().getString(R.string.quote_stock_alert_register_failed));
			}else{
				new RegisterDeviceTask().execute();
			} 
		}
	}
	
	private class RegisterDeviceTask extends AsyncTask<Void, Void, Exception> {

		private String brokerCode;
		private Exception ex = null;
		private String status;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			brokerCode = sharedPreferences.getString(PreferenceConstants.BROKER_CODE, "");
			
			if(mDialog != null)
				mDialog.setMessage(getResources().getString(R.string.stock_alert_progress_register));
			
			if(!mDialog.isShowing())
				mDialog.show();
		}

		@Override
		protected Exception doInBackground(Void... params) {
			
			try{
				status = StockAlertUtil.registerDevice(jwtToken, brokerCode);
				
				if(status == null || !status.equals("200"))
					throw new Exception("Failed register device");
			}
			catch(Exception e){
				ex = e;
			}
			
			return ex;
		}

		@Override
		protected void onPostExecute(Exception result) {
			super.onPostExecute(result);
			if(mDialog != null && mDialog.isShowing()){
				mDialog.dismiss();

				if (result != null) {
					showAlert(result.getMessage());
				}else{
					StockAlertUtil.saveDeviceRegisterFlag(QuoteActivity.this);
					showAlert(getResources().getString(R.string.stock_alert_enabled_msg));
				}
			}
		}
	}
	
	//READ_PHONE_STATE is considered a dangerous permission in Android M (6.0) you need to request the permission at runtime
	private void checkForPhoneStatePermission() {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

			if (ContextCompat.checkSelfPermission(QuoteActivity.this,
					Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

				// Should we show an explanation?
				if (ActivityCompat.shouldShowRequestPermissionRationale(
						QuoteActivity.this, Manifest.permission.READ_PHONE_STATE)) {

					showPermissionMessage();
				} else {
					ActivityCompat.requestPermissions(
							QuoteActivity.this,
							new String[] { Manifest.permission.READ_PHONE_STATE },
							REQUEST_PHONE_STATE);
				}
			}
		}
	}

	private void showPermissionMessage(){
		new AlertDialog.Builder(this)
		.setTitle("Read phone state")
		.setMessage("This app requires the permission to read phone state to continue")
		.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ActivityCompat.requestPermissions(QuoteActivity.this,
						new String[]{Manifest.permission.READ_PHONE_STATE},
						REQUEST_PHONE_STATE);
			}
		}).create().show();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
			String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		switch (requestCode) {
		case REQUEST_PHONE_STATE:

			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				regGcm();
			} else {
				String phone_state_permission_denied	= "Unable to register push notification without granting permission";
				showAlert(phone_state_permission_denied);
			}
			break;
		}
	}
	//Change_Request-20170119, ReqNo.15 - END
}