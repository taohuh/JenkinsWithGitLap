package com.n2nconnect.android.stock.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.StockApplication;

import android.app.Activity;

public class NewsCategory {
	
	private String source;
	private String category;
	private String parentCategory;
	private String name;
	private float seq;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSeq() {
		return seq;
	}

	public void setSeq(float seq) {
		this.seq = seq;
	}
	
	public static ArrayList<NewsCategory> getUniqueNewsSources(Activity activity, boolean isRequireAllNewsSource){
		 	
		ArrayList<NewsCategory> newsSources = new ArrayList<NewsCategory>();
		ArrayList<String> tempUniqueSource = new ArrayList<String>();
		
		if(isRequireAllNewsSource){
			NewsCategory allSource = new NewsCategory();
			allSource.setCategory("");
			allSource.setName("All");
			allSource.setSource("");
			
			newsSources.add(allSource);
		}
		
		StockApplication application = (StockApplication)activity.getApplication();
		List<NewsCategory> allNewsCategories = application.getNewsCategories();

		if(allNewsCategories != null) {
			for (Iterator<NewsCategory> itr = allNewsCategories.iterator(); itr.hasNext(); ) {
				NewsCategory newsCategory = itr.next();
				String source = newsCategory.getSource();

				if (!tempUniqueSource.contains(source)) {
					tempUniqueSource.add(source);

					NewsCategory tempNewsCategory = new NewsCategory();
					tempNewsCategory.setSource(source);
					tempNewsCategory.setCategory(newsCategory.getCategory());
					tempNewsCategory.setSeq(newsCategory.getSeq());
					tempNewsCategory.setName(AppConstants.elasticNewsSourceMap.get(source.toUpperCase()));
					newsSources.add(tempNewsCategory);
				}
			}
		}
		
		return newsSources;
	}
	
	public static ArrayList<NewsCategory> getNewsCategoryBySource(String source, Activity activity, boolean isRequireAllNewsCategory){
		
		ArrayList<NewsCategory> newsCategories = new ArrayList<NewsCategory>();
		if(isRequireAllNewsCategory){
			NewsCategory allCategory = new NewsCategory();
			allCategory.setCategory("");
			allCategory.setName("All");
			allCategory.setSource("");
			newsCategories.add(allCategory);
		}
		
		StockApplication application = (StockApplication) activity.getApplication();
		List<NewsCategory> allNewsCategories = application.getNewsCategories();
		
		for(NewsCategory nCat: allNewsCategories){
			if(nCat.getSource().equalsIgnoreCase(source)){
				newsCategories.add(nCat);
			}
		}
		
		if(isRequireAllNewsCategory && newsCategories.size() == 2){
			newsCategories.remove(1);
		}
		
		return newsCategories;
	}
}