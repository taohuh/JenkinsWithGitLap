package com.n2nconnect.android.stock.model;

import java.util.Date;

import com.n2nconnect.android.stock.AppConstants;

public class TimeSales {

	private Date time;
	private char tradedat;
	private float price;
	private long volume;
	private int value;
	private int recordsNo;
	// Sonia@20130918 - Change_Request-20130225, ReqNo.5 - START
	private char foreign;
	private String buyerBrokerHouse;
	private char domestic;
	private String sellerBrokerHouse;
	// Sonia@20130918 - Change_Request-20130225, ReqNo.5 - END
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public char getTradedat() {
		return tradedat;
	}
	public void setTradedat(char tradedat) {
		this.tradedat = tradedat;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public long getVolume() {
		return volume;
	}
	public void setVolume(long volume) {
		this.volume = volume;
	}
	public float getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public int getRecordsNo() {
		return recordsNo;
	}
	public void setRecordsNo(int recordsNo) {
		this.recordsNo = recordsNo;
	}
	// Sonia@20130918 - Change_Request-20130225, ReqNo.5 - START
	public char getForeign() {
		return foreign;
	}
	public void setForeign(char foreign) {
		this.foreign = foreign;
	}
	public String getBuyerBrokerHouse() {
		return buyerBrokerHouse;
	}
	public void setBuyerBrokerHouse(String buyerBrokerHouse) {
		this.buyerBrokerHouse = buyerBrokerHouse;
	}
	public char getDomestic() {
		return domestic;
	}
	public void setDomestic(char domestic) {
		this.domestic = domestic;
	}
	public String getSellerBrokerHouse() {
		return sellerBrokerHouse;
	}
	public void setSellerBrokerHouse(String sellerBrokerHouse) {
		this.sellerBrokerHouse = sellerBrokerHouse;
	}
	// Sonia@20130918 - Change_Request-20130225, ReqNo.5 - END
	
	//Added by Thinzar@20140402, Change_Request-20150402, ReqNo. 2 - START
	//@int viewPreference: 0=unit, 1=lot
	public long getVolume(int viewPreference, int sharePerLot) {
		if(viewPreference == AppConstants.INT_TRADE_QTY_IN_UNIT)
			return volume;
		else
			return volume/sharePerLot;
	}
	//Added by Thinzar@20140402, Change_Request-20150402, ReqNo. 2 - END
}
