package com.n2nconnect.android.stock;

import com.n2nconnect.android.stock.util.QcHttpConnectUtil;

public class QCAliveThread extends Thread {
	private OnQCAliveListener registeredListener;
	private int refreshRate;
	private String userParam;
	public static volatile boolean stopRequested; 
	private Thread runThread;
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
	private boolean mPaused;
	
	public OnQCAliveListener getRegisteredListener() {
		return registeredListener;
	}
	
	public void setRegisteredListener(OnQCAliveListener registeredListener) {
		this.registeredListener = registeredListener;
	}

	public String getUserParam() {
		return userParam;
	}

	public void setUserParam(String userParam) {
		this.userParam = userParam;
	}
	
	public interface OnQCAliveListener {
		public void QCAliveEvent(boolean isResume);
	}
	
	public QCAliveThread(String userParam, int refreshRate) {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		this.mPaused		= false;
		this.userParam = userParam;
		this.refreshRate = refreshRate;
		this.stopRequested = false;
	}

	public void run(){
		
		while(!stopRequested){
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			synchronized (this) {
				while (mPaused) {
					try {
						wait();
					} catch (InterruptedException e) {
					}
				}
			}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			
			try{
				
				runThread = Thread.currentThread();
				
				if (refreshRate > 0) {
					long waitTime = refreshRate * 1000;
					Thread.sleep(waitTime);
				} else if (refreshRate == 0) {
					Thread.sleep(AppConstants.QC_KEEP_ALIVE_RATE * 1000);
					continue;
				} else {				
					Thread.sleep(AppConstants.QC_KEEP_ALIVE_RATE * 1000);
				}
				
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				Map parameters = new HashMap();
				parameters.put(ParamConstants.USERPARAM_TAG, userParam);

				QcHttpConnectUtil.QCAlive(parameters); // NOTE : No retry perform on function execute in thread
				*/ 
				QcHttpConnectUtil.QCAlive(userParam); // NOTE : No retry perform on function execute in thread
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				if (registeredListener != null) {
					registeredListener.QCAliveEvent(true);
				}

			}catch (InterruptedException e) {
				e.printStackTrace();
			} catch (FailedAuthenicationException e) {				
				e.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public void stopRequest() {
        stopRequested = true;

        if (runThread != null ) {
            runThread.interrupt();
        }
    }
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	public void pauseRequest() {
		synchronized (this){
			mPaused = true;
		}
	}

	public void resumeRequest() {
		synchronized (this){
			mPaused = false;
			notify();
		}
	}
	
	public boolean isPaused(){
		return mPaused;
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
}
