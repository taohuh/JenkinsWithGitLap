package com.n2nconnect.android.stock;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.model.TradeOrder;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;

public class TradeStatusThread extends Thread {

	private onRefreshTradeStatusListener registeredListener;
	private String userParam;
	private int refreshRate;
	private volatile boolean stopRequested; 
	private List<TradeOrder> tradeOrders;
	private Thread runThread;
	private TradeAccount tradeAccounts;
	/*Mary@20130529 - comment unused variable
	private String latestTimeStamp;
	*/
	private String senderCode;
	private boolean mPaused;

	public onRefreshTradeStatusListener getRegisteredListener() {
		return registeredListener;
	}

	public String getUserParam() {
		return userParam;
	}

	public void setUserParam(String userParam) {
		this.userParam = userParam;
	}
	
	public void setTradeAcount(TradeAccount tradeAccounts){
		this.tradeAccounts = tradeAccounts;
	}
	
	public TradeAccount getTradeAccount(){
		return tradeAccounts;
	}
	
	/* Mary@20130529 - comment unused variable
	public void setTimeStamp(String timeStamp){
		this.latestTimeStamp = timeStamp;
	}
	*/

	public void setSenderCode(String senderCode){
		this.senderCode = senderCode;
	}
	
	public void setTradeAccount(TradeAccount tradeAccounts){
		this.tradeAccounts = tradeAccounts;
	}
	
	public void setRegisteredListener(onRefreshTradeStatusListener registeredListener) {
		this.registeredListener = registeredListener;
	}

	public interface onRefreshTradeStatusListener {
		public void OnTradeRefreshEvent(List<TradeOrder> tradeorders);
	}

	public TradeStatusThread(String userParam, int refreshRate, TradeAccount tradeAccounts, String senderCode){
		mPaused = false;
		this.userParam = userParam;
		this.refreshRate = refreshRate;
		stopRequested = false;
		this.tradeAccounts = tradeAccounts;
		this.senderCode = senderCode;
	}

	public void run() {

		while (!stopRequested) {
			System.out.println("TradeStatusThread: run()");
			synchronized (this) {
				while (mPaused) {
					try {
						wait();
					} catch (InterruptedException e) {
					}
				}
			}
			
			try {
				
				runThread = Thread.currentThread();

				if (refreshRate > 0) {
					long waitTime = refreshRate * 1000;
					Thread.sleep(waitTime);
				} else if (refreshRate == 0) {
					Thread.sleep(AppConstants.DEFAULT_REFRESH_RATE * 1000);
					continue;
				} else {				
					Thread.sleep(AppConstants.DEFAULT_REFRESH_RATE * 1000);
				}
				
				long latestTimeStamp = 0;
				
				Map<String, Object> parameters = new HashMap<String, Object>();
				
				for (Iterator<TradeOrder> itr=DefinitionConstants.tradeOrders.iterator(); itr.hasNext(); ) {
					TradeOrder order	= itr.next();
					String arr[] 		= order.getLastUpdate().split("\\.");
					String newTimeStamp = arr[0]+arr[1];
					long update 		= Long.parseLong(newTimeStamp);

					if(update > latestTimeStamp){
						latestTimeStamp = update;
					}
				}

				String timestamp = Long.toString(latestTimeStamp);

				if(timestamp!=null){
					parameters.put(ParamConstants.TIMESTAMP_TAG, FormatUtil.formatTimeStamp(timestamp));
				}
				
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
				/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
				if(AppConstants.EnableEncryption)
				*/
				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
				// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
					parameters.put(ParamConstants.USERPARAM_TAG, userParam);
				
				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
				if(tradeAccounts!=null){
					parameters.put(ParamConstants.ACCOUNTNO_TAG, tradeAccounts.getAccountNo());
					parameters.put(ParamConstants.BRANCHCODE_TAG, tradeAccounts.getBranchCode());
					//parameters.put(ParamConstants.BROKERCODE_TAG, tradeAccounts.getBrokerCode());	//Added By Thinzar@20140923 
				}else{
					parameters.put(ParamConstants.ACCOUNTNO_TAG, "");
					parameters.put(ParamConstants.BRANCHCODE_TAG, "");
					//parameters.put(ParamConstants.BROKERCODE_TAG, "");	//Added By Thinzar@20140923
				}
				
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - NOTE : not perform retry for thread
				String response = AtpConnectUtil.tradeStatus(parameters);
				tradeOrders		= AtpMessageParser.parseTradeStatus(response);
				
				if (registeredListener != null) {
					registeredListener.OnTradeRefreshEvent(tradeOrders);
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (FailedAuthenicationException e) {				
				e.printStackTrace();
				try {
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					List<String>data = QcHttpConnectUtil.relogin();
					userParam = data.get(0);
				} catch (FailedAuthenicationException e2) {
				*/
				AppConstants.QCdata	= QcHttpConnectUtil.relogin();
				userParam	= AppConstants.QCdata.getStrUserParam();
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}catch(Exception e2){
					e2.printStackTrace();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void stopRequest() {
		System.out.println("TradeStatusThread, stopRequest()");
        stopRequested = true;

        if (runThread != null ) {
            runThread.interrupt();
        }
    }
	
	public void pauseRequest() {
		synchronized (this){
			mPaused = true;
		}
	}

	public void resumeRequest() {
		System.out.println("TradeStatusThread, resumeRequest");
		synchronized (this){
			mPaused = false;
			notify();
		}
	}
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	public boolean isPaused(){
		return mPaused;
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
}
