package com.n2nconnect.android.stock.model;

import java.util.Date;

public class Watchlist implements Comparable<Watchlist> {
	private String id;
	private String name;
	private String clientCode;	
	private String exchangeCode;
	private String remark;
	private Date lastModified;
	private String flag;
	private String status;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getExchangeCode() {
		return exchangeCode;
	}
	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	@Override
	public int compareTo(Watchlist compare) {
		if (Integer.parseInt(id) == Integer.parseInt(compare.getId())) {
			return 0;
		} else if (Integer.parseInt(id) > Integer.parseInt(compare.getId())) {
			return 1;
		} else {
			return -1;
		}
		
	}
	
}
