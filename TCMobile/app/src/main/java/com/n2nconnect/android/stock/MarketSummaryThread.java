package com.n2nconnect.android.stock;

import java.util.HashMap;
import java.util.Map;

import com.n2nconnect.android.stock.model.MarketSummary;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;

public class MarketSummaryThread extends Thread{

	private OnMarketSummaryListener registeredListener;
	private String strSymbolCode;
	private int refreshRate;
	private volatile boolean stopRequested; 
	private Thread runThread;
	private boolean mPaused;
	
	public OnMarketSummaryListener getRegisteredListener() {
		return registeredListener;
	}

	public void setRegisteredListener(OnMarketSummaryListener registeredListener) {
		this.registeredListener = registeredListener;
	}
	
	public interface OnMarketSummaryListener {
		public void refreshMarketSummaryEvent(MarketSummary changedMarketSummary);
	}

	public MarketSummaryThread(String strSymbolCode, int refreshRate) {
		this.mPaused		= false;
		this.strSymbolCode 	= strSymbolCode;
		this.refreshRate 	= refreshRate;
		this.stopRequested 	= false;
	}

	public void run() {
		while (!stopRequested) {

			synchronized (this) {
				while (mPaused) {
					try {
						wait();
					} catch (InterruptedException e) {
					}
				}
			}

			try {
				runThread = Thread.currentThread();

				if (refreshRate > 0) {
					long waitTime = refreshRate * 1000;
					Thread.sleep(waitTime);
				} else if (refreshRate == 0) {
					Thread.sleep(AppConstants.DEFAULT_REFRESH_RATE * 1000);
					continue;
				} else {				
					Thread.sleep(AppConstants.DEFAULT_REFRESH_RATE * 1000);
				}

				MarketSummary marketSummary = null;

				if (strSymbolCode != null && strSymbolCode.trim().length() > 0) {
					Map<String, String> parameters	= new HashMap<String, String>();
					parameters.put(ParamConstants.SYMBOLCODE_TAG, strSymbolCode);
					
					String strExchangeCode 			= AppConstants.DEFAULT_EXCHANGE_CODE;
					if( AppConstants.DEFAULT_MARKET_CODE.equals(strSymbolCode) )
						strExchangeCode = AppConstants.DEFAULT_EXCHANGE_CODE;
					parameters.put(ParamConstants.EXCHANGECODE_TAG, strExchangeCode);
					
					String response = QcHttpConnectUtil.imageMarket(parameters);
					
					if(response == null)				
						throw new Exception(ErrorCodeConstants.FAIL_GET_MARKET_DETAIL);
					else
						marketSummary = QcMessageParser.parseMarketSummary(response);
				}else{
					marketSummary = new MarketSummary();
				}
				if (registeredListener != null) {
					registeredListener.refreshMarketSummaryEvent(marketSummary);
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (FailedAuthenicationException e) {				
				e.printStackTrace();
				try {
					AppConstants.QCdata	= QcHttpConnectUtil.relogin();
				}catch(Exception e2){
					e2.printStackTrace();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
	}

	public void stopRequest() {
		stopRequested = true;

		if (runThread != null ) {
			runThread.interrupt();
		}
	}

	public void pauseRequest() {
		synchronized (this){
			mPaused = true;
		}
	}

	public void resumeRequest() {
		synchronized (this){
			mPaused = false;
			notify();
		}
	}
	
	public void setStrSymbolCode(String strSymbolCode){
		this.strSymbolCode = strSymbolCode;
	}
	public String getStrSymbolCode(){
		return this.strSymbolCode;
	}
}
