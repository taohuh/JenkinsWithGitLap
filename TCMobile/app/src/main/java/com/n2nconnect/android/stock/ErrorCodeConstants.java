	package com.n2nconnect.android.stock;
	
	import com.n2nconnect.android.stock.util.SystemUtil;
	
	
	public class ErrorCodeConstants {
	
	private static final String strDefaultAdvice			= "\nPlease try again later.";
	private static final String strDefaultFurtherAction		= "\nIf problem persist, may contact our support for further assistance.";
	private static final String strFurtherAction_Relogin	= "\nIf problem persist, kindly relogin. If further assistance needed, may contact our support.";
	
	public static final String NO_DATA_MSG					= "No Data Available.";
	
	public static final String FAIL_GET_SESSION 			= "ERROR_1001-ATP Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_GET_ENCRYPTED_SESSION 	= "ERROR_1002-ATP Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_GET_KEY					= "ERROR_1003-ATP Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_GET_ENCRYPTED_KEY		= "ERROR_1004-ATP Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_GET_PUBLIC_KEY			= "ERROR_1005-ATP Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_UPDATE_AGREEMENT		= "ERROR_1006-Fail To Update Agreement." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_DEL_STOCK_IN_WATCHLIST	= "ERROR_1007-Fail To Delete Selected Stock From Watchlist." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_RETRIEVE_ACCINFO		= "ERROR_1008-Fail To Retrieve Account Information." + strDefaultAdvice	+ "\nThanks.";
	
	public static final String FAIL_RETRIEVE_ACCINFO_2		= "ERROR_1009-Fail To Retrieve Account Information." + strDefaultAdvice	+ "\nThanks.";
	
	public static final String FAIL_REGISTER_MAIL			= "ERROR_1010-Fail To Register Mail." + strDefaultAdvice	+ "\nThanks.";
	
	public static final String FAIL_FORGET_PASSWORD_REQ		= "ERROR_1011-Forget Password Request Fail." + strDefaultAdvice	+ "\nThanks.";
	
	public static final String FAIL_FORGET_PIN_REQ			= "ERROR_1012-Forget Pin Request Fail." + strDefaultAdvice  + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_TRADE					= "ERROR_1013-Fail To Trade." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_GET_TRADE_LIMIT			= "ERROR_1014-Fail To Get Trade Limit." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_CHANGE_PASSWORD			= "ERROR_1015-Fail To Change Password." + strDefaultAdvice	+ "\nThanks.";
	
	public static final String FAIL_CHANGE_PASSWORD_2		= "ERROR_1016-Fail To Change Password." + strDefaultAdvice	+ "\nThanks.";
	
	public static final String FAIL_CHANGE_PIN_2			= "ERROR_1017-Fail To Change Pin." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_DEL_WATCHLIST			= "ERROR_1018-Fail To Delete Selected Watchlist." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_UPDATE_WATCHLIST		= "ERROR_1019-Fail To Update Selected Watchlist." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_ADD_STOCK_INTO_WATCHLIST= "ERROR_1020-Fail To Add Selected Stock Into Watchlist." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_GET_WATCHLIST_STOCK		= "ERROR_1021-Fail To Retrieve Watchlist Details." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_GET_WATCHLIST			= "ERROR_1022-Fail To Retrieve Watchlist." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_GET_PORTFOLIO			= "ERROR_1023-Fail To Retrieve Portfolio." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_GET_TRADE_STATUS		= "ERROR_1024-Fail To Retrieve Trade Status." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_GET_TRADE_CLIENT		= "ERROR_1025-Fail To Retrieve Trade Client." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	public static final String FAIL_GET_QC_KEY				= "ERROR_1026-QC Connection Handshake Fail." + strDefaultAdvice	+ strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_LOGIN_QC				= "ERROR_1027-QC Connection Handshake Fail." + strDefaultAdvice	+ strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_LOAD_IMAGE				= "ERROR_1028-Fail To Load Line Chart." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_GET_SECTOR				= "ERROR_1029-Fail To Get Sector Details." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_GET_INDICES				= "ERROR_1029-Fail To Get Indices Details." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_GET_PATH				= "ERROR_1030-Fail To Get Line Chart Details." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_GET_NEWS				= "ERROR_1031-Fail To Get News." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_GET_UPDATED_NEWS		= "ERROR_1032-Fail To Get Updated News." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_GET_MARKET_DETAIL		= "ERROR_1032-Fail To Get Market Details." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_GET_SORTED_DETAIL		= "ERROR_1033-Fail To Get Sorted Details." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_GET_STOCK_DETAIL		= "ERROR_1034-Fail To Get Stock Details." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_GET_QUOTE_DETAIL		= "ERROR_1035-Fail To Get Quote Details." + strDefaultAdvice + "\nThanks.";
	
	// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.11
	
	public static final String FAIL_GET_CURRENCY_EXCHANGE_DETAIL
															= "ERROR_1036-Fail To Get Currency Exchange Rate Details." + strDefaultAdvice + "\nThanks.";
	
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
	public static final String FAIL_GET_E2EE_PARAM			= "ERROR_1037-ATP Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	public static final String FAIL_TRADE_LOGIN				= "ERROR_1038-ATP Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
	
	// Added by Mary@20130121 - Change_Request-20130104, ReqNo.3
	public static final String FAIL_UPD_RDS_STATUS			= "ERROR_1039-Fail To Update RDS Status." + strDefaultAdvice + "\nThanks.";
	
	// Added by Mary@20130204 - while login shouldn't show session expired error message - START
	public static final String FAIL_AUTHENTICATION_IOE		= "ERROR_1040-ATP Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	
	// Added by Mary@20130205 - Change_Request-201301014, ReqNo.1
	public static final String FAIL_GET_VALID_E2EE_PARAM	= "ERROR_1042-ATP Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	
	//Added by Mary@20130308 - Fixes_Request-20130108, ReqNo.21
	public static final String NO_LMS_SUBSCRIPTION			= "ERROR_1041-Failed To Gather User Subscribed Exchange List.\nKindly Ensure Neccessary Subscription Has Been Made.\nThanks.";
	
	//Added by Kw@20130502 - Fix_Request-20130426, ReqNo.3
	public static final String Fail_GET_MARKET_DEPTH 		= "ERROR_1042-Failed To Get Market Depth Details.";
	
	// Added by Mary@20130823 - Fixes_Request-20130711, ReqNo.16
	public static final String FAIL_GET_LMS_SERVICE			= "ERROR_1043-LMS Connection Handshake Fail." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	
	//Added by Thinzar - Change_Request-20140701,ReqNo.1 & 6 
	public static final String FAIL_GET_CONFIG_FILE 		= "ERROR_1044-Failed to get configuration file. Please check your network connection and try again! " + strDefaultFurtherAction + "\nThanks.";
	
	//Added by Thinzar - Change_Request-20160101, ReqNo.2
	public static final String FAIL_PROCESS_LMS				= "ERROR_1045-LMS Login, process failure. " + strDefaultAdvice + strDefaultFurtherAction;
	
	//Added by Thinzar - Change_Request-20160101, ReqNo7
	public static final String FAIL_GET_TRADE_HISTORY		= "ERROR_1046-Failed to Get Trade History." + strDefaultAdvice + strFurtherAction_Relogin + "\nThanks.";
	
	//Fixes_Request-20170103, ReqNo.7
	public static final String FAIL_TRADE_UNEXPECTED		= "Unable to receive confirmation from the server. Your order may not have been submitted successfully. \nPlease verify your order status or check with your Trading Representative before re-submitting the order.";
	
	//Added by Thinzar - Change_Request 20160101, ReqNo.1
	public static final String SENDER_CODE_NOT_MATCH		= "ERROR_9981-Unexpected Error, please relogin again.";
	
	//Added by Thinzar - Change_Request-20150901, ReqNo.1
	public static final String FAIL_CONNECT_ATP				= "ERROR_9982-Sorry. We have lost your session, please try to login again";
	
	public static final String NETWORK_UNAVAILABLE 			= "ERROR_9983-Network connection interrupted, please check your network and try again." + strFurtherAction_Relogin + "\nThanks.";
	
	//Added by Mary@20131011 - Fixes_Request-20130711, ReqNo.33
	public static final String INVALID_ATP_REQUEST_DATA		= "ERROR_9984-Invalid ATP Request Data." + strDefaultAdvice + "\nThanks.";
	
	/** Exception Brief Information 
	 * (MUE) MalformedURLException 	- occurs when URL is not invalid and can't be parse like incorrect format or protocol
	 * (IOE) IOException			- occurs when URL valid but not accessible
	 * (NPE) NullPointerException	- occurs when item is null
	 * () BackendProcessError		- own define execption to handle error message of exception happen on backend process like store procedure variable undefined
	 **/
	
	// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14
	public static final String ATP_INVALID_RESPONSE			= "ERROR_9985-Invalid ATP Response." + strDefaultAdvice + "\nThanks.";
	
	// Added by Mary@20130823 - Fixes_Request-20130711, ReqNo.16
	public static final String OS_CAUSE_INVALID_CERT		= "ERROR_9986-Fail to validate SSL Certificate." 
																+ ( ( SystemUtil.getDeviceOSVersion() != null & SystemUtil.getDeviceOSVersion().trim().length() > 0
																		& ( Double.parseDouble( SystemUtil.getDeviceOSVersion().substring(0, SystemUtil.getDeviceOSVersion().lastIndexOf(".") ) ) ) < 3.0 
																	) ? 
																	"\n\nP/S : Detected device android version is " + SystemUtil.getDeviceOSVersion()  
																	+ " while app supported version is 3.0 or above. Please upgrade device android version." : "" );
	
	//Added by Mary@20130705 - public key error - START
	public static final String MISSING_PUBLIC_KEY			= "ERROR_9987-Fail to Get Initial Data." + strDefaultAdvice + "\nThanks.";
	public static final String FAIL_TO_READ_PB_KEY			= "ERROR_9988-Fail to Get Initial Data." + strDefaultAdvice + "\nThanks.";
	public static final String NO_SUCH_ALGORITHM			= "ERROR_9989-Fail to Get Initial Data." + strDefaultAdvice + "\nThanks.";
	public static final String INVALID_KEY_SPEC_EXCEPTION	= "ERROR_9990-Fail to Get Initial Data." + strDefaultAdvice + "\nThanks.";
	public static final String MISSING_PRIVATE_KEY			= "ERROR_9987-Fail to Get Initial Data." + strDefaultAdvice + "\nThanks.";
	//Added by Mary@20130705 - public key error - END
	
	// Added by Mary@20130606 - app process error
	public static final String FAIL_PROCESS_ERROR			= "ERROR_9991-Fail to Process Data." + strDefaultAdvice + "\nThanks.";
	
	public static final String FAIL_COMMUNICATION_CHART		= "ERROR_9992-Invalid Chart URL Request.";
	
	public static final String FAIL_COMMUNICATION_QC		= "ERROR_9992-Invalid QC URL Request.";
	
	public static final String strBackEndProcessErrorCode	= "ERROR_9993";
	
	public static final String FAIL_BACKEND_PROCESS			= strBackEndProcessErrorCode + "-Backend Process Return Fail."	+ strDefaultAdvice	+ strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_COMMUNICATION_UUE		= "ERROR_9994-Fail to Read Data."	+ strDefaultAdvice	+ strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_COMMUNICATION_NPE		= "ERROR_9995-Fail To Retrieve Data."	+ strDefaultAdvice	+ strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_COMMUNICATION_MUE		= "ERROR_9996-Invalid URL Request.";
	
	public static final String FAIL_COMMUNICATION_EOF		= "ERROR_9997-Fail To Retrieve Data."	+ strDefaultAdvice	+ strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_COMMUNICATION_IOE		= "ERROR_9998-Network/Server Not Reachable." + strDefaultAdvice + strDefaultFurtherAction + "\nThanks.";
	
	public static final String FAIL_COMMUNICATION_E			= "ERROR_9999."	+ strDefaultAdvice	+ strDefaultFurtherAction + "\nThanks.";
	
	//Added by Thinzar@20141110, Change_Request-20141101, ReqNo. 2 
	//Warning Messages (on orderPad)
	public static final String STOP_BUY_WARNING_MSG			= "To submit a Buy Stop order, Trigger Price should be > Trigger (LastTrade, BestBid, BestOffer).";
	public static final String STOP_SELL_WARNING_MSG		= "To submit a Sell Stop Order, Trigger Price should be < Trigger (LastTrade, BestBid, BestOffer).";
	public static final String IFTOUCH_BUY_WARNING_MSG		= "To submit a Buy If-Touch Order, Trigger Price should be < Trigger (LastTrade, BestBid, BestOffer).";
	public static final String IFTOUCH_SELL_WARNING_MSG		= "To submit a Sell If-Touch Order, Trigger Price should be > Trigger (LastTrade, BestBid, BestOffer).";
	
	//Added by Thinzar, Change_Request-20141101, ReqNo. 16
	public static final String REQUIRE_LOGIN_TCPLUS			= "Dear Customer, for first time login, please visit TC Plus at TC_PLUS_LINK to change your password, trading pin and security questions.";

	//Elastic News
	public static final String FAIL_WRITE_EXTERNAL_STORAGE 	= "Unable to create file/folder in external storage. Please try again later.";

	public static final String ROOT_DETECTED 				= "Kindly note that this application is only supported on unmodified version of Android - code";

	/* Mary@20130918 - Fixes_Request-20130711, ReqNo.21
	public static boolean isOwnException(String strErrMsg){
	*/
	public static boolean isOwnException(String strErrMsg) throws Exception{
		if(strErrMsg != null){
			strErrMsg			= strErrMsg.toUpperCase();
			int intReturnInd	= strErrMsg.indexOf("\r");
		    int intNewLineInd	= strErrMsg.indexOf("\n");
		    
		    if( intReturnInd == -1 || (intNewLineInd != -1 && intReturnInd > intNewLineInd) )
		    	strErrMsg = strErrMsg.split("\n")[0];
		    else
		    	strErrMsg = strErrMsg.split("\r")[0];
		    
			if( strErrMsg.matches(".*?\\b\\s*ATP\\s*\\b.*?") || strErrMsg.matches(".*?\\b\\s*" + ErrorCodeConstants.strBackEndProcessErrorCode + "\\s*\\b.*?") || strErrMsg.matches(".*?\\b\\s*QC\\s*\\b.*?") )
				return true;
		}
		
		return false;
	}
	
	/* Mary@20130918 - Fixes_Request-20130711, ReqNo.21
	public static boolean isATPBackendResponseErrMsg(String strMsg){
	*/
	public static boolean isATPBackendResponseErrMsg(String strMsg) throws Exception{
		if(strMsg != null){
			strMsg				= strMsg.toUpperCase();
			int intReturnInd	= strMsg.indexOf("\r");
		    int intNewLineInd	= strMsg.indexOf("\n");
		    
		    if( intReturnInd == -1 || (intNewLineInd != -1 && intReturnInd > intNewLineInd) )
		    	strMsg = strMsg.split("\n")[0];
		    else
		    	strMsg = strMsg.split("\r")[0]; 
		    
		    if( strMsg.toUpperCase().matches(".*?\\b\\s*" + AppConstants.ERROR_STRING + "\\s*\\b.*?\\b\\s*ATP\\s*\\b.*?") )
		    	return true;
		}
	    
	    return false;
	}
	
	/* Mary@20130918 - Fixes_Request-20130711, ReqNo.21
	public static boolean isBackendExceptionMsg(String strMsg){
	*/
	public static boolean isBackendExceptionMsg(String strMsg) throws Exception{
		if(strMsg != null){
			strMsg				= strMsg.toUpperCase();
			int intReturnInd	= strMsg.indexOf("\r");
		    int intNewLineInd	= strMsg.indexOf("\n");
		    
		    if( intReturnInd == -1 || (intNewLineInd != -1 && intReturnInd > intNewLineInd) )
		    	strMsg = strMsg.split("\n")[0];
		    else
		    	strMsg = strMsg.split("\r")[0]; 
		    
		    if( strMsg.toUpperCase().matches(".*?\\b\\s*" + AppConstants.ERROR_STRING + "\\s*\\b.*?") )
		    	return true;
		}
	    
	    return false;
	}
	
	/* Mary@20130918 - Fixes_Request-20130711, ReqNo.21
	public static boolean isInvalidATPSessionErrMsg(String strMsg){
	*/
	public static boolean isInvalidATPSessionErrMsg(String strMsg) throws Exception{
		if(strMsg != null){
			strMsg				= strMsg.toUpperCase();
		
			int intReturnInd	= strMsg.indexOf("\r");
		    int intNewLineInd	= strMsg.indexOf("\n");
		    
		    if( intReturnInd == -1 || (intNewLineInd != -1 && intReturnInd > intNewLineInd) )
		    	strMsg = strMsg.split("\n")[0];
		    else
		    	strMsg = strMsg.split("\r")[0];
		    /* Mary@20130625 - Fixes_Request-20130523, ReqNo.18
		    if( strMsg.toUpperCase().matches(".*?ERROR.*?401.*?1100.*?ATP.*?") )
		    */
		    if( strMsg.toUpperCase().matches(".*?ERROR.*?401.*?1100.*?ATP.*?") || strMsg.toUpperCase().matches(".*?ERROR.*?401.*?1201.*?ATP.*?") )
		    	return true;
		}
	    
	    return false;
	}
	
	/* Mary@20130918 - Fixes_Request-20130711, ReqNo.21
	public static boolean isInvalidATPEncryptionKeyErrMsg(String strMsg){
	*/
	public static boolean isInvalidATPEncryptionKeyErrMsg(String strMsg) throws Exception{
		if(strMsg != null){
			strMsg				= strMsg.toUpperCase();
		
			int intReturnInd	= strMsg.indexOf("\r");
		    int intNewLineInd	= strMsg.indexOf("\n");
		    
		    if( intReturnInd == -1 || (intNewLineInd != -1 && intReturnInd > intNewLineInd) )
		    	strMsg = strMsg.split("\n")[0];
		    else
		    	strMsg = strMsg.split("\r")[0];
		    
		    if( strMsg.toUpperCase().matches(".*?ERROR.*?500.*?1116.*?ATP.*?") )
		    	return true;
		}
	    
	    return false;
	}
	
	/* Mary@20130918 - Fixes_Request-20130711, ReqNo.21
	public static boolean isInvalidQCClient(String strMsg){
	*/
	public static boolean isInvalidQCClient(String strMsg) throws Exception{
		if(strMsg != null){
			strMsg				= strMsg.toUpperCase();
			int intReturnInd	= strMsg.indexOf("\r");
		    int intNewLineInd	= strMsg.indexOf("\n");
		    
		    if( intReturnInd == -1 || (intNewLineInd != -1 && intReturnInd > intNewLineInd) )
		    	strMsg = strMsg.split("\n")[0];
		    else
		    	strMsg = strMsg.split("\r")[0];
		    
		    if( strMsg.toUpperCase().matches(".*?ERROR.*?401.*?INVALID.*?CLIENT.*?") )
		    	return true;
		}
	    
	    return false;
	}
	
	/* Mary@20130918 - Fixes_Request-20130711, ReqNo.21
	public static boolean isIOAutheticationNull(String strMsg){
	*/
	public static boolean isIOAutheticationNull(String strMsg) throws Exception{
		if(strMsg != null){
			strMsg				= strMsg.toUpperCase();
			int intReturnInd	= strMsg.indexOf("\r");
		    int intNewLineInd	= strMsg.indexOf("\n");
		    
		    if( intReturnInd == -1 || (intNewLineInd != -1 && intReturnInd > intNewLineInd) )
		    	strMsg = strMsg.split("\n")[0];
		    else
		    	strMsg = strMsg.split("\r")[0];
		    
		    /* Mary@20130624 - Fixes_Request-20130523, ReqNo.18
		    if( strMsg.toUpperCase().matches(".*?RECEIVED AUTHENTICATION CHALLENGE IS NULL.*?") )
		    */
		    if( strMsg.toUpperCase().matches(".*?RECEIVED AUTHENTICATION CHALLENGE IS NULL.*?") || strMsg.toUpperCase().matches(".*?NO AUTHENTICATION CHALLENGES FOUND.*?") )
		    	return true;
		}
	    
	    return false;
	}
	
	/* Mary@20130918 - Fixes_Request-20130711, ReqNo.21
	// Added by Mary@20130823 - Fixes_Request-20130711, ReqNo.16 - START
	public static boolean isExtCertPathValidatorException(String strMsg){
	*/
	public static boolean isExtCertPathValidatorException(String strMsg) throws Exception{
		if(strMsg != null){
			strMsg				= strMsg.toUpperCase();
			int intReturnInd	= strMsg.indexOf("\r");
		    int intNewLineInd	= strMsg.indexOf("\n");
		    
		    if( intReturnInd == -1 || (intNewLineInd != -1 && intReturnInd > intNewLineInd) )
		    	strMsg = strMsg.split("\n")[0];
		    else
		    	strMsg = strMsg.split("\r")[0];
		    
		    if( strMsg.toUpperCase().matches(".*?ORG.BOUNCYCASTLE.JCE.EXCEPTION.EXTCERTPATHVALIDATOREXCEPTION.*?COULD NOT VALIDATE CERTIFICATE SIGNATURE.*?") )
		    	return true;
		}
	    
	    return false;
	}
	// Added by Mary@20130823 - Fixes_Request-20130711, ReqNo.16 - END
}
