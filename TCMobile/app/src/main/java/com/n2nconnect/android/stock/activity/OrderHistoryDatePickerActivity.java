package com.n2nconnect.android.stock.activity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class OrderHistoryDatePickerActivity extends CustomWindow implements OnDoubleLoginListener {

	private EditText txtDateFrom;
	private EditText txtDateTo;
	private Button btnSearch;
	private Button btnCancel;
	private TextView txtOrderHistoryDuration;

	private Calendar calendar;
	private static final int FROM_DATE_DIALOG_ID = 1;
	private static final int TO_DATE_DIALOG_ID = 2;
	
	private String strDateFrom;
	private String strDateTo;
	private String errMsg;
	private AlertDialog.Builder alert;
	private SimpleDateFormat sdf;
	private Date today;
	
	private DoubleLoginThread loginThread;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.order_history_datepicker);
		this.title.setText(getResources().getString(R.string.ord_history_module_title));
		this.icon.setImageResource( AppConstants.brokerConfigBean.getIntOrdBookMenuIconBgImg() );

		txtDateFrom = (EditText) findViewById(R.id.fromDate);
		txtDateTo 	= (EditText) findViewById(R.id.toDate);
		btnSearch 	= (Button) findViewById(R.id.btnSearch);
		btnCancel 	= (Button) findViewById(R.id.btnCancel);
		txtOrderHistoryDuration	= (TextView) findViewById(R.id.txtOrderHistoryDuration);

		calendar 	= Calendar.getInstance();
		sdf 		= new SimpleDateFormat("dd-MM-yyyy");
		today		= new Date();
		
		Date hintDateGap	= getDateGap(-1 * AppConstants.brokerConfigBean.getOrderHistoryDuration(), sdf.format(today));
		
		txtDateFrom.setHint(sdf.format(hintDateGap));
		txtDateTo.setHint(sdf.format(today));
		
		txtDateFrom.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				setDate(FROM_DATE_DIALOG_ID);
			}
		});

		txtDateTo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(txtDateFrom.getText().length() == 0){
					errMsg	= getResources().getString(R.string.ord_history_enter_from_date_first);
					alert	= createAlert(errMsg, txtDateFrom);
					alert.show();
				}else
					setDate(TO_DATE_DIALOG_ID);
			}
		});
		
		btnSearch.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				strDateFrom	= formatDate(txtDateFrom.getText().toString());
				strDateTo	= formatDate(txtDateTo.getText().toString());
				
				if(strDateFrom == null || strDateFrom.isEmpty()){
					errMsg	= getResources().getString(R.string.ord_history_from_date_required);	//"FROM date is required!";
					alert	= createAlert(errMsg, txtDateFrom);
					alert.show();
					
				} else if(strDateTo == null || strDateTo.isEmpty()){
					errMsg	= getResources().getString(R.string.ord_history_to_date_required);	//"TO date is required!";
					alert	= createAlert(errMsg, txtDateTo);
					alert.show();
					
				} else if(Long.parseLong(strDateFrom) > Long.parseLong(strDateTo)){
					errMsg	= getResources().getString(R.string.ord_history_date_error_msg);	//"End date cannot be earlier than start date!";
					alert	= createAlert(errMsg, txtDateTo);
					alert.show();
					
				} else {
					resetATPExpireTime();
					
					Intent data 		= new Intent();
					data.putExtra(ParamConstants.DATE_FROM_TAG, formatDate(txtDateFrom.getText().toString()));
					data.putExtra(ParamConstants.DATE_TO_TAG, formatDate(txtDateTo.getText().toString()));
					OrderHistoryDatePickerActivity.this.setResult(RESULT_OK, data); 
					OrderHistoryDatePickerActivity.this.finish();
				}
			}
		});
		
		btnCancel.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent data 		= new Intent();
				data.putExtra(ParamConstants.DATE_FROM_TAG, "");
				data.putExtra(ParamConstants.DATE_TO_TAG, "");
				OrderHistoryDatePickerActivity.this.setResult(RESULT_OK, data); 
				OrderHistoryDatePickerActivity.this.finish();
			}
		});
		
		txtOrderHistoryDuration.setText(getResources().getString(R.string.ord_history_note) + " " + AppConstants.brokerConfigBean.getOrderHistoryDuration());
		
		//initialize doublieLoginThread
		loginThread = ((StockApplication) OrderHistoryDatePickerActivity.this.getApplication()).getDoubleLoginThread();
	}

	private void setDate(final int id) {
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		
		DatePickerDialog dpd;
		dpd = new DatePickerDialog(OrderHistoryDatePickerActivity.this, 0, new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int month, int day) {
				
				if (id == FROM_DATE_DIALOG_ID){
					txtDateFrom.setText(day + "-" + (month+1) + "-" + year);
					txtDateTo.setText("");
				}
				else if (id == TO_DATE_DIALOG_ID){
					txtDateTo.setText(day + "-" + (month+1) + "-" + year);
				}
			}
		}, year, month, day);
		
		if(id == FROM_DATE_DIALOG_ID){
			dpd.getDatePicker().setMaxDate(today.getTime());
		}
		
		if(id == TO_DATE_DIALOG_ID){
			if(txtDateFrom.getText().toString().length() > 0){
				String df	= txtDateFrom.getText().toString();
				
				//Calendar c = Calendar.getInstance();	
				
				try {
					dpd.getDatePicker().setMinDate(sdf.parse(df).getTime());
					
					Date endDate	= getDateGap(AppConstants.brokerConfigBean.getOrderHistoryDuration(), df);
				    
					if(endDate.compareTo(today) < 0)
				    	dpd.getDatePicker().setMaxDate(endDate.getTime());
				    else
				    	dpd.getDatePicker().setMaxDate(today.getTime());
					
				} catch (ParseException e) {
				    e.printStackTrace();
				}
			} else{
				dpd.getDatePicker().setMaxDate(today.getTime());
			}
		}
		
		dpd.show();
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		if( idleThread != null && idleThread.isPaused() )
			idleThread.resumeRequest(false);
		
		if( loginThread != null && loginThread.isPaused() ){
			loginThread.setRegisteredListener(OrderHistoryDatePickerActivity.this);
			loginThread.resumeRequest();
		}
	}
	
	private Date getDateGap(int duration, String dateFrom){
		
		Calendar c = Calendar.getInstance();	
		
		try {
			
			c.setTime(sdf.parse(dateFrom));
		    c.add(Calendar.DAY_OF_YEAR, duration);  
			
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		
		return c.getTime();
	}
	
	private AlertDialog.Builder createAlert(String errorMsg, final EditText txtField){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryDatePickerActivity.this);
		builder
		//.setIcon(android.R.attr.alertDialogStyle)
		.setMessage(errorMsg)
		.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				txtField.requestFocus();
			}
		})
		.create();
		
		return builder;
	}
	
	//format dd-MM-yyyy to yyyyMMdd
	@SuppressLint("SimpleDateFormat")
	public static String formatDate(String selectedDate){
		if(selectedDate != null && !selectedDate.isEmpty()){
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			Date date	=null;
			SimpleDateFormat df2	=null;
			
			try {
				date = df.parse(selectedDate);
				df2 = new SimpleDateFormat("yyyyMMdd");
				
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return df2.format(date).toString();
		} else {
			return null;
		}
	}

	//to detect double login
	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj		= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if( OrderHistoryDatePickerActivity.this.mainMenu.isShowing() )
					OrderHistoryDatePickerActivity.this.mainMenu.hide();
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				
				if( !isFinishing() )
					OrderHistoryDatePickerActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};
	
	private void resetATPExpireTime(){
    	if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
    }
}