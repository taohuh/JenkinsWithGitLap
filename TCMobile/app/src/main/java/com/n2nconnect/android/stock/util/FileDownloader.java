package com.n2nconnect.android.stock.util;

import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * To download file and write to external storage
 */
public class FileDownloader {
    private static final int  MEGABYTE = 1024 * 1024;

    public static void downloadFile(String fileUrl, File directory) throws Exception{
        try{

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            int responseCode = urlConnection.getResponseCode();
            DefinitionConstants.Debug("FileDownloader response:" + responseCode);

            if(responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = urlConnection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(directory);
                //int totalSize   = urlConnection.getContentLength();

                byte[] buffer = new byte[MEGABYTE];
                int bufferLength = 0;
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fileOutputStream.write(buffer, 0, bufferLength);
                }

                fileOutputStream.flush();
                fileOutputStream.close();
                inputStream.close();

            }else{
                throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
            }

        }catch(FileNotFoundException e){
            e.printStackTrace();
            throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
        }catch(MalformedURLException e){
            e.printStackTrace();
            throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_MUE);
        }
        catch(IOException e){
            e.printStackTrace();
            throw new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_IOE);
        }
    }
}
