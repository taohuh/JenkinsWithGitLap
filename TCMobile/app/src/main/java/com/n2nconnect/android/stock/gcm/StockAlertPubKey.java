package com.n2nconnect.android.stock.gcm;

public class StockAlertPubKey {
	private String kty;
	private String kid;
	private String alg;
	private String publicKey;
	private String eObject;

	public String getKty() {
		return kty;
	}

	public void setKty(String kty) {
		this.kty = kty;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public String getAlg() {
		return alg;
	}

	public void setAlg(String alg) {
		this.alg = alg;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String geteObject() {
		return eObject;
	}

	public void seteObject(String eObject) {
		this.eObject = eObject;
	}
}