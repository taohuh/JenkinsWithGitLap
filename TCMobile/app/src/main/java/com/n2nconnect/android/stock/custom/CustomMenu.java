package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;

import android.content.Context;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

/**
 * CustomMenu class
 *
 * This is the class that manages our menu items and the popup window.
 *
 */
public class CustomMenu {

	/**
	 * Some global variables.
	 */
	private ArrayList<CustomMenuItem> menuItems;
	private OnMenuItemSelectedListener listener = null;
	private Context context = null;
	private LayoutInflater layoutInflater = null;
	private PopupWindow popupWindow = null;
	private boolean isShowing = false;
	private boolean hideOnSelect = true;
	private int rows = 0;
	private int itemsPerLineInPortraitOrientation = 3;
	private int itemsPerLineInLandscapeOrientation = 6;
	
	/**
	 * The interface for returning the item clicked.
	 */
	public interface OnMenuItemSelectedListener {
		public void menuItemSelectedEvent(CustomMenuItem selection);
	}
	
	/**
	 * Use this method to determine if the menu is currently displayed to the user.
	 * @return boolean isShowing
	 */	
	public boolean isShowing() { return isShowing; }
	
	/**
	 * This setting controls whether or not the menu closes after an item is selected.
	 * @param boolean doHideOnSelect
	 * @return void
	 */	
	public void setHideOnSelect(boolean doHideOnSelect) { hideOnSelect = doHideOnSelect; } 
	
	/**
	 * Use this method to decide how many of your menu items you'd like one a single line.
	 * This setting in particular applied to portrait orientation.
	 * @param int count
	 * @return void
	 */	
	public void setItemsPerLineInPortraitOrientation(int count) { itemsPerLineInPortraitOrientation = count; }
	
	/**
	 * Use this method to decide how many of your menu items you'd like one a single line.
	 * This setting in particular applied to landscape orientation.
	 * @param int count
	 * @return void
	 */	
	public void setItemsPerLineInLandscapeOrientation(int count) { itemsPerLineInLandscapeOrientation = count; }
	
	/**
	 * Use this method to assign your menu items. You can only call this when the menu is hidden.
	 * @param ArrayList<CustomMenuItem> items
	 * @return void
	 * @throws Exception "Menu list may not be modified while menu is displayed."
	 */	
	public synchronized void setMenuItems(ArrayList<CustomMenuItem> items) throws Exception {
		if (isShowing) {
			throw new Exception("Menu list may not be modified while menu is displayed.");
		}
		menuItems = items;
	}
	
	/**
	 * This is our constructor.  Note we require a layout inflater.  There is probably a way to
	 * grab one of these from the local context but I failed to find it.
	 * @param Context context
	 * @param OnMenuItemSelectedListener listener
	 * @param LayoutInflater lo
	 * @return void
	 */	
	public CustomMenu(Context context, OnMenuItemSelectedListener listener, LayoutInflater inflater) {
		this.listener = listener;
		menuItems = new ArrayList<CustomMenuItem>();
		this.context = context;
		layoutInflater = inflater;
	}
	
	/**
	 * Display your menu. Not we require a view from the parent.  This is so we can get
	 * a window token.  It doesn't matter which view on the parent is passed in.
	 * @param View view
	 * @return void
	 */	
	public synchronized void show(View view) {
		isShowing = true;
		boolean isLandscape = false;
		int itemCount = menuItems.size();
		if (itemCount<1) return; //no menu items to show
		if (popupWindow != null) return; //already showing
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		if (display.getWidth() > display.getHeight()) isLandscape = true;
		View menuView= layoutInflater.inflate(R.layout.custom_menu, null);
		popupWindow = new PopupWindow(menuView,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT, false);
        popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
        popupWindow.setWidth(display.getWidth());
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        
        int divisor = itemsPerLineInPortraitOrientation;
        if (isLandscape) divisor = itemsPerLineInLandscapeOrientation;
        int remainder = 0;
        if (itemCount < divisor) {
        	rows = 1;
        	remainder = itemCount;
        } else {
        	rows = (itemCount / divisor);
        	remainder = itemCount % divisor;
        	if (remainder != 0) rows++;
        }
        TableLayout table = (TableLayout) menuView.findViewById(R.id.customMenuTable);
        table.removeAllViews();
        for (int i=0; i < rows; i++) {
        	TableRow row = null;
    		TextView captionLabel = null;
    		ImageView iconImage = null;
    		//create headers
    		row = new TableRow(context);
    		row.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    		for (int j=0; j< divisor; j++) {
    			if (i*divisor+j >= itemCount) break;
    			final CustomMenuItem menuItem = menuItems.get(i*divisor+j);
    			View itemLayout = layoutInflater.inflate(R.layout.custom_menu_item, null);
    			//captionLabel = (TextView)itemLayout.findViewById(R.id.menuItemCaption);
    			//captionLabel.setText(menuItem.getCaption());
    			
    			iconImage = (ImageView)itemLayout.findViewById(R.id.menuItemIcon);
    			iconImage.setImageResource(menuItem.getImageResourceId());
    			itemLayout.setOnClickListener( new OnClickListener() {
				   @Override
				   public void onClick(View view) {
						listener.menuItemSelectedEvent(menuItem);
						if (hideOnSelect) hide();
				   }
				});
    			row.addView(itemLayout);
    		}
    		table.addView(row);
        }
	}
	
	/**
	 * Hide your menu.
	 * @return void
	 */	
	public synchronized void hide() {
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
		try{
			isShowing = false;
			if( popupWindow != null && popupWindow.isShowing() ) {
				popupWindow.dismiss();
				popupWindow = null;
			}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
		return;
	}
}
