package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.Sector;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

/**
 *
 * This is the class that manages our menu items and the popup window.
 */
public class ExchangeListView {

	/**
	 * Some global variables.
	 */
	private List<Map<String, Object>> listExchangeMap;
	private OnExchangListEventListener listener;
	private Context context;
	private LayoutInflater layoutInflater;
	private PopupWindow popupWindow;
	private boolean isShowing;
	private boolean hideOnSelect;
	private Map<String, Object> selectedExchangeMap;
	private Sector selectedSector;
	private int selectedExchgRow;
	private int selectedSectorRow;
	private View windowView;
	/* Mary@20130604 - comment unused variable
	private View view;
	*/
	private boolean isShowSector;
	private List<String> listExchangeHaveSector;
	private List<ExchangeInfo> exchangeInfos;
	private List<Sector> sectorList;
	private final int intOriHeight;
	private final int intOriMarginTop;
	private final int intOriMarginBottom;
	private final float fltOriWeight;
	
	public interface OnExchangListEventListener {
		public void exchangeListSelectedEvent(Map<String, Object> exchangeMap, Sector sector);
	}
	
	public void positionExchange() {
		ScrollView exchangeListScroll	= (ScrollView) windowView.findViewById(R.id.exchangeListScroll);		
		int index 						= 1;
		String selectedTrdExchgCode		= (String)selectedExchangeMap.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE);
		for(Iterator<Map<String, Object>> itr=listExchangeMap.iterator(); itr.hasNext(); ){
			Map<String, Object> compare = itr.next();
			if( ( String.valueOf( compare.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) ) ).equals(selectedTrdExchgCode) )
				break;

			index++;
		}
		int scrollHeight 			= (index * 35) - 80;
		if(scrollHeight>0)
			exchangeListScroll.scrollTo(0, scrollHeight);

		TableLayout tableLayout		= (TableLayout) windowView.findViewById(R.id.exchangeListTable);
		TableRow tableRow 			= (TableRow) tableLayout.getChildAt(index-1);
		tableRow.setBackgroundColor(Color.DKGRAY);
		
		LinearLayout llSelectedRow	= (LinearLayout) tableRow.getChildAt(0);
		llSelectedRow.setBackgroundColor(Color.DKGRAY);
		
		TextView exchangeLabel		= (TextView)llSelectedRow.findViewById(R.id.tv_exchangeLabel);
		exchangeLabel.setTextColor(Color.WHITE);
		
  		selectedExchgRow 				= index - 1;
  		
  		if(isShowSector){
   			LinearLayout.LayoutParams layoutParam	= null;
   			if( listExchangeHaveSector.contains( selectedTrdExchgCode ) ){
       			layoutParam = new LinearLayout.LayoutParams(0, intOriHeight, fltOriWeight);
       			layoutParam.setMargins(5, intOriMarginTop, 3, intOriMarginBottom);
       			constructSectorTableView(selectedTrdExchgCode);
       		}else{
       			layoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, intOriHeight);
       			layoutParam.setMargins(5, intOriMarginTop, 5, intOriMarginBottom);
       		}
   			exchangeListScroll.setLayoutParams(layoutParam);
  		}
	}
	
	public void positionSector() {
		ScrollView sectorListScroll				= (ScrollView) windowView.findViewById(R.id.exchangeSectorListScroll);
		
		if(selectedSector == null){
			sectorListScroll.scrollTo(0, 0);
			return;
		}
		
		String strSelectedSectorCode			= selectedSector.getSectorCode();
		String selectedTrdExchgCode				= (String)selectedExchangeMap.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE);
		for( Iterator<ExchangeInfo> itr=exchangeInfos.iterator(); itr.hasNext(); ){
			ExchangeInfo exchangeInfo = itr.next();
			if( exchangeInfo.getTrdExchgCode().equals(selectedTrdExchgCode) ){
				List<Sector> listSector	= exchangeInfo.getSectors();
				int index 						= 1;
				for( Iterator<Sector> itr2=listSector.iterator(); itr2.hasNext(); ){
					Sector sector	= itr2.next();
					if( sector.getSectorCode().equals(strSelectedSectorCode) )
						break;
		
					index++;
				}
				
				int scrollHeight;
				if(index>7){
					scrollHeight = (index * 70) - 80;
				}else{
					scrollHeight = (index * 35) - 80;
				}
				if(scrollHeight>0)
					sectorListScroll.scrollTo(0, scrollHeight);

				TableLayout tableLayout		= (TableLayout) windowView.findViewById(R.id.exchangeSectorListTable);
				TableRow tableRow 			= (TableRow) tableLayout.getChildAt(index-1);
				tableRow.setBackgroundColor(Color.DKGRAY);
				
				final TextView textLabel 	= (TextView) tableRow.getChildAt(0);
				textLabel.setBackgroundColor(Color.DKGRAY);
				textLabel.setTextColor(Color.WHITE);
				
		  		selectedSectorRow 				= index - 1;
			}
		}
	}
	
	
	public boolean isShowing() { 
		return isShowing; 
	}
		
	public void setHideOnSelect(boolean doHideOnSelect) { 
		hideOnSelect = doHideOnSelect; 
	} 
	
	public synchronized void setListExchangeMap(List<Map<String, Object>> exchangeMapLists) throws Exception {
		if (isShowing) {
			throw new Exception("Exchange List may not be updated while list is displayed.");
		}
		this.listExchangeMap = exchangeMapLists;
	}
	
	public List<Map<String, Object>> getListExchangeMap(){
		return this.listExchangeMap;
	}
	
	public synchronized void setListExchangeInfo(List<ExchangeInfo> exchangeInfoLists) throws Exception {
		this.exchangeInfos = exchangeInfoLists;
	}
	
	public List<ExchangeInfo> getListExchangeInfo(){
		return this.exchangeInfos;
	}
	
	public ExchangeListView(Context context, OnExchangListEventListener listener, LayoutInflater inflater, List<String> listExchangeHaveSector) {
		this.listener 							= listener;
		listExchangeMap							= new ArrayList<Map<String, Object>>();
		this.context 							= context;
		layoutInflater 							= inflater;
		this.listExchangeHaveSector				= listExchangeHaveSector;
		this.isShowSector						= this.listExchangeHaveSector != null && !listExchangeHaveSector.isEmpty() ? true : false;
		exchangeInfos							= new ArrayList<ExchangeInfo>();
		
		windowView 								= layoutInflater.inflate(R.layout.exchange_list_table_window, null);
		ScrollView exchangeListScroll			= (ScrollView) windowView.findViewById(R.id.exchangeListScroll);
		LinearLayout.LayoutParams layoutParam 	= (LinearLayout.LayoutParams) exchangeListScroll.getLayoutParams();
		intOriHeight							= layoutParam.height;
		intOriMarginTop							= layoutParam.topMargin;
		intOriMarginBottom						= layoutParam.bottomMargin;
		fltOriWeight							= layoutParam.weight;
	}
	
	public synchronized void show(View view, Map<String, Object> exchangeMap) {
		this.selectedExchangeMap	= exchangeMap;
		/* Mary@20130604 - comment unused variable
		this.view 					= view;
		*/
		isShowing					= true;
		int itemCount 				= listExchangeMap.size();

		if(itemCount<1)
			return;
		
		if(popupWindow != null)
			return; 
		
		Display display 			= ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		
        this.constructTableView();
              
		popupWindow = new PopupWindow(windowView, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, false);
        popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
        popupWindow.setWidth(display.getWidth());
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        
        windowView.post(new Runnable() {
        	public void run() {
        		ExchangeListView.this.positionExchange();
        	}
        });
	}
	
	private void constructTableView() {		
		final ScrollView scrollView				= (ScrollView) windowView.findViewById(R.id.exchangeListScroll);
		
		/* a) Amendment left scrollview to fully occupy the pop-up window */
		LinearLayout.LayoutParams layoutParam 	= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, intOriHeight);
		layoutParam.setMargins(10, intOriMarginTop, 5, intOriMarginBottom);
		scrollView.setLayoutParams(layoutParam);
		
		/* b) populate pop-up window left scrollview */
		final TableLayout tableLayout 			= (TableLayout) windowView.findViewById(R.id.exchangeListTable);
   	 	tableLayout.removeAllViews();
        int id 									= 0;
        for(Iterator<Map<String, Object>> itr=listExchangeMap.iterator(); itr.hasNext();){
        	Map<String, Object> exchangeMap	= itr.next();
       	 
            final TableRow tableRow			= new TableRow(context);   
            tableRow.setId(id);
            tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
            tableRow.setWeightSum(1);
            
            final LinearLayout llExchglist	= (LinearLayout) layoutInflater.inflate(R.layout.exchange_list_item, null);
            
            final TextView exchangeLabel	= (TextView)llExchglist.findViewById(R.id.tv_exchangeLabel);
            exchangeLabel.setText( String.valueOf( exchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );
            
            /*Commented by Thinzar, Change_Request-20160101, ReqNo.3, [decided to remove exchange icon in new TCMobile 2.0]
            final ImageView exchangeImage 	= (ImageView) llExchglist.findViewById(R.id.iv_exchangeImage);
			Integer resourceId 				= (Integer) exchangeMap.get( ParamConstants.PARAM_INT_EXCHG_IMAGE_ID );
			if(resourceId != null)
				exchangeImage.setImageResource( resourceId.intValue() );
			// Added by Mary@20130110 - handle null image source - START
			else
				exchangeImage.setImageResource( android.R.color.transparent );
            // Added by Mary@20130110 - handle null image source - END
			*/
            
            tableRow.addView(llExchglist);
            
            tableRow.setOnClickListener(new OnClickListener() {
	           	 public void onClick(View view) {
	           		final TableRow previousRow			= (TableRow) tableLayout.getChildAt(selectedExchgRow);
	           		LinearLayout llPrevExchgList		= (LinearLayout) previousRow.getChildAt(0);
	           		TextView previousLabel 				= (TextView) previousRow.findViewById(R.id.tv_exchangeLabel);
	           		
	           		llPrevExchgList.setBackgroundResource(R.color.PopWindowBackColor);
	           		previousRow.setBackgroundResource(R.color.PopWindowBackColor);
	           		previousLabel.setTextColor(Color.BLACK);
	           		 
	           		selectedExchangeMap 					= listExchangeMap.get( tableRow.getId() );
	           		selectedExchgRow 						= tableRow.getId();
	           		selectedSector							= null;

	           		llExchglist.setBackgroundColor(Color.DKGRAY);
	           		tableRow.setBackgroundColor(Color.DKGRAY);	           		 
	           		exchangeLabel.setTextColor(Color.WHITE);
	           		 
	           		if( isShowSector){
	           			LinearLayout.LayoutParams layoutParam	= null;
	           			String strExchgCode						= String.valueOf(selectedExchangeMap.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE));
	           			if( listExchangeHaveSector.contains( strExchgCode ) ){
		           			layoutParam = new LinearLayout.LayoutParams(0, intOriHeight, fltOriWeight);
		           			layoutParam.setMargins(5, intOriMarginTop, 3, intOriMarginBottom);
		           			constructSectorTableView(strExchgCode);
		           		}else{
		           			layoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, intOriHeight);
		           			layoutParam.setMargins(5, intOriMarginTop, 5, intOriMarginBottom);
		           			listener.exchangeListSelectedEvent(selectedExchangeMap, null);
			           		 if(hideOnSelect)
			           			 ExchangeListView.this.hide();
		           		}
		           		scrollView.setLayoutParams(layoutParam);
	           		}else{
	           			listener.exchangeListSelectedEvent(selectedExchangeMap, null);
						if(hideOnSelect)
							ExchangeListView.this.hide();
	           		}
	           	 }
            });          
            
            tableLayout.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );  
            id++;
       }
	}
	
	private void constructSectorTableView(String selectedTradeExchangeCode) {
		if(exchangeInfos == null || ( exchangeInfos != null && exchangeInfos.isEmpty() ) )
			return;
		
		/* a) Get selected exchange sector list */
		sectorList	= null;
		for(Iterator<ExchangeInfo> itr = exchangeInfos.iterator(); itr.hasNext();){
			ExchangeInfo Info	= itr.next();
			if( Info.getTrdExchgCode() != null && Info.getTrdExchgCode().equals(selectedTradeExchangeCode) )
				sectorList = Info.getSectors();
		}
		
		if(sectorList == null || ( sectorList != null && sectorList.isEmpty() ) )
			return;
		
		/* b) populate pop-up window right scrollview */
		final TableLayout tableLayout 	= (TableLayout) windowView.findViewById(R.id.exchangeSectorListTable);
   	 	tableLayout.removeAllViews();
        int id 							= 0;
        for(Iterator<Sector> itr=sectorList.iterator(); itr.hasNext();){
        	Sector sector					= itr.next();
       	 
            final TableRow tableRow			= new TableRow(context);   
            tableRow.setId(id);
            tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
            tableRow.setWeightSum(1);
            
            final TextView textLabel 		= (TextView) layoutInflater.inflate(R.layout.sector_list_item, null);
            textLabel.setText(sector.getSectorName());
			
            tableRow.addView(textLabel);
            
            tableRow.setOnClickListener(new OnClickListener() {
	           	 public void onClick(View view) {
	           		final TableRow previousRow	= (TableRow) tableLayout.getChildAt(selectedSectorRow);
	           		TextView tvPreviousLabel	= (TextView) previousRow.getChildAt(0);
	           		
	           		previousRow.setBackgroundResource(R.color.PopWindowBackColor);
	           		tvPreviousLabel.setBackgroundResource(R.color.PopWindowBackColor);
	           		tvPreviousLabel.setTextColor(Color.BLACK);
	           		
	           		
	           		selectedSector		 		= sectorList.get( tableRow.getId() );
	           		selectedSectorRow 			= tableRow.getId();

	           		textLabel.setBackgroundColor(Color.DKGRAY);
	           		tableRow.setBackgroundColor(Color.DKGRAY);	           		 
	           		textLabel.setTextColor(Color.WHITE);
	    			listener.exchangeListSelectedEvent(selectedExchangeMap, selectedSector);
					if(hideOnSelect)
						ExchangeListView.this.hide();
	           	 }
            });          
            
            tableLayout.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );  
            id++;
       }
        
       positionSector();
	}
	
	
	public synchronized void hide() {
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
		try{
			isShowing = false;
			if( popupWindow != null && popupWindow.isShowing() ) {
				popupWindow.dismiss();
				popupWindow = null;
			}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
		return;
	}
}
