package com.n2nconnect.android.stock.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.StockInfo;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import java.util.List;

public class IBillionaireActivity extends CustomWindow implements DoubleLoginThread.OnDoubleLoginListener {

    private AlertDialog alertException;

    private String iBMainUrl;
    private WebView wvIBillionaire;
    private ProgressDialog dialog;

    private String stockCodeFromUrl;
    private String exchangeCodeFromUrl;
    private String qcExchangeCode;
    private String symbolCode;
    private String trdExchangeCode;
    private String strSymbolCode;

    private int lotSize;
    private String instrument;
    private boolean isPayableWithCPF;

    private String IB_PAGE_TYPE;
    private int intRetryCtr;

    private DoubleLoginThread loginThread;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.title.setText(getResources().getString(R.string.ib_module_title));
        this.icon.setImageResource(R.drawable.menuicon_ibillionaire);

        setContentView(R.layout.activity_ibillionaire);

        wvIBillionaire  = (WebView)findViewById(R.id.wv_ibillionaire);
        dialog          = new ProgressDialog(this);

        Bundle extras	= this.getIntent().getExtras();
        if(extras != null){
            IB_PAGE_TYPE		= extras.getString(ParamConstants.INFO_TYPE_TAG);

            if(IB_PAGE_TYPE.equals(ParamConstants.SCREENER_TYPE_INDIVIDUAL)){
                trdExchangeCode		= extras.getString(ParamConstants.EXCHANGECODE_TAG);
                symbolCode			= extras.getString(ParamConstants.SYMBOLCODE_TAG);
                lotSize				= extras.getInt(ParamConstants.LOT_SIZE_TAG);
                instrument			= extras.getString(ParamConstants.INSTRUMENT_TAG);
                isPayableWithCPF	= extras.getBoolean(ParamConstants.IS_PAYABLE_WITH_CPF);

                strSymbolCode		= symbolCode.substring(0, symbolCode.lastIndexOf(".") );
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if( idleThread != null && idleThread.isPaused() )
            idleThread.resumeRequest(false);

        // Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
        loginThread = ((StockApplication) IBillionaireActivity.this.getApplication()).getDoubleLoginThread();
        if( loginThread != null && loginThread.isPaused() ){
            loginThread.setRegisteredListener(IBillionaireActivity.this);
            loginThread.resumeRequest();
        }
        // Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END

        iBMainUrl = generateIBUrl();
        System.out.println("iBMainUrl:" + iBMainUrl);

        wvIBillionaire.getSettings().setJavaScriptEnabled(true);
        wvIBillionaire.setWebViewClient(new WebViewController());

        //to enhance the performance with hardware acceleration, android 19 has Chromium engine for WebView
        if (Build.VERSION.SDK_INT >= 19) {
            wvIBillionaire.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            wvIBillionaire.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        wvIBillionaire.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if( ! IBillionaireActivity.this.isFinishing() ){
                    dialog.setMessage("Loading");
                    dialog.show();
                }

                if(newProgress == 100 && !IBillionaireActivity.this.isFinishing() &&
                        dialog.isShowing())
                    dialog.dismiss();
            }
        });

        wvIBillionaire.loadUrl(iBMainUrl);
    }

    @Override
    public void onPause(){
        try{
            super.onPause();

            if(loginThread != null)
                loginThread.pauseRequest();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void DoubleLoginEvent(List<String> response) {
        Message message	= handler3.obtainMessage();
        message.obj 	= response;
        handler3.sendMessage(message);
    }

    final Handler handler3 = new Handler(){
        public void handleMessage(Message message){
            List<String> response = (List<String>) message.obj;

            if(response != null && response.size() > 2 && !response.get(2).equals("0") ){

                if(loginThread != null)
                    loginThread.stopRequest();

                if(LoginActivity.QCAlive != null)
                    LoginActivity.QCAlive.stopRequest();

                if( IBillionaireActivity.this.mainMenu.isShowing() )
                    IBillionaireActivity.this.mainMenu.hide();

                if( !isFinishing() )
                    IBillionaireActivity.this.doubleLoginMessage((String)response.get(1));
            }
        }
    };

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (wvIBillionaire.canGoBack() && (keyCode == KeyEvent.KEYCODE_BACK)) {
            // Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
            if(idleThread != null && ! isShowingIdleAlert)
                idleThread.resetExpiredTime();
            // Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

            if(this.mainMenu.isShowing())
                this.mainMenu.hide();

            wvIBillionaire.goBack();
            finish();

            return true;
        }else if(keyCode==KeyEvent.KEYCODE_BACK){
            // Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
            if(idleThread != null && ! isShowingIdleAlert)
                idleThread.resetExpiredTime();
            // Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public class WebViewController extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            DefinitionConstants.Debug("iBillionaire url:" + url);

            if(idleThread != null && ! isShowingIdleAlert)
                idleThread.resetExpiredTime();

            if(url.startsWith("itradecimbtradebuttonclicked:")){
                String symbolCodeFromUrl	= getSymbolCodeFromUrl(url);

                if(AppConstants.noTradeClient){
                    showAlertDialog("You do not have a trading account", false);
                } else if(qcExchangeCode == null || qcExchangeCode.isEmpty()){
                    showAlertDialog(getResources().getString(R.string.stock_not_found_screener_trade), false);

                }else{

                    if(symbolCode != null && symbolCode.equalsIgnoreCase(symbolCodeFromUrl)){
                        startTradeActivity(symbolCodeFromUrl, Integer.toString(lotSize), instrument, isPayableWithCPF);
                    } else{
                        //request coming from global Indices, fetch Stock Info
                        new FetchStockInfo().execute(symbolCodeFromUrl);
                    }
                }

                return true;

            } else if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
                view.reload();

                return true;
            }
            else {
                view.loadUrl(url);

                return false;
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            showAlertDialog(getResources().getString(R.string.ib_not_available), true);
        }
    }

    private void showAlertDialog(String alertMsg, final boolean isExit){
        alertException 	= new AlertDialog.Builder(IBillionaireActivity.this)
                .setMessage(alertMsg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if(isExit){
                            IBillionaireActivity.this.finish();
                        }
                        return;
                    }
                }).create();

        if(!IBillionaireActivity.this.isFinishing()) //Fixes for Crashlytics
            alertException.show();
    }

    private void startTradeActivity(String mSymbolCode, String mLotSize, String mInstrument, boolean mIsPayableWithCPF) {

        Intent intent = new Intent();
        intent.putExtra(ParamConstants.SYMBOLCODE_TAG, mSymbolCode);
        intent.putExtra(ParamConstants.LOT_SIZE_TAG, mLotSize);
        intent.putExtra(ParamConstants.INSTRUMENT_TAG, mInstrument);
        intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, mIsPayableWithCPF);
        intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
        intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
        intent.putExtra(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG, ParamConstants.PARAM_ORDER_SOURCE_IB);

        intent.setClass(IBillionaireActivity.this, TradeActivity.class);

        startActivity(intent);

    }

    private String getSymbolCodeFromUrl(String url){
        //e.g., itradecimbtradebuttonclicked://stockcode:AAPL/exchange:O
        String params = url.substring(url.indexOf("://")+3, url.length());
        String[] temp	= params.split("/");

        String[] symbolCodeChunk	= temp[0].split(":");
        stockCodeFromUrl			= symbolCodeChunk[1];

        String[] exchangeChunk		= temp[1].split(":");
        exchangeCodeFromUrl			= exchangeChunk[1];

        qcExchangeCode		= SystemUtil.getQcExchangeCode(exchangeCodeFromUrl);	//to support delay feed

        if(qcExchangeCode != null && !qcExchangeCode.isEmpty()){
            return stockCodeFromUrl + "." + qcExchangeCode;
        } else
            return stockCodeFromUrl + "." + exchangeCodeFromUrl;
    }

    private String generateIBUrl(){
        if(IB_PAGE_TYPE.equals(ParamConstants.IB_TYPE_INDIVIDUAL))
            return AppConstants.brokerConfigBean.getiBMainUrl() + "stock/" + strSymbolCode + ":" + trdExchangeCode;
        else
            return AppConstants.brokerConfigBean.getiBMainUrl();

    }

    private StockInfo stockInfo;

    private class FetchStockInfo extends AsyncTask<String, Void, String[]> {
        private Exception exception 						= null;
        private int nextTaskId;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!IBillionaireActivity.this.isFinishing()){
                dialog.setMessage("fetching StockInfo");
                dialog.show();
            }
        }

        @Override
        protected String[] doInBackground(String... params) {

            String[] symbolCodes 	= { params[0] };

            String response = null;
            try {
                intRetryCtr = 0;

                response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
                while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
                    intRetryCtr++;
                    response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
                }
                intRetryCtr = 0;

                if (response == null)
                    throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);

            } catch (Exception e) {
                exception = e;
            }

            if(response != null)
                stockInfo = QcMessageParser.parseStockDetail(response);

            return symbolCodes;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);

            if (dialog != null && dialog.isShowing())
                dialog.dismiss();

            if (exception != null) {

                if (!IBillionaireActivity.this.isFinishing())
                    showAlertDialog(exception.getMessage(), false);
                return;
            } else{
                String strSymbolCode	= stockInfo.getSymbolCode();

                if(strSymbolCode == null || strSymbolCode.isEmpty())
                    showAlertDialog(getResources().getString(R.string.stock_not_found_screener_trade), false);
                else
                    startTradeActivity(stockInfo.getSymbolCode(), Integer.toString(stockInfo.getSharePerLot()), stockInfo.getInstrument(), stockInfo.isPayableWithCPF());
            }
        }
    }
}