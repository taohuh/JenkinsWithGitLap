package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.ScreenReceiver;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.StockNewsAdapter;
import com.n2nconnect.android.stock.custom.TimeSalesPullToRefresh;
import com.n2nconnect.android.stock.custom.TimeSalesPullToRefresh.RefreshListener;
import com.n2nconnect.android.stock.custom.WatchListView;
import com.n2nconnect.android.stock.custom.WatchListView.OnWatchlistEventListener;
import com.n2nconnect.android.stock.model.BusinessDone;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.LotMarket;
import com.n2nconnect.android.stock.model.MarketDepth;
import com.n2nconnect.android.stock.model.StockInfo;
import com.n2nconnect.android.stock.model.StockNews;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TimeSales;
import com.n2nconnect.android.stock.model.Watchlist;
import com.n2nconnect.android.stock.util.ArchiveNewsUtil;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class StockActivity extends CustomWindow 
implements OnTouchListener, OnRefreshStockListener, OnDoubleLoginListener
// Added by Mary@20130828 - Fixes_Request-20130711, ReqNo.17
, OnWatchlistEventListener
{

	private String symbolCode;
	// Added by Kw@20130222 - Change_Request20130104, ReqNo.6
	private String splitTrdExchgCode;
	public String splitQCExchgCode;
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private String userParam;
	*/
	
	private StockInfo stockInfo;
	private StockInfo stockInfoExtra;		//Added by Thinzar, Change_Request-20141101, ReqNo. 
	/* Mary@20130604 - comment unused variable
	private View stockDetailView;
	*/
	private TextView highLabel;
	private TextView lowLabel;
	private TextView volumeLabel;
	private TextView valueLabel;
	private TextView lastDoneLabel;
	private TextView changeLabel;
	private TextView changePercentLabel;
	private TextView bidSizeLabel;
	private TextView bidPriceLabel;
	private TextView askSizeLabel;
	private TextView askPriceLabel;
	private TextView totalBidLabel;
	private TextView totalAskLabel;
	private TextView openLabel;
	private TextView lacpLabel;			//Added by Thinzar, Change_Request-20141101, ReqNo. 5
	
	//Change_Request-20170119, ReqNo.12
	private LinearLayout lacpLayout;	
	private LinearLayout topPriceLayout;
	private TextView topLabel;
	
	private TextView previousLabel;		//note: changed the label text from PREV fto CLOSE, Change_Request-20141101, ReqNo. 5
	private TextView nameLabel;
	private TextView companyLabel;
	private TextView noResultFound;
	private TextView noChartFound;
	private ImageView arrowImage;
	private ImageView chartView;
	//private ImageView imgPage;		//replaced by programmatically generated dots
	private ScrollView newsScrollView;
	private View layout0 = null;
	private View layout1 = null;
	private View layout2 = null;
	private View layout3 = null;
	private View layout4 = null;
	private View layout5 = null;
	// Mary@20120723 - ChangeRequest-20120719, ReqNo.1
	private View layout6 = null;
	private View layout7 = null;
	// Sonia@20131001 - Change_Request-20130225, ReqNo.5
	private View layout8 = null;
	private LinearLayout linearProgress;
	private ProgressDialog dialog;

	private Button Trade;
	private MyPagerAdapter adapter;
	private ViewPager myPager;
	private LayoutInflater inflater;
	//private AnimationDrawable loadAnimation;
	private ProgressBar chartProgressBar;
	
	private OrientationEventListener orientationListener;
	private RefreshStockThread refreshThread;
	private DoubleLoginThread loginThread;
	private Watchlist watchlist;
	private FCMBtnOnClickAsyncTask fcmBtnOnClickAsyncTask;
	
	public static int chartDuration;
	private int ChartType;
	private int width;
	private int height;
	private int currentPage;
	private boolean isChartActivityStarted;
	
	private List<View> mView;
	private List<Watchlist> watchlists;
	private List<TextView> bidSizeLabels;
	private List<TextView> bidPriceLabels;
	private List<TextView> askPriceLabels;
	private List<TextView> askSizeLabels;
	// Added by Mary@20130613 - Fixes_Request-20130523, ReqNo.9
	private List<TextView> counterLabels;
	/* Mary@20130604 - comment unused variable
	private List marketDepth;
	*/
	private List<StockNews> newsUpdates;
	private List<MarketDepth> lstNewMarketDepth;
	
	private boolean isMarketDepthShow 	= false;
	/* Mary@20130603 - comment unused variable
	private boolean FirstTime 			= true;
	*/
	private boolean istradeValid 		= false;
	private boolean isExpired	 		= false;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr 			= 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	//Added by Thinzar@20140811 -  Change_Request-20140801, ReqNo.1
	private boolean hasCorporateActionAlertShown = false;	//to control the alert to display only once

	// Added by Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - START
	private OnClickListener FCMBtnOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
			
			fcmBtnOnClickAsyncTask = new FCMBtnOnClickAsyncTask(StockActivity.this, v);
			fcmBtnOnClickAsyncTask.execute();
		}
	};
	// Mary@20120723 - ChangeRequest-20120719-RequestNo-1 - END

	private OnClickListener NewFCMBtnOnClickListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			int intID = v.getId();
			int packageId	= 0;
			String brokerCode	= sharedPreferences.getString(PreferenceConstants.BROKER_CODE, "");
			String userName		= sharedPreferences.getString(PreferenceConstants.USER_NAME, "");
			
			switch (intID) {
			case R.id.btnCompanyInfo:
				packageId	= 0;
				break;	
			case R.id.btnSynopsis:
				packageId	= 0;		//TODO:combined together with company info, remove this in the future
				break;
			case R.id.btnKeyPerson:
				packageId	= 1;
				break;
			case R.id.btnShareholdingSummary:
				packageId	= 2;
				break;
			case R.id.btnAnnouncement:
				packageId	= 3;
				break;

			case R.id.btnFinancialReport:
				packageId	= 4;
				break;
			}
			
			Intent newFundamentalIntent	= new Intent();
			newFundamentalIntent.setClass(StockActivity.this, FundamentalCapitalIQActivity.class);
			
			newFundamentalIntent.putExtra(ParamConstants.STOCK_CODE_TAG, symbolCode);
			newFundamentalIntent.putExtra(ParamConstants.BROKERCODE_TAG, brokerCode);
			newFundamentalIntent.putExtra(ParamConstants.USERNAME_TAG, userName);
			newFundamentalIntent.putExtra(ParamConstants.PACKAGE_ID_TAG, Integer.toString(packageId));
			
			StockActivity.this.startActivity(newFundamentalIntent);
		}
	};
	
	
	// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12 - START
	private final int INT_BID_RELATED 	= 0;
	private final int INT_ASK_RELATED 	= 1;
	// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12 - END
	
	// Added by Mary@20130612 - Fixes_Request-20130523, ReqNo.9
	private final int INT_STOCK_DETAILS_PAGE_POS	= 1;
	private final int INT_MARKET_DEPTH_PAGE_POS 	= 2;
	private final int INT_STOCK_CHART_PAGE_POS 		= 3;
	private final int INT_TIME_SALES_PAGE_POS 		= 4;
	private final int INT_BUSINESS_DONE_PAGE_POS 	= 5;
	private final int INT_STOCK_NEWS_PAGE_POS 		= 6;
	private final int INT_FUNDAMENTAL_PAGE_POS 		= 7;
	
	// Added by Mary@20130620 - Change_Request-20130424, ReqNo.9
	public static boolean isShowFundamentalCapitalIQ	= false;
	
	// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - START
	/*
	private List<TextView> timeLabels;
	private List<TextView> tradedatLabels;
	private List<TextView> priceLabels;
	private List<TextView> volumeLabels;
	*/
	// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - END
	
	// Added by Sonia@20130717 - ChangeRequest-20130225, ReqNo.5 - START
	private final int INT_TIME_SALES_PAGE = 5;
	private ArrayList<TimeSales> newTransList = new ArrayList<TimeSales>();
	private TimeSalesPullToRefresh refreshV;
	private ScrollView sv;
	private LayoutInflater layoutInf;
	private long totalTrades;
	private boolean releasePullToRefresh = false;
	private boolean hasLatestRecord = false;
	public enum RecordAge { NEW, OLD; }
	private ProgressDialog progressDialog;
	// Added by Sonia@20130717 - ChangeRequest-20130225, ReqNo.5 - END
	
	// Added by Sonia@20130918 - ChangeRequest-20130225, ReqNo.5 - START
	private boolean isExchangePH;
	/*
	private List<TextView> BBHLabels;
	private List<TextView> SBHLabels;
	*/
	// Added by Sonia@20130918 - ChangeRequest-20130225, ReqNo.5 - END
	
	// Added by Sonia@20131002 - ChangeRequest-20130225, ReqNo.5 - START
	private List<TextView> bd_priceLabels;
	private List<TextView> bVolumeLabels;
	private List<TextView> bVolume_PercentLabels;
	private List<TextView> sVolumeLabels;
	private List<TextView> sVolume_PercentLabels;
	private List<TextView> totalVolumeLabels;
	private List<TextView> totalValueLabels;
	// Added by Sonia@20131002 - ChangeRequest-20130225, ReqNo.5 - END
	
	//Added by Thinzar, Fixes_Request-20150401, ReqNo. 6
	private int timeSalesViewPref;
	private int businessDoneViewPref;
	
	String strSymbolExchg;
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.3, to generate pager dots programmatically
	private ImageView pagerDot1;
	private ImageView pagerDot2;
	private ImageView pagerDot3;
	private ImageView pagerDot4;
	private ImageView pagerDot5;
	private ImageView pagerDot6;
	private ImageView pagerDot7;
	private ImageView imgStkRefresh;
	private ImageView imgOrientation;
	
	private WebView stockChartWebView;
	private String code;
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.8
	private int requestedPagerId;
	private boolean isRequestPagerRedirect = false;
	
	//Added by Diyana@20160614 - ReqNo.1
	private String  encryptedTime;
	//private String stockCode;
	//private String stockName;
	private StockApplication application;
	private ExchangeInfo selectedExchange;
	private String selectedExchangeCode;
	private LinearLayout linearLayoutLabel;
	private int s;
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.2
	private ImageView imgScreenerStarRating;
	private ImageView imgScreenerRiskRating;
	private LinearLayout screenerLayout;
	private LinearLayout topRightBlankLayout;
	private LinearLayout iBillionaireLayout;
	private ImageView imgIBillionaire;
	//private final String iBillionairePkgName	= "com.ibillionaire.sg";
	private String symbolForIBillionaire;
	
	//Change_request-20170119
	private final int PKG_COMP_INFO			= 0;
	private final int PKG_COMP_KEY_PERSON	= 1;
	private final int PKG_SHAREHOLDING_SUMM	= 2;
	private final int PKG_ANNOUNCEMENT		= 3;
	private final int PKG_FINANCIAL_REPORT	= 4;
	private final int PKG_ESTIMATES			= 5;
	private final int PKG_SUPPLY_CHAIN		= 6;
	private final int PKG_MKT_ACTIVITY		= 7;
	
	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.stock_info);

			this.title.setText(getResources().getString(R.string.stock_info_module_title));
			/*
			 * Mary@20120814 - Change_Request-20120719, ReqNo.4
			 * this.icon.setImageResource(LayoutSettings.menu_stock_icn);
			 */
			this.icon.setImageResource(R.drawable.menuicon_stockinfo);
	
			// Added by Mary@20121112 - Fixes_Request-20121023, ReqNo.4
			init( this.getIntent() );
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
			finish();
		}
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - END
	}
	
	public void onResume() {
		System.out.println("onResume()");
		try {
			super.onResume();
			
			application		= (StockApplication) this.getApplication();
			
			selectedExchange 					= application.getSelectedExchange();
			selectedExchangeCode				= selectedExchange.getQCExchangeCode();
			
			// Added by Mary@20130718 - Fixes_Request-20130711, ReqNo.4 - START
			loginThread = ( (StockApplication) StockActivity.this.getApplication() ).getDoubleLoginThread();
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					
					loginThread.start();
					( (StockApplication) StockActivity.this.getApplication() ).setDoubleLoginThread(loginThread);
				}
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(StockActivity.this);
				loginThread.resumeRequest();
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
			}
			loginThread.setRegisteredListener(StockActivity.this);
			// Added by Mary@20130718 - Fixes_Request-20130711, ReqNo.4 - END
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= StockActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
			AppConstants.showMenu	= true;

			openLabel 				= (TextView) this.findViewById(R.id.openInfoLabel);
			lacpLabel				= (TextView) this.findViewById(R.id.lacpInfoLabel);		//Added by Thinzar, Change_Request-20141101, ReqNo. 5
			highLabel 				= (TextView) this.findViewById(R.id.highInfoLabel);
			lowLabel 				= (TextView) this.findViewById(R.id.lowInfoLabel);
			previousLabel 			= (TextView) this.findViewById(R.id.previousInfoLabel);
			volumeLabel 			= (TextView) this.findViewById(R.id.volumeInfoLabel);
			valueLabel 				= (TextView) this.findViewById(R.id.valueInfoLabel);
			nameLabel 				= (TextView) this.findViewById(R.id.stockNameLabel);
			companyLabel 			= (TextView) this.findViewById(R.id.companyNameLabel);
			arrowImage 				= (ImageView) this.findViewById(R.id.arrowImage);
			lastDoneLabel			= (TextView) this.findViewById(R.id.lastDoneLabel);
			changeLabel 			= (TextView) this.findViewById(R.id.priceChangeLabel);
			changePercentLabel 		= (TextView) this.findViewById(R.id.pricePercentLabel);
			
			//Change_Request-20170119, ReqNo.12
			lacpLayout				= (LinearLayout) this.findViewById(R.id.lacpInfoLayout);
			topPriceLayout			= (LinearLayout) this.findViewById(R.id.topPriceLayout);
			topLabel				= (TextView) this.findViewById(R.id.topPriceLabel);
			
			//Added by Thinzar, Change_Request-20160722, ReqNo.2
			topRightBlankLayout		= (LinearLayout) this.findViewById(R.id.top_right_blank_layout);
			imgScreenerStarRating	= (ImageView) this.findViewById(R.id.imgScreenerStarRating);
			imgScreenerRiskRating	= (ImageView) this.findViewById(R.id.imgScreenerRiskRating);
			screenerLayout			= (LinearLayout) this.findViewById(R.id.screenerLayout);
			
			//Added by Thinzar, Change_Request-20160722, ReqNo.8
			iBillionaireLayout		= (LinearLayout) this.findViewById(R.id.iBillionaireLayout);
			imgIBillionaire			= (ImageView) this.findViewById(R.id.imgiBillionaire);
			
			openLabel			.setText("0.000");
			lacpLabel			.setText("0.000"); 		//Added by Thinzar, Change_Request-20141101, ReqNo. 5		
			highLabel			.setText("0.000");
			lowLabel			.setText("0.000");
			previousLabel		.setText("0.000");
			volumeLabel			.setText("0.000");
			valueLabel			.setText("0.000");
			nameLabel			.setText("-");
			companyLabel		.setText("-");
			arrowImage			.setImageDrawable(null);
			lastDoneLabel		.setText("0.000");
			changeLabel			.setText("0.00");
			changePercentLabel	.setText("0.00%");
			
			dialog 					= new ProgressDialog(StockActivity.this);

			new stockRefresh().execute();
		} catch (Exception ex) {
			ex.printStackTrace();
			finish();
		}
	}

	@Override
	protected void onPause() {
		 // Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6
        try{
        	super.onPause();
        	
			if(refreshThread != null)
				refreshThread.pauseRequest();
	
			if(myPager != null)
				currentPage = myPager.getCurrentItem();
	
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
			
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
			// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
			
			if(dialog != null && dialog.isShowing())
				dialog.dismiss();
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - START
        }catch(Exception e){
        	e.printStackTrace();
        }
        // Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - END
	}
	
	
	
	 @Override
	protected void onDestroy() {
		super.onDestroy();
		
		System.out.println("StockActivity onDestroy");
		//stockNewsArrayList = null;
	}

	@Override
    protected void onNewIntent(Intent intent){        
        // Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6
        try{
        	super.onNewIntent(intent);
        	
	        if( intent.getBooleanExtra(ParamConstants.PARAM_STR_RESTART_ACTIVITY, false) )
	        	init(intent);
	    // Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - START
        }catch(Exception e){
        	e.printStackTrace();
        }
        // Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - END
    }

	public void onStop() {
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6
		try{
			super.onStop();
			
			orientationListener.disable();
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - START
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	    // Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - END
	}
	
	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		Message message = handler.obtainMessage();
		message.obj 	= changedSymbols;
		handler.sendMessage(message);
	}

	final Handler handler = new Handler() {
		public void handleMessage(Message message) {
			List<StockSymbol> changedSymbols = (List<StockSymbol>)message.obj;
			StockActivity.this.refreshStockData(changedSymbols);
		}
	};
	private TextView txtFundamentalNewsLabel;

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
			else
				finish();
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	/* Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - START
	public void init(Intent intent){
	*/
	public void init(Intent intent) throws Exception{
		try{
	// Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - END
			List<String> ValidTrade 		= DefinitionConstants.getTrdExchangeCode();
			
			Bundle extras	= intent.getExtras();
			/*
			if(intent != null)
				symbolCode = intent.getStringExtra(ParamConstants.SYMBOLCODE_TAG);
			*/
			
			if(extras != null){
				symbolCode	= extras.getString(ParamConstants.SYMBOLCODE_TAG);
				//Added by Diyana@20160614 -ReqNo.1  -START	//Commented by Thinzar, extra params, no need
				//stockCode = extras.getString(ParamConstants.STOCK_CODE_TAG);
				//stockName = extras.getString(ParamConstants.STOCK_NAME_TAG);
				//Added by Diyana@20160614 -ReqNo.1  -END
				
				if(extras.containsKey(ParamConstants.STOCK_PAGER_ID_TAG)){
					isRequestPagerRedirect	= true;
					requestedPagerId	= extras.getInt(ParamConstants.STOCK_PAGER_ID_TAG);
				}
			}
			
			//Added by Thinzar@20140723 - Change_Request-20140701, ReqNo. 8
			lstNewMarketDepth 	= null;	
			
			//imgPage 					= (ImageView) findViewById(R.id.imgPage);		//commented, Change_Request-20160101, ReqNo.3
			//Added by Thinzar, Change_Request-20160101,ReqNo.3
			pagerDot1				= (ImageView)this.findViewById(R.id.pagerDot1);
			pagerDot2				= (ImageView)this.findViewById(R.id.pagerDot2);
			pagerDot3				= (ImageView)this.findViewById(R.id.pagerDot3);
			pagerDot4				= (ImageView)this.findViewById(R.id.pagerDot4);
			pagerDot5				= (ImageView)this.findViewById(R.id.pagerDot5);
			pagerDot6				= (ImageView)this.findViewById(R.id.pagerDot6);
			pagerDot7				= (ImageView)this.findViewById(R.id.pagerDot7);
			imgStkRefresh			= (ImageView)this.findViewById(R.id.imgStkRefresh);
			imgOrientation			= (ImageView)this.findViewById(R.id.imgOrientation);
			
			//Added by Thinzar, Fixes_Request-20150401, ReqNo. 9 
			newTransList.clear();
			
			/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20 - START
			String[] splitExchgCode 		= symbolCode.split("\\.");
			splitQCExchgCode 				= splitExchgCode[1];
			splitTrdExchgCode				= splitExchgCode[1];
			*/
			splitQCExchgCode				= symbolCode.substring( (symbolCode.lastIndexOf(".") + 1), symbolCode.length() );
			splitTrdExchgCode				= splitQCExchgCode;
			// Mary@20130626 - Fixes_Request-20130523, ReqNo.20 - END
			StockApplication application	= (StockApplication) StockActivity.this.getApplication();
			// Added by Mary@20130220 - Fixes_Request-20130108, ReqNo.14
			ExchangeInfo exchgInfoMatch		= null;
			for( Iterator<ExchangeInfo> itr2=application.getExchangeInfos().iterator(); itr2.hasNext(); ){
				ExchangeInfo exchgInfo 	= itr2.next();
				/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
				if( exchgInfo.getTrdExchgCode() != null && splitExchgCode[1].equals( exchgInfo.getQCExchangeCode() ) ){
				*/
				if( exchgInfo.getTrdExchgCode() != null &&  ( splitTrdExchgCode.equals( exchgInfo.getQCExchangeCode() ) || splitTrdExchgCode.equals( exchgInfo.getTrdExchgCode() ) ) ){
					splitTrdExchgCode	= exchgInfo.getTrdExchgCode();
					// Added by Mary@20130220 - Fixes_Request-20130108, ReqNo.14
					exchgInfoMatch		= exchgInfo;
					break;
				}
			}
			
			// Added by Mary@20130620 - Change_Request-20130424, ReqNo.9
			isShowFundamentalCapitalIQ	= isShowFundamentalCapitalIQPage(AppConstants.mapFCBitMode, splitQCExchgCode);
			
			DisplayMetrics metrics		= new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
			height 						= metrics.heightPixels;
			width 						= metrics.widthPixels;
	
			inflater 					= getLayoutInflater();
			
			//Edited by Thinzar, Change_Request-20160101, reqNo.8
			//currentPage 				= 1;
			if(isRequestPagerRedirect){
				currentPage	= requestedPagerId;
				
				switch(requestedPagerId){
				case INT_STOCK_NEWS_PAGE_POS:
					setPagerDot(pagerDot6, INT_STOCK_NEWS_PAGE_POS);
					break;
				case INT_STOCK_CHART_PAGE_POS:
					setPagerDot(pagerDot3, INT_STOCK_CHART_PAGE_POS);
					break;
				}
			} else{
				currentPage	= 1;
			}
			
			Trade 						= (Button) findViewById(R.id.tradeButton);
	
			mView 						= new ArrayList<View>();
					
			layout0 					= inflater.inflate(R.layout.empty_view, null);
			layout1 					= inflater.inflate(R.layout.stock_detail, null);
			layout2 					= inflater.inflate(R.layout.market_depth, null);
			layout3 					= inflater.inflate(R.layout.stock_chart, null);
			
			/* Mary@20120723 - ChangeRequest-20120719, ReqNo.1 layout5 =
			 * inflater.inflate(R.layout.empty_view, null);
			 */
			/* Mary@20130620 - Change_Request-20130424, ReqNo.9
			layout5 					= AppConstants.isShowFundamentalCapitalICQ ? inflater.inflate(R.layout.fundamental_capital_icq_menu, null) : inflater.inflate(R.layout.empty_view, null);
			*/
			// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - START
			// Added by Sonia@20130923 - Change_Request-20130225, ReqNo.5
			//layout4						= splitQCExchgCode.equals(AppConstants.EXCHANGECODE_PH) ? inflater.inflate(R.layout.time_salesph, null) : inflater.inflate(R.layout.time_sales, null);
			layout4						=  inflater.inflate(R.layout.time_sales, null);
			// Sonia@20131001 - Change_Request-20130225, ReqNo.5
			layout5						= inflater.inflate(R.layout.business_done, null);
			layout6 					= inflater.inflate(R.layout.news_update, null);
			if (isShowFundamentalCapitalIQ){
				layout7						= inflater.inflate(R.layout.fundamental_capital_icq_menu, null);
			}
			// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - END
			layout8						= inflater.inflate(R.layout.empty_view, null);
			
			mView.add(layout0);
			mView.add(layout1);
			mView.add(layout2);
			mView.add(layout3);
			mView.add(layout4);
			// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - START
			// Added by Mary@20120723 - ChangeRequest-20120719, ReqNo.1
			mView.add(layout5);
			// Sonia@20131001 - Change_Request-20130225, ReqNo.5 - START
			mView.add(layout6);
			// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - END
			if(isShowFundamentalCapitalIQ){
				mView.add(layout7);
			}
			// Sonia@20131001 - Change_Request-20130225, ReqNo.5 - END
			// Sonia@20131001 - Change_Request-20130225, ReqNo.5
			mView.add(layout8);
			
			
			
			/* Mary@20130620 - Change_Request-20130424, ReqNo.9
			// Added by Mary@20120723 - ChangeRequest-20120719, ReqNo.1
			imgPage.setImageResource(AppConstants.isShowFundamentalCapitalICQ ? LayoutSettings.paging1of5 : LayoutSettings.paging1of4);
			*/
			// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5

			//Edited by Thinzar, for TcMobile 2.0
			//imgPage.setImageResource(isShowFundamentalCapitalIQ ? LayoutSettings.paging1of7 : LayoutSettings.paging1of6);
			if(!isShowFundamentalCapitalIQ){
				pagerDot7.setVisibility(View.GONE);
			}
			
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException 				= new AlertDialog.Builder(StockActivity.this)
											//.setIcon(android.R.attr.alertDialogStyle)
											.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog, int id) {
													// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
													try{
														dialog.dismiss();
													// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
													}catch(Exception e){
														e.printStackTrace();
													}
													// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
													return;
												}
											}).create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END	
			
			/* Mary@20130225 - Change_Request-20130225, ReqNo.1
			if(!AppConstants.noTradeClient){
			*/
			if( !AppConstants.noTradeClient && AppConstants.brokerConfigBean.enableTrade() ){
				/* Mary@20130220 - Fixes_Request-20130108, ReqNo.14 - START
				if ( ! ValidTrade.contains(splitTrdExchgCode) ) {
				*/
				if( ! ValidTrade.contains(splitTrdExchgCode) || exchgInfoMatch == null ){
				// Mary@20130220 - Fixes_Request-20130108, ReqNo.14 - END
					Trade.setVisibility(View.INVISIBLE);
					istradeValid = false;
				} else {
					// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2
					Trade.setVisibility(View.VISIBLE);
					istradeValid = true;
				}
			}else{
				istradeValid = false;
				Trade.setVisibility(View.INVISIBLE);
			}
	
			Trade.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
						
					// Added by Thinzar, Change_Request-20160101, ReqNo.11
					if (AppConstants.brokerConfigBean.getLstSectorNoTrading().contains(AppConstants.SELECTED_FEED_MARKET_CODE)) {
						List<LotMarket> lotMarketList	= selectedExchange.processAndGetMarketInfo();
						String lotMarketName 			= "";
						
						for(LotMarket market:lotMarketList){
							if(market.getCode().equalsIgnoreCase(AppConstants.SELECTED_FEED_MARKET_CODE)){
								lotMarketName	= market.getName();
								break;
							}
						}
						alertException.setMessage(getResources().getString(R.string.sector_no_trading_msg) + " " + lotMarketName + ".");
						alertException.show();
						
					} else {
						// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
						try {
							// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
							if (idleThread != null && !isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END

							// Added by Mary@20130225 - Change_Request-20130104, ReqNo.3 - START
							if ((AppConstants.getRDSInfo().isBlockByRDS() && !AppConstants.getRDSInfo()
									.getStrLocalExchangeCode().contains(splitTrdExchgCode))) {
								
								Intent intent = new Intent();
								intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
								intent.putExtra(ParamConstants.RDS_URL_TAG, AppConstants.getRDSInfo().getStrRDSUrl());
								intent.putExtra(ParamConstants.IS_BLOCK_BY_RDS_TAG,
										AppConstants.getRDSInfo().isBlockByRDS());
								intent.setClass(StockActivity.this, RiskDisclosureStatementActivity.class);
								startActivityForResult(intent, AppConstants.RDS_REQUEST);
								// Mary@20130327 - Change_Request-20130104, ReqNo.3 - END

								return;
							} else {
								// Added by Mary@20130225 - Change_Request-20130104, ReqNo.3 - END
								ArrayList<StockInfo> Stock = new ArrayList<StockInfo>();
								Stock.add(stockInfo);

								Intent intent = new Intent();
								intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
								intent.putExtra(ParamConstants.LOT_SIZE_TAG,
										Integer.toString(stockInfo.getSharePerLot()));
								intent.putExtra(ParamConstants.INSTRUMENT_TAG, stockInfo.getInstrument());
								// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4
								intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stockInfo.isPayableWithCPF());
								intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
								// Added by Mary@20130925 - Change_Request-20130724, ReqNo.18
								intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
								intent.setClass(StockActivity.this, TradeActivity.class);
								startActivity(intent);
							}
							// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
						} catch (Exception e) {
							e.printStackTrace();
						}
						// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
					}
				}
			});
	
			this.findViewById(R.id.watchButton).setOnClickListener( new OnClickListener() {
				public void onClick(View view) {
					/* Mary@20130830 - Move to Asyn Task to avoid android.os.NetworkOnMainThreadException - START
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					try{
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START 
						 * StockActivity.this.populateWatchlists();
						/
						try {
							StockActivity.this.populateWatchlists();
						} catch (FailedAuthenicationException e) {
							if (refreshThread != null)
								refreshThread.stopRequest();
		
							if (loginThread != null)
								loginThread.stopRequest();
		
							if (LoginActivity.QCAlive != null)
								LoginActivity.QCAlive.stopRequest();
		
							StockActivity.this.sessionExpired();
							return;
						} catch (Exception e) {
							alertException.setMessage(e.getMessage());
							// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
							if( ! StockActivity.this.isFinishing() )
								alertException.show();
							return;
						}
						// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 -END
		
						if (watchlists.isEmpty()) {
							/* Mary@20130125 - Fixes_Request-20130110, ReqNo.13
							AlertDialog alert = new AlertDialog.Builder(StockActivity.this)
								.setTitle("No Available Watchlist")
								.setMessage("Please go to Watchlist screen to create at least 1 watchlist.")
								.setPositiveButton("OK", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,	int whichButton) {
										dialog.dismiss();
									}
							}).create();
							alert.show();
							*/
							/* Mary@20130828 - Fixes_Request-20130711, ReqNo.17 - START
							StockActivity.this.enterWatchlistName();
							/
							
							WatchListView watchListView = new WatchListView(StockActivity.this, StockActivity.this, getLayoutInflater());
							watchListView.setHideOnSelect(true);
							try {
								watchListView.setWatchlists(watchlists);
							} catch (Exception e) {			
								e.printStackTrace();
							}
							watchListView.enterWatchlistName("0", true);
							// Mary@20130828 - Fixes_Request-20130711, ReqNo.17 - END
							return;
						}
						
						final String[] items= new String[watchlists.size()];
						int index			= 0;
						for (Iterator<Watchlist> itr = watchlists.iterator(); itr.hasNext();) {
							Watchlist watchlist = itr.next();
							items[index] 		= watchlist.getName();
							index++;
						}
		
						StockActivity.this.showWatchlistDialog(items);
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
					}catch(Exception e){
						e.printStackTrace();
					}
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END 
					*/
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
					
					new populateWatchlists().execute();
					// Mary@20130830 - Move to Asyn Task to avoid android.os.NetworkOnMainThreadException - END
				}
			});
	
			LinearLayout containerLayout = (LinearLayout) findViewById(R.id.containerLayout);
			containerLayout.setOnTouchListener((OnTouchListener) this);
	
			/*
			 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START 
			 * userParam = sharedPreferences.getString(PreferenceConstants.QC_USER_PARAM, null);
			 * try { 
			 * 		if (userParam == null) { 
			 * 			List<String>data = QcHttpConnectUtil.relogin(); userParam = data.get(0);
			 * 			SharedPreferences.Editor editor = sharedPreferences.edit();
			 * 			editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
			 * 			editor.commit(); 
			 * 		} 
			 * } catch (FailedAuthenicationException e) {
			 *		e.printStackTrace(); 
			 * }
			 */
	
			orientationListener 		= new OrientationEventListener(this, SensorManager.SENSOR_DELAY_UI) {
				public void onOrientationChanged(int orientation) {
					if(orientation > 70 && orientation < 290){
						if(!isChartActivityStarted){
							isChartActivityStarted		= true;
							ScreenReceiver.wasScreenOn	= false;
							
							Intent intent 				= new Intent();
							intent.putExtra(ParamConstants.CHART_DURATION_TAG, chartDuration);
							intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
							intent.setClass(StockActivity.this, ChartActivity.class);
							startActivity(intent);
						}
					}else{
						isChartActivityStarted = false;
					}
				}
			};
	
			orientationListener.disable();
			
			watchlists 					= new ArrayList<Watchlist>();
			// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - START
			
			//Added by Thinzar, Fixes_Request-20150401, ReqNo. 6
			this.timeSalesViewPref		= AppConstants.brokerConfigBean.getIntViewQtyMeasurement();
			this.businessDoneViewPref	= AppConstants.brokerConfigBean.getIntViewQtyMeasurement();
			
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.6 - END
	}
	
	/* Mary@20130828 - Fixes_Request-20130711, ReqNo.17
	// Added by Mary@20130125 - Fixes_Request-20130110, ReqNo.13 - START
	private void enterWatchlistName(){
		try{
			final EditText input 		= new EditText(this);
	
			AlertDialog.Builder alert	= new AlertDialog.Builder(this)
				.setIcon(android.R.attr.alertDialogStyle)
				.setTitle("Watchlist Name")
				.setMessage("Please enter new name:")
				.setView(input)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						try{
							String name = input.getText().toString().trim();
							new executeWLCreate().execute(name);
							return;
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				})
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						dialog.dismiss();
					}
			});
	
			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! this.isFinishing() )
				alert.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	
	private class executeWLCreate extends AsyncTask<String, Void, String>{
		Exception exception		= null;
		String strWatchlistID 	= "1";
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( ! dialog.isShowing() ){
					dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() )
						dialog.show();
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(String... params) {
			String strWatchlistName 		= params[0];

			// TODO Auto-generated method stub
			String atpUserParam				= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			String senderCode 				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
			String paramString 				= sharedPreferences.getString(PreferenceConstants.ATP_PARAM_STRING, null);

			Map<String, Object> parameters 	= new HashMap<String, Object>();
			parameters.put(ParamConstants.FAVORATEID_TAG, strWatchlistID);


			/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
			if(AppConstants.EnableEncryption)
			*/
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			parameters.put(ParamConstants.FAVORATENAME_TAG, strWatchlistName);
			parameters.put(ParamConstants.PARAMSTRING_TAG, paramString);

			try {
				boolean isSuccess	= AtpConnectUtil.updateFavListName(parameters);
				while( intRetryCtr < AppConstants.intMaxRetry && ! isSuccess ){
					intRetryCtr++;
					isSuccess	= AtpConnectUtil.updateFavListName(parameters);
				}
				intRetryCtr			= 0;
				
				if( ! isSuccess )
					throw new Exception(ErrorCodeConstants.FAIL_UPDATE_WATCHLIST);
			}catch(Exception e){
				e.printStackTrace();
				exception = e;
				return null;
			}
			return strWatchlistName;
		}

		@Override
		protected void onPostExecute(String strWatchlistName){
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
			if( dialog != null && dialog.isShowing() )
				dialog.dismiss();
				
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() )
						alertException.show();
					return;
				}
	
				new addwatchlistStock().execute(strWatchlistID, strWatchlistName, symbolCode);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	// Added by Mary@20130125 - Fixes_Request-20130110, ReqNo.13 - END

	private void refreshStockData(List<StockSymbol> changedSymbols) {
		try{
			/* UPDATE STOCK INFO SECTION (UPPER PART) */
			final StockSymbol symbol = changedSymbols.get(0);
			
			if( symbol.getSymbolCode().equals( stockInfo.getSymbolCode() ) ){
				/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
				String[] symbolExchg	= symbol.getSymbolCode().split("\\.");
				*/
				
				if( symbol.checkDiscrepancy(stockInfo) ){
					
					if( symbol.getLastDone() != stockInfo.getLastDone()	|| symbol.getTotalTrade() != stockInfo.getTotalTrade() ){
						float gap	= symbol.getLastDone() - stockInfo.getLastDone();
						stockInfo.setLastDone(symbol.getLastDone());
						stockInfo.setTotalTrade(symbol.getTotalTrade());
						
						/* Commented out by Thinzar, replace by the following block of code, Change_Request-20141101, ReqNo. 8 
						if(strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
							lastDoneLabel.setText( FormatUtil.formatPrice2(stockInfo.getLastDone(), "-") );
						else
							lastDoneLabel.setText( FormatUtil.formatPrice(stockInfo.getLastDone(), "-") );
						
						changeLabel.setText( FormatUtil.formatPriceChange(stockInfo.getPriceChange()) );
						changePercentLabel.setText( FormatUtil.formatPricePercent(stockInfo.getChangePercent()) );
						*/
						lastDoneLabel.setText(FormatUtil.formatPrice(stockInfo.getLastDone(), null, strSymbolExchg));
						changeLabel.setText(FormatUtil.formatPriceChangeByExchange(stockInfo.getPriceChange(), strSymbolExchg));
						changePercentLabel.setText(FormatUtil.formatPricePercent( stockInfo.getChangePercent()));
						
						if (gap > 0) {
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							 * lastDoneLabel.setBackgroundColor(LayoutSettings.colorGreen);
							 * lastDoneLabel.setTextColor(Color.BLACK);
							 */
							lastDoneLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
							lastDoneLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
						} else if (gap < 0) {
							/*
							 * Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							 * lastDoneLabel.setBackgroundColor(LayoutSettings.colorRed);
							 * lastDoneLabel.setTextColor(Color.WHITE);
							 */
							lastDoneLabel.setBackgroundColor(getResources().getColor(R.color.DownStatus_BgColor));
							lastDoneLabel.setTextColor(getResources().getColor(R.color.DownStatus_FgColor));
						/* Mary@20120905 - Fixes_Request-20120815, ReqNo.8 
						 * } else if (gap == 0 && symbol.getTotalTrade()!=stockInfo.getTotalTrade()) {
						 */
						} else if (gap == 0	&& symbol.getVolume() != stockInfo.getVolume()) {
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							 * lastDoneLabel.setBackgroundColor(LayoutSettings.colorYellow);
							 * lastDoneLabel.setTextColor(Color.BLACK);
							 */
							lastDoneLabel.setBackgroundColor(getResources().getColor(R.color.DiffTtlStatus_BgColor));
							lastDoneLabel.setTextColor(getResources().getColor(R.color.DiffTtlStatus_FgColor));
						}

						lastDoneLabel.postDelayed(new Runnable() {
							@Override
							public void run() {
								lastDoneLabel.setBackgroundColor(Color.TRANSPARENT);
								/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - START 
								 * if (symbol.getPriceChange() > 0) {
								 *		lastDoneLabel.setTextColor(LayoutSettings.colorGreen);
								 * 		changeLabel.setTextColor(LayoutSettings.colorGreen);
								 * 		changePercentLabel.setTextColor(LayoutSettings.colorGreen); 
								 * }else if (symbol.getPriceChange() < 0) {
								 * 		lastDoneLabel.setTextColor(LayoutSettings.colorRed);
								 * 		changeLabel.setTextColor(LayoutSettings.colorRed);
								 * 		changePercentLabel.setTextColor(LayoutSettings.colorRed); 
								 * } else {
								 * 		lastDoneLabel.setTextColor(Color.BLACK);
								 * 		changeLabel.setTextColor(Color.BLACK);
								 * 		changePercentLabel.setTextColor(Color.BLACK);
								 * }
								 */
								int intStatusColor = getResources().getColor(R.color.NoStatus_FgColor);
								if (symbol.getPriceChange() > 0)
									intStatusColor = getResources().getColor(R.color.UpStatus_BgColor);
								else if (symbol.getPriceChange() < 0)
									intStatusColor = getResources().getColor(R.color.DownStatus_BgColor);

								lastDoneLabel.setTextColor(intStatusColor);
								changeLabel.setTextColor(intStatusColor);
								changePercentLabel.setTextColor(intStatusColor);
								// Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - END
							}
						}, 1500);

						if (stockInfo.getPriceChange() > 0)
							arrowImage.setImageResource(LayoutSettings.upArrow);
						else if (stockInfo.getPriceChange() < 0)
							arrowImage.setImageResource(LayoutSettings.downArrow);
						else
							arrowImage.setImageDrawable(null);
					}

					if (symbol.getVolume() != stockInfo.getVolume()) {
						stockInfo.setVolume( symbol.getVolume() );
						/* Mary@20120911 - Change_Request-20120719, ReqNo.12 - START
						volumeLabel.setText(FormatUtil.formatLong(stockInfo.getVolume()));
						*/
						int intLotSize = AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? stockInfo.getSharePerLot() : 1;
						//System.out.println("test :"+stockInfo.getSharePerLot());
						/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
						/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
						volumeLabel.setText( FormatUtil.formatLong(stockInfo.getVolume() / intLotSize) );
						/
						volumeLabel.setText( FormatUtil.formatLong(stockInfo.getVolume() / intLotSize, "-") );
						// Mary@20120911 - Change_Request-20120719, ReqNo.12 - END
						*/
						volumeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (stockInfo.getVolume() / intLotSize), "-") );
					}
					
					if (symbol.getValue() != stockInfo.getValue()) {
						stockInfo.setValue(symbol.getValue());
						/*
						if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
							valueLabel.setText( FormatUtil.formatDouble2(stockInfo.getValue(), "-") );
						else
							valueLabel.setText( FormatUtil.formatDouble( stockInfo.getValue(), "-" ) );
						*/
						//Commented out the above block and add the following by Thinzar, Change_Request-20141101, ReqNo 8
						valueLabel.setText(FormatUtil.formatDoubleByExchange(stockInfo.getValue(), null, strSymbolExchg));
					}
					
					if( symbol.getHigh() != stockInfo.getHigh() ){
						stockInfo.setHigh( symbol.getHigh() );
						/*
						if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
							highLabel.setText( FormatUtil.formatFloat2( stockInfo.getHigh(), "-" ) );
						else
							highLabel.setText( FormatUtil.formatFloat( stockInfo.getHigh(), "-" ) );
						*/
						//Commented out the above code block and edited by Thinzar, Change_Request-20141101, ReqNo 8
						highLabel.setText(FormatUtil.formatPrice(stockInfo.getHigh(), "-", strSymbolExchg));
					}
					
					if( symbol.getLow() != stockInfo.getLow() ){
						stockInfo.setLow(symbol.getLow());
						/*
						if( strSymbolExchg.equalsIgnoreCase("JK")	|| strSymbolExchg.equalsIgnoreCase("JKD") )
							lowLabel.setText( FormatUtil.formatFloat2( stockInfo.getLow(), "-" ) );
						else
							lowLabel.setText( FormatUtil.formatFloat( stockInfo.getLow(), "-" ) );
						*/
						//Commented out the above code block and edited by Thinzar, Change_Request-20141101, ReqNo 8
						lowLabel.setText(FormatUtil.formatPrice(stockInfo.getLow(), "-", strSymbolExchg));
					}
				}
				
				
				/* UPDATE MARKET DEPTH SECTION (BOTTOM PART) */
				/* Mary@20130612 - Fixes_Request-20130523, ReqNo.9 - START
				// Kw@20130510 - Fixes_Request-20130424, ReqNo.7 START
				List<MarketDepth> oldMarketDepth = marketDepths;
				//marketDepth 		= marketDepths;//stockInfo.getMarketDepths();//
				//System.out.println("MARKETDEPTH NUMBER OLD:"+oldMarketDepth.size());
				/* Mary@20130604 - comment unused variable
				Exception exception 					= null;
				/
				String[] symbolCodes 			= { symbolCode };
				String response 				= null;
				try {
					 if(AppConstants.brokerConfigBean.isMultilevelMarketDepth()){
						 response	= QcHttpConnectUtil.imageQuote2(symbolCode, AppConstants.getMarketDepthLvl(splitQCExchgCode));
						 while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
								intRetryCtr++;
								response	= QcHttpConnectUtil.imageQuote2(symbolCode, AppConstants.getMarketDepthLvl(splitQCExchgCode));
						}
						intRetryCtr	= 0;
						
						if (response == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
					
						marketDepths= QcMessageParser.parseMarketDepth4(response);
					 }else{
						response	= QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
						while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
								intRetryCtr++;	
								response	= QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
						}
						intRetryCtr	= 0;
						
						if (response == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
						
						marketDepths= QcMessageParser.parseMarketDepth3(response,AppConstants.getMarketDepthLvl(splitTrdExchgCode));
					 }
				} catch (Exception e) {
					/* Mary@20130604 - comment unused variable
					exception = e;
					/
					e.printStackTrace();
				}
				//new MarketDepthTask().execute();
				//update new one 
				
				final List<Float> marketDepthAskPrice 	= new ArrayList<Float>();	//= symbol.getMarketDepthAskPrice();
				final List<Long> marketDepthAskSize  	= new ArrayList<Long>();	//= symbol.getMarketDepthAskSize();
				final List<Float> marketDepthBidPrice 	= new ArrayList<Float>(); 	//= symbol.getMarketDepthBidPrice();
				final List<Long> marketDepthBidSize   	= new ArrayList<Long>();	//= symbol.getMarketDepthBidSize()
				/* Mary@20130603 - comment unused variable
				int i 									= 0;
				/
				
				for (Iterator<MarketDepth> itr = marketDepths.iterator(); itr.hasNext();) {
					MarketDepth marketU	= itr.next();
					//System.out.println("Marketdepthtest1 getaskprice"+marketU.getAskPrice());
					marketDepthAskPrice.add( marketU.getAskPrice() );;
					marketDepthAskSize.add( marketU.getAskSize() );
					marketDepthBidPrice.add( marketU.getBidPrice() );				
					marketDepthBidSize.add( marketU.getBidSize() );			
				
				}
				// Kw@20130510 - Fixes_Request-20130424, ReqNo.7 END

				// marketDepth = stockInfo.getMarketDepths();

				long totalBid 		= 0;
				long totalAsk 		= 0;
				double bidSum 		= 0;
				double askSum 		= 0;

				long gapBidSize 	= 0;
				float gapBidPrice 	= 0;
				long gapAskSize 	= 0;
				float gapAskPrice 	= 0;

				int id 				= 0;

				// Kw@20130510 - Fixes_Request-20130424, ReqNo.7 START
				if(isMarketDepthShow){
					// Added by Mary@20120911-Change_Request-20120719, ReqNo.12
					int intLotSize = AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? stockInfo.getSharePerLot() : 1;
					
					for (Iterator<MarketDepth> itr = oldMarketDepth.iterator(); itr.hasNext();) {
						final TextView bidsizeLabel	= bidSizeLabels.get(id);
						final TextView bidpriceLabel= bidPriceLabels.get(id);
						final TextView asksizeLabel = askSizeLabels.get(id);
						final TextView askpriceLabel= askPriceLabels.get(id);

						MarketDepth market 			= itr.next();
						gapBidSize 					= marketDepthBidSize.get(id) - market.getBidSize();
						/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - START 
						 * if(gapBidSize>0){
						 * 		bidsizeLabel.setBackgroundColor(LayoutSettings.colorGreen);
						 * 		bidsizeLabel.setTextColor(Color.BLACK); 
						 * }else if(gapBidSize<0){
						 * 		bidsizeLabel.setBackgroundColor(LayoutSettings.colorRed); bidsizeLabel.setTextColor(Color.WHITE); }
						 /
						if (gapBidSize > 0) {
							bidsizeLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
							bidsizeLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
						} else if (gapBidSize < 0) {
							bidsizeLabel.setBackgroundColor(getResources().getColor(R.color.DownStatus_BgColor));
							bidsizeLabel.setTextColor(getResources().getColor(R.color.DownStatus_FgColor));
						}
						// Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - END

						gapBidPrice = marketDepthBidPrice.get(id) - market.getBidPrice();
						/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - START 
						 * if(gapBidPrice>0.0){
						 * 		bidpriceLabel.setBackgroundColor(LayoutSettings.colorGreen);
						 * 		bidpriceLabel.setTextColor(Color.BLACK); 
						 * }else if(gapBidPrice<0.0){
						 * 		bidpriceLabel.setBackgroundColor(LayoutSettings.colorRed);
						 * 		bidpriceLabel.setTextColor(Color.WHITE); 
						 * }
						 /
						if (gapBidPrice > 0.0) {
							bidpriceLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
							bidpriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
						} else if (gapBidPrice < 0.0) {
							bidpriceLabel.setBackgroundColor(getResources().getColor(R.color.DownStatus_BgColor));
							bidpriceLabel.setTextColor(getResources().getColor(R.color.DownStatus_FgColor));
						}
						// Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - END

						gapAskSize = marketDepthAskSize.get(id)	- market.getAskSize();
						if (gapAskSize > 0) {
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							 * asksizeLabel.setBackgroundColor(LayoutSettings.colorGreen);
							 * asksizeLabel.setTextColor(Color.BLACK);
							 /
							asksizeLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
							asksizeLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
						} else if (gapAskSize < 0) {
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							 * asksizeLabel.setBackgroundColor(LayoutSettings.colorRed);
							 * asksizeLabel.setTextColor(Color.WHITE);
							 /
							asksizeLabel.setBackgroundColor(getResources().getColor(R.color.DownStatus_BgColor));
							asksizeLabel.setTextColor(getResources().getColor(R.color.DownStatus_FgColor));
						}

						gapAskPrice = marketDepthAskPrice.get(id) - market.getAskPrice();
						if (gapAskPrice > 0.0) {
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							 * askpriceLabel.setBackgroundColor(LayoutSettings.colorGreen);
							 * askpriceLabel.setTextColor(Color.BLACK);
							 /
							askpriceLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
							askpriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
						} else if (gapAskPrice < 0.0) {
							/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
							 * askpriceLabel.setBackgroundColor(LayoutSettings.colorRed);
							 * askpriceLabel.setTextColor(Color.WHITE);
							 /
							askpriceLabel.setBackgroundColor(getResources().getColor(R.color.DownStatus_BgColor));
							askpriceLabel.setTextColor(getResources().getColor(R.color.DownStatus_FgColor));
						}

						bidsizeLabel.postDelayed(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								bidsizeLabel.setBackgroundColor(Color.TRANSPARENT);
								bidsizeLabel.setTextColor(Color.BLACK);
							}
						}, 1500);

						final int postID = id;
						bidpriceLabel.postDelayed(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								bidpriceLabel.setBackgroundColor(Color.TRANSPARENT);

								if (marketDepthBidPrice.get(postID) > stockInfo.getPreviousClose())
									/*
									 * Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
									 * bidpriceLabel.setTextColor(LayoutSettings.colorGreen);
									 /
									bidpriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_BgColor));
								else if (marketDepthBidPrice.get(postID) < stockInfo.getPreviousClose() && marketDepthBidPrice.get(postID) != 0)
									/*
									 * Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
									 * bidpriceLabel.setTextColor(LayoutSettings.colorRed);
									 /
									bidpriceLabel.setTextColor(getResources().getColor(R.color.DownStatus_BgColor));
								else
									/*
									 * Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
									 * bidpriceLabel.setTextColor(Color.BLACK);
									 /
									bidpriceLabel.setTextColor(getResources().getColor(R.color.NoStatus_FgColor));
							}
						}, 1500);

						asksizeLabel.postDelayed(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								asksizeLabel.setBackgroundColor(Color.TRANSPARENT);
								asksizeLabel.setTextColor(Color.BLACK);
							}
						}, 1500);

						askpriceLabel.postDelayed(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								askpriceLabel.setBackgroundColor(Color.TRANSPARENT);

								if (marketDepthAskPrice.get(postID) > stockInfo.getPreviousClose())
									/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
									 * askpriceLabel.setTextColor(LayoutSettings.colorGreen);
									 /
									askpriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_BgColor));
								else if (marketDepthAskPrice.get(postID) < stockInfo.getPreviousClose() && marketDepthAskPrice.get(postID) != 0.0)
									/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
									 * askpriceLabel.setTextColor(LayoutSettings.colorRed);
									 /
									askpriceLabel.setTextColor(getResources().getColor(R.color.DownStatus_BgColor));
								else
									/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
									 * askpriceLabel.setTextColor(Color.BLACK);
									 /
									askpriceLabel.setTextColor(getResources().getColor(R.color.NoStatus_FgColor));
							}
						}, 1500);

						if( marketDepthBidSize.get(id) != market.getBidSize() )
							/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
							/* Mary@20120911-Change_Request-20120719, ReqNo.12
							bidsizeLabel.setText(FormatUtil.formatLong(marketDepthBidSize.get(id)));
							/
							bidSizeLabel.setText( FormatUtil.formatLong(marketDepthBidSize.get(id) / intLotSize) );
							/
							bidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepthBidSize.get(id) / intLotSize) ) );

						if (marketDepthBidPrice.get(id) != market.getBidPrice()) {
							if( symbolExchg[1].equals("JK")	|| symbolExchg[1].equals("JKD") )
								bidpriceLabel.setText(FormatUtil.formatPrice2(marketDepthBidPrice.get(id)));
							else
								bidpriceLabel.setText(FormatUtil.formatPrice(marketDepthBidPrice.get(id)));
						}

						if( marketDepthAskSize.get(id) != market.getAskSize() )
							/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
							/* Mary@20120911-Change_Request-20120719, ReqNo.12
							asksizeLabel.setText(FormatUtil.formatLong(marketDepthAskSize.get(id)));
							/
							asksizeLabel.setText(FormatUtil.formatLong(marketDepthAskSize.get(id) / intLotSize));
							/
							asksizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepthAskSize.get(id) / intLotSize) ) );

						if( marketDepthAskPrice.get(id) != market.getAskPrice() ){
							if( symbolExchg[1].equals("JK") || symbolExchg[1].equals("JKD") )
								askpriceLabel.setText( FormatUtil.formatPrice2( marketDepthAskPrice.get(id) ) );
							else
								askpriceLabel.setText( FormatUtil.formatPrice( marketDepthAskPrice.get(id) ) );
						}

						// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo. 3 - START
						if( !AppConstants.isMarketOrderStock( market.getBidPrice() ) && !AppConstants.isMarketPriceStock( market.getBidPrice() ) ){
							totalBid 	+= market.getBidSize();
							bidSum 		+= market.getBidSize() * market.getBidPrice();
						}
						
						if( !AppConstants.isMarketOrderStock( market.getAskPrice() ) && !AppConstants.isMarketPriceStock( market.getAskPrice() ) ){
							totalAsk 	+= market.getAskSize();
							askSum 		+= market.getAskSize()	* market.getAskPrice();
						}
						// Added by Mary@20121001 - Fixes_Request-20120815, ReqNo. 3 - END
						id++;
					}

					/* Mary@20121001 - Fixes_Request-20120815, ReqNo. 3
					for (Iterator itr = marketDepth.iterator(); itr.hasNext();) {
						MarketDepth market = (MarketDepth) itr.next();

						if (market.getBidPrice() != -999001 && market.getBidPrice() != 999001 && market.getBidPrice() != -999002 && market.getBidPrice() != 999002) {
							totalBid 	+= market.getBidSize();
							bidSum 		+= market.getBidSize() * market.getBidPrice();
						}

						if (market.getAskPrice() != -999001	&& market.getAskPrice() != 999001 && market.getAskPrice() != -999002 && market.getAskPrice() != 999002) {
							totalAsk 	+= market.getAskSize();
							askSum 		+= market.getAskPrice()	* market.getAskSize();
						}

					}
					*/

					/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
					/* Mary@20120911-Change_Request-20120719, ReqNo.12
					bidSizeLabel.setText(totalBid == 0? "0" : FormatUtil.formatLong(totalBid));
					/
					bidSizeLabel.setText(FormatUtil.formatLong(totalBid / intLotSize));
					/
					bidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalBid / intLotSize) ) );
					
					/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
					/* Mary@20120911-Change_Request-20120719, ReqNo.12
					askSizeLabel.setText(FormatUtil.formatLong(totalAsk));
					/
					askSizeLabel.setText(FormatUtil.formatLong(totalAsk	/ intLotSize));
					/
					askSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalAsk / intLotSize) ) );

					float averageBid = 0;
					if (bidSum != 0 && totalBid != 0)
						averageBid = (float) bidSum / totalBid;

					bidPriceLabel.setText(FormatUtil.formatPrice(averageBid));
					bidPriceLabel.setTextColor(Color.BLACK);

					float averageAsk = 0;
					if(askSum != 0 && totalAsk != 0)
						averageAsk = (float) askSum / totalAsk;

					askPriceLabel.setText(FormatUtil.formatPrice(averageAsk));
					askPriceLabel.setTextColor(Color.BLACK);

					totalBidLabel.setText(FormatUtil.formatLong(Double.valueOf(bidSum).longValue()));
					totalAskLabel.setText(FormatUtil.formatLong(Double.valueOf(askSum).longValue()));
					// Kw@20130510 - Fixes_Request-20130424, ReqNo.7 
					//marketDepths = QcMessageParser.parseMarketDepth2(symbol);
					stockInfo.setMarketDepths(marketDepths);
				}
				*/
				if(isMarketDepthShow)
					new MarketDepthTask().execute( mView.get(INT_MARKET_DEPTH_PAGE_POS) );
				// Mary@20130612 - Fixes_Request-20130523, ReqNo.9 - END
				
				// Sonia@20130820 - Change_Request-20130225, ReqNo.5 - START
				AppConstants.MAXRECORDNO = symbol.getLastTrade_No();
				if(AppConstants.MAXRECORDNO > AppConstants.FINAL_LASTRECORDNO)
				hasLatestRecord = true;
				// Sonia@20130820 - Change_Request-20130225, ReqNo.5 - END
			}
		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private class MyPagerAdapter extends PagerAdapter {
		public int getCount() {
			/* Mary@20120723 - ChangeRequest-20120719, ReqNo.1 
			 * return 6;
			 */
			/* Mary@20130620 - Change_Request-20130424, ReqNo.9
			return (AppConstants.isShowFundamentalCapitalICQ ? 7 : 6);
			*/
			// Sonia@20131001 - Change_Request-20130225, ReqNo.5 - START
			/*// Added by Sonia@20130702 - Change_Request-20130225, ReqNo.5
			return (isShowFundamentalCapitalIQ ? 8 : 7);*/
			
			return (isShowFundamentalCapitalIQ ? 9 : 8);
			// Sonia@20131001 - Change_Request-20130225, ReqNo.5 - END
		}

		public Object instantiateItem(View collection, int position) {
			((ViewPager) collection).addView(mView.get(position), 0);
			
			/* Mary@20121001 - Fixes_Request-201200815, ReqNo.3 - START
			if (position == 1) {
				new StockDetailTask().execute(mView.get(position));
			}

			if (position == 2) {
				new MarketDepthTask().execute(mView.get(position));
			}

			if (position == 3) {
				noResultFound = (TextView) mView.get(position).findViewById(
						R.id.noResultsFoundView);
				linearProgress = (LinearLayout) mView.get(position)
						.findViewById(R.id.linearProgressBar);
				linearProgress.setVisibility(View.VISIBLE);

				new StockNewsTask().execute(mView.get(position));
			}

			if (position == 4) {
				chartView = (ImageView) mView.get(position).findViewById(
						R.id.chartImageView);
				noChartFound = (TextView) mView.get(position).findViewById(
						R.id.noChartFoundView);
				chartView.setScaleType(ScaleType.CENTER);
				chartView.setImageResource(LayoutSettings.loading_animate);
				new StockChartTask().execute(mView.get(position));
			}

			// Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - START
			if (position == 5 && AppConstants.isShowFundamentalCapitalICQ) {
				new FundamentalCapitalMenuTask().execute(mView.get(position));
			}
			// Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - END
			*/
			
			switch(position){
				case INT_STOCK_DETAILS_PAGE_POS :
					//new StockDetailTask().execute(mView.get(position));	//Edited by Thinzar, Change_Request-20141101, ReqNo. 6
					new StockRefreshImage6Task().execute(mView.get(position));
					break;
				/* Mary@20130612 - Fixes_Request-20130523, ReqNo.9
				case 2 :
				*/
				case INT_MARKET_DEPTH_PAGE_POS :
					new MarketDepthTask().execute(mView.get(position));
					break;
					
				case INT_STOCK_CHART_PAGE_POS :	
					stockChartWebView 	=(WebView)mView.get(position).findViewById(R.id.stockChartWebView);
					chartView 			= (ImageView) mView.get(position).findViewById(R.id.chartImageView);
					noChartFound 		= (TextView) mView.get(position).findViewById(R.id.noChartFoundView);
					chartProgressBar 	= (ProgressBar) mView.get(position).findViewById(R.id.chartProgressBar);
					/* Fixes_Request_20170701, ReqNo.7
					chartView.setScaleType(ScaleType.CENTER);
					chartView.setImageResource(LayoutSettings.loading_animate);
					 */
					chartView.setVisibility(View.GONE);
					chartProgressBar.setVisibility(View.VISIBLE);
					
					new StockChartTask().execute(mView.get(position));
					
					break;
				// Added by Sonia@20130702 - Change_Request-20130225, ReqNo.5 - START
				/*case INT_TIME_SALES_PAGE : break;*/ 
				case INT_BUSINESS_DONE_PAGE_POS : 
					
					new BusinessDoneTask().execute( mView.get(position) );
					break;
					
				case INT_STOCK_NEWS_PAGE_POS :
					
					noResultFound	= (TextView) mView.get(position).findViewById(R.id.noResultsFoundView);
					linearProgress	= (LinearLayout) mView.get(position).findViewById(R.id.linearProgressBar);
					
					linearProgress.setVisibility(View.VISIBLE);
					noResultFound.setVisibility(View.GONE);
					
					if(AppConstants.brokerConfigBean.isEnableElasticNews()){
						new GetElasticNews().execute(mView.get(position));
						
					}else if(AppConstants.brokerConfigBean.isArchiveNews()){
						String[] parts = symbolCode.split("\\.");
						code = parts[0]; // code
						
						new GetArchiveNews().execute(mView.get(position));
						
					}else{
						new StockNewsTask().execute(mView.get(position));
					}
					
					break;
				// Sonia@20131001 - Change_Request-20130225, ReqNo.5 - START
				
				// Sonia@20131001 - Change_Request-20130225, ReqNo.5 - END
				case INT_FUNDAMENTAL_PAGE_POS :
					/* Mary@20130620 - Change_Request-20130424, ReqNo.9
					if (AppConstants.isShowFundamentalCapitalICQ)
					*/			
					if(isShowFundamentalCapitalIQ)
						new FundamentalCapitalMenuTask().execute( mView.get(position) );
					
					break;
					// Added by Sonia@20130702 - Change_Request-20130225, ReqNo.5 - END
			}
			// Mary@20121001 - Fixes_Request-201200815, ReqNo.3 - END			

			return mView.get(position);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2){
			( (ViewPager) arg0 ).removeView( mView.get(arg1) );
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1){
			return arg0 == ((View) arg1);
		}

		@Override
		public Parcelable saveState(){
			return null;
		}
	}

	private class StockChartTask extends AsyncTask<View, Void, View> {
		@Override
		protected void onPreExecute() {
			
		}

		@Override
		protected View doInBackground(View... view) {
			View viewChart					= view[0];

			Map<String, Object> parameters 	= new HashMap<String, Object>();
			parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
			/* Mary@20120803 - move UI statement from doInBackground to onPreExecute to avoid CalledFromWrongThreadException
			 * if(chartView.getDrawable()!=null){
			 * 		noChartFound.setVisibility(View.GONE); loadAnimation = (AnimationDrawable)chartView.getDrawable();
			 * 		loadAnimation.start(); 
			 * }
			 */
			/* Mary@20121001 - Fixes_Request-201200815, ReqNo.3 - START
			if (chartDuration == AppConstants.CHART_DURATION_1_DAY) {
				StockActivity.this.displayChartEvent2(parameters,
						CommandConstants.CHART_TYPE_INTRADAY);
			}

			if (chartDuration == AppConstants.CHART_DURATION_1_MONTH) {
				parameters.put(ParamConstants.CHART_DURATION_TAG, new Integer(
						30));
				StockActivity.this.displayChartEvent2(parameters,
						CommandConstants.CHART_TYPE_HISTORICAL);
			}

			if (chartDuration == AppConstants.CHART_DURATION_3_MONTHS) {
				parameters.put(ParamConstants.CHART_DURATION_TAG, new Integer(
						90));
				StockActivity.this.displayChartEvent2(parameters,
						CommandConstants.CHART_TYPE_HISTORICAL);
			}

			if (chartDuration == AppConstants.CHART_DURATION_6_MONTHS) {
				parameters.put(ParamConstants.CHART_DURATION_TAG, new Integer(
						180));
				StockActivity.this.displayChartEvent2(parameters,
						CommandConstants.CHART_TYPE_HISTORICAL);
			}

			if (chartDuration == AppConstants.CHART_DURATION_1_YEAR) {
				parameters.put(ParamConstants.CHART_DURATION_TAG, new Integer(
						365));
				StockActivity.this.displayChartEvent2(parameters,
						CommandConstants.CHART_TYPE_HISTORICAL);
			}

			if (chartDuration == AppConstants.CHART_DURATION_2_YEARS) {
				parameters.put(ParamConstants.CHART_DURATION_TAG, new Integer(
						730));
				StockActivity.this.displayChartEvent2(parameters,
						CommandConstants.CHART_TYPE_HISTORICAL);
			}
			*/
			System.out.println("StockChartTask chartDuration:" + chartDuration);
			switch(chartDuration){
				case AppConstants.CHART_DURATION_1_DAY :
					StockActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_INTRADAY);
					break;

				case AppConstants.CHART_DURATION_1_MONTH :
					parameters.put( ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_1MONTH);
					StockActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL);
					break;

				case AppConstants.CHART_DURATION_3_MONTHS :
					parameters.put( ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_3MONTHS );
					StockActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL);
					break;

				case AppConstants.CHART_DURATION_6_MONTHS :
					parameters.put( ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_6MONTHS);
					StockActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL);
					break;

				case AppConstants.CHART_DURATION_1_YEAR :
					parameters.put( ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_12MONTHS );
					StockActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL);
					break;

				case AppConstants.CHART_DURATION_2_YEARS :
					parameters.put( ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_24MONTHS );
					StockActivity.this.displayChartEvent2(parameters, CommandConstants.CHART_TYPE_HISTORICAL);
					break;
			}
			// Mary@20121001 - Fixes_Request-201200815, ReqNo.3 - END

			return viewChart;
		}

		@Override
		protected void onPostExecute(View view) {
			super.onPostExecute(view);

			StockActivity.this.prepareStockChartView(view);
		}
	}

	private class MarketDepthTask extends AsyncTask<View, Void, View> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private Exception exception 					= null;
		private AsyncTask<View, Void, View> updateTask 	= null;
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private List<MarketDepth> lstOldMarketDepth		= new ArrayList<MarketDepth>();

		@Override
		protected void onPreExecute() {
			updateTask = this;
			if(lstNewMarketDepth != null){
				for(Iterator<MarketDepth> itr = lstNewMarketDepth.iterator(); itr.hasNext();){
					lstOldMarketDepth.add( itr.next() );
				}
			}
		}

		@Override
		protected View doInBackground(View... view) {
			String[] symbolCodes = { symbolCode };
			
			//System.out.println("marketdepth symcode:"+symbolCode + "RESPONDE:"+ AppConstants.getMarketDepthLvl("avb"));
	//		response	= QcHttpConnectUtil.imageQuote(symbolCode, AppConstants.getMarketDepthLvl(splitTrdExchgCode));
	
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START 
			 * Map parameters = new HashMap();
			 * parameters.put(ParamConstants.USERPARAM_TAG, userParam);
			 * parameters.put(ParamConstants.SYMBOLCODES_TAG, symbolCodes);
			 * 
			 * String response = null; 
			 * try{ 
			 * 		response = QcHttpConnectUtil.imageQuote(parameters,
			 * 		CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
			 * 
			 */
			String response = null;
			try {
				//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 -START
				 if(AppConstants.brokerConfigBean.isMultilevelMarketDepth()){
					 response	= QcHttpConnectUtil.imageQuote2(symbolCode,AppConstants.getMarketDepthLvl(splitQCExchgCode));//AppConstants.getMarketDepthLvl("SG"));//
					 while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
						intRetryCtr++;
						response	= QcHttpConnectUtil.imageQuote2(symbolCode, AppConstants.getMarketDepthLvl(splitQCExchgCode));//AppConstants.getMarketDepthLvl("SG"));//splitTrdExchgCode));
						//response	= QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
					}
					intRetryCtr	= 0;
					
					if (response == null)
						throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
					//System.out.println("MARKETDEPTH NEW :"+response);
					lstNewMarketDepth= QcMessageParser.parseMarketDepth4(response);
				 }else{
					response	= QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
					while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
						intRetryCtr++;
						//response	= QcHttpConnectUtil.imageQuote2(symbolCode, AppConstants.getMarketDepthLvl(splitTrdExchgCode));
						response	= QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_MARKETDEPTH);
					}
					intRetryCtr	= 0;
					
					if (response == null)
						throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
					
					/* Mary@20130725 - Fixes_Request-20130711, ReqNo.9
					lstNewMarketDepth= QcMessageParser.parseMarketDepth3(response,AppConstants.getMarketDepthLvl(splitTrdExchgCode));
					*/
					lstNewMarketDepth= QcMessageParser.parseMarketDepth3(response,AppConstants.getMarketDepthLvl(splitQCExchgCode));
				 }
				
			
				//	marketDepths= QcMessageParser.parseMarketDepth(response);
				//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 END
		
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START 
			 * }catch (FailedAuthenicationException e){
			 * 		updateTask.cancel(true);
			 * 		new MarketDepthTask().execute(view[0]); // might caused infinite loop if FailedAuthenicationException continuously thrown
			 * 		e.printStackTrace(); 
			 * }
			 */
			} catch (Exception e) {
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			return view[0];
		}
		
		@Override
		protected void onPostExecute(View view) {
			super.onPostExecute(view);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					alertException.setButton(getResources().getString(R.string.dialog_button_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() )
						alertException.show();
					exception = null;
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
				stockInfo.setMarketDepths(lstNewMarketDepth);
				
				/* Mary@20130612 - Fixes_Request-20130523, ReqNo.9
				StockActivity.this.prepareMarketDepthView(view);
				*/
				StockActivity.this.prepareMarketDepthView(view, lstOldMarketDepth, lstNewMarketDepth);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - START
	private class TimeSalesTask extends AsyncTask<View, Void, View> {
		
		private Exception exception 					= null;
		private AsyncTask<View, Void, View> updateTask 	= null;
		private RecordAge recordAge						= RecordAge.NEW;
		private List<TimeSales> lstTimeSales			= new ArrayList<TimeSales>();
		String[] symbolExchg							= null;
		
		TimeSalesTask(RecordAge recordAge){
			this.recordAge	= recordAge;
		}
		
		@Override
		protected void onPreExecute() {
			System.out.println("debug: timesales");
			updateTask = this;
			
			//Added by Thinzar, Fixes_Request-20150401, ReqNo.6
			timeSalesViewPref		= AppConstants.brokerConfigBean.getIntViewQtyMeasurement();
		}
		
		@Override
		protected View doInBackground(View... view) {
			
			String response = null;
			try {
					releasePullToRefresh 	= false;
					isExchangePH 			= false;
					symbolExchg 			= stockInfo.getSymbolCode().split("\\.");
					
					int intIndexFrom		= 0;
					int intIndexTo			= 0;
					
					if(newTransList.size() > 0){
						if(this.recordAge == RecordAge.NEW){
							intIndexFrom	= newTransList.get(0).getRecordsNo() + 1;
							intIndexTo		= intIndexFrom + 99;
						}else if(this.recordAge == RecordAge.OLD){
							intIndexTo		= newTransList.get(newTransList.size() - 1).getRecordsNo() - 1;
							intIndexFrom	= intIndexTo > 99 ? (intIndexTo - 99) : 0;
						}
					}else{
						intIndexTo			= stockInfo.getLastTradeNo() > AppConstants.MAXRECORDNO ? stockInfo.getLastTradeNo() : AppConstants.MAXRECORDNO;
						if(intIndexTo == -1){
							intIndexTo		= (int) (stockInfo.getTotalTrade() - 1);
							Log.d("SoniaInvestigate: -1", String.valueOf(intIndexTo));
							if(intIndexTo == -1)
								intIndexTo	= 99;
						}
						intIndexFrom		= intIndexTo > 99 ? (intIndexTo - 99) : 0;
					}
					
					// intIndexTo = -1 to incase server return feed -1 (to display all data start from 0 to -1(last))
					if(intIndexTo >= intIndexFrom){
						response 			= QcHttpConnectUtil.transactionTimeSales(symbolCode, intIndexFrom, intIndexTo);
						
						// Added by Sonia@20130918 - Change_Request-20130225, ReqNo.5 - START
						if( symbolExchg[1].equals(AppConstants.EXCHANGECODE_PH) ){
							lstTimeSales		= QcMessageParser.parseTimeSalesPH(response);
							isExchangePH 		= true;
						}else
							lstTimeSales		= QcMessageParser.parseTimeSales(response);
						// Added by Sonia@20130918 - Change_Request-20130225, ReqNo.5 - END
						
						if(lstTimeSales.size() > 0){
							if(newTransList.size() > 0){
								int intNewLastRecordNo	= lstTimeSales.get(0).getRecordsNo(); // smallest record value in new list
								AppConstants.MINRECORDNO = intNewLastRecordNo;
								Collections.reverse(lstTimeSales);
								if( newTransList.get(0).getRecordsNo() < intNewLastRecordNo )
									newTransList.addAll(0, lstTimeSales);
								else
									newTransList.addAll(newTransList.size(), lstTimeSales);
							}else{
								int intLastRecordNo = lstTimeSales.get(0).getRecordsNo(); // get the smallest record no in array list = MINRECORDNO
								AppConstants.MINRECORDNO = intLastRecordNo;
								Collections.reverse(lstTimeSales);
								newTransList.addAll(lstTimeSales);
							}
						}
						releasePullToRefresh = true;
					}
					AppConstants.FINAL_LASTRECORDNO = newTransList.size() > 0 ? newTransList.get(0).getRecordsNo() : 0;
			} catch (Exception e) {
				exception = e;
			}
			return view[0];
		}
		
		@Override
		protected void onPostExecute(View view) {
			super.onPostExecute(view);
			
			linearProgress.setVisibility(View.GONE);
			
			if (exception != null) {
				alertException.setMessage(exception.getMessage());
				alertException.setButton(getResources().getString(R.string.dialog_button_ok),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
				if( ! StockActivity.this.isFinishing() )
					alertException.show();
				exception = null;
				return;
			}
			StockActivity.this.prepareTimeSalesView(view, newTransList);
			
			releasePullToRefresh = true;
		}
	}
	// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - END
	
	// Added by Sonia@20131001 - ChangeRequest-20130225, ReqNo.5 - START
	private class BusinessDoneTask extends AsyncTask<View, Void, View> {
		
		private Exception exception 					= null;
		private AsyncTask<View, Void, View> updateTask 	= null;
		private List<BusinessDone> lstBusinessDone		= new ArrayList<BusinessDone>();
		String[] symbolExchg							= null;
		
		@Override
		protected void onPreExecute() {
			
			updateTask = this;
			
			//Added by Thinzar, Fixes_Request-20150401, ReqNo. 6
			businessDoneViewPref	= AppConstants.brokerConfigBean.getIntViewQtyMeasurement();
		}
		
		@Override
		protected View doInBackground(View... view) {
			
			String response = null;
			try {
				response 			= QcHttpConnectUtil.getBusinessDoneFeed(symbolCode);
				lstBusinessDone		= QcMessageParser.parseBusinessDone(response);
			} catch (Exception e) {
				exception = e;
			}
			return view[0];
		}
		
		@Override
		protected void onPostExecute(View view) {
			super.onPostExecute(view);
			
			if (exception != null) {
				alertException.setMessage(exception.getMessage());
				alertException.setButton(getResources().getString(R.string.dialog_button_ok),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
				if( ! StockActivity.this.isFinishing() )
					alertException.show();
				exception = null;
				return;
			}
			StockActivity.this.prepareBusinessDoneView(view, lstBusinessDone);			
		}
	}
	// Added by Sonia@20131001 - ChangeRequest-20130225, ReqNo.5 - END
	
	private class StockDetailTask extends AsyncTask<View, Void, View> {
		@Override
		protected View doInBackground(View... view) {			
			return view[0];
		}	
		
		@Override
		protected void onPostExecute(View view) {
			super.onPostExecute(view);

			StockActivity.this.prepareStockDetailView(view);
		}
	}

	private class StockNewsTask extends AsyncTask<View, Void, View> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private Exception exception 					= null;
		private AsyncTask<View, Void, View> updateTask 	= null;

		@Override
		protected void onPreExecute() {
			updateTask = this;
		}

		@Override
		protected View doInBackground(View... view) {
			String response = null;

		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START Map
		 * parameters = new HashMap();
		 * parameters.put(ParamConstants.USERPARAM_TAG, userParam);
		 * parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
		 * 
		 * try { response = QcHttpConnectUtil.newsUpdate(parameters);
		 */
			try {
				response = QcHttpConnectUtil.newsUpdate(symbolCode);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = QcHttpConnectUtil.newsUpdate(symbolCode);
				}
				intRetryCtr = 0;

				/* NOTE : NO alert of exception as it already have handling for
				 * null value and NULL is acceptable where it will auto
				 * defaulted to get default exchange code
				 */
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START 
			 * } catch (FailedAuthenicationException e) {
			 * 		updateTask.cancel(true); new
			 * 		StockNewsTask().execute(); // might caused infinite loop if FailedAuthenicationException continuously thrown 
			 * 		e.printStackTrace(); 
			 * }
			 */
			} catch (Exception ex) {
				exception = ex;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END;

			newsUpdates = null;
			
			if (response != null)
				newsUpdates = QcMessageParser.parseNewsUpdate(response);

			return view[0];
		}
		
		@Override
		protected void onPostExecute(View view) {
			super.onPostExecute(view);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				linearProgress.setVisibility(View.GONE);
	
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					alertException.setButton(getResources().getString(R.string.dialog_button_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() )
						alertException.show();
					exception = null;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
				StockActivity.this.prepareNewsUpdateView(view);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	private void showWatchlistDialog(final String[] items) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(StockActivity.this);
			builder.setTitle(getResources().getString(R.string.longclick_add_watchlist));
			builder.setItems(items, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
					
					
					watchlist 			= watchlists.get(item);
					String[] itemName 	= { items[item] };
	
					List<String[]> arr 	= new ArrayList<String[]>();
					arr.add(items);
					arr.add(itemName);
	
					new getWatchlistSymbol().execute(arr);
	
					dialog.dismiss();
				}
			});
			
			AlertDialog alert		= builder.create();
			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
			if( ! StockActivity.this.isFinishing() )
				alert.show();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	private class getWatchlistSymbol extends AsyncTask<List<String[]>, Void, String[]> {
		private String[] symbols;
		private String itemName;
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private Exception exception 	= null;
		
		@Override
		protected String[] doInBackground(List<String[]>... list) {			
			String[] item	= list[0].get(1);
			itemName 		= item[0];

			/*
			 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START 
			 * symbols = StockActivity.this.getWatchlistSymbols(watchlist.getId());
			 */
			try {
				symbols = StockActivity.this.getWatchlistSymbols(watchlist.getId());
			}catch (Exception e){
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			return list[0].get(0);
		}
		
		@Override
		protected void onPostExecute(final String[] items) {
			super.onPostExecute(items);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if (isExpired == true) {
					StockActivity.this.sessionExpired();
					return;
				}
	
				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() )
						alertException.show();
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
				// Added by Mary@20130828 - Change_Request-20130724, ReqNo.10 - START
				if(AppConstants.brokerConfigBean.isImposeMaxStockAddToWatchlist()
						& symbols.length >= AppConstants.brokerConfigBean.getIntMaxNoOfStockAddToWatchlist() ){
					AlertDialog alert = new AlertDialog.Builder(StockActivity.this)
					.setTitle(getResources().getString(R.string.max_stock_per_watchlist_title))
					.setMessage(getResources().getString(R.string.max_stock_per_watchlist_msg) + " :" + AppConstants.brokerConfigBean.getIntMaxNoOfStockAddToWatchlist())
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							dialog.dismiss();
						}
					})
					.create();
				
					if( ! StockActivity.this.isFinishing() )
						alert.show();
					
					return;
				}else{
				// Added by Mary@20130828 - Change_Request-20130724, ReqNo.10 - END
					for (int i = 0; i < symbols.length; i++) {
						String symbol = symbols[i];
						if( symbol.equals(symbolCode) ){
							AlertDialog alert = new AlertDialog.Builder(StockActivity.this)
								.setTitle(getResources().getString(R.string.watchlist_duplicate_title))
								.setMessage(getResources().getString(R.string.watchlist_duplicate_msg))
								.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int whichButton) {
										dialog.dismiss();
										StockActivity.this.showWatchlistDialog(items);
									}
								})
								.create();
							
							// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
							if( ! StockActivity.this.isFinishing() )
								alert.show();
							
							return;
						}
					}
		
					/* Mary@20130130 - Fixes_Request-20130110, ReqNo.13
					new addwatchlistStock().execute(itemName);
					*/
					String[] strArr	= {watchlist.getId(), itemName, symbolCode};
					new addwatchlistStock().execute(strArr);
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	private class addwatchlistStock extends AsyncTask<String, Void, String> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private Exception exception = null;

		@Override
		protected String doInBackground(String... item) {
			/*
			 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			 * StockActivity.this.addWatchlistStock(watchlist); return item[0];
			 */
			try {
				/* Mary@20130130 - Fixes_Request-20130110, ReqNo.13
				StockActivity.this.addWatchlistStock(watchlist);
				*/
				StockActivity.this.addWatchlistStock(item[0], item[1], item[2]);
			} catch (Exception e) {
				exception = e;
			}
			return item[1];
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if (isExpired) {
					StockActivity.this.sessionExpired();
					return;
				}
	
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() )
						alertException.show();
					
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.added_to_watchlist) + result, Toast.LENGTH_SHORT).show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

	}

	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	// private String[] getWatchlistSymbols(String watchlistId) {
	private String[] getWatchlistSymbols(String watchlistId) throws Exception {
		String atpUserParam 			= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
		String senderCode 				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

		Map<String, Object> parameters 	= new HashMap<String, Object>();
		// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if( AppConstants.brokerConfigBean.isEnableEncryption() )
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
		// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
		
		parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
		parameters.put(ParamConstants.FAVORATEID_TAG, watchlistId);

		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START 
		 * try { 
		 * 		String response 	= AtpConnectUtil.getFavListStockCode(parameters);
		 * 		String[] stockCodes = AtpMessageParser.parseFavListStockCode(response);
		 * 
		 * 		return stockCodes; 
		 * } catch (FailedAuthenicationException e) {
		 * 		e.printStackTrace();
		 * 		if(e.getMessage().equals("Session is expired")){ 
		 * 			isExpired = false;
		 * 			refreshThread.stopRequest(); 
		 * 			loginThread.stopRequest();
		 * 			LoginActivity.QCAlive.stopRequest(); 
		 * 			this.sessionExpired(); 
		 * 		} 
		 * }
		 * return null;
		 */
		String response = null;
		try {
			response = AtpConnectUtil.getFavListStockCode(parameters);
			while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
				intRetryCtr++;
				response = AtpConnectUtil.getFavListStockCode(parameters);
			}
			intRetryCtr = 0;

			if (response == null)
				throw new Exception(ErrorCodeConstants.FAIL_GET_WATCHLIST_STOCK);
		} catch (FailedAuthenicationException e) {
			isExpired = true;
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(refreshThread != null)
				refreshThread.stopRequest();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(loginThread != null)
				loginThread.stopRequest();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(LoginActivity.QCAlive != null)
				LoginActivity.QCAlive.stopRequest();
			throw e;
		} catch (Exception e) {
			throw e;
		}

		return AtpMessageParser.parseFavListStockCode(response);
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}

	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	// private void addWatchlistStock(Watchlist watchlist) {
	/* Mary@20130130 - Fixes_Request-20130110, ReqNo.13
	private void addWatchlistStock(Watchlist watchlist) throws Exception {
	*/
	private void addWatchlistStock(String strWatchlistID, String strWatchlistName, String strStockSymbolCode) throws Exception {
		String atpUserParam				= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
		String senderCode 				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

		Map<String, Object> parameters	= new HashMap<String, Object>();
		// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		*/
		if( AppConstants.brokerConfigBean.isEnableEncryption() )
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
		// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);

		parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
		/* Mary@20130130 - Fixes_Request-20130110, ReqNo.13 - START
		parameters.put(ParamConstants.FAVORATEID_TAG, watchlist.getId());
		int index = symbolCode.indexOf(".");
		if (index != -1) {
			parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode.substring(0, index));
		} else {
			parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
		}
		*/
		parameters.put(ParamConstants.FAVORATEID_TAG, strWatchlistID);
		
		// Added by Mary@20130625 - Fixes_Request-20130523, ReqNo.21 - START
		StockApplication application= (StockApplication) this.getApplication();
		String strExchangeCode		= application.getSelectedExchange().getQCExchangeCode();
		// Added by Mary@20130625 - Fixes_Request-20130523, ReqNo.21 - END
					
		/* Mary@20130625 - Fixes_Request-20130523, ReqNo.20
		int index 						= strStockSymbolCode.indexOf(".");
		*/
		int index 					= strStockSymbolCode.lastIndexOf(".");
		if(index != -1){
			parameters.put( ParamConstants.SYMBOLCODE_TAG, strStockSymbolCode.substring(0, index) );
			// Added by Mary@20130625 - Fixes_Request-20130523, ReqNo.21
			strExchangeCode	= strStockSymbolCode.substring( (index + 1), strStockSymbolCode.length() );
		}else{
			parameters.put(ParamConstants.SYMBOLCODE_TAG, strStockSymbolCode);
		}
		// Mary@20130130 - Fixes_Request-20130110, ReqNo.13 - END
		
		/* Mary@20130625 - Fixes_Request-20130523, ReqNo.21 - START
		StockApplication application = (StockApplication) this.getApplication();
		parameters.put(ParamConstants.EXCHANGECODE_TAG, application.getSelectedExchange().getQCExchangeCode());
		*/
		strExchangeCode				= (strExchangeCode.length() > 1 && strExchangeCode.toUpperCase().endsWith("D") ) ? strExchangeCode.substring(0, (strExchangeCode.length() - 1) )  : strExchangeCode;
		parameters.put(ParamConstants.EXCHANGECODE_TAG, strExchangeCode);
		// Mary@20130625 - Fixes_Request-20130523, ReqNo.21 - END
		
		/*
		 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START try {
		 * 
		 * AtpConnectUtil.addFavListStock(parameters);
		 * 
		 * } catch (FailedAuthenicationException e) { 
		 * 		e.printStackTrace();
		 * 		if(e.getMessage().equals("Session is expired")){ 
		 * 			isExpired = true;
		 * 			refreshThread.stopRequest(); loginThread.stopRequest();
		 * 			LoginActivity.QCAlive.stopRequest(); 
		 * 		} 
		 * }
		 */

		try {
			boolean isSuccess = AtpConnectUtil.addFavListStock(parameters);
			while (intRetryCtr < AppConstants.intMaxRetry && !isSuccess) {
				intRetryCtr++;
				isSuccess = AtpConnectUtil.addFavListStock(parameters);
			}
			intRetryCtr = 0;

			if (!isSuccess)
				throw new Exception(ErrorCodeConstants.FAIL_ADD_STOCK_INTO_WATCHLIST);
		} catch (FailedAuthenicationException e) {
			isExpired = true;
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(refreshThread != null)
				refreshThread.stopRequest();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(loginThread != null)
				loginThread.stopRequest();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
			if(LoginActivity.QCAlive != null)
				LoginActivity.QCAlive.stopRequest();
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}

	/*
	 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 private void
	 * populateWatchlists() {
	 */
	/* Mary@20130830 - Move to Asyn Task to avoid android.os.NetworkOnMainThreadException - START
	private void populateWatchlists() throws Exception {
		String atpUserParam 			= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
		String senderCode				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

		Map<String, Object> parameters 	= new HashMap<String, Object>();
		// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
		/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
		if(AppConstants.EnableEncryption)
		/
		if( AppConstants.brokerConfigBean.isEnableEncryption() )
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
		// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
			parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
		
		parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
		/*
		 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START try { String
		 * response = AtpConnectUtil.getFavListInfo(parameters); watchlists =
		 * AtpMessageParser.parseFavListInfo(response);
		 * 
		 * } catch (FailedAuthenicationException e) { 
		 * 		e.printStackTrace();
		 * 		refreshThread.stopRequest(); 
		 * 		loginThread.stopRequest();
		 * 		LoginActivity.QCAlive.stopRequest(); 
		 * 		this.sessionExpired(); 
		 * }
		 /
		try {
			String response = AtpConnectUtil.getFavListInfo(parameters);
			while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
				intRetryCtr++;
				response = AtpConnectUtil.getFavListInfo(parameters);
			}
			intRetryCtr = 0;

			if (response == null)
				throw new Exception(ErrorCodeConstants.FAIL_GET_WATCHLIST);

			watchlists = AtpMessageParser.parseFavListInfo(response);
		} catch (Exception e) {
			throw e;
		}
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}
	*/
	private class populateWatchlists extends AsyncTask<Void, Void, Exception>{
		String atpUserParam 								= null;
		String senderCode									= null;
		private boolean isDialogOwner						= false;
		private AsyncTask<Void, Void, Exception> updateTask = null;
		private Exception exception							= null;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			updateTask = this;
			
			try{
				atpUserParam 	= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
				senderCode		= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
				
				if( ! StockActivity.this.dialog.isShowing() ){
					StockActivity.this.dialog.setMessage(getResources().getString(R.string.retrieving_watchlist_msg));
					
					if( ! StockActivity.this.isFinishing() ){
						StockActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		@Override
		protected Exception doInBackground(Void... arg0) {
			Map<String, Object> parameters 	= new HashMap<String, Object>();
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			try {
				String response = AtpConnectUtil.getFavListInfo(parameters);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = AtpConnectUtil.getFavListInfo(parameters);
				}
				intRetryCtr = 0;
	
				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_WATCHLIST);
				
				watchlists = AtpMessageParser.parseFavListInfo(response);
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				return e;
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Void... params) {
			try{
				if( ! StockActivity.this.dialog.isShowing() ){
					StockActivity.this.dialog.setMessage(getResources().getString(R.string.retrieving_watchlist_msg));
					
					if( ! StockActivity.this.isFinishing() ){
						StockActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected void onPostExecute(Exception exception) {
			super.onPostExecute(exception);

			try{
				if (exception != null) {
					if( StockActivity.this.dialog != null && StockActivity.this.dialog.isShowing() && isDialogOwner){
						StockActivity.this.dialog.dismiss();
						isDialogOwner = false;
					}
					
					if(exception instanceof FailedAuthenicationException){
						if (refreshThread != null)
							refreshThread.stopRequest();

						if (loginThread != null)
							loginThread.stopRequest();

						if (LoginActivity.QCAlive != null)
							LoginActivity.QCAlive.stopRequest();

						StockActivity.this.sessionExpired();
					}else{
						alertException.setMessage(exception.getMessage());
						alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								finish();
							}
						});
						
						if( ! StockActivity.this.isFinishing() )
							alertException.show();
					}
					
					exception = null;
					return;
				}

				//edited for crashlytics
				if( !StockActivity.this.isFinishing() && StockActivity.this.dialog != null
						&& StockActivity.this.dialog.isShowing() && isDialogOwner){
					StockActivity.this.dialog.dismiss();
					isDialogOwner = false;
				}
				
				if( watchlists.isEmpty() ){					
					WatchListView watchListView = new WatchListView(StockActivity.this, StockActivity.this, getLayoutInflater());
					watchListView.setHideOnSelect(true);
					try {
						watchListView.setWatchlists(watchlists);
					} catch (Exception e) {			
						e.printStackTrace();
					}
					watchListView.enterWatchlistName("0", true);
					
					return;
				}
				
				final String[] items= new String[watchlists.size()];
				int index			= 0;
				for (Iterator<Watchlist> itr = watchlists.iterator(); itr.hasNext();) {
					Watchlist watchlist = itr.next();
					items[index] 		= watchlist.getName();
					index++;
				}

				StockActivity.this.showWatchlistDialog(items);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	// Mary@20130830 - Move to Asyn Task to avoid android.os.NetworkOnMainThreadException - END
	
	//Added by Thinzar@20141113, Change_Request-20141101, ReqNo. 6 - START
	private class StockRefreshImage6Task extends AsyncTask<View, Void, String[]>{
		private Exception exception			= null;
		private boolean isDialogOwner		= false;
		private View v;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			System.out.println("StockRefreshImage6Task()");
			
			try{
				if( ! StockActivity.this.dialog.isShowing() ){
					StockActivity.this.dialog.setMessage(getResources().getString(R.string.fetching_stock_info_msg));
					
					if( ! StockActivity.this.isFinishing() ){
						StockActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected String[] doInBackground(View... views) {
			v					= views[0];
			String response		= null;
			boolean isUpdated	= false;
			
			try{
				response	= QcHttpConnectUtil.doImageRequest06(symbolCode);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response	= QcHttpConnectUtil.doImageRequest06(symbolCode);
				}
				intRetryCtr = 0;
				
				if(response	== null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
				
				
			} catch (Exception e){
				exception	= e;
			}
			
			if(response != null)	{
				stockInfoExtra	= QcMessageParser.parseStockDetailImage6(response, symbolCode); 
			}
			return null;
		}

		@Override
		protected void onPostExecute(String[] result) {
			super.onPostExecute(result);
			
			if( !StockActivity.this.isFinishing() && StockActivity.this.dialog != null &&
					StockActivity.this.dialog.isShowing() && isDialogOwner){
				StockActivity.this.dialog.dismiss();
				isDialogOwner = false;
			}
			
			if (exception != null) {
				alertException.setMessage(exception.getMessage());
				alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
						finish();
					}
				});
				
				if(!StockActivity.this.isFinishing() )
					alertException.show();
				
				exception = null;
				return;
			} else{
				
				if(v != null) //fixes for Crashlytics
					StockActivity.this.prepareStockDetailView(v);
				
			}
		}
	}
	//Added by Thinzar@20141113, Change_Request-20141101, ReqNo. 6 - END

	private class stockRefresh extends AsyncTask<View, Void, String[]> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private Exception exception 						= null;
		// Added by Mary@20121003 - Fixes_Request-20120724, ReqNo.13
		private boolean isDialogOwner						= false;
		private AsyncTask<View, Void, String[]> updateTask 	= null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			updateTask = this;
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				/* Mary@20121003 - Fixes_Request-20120724, ReqNo.13 - START
				StockActivity.this.dialog.setMessage("Loading...");
				StockActivity.this.dialog.show();
				*/
				if( ! StockActivity.this.dialog.isShowing() ){
					StockActivity.this.dialog.setMessage(getResources().getString(R.string.refreshing_stock_details));
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() ){
						StockActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
				// Mary@20121003 - Fixes_Request-20120724, ReqNo.13 - END
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String[] doInBackground(View... params) {
			String[] symbolCodes = { symbolCode };

			/*
			 * Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START Map
			 * parameters = new HashMap();
			 * parameters.put(ParamConstants.USERPARAM_TAG, userParam);
			 * parameters.put(ParamConstants.SYMBOLCODES_TAG, symbolCodes);
			 * 
			 * String response = null; 
			 * try { 
			 * 		response = QcHttpConnectUtil.imageQuote(parameters,
			 * 		CommandConstants.IMAGE_QUOTE_DETAIL);
			 */

			String response = null;
			try {
				response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
				}
				intRetryCtr = 0;

				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START 
			 * }catch (FailedAuthenicationException e) { 
			 * 		updateTask.cancel(true); 
			 * 		new stockRefresh().execute(); e.printStackTrace(); 
			 * }
			 */
			} catch (Exception e) {
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			if(response != null)
				stockInfo = QcMessageParser.parseStockDetail(response);

			return symbolCodes;
		}
		
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - START
		@Override
		 protected void onProgressUpdate(Void... params) {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( ! StockActivity.this.dialog.isShowing() ){
					StockActivity.this.dialog.setMessage(getResources().getString(R.string.refreshing_stock_details));
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() ){
						StockActivity.this.dialog.show();
						isDialogOwner	= true;
					}
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - END

		@Override
		protected void onPostExecute(String[] symbolCodes) {
			super.onPostExecute(symbolCodes);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				/* Mary@20120920 - Fixes_Request-20120815, ReqNo.13
				if( dialog.isShowing() )
					dialog.dismiss();
				*/
	
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if (exception != null) {
					//  Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - START
					if( StockActivity.this.dialog != null && StockActivity.this.dialog.isShowing() && isDialogOwner){
						StockActivity.this.dialog.dismiss();
						isDialogOwner = false;
					}
					//  Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - END
					
					alertException.setMessage(exception.getMessage());
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							finish();
						}
					});
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() )
						alertException.show();
					
					exception = null;
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12 - START
				/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
				String strQCExhgCode	= ( stockInfo.getSymbolCode().split("\\.") )[1];
				*/
				String strQCExhgCode	= stockInfo.getSymbolCode().substring( (stockInfo.getSymbolCode().lastIndexOf(".") + 1), stockInfo.getSymbolCode().length() );
				String strTrdExchgCode	= null;
				for(int i=0; i< DefinitionConstants.getValidQCExchangeCodes().size(); i++){
					String strCurrQCExchgCode = DefinitionConstants.getValidQCExchangeCodes().get(i);
					if( strQCExhgCode.equals(strCurrQCExchgCode) ){
						strTrdExchgCode	= DefinitionConstants.getTrdExchangeCode().get(i);
						break;
					}
				}
				// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12 - END
				// Added by Mary@20121114 - Fixes_Request-20120719, ReqNo.12
				AppConstants.updateTradePreferenceSharedPreference(StockActivity.this, sharedPreferences, strTrdExchgCode);
	
				prepareStockView();
	
				refreshThread = ((StockApplication) StockActivity.this.getApplication()).getRefreshThread();
				if (refreshThread == null) {
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
					if(AppConstants.hasLogout){
						finish();
					}else{
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
						int rate		= sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
						/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
						 * refreshThread = new RefreshStockThread(userParam, symbolCodes, rate);
						 */
						refreshThread 	= new RefreshStockThread(AppConstants.QCdata.getStrUserParam(), symbolCodes, rate);
						refreshThread.start();
						((StockApplication) StockActivity.this.getApplication()).setRefreshThread(refreshThread);
					}
				/* Mary@20130924 - Fixes_Request-20130711, ReqNo.22
				} else {
				*/
				/* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
				}else if( refreshThread.isPaused() ){
					refreshThread.resumeRequest();
					refreshThread.setSymbolCodes(symbolCodes);
				*/
				}
				refreshThread.setRegisteredListener(StockActivity.this);
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29
				refreshThread.setSymbolCodes(symbolCodes);
				
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
				 if( refreshThread.isPaused() ){
					 /* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
					 refreshThread.setSymbolCodes(symbolCodes);
					 */
					 refreshThread.resumeRequest();
				 }
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END
	
				loginThread = ((StockApplication) StockActivity.this.getApplication()).getDoubleLoginThread();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
				if(loginThread == null){
					if(AppConstants.hasLogout){
						finish();
					}else{
						loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
						if (AppConstants.brokerConfigBean.isEnableEncryption())
							loginThread.setUserParam(AppConstants.userParamInByte);
						loginThread.start();
						((StockApplication) StockActivity.this.getApplication()).setDoubleLoginThread(loginThread);
					}
				}
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
				loginThread.setRegisteredListener(StockActivity.this);
				
				//Change_Request-20160722, ReqNo.2
				prepareTheScreenerButtons(StockActivity.this.stockInfo);
	
				adapter 	= new MyPagerAdapter();
				myPager 	= (ViewPager) findViewById(R.id.mypager);
				myPager.setAdapter(adapter);
				myPager.setCurrentItem(currentPage, true);
				myPager.setOnPageChangeListener(new OnPageChangeListener() {
					@Override
					public void onPageScrollStateChanged(int status) {
						// TODO Auto-generated method stub
					}
	
					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						// TODO Auto-generated method stub
					}
	
					@Override
					public void onPageSelected(int position) {
						System.out.println("debug: onpageSelected, position=" + position);
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
						boolean isValidSelection = false;
						
						
						/* Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - START
						 * if(position == 0){ 
						 * 		myPager.setCurrentItem(position + 1,true); 
						 * }else if(position==1){
						 * 		imgPage.setImageResource(isShowFundamentalCapitalICQ ? LayoutSettings.paging1); orientationListener.disable();
						 * 		isMarketDepthShow = false; 
						 * }else if(position==2){
						 * 		imgPage.setImageResource(LayoutSettings.paging2);
						 * 		orientationListener.disable(); 
						 * 		isMarketDepthShow = true;
						 * }else if(position==3){
						 * 		imgPage.setImageResource(LayoutSettings.paging3);
						 * 		orientationListener.disable(); 
						 * 		isMarketDepthShow = false;
						 * }else if(position==4){
						 * 		imgPage.setImageResource(LayoutSettings.paging4);
						 * 		isMarketDepthShow = false;
						 * 		orientationListener.enable();
						 * }else if(position == 5){ 
						 * 		myPager.setCurrentItem(position - 1,true); 
						 * }
						 */
	
						/* Mary@20121001 - FixesRequest-20120815, ReqNo.3 - START
						if (position == 0) {
							myPager.setCurrentItem(position + 1, true);
						} else if (position == 1) {
							imgPage.setImageResource(AppConstants.isShowFundamentalCapitalICQ ? LayoutSettings.paging1of5 : LayoutSettings.paging1of4);
							orientationListener.disable();
							isMarketDepthShow = false;
						} else if (position == 2) {
							imgPage.setImageResource(AppConstants.isShowFundamentalCapitalICQ ? LayoutSettings.paging2of5
									: LayoutSettings.paging2of4);
							orientationListener.disable();
							isMarketDepthShow = true;
						} else if (position == 3) {
							imgPage.setImageResource(AppConstants.isShowFundamentalCapitalICQ ? LayoutSettings.paging3of5
									: LayoutSettings.paging3of4);
							orientationListener.disable();
							isMarketDepthShow = false;
						} else if (position == 4) {
							imgPage.setImageResource(AppConstants.isShowFundamentalCapitalICQ ? LayoutSettings.paging4of5
									: LayoutSettings.paging4of4);
							isMarketDepthShow = false;
							orientationListener.enable();
						} else if (position == 5) {
							if (AppConstants.isShowFundamentalCapitalICQ) {
								imgPage.setImageResource(LayoutSettings.paging5of5);
								isMarketDepthShow = false;
								orientationListener.disable();
							} else {
								myPager.setCurrentItem(position - 1, true);
							}
						} else if (position == 6
								&& AppConstants.isShowFundamentalCapitalICQ) {
							myPager.setCurrentItem(position - 1, true);
						}
						// Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - END
						*/
						
						switch(position){
						case 0 :
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
							isValidSelection = true;
							
							myPager.setCurrentItem(position + 1, true);
							break;
						case INT_STOCK_DETAILS_PAGE_POS :
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
							isValidSelection = true;
							/* Mary@20130620 - Change_Request-20130424, ReqNo.9
							imgPage.setImageResource(AppConstants.isShowFundamentalCapitalICQ ? LayoutSettings.paging1of5 : LayoutSettings.paging1of4);
							*/
							// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5
							//imgPage.setImageResource(isShowFundamentalCapitalIQ ? LayoutSettings.paging1of7 : LayoutSettings.paging1of6);
							setPagerDot(pagerDot1, INT_STOCK_DETAILS_PAGE_POS);	//commented above and added by Thinzar, Change_Request-20160101, ReqNo.3
							
							orientationListener.disable();
							isMarketDepthShow = false;
							break;
						case INT_MARKET_DEPTH_PAGE_POS :
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
							isValidSelection = true;
							
							/* Mary@20130620 - Change_Request-20130424, ReqNo.9
							imgPage.setImageResource(AppConstants.isShowFundamentalCapitalICQ ? LayoutSettings.paging2of5 : LayoutSettings.paging2of4);
							*/
							// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5
							//imgPage.setImageResource(isShowFundamentalCapitalIQ ? LayoutSettings.paging2of7 : LayoutSettings.paging2of6);
							setPagerDot(pagerDot2, INT_MARKET_DEPTH_PAGE_POS);	//commented above and added by Thinzar, Change_Request-20160101, ReqNo.3
							
							orientationListener.disable();
							isMarketDepthShow = true;
							break;
						case INT_STOCK_CHART_PAGE_POS :
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
							isValidSelection = true;
							
							/* Mary@20130620 - Change_Request-20130424, ReqNo.9
							imgPage.setImageResource(AppConstants.isShowFundamentalCapitalICQ ? LayoutSettings.paging4of5 : LayoutSettings.paging4of4);
							*/
							// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5
							//imgPage.setImageResource(isShowFundamentalCapitalIQ ? LayoutSettings.paging4of7 : LayoutSettings.paging4of6);
							setPagerDot(pagerDot3, INT_STOCK_CHART_PAGE_POS);	//commented above and added by Thinzar, Change_Request-20160101, ReqNo.3
							
							orientationListener.enable();
							isMarketDepthShow = false;
							break;
						
						case INT_TIME_SALES_PAGE_POS :
							// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - START
							isValidSelection = true;
							
							//imgPage.setImageResource(isShowFundamentalCapitalIQ ? LayoutSettings.paging5of7 : LayoutSettings.paging5of6);
							setPagerDot(pagerDot4, INT_TIME_SALES_PAGE_POS);	//commented above and added by Thinzar, Change_Request-20160101, ReqNo.3
							
							orientationListener.disable();
							
							if (newTransList.size() == 0){
								linearProgress	= (LinearLayout) mView.get(position).findViewById(R.id.linearProgressBar);
								linearProgress.setVisibility(View.VISIBLE);
								new TimeSalesTask(RecordAge.NEW).execute(mView.get(position));
							} 
							//Added by Thinzar, Fixes_Request-20150401, ReqNo. 6 - START
							else if(timeSalesViewPref != AppConstants.brokerConfigBean.getIntViewQtyMeasurement()){
								new TimeSalesTask(RecordAge.NEW).execute(mView.get(position));
							}
							//Added by Thinzar, Fixes_Request-20150401, ReqNo. 6 - END
							
							break;
						case INT_BUSINESS_DONE_PAGE_POS :
							// Sonia@20131001 - Change_Request-20130225, ReqNo.5 - START
							isValidSelection = true;
							
							//imgPage.setImageResource(isShowFundamentalCapitalIQ ? LayoutSettings.paging6of7 : LayoutSettings.paging6of6);
							setPagerDot(pagerDot5, INT_BUSINESS_DONE_PAGE_POS);	//commented above and added by Thinzar, Change_Request-20160101, ReqNo.3
							
							orientationListener.disable();
							isMarketDepthShow = false;
							
							//Added by Thinzar, Fixes_Request-20150401, ReqNo. 6 - START
							if(businessDoneViewPref != AppConstants.brokerConfigBean.getIntViewQtyMeasurement()){
								new BusinessDoneTask().execute( mView.get(position) );
							}
							//Added by Thinzar, Fixes_Request-20150401, ReqNo. 6 - END
							
							break;
							// Sonia@20131001 - Change_Request-20130225, ReqNo.5 - END
						case INT_STOCK_NEWS_PAGE_POS :
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
							isValidSelection = true;
							
							/* Mary@20130620 - Change_Request-20130424, ReqNo.9
							imgPage.setImageResource(AppConstants.isShowFundamentalCapitalICQ ? LayoutSettings.paging3of5 : LayoutSettings.paging3of4);
							*/
							// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5
							//imgPage.setImageResource(isShowFundamentalCapitalIQ ? LayoutSettings.paging3of7 : LayoutSettings.paging3of6);
							setPagerDot(pagerDot6, INT_STOCK_NEWS_PAGE_POS);	//commented above and added by Thinzar, Change_Request-20160101, ReqNo.3
							
							orientationListener.disable();
							isMarketDepthShow = false;
							break;
						case INT_FUNDAMENTAL_PAGE_POS :
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
							isValidSelection = true;
							
							/* Mary@20130620 - Change_Request-20130424, ReqNo.9
							if (AppConstants.isShowFundamentalCapitalICQ) {
							*/
							if (isShowFundamentalCapitalIQ) {
								// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5
								//imgPage.setImageResource(LayoutSettings.paging7of7);
								setPagerDot(pagerDot7, INT_FUNDAMENTAL_PAGE_POS);	//commented above and added by Thinzar, Change_Request-20160101, ReqNo.3
								
								orientationListener.disable();
								isMarketDepthShow = false;
							}else {
								myPager.setCurrentItem(position - 1, true);
								break;
							}
							break;
						case 8 :
							// Added by Sonia@20130702 - ChangeRequest-20130225, ReqNo.5 - END
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
							isValidSelection = true;
							/* Mary@20130620 - Change_Request-20130424, ReqNo.9
							if(AppConstants.isShowFundamentalCapitalICQ)
							*/
							if(isShowFundamentalCapitalIQ)
								myPager.setCurrentItem(position - 1, true);
							break;
					}
					// Mary@20121001 - FixesRequest-20120815, ReqNo.3 - END
					
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(isValidSelection && idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				}
			});
			
			//  Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - START
			if( StockActivity.this.dialog.isShowing() && isDialogOwner){
				StockActivity.this.dialog.dismiss();
				isDialogOwner = false;
			}
			// Added by Mary@20120920 - Fixes_Request-20120815, ReqNo.13 - END
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.2 - START
	private void prepareTheScreenerButtons(final StockInfo stkInfo){
		
		if(AppConstants.brokerConfigBean.isEnableScreener()){
			
			imgScreenerStarRating.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					
					startTheScreenerActivity(stkInfo);
				}
				
			});
			
			imgScreenerRiskRating.setOnClickListener(new OnClickListener(){
				
				@Override
				public void onClick(View v) {
					
					startTheScreenerActivity(stkInfo);
				}
				
			});
		}
	}
	
	private void startTheScreenerActivity(final StockInfo stkInfo){
		String exchgFromSymbol	= symbolCode.substring(symbolCode.lastIndexOf(".") + 1, symbolCode.length());
		String trdExchgCode	= SystemUtil.getTrdExchgCode(exchgFromSymbol);
		
		Intent intent	= new Intent(StockActivity.this, TheScreenerActivity.class);
		intent.putExtra(ParamConstants.INFO_TYPE_TAG, ParamConstants.SCREENER_TYPE_INDIVIDUAL);
		intent.putExtra(ParamConstants.EXCHANGECODE_TAG, trdExchgCode);
		intent.putExtra(ParamConstants.SYMBOLCODE_TAG, stkInfo.getSymbolCode());
		intent.putExtra(ParamConstants.STOCK_CODE_TAG, stkInfo.getStockCode());
		intent.putExtra(ParamConstants.LOT_SIZE_TAG, stkInfo.getIntLotSize());
		intent.putExtra(ParamConstants.INSTRUMENT_TAG, stkInfo.getInstrument());
		intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stkInfo.isPayableWithCPF());
		
		startActivity(intent);
	}
	//Added by Thinzar, Change_Request-20160722, ReqNo.2 - END
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.3
	private void setPagerDot(ImageView imgPagerDot, int currentPagePos){
		//reset all dots to white first
		pagerDot1.setImageResource(R.drawable.bottom_bar_red_dot);
		pagerDot2.setImageResource(R.drawable.bottom_bar_red_dot);
		pagerDot3.setImageResource(R.drawable.bottom_bar_red_dot);
		pagerDot4.setImageResource(R.drawable.bottom_bar_red_dot);
		pagerDot5.setImageResource(R.drawable.bottom_bar_red_dot);
		pagerDot6.setImageResource(R.drawable.bottom_bar_red_dot);
		imgStkRefresh.setVisibility(View.GONE);
		imgOrientation.setVisibility(View.GONE);
		
		if(isShowFundamentalCapitalIQ){
			pagerDot7.setVisibility(View.VISIBLE);
			pagerDot7.setImageResource(R.drawable.bottom_bar_red_dot);
		}
		
		//set red dot
		imgPagerDot.setImageResource(R.drawable.bottom_bar_white_dot);
		
		//set refresh button for business done and news page
		if(currentPagePos == this.INT_BUSINESS_DONE_PAGE_POS){
			imgStkRefresh.setVisibility(View.VISIBLE);
			imgStkRefresh.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					new BusinessDoneTask().execute(mView.get(INT_BUSINESS_DONE_PAGE_POS));
				}
			});
		} else if(currentPagePos == this.INT_STOCK_NEWS_PAGE_POS){
			imgStkRefresh.setVisibility(View.VISIBLE);
			imgStkRefresh.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					if(AppConstants.brokerConfigBean.isArchiveNews()==true){
						String[] parts = symbolCode.split("\\.");
						code = parts[0]; // code
						
						new GetArchiveNews().execute(mView.get(INT_STOCK_NEWS_PAGE_POS));
			
					}else{
						new StockNewsTask().execute(mView.get(INT_STOCK_NEWS_PAGE_POS));
					
					}
				}
			});
		}else if(currentPagePos == this.INT_STOCK_CHART_PAGE_POS){
			imgOrientation.setVisibility(View.VISIBLE);
		}
	}

	// Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - START
	private class FundamentalCapitalMenuTask extends AsyncTask<View, Void, View> {
		@Override
		protected View doInBackground(View... view) {
			return view[0];
		}

		@Override
		protected void onPostExecute(View view) {
			super.onPostExecute(view);
			
			//Edited by Thinzar, Change_Request-20170119, ReqNo.3
			if(AppConstants.brokerConfigBean.getNewFundamentalSourceCode().length() > 0){
				StockActivity.this.prepareFundamentalCapitalMenuView2(view);
			} else{
				StockActivity.this.prepareFundamentalCapitalMenuView(view);
			}
		}
	}
	
	//Change_Request-20170119, ReqNo.3 - START
	private void prepareFundamentalCapitalMenuView2(View view) {
		/*  
		 √ 0 = Company Info
		 √ 1 = Company Key Person
		 √ 2 = Shareholding Summary
		 √ 3 = Announcement
		 √ 4 = Financial Report
		 X 5 = Estimates
		 X 6 = Supply Chain
		 X 7 = Market Activity
		 *
		 * ThompsonReuters only have 0, 1, 4
		 */
		
		List<ImageButton> lstButton			= new ArrayList<ImageButton>();
		lstButton.add( (ImageButton) view.findViewById(R.id.btnSynopsis) );
		lstButton.add( (ImageButton) view.findViewById(R.id.btnCompanyInfo) );
		lstButton.add( (ImageButton) view.findViewById(R.id.btnAnnouncement) );
		lstButton.add( (ImageButton) view.findViewById(R.id.btnKeyPerson) );
		lstButton.add( (ImageButton) view.findViewById(R.id.btnShareholdingSummary) );
		lstButton.add( (ImageButton) view.findViewById(R.id.btnFinancialReport) );
		
		for(int i=0; i<lstButton.size(); i++){
			ImageButton btn	= lstButton.get(i);
			btn.setOnClickListener(NewFCMBtnOnClickListener);
		}
		
		if(AppConstants.brokerConfigBean.getNewFundamentalSourceCode().equals("TRKD")){
			LinearLayout tlFundamentalCapitalIQMenuRow2	= (LinearLayout)view.findViewById(R.id.tlFundamentalCapitalIQMenuRow2);
			LinearLayout captionLayout2	= (LinearLayout)view.findViewById(R.id.tl2);
			
			tlFundamentalCapitalIQMenuRow2.setVisibility(View.GONE);
			captionLayout2.setVisibility(View.GONE);
		}
		
		txtFundamentalNewsLabel		= (TextView)view.findViewById(R.id.txtFundamentalNewsLabel); 	//Fixes_Request-20170701, ReqNo.9
		txtFundamentalNewsLabel.setText(AppConstants.brokerConfigBean.getNewFundamentalNewsLabel());
	}
	//Change_Request-20170119, ReqNo.3 - END

	private void prepareFundamentalCapitalMenuView(View view) {
		List<ImageButton> lstButton			= new ArrayList<ImageButton>();
		List<TextView> lstTextView		= new ArrayList<TextView>();
		try{
			if(view == null)
				return;
			
			lstButton.add( (ImageButton) view.findViewById(R.id.btnSynopsis) );
			lstButton.add( (ImageButton) view.findViewById(R.id.btnCompanyInfo) );
			lstButton.add( (ImageButton) view.findViewById(R.id.btnAnnouncement) );
			lstButton.add( (ImageButton) view.findViewById(R.id.btnKeyPerson) );
			lstButton.add( (ImageButton) view.findViewById(R.id.btnShareholdingSummary) );
			lstButton.add( (ImageButton) view.findViewById(R.id.btnFinancialReport) );
			
			lstTextView.add( (TextView) view.findViewById(R.id.tvSynopsis) );
			lstTextView.add( (TextView) view.findViewById(R.id.tvCompInfo) );
			lstTextView.add( (TextView) view.findViewById(R.id.tvAnnouncement) );
			lstTextView.add( (TextView) view.findViewById(R.id.tvKeyPerson) );
			lstTextView.add( (TextView) view.findViewById(R.id.tvShareHolding) );
			lstTextView.add( (TextView) view.findViewById(R.id.tvFinancialReport) );
			
			String strFCBitCode_Base64 	= AppConstants.mapFCBitMode.get(splitQCExchgCode);
			
			for(int i=0; i<lstButton.size(); i++){
				ImageButton btn	= lstButton.get(i);
				TextView tv	= lstTextView.get(i);
				
				if( ! SystemUtil.isBitOn_Base64(strFCBitCode_Base64, i) ){
					btn.setEnabled(false);
					btn.getBackground().setAlpha(80);
					tv.setTextColor( tv.getTextColors().withAlpha(80) );
				}else{
					btn.setOnClickListener(FCMBtnOnClickListener);
				}
			}
			// Added by Mary@20130620 - Change_Request-20130424, ReqNo.9 - END
			
			//Added by Thinzar@20150526, Change_Request-20150401, ReqNo. 3
			txtFundamentalNewsLabel		= (TextView)findViewById(R.id.txtFundamentalNewsLabel);
			txtFundamentalNewsLabel.setText(AppConstants.brokerConfigBean.getFundamentalNewsLabel());
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			lstButton.clear();
			lstButton = null;
			
			lstTextView.clear();
			lstTextView = null;
		}
	}

	private class FCMBtnOnClickAsyncTask extends AsyncTask<String, Void, Boolean> {
		private Context context;
		private AsyncTask updateTask= null;
		private View view 			= null;
		private Intent intent 		= new Intent();
		private int intID;
		public FCMBtnOnClickAsyncTask(StockActivity activity, View view) {
			this.context= activity;
			this.view	= view;
		}

		@Override
		protected void onPreExecute() {
			this.updateTask = this;
			intID = this.view.getId();
		}

		@Override
		protected Boolean doInBackground(String... args) {

			intent.putExtra(ParamConstants.PARAM_FC_SELECTED_VIEW, ParamConstants.PARAM_INT_VIEW_FC_FUNDAMENTAL_INFO);
			
			switch (intID) {
				case R.id.btnAnnouncement:
					intent.putExtra(ParamConstants.PARAM_FC_CATEGORY, ParamConstants.PARAM_STR_FCIQ_FI_ANNOUNCEMENT);
					break;
				case R.id.btnCompanyInfo:
					intent.putExtra(ParamConstants.PARAM_FC_CATEGORY, ParamConstants.PARAM_STR_FC_FI_COMPANY_INFO);
					break;
				case R.id.btnFinancialReport:
					intent.putExtra(ParamConstants.PARAM_FC_SELECTED_VIEW, ParamConstants.PARAM_INT_VIEW_FC_FINANCIAL_REPORT);
					intent.putExtra(ParamConstants.PARAM_FC_REPORT_TYPE, AppConstants.strDefaultReportType);
					//intent.putExtra(ParamConstants.PARAM_FC_REPORT_PERIOD, AppConstants.strDefaultReportPeriod);
					intent.putExtra(ParamConstants.PARAM_FC_REPORT_PERIOD, AppConstants.brokerConfigBean.getFundamentalDefaultReportCode());	//edited by Thinzar, Change_Request-20150401, ReqNo. 3
					break;
				case R.id.btnKeyPerson:
					intent.putExtra(ParamConstants.PARAM_FC_CATEGORY, ParamConstants.PARAM_STR_FC_FI_KEY_PERSON);
					break;
				case R.id.btnShareholdingSummary:
					intent.putExtra(ParamConstants.PARAM_FC_CATEGORY, ParamConstants.PARAM_STR_FC_FI_SHAREHOLDER);
					break;
				case R.id.btnSynopsis:
					intent.putExtra(ParamConstants.PARAM_FC_CATEGORY, ParamConstants.PARAM_STR_FC_FI_SYNOPSIS);
					break;
			}

			return true;
		}

		@Override
		protected void onPostExecute(final Boolean isSuccess) {
			if (isSuccess) {
				StockApplication application	= (StockApplication) StockActivity.this.getApplication();
				
				//Edited by Thinzar, got error for delayed exchange if use QCExchangeCode 
				//intent.putExtra(ParamConstants.EXCHANGECODE_TAG, application.getSelectedExchange().getQCExchangeCode());
				intent.putExtra(ParamConstants.EXCHANGECODE_TAG, application.getSelectedExchange().getTrdExchgCode());
				
				intent.putExtra(ParamConstants.SYMBOLCODE_TAG, StockActivity.this.stockInfo.getSymbolCode());
				intent.setClass(StockActivity.this, FundamentalCapitalIQActivity.class);
				
				//Edited by Thinzar, Change_Request-20150526, ReqNo. 3, commented out previous defaultSourceCode, gete from config file
				//intent.putExtra(ParamConstants.PARAM_FC_SOURCE_CODE, AppConstants.strDefaultSourceCode);
				intent.putExtra(ParamConstants.PARAM_FC_SOURCE_CODE, AppConstants.brokerConfigBean.getFundamentalSourceCode());
				context.startActivity(intent);
			}
		}
	}

	// Mary@20120723 - ChangeRequest-20120719, ReqNo.1 - END

	private void prepareStockView() {
		strSymbolExchg = stockInfo.getSymbolCode().substring( (stockInfo.getSymbolCode().lastIndexOf(".") + 1), stockInfo.getSymbolCode().length() );

		//Added by Thinzar@20141112, Change_Request-20141101, ReqNo. 5, 8 - START (to replace the above block of code)
		openLabel.setText(FormatUtil.formatPrice(stockInfo.getOpen(), "-", strSymbolExchg));
		previousLabel.setText(FormatUtil.formatPrice(stockInfo.getPreviousClose(), "-", strSymbolExchg));
		highLabel.setText(FormatUtil.formatPrice(stockInfo.getHigh(), "-", strSymbolExchg));
		lowLabel.setText(FormatUtil.formatPrice(stockInfo.getLow(), "-", strSymbolExchg));
		lacpLabel.setText(FormatUtil.formatPrice(stockInfo.getLACP(), "-", strSymbolExchg));
		//Added by Thinzar@20141112, Change_Request-20141101, ReqNo. 5, 8 - END

		//Change_Request-20170119, ReqNo.12
		if(AppConstants.brokerConfigBean.isShowTopAtLactp()){
			lacpLayout.setVisibility(View.GONE);
			topPriceLayout.setVisibility(View.VISIBLE);
			topLabel.setText(FormatUtil.formatPrice(stockInfo.getTheorecticalPrice(), "-", strSymbolExchg));
		}
		
		if(stockInfo.getVolume() != 0){
			/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
			volumeLabel.setText(FormatUtil.formatLong(stockInfo.getVolume()));
			*/	
			
			
			/*KW@20121205 fix request */
			int intLotSize = AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? stockInfo.getSharePerLot() : 1;
			/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
			volumeLabel.setText(FormatUtil.formatLong(stockInfo.getVolume()/intLotSize, "-"));
			*/
			volumeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (stockInfo.getVolume()/intLotSize) , "-") );
		}else{
			volumeLabel.setText("-");
		}
		
		/*if(stockInfo.getValue() != 0){
			if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
				valueLabel.setText( FormatUtil.formatDouble2(stockInfo.getValue(), "-") );
			else
				valueLabel.setText( FormatUtil.formatDouble(stockInfo.getValue(), "-") );
		}else{
			if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
				valueLabel.setText("0");
			else
				valueLabel.setText("0.000");
		}
		*/
		//Commented out the above block and add the following by Thinzar, Change_Request-20141101, ReqNo 8
		valueLabel.setText(FormatUtil.formatDoubleByExchange(stockInfo.getValue(), "-", strSymbolExchg)); 

		if(width <= 400 && height <= 800){
			/* Mary@20130718 - Change_Request-20130424, ReqNo.13
			if(stockInfo.getStockName().length() >= 16)
			*/
			if(stockInfo.getStockName(true).length() >= 16)
				nameLabel.setTextSize(15);
		}
		/* Mary@20130718 - Change_Request-20130424, ReqNo.13
		nameLabel.setText(stockInfo.getStockName());
		*/
		nameLabel.setText( stockInfo.getStockName(true) );
		companyLabel.setText(stockInfo.getCompanyName());

		if(stockInfo.getPriceChange() > 0)
			arrowImage.setImageResource(LayoutSettings.upArrow);
		else if(stockInfo.getPriceChange() < 0)
			arrowImage.setImageResource(LayoutSettings.downArrow);
		else
			arrowImage.setImageDrawable(null);
		
		/* Commented out the whole block and replaced by the block below by Thinzar, Change_Request-20141101, ReqNo.8
		// Mary@20130626 - Fixes_Request-20130523, ReqNo.20
		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
			// Mary@20121001 - Fixes_Request-20120815, ReqNo.15
			lastDoneLabel.setText( FormatUtil.formatPrice2( stockInfo.getLastDone(), "-" ) );
			changeLabel.setText( FormatUtil.formatPriceChange2( stockInfo.getPriceChange() ) );
		}else{
			// Mary@20121001 - Fixes_Request-20120815, ReqNo.15
			lastDoneLabel.setText( FormatUtil.formatPrice( stockInfo.getLastDone(), "-" ) );
			changeLabel.setText( FormatUtil.formatPriceChange( stockInfo.getPriceChange() ) );
		}
		*/
		//Added by Thinzar@20141112, Change_Request-20141101, ReqNo. 8 (to replace the above block of code)
		lastDoneLabel.setText(FormatUtil.formatPrice(stockInfo.getLastDone(), "-", strSymbolExchg));
		changeLabel.setText(FormatUtil.formatPriceChangeByExchange(stockInfo.getPriceChange(), strSymbolExchg));
		
		changePercentLabel.setText( FormatUtil.formatPricePercent( stockInfo.getChangePercent() ) );

		int intStatusColor = getResources().getColor(R.color.NoStatus_FgColor);
		if (stockInfo.getPriceChange() > 0)
			intStatusColor = getResources().getColor(R.color.UpStatus_BgColor);
		else if (stockInfo.getPriceChange() < 0)
			intStatusColor = getResources().getColor(R.color.DownStatus_BgColor);

		lastDoneLabel.setTextColor(intStatusColor);
		changeLabel.setTextColor(intStatusColor);
		changePercentLabel.setTextColor(intStatusColor);
		// Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status - END
		
		//Added by Thinzar, Change_Request-20160722, ReqNo.2 - START
		if(AppConstants.brokerConfigBean.isEnableScreener()){
			screenerLayout.setVisibility(View.VISIBLE);
			
			int starRating  = stockInfo.getTheScreenerStarRating();
			int riskLevel	= stockInfo.getTheScreenerRiskLevel();
			boolean hasStarRating	= true;
			boolean hasRiskLevel	= true;
			
			System.out.println("screener: " + stockInfo.getStockCode() + ", star=" + stockInfo.getTheScreenerStarRating() + ", risk=" + stockInfo.getTheScreenerRiskLevel());
			
			switch(starRating){
				case 0:
					imgScreenerStarRating.setImageResource(R.drawable.star0);
					break;
				case 1:
					imgScreenerStarRating.setImageResource(R.drawable.star1);
					break;
				case 2:
					imgScreenerStarRating.setImageResource(R.drawable.star2);
					break;
				case 3:
					imgScreenerStarRating.setImageResource(R.drawable.star3);
					break;
				case 4:
					imgScreenerStarRating.setImageResource(R.drawable.star4);
					break;
				default:
					hasStarRating	= false;
					imgScreenerStarRating.setVisibility(View.GONE);
					break;	
			}
			
			switch(riskLevel){
				case -1:
					imgScreenerRiskRating.setImageResource(R.drawable.risk_high);
					break;
				case 0:
					imgScreenerRiskRating.setImageResource(R.drawable.risk_medium);
					break;
				case 1:
					imgScreenerRiskRating.setImageResource(R.drawable.risk_low);
					break;
				default:
					hasRiskLevel	= false;
					imgScreenerRiskRating.setVisibility(View.GONE);
					break;
			} 
			
			if(!hasStarRating && !hasRiskLevel){
				screenerLayout.setVisibility(View.GONE);
				topRightBlankLayout.setVisibility(View.VISIBLE);
			}
		}
		//Added by Thinzar, Change_Request-20160722, ReqNo.2 - END
		
		//Added by Thinzar, Change_Request-20160722, ReqNo.8 - START
		if(AppConstants.brokerConfigBean.isEnableIBillionaire){
			String mStockSymbol		= stockInfo.getSymbolCode();
			String[] arrStockSymbol	= mStockSymbol.split("\\.");
			String mSymbolCode		= arrStockSymbol[0];
			String mExchgCode		= arrStockSymbol[1];
			mExchgCode				= ( mExchgCode.length() > 1 && mExchgCode.toUpperCase().endsWith("D") ) ? mExchgCode.substring(0, mExchgCode.length() - 1) : mExchgCode;
			
			if(AppConstants.brokerConfigBean.getArrExchgIBillionaire().contains(mExchgCode)){
				iBillionaireLayout.setVisibility(View.VISIBLE);
				topRightBlankLayout.setVisibility(View.GONE);
				
				symbolForIBillionaire	= mSymbolCode + ":" + mExchgCode;
				final String iBillionairePkgName	= AppConstants.iBillionairePkgNameMap.get(getApplicationContext().getPackageName());
				
				imgIBillionaire.setOnClickListener(new OnClickListener(){
	
					@Override
					public void onClick(View v) {
						DefinitionConstants.Debug("StockActivity iBillionaire redirecting for stock: " + symbolForIBillionaire);
						//Change_Request-20170601, ReqNo.18 - START
						if(AppConstants.brokerConfigBean.isLinkIBWebview()){
							String exchgFromSymbol	= symbolCode.substring(symbolCode.lastIndexOf(".") + 1, symbolCode.length());
							String trdExchgCode	= SystemUtil.getTrdExchgCode(exchgFromSymbol);

							Intent iBIntent = new Intent();
							iBIntent.setClass(StockActivity.this, IBillionaireActivity.class);
							iBIntent.putExtra(ParamConstants.INFO_TYPE_TAG, ParamConstants.IB_TYPE_INDIVIDUAL);
							iBIntent.putExtra(ParamConstants.EXCHANGECODE_TAG, trdExchgCode);
							iBIntent.putExtra(ParamConstants.SYMBOLCODE_TAG, stockInfo.getSymbolCode());
							iBIntent.putExtra(ParamConstants.STOCK_CODE_TAG, stockInfo.getStockCode());
							iBIntent.putExtra(ParamConstants.LOT_SIZE_TAG, stockInfo.getIntLotSize());
							iBIntent.putExtra(ParamConstants.INSTRUMENT_TAG, stockInfo.getInstrument());
							iBIntent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stockInfo.isPayableWithCPF());

							startActivity(iBIntent);
						}
						//Change_Request-20170601, ReqNo.18 - END
						else {
							try {
								StockActivity.this.getPackageManager().getApplicationInfo(iBillionairePkgName, 0);

								showDialogWarnOpenExternalApp(symbolForIBillionaire);

							} catch (Exception e) {
								showDialogToInstallApp(iBillionairePkgName);

							}
						}
					}
				});
			} else {
				hideIBillionaireLayoutAndAdjustTopSpace();	
			}
			
		}else{
			hideIBillionaireLayoutAndAdjustTopSpace();
		}		
	}
	
	private void hideIBillionaireLayoutAndAdjustTopSpace(){
		iBillionaireLayout.setVisibility(View.GONE);
		
		if(!AppConstants.brokerConfigBean.isEnableScreener()){
			topRightBlankLayout.setVisibility(View.VISIBLE);
		}
	}
	
	private void showDialogWarnOpenExternalApp(final String mSymbolCode){
		if(!AppConstants.hasWarnOpenExternalApp){
			AlertDialog.Builder builder = new AlertDialog.Builder(StockActivity.this);
	        builder.setMessage(getResources().getString(R.string.open_external_app_warning));
	        builder.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
	                new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int id) {
	                    	AppConstants.hasWarnOpenExternalApp	= true;
	                    	
	                    	startIBillionaireIntent(mSymbolCode);
	                    }
	                });
	        builder.setNegativeButton("Cancel",
	                new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int id) {
	                        dialog.cancel();
	                    }
	                });
	        
	        builder.show();
		}else{
			startIBillionaireIntent(mSymbolCode);
		}
	}
	
	private void startIBillionaireIntent(final String mSymbolCode){
		final String iBillionaireAppLink	= AppConstants.iBillionaireAppLinkMap.get(getApplicationContext().getPackageName());
		
		try{
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(iBillionaireAppLink + "://stock/" + mSymbolCode)));
			
		}catch(Exception e){
			e.printStackTrace();
			Toast.makeText(StockActivity.this, getResources().getString(R.string.feature_not_available) + iBillionaireAppLink, Toast.LENGTH_LONG).show();
		}
	}
	
	private void showDialogToInstallApp(final String packageName){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(StockActivity.this);
        builder.setTitle(R.string.title_install_app);
        builder.setMessage(R.string.app_not_installed);
        builder.setPositiveButton(getResources().getString(R.string.dialog_button_yes),
                new DialogInterface.OnClickListener() {
        	
                    public void onClick(DialogInterface dialog, int id) {
                    	try {
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
						    
						} catch (android.content.ActivityNotFoundException anfe) {
							
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
						    
						}
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.dialog_button_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        
        builder.show();
	}
	//Added by Thinzar, Change_Request-20160722, ReqNo.8 - END

	private void prepareStockDetailView(View view) {
		if(view == null)
			return;

		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
		String[] symbolExchg		= stockInfo.getSymbolCode().split("\\.");
		*/
		String strSymbolExchg		= stockInfo.getSymbolCode().substring( (stockInfo.getSymbolCode().lastIndexOf(".") + 1), stockInfo.getSymbolCode().length() );

		LinearLayout linear 		= (LinearLayout) view.findViewById(R.id.linearStockDetail);

		TextView sectorLabel 		= (TextView) linear.findViewById(R.id.sectorLabel);
		if (stockInfo.getSectorName().length() > 24) {
			sectorLabel.setTextSize(13);
		}
		sectorLabel.setText(stockInfo.getSectorName());
		
		TextView codeLabel 			= (TextView) linear.findViewById(R.id.symbolCodeLabel);
		codeLabel.setText(stockInfo.getSymbolCode());

		TextView stockStatusLabel 	= (TextView) linear.findViewById(R.id.statusLabel);
		stockStatusLabel.setText(stockInfo.getStatus());
		
		TextView stockBVolLabel 	= (TextView) linear.findViewById(R.id.BVolLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		stockBVolLabel.setText(FormatUtil.formatLong(stockInfo.getBuyVolume()));
		*/
		int viewPreference = AppConstants.brokerConfigBean.getIntViewQtyMeasurement();						//Added by Thinzar, Change_Request-20150402, ReqNo.2
		stockBVolLabel.setText( FormatUtil.formatLong(stockInfo.getBuyVolume(viewPreference), "-") );		//Edited by Thinzar, Change_Request-20150402, ReqNo.2
		
		TextView stockSVolLabel 	= (TextView) linear.findViewById(R.id.SVolLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		stockSVolLabel.setText(FormatUtil.formatLong(stockInfo.getSellVolume()));
		*/
		stockSVolLabel.setText( FormatUtil.formatLong(stockInfo.getSellVolume(viewPreference), "-") );		//Edited by Thinzar, Change_Request-20150402, ReqNo.2
		
		TextView stockBTransLabel 	= (TextView) linear.findViewById(R.id.totalBTransLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		stockBTransLabel.setText(FormatUtil.formatLong(stockInfo.getBuyTrans()));
		*/
		stockBTransLabel.setText( FormatUtil.formatLong(stockInfo.getBuyTrans(), "-") );
		
		TextView stockSTransLabel 	= (TextView) linear.findViewById(R.id.totalSTransLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		stockSTransLabel.setText(FormatUtil.formatLong(stockInfo.getSellTrans()));
		*/
		stockSTransLabel.setText( FormatUtil.formatLong(stockInfo.getSellTrans(), "-") );
		
		TextView totalTransLabel 	= (TextView) linear.findViewById(R.id.totalTransLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		totalTransLabel.setText(FormatUtil.formatLong(stockInfo.getTotalTrade()));
		*/
		totalTransLabel.setText( FormatUtil.formatLong(stockInfo.getTotalTrade(), "-") );
		
		TextView marketCapLabel 	= (TextView) linear.findViewById(R.id.marketCapLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		marketCapLabel.setText(FormatUtil.formatLong(new Double(stockInfo.getMarketCap()).longValue(), false));
		*/
		marketCapLabel.setText( FormatUtil.formatLong( Double.valueOf( stockInfo.getMarketCap() ).longValue(), true, "-") );	//edited by Thinzar, Fixes_Request-20150401, ReqNo.4  
		
		TextView shareIssuedLabel 	= (TextView) linear.findViewById(R.id.shareIssueLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		shareIssuedLabel.setText(FormatUtil.formatLong(stockInfo.getShareIssued(), false));
		*/
		shareIssuedLabel.setText( FormatUtil.formatLong(stockInfo.getShareIssued(), true, "-") );		//edited by Thinzar, Fixes_Request-20150401, ReqNo.4

		TextView ceilingLabel		= (TextView) linear.findViewById(R.id.ceilingLabel);
		TextView floorLabel			= (TextView) linear.findViewById(R.id.floorLabel);
		/* Commented out by Thinzar, Change_Request-20141101, ReqNo. 8
		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
			ceilingLabel.setText( FormatUtil.formatPrice2(stockInfo.getCeilingPrice(), "-") );
			floorLabel.setText( FormatUtil.formatPrice2(stockInfo.getFloorPrice(), "-") );
		} else {
			ceilingLabel.setText( FormatUtil.formatPrice(stockInfo.getCeilingPrice(), "-") );
			floorLabel.setText( FormatUtil.formatPrice(stockInfo.getFloorPrice(), "-") );
		}
		*/
		//Added by Thinzar, Change_Request-20141101, ReqNo. 8 (to replace the above block of code
		ceilingLabel.setText( FormatUtil.formatPrice(stockInfo.getCeilingPrice(), "-", strSymbolExchg));
		floorLabel.setText(FormatUtil.formatPrice(stockInfo.getFloorPrice(), "-", strSymbolExchg));
		
		TextView parValueLabel 		= (TextView) linear.findViewById(R.id.parValueLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		parValueLabel.setText(FormatUtil.formatPrice(stockInfo.getParValue()));
		*/
		parValueLabel.setText(FormatUtil.formatPrice(stockInfo.getParValue(), "-", strSymbolExchg));	//edited by Thinzar, cater for Philippines exchange also
		
		TextView lotLabel 			= (TextView) linear.findViewById(R.id.lotSizeLabel);
		lotLabel.setText(FormatUtil.formatPrice2(stockInfo.getSharePerLot(), "-"));
		
		TextView peLabel 			= (TextView) linear.findViewById(R.id.peRatioLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		peLabel.setText(FormatUtil.formatFloat(stockInfo.getPeRatio()));
		*/
		peLabel.setText( FormatUtil.formatFloat(stockInfo.getPeRatio(), "-") );
		
		TextView epsLabel 			= (TextView) linear.findViewById(R.id.epsLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		epsLabel.setText(FormatUtil.formatPrice(stockInfo.getEps()));
		*/
		epsLabel.setText( FormatUtil.formatPrice(stockInfo.getEps(), "-"));
		
		TextView ntaLabel			= (TextView) linear.findViewById(R.id.ntaLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		ntaLabel.setText(FormatUtil.formatPrice(stockInfo.getNta()));
		*/
		ntaLabel.setText( FormatUtil.formatPrice(stockInfo.getNta(), "-") );
		
		//Change_Request-20161101, ReqNo.3
		TextView totalShortSellVolLabel	= (TextView)linear.findViewById(R.id.totalShortSellVolLabel);
		totalShortSellVolLabel.setText(FormatUtil.formatLong(stockInfo.getTotalShortSellVolume(), "-"));
		
		TextView currencyLabel			= (TextView)linear.findViewById(R.id.currencyLabel);
		currencyLabel.setText(stockInfo.getCurrency());
		
		//Added by Thinzar@20140811 - Change_Request-20140801, ReqNo. 1 - START
		if(stockInfo.isCorporateActionNews() == true && hasCorporateActionAlertShown == false){
			
			hasCorporateActionAlertShown = true;
			AlertDialog.Builder builder = new AlertDialog.Builder(StockActivity.this);
            builder.setTitle(R.string.str_corporate_action_msg_title);
            builder.setIcon(android.R.drawable.ic_dialog_info);
            builder.setMessage(R.string.str_corporate_action_msg);
            builder.setPositiveButton(R.string.str_corporate_action_msg_btn1,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        	myPager.setCurrentItem(INT_STOCK_NEWS_PAGE_POS, true);	
                        	setPagerDot(pagerDot6, INT_STOCK_NEWS_PAGE_POS);
                        }
                    });

            builder.setNegativeButton(R.string.str_corporate_action_msg_btn2,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();

                        }
                    });

			if(!StockActivity.this.isFinishing()) //Fixes for crashlytics
            	builder.show();
		}
		//Added by Thinzar@20140811 - END
		
		//Added by Thinzar@20141112, Change_Request-20141101, ReqNo. 6 - START
		TextView txtForeignLimit			= (TextView)linear.findViewById(R.id.foreignLimitLabel);
		TextView txtForeignOwnershipLimit	= (TextView)linear.findViewById(R.id.foreignOwnershipLimitLabel);
		TextView txt52WeekHi				= (TextView)linear.findViewById(R.id.label52WeekHi);
		TextView txt52WeekLo				= (TextView)linear.findViewById(R.id.label52WeekLo);
		TextView txtDynamicHi				= (TextView)linear.findViewById(R.id.labelDynamicHi);
		TextView txtDynamicLo				= (TextView)linear.findViewById(R.id.labelDynamicLo);
		
		txtForeignLimit.setText(stockInfo.getForeignLimit());
		
		if(stockInfoExtra != null){	
			
			txtForeignOwnershipLimit.setText(stockInfoExtra.getForeignOwnershipLimit());
			txt52WeekHi.setText(stockInfoExtra.getPrice52WeekHi());
			txt52WeekLo.setText(stockInfoExtra.getPrice52WeekLo());
			txtDynamicHi.setText(stockInfoExtra.getDynamicHi());
			txtDynamicLo.setText(stockInfoExtra.getDynamicLo());
		}
		//Added by Thinzar@20141112, Change_Request-20141101, ReqNo. 6 - END
	}

	/* Mary@20130612 - Fixes_Request-20130523, ReqNo.9 - START
	private void prepareMarketDepthView(View view) {
		try{
			if(view == null)
				return;
	
			String[] symbolExchg 			= stockInfo.getSymbolCode().split("\\.");
			TableLayout tableLayoutBidAsk	= (TableLayout) view.findViewById(R.id.bidAskTable);
			//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 START
			TableLayout tableLayoutSummary 	= (TableLayout) view.findViewById(R.id.bidAskSummaryTable);
			//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 END
			tableLayoutBidAsk.removeAllViews();
	
			bidSizeLabels 					= new ArrayList<TextView>();
			bidPriceLabels 					= new ArrayList<TextView>();
			askPriceLabels 					= new ArrayList<TextView>();
			askSizeLabels 					= new ArrayList<TextView>();
	
			int id 							= 0;
			long totalBid 					= 0;
			long totalAsk 					= 0;
			double bidSum 					= 0;
			double askSum 					= 0;
	
			// Added by Mary@20120911-Change_Request-20120719, ReqNo.12
			int intLotSize 			= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? stockInfo.getSharePerLot() : 1;

			for (Iterator<MarketDepth> itr = marketDepths.iterator(); itr.hasNext();) {
				MarketDepth marketDepth 	= itr.next();
				/*
				System.out.println("[Stock][prepareMarketDepthView]marketdept no: "+id+" - valueaskprice: "+ marketDepth.getAskPrice() + ", valueAskSize: "+ marketDepth.getAskSize());
				System.out.println("[Stock][prepareMarketDepthView]marketdept no: "+id+" - valueBidPricee: "+ marketDepth.getBidPrice() + ", valueBidSize: "+ marketDepth.getBidSize());
				/
				
				TableRow tableRow 			= new TableRow(this);
				tableRow.setId(id);
				tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
				tableRow.setWeightSum(1);
				View rowView				= null;				
				
				/* Mary@20120827 - Fixes_Request-20120815, ReqNo.3
				if(width > 480 & height > 800)
					rowView = this.getLayoutInflater().inflate(R.layout.bid_ask_row2, null);
				else
					rowView = this.getLayoutInflater().inflate(R.layout.bid_ask_row, null);
				/
				rowView = this.getLayoutInflater().inflate( (width > 480 & height > 800) ? R.layout.bid_ask_row2 : R.layout.bid_ask_row, null);
	
				TextView countLabel 		= (TextView) rowView.findViewById(R.id.countLabel);
				countLabel.setText(String.valueOf(id + 1));
	
				final TextView bidSizeLabel = (TextView) rowView.findViewById(R.id.bidSizeLabel);
				/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
				/* Mary@20120911-Change_Request-20120719, ReqNo.12
				bidSizeLabel.setText( marketDepth.getBidSize() == 0 ? "0" : FormatUtil.formatLong( marketDepth.getBidSize() ) );
				/
				bidSizeLabel.setText(FormatUtil.formatLong(marketDepth.getBidSize()	/ intLotSize));
				/
				bidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepth.getBidSize() / intLotSize) ) );
	
				final TextView bidPriceLabel = (TextView) rowView.findViewById(R.id.bidPriceLabel);
				/* Mary@20120911-Change_Request-20120719, ReqNo.12 
				 * if(marketDepth.getBidPrice() == -999001 || marketDepth.getBidPrice() == 999001) {
				 /
				if(AppConstants.isMarketOrderStock( marketDepth.getBidPrice() ) ){
					bidPriceLabel.setText("MO");
				/* Mary@20120911-Change_Request-20120719, ReqNo.12 
				 * } else if (marketDepth.getBidPrice() == -999002 || marketDepth.getBidPrice() == 999002) {
				 /
				}else if( AppConstants.isMarketPriceStock( marketDepth.getBidPrice() ) ){
					bidPriceLabel.setText("MP");
				}else{
					if( symbolExchg[1].equals("JK") || symbolExchg[1].equals("JKD") )
						bidPriceLabel.setText( FormatUtil.formatPrice2( marketDepth.getBidPrice() ) );
					else
						bidPriceLabel.setText( FormatUtil.formatPrice( marketDepth.getBidPrice() ) );
				}
	
				if (marketDepth.getBidPrice() == stockInfo.getPreviousClose() || marketDepth.getBidPrice() == 0)
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status 
					 * bidPriceLabel.setTextColor(Color.BLACK);
					 /
					bidPriceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
				else if (marketDepth.getBidPrice() < stockInfo.getPreviousClose())
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					 * bidPriceLabel.setTextColor(LayoutSettings.colorRed);
					 /
					bidPriceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
				else
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					 * bidPriceLabel.setTextColor(LayoutSettings.colorGreen);
					 /
					bidPriceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
	
				bidPriceLabel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						/* Mary@20130225 - Change_Request-20130225, ReqNo.1
						// Added by Mary@20120822 - Fixes_Request-20120815, ReqNo.2
						if (!AppConstants.noTradeClient) {
						/
						if( !AppConstants.noTradeClient && AppConstants.brokerConfigBean.enableTrade()){
							ArrayList<StockInfo> Stock = new ArrayList<StockInfo>();
							Stock.add(stockInfo);
	
							/* Mary@20120830 - Change_Request-20120719, ReqNo.12 - START
							 * if(!bidPriceLabel.getText().toString().equals("0.000") || istradeValid == false){ 
							 * Intent intent = new Intent(); 
							 * intent.putExtra(ParamConstants.BID_ASK_PRICE_TAG, bidPriceLabel.getText().toString() );
							 /
							String strTemp = bidPriceLabel.getText().toString().trim();
							if (!(strTemp.equals("0.000") || strTemp.equals("0") || strTemp.equals("-")) && istradeValid) {								
								Intent intent 	= new Intent();
								intent.putExtra( ParamConstants.BID_ASK_PRICE_TAG, strTemp);
								intent.putExtra( ParamConstants.BID_ASK_QTY_TAG, getCumulativeBidAskQty(marketDepths, strTemp, INT_BID_RELATED) );
								// Mary@20120830 - Change_Request-20120719, ReqNo.12
								// - END
								intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
								intent.putExtra(ParamConstants.LOT_SIZE_TAG, Integer.toString( stockInfo.getSharePerLot() ) );
								intent.putExtra(ParamConstants.INSTRUMENT_TAG, stockInfo.getInstrument() );
								// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4
								intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stockInfo.isPayableWithCPF());
								intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
								intent.setClass(StockActivity.this,	TradeActivity.class);
								startActivity(intent);
							}
							// Added by Mary@20120830 - Change_Request-20120719,
							// ReqNo.12
							strTemp = null;
						}
					}
				});
	
				bidSizeLabels.add(bidSizeLabel);
				bidPriceLabels.add(bidPriceLabel);
	
				final TextView askSizeLabel = (TextView) rowView.findViewById(R.id.askSizeLabel);
				/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
				/* Mary@20120911-Change_Request-20120719, ReqNo.12
				askSizeLabel.setText(marketDepth.getAskSize() == 0 ? "0" : FormatUtil.formatLong(marketDepth.getAskSize()));
				/
				askSizeLabel.setText(FormatUtil.formatLong(marketDepth.getAskSize()	/ intLotSize));
				/
				askSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepth.getAskSize() / intLotSize) ) );
	
				final TextView askPriceLabel = (TextView) rowView.findViewById(R.id.askPriceLabel);
				/* Mary@20120827 - Fixes_Request-20120815, ReqNo.3
				if (marketDepth.getAskPrice() == -999001 || marketDepth.getAskPrice() == 999001) {
				/
				if( AppConstants.isMarketOrderStock( marketDepth.getAskPrice() ) ){
					askPriceLabel.setText("MO");
				/* Mary@20120827 - Fixes_Request-20120815, ReqNo.3
				} else if (marketDepth.getAskPrice() == -999002 || marketDepth.getAskPrice() == 999002) {
				/
				}else if( AppConstants.isMarketPriceStock( marketDepth.getAskPrice() ) ){
					askPriceLabel.setText("MP");
				}else{
					if( symbolExchg[1].equals("JK") || symbolExchg[1].equals("JKD") )
						askPriceLabel.setText(FormatUtil.formatPrice2(marketDepth.getAskPrice()));
					else
						askPriceLabel.setText(FormatUtil.formatPrice(marketDepth.getAskPrice()));
				}
				if (marketDepth.getAskPrice() == stockInfo.getPreviousClose() || marketDepth.getAskPrice() == 0)
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					 * askPriceLabel.setTextColor(Color.BLACK);
					 /
					askPriceLabel.setTextColor(getResources().getColor(R.color.NoStatus_FgColor));
				else if ( marketDepth.getAskPrice() < stockInfo.getPreviousClose() )
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					 * askPriceLabel.setTextColor(LayoutSettings.colorRed);
					 /
					askPriceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
				else
					/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
					 * askPriceLabel.setTextColor(LayoutSettings.colorGreen);
					 /
					askPriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_BgColor));
	
				askPriceLabel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						// Added by Mary@20120822 - Fixes_Request-20120815, ReqNo.2
						if( !AppConstants.noTradeClient ){
							ArrayList<StockInfo> Stock = new ArrayList<StockInfo>();
							Stock.add(stockInfo);
	
							/* Mary@20120830 - Change_Request-20120719, ReqNo.12 - START
							 * if(!askPriceLabel.getText().toString().equals("0.000") || istradeValid == false){
							 /
							String strTemp = askPriceLabel.getText().toString().trim();
							if( !( strTemp.equals("0.000") || strTemp.equals("0") || strTemp.equals("-") ) && istradeValid ){							
								Intent intent = new Intent();
								intent.putExtra(ParamConstants.BID_ASK_PRICE_TAG, strTemp);
								intent.putExtra(ParamConstants.BID_ASK_QTY_TAG,	getCumulativeBidAskQty(marketDepths, strTemp, INT_ASK_RELATED) );
								// Mary@20120830 - Change_Request-20120719, ReqNo.12 - END
								intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
								intent.putExtra(ParamConstants.INSTRUMENT_TAG, stockInfo.getInstrument());
								intent.putExtra(ParamConstants.LOT_SIZE_TAG, Integer.toString(stockInfo.getSharePerLot()));
								// Added by Mary@20130108 - Change_Request-20130104, ReqNo.4
								intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stockInfo.isPayableWithCPF());
								intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
								intent.setClass(StockActivity.this, TradeActivity.class);
								startActivity(intent);
							}
							// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12
							strTemp = null;
						}
					}
				});
	
				askSizeLabels.add(askSizeLabel);
				askPriceLabels.add(askPriceLabel);
	
				/* Mary@20120827 - Fixes_Request-20120815, ReqNo.3
				if (marketDepth.getBidPrice() != -999001 && marketDepth.getBidPrice() != 999001	&& marketDepth.getBidPrice() != -999002	&& marketDepth.getBidPrice() != 999002) {
				/
				if( ! AppConstants.isMarketOrderStock( marketDepth.getBidPrice() ) && ! AppConstants.isMarketPriceStock( marketDepth.getBidPrice() ) ){
					totalBid	+= marketDepth.getBidSize();
					bidSum		+= marketDepth.getBidSize() * marketDepth.getBidPrice();
				}
	
				/* Mary@20120827 - Fixes_Request-20120815, ReqNo.3
				if (marketDepth.getAskPrice() != -999001 && marketDepth.getAskPrice() != 999001 && marketDepth.getAskPrice() != -999002 && marketDepth.getAskPrice() != 999002) {
				 /
				if( ! AppConstants.isMarketOrderStock( marketDepth.getAskPrice() ) && !AppConstants.isMarketPriceStock( marketDepth.getAskPrice() ) ){
					totalAsk	+= marketDepth.getAskSize();
					askSum 		+= marketDepth.getAskPrice() * marketDepth.getAskSize();
				}
	
				tableRow.addView(rowView);
	
				LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.bidAskLayout);
	
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				if( (id + 1) % 2 != 0 )
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
				else
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
				/
				rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);
				
				tableLayoutBidAsk.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
	
				id++;
			}
	
			TableRow tableRow 		= new TableRow(this);
			tableRow.setId(id);
			tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
			tableRow.setWeightSum(1);
			
			View rowView			= null;
			/* Mary@20121001 - Fixes_Request-20120815, ReqNo.3
			if (width > 480 & height > 800) {
				rowView = this.getLayoutInflater().inflate(R.layout.bid_ask_row2, null);
			} else {
				rowView = this.getLayoutInflater().inflate(R.layout.bid_ask_row, null);
			}
			/
			rowView 				= this.getLayoutInflater().inflate( (width > 480 & height > 800) ? R.layout.bid_ask_row2 : R.layout.bid_ask_row, null);
	
			TextView countLabel		= (TextView) rowView.findViewById(R.id.countLabel);
			countLabel.setText("");
	
			bidSizeLabel 			= (TextView) rowView.findViewById(R.id.bidSizeLabel);
			/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
			/* Mary@20120911-Change_Request-20120719, ReqNo.12
			bidSizeLabel.setText(totalBid == 0? "0" : FormatUtil.formatLong(totalBid));
			/
			bidSizeLabel.setText(FormatUtil.formatLong(totalBid / intLotSize));
			/
			bidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalBid / intLotSize) ) );
	
			float averageBid 		= 0;
			if (bidSum != 0 && totalBid != 0)
				averageBid = (float) bidSum / totalBid;
			
			bidPriceLabel = (TextView) rowView.findViewById(R.id.bidPriceLabel);
			bidPriceLabel.setText(FormatUtil.formatPrice(averageBid));
			bidPriceLabel.setTextColor(Color.BLACK);
	
			askSizeLabel 			= (TextView) rowView.findViewById(R.id.askSizeLabel);
			/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
			/* Mary@20120911-Change_Request-20120719, ReqNo.12
			askSizeLabel.setText(FormatUtil.formatLong(totalAsk));
			/
			askSizeLabel.setText(FormatUtil.formatLong(totalAsk / intLotSize));
			/
			askSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalAsk / intLotSize) ) );
	
			double averageAsk		= 0;
			if (askSum != 0 && totalAsk != 0)
				averageAsk	= askSum / totalAsk;
				
			askPriceLabel 			= (TextView) rowView.findViewById(R.id.askPriceLabel);
			askPriceLabel.setText(FormatUtil.formatPrice(Double.valueOf(averageAsk).floatValue()));
			askPriceLabel.setTextColor(Color.BLACK);
	
			LinearLayout rowLayout 	= (LinearLayout) rowView.findViewById(R.id.bidAskLayout);
			rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
			tableRow.addView(rowView);
			//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 START
			tableLayoutSummary.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
			//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 END
			TableRow totalRow	= new TableRow(this);
			totalRow.setId(id++);
			totalRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT) );
			totalRow.setWeightSum(1);
			
			View totalView 		= null;
			/* Mary@20121001 - Fixes_Request-20120815, ReqNo.3
			if (width > 480 && height > 800) {
				totalView = this.getLayoutInflater().inflate(R.layout.sum_depth_row2, null);
			} else {
				totalView = this.getLayoutInflater().inflate(R.layout.sum_depth_row, null);
			}
			/
			
			totalView		= this.getLayoutInflater().inflate( (width > 480 && height > 800) ? R.layout.sum_depth_row2 : R.layout.sum_depth_row, null);
	
			TextView tBid 	= (TextView) totalView.findViewById(R.id.totalBid);
			TextView tAsk 	= (TextView) totalView.findViewById(R.id.totalAsk);
	
			totalBidLabel 	= (TextView) totalView.findViewById(R.id.totalBidLabel);
			totalBidLabel.setText(FormatUtil.formatLong(Double.valueOf(bidSum).longValue()));
			
			totalAskLabel 	= (TextView) totalView.findViewById(R.id.totalAskLabel);
			totalAskLabel.setText(FormatUtil.formatLong(Double.valueOf(askSum).longValue()));
	
			/* Mary@20130312 - Fixes_Request-20130108, ReqNo.19
			if( AppConstants.DEFAULT_EXCHANGE_CODE.equals("MY") ){
			/
			if( AppConstants.getListDerivativesExchgCode().contains(AppConstants.DEFAULT_EXCHANGE_CODE) ){
				totalBidLabel.setVisibility(View.GONE);
				totalAskLabel.setVisibility(View.GONE);
				tBid.setVisibility(View.GONE);
				tAsk.setVisibility(View.GONE);
			}else{
				totalBidLabel.setVisibility(View.VISIBLE);
				totalAskLabel.setVisibility(View.VISIBLE);
				tBid.setVisibility(View.VISIBLE);
				tAsk.setVisibility(View.VISIBLE);
			}
	
			LinearLayout sumLayout = (LinearLayout) totalView.findViewById(R.id.sumDepthLayout);
			sumLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
			totalRow.addView(totalView);
			//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 START
			tableLayoutSummary.addView(totalRow, new TableLayout.LayoutParams(	TableRow.LayoutParams.MATCH_PARENT,	TableRow.LayoutParams.WRAP_CONTENT));
			//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 END

			if(width > 400 && height > 800){
				if(marketDepths.size() < 6){
					TableRow tableRow2 = new TableRow(this);
					tableRow2.setId(id);
					tableRow2.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
					tableRow2.setWeightSum(1);
	
					View rowView2 = this.getLayoutInflater().inflate(R.layout.bid_ask_row2, null);
	
					rowView2.setBackgroundColor(Color.WHITE);
					tableRow2.addView(rowView2);
	
					TextView countLabel2 = (TextView) rowView2.findViewById(R.id.countLabel);
					countLabel2.setText("");
	
					TextView bidSizeLabel = (TextView) rowView2.findViewById(R.id.bidSizeLabel);
					bidSizeLabel.setText("");
	
					TextView bidPriceLabel = (TextView) rowView2.findViewById(R.id.bidPriceLabel);
					bidPriceLabel.setText("");
	
					TextView askSizeLabel = (TextView) rowView2.findViewById(R.id.askSizeLabel);
					askSizeLabel.setText("");
	
					TextView askPriceLabel = (TextView) rowView2.findViewById(R.id.askPriceLabel);
					askPriceLabel.setText("");
					//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 START
					tableLayoutSummary.addView(tableRow2, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,	TableRow.LayoutParams.WRAP_CONTENT));
					//Added by Kw@20130116 - Change_Request20130104, ReqNo.6 START
				}
			}
	
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	/* Mary@20130826 - Fixes_Request-20130711, ReqNo.14 - START
	private void prepareMarketDepthView(View view, final List<MarketDepth> lstOldMarketDepths, final List<MarketDepth> lstNewMarketDepths) {
		/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
		String[] symbolExchg			= null;
		/
		String strSymbolExchg			= null;
		TableLayout tableLayoutBidAsk	= null;
		TableLayout tableLayoutSummary	= null;

		int id 							= 0;
		long totalBid 					= 0;
		long totalAsk 					= 0;
		double bidSum 					= 0;
		double askSum 					= 0;
		
		long gapBidSize 				= 0;
		float gapBidPrice 				= 0;
		long gapAskSize 				= 0;
		float gapAskPrice 				= 0;

		int intLotSize 					= 0;
		
		try{			
			if(view == null)
				return;
	
			/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
			symbolExchg 		= stockInfo.getSymbolCode().split("\\.");
			/
			strSymbolExchg 		= stockInfo.getSymbolCode().substring( (stockInfo.getSymbolCode().lastIndexOf(".") + 1), stockInfo.getSymbolCode().length() );
			
			if(lstOldMarketDepths.size() == 0){
				tableLayoutBidAsk	= (TableLayout) view.findViewById(R.id.bidAskTable);
				tableLayoutSummary 	= (TableLayout) view.findViewById(R.id.bidAskSummaryTable);
				tableLayoutBidAsk.removeAllViews();
				
				bidSizeLabels 		= new ArrayList<TextView>();
				bidPriceLabels		= new ArrayList<TextView>();
				askPriceLabels		= new ArrayList<TextView>();
				askSizeLabels		= new ArrayList<TextView>();
				counterLabels		= new ArrayList<TextView>();
			}
		
			intLotSize			= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? stockInfo.getSharePerLot() : 1;

			for (Iterator<MarketDepth> itr = lstNewMarketDepths.iterator(); itr.hasNext();) {
				final MarketDepth marketDepthNew= itr.next();
				final MarketDepth marketDepthOld= ( lstOldMarketDepths.size() > id ) ? lstOldMarketDepths.get(id) : null;
				/*
				System.out.println("[Stock][prepareMarketDepthView]marketdept no: "+id+" - valueaskprice: "+ marketDepth.getAskPrice() + ", valueAskSize: "+ marketDepth.getAskSize());
				System.out.println("[Stock][prepareMarketDepthView]marketdept no: "+id+" - valueBidPricee: "+ marketDepth.getBidPrice() + ", valueBidSize: "+ marketDepth.getBidSize());
				/
				
				View rowView		= null;
				TableRow tableRow 	= null; 
				if(marketDepthOld == null){
					tableRow 				= new TableRow(this);
					tableRow.setId(id);
					tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
					tableRow.setWeightSum(1);
					
					rowView					= this.getLayoutInflater().inflate( (width > 480 & height > 800) ? R.layout.bid_ask_row2 : R.layout.bid_ask_row, null);
					LinearLayout rowLayout 	= (LinearLayout) rowView.findViewById(R.id.bidAskLayout);
					rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);
				}
				
				/** COUNTER - START /
				final TextView tvCountLabel		= marketDepthOld != null ? counterLabels.get(id) : (TextView) rowView.findViewById(R.id.countLabel);
				tvCountLabel.setText( String.valueOf(id + 1) );
				if(marketDepthOld == null){
					counterLabels.add(tvCountLabel);
				}
				/** COUNTER - END /
				
				/** BID SIZE - START /
				final TextView tvBidSizeLabel 	=  marketDepthOld != null ? bidSizeLabels.get(id) : (TextView) rowView.findViewById(R.id.bidSizeLabel);
				tvBidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepthNew.getBidSize() / intLotSize) ) );
				if(marketDepthOld != null){
					gapBidSize	= marketDepthNew.getBidSize() - marketDepthOld.getBidSize();
					if (gapBidSize > 0) {
						tvBidSizeLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
						tvBidSizeLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
					} else if (gapBidSize < 0) {
						tvBidSizeLabel.setBackgroundColor(getResources().getColor(R.color.DownStatus_BgColor));
						tvBidSizeLabel.setTextColor(getResources().getColor(R.color.DownStatus_FgColor));
					}
					
					tvBidSizeLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvBidSizeLabel.setBackgroundColor(Color.TRANSPARENT);
							tvBidSizeLabel.setTextColor(Color.BLACK);
						}
					}, 1500);
				}else{
					bidSizeLabels.add(tvBidSizeLabel);
				}
				/** BID SIZE - END /
				
				/** BID PRICE - START /
				final TextView tvBidPriceLabel 	=  marketDepthOld != null ? bidPriceLabels.get(id) : (TextView) rowView.findViewById(R.id.bidPriceLabel);
				if(AppConstants.isMarketOrderStock( marketDepthNew.getBidPrice() ) ){
					tvBidPriceLabel.setText("MO");
				}else if( AppConstants.isMarketPriceStock( marketDepthNew.getBidPrice() ) ){
					tvBidPriceLabel.setText("MP");
				}else{
					/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
					if( symbolExchg[1].equals("JK") || symbolExchg[1].equals("JKD") )
					/
					if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
						tvBidPriceLabel.setText( FormatUtil.formatPrice2( marketDepthNew.getBidPrice() ) );
					else
						tvBidPriceLabel.setText( FormatUtil.formatPrice( marketDepthNew.getBidPrice() ) );
				}
	
				if(marketDepthOld != null){					
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23
					if ( ! AppConstants.isMarketOrderStock( marketDepthNew.getBidPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthNew.getBidPrice() ) ){
						gapBidPrice = marketDepthNew.getBidPrice() - marketDepthOld.getBidPrice();
						if(gapBidPrice > 0.0 || AppConstants.isMarketOrderStock( marketDepthNew.getBidPrice() ) || AppConstants.isMarketPriceStock( marketDepthNew.getBidPrice() ) ){
							tvBidPriceLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
							tvBidPriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
						}else if(gapBidPrice < 0.0){
							tvBidPriceLabel.setBackgroundColor(getResources().getColor(R.color.DownStatus_BgColor));
							tvBidPriceLabel.setTextColor(getResources().getColor(R.color.DownStatus_FgColor));
						}
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - START
					}else{
						if( ! AppConstants.isMarketOrderStock( marketDepthOld.getBidPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthOld.getBidPrice() )  ){
							tvBidPriceLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
							tvBidPriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
						}
					}
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - END

					tvBidPriceLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvBidPriceLabel.setBackgroundColor(Color.TRANSPARENT);
							
							// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23
							if ( ! AppConstants.isMarketOrderStock( marketDepthNew.getBidPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthNew.getBidPrice() ) ){
								if (marketDepthNew.getBidPrice() == stockInfo.getLastDone() || marketDepthNew.getBidPrice() == 0)
									tvBidPriceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
								else if (marketDepthNew.getBidPrice() < stockInfo.getLastDone())
									tvBidPriceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
								else
									tvBidPriceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
							// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - START
							}else{
								tvBidPriceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
							}
							// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - END
						}
					}, 1500);
				}else{
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23
					if ( ! AppConstants.isMarketOrderStock( marketDepthNew.getBidPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthNew.getBidPrice() ) ){
						if (marketDepthNew.getBidPrice() == stockInfo.getLastDone() || marketDepthNew.getBidPrice() == 0)
							tvBidPriceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
						else if (marketDepthNew.getBidPrice() < stockInfo.getLastDone())
							tvBidPriceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
						else
							tvBidPriceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - START
					}else{
						tvBidPriceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
					}
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - END
				}
	
				tvBidPriceLabel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END

						if( !AppConstants.noTradeClient && AppConstants.brokerConfigBean.enableTrade() ){
							ArrayList<StockInfo> Stock = new ArrayList<StockInfo>();
							Stock.add(stockInfo);

							String strTemp = tvBidPriceLabel.getText().toString().trim();
							if (!(strTemp.equals("0.000") || strTemp.equals("0") || strTemp.equals("-")) && istradeValid) {								
								Intent intent 	= new Intent();
								intent.putExtra( ParamConstants.BID_ASK_PRICE_TAG, strTemp);
								intent.putExtra( ParamConstants.BID_ASK_QTY_TAG, getCumulativeBidAskQty(lstNewMarketDepths, strTemp, INT_BID_RELATED) );
								intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
								intent.putExtra(ParamConstants.LOT_SIZE_TAG, Integer.toString( stockInfo.getSharePerLot() ) );
								intent.putExtra(ParamConstants.INSTRUMENT_TAG, stockInfo.getInstrument() );
								intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stockInfo.isPayableWithCPF());
								intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
								intent.setClass(StockActivity.this,	TradeActivity.class);
								startActivity(intent);
							}
							strTemp = null;
						}
					}
				});
				
				if(marketDepthOld == null)
					bidPriceLabels.add(tvBidPriceLabel);
				/** BID PRICE - END /
				
				/** ASK SIZE - START /
				final TextView tvAskSizeLabel		= marketDepthOld != null ? askSizeLabels.get(id) : (TextView) rowView.findViewById(R.id.askSizeLabel);
				tvAskSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepthNew.getAskSize() / intLotSize) ) );
				
				if(marketDepthOld != null){
					gapAskSize = marketDepthNew.getAskSize() - marketDepthOld.getAskSize();
					if (gapAskSize > 0) {
						tvAskSizeLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
						tvAskSizeLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
					} else if (gapAskSize < 0) {
						tvAskSizeLabel.setBackgroundColor(getResources().getColor(R.color.DownStatus_BgColor));
						tvAskSizeLabel.setTextColor(getResources().getColor(R.color.DownStatus_FgColor));
					}
					
					tvAskSizeLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvAskSizeLabel.setBackgroundColor(Color.TRANSPARENT);
							tvAskSizeLabel.setTextColor(Color.BLACK);
						}
					}, 1500);
				}else{
					askSizeLabels.add(tvAskSizeLabel);
				}
				/** ASK SIZE - END /
				
				/** ASK PRICE - START /
				final TextView tvAskPriceLabel	= marketDepthOld != null ? askPriceLabels.get(id) : (TextView) rowView.findViewById(R.id.askPriceLabel);
				if( AppConstants.isMarketOrderStock( marketDepthNew.getAskPrice() ) ){
					tvAskPriceLabel.setText("MO");
				}else if( AppConstants.isMarketPriceStock( marketDepthNew.getAskPrice() ) ){
					tvAskPriceLabel.setText("MP");
				}else{
					/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
					if( symbolExchg[1].equals("JK") || symbolExchg[1].equals("JKD") )
					/
					if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
						tvAskPriceLabel.setText(FormatUtil.formatPrice2(marketDepthNew.getAskPrice()));
					else
						tvAskPriceLabel.setText(FormatUtil.formatPrice(marketDepthNew.getAskPrice()));
				}
				
				if(marketDepthOld != null){
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23
					if ( ! AppConstants.isMarketOrderStock( marketDepthNew.getAskPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthNew.getAskPrice() ) ){
						gapAskPrice = marketDepthNew.getAskPrice() - marketDepthOld.getAskPrice();
						if (gapAskPrice > 0.0 || AppConstants.isMarketOrderStock( marketDepthNew.getAskPrice() ) || AppConstants.isMarketPriceStock( marketDepthNew.getAskPrice() ) ) {
							tvAskPriceLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
							tvAskPriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
						} else if (gapAskPrice < 0.0) {
							tvAskPriceLabel.setBackgroundColor(getResources().getColor(R.color.DownStatus_BgColor));
							tvAskPriceLabel.setTextColor(getResources().getColor(R.color.DownStatus_FgColor));
						}
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - START
					}else{
						if( ! AppConstants.isMarketOrderStock( marketDepthOld.getAskPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthOld.getAskPrice() ) ){
							tvAskPriceLabel.setBackgroundColor(getResources().getColor(R.color.UpStatus_BgColor));
							tvAskPriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_FgColor));
						}
					}
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - END
					
					tvAskPriceLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvAskPriceLabel.setBackgroundColor(Color.TRANSPARENT);
							
							// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23
							if ( ! AppConstants.isMarketOrderStock( marketDepthNew.getAskPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthNew.getAskPrice() ) ){
								if (marketDepthNew.getAskPrice() == stockInfo.getLastDone() || marketDepthNew.getAskPrice() == 0)
									tvAskPriceLabel.setTextColor(getResources().getColor(R.color.NoStatus_FgColor));
								else if ( marketDepthNew.getAskPrice() < stockInfo.getLastDone() )
									tvAskPriceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
								else
									tvAskPriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_BgColor));
							// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - START
							}else{
								tvAskPriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_BgColor));
							}
							// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - END
						}
					}, 1500);
				}else{
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23
					if ( ! AppConstants.isMarketOrderStock( marketDepthNew.getAskPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthNew.getAskPrice() ) ){
						if (marketDepthNew.getAskPrice() == stockInfo.getLastDone() || marketDepthNew.getAskPrice() == 0)
							tvAskPriceLabel.setTextColor(getResources().getColor(R.color.NoStatus_FgColor));
						else if ( marketDepthNew.getAskPrice() < stockInfo.getLastDone() )
							tvAskPriceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
						else
							tvAskPriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_BgColor));
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - START
					}else{
						tvAskPriceLabel.setTextColor(getResources().getColor(R.color.UpStatus_BgColor));
					}
					// Added by Mary@20130628 - Fixes_Request-20130523, ReqNo.23 - END
				}
	
				tvAskPriceLabel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
						
						if( !AppConstants.noTradeClient ){
							ArrayList<StockInfo> Stock = new ArrayList<StockInfo>();
							Stock.add(stockInfo);

							String strTemp = tvAskPriceLabel.getText().toString().trim();
							if( !( strTemp.equals("0.000") || strTemp.equals("0") || strTemp.equals("-") ) && istradeValid ){							
								Intent intent = new Intent();
								intent.putExtra(ParamConstants.BID_ASK_PRICE_TAG, strTemp);
								intent.putExtra(ParamConstants.BID_ASK_QTY_TAG,	getCumulativeBidAskQty(lstNewMarketDepths, strTemp, INT_ASK_RELATED) );
								intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
								intent.putExtra(ParamConstants.INSTRUMENT_TAG, stockInfo.getInstrument());
								intent.putExtra(ParamConstants.LOT_SIZE_TAG, Integer.toString(stockInfo.getSharePerLot()));
								intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stockInfo.isPayableWithCPF());
								intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
								intent.setClass(StockActivity.this, TradeActivity.class);
								startActivity(intent);
							}
							strTemp = null;
						}
					}
				});
				
				if(marketDepthOld == null)
					askPriceLabels.add(tvAskPriceLabel);
				/** ASK PRICE - END /
				
				if(marketDepthOld == null){
					tableRow.addView(rowView);			
					tableLayoutBidAsk.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
				}
	
				/** COMPUTATION - SUMMARY OF BID & ASK - START /
				if( ! AppConstants.isMarketOrderStock( marketDepthNew.getBidPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthNew.getBidPrice() ) ){
					totalBid	+= marketDepthNew.getBidSize();
					bidSum		+= marketDepthNew.getBidSize() * marketDepthNew.getBidPrice();
				}

				if( ! AppConstants.isMarketOrderStock( marketDepthNew.getAskPrice() ) && !AppConstants.isMarketPriceStock( marketDepthNew.getAskPrice() ) ){
					totalAsk	+= marketDepthNew.getAskSize();
					askSum 		+= marketDepthNew.getAskPrice() * marketDepthNew.getAskSize();
				}
				/** COMPUTATION - SUMMARY OF BID & ASK - END /
				
				id++;
			}
	

			/** AVERAGE OF BID & ASK - START /
			TableRow tableRow	= null;
			View rowView		= null;
			if(lstOldMarketDepths.size() == 0){
				tableRow 				= new TableRow(this);
				tableRow.setId(id);
				tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
				tableRow.setWeightSum(1);
				
				rowView					= this.getLayoutInflater().inflate( (width > 480 & height > 800) ? R.layout.bid_ask_row2 : R.layout.bid_ask_row, null);
				TextView countLabel		= (TextView) rowView.findViewById(R.id.countLabel);
				countLabel.setText("");
			}
	
			/*** AVERAGE OF BID - START /
			if(lstOldMarketDepths.size() == 0)
				bidSizeLabel	= (TextView) rowView.findViewById(R.id.bidSizeLabel);
			bidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalBid / intLotSize) ) );
	
			if(lstOldMarketDepths.size() == 0){
				bidPriceLabel 	= (TextView) rowView.findViewById(R.id.bidPriceLabel);
				bidPriceLabel.setTextColor(Color.BLACK);
			}
			bidPriceLabel.setText( FormatUtil.formatPrice( (bidSum != 0 && totalBid != 0) ? ( (float) bidSum / totalBid ) : 0 ) );
			/*** AVERAGE OF BID - END /
			
			/*** AVERAGE OF ASK - START /
			if(lstOldMarketDepths.size() == 0)
				askSizeLabel 	= (TextView) rowView.findViewById(R.id.askSizeLabel);
			askSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalAsk / intLotSize) ) );
				
			if(lstOldMarketDepths.size() == 0){
				askPriceLabel 	= (TextView) rowView.findViewById(R.id.askPriceLabel);
				askPriceLabel.setTextColor(Color.BLACK);
			}
			askPriceLabel.setText(FormatUtil.formatPrice( (askSum != 0 && totalAsk != 0) ? ( (float) askSum / totalAsk ) : 0 ) );
			/*** AVERAGE OF ASK - END /
			
			/** TOTAL OF BID & ASK - START /
			if(lstOldMarketDepths.size() == 0){
				LinearLayout rowLayout 			= (LinearLayout) rowView.findViewById(R.id.bidAskLayout);
				rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
				tableRow.addView(rowView);
				tableLayoutSummary.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
				TableRow totalRow				= new TableRow(this);
				totalRow.setId(id++);
				totalRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT) );
				totalRow.setWeightSum(1);
				
				View totalView 					= this.getLayoutInflater().inflate( (width > 480 && height > 800) ? R.layout.sum_depth_row2 : R.layout.sum_depth_row, null);
				
				totalBidLabel 					= (TextView) totalView.findViewById(R.id.totalBidLabel);
				totalAskLabel 					= (TextView) totalView.findViewById(R.id.totalAskLabel);
				
				TextView tBid 					= (TextView) totalView.findViewById(R.id.totalBid);
				TextView tAsk 					= (TextView) totalView.findViewById(R.id.totalAsk);
				
				if( AppConstants.getListDerivativesExchgCode().contains(AppConstants.DEFAULT_EXCHANGE_CODE) ){
					totalBidLabel.setVisibility(View.GONE);
					totalAskLabel.setVisibility(View.GONE);
					tBid.setVisibility(View.GONE);
					tAsk.setVisibility(View.GONE);
				}else{
					totalBidLabel.setVisibility(View.VISIBLE);
					totalAskLabel.setVisibility(View.VISIBLE);
					tBid.setVisibility(View.VISIBLE);
					tAsk.setVisibility(View.VISIBLE);
				}
				
				LinearLayout sumLayout 			= (LinearLayout) totalView.findViewById(R.id.sumDepthLayout);
				sumLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
				totalRow.addView(totalView);
				tableLayoutSummary.addView(totalRow, new TableLayout.LayoutParams(	TableRow.LayoutParams.MATCH_PARENT,	TableRow.LayoutParams.WRAP_CONTENT));
				
				if(width > 400 && height > 800){
					if(lstNewMarketDepths.size() < 6){
						TableRow tableRow2	= new TableRow(this);
						tableRow2.setId(id);
						tableRow2.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
						tableRow2.setWeightSum(1);
		
						View rowView2 		= this.getLayoutInflater().inflate(R.layout.bid_ask_row2, null);
						rowView2.setBackgroundColor(Color.WHITE);
						( (TextView) rowView2.findViewById(R.id.countLabel) ).setText("");
						( (TextView) rowView2.findViewById(R.id.bidSizeLabel) ).setText("");
						( (TextView) rowView2.findViewById(R.id.bidPriceLabel) ).setText("");
						( (TextView) rowView2.findViewById(R.id.askSizeLabel) ).setText("");
						( (TextView) rowView2.findViewById(R.id.askPriceLabel) ).setText("");
						
						tableRow2.addView(rowView2);
						
						tableLayoutSummary.addView(tableRow2, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
					}
				}
			}
			totalBidLabel.setText(FormatUtil.formatLong(Double.valueOf(bidSum).longValue()));
			totalAskLabel.setText(FormatUtil.formatLong(Double.valueOf(askSum).longValue()));			
			/** TOTAL OF BID & ASK - end /
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
			if(symbolExchg != null)
				symbolExchg = null;
			/
			strSymbolExchg	= null;
			
			if(tableLayoutBidAsk != null){
				tableLayoutBidAsk.destroyDrawingCache();
				tableLayoutBidAsk = null;
			}
			
			if(tableLayoutSummary != null){
				tableLayoutSummary.destroyDrawingCache();
				tableLayoutSummary = null;
			}

			id			= 0;
			totalBid 	= 0;
			totalAsk 	= 0;
			bidSum 		= 0;
			askSum 		= 0;
			gapBidSize 	= 0;
			gapBidPrice = 0;
			gapAskSize 	= 0;
			gapAskPrice = 0;
			intLotSize 	= 0;
		}
	}
	// Mary@20130612 - Fixes_Request-20130523, ReqNo.9 - END
	*/
	private void prepareMarketDepthView(View view, final List<MarketDepth> lstOldMarketDepths, final List<MarketDepth> lstNewMarketDepths) {
		String strSymbolExchg			= null;
		TableLayout tableLayoutBidAsk	= null;
		TableLayout tableLayoutSummary	= null;

		int id 							= 0;
		long totalBid 					= 0;
		long totalAsk 					= 0;
		double bidSum 					= 0;
		double askSum 					= 0;

		int intLotSize 					= 0;
		
		try{			
			if(view == null)
				return;
			strSymbolExchg 		= stockInfo.getSymbolCode().substring( (stockInfo.getSymbolCode().lastIndexOf(".") + 1), stockInfo.getSymbolCode().length() );
			
			if(lstOldMarketDepths.size() == 0){
				tableLayoutBidAsk	= (TableLayout) view.findViewById(R.id.bidAskTable);
				tableLayoutSummary 	= (TableLayout) view.findViewById(R.id.bidAskSummaryTable);
				tableLayoutBidAsk.removeAllViews();
				
				bidSizeLabels 		= new ArrayList<TextView>();
				bidPriceLabels		= new ArrayList<TextView>();
				askPriceLabels		= new ArrayList<TextView>();
				askSizeLabels		= new ArrayList<TextView>();
				counterLabels		= new ArrayList<TextView>();
			}
		
			intLotSize			= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? stockInfo.getSharePerLot() : 1;

			for (Iterator<MarketDepth> itr = lstNewMarketDepths.iterator(); itr.hasNext();) {
				final MarketDepth marketDepthNew= itr.next();
				final MarketDepth marketDepthOld= ( lstOldMarketDepths.size() > id ) ? lstOldMarketDepths.get(id) : null;
				
				View rowView		= null;
				TableRow tableRow 	= null; 
				if(marketDepthOld == null){
					tableRow 				= new TableRow(this);
					tableRow.setId(id);
					tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
					tableRow.setWeightSum(1);
					
					//rowView					= this.getLayoutInflater().inflate( (width > 480 & height > 800) ? R.layout.bid_ask_row2 : R.layout.bid_ask_row, null);
					rowView					= this.getLayoutInflater().inflate(R.layout.bid_ask_row, null);
					/*commented, Change_Request-20160101, ReqNo.3
					LinearLayout rowLayout 	= (LinearLayout) rowView.findViewById(R.id.bidAskLayout);
					rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);
					 */
				}
				
				/** COUNTER - START **/
				final TextView tvCountLabel		= marketDepthOld != null ? counterLabels.get(id) : (TextView) rowView.findViewById(R.id.countLabel);
				tvCountLabel.setText( String.valueOf(id + 1) );
				if(marketDepthOld == null)
					counterLabels.add(tvCountLabel);

				/** COUNTER - END **/
				
				/** BID SIZE - START **/
				final TextView tvBidSizeLabel 	=  marketDepthOld != null ? bidSizeLabels.get(id) : (TextView) rowView.findViewById(R.id.bidSizeLabel);
				tvBidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepthNew.getBidSize() / intLotSize) ) );
				if(marketDepthOld != null){
					tvBidSizeLabel.setBackgroundColor( AppConstants.getIntBidAskQtyBlinkBackgroundColor(this, marketDepthOld.getBidSize(), marketDepthNew.getBidSize() ) );
					tvBidSizeLabel.setTextColor( AppConstants.getIntBidAskQtyBlinkTextColor(this, marketDepthOld.getBidSize(), marketDepthNew.getBidSize() ) );

					tvBidSizeLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvBidSizeLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
							tvBidSizeLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
						}
					}, 1500);
				}else{
					bidSizeLabels.add(tvBidSizeLabel);
				}
				/** BID SIZE - END **/
				
				/** BID PRICE - START **/
				final TextView tvBidPriceLabel 	=  marketDepthOld != null ? bidPriceLabels.get(id) : (TextView) rowView.findViewById(R.id.bidPriceLabel);
				tvBidPriceLabel.setText( AppConstants.getStrFormattedBidAskPrice(marketDepthNew.getBidPrice(), strSymbolExchg) );
	
				if(marketDepthOld != null){
					tvBidPriceLabel.setBackgroundColor( AppConstants.getIntBidAskPriceBlinkBackgroundColor( this, marketDepthOld.getBidPrice(), marketDepthNew.getBidPrice() ) );
					tvBidPriceLabel.setTextColor( AppConstants.getIntBidAskPriceBlinkTextColor( this, marketDepthOld.getBidPrice(), marketDepthNew.getBidPrice(), tvBidPriceLabel.getCurrentTextColor() ) );

					tvBidPriceLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							try{
								tvBidPriceLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
								tvBidPriceLabel.setTextColor( AppConstants.getIntBidAskPriceTextColor( StockActivity.this, marketDepthNew.getBidPrice(), stockInfo.getLACP() ) );
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}, 1500);
				}else{
					tvBidPriceLabel.setTextColor( AppConstants.getIntBidAskPriceTextColor( this, marketDepthNew.getBidPrice(), stockInfo.getLACP() ) );
				}
	
				tvBidPriceLabel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();

						if( !AppConstants.noTradeClient && AppConstants.brokerConfigBean.enableTrade() ){
							ArrayList<StockInfo> Stock = new ArrayList<StockInfo>();
							Stock.add(stockInfo);

							String strTemp = tvBidPriceLabel.getText().toString().trim();
							if (!(strTemp.equals("0.000") || strTemp.equals("0") || strTemp.equals("-")) && istradeValid) {								
								Intent intent 	= new Intent();
								intent.putExtra( ParamConstants.BID_ASK_PRICE_TAG, strTemp);
								intent.putExtra( ParamConstants.BID_ASK_QTY_TAG, getCumulativeBidAskQty(lstNewMarketDepths, strTemp, INT_BID_RELATED) );
								intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
								intent.putExtra(ParamConstants.LOT_SIZE_TAG, Integer.toString( stockInfo.getSharePerLot() ) );
								intent.putExtra(ParamConstants.INSTRUMENT_TAG, stockInfo.getInstrument() );
								intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stockInfo.isPayableWithCPF());
								intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
								// Added by Mary@20130925 - Change_Request-20130724, ReqNo.18
								intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
								intent.setClass(StockActivity.this,	TradeActivity.class);
								startActivity(intent);
							}
							strTemp = null;
						}
					}
				});
				
				if(marketDepthOld == null)
					bidPriceLabels.add(tvBidPriceLabel);
				/** BID PRICE - END **/
				
				/** ASK SIZE - START **/
				final TextView tvAskSizeLabel		= marketDepthOld != null ? askSizeLabels.get(id) : (TextView) rowView.findViewById(R.id.askSizeLabel);
				tvAskSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (marketDepthNew.getAskSize() / intLotSize) ) );
				
				if(marketDepthOld != null){
					tvAskSizeLabel.setBackgroundColor( AppConstants.getIntBidAskQtyBlinkBackgroundColor( this, marketDepthOld.getAskSize(), marketDepthNew.getAskSize() ) );
					tvAskSizeLabel.setTextColor( AppConstants.getIntBidAskQtyBlinkTextColor( this, marketDepthOld.getAskSize(), marketDepthNew.getAskSize() ) );
					
					tvAskSizeLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							tvAskSizeLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
							tvAskSizeLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
						}
					}, 1500);
				}else{
					askSizeLabels.add(tvAskSizeLabel);
				}
				/** ASK SIZE - END **/
				
				/** ASK PRICE - START **/
				final TextView tvAskPriceLabel	= marketDepthOld != null ? askPriceLabels.get(id) : (TextView) rowView.findViewById(R.id.askPriceLabel);
				tvAskPriceLabel.setText( AppConstants.getStrFormattedBidAskPrice(marketDepthNew.getAskPrice(), strSymbolExchg) );
				
				if(marketDepthOld != null){
					tvAskPriceLabel.setBackgroundColor( AppConstants.getIntBidAskPriceBlinkBackgroundColor( this, marketDepthOld.getAskPrice(), marketDepthNew.getAskPrice() ) );
					tvAskPriceLabel.setTextColor( AppConstants.getIntBidAskPriceBlinkTextColor( this, marketDepthOld.getAskPrice(), marketDepthNew.getAskPrice(), tvAskPriceLabel.getCurrentTextColor() ) );
					
					tvAskPriceLabel.postDelayed(new Runnable() {
						@Override
						public void run() {
							try{
								tvAskPriceLabel.setBackgroundColor( getResources().getColor(R.color.NoStatus_BgColor) );
								tvAskPriceLabel.setTextColor( AppConstants.getIntBidAskPriceTextColor( StockActivity.this, marketDepthNew.getAskPrice(), stockInfo.getLACP() ) );
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}, 1500);
				}else{
					tvAskPriceLabel.setTextColor( AppConstants.getIntBidAskPriceTextColor( this, marketDepthNew.getAskPrice(), stockInfo.getLACP() ) );
				}
	
				tvAskPriceLabel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						
						if( !AppConstants.noTradeClient ){
							ArrayList<StockInfo> Stock = new ArrayList<StockInfo>();
							Stock.add(stockInfo);

							String strTemp = tvAskPriceLabel.getText().toString().trim();
							if( !( strTemp.equals("0.000") || strTemp.equals("0") || strTemp.equals("-") ) && istradeValid ){							
								Intent intent = new Intent();
								intent.putExtra(ParamConstants.BID_ASK_PRICE_TAG, strTemp);
								intent.putExtra(ParamConstants.BID_ASK_QTY_TAG,	getCumulativeBidAskQty(lstNewMarketDepths, strTemp, INT_ASK_RELATED) );
								intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbolCode);
								intent.putExtra(ParamConstants.INSTRUMENT_TAG, stockInfo.getInstrument());
								intent.putExtra(ParamConstants.LOT_SIZE_TAG, Integer.toString(stockInfo.getSharePerLot()));
								intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, stockInfo.isPayableWithCPF());
								intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
								// Added by Mary@20130925 - Change_Request-20130724, ReqNo.18
								intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
								intent.setClass(StockActivity.this, TradeActivity.class);
								startActivity(intent);
							}
							strTemp = null;
						}
					}
				});
				
				if(marketDepthOld == null)
					askPriceLabels.add(tvAskPriceLabel);
				/** ASK PRICE - END **/
				
				if(marketDepthOld == null){
					tableRow.addView(rowView);			
					tableLayoutBidAsk.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
				}
	
				/** COMPUTATION - SUMMARY OF BID & ASK - START **/
				if( ! AppConstants.isMarketOrderStock( marketDepthNew.getBidPrice() ) && ! AppConstants.isMarketPriceStock( marketDepthNew.getBidPrice() ) ){
					totalBid	+= marketDepthNew.getBidSize();
					bidSum		+= marketDepthNew.getBidSize() * marketDepthNew.getBidPrice();
				}

				if( ! AppConstants.isMarketOrderStock( marketDepthNew.getAskPrice() ) && !AppConstants.isMarketPriceStock( marketDepthNew.getAskPrice() ) ){
					totalAsk	+= marketDepthNew.getAskSize();
					askSum 		+= marketDepthNew.getAskPrice() * marketDepthNew.getAskSize();
				}
				/** COMPUTATION - SUMMARY OF BID & ASK - END **/
				
				id++;
			}
	
			/** AVERAGE OF BID & ASK - START **/
			TableRow tableRow	= null;
			View rowView		= null;
			if(lstOldMarketDepths.size() == 0){
				tableRow 				= new TableRow(this);
				tableRow.setId(id);
				tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
				tableRow.setWeightSum(1);
				
				//rowView					= this.getLayoutInflater().inflate( (width > 480 & height > 800) ? R.layout.bid_ask_row2 : R.layout.bid_ask_row, null);	//edited, change_Request-20160101, ReqNo.3
				rowView					= this.getLayoutInflater().inflate(R.layout.bid_ask_row, null);
				TextView countLabel		= (TextView) rowView.findViewById(R.id.countLabel);
				countLabel.setText("");
			}
	
			/*** AVERAGE OF BID - START ***/
			if(lstOldMarketDepths.size() == 0)
				bidSizeLabel	= (TextView) rowView.findViewById(R.id.bidSizeLabel);
			bidSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalBid / intLotSize) ) );
	
			if(lstOldMarketDepths.size() == 0){
				bidPriceLabel 	= (TextView) rowView.findViewById(R.id.bidPriceLabel);
				bidPriceLabel.setTextColor(Color.BLACK);
			}
			float avgBidPrice	=  (bidSum != 0 && totalBid != 0) ? ( (float) bidSum / totalBid ) : 0;
			bidPriceLabel.setText(FormatUtil.formatPrice(avgBidPrice, "-", strSymbolExchg));
			/*** AVERAGE OF BID - END ***/
			
			/*** AVERAGE OF ASK - START ***/
			if(lstOldMarketDepths.size() == 0)
				askSizeLabel 	= (TextView) rowView.findViewById(R.id.askSizeLabel);
			askSizeLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (totalAsk / intLotSize) ) );
				
			if(lstOldMarketDepths.size() == 0){
				askPriceLabel 	= (TextView) rowView.findViewById(R.id.askPriceLabel);
				askPriceLabel.setTextColor(Color.BLACK);
			}
			float avgAskPrice	= (askSum != 0 && totalAsk != 0) ? ( (float) askSum / totalAsk ) : 0;
			askPriceLabel.setText(FormatUtil.formatPrice(avgAskPrice, "-", strSymbolExchg));
			/*** AVERAGE OF ASK - END ***/
			
			/** TOTAL OF BID & ASK - START **/
			if(lstOldMarketDepths.size() == 0){
				LinearLayout rowLayout 			= (LinearLayout) rowView.findViewById(R.id.bidAskLayout);
				rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
				tableRow.addView(rowView);
				tableLayoutSummary.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
				TableRow totalRow				= new TableRow(this);
				totalRow.setId(id++);
				totalRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT) );
				totalRow.setWeightSum(1);
				
				//View totalView 					= this.getLayoutInflater().inflate( (width > 480 && height > 800) ? R.layout.sum_depth_row2 : R.layout.sum_depth_row, null);	//Edited Change_Request-20160101, ReqNo.3
				View totalView 					= this.getLayoutInflater().inflate(R.layout.sum_depth_row, null);	
				
				totalBidLabel 					= (TextView) totalView.findViewById(R.id.totalBidLabel);
				totalAskLabel 					= (TextView) totalView.findViewById(R.id.totalAskLabel);
				
				TextView tBid 					= (TextView) totalView.findViewById(R.id.totalBid);
				TextView tAsk 					= (TextView) totalView.findViewById(R.id.totalAsk);
				
				if( AppConstants.getListDerivativesExchgCode().contains(AppConstants.DEFAULT_EXCHANGE_CODE) ){
					totalBidLabel.setVisibility(View.GONE);
					totalAskLabel.setVisibility(View.GONE);
					tBid.setVisibility(View.GONE);
					tAsk.setVisibility(View.GONE);
				}else{
					totalBidLabel.setVisibility(View.VISIBLE);
					totalAskLabel.setVisibility(View.VISIBLE);
					tBid.setVisibility(View.VISIBLE);
					tAsk.setVisibility(View.VISIBLE);
				}
				
				LinearLayout sumLayout 			= (LinearLayout) totalView.findViewById(R.id.sumDepthLayout);
				sumLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
				totalRow.addView(totalView);
				tableLayoutSummary.addView(totalRow, new TableLayout.LayoutParams(	TableRow.LayoutParams.MATCH_PARENT,	TableRow.LayoutParams.WRAP_CONTENT));
				
				if(width > 400 && height > 800){
					if(lstNewMarketDepths.size() < 6){
						TableRow tableRow2	= new TableRow(this);
						tableRow2.setId(id);
						tableRow2.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
						tableRow2.setWeightSum(1);
		
						View rowView2 		= this.getLayoutInflater().inflate(R.layout.bid_ask_row2, null);
						rowView2.setBackgroundColor(Color.WHITE);
						( (TextView) rowView2.findViewById(R.id.countLabel) ).setText("");
						( (TextView) rowView2.findViewById(R.id.bidSizeLabel) ).setText("");
						( (TextView) rowView2.findViewById(R.id.bidPriceLabel) ).setText("");
						( (TextView) rowView2.findViewById(R.id.askSizeLabel) ).setText("");
						( (TextView) rowView2.findViewById(R.id.askPriceLabel) ).setText("");
						
						tableRow2.addView(rowView2);
						
						tableLayoutSummary.addView(tableRow2, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.WRAP_CONTENT));
					}
				}
			}
			totalBidLabel.setText(FormatUtil.formatLong(Double.valueOf(bidSum).longValue()));
			totalAskLabel.setText(FormatUtil.formatLong(Double.valueOf(askSum).longValue()));			
			/** TOTAL OF BID & ASK - end **/
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			strSymbolExchg	= null;
			
			if(tableLayoutBidAsk != null){
				tableLayoutBidAsk.destroyDrawingCache();
				tableLayoutBidAsk = null;
			}
			
			if(tableLayoutSummary != null){
				tableLayoutSummary.destroyDrawingCache();
				tableLayoutSummary = null;
			}

			id			= 0;
			totalBid 	= 0;
			totalAsk 	= 0;
			bidSum 		= 0;
			askSum 		= 0;
			intLotSize 	= 0;
		}
	}
	// Mary@20130826 - Fixes_Request-20130711, ReqNo.14 - END
	
	// Sonia@20130702 - Change_Request-20130225, ReqNo.5 - Start
	private void prepareTimeSalesView(final View view, final List<TimeSales> lstTimeSales) {
		
		String[] symbolExchg				= null;
		TableLayout tableLayoutTimeSales	= null;
		
		int id = 0;
		
		try{
			if(view == null)
				return;
				
				if( progressDialog != null && progressDialog.isShowing() )
					progressDialog.dismiss();
				
				symbolExchg 						= stockInfo.getSymbolCode().split("\\.");
				totalTrades							= stockInfo.getTotalTrade();
				
				layoutInf 							= getLayoutInflater();
				final LinearLayout linear 			= (LinearLayout) view.findViewById(R.id.timesalesRefreshView);
				View v								= layoutInf.inflate(R.layout.pull_to_refresh_timesales,null);
				v.setPadding(0, -20, 0, 0);
				linear.addView(v);
				
				sv		 							= (ScrollView) findViewById(R.id.s_ScrollView);
				
				refreshV 							= (TimeSalesPullToRefresh) findViewById(R.id.s_RefreshableView);
				refreshV.mfooterView 				= (View) findViewById(R.id.s_RefreshFooter);
				refreshV.refreshIndicatorView2 		= (ImageView) findViewById(R.id.s_bottom_arrowImageView);
				refreshV.bar2 						= (ProgressBar) findViewById(R.id.s_bottom_progressBar);
				refreshV.sv							= sv;
				refreshV.mfooterViewText 			= (TextView) findViewById(R.id.s_tryrefresh_footer_text);
				
				LinearLayout layoutTapRefresh 		= (LinearLayout) view.findViewById(R.id.tapToRefresh);
				
				tableLayoutTimeSales	= (TableLayout) view.findViewById(R.id.salesTable);
				tableLayoutTimeSales.removeAllViews();
				
				//Added by Thinzar, Change_Request-20161101, ReqNo.1 - START
				TextView txtBBHTitle				= (TextView)view.findViewById(R.id.txtBBHTitle);
				TextView txtSBHTitle				= (TextView)view.findViewById(R.id.txtSBHTitle);
				if(isExchangePH){
					txtBBHTitle.setVisibility(View.VISIBLE);
					txtSBHTitle.setVisibility(View.VISIBLE);
				}else{
					txtBBHTitle.setVisibility(View.GONE);
					txtSBHTitle.setVisibility(View.GONE);
				}
				//Added by Thinzar, Change_Request-20161101, ReqNo.1 - END
				
				/* Commented out by Thinzar, unused variables
				timeLabels 		= new ArrayList<TextView>();
				tradedatLabels	= new ArrayList<TextView>();
				priceLabels		= new ArrayList<TextView>();
				volumeLabels	= new ArrayList<TextView>();
				BBHLabels		= new ArrayList<TextView>();
				SBHLabels		= new ArrayList<TextView>();
				*/
				for (Iterator<TimeSales> itr = lstTimeSales.iterator(); itr.hasNext();) {
					final TimeSales timeSalesList = itr.next();
					
					View rowView		= null;
					TableRow tableRow 	= null;
					
					tableRow 				= new TableRow(this);
					tableRow.setId(id);
					tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
					tableRow.setWeightSum(1);
						
					//rowView				= symbolExchg[1].equals(AppConstants.EXCHANGECODE_PH) ? this.getLayoutInflater().inflate(R.layout.time_salesph_row, null) : this.getLayoutInflater().inflate(R.layout.timesales_row, null);
					rowView					= this.getLayoutInflater().inflate(R.layout.timesales_row, null);
						
					/* Commented out for TCMobile 2.0 enhancements, no need to set alternate color background
					if ( symbolExchg[1].equals(AppConstants.EXCHANGECODE_PH) ){
						LinearLayout rowLayoutRL	= (LinearLayout) rowView.findViewById(R.id.relativeLayoutTS_PH);
						rowLayoutRL.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);
					}else{
						LinearLayout rowLayoutLL 	= (LinearLayout) rowView.findViewById(R.id.salesLayout);
						rowLayoutLL.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);
					}
					*/
						
					/** TIME - START **/
					final TextView timeLabel 	= (TextView) rowView.findViewById(R.id.timeLabel);
					String convertDate 			= FormatUtil.formatDateString(timeSalesList.getTime(), "HH:mm:ss");
					timeLabel.setText(convertDate);
					//timeLabels.add(timeLabel);
					
					/** TRADED AT - START , changed label to "PI"-purchase indicator, 20161116 **/
					//Added by Thinzar@20140912, Change_Request-20140901, ReqNo.4 - START
					if(timeSalesList.getTradedat() == 'M' || timeSalesList.getTradedat() == 'm')
						timeSalesList.setTradedat('b');
					else if(timeSalesList.getTradedat() == 'N' || timeSalesList.getTradedat() == 'n')
						timeSalesList.setTradedat('s');
					else if(timeSalesList.getTradedat() == 'O' || timeSalesList.getTradedat() == 'o')
						timeSalesList.setTradedat('-');
					//Added by Thinzar@20140912, Change_Request-20140901, ReqNo.4 - END
					final TextView tradedatLabel = (TextView) rowView.findViewById(R.id.tradedatLabel);
					String convertTrade			 = String.valueOf( timeSalesList.getTradedat());
					
					tradedatLabel.setText(convertTrade);
						
					if( symbolExchg[1].equals(AppConstants.EXCHANGECODE_PH) ){
						if(timeSalesList.getTradedat() == 'b')
							tradedatLabel.setTextColor(getResources().getColor(R.color.timeSales_Green));
						else if(timeSalesList.getTradedat() == 's')
							tradedatLabel.setTextColor(getResources().getColor(R.color.timeSales_Red));
						else
							tradedatLabel.setTextColor(getResources().getColor(R.color.timeSales_Gold));
					}else{
						if(timeSalesList.getTradedat() == 'b')
							tradedatLabel.setTextColor(getResources().getColor(R.color.timeSales_Red));
						else if(timeSalesList.getTradedat() == 's')
							tradedatLabel.setTextColor(getResources().getColor(R.color.timeSales_Green));
						else
							tradedatLabel.setTextColor(Color.BLACK);
					}
					
					//tradedatLabels.add(tradedatLabel);
					
					/** PRICE - START **/
					final TextView priceLabel	 = (TextView) rowView.findViewById(R.id.priceLabel);
					/* Commented out and edited by Thinzar, Change_Request-20141101, ReqNo 
					if( symbolExchg[1].equals("JK") || symbolExchg[1].equals("JKD") )
						priceLabel.setText(FormatUtil.formatPrice2( timeSalesList.getPrice() ));
					else
						priceLabel.setText(FormatUtil.formatPrice( timeSalesList.getPrice() ));
					*/
					priceLabel.setText(FormatUtil.formatPrice(timeSalesList.getPrice(), "-", symbolExchg[1]));
					
					if( symbolExchg[1].equals(AppConstants.EXCHANGECODE_PH) ){
						if(timeSalesList.getTradedat() == 'b')
							priceLabel.setTextColor(getResources().getColor(R.color.timeSales_Green));
						else if(timeSalesList.getTradedat() == 's')
							priceLabel.setTextColor(getResources().getColor(R.color.timeSales_Red));
						else
							priceLabel.setTextColor(getResources().getColor(R.color.timeSales_Gold));
					}else{
						if(timeSalesList.getPrice() < stockInfo.getLACP())
							priceLabel.setTextColor(getResources().getColor(R.color.timeSales_Red));
						else if(timeSalesList.getPrice() > stockInfo.getLACP())
							priceLabel.setTextColor(getResources().getColor(R.color.timeSales_Green));
						else
							priceLabel.setTextColor(Color.BLACK);
					}
					//priceLabels.add(priceLabel);
					
					/** VOLUME - START **/
					final TextView volumeLabel	 = (TextView) rowView.findViewById(R.id.volumeLabel);
					volumeLabel.setText(FormatUtil.formatLong(timeSalesList.getVolume(AppConstants.brokerConfigBean.getIntViewQtyMeasurement(), stockInfo.getSharePerLot())));	//Edited by Thinzar, Change_Request-20150401, ReqNo.2
					//volumeLabels.add(volumeLabel);
					
					//Added by Thinzar, Change_Request-20161101, ReqNo.1 - START
					/** BBH & SBH - START **/
					if(isExchangePH){
						final TextView txtBBH		= (TextView) rowView.findViewById(R.id.BBHLabel);
						txtBBH.setVisibility(View.VISIBLE);
						txtBBH.setText(timeSalesList.getBuyerBrokerHouse());
						//BBHLabels.add(txtBBH);

						final TextView txtSBH		= (TextView) rowView.findViewById(R.id.SBHLabel);
						txtSBH.setVisibility(View.VISIBLE);
						txtSBH.setText(timeSalesList.getSellerBrokerHouse());
						//SBHLabels.add(txtSBH);
					}
					//Added by Thinzar, Change_Request-20161101, ReqNo.1 - END
					
					tableRow.addView(rowView);
					tableLayoutTimeSales.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
					id++;
				}
				
				if(releasePullToRefresh){
					refreshV.setRefreshListener(new RefreshListener(){
						public void onRefresh() {
							if(hasLatestRecord){
								if (refreshV.mRefreshState == 4){
									if(idleThread != null && ! isShowingIdleAlert)
										idleThread.resetExpiredTime();
										progressDialog = ProgressDialog.show(linear.getContext(), "", getResources().getString(R.string.dialog_progress_loading));
										
										new TimeSalesTask(RecordAge.NEW).execute(view);
										refreshV.finishRefresh();
								}
							}
							if(refreshV.mfooterRefreshState == 4){
								if(idleThread != null && ! isShowingIdleAlert)
									idleThread.resetExpiredTime();
									progressDialog = ProgressDialog.show(linear.getContext(), "", getResources().getString(R.string.dialog_progress_loading));
									
									new TimeSalesTask(RecordAge.OLD).execute(view);
									refreshV.finishRefresh();
									sv.post(new Runnable() {
									    public void run() {
									        sv.fullScroll(View.FOCUS_DOWN);
									    }
									});
							}
							
						}
					});
				}
				
				// Tap to Refresh
				if(lstTimeSales.isEmpty()){
					layoutTapRefresh.setVisibility(View.VISIBLE);
					layoutTapRefresh.setBackgroundResource(R.color.trans);
					
					layoutTapRefresh.setOnClickListener(new OnClickListener() {
			            @Override
			            public void onClick(View v) {
			                // TODO Auto-generated method stub
			                linearProgress	= (LinearLayout) findViewById(R.id.linearProgressBar);
							linearProgress.setVisibility(View.VISIBLE);
							
			                new TimeSalesTask(RecordAge.NEW).execute(view);
			            }
			        });
					
				}else{
					layoutTapRefresh.setVisibility(View.GONE);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(symbolExchg != null)
					symbolExchg = null;
				
				if(tableLayoutTimeSales != null){
					tableLayoutTimeSales.destroyDrawingCache();
					tableLayoutTimeSales = null;
				}
				id = 0;
			}
	}
	// Sonia@20130702 - Change_Request-20130225, ReqNo.5 - End
	
	// Sonia@20130702 - Change_Request-20130225, ReqNo.5 - Start
	private void prepareBusinessDoneView(final View view, final List<BusinessDone> lstBusinessDone) {
		
		TableLayout tableLayoutBusinessDone	= null;
		
		int id = 0;
		
		try{
			if(view == null)
				return;
				
				LinearLayout noRecord 	= (LinearLayout) view.findViewById(R.id.noRecords);
				
				tableLayoutBusinessDone	= (TableLayout) view.findViewById(R.id.doneTable);
				tableLayoutBusinessDone.removeAllViews();
				
				bd_priceLabels			= new ArrayList<TextView>();
				bVolumeLabels			= new ArrayList<TextView>();
				bVolume_PercentLabels	= new ArrayList<TextView>();
				sVolumeLabels			= new ArrayList<TextView>();
				sVolume_PercentLabels	= new ArrayList<TextView>();
				totalVolumeLabels		= new ArrayList<TextView>();
				totalValueLabels		= new ArrayList<TextView>();
				
				for (Iterator<BusinessDone> itr = lstBusinessDone.iterator(); itr.hasNext();) {
					final BusinessDone businessDoneList = itr.next();
					
					View rowView		= null;
					TableRow tableRow 	= null;
					
						tableRow 				= new TableRow(this);
						tableRow.setId(id);
						tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
						tableRow.setWeightSum(1);
						
						rowView					= this.getLayoutInflater().inflate(R.layout.businessdone_row, null);
						/* Commented out for TCMobile 2.0 enhancements
						LinearLayout rowLayout 	= (LinearLayout) rowView.findViewById(R.id.businessDoneLayout);
						rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);
						*/
					/** PRICE - START **/
					final TextView bd_priceLabel	 = (TextView) rowView.findViewById(R.id.priceLabel);
					bd_priceLabel.setText(FormatUtil.formatPrice( businessDoneList.getPrice() ));
					
						if(businessDoneList.getPrice() < stockInfo.getLACP())
							bd_priceLabel.setTextColor(getResources().getColor(R.color.timeSales_Red));
						else if(businessDoneList.getPrice() > stockInfo.getLACP())
							bd_priceLabel.setTextColor(getResources().getColor(R.color.timeSales_Green));
						else
							bd_priceLabel.setTextColor(Color.BLACK);
						
						bd_priceLabels.add(bd_priceLabel);
					/** PRICE - END **/
					
					/** B VOLUME - START **/
					final TextView bVolumeLabel	 = (TextView) rowView.findViewById(R.id.bVolLabel);
					long b_vol = businessDoneList.getBVolume(AppConstants.brokerConfigBean.getIntViewQtyMeasurement(), stockInfo.getSharePerLot());		//Edited by Thinzar, Change_Request-20150401, ReqNo.2
						bVolumeLabel.setText(FormatUtil.formatLong( b_vol ));
						bVolumeLabels.add(bVolumeLabel);
					/** B VOLUME - END **/
						
					/** B VOLUME % - START **/
					final TextView bVolume_PercentLabel	 = (TextView) rowView.findViewById(R.id.bVolPercentLabel);
						long total_vol = businessDoneList.getTotalVolume(AppConstants.brokerConfigBean.getIntViewQtyMeasurement(), stockInfo.getSharePerLot());		//Edited by Thinzar, Change_Request-20150401, ReqNo.2
						double totalBVol = (double)b_vol / (double)total_vol;
						double bVolP = totalBVol * 100;
						bVolume_PercentLabel.setText(FormatUtil.formatPercent( bVolP ));
						bVolume_PercentLabels.add(bVolume_PercentLabel);
					/** B VOLUME % - END **/
					
					/** S VOLUME - START **/
					final TextView sVolumeLabel	 = (TextView) rowView.findViewById(R.id.sVolLabel);
					long s_vol = businessDoneList.getSVolume(AppConstants.brokerConfigBean.getIntViewQtyMeasurement(), stockInfo.getSharePerLot());		//Edited by Thinzar, Change_Request-20150401, ReqNo.2
						sVolumeLabel.setText(FormatUtil.formatLong( s_vol ));
						sVolumeLabels.add(sVolumeLabel);
					/** S VOLUME - END **/
					
					/** S VOLUME % - START **/
					final TextView sVolume_PercentLabel	 = (TextView) rowView.findViewById(R.id.sVolPercentLabel);
						double totalSVol = (double)s_vol / (double)total_vol;
						double sVolP = totalSVol * 100;
						sVolume_PercentLabel.setText(FormatUtil.formatPercent( sVolP ));
						sVolume_PercentLabels.add(sVolume_PercentLabel);
					/** S VOLUME % - END **/
					
					/** TOTAL VOLUME - START **/
					final TextView totalVolumeLabel	 = (TextView) rowView.findViewById(R.id.totalVolumeLabel);
						totalVolumeLabel.setText(FormatUtil.formatLong( total_vol ));
						totalVolumeLabels.add(totalVolumeLabel);
					/** TOTAL VOLUME - END **/
					
					/** TOTAL VALUE - START **/
					final TextView totalValueLabel	 = (TextView) rowView.findViewById(R.id.totalValueLabel);
						totalValueLabel.setText(FormatUtil.formatLong( businessDoneList.getTotalValue() ));
						totalValueLabels.add(totalValueLabel);
					/** TOTAL VALUE - END **/
					
					tableRow.addView(rowView);
					tableLayoutBusinessDone.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
					id++;
				}
				
				if(lstBusinessDone.isEmpty()){
					noRecord.setVisibility(View.VISIBLE);
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				
				if(tableLayoutBusinessDone != null){
					tableLayoutBusinessDone.destroyDrawingCache();
					tableLayoutBusinessDone = null;
				}
				id = 0;
			}
	}
	// Sonia@20130702 - Change_Request-20130225, ReqNo.5 - End
		
	// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12 - START
	private String getCumulativeBidAskQty(List<MarketDepth> marketDepths, String strSelectedBidAskPrice, int intBidOrAsk) {
		float fltSelectedBidAskPrice= 0f;
		long lngCumulativeBidAskQty = 0;
		boolean isGetOwn 			= false;
		try {
			if(strSelectedBidAskPrice == null)
				return "0";
			
			strSelectedBidAskPrice	= strSelectedBidAskPrice.replaceAll(",", "").trim();

			if( strSelectedBidAskPrice.equalsIgnoreCase("MP") || strSelectedBidAskPrice.equalsIgnoreCase("MO") )
				isGetOwn = true;
			else
				fltSelectedBidAskPrice = Float.parseFloat(strSelectedBidAskPrice);

			for (Iterator<MarketDepth> itr = marketDepths.iterator(); itr.hasNext();) {
				MarketDepth marketDepth = itr.next();
				float fltLoopPrice = intBidOrAsk == INT_BID_RELATED ? marketDepth.getBidPrice() : marketDepth.getAskPrice();
				long lngLoopSize = intBidOrAsk == INT_BID_RELATED ? marketDepth.getBidSize() : marketDepth.getAskSize();
				if (isGetOwn) {
					lngCumulativeBidAskQty = lngLoopSize;
					break;
				}else{
					if( (intBidOrAsk == INT_ASK_RELATED && ( fltLoopPrice <= fltSelectedBidAskPrice || AppConstants.isMarketPriceStock(fltLoopPrice) ) )
							|| ( intBidOrAsk == INT_BID_RELATED && ( fltLoopPrice >= fltSelectedBidAskPrice || AppConstants.isMarketPriceStock(fltLoopPrice) ) ) )
						lngCumulativeBidAskQty += lngLoopSize;
					else
						break;
				}
			}
			return String.valueOf(lngCumulativeBidAskQty);
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		} finally {
			fltSelectedBidAskPrice	= 0;
			lngCumulativeBidAskQty 	= 0;
			isGetOwn 				= false;
		}

	}

	// Added by Mary@20120830 - Change_Request-20120719, ReqNo.12 - END

	private void prepareNewsUpdateView(View view) {
		if(view == null)
			return;

		/* Mary@20121102 - Fixes_Request-20121102, ReqNo.1
		if(newsUpdates != null) {
		*/
		if(newsUpdates != null && newsUpdates.size() > 0){
			noResultFound.setVisibility(View.GONE);
		} else {
			noResultFound.setVisibility(View.VISIBLE);
			return;
		}

		newsScrollView 			= (ScrollView) view.findViewById(R.id.newsScrollView);
		newsScrollView.setOnTouchListener(this);

		TableLayout tableLayout = (TableLayout) view.findViewById(R.id.newsTable);
		tableLayout.removeAllViews();

		int id 					= 0;
		/* Mary@20130620 - To be configurable according to broker
		if( !AppConstants.isJavaQC ){
		*/
		if( ! AppConstants.brokerConfigBean.isConnectJavaQC() ){
			/* Mary@20121102 - Fixes_Request-20121102, ReqNo.1
			if (newsUpdates != null) {
			*/
				for (Iterator<StockNews> itr = newsUpdates.iterator(); itr.hasNext();) {
					final StockNews news 	= itr.next();
					
					TableRow tableRow 		= new TableRow(this);
					tableRow.setId(id);
					tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
					tableRow.setWeightSum(1);
					
					View rowView 			= this.getLayoutInflater().inflate(R.layout.news_update_row, null);
					
					TextView newsTitleLabel = (TextView) rowView.findViewById(R.id.newsTitleLabel);
					newsTitleLabel.setSingleLine(false);
					newsTitleLabel.setEllipsize(TruncateAt.END);
					
					//Added by Thinzar@20150225, Fixes_Request-20150201, ReqNo. 2 - START
					//special case for CIMB_SG, if news title contains <origin Href="quoteref">dbsm.si</origin>, strip the HTML tags and convert the stock symbol to upper case
					String newsTitle	= news.getNewsTitle();
					if(newsTitle.contains("</origin>") || newsTitle.contains("</Origin>")){
						newsTitle	= newsTitle.replaceAll("<[^>]*>", "");		//.toUpperCase();
					}
					newsTitleLabel.setText(newsTitle);
					//Added by Thinzar@20150225, Fixes_Request-20150201, ReqNo. 2 - END
					
					TextView symbolLabel 	= (TextView) rowView.findViewById(R.id.newsSymbolLabel);
					/* Mary@20130910 - Change_Request-20130724, ReqNo.13
					symbolLabel.setText(news.getSymbolCode());
					*/
					symbolLabel.setText( news.getSymbolName() == null ?  news.getSymbolCode() : news.getSymbolName() );
					
					TextView newsDateLabel = (TextView) rowView.findViewById(R.id.newsDateLabel);
					if (news.getNewsDate() != null)
						newsDateLabel.setText(FormatUtil.formatDateString(news.getNewsDate(), FormatUtil.NEWSDATE_FORMAT_2));

					tableRow.addView(rowView);
					tableRow.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
							if(idleThread != null && ! isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
							
							ScreenReceiver.wasScreenOn 	= false;
							Intent intent 				= new Intent();
							intent.putExtra(ParamConstants.NEWSID_TAG, news.getNewsId());
							intent.setClass(StockActivity.this,	NewsActivity.class);
							startActivity(intent);
						}
					});

					LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.newsUpdateLayout);

					/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
					if( (id + 1) % 2 != 0 )
						rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
					else
						rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
					*/
					rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light );

					tableLayout.addView(tableRow, new TableLayout.LayoutParams(	TableRow.LayoutParams.MATCH_PARENT,	TableRow.LayoutParams.WRAP_CONTENT));
					id++;
				}
			}else{
			/* Mary@20121102 - Fixes_Request-20121102, ReqNo.1
			if(newsUpdates.size() != 0){
			*/
				for(Iterator<StockNews> itr = newsUpdates.iterator(); itr.hasNext();){
					final StockNews news 	= itr.next();

					TableRow tableRow 		= new TableRow(this);
					tableRow.setId(id);
					tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
					tableRow.setWeightSum(1);
					View rowView 			= this.getLayoutInflater().inflate(R.layout.news_update_row, null);
					
					TextView newsTitleLabel = (TextView) rowView.findViewById(R.id.newsTitleLabel);
					//Added by Thinzar@20150225, Fixes_Request-20150201, ReqNo. 2 - START
					//special case for CIMB_SG, if news title contains <origin Href="quoteref">dbsm.si</origin>, strip the HTML tags and convert the stock symbol to upper case
					String newsTitle	= news.getNewsTitle();
					if(newsTitle.contains("</origin>") || newsTitle.contains("</Origin>")){
						newsTitle	= newsTitle.replaceAll("<[^>]*>", "");		//.toUpperCase();
					}
					newsTitleLabel.setText(newsTitle);
					//Added by Thinzar@20150225, Fixes_Request-20150201, ReqNo. 2 - END
					
					TextView symbolLabel 	= (TextView) rowView.findViewById(R.id.newsSymbolLabel);
					/* Mary@20130910 - Change_Request-20130724, ReqNo.13
					symbolLabel.setText(news.getSymbolCode());
					*/
					symbolLabel.setText( news.getSymbolName() == null ?  news.getSymbolCode() : news.getSymbolName() );
					
					TextView newsDateLabel	= (TextView) rowView.findViewById(R.id.newsDateLabel);
					if (news.getNewsDate() != null)
						newsDateLabel.setText(FormatUtil.formatDateString(news.getNewsDate(), FormatUtil.NEWSDATE_FORMAT_2));

					tableRow.addView(rowView);
					tableRow.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view){
							// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
							if(idleThread != null && ! isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
							
							ScreenReceiver.wasScreenOn = false;
							Intent intent = new Intent();
							intent.putExtra(ParamConstants.NEWSID_TAG, news.getNewsId());
							intent.setClass(StockActivity.this, NewsActivity.class);
							startActivity(intent);
						}
					});

					LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.newsUpdateLayout);

					/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
					if( (id + 1) % 2 != 0 )
						rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
					else
						rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
					*/
					rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light );

					tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
					id++;
				}
			/* Mary@20121102 - Fixes_Request-20121102, ReqNo.1
			}else{
				TableRow tableRow = new TableRow(this);
				tableRow.setId(3);
				tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
				tableRow.setWeightSum(1);
				View rowView = this.getLayoutInflater().inflate(R.layout.news_update_row, null);
				TextView newsTitleLabel = (TextView) rowView.findViewById(R.id.newsTitleLabel);
				newsTitleLabel.setText("No News available for this symbol");

				TextView symbolLabel = (TextView) rowView.findViewById(R.id.newsSymbolLabel);
				symbolLabel.setText("");
				TextView newsDateLabel = (TextView) rowView.findViewById(R.id.newsDateLabel);
				newsDateLabel.setText("");

				tableRow.addView(rowView);

				LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.newsUpdateLayout);

				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				if( (id + 1) % 2 != 0 )
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
				else
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
				/
				rowLayout.setBackgroundResource( ( (id + 1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light );

				tableLayout.addView(tableRow, new TableLayout.LayoutParams(	TableRow.LayoutParams.MATCH_PARENT,	TableRow.LayoutParams.WRAP_CONTENT));
			}
			*/
		}
	}

	private void prepareStockChartView(final View view){
		if (view == null)
			return;
		linearLayoutLabel =(LinearLayout)view.findViewById(R.id.linearLayout2);

		final TextView oneDayLabel 		= (TextView) view.findViewById(R.id.oneDayLabel);
		final TextView oneMonthLabel 	= (TextView) view.findViewById(R.id.oneMonthLabel);
		final TextView threeMonthLabel 	= (TextView) view.findViewById(R.id.threeMonthLabel);
		final TextView sixMonthLabel 	= (TextView) view.findViewById(R.id.sixMonthLabel);
		final TextView oneYearLabel 	= (TextView) view.findViewById(R.id.oneYearLabel);
		final TextView twoYearLabel 	= (TextView) view.findViewById(R.id.twoYearLabel);

		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.3 - START
		if (chartDuration == AppConstants.CHART_DURATION_1_DAY) {
			oneDayLabel.setBackgroundResource(LayoutSettings.selectedEdge);
			oneMonthLabel.setBackgroundResource(0);
			threeMonthLabel.setBackgroundResource(0);
			sixMonthLabel.setBackgroundResource(0);
			oneYearLabel.setBackgroundResource(0);
			twoYearLabel.setBackgroundResource(0);
		} else if (chartDuration == AppConstants.CHART_DURATION_1_MONTH) {
			oneDayLabel.setBackgroundResource(0);
			oneMonthLabel.setBackgroundResource(LayoutSettings.selectedEdge);
			threeMonthLabel.setBackgroundResource(0);
			sixMonthLabel.setBackgroundResource(0);
			oneYearLabel.setBackgroundResource(0);
			twoYearLabel.setBackgroundResource(0);
		} else if (chartDuration == AppConstants.CHART_DURATION_3_MONTHS) {
			oneDayLabel.setBackgroundResource(0);
			oneMonthLabel.setBackgroundResource(0);
			threeMonthLabel.setBackgroundResource(LayoutSettings.selectedEdge);
			sixMonthLabel.setBackgroundResource(0);
			oneYearLabel.setBackgroundResource(0);
			twoYearLabel.setBackgroundResource(0);
		} else if (chartDuration == AppConstants.CHART_DURATION_6_MONTHS) {
			oneDayLabel.setBackgroundResource(0);
			oneMonthLabel.setBackgroundResource(0);
			threeMonthLabel.setBackgroundResource(0);
			sixMonthLabel.setBackgroundResource(LayoutSettings.selectedEdge);
			oneYearLabel.setBackgroundResource(0);
			twoYearLabel.setBackgroundResource(0);
		} else if (chartDuration == AppConstants.CHART_DURATION_1_YEAR) {
			oneDayLabel.setBackgroundResource(0);
			oneMonthLabel.setBackgroundResource(0);
			threeMonthLabel.setBackgroundResource(0);
			sixMonthLabel.setBackgroundResource(0);
			oneYearLabel.setBackgroundResource(LayoutSettings.selectedEdge);
			twoYearLabel.setBackgroundResource(0);
		} else if (chartDuration == AppConstants.CHART_DURATION_2_YEARS) {
			oneDayLabel.setBackgroundResource(0);
			oneMonthLabel.setBackgroundResource(0);
			threeMonthLabel.setBackgroundResource(0);
			sixMonthLabel.setBackgroundResource(0);
			oneYearLabel.setBackgroundResource(0);
			twoYearLabel.setBackgroundResource(LayoutSettings.selectedEdge);
		}
		*/
		oneDayLabel.setBackgroundResource(0);
		oneMonthLabel.setBackgroundResource(0);
		threeMonthLabel.setBackgroundResource(0);
		sixMonthLabel.setBackgroundResource(0);
		oneYearLabel.setBackgroundResource(0);
		twoYearLabel.setBackgroundResource(0);
		switch(chartDuration){
			case AppConstants.CHART_DURATION_1_DAY :
				oneDayLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				break;
			
			case AppConstants.CHART_DURATION_1_MONTH :
				oneMonthLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				break;
			
			case AppConstants.CHART_DURATION_3_MONTHS :
				threeMonthLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				break;
			
			case AppConstants.CHART_DURATION_6_MONTHS :
				sixMonthLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				break;
			
			case AppConstants.CHART_DURATION_1_YEAR :
				oneYearLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				break;
				
			case AppConstants.CHART_DURATION_2_YEARS :
				twoYearLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				break;
		}
		// Mary@20121001 - Fixes_Request-20120815, ReqNo.3 - END

		oneDayLabel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
				
				
				if( noChartFound.isShown() )
					noChartFound.setVisibility(View.GONE);
				/* Fixes_Request_20170701, ReqNo.7
				chartView.setScaleType(ScaleType.CENTER);
				chartView.setImageResource(LayoutSettings.loading_animate);
				loadAnimation = (AnimationDrawable) chartView.getDrawable();
				loadAnimation.start();
				*/
				chartView.setVisibility(View.GONE);
				chartProgressBar.setVisibility(View.VISIBLE);

				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
				ChartType = CommandConstants.CHART_TYPE_INTRADAY;
				chartDuration = AppConstants.CHART_DURATION_1_DAY;

				displayChartEvent2(parameters, ChartType);
				oneDayLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				oneMonthLabel.setBackgroundResource(0);
				threeMonthLabel.setBackgroundResource(0);
				sixMonthLabel.setBackgroundResource(0);
				oneYearLabel.setBackgroundResource(0);
				twoYearLabel.setBackgroundResource(0);
			}
		});

		oneMonthLabel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
				
				if( noChartFound.isShown() )
					noChartFound.setVisibility(View.GONE);
					
				/* Fixes_Request_20170701, ReqNo.7
				chartView.setScaleType(ScaleType.CENTER);
				chartView.setImageResource(LayoutSettings.loading_animate);
				loadAnimation = (AnimationDrawable) chartView.getDrawable();
				loadAnimation.start();
				*/
				chartView.setVisibility(View.GONE);
				chartProgressBar.setVisibility(View.VISIBLE);

				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
				parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_1MONTH);
				ChartType = CommandConstants.CHART_TYPE_HISTORICAL;
				chartDuration = AppConstants.CHART_DURATION_1_MONTH;

				displayChartEvent2(parameters, ChartType);
				oneDayLabel.setBackgroundResource(0);
				oneMonthLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				threeMonthLabel.setBackgroundResource(0);
				sixMonthLabel.setBackgroundResource(0);
				oneYearLabel.setBackgroundResource(0);
				twoYearLabel.setBackgroundResource(0);
			}
		});

		threeMonthLabel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
				
				
				if( noChartFound.isShown() )
					noChartFound.setVisibility(View.GONE);
				
				/* Fixes_Request_20170701, ReqNo.7
				chartView.setScaleType(ScaleType.CENTER);
				chartView.setImageResource(LayoutSettings.loading_animate);
				loadAnimation = (AnimationDrawable) chartView.getDrawable();
				loadAnimation.start();
				*/
				chartView.setVisibility(View.GONE);
				chartProgressBar.setVisibility(View.VISIBLE);

				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
				parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_3MONTHS);
				ChartType = CommandConstants.CHART_TYPE_HISTORICAL;
				chartDuration = AppConstants.CHART_DURATION_3_MONTHS;

				displayChartEvent2(parameters, ChartType);
				oneDayLabel.setBackgroundResource(0);
				oneMonthLabel.setBackgroundResource(0);
				threeMonthLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				sixMonthLabel.setBackgroundResource(0);
				oneYearLabel.setBackgroundResource(0);
				twoYearLabel.setBackgroundResource(0);
			}
		});

		sixMonthLabel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
				
				if( noChartFound.isShown() )
					noChartFound.setVisibility(View.GONE);
				
				/* Fixes_Request_20170701, ReqNo.7
				chartView.setScaleType(ScaleType.CENTER);
				chartView.setImageResource(LayoutSettings.loading_animate);
				loadAnimation = (AnimationDrawable) chartView.getDrawable();
				loadAnimation.start();
				*/
				chartView.setVisibility(View.GONE);
				chartProgressBar.setVisibility(View.VISIBLE);

				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
				parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_6MONTHS);
				ChartType = CommandConstants.CHART_TYPE_HISTORICAL;
				chartDuration = AppConstants.CHART_DURATION_6_MONTHS;

				displayChartEvent2(parameters, ChartType);
				oneDayLabel.setBackgroundResource(0);
				oneMonthLabel.setBackgroundResource(0);
				threeMonthLabel.setBackgroundResource(0);
				sixMonthLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				oneYearLabel.setBackgroundResource(0);
				twoYearLabel.setBackgroundResource(0);
			}
		});

		oneYearLabel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
				
				if( noChartFound.isShown() )
					noChartFound.setVisibility(View.GONE);
				
				/* Fixes_Request_20170701, ReqNo.7
				chartView.setScaleType(ScaleType.CENTER);
				chartView.setImageResource(LayoutSettings.loading_animate);
				loadAnimation = (AnimationDrawable) chartView.getDrawable();
				loadAnimation.start();
				*/
				chartView.setVisibility(View.GONE);
				chartProgressBar.setVisibility(View.VISIBLE);

				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
				parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_12MONTHS);
				ChartType = CommandConstants.CHART_TYPE_HISTORICAL;
				chartDuration = AppConstants.CHART_DURATION_1_YEAR;

				displayChartEvent2(parameters, ChartType);
				oneDayLabel.setBackgroundResource(0);
				oneMonthLabel.setBackgroundResource(0);
				threeMonthLabel.setBackgroundResource(0);
				sixMonthLabel.setBackgroundResource(0);
				oneYearLabel.setBackgroundResource(LayoutSettings.selectedEdge);
				twoYearLabel.setBackgroundResource(0);
			}
		});

		twoYearLabel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
				
				if( noChartFound.isShown() )
					noChartFound.setVisibility(View.GONE);
				
				/* Fixes_Request_20170701, ReqNo.7
				chartView.setScaleType(ScaleType.CENTER);
				chartView.setImageResource(LayoutSettings.loading_animate);
				loadAnimation = (AnimationDrawable) chartView.getDrawable();
				loadAnimation.start();
				*/
				chartView.setVisibility(View.GONE);
				chartProgressBar.setVisibility(View.VISIBLE);

				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put(ParamConstants.SYMBOLCODE_TAG, symbolCode);
				parameters.put(ParamConstants.CHART_DURATION_TAG, CommandConstants.DAYS_CHART_24MONTHS);
				ChartType = CommandConstants.CHART_TYPE_HISTORICAL;
				chartDuration = AppConstants.CHART_DURATION_2_YEARS;

				displayChartEvent2(parameters, ChartType);
				oneDayLabel.setBackgroundResource(0);
				oneMonthLabel.setBackgroundResource(0);
				threeMonthLabel.setBackgroundResource(0);
				sixMonthLabel.setBackgroundResource(0);
				oneYearLabel.setBackgroundResource(0);
				twoYearLabel.setBackgroundResource(LayoutSettings.selectedEdge);

			}
		});

	}

	private class DisplayChartTask extends AsyncTask<Map<String, Object>, Void, Bitmap> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		private Exception exception 									= null;
		private AsyncTask<Map<String, Object>, Void, Bitmap> updateTask = null;
		
		@Override
		protected void onPreExecute() {
			updateTask = this;
			
			// Mary@20120803 - move UI statement from doInBackground to onPreExecute to avoid CalledFromWrongThreadException
			/* Fixes_Request_20170701, ReqNo.7
			if (chartView.getDrawable() != null) {

				//noChartFound.setVisibility(View.GONE);	//visibility is gone at this point, throws CalledFromWrongThreadException 
							
				loadAnimation	= (AnimationDrawable) chartView.getDrawable();
				loadAnimation.start();
			}
			 */
			chartView.setVisibility(View.GONE);
			chartProgressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Bitmap doInBackground(Map<String, Object>... map) {
			Bitmap bitmap = null;

			try {

				String imageUrl = QcHttpConnectUtil.chartImage(map[0],
						ChartType);

				if (imageUrl == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_PATH);

				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				while (intRetryCtr < AppConstants.intMaxRetry && imageUrl == null) {
					intRetryCtr++;
					imageUrl = QcHttpConnectUtil.chartImage(map[0], ChartType);
				}
				intRetryCtr = 0;

				if(imageUrl.trim().length() > 0){
					// NOTE : No retry perform as there is possible that there are null bitmap loaded
					try {
						bitmap = QcHttpConnectUtil.loadBitmap(imageUrl);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			return bitmap;
		}

		protected void onPostExecute(Bitmap bitmap) {
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				//loadAnimation.stop();
				chartProgressBar.setVisibility(View.GONE);

				if (exception != null) {
					alertException.setMessage(exception.getMessage());
					alertException.setButton(getResources().getString(R.string.dialog_button_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						}
					);
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! StockActivity.this.isFinishing() )
						alertException.show();
					
					exception = null;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

				if (bitmap != null) {
					chartView.setVisibility(View.VISIBLE);
					chartView.setScaleType(ScaleType.FIT_XY);
					chartView.setImageResource(0);
					chartView.setImageBitmap(bitmap);
				} else {
					//chartView.setImageResource(0);
					chartView.setVisibility(View.GONE);
					noChartFound.setVisibility(View.VISIBLE);
				}

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	private void displayChartEvent2(final Map<String, Object> parameters, final int chartType) {
		ChartType = chartType;
		new DisplayChartTask().execute(parameters);
	}
	
	// Added by Mary@20130620 - Change_Request-20130424, ReqNo.9 - START
	private boolean isShowFundamentalCapitalIQPage(Map<String, String> mapFCBitMode, String strQCExchgCode){
		try{
			String strFCBitMode	= mapFCBitMode.get(strQCExchgCode);
			//added Null checking, Thinzar@20141103
			if(strFCBitMode != null){
				for(int bitIdx=0; bitIdx<6; bitIdx++){
					if( SystemUtil.isBitOn_Base64(strFCBitMode, bitIdx) )
						return true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	// Added by Mary@20130620 - Change_Request-20130424, ReqNo.9 - END

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler() {
		public void handleMessage(Message message) {
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if (response.get(2).equals("1102")) {
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(StockActivity.this.mainMenu.isShowing()){
					StockActivity.this.mainMenu.hide();
				}

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(refreshThread != null)
					refreshThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				if(!isFinishing()){
					StockActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	// Added by Mary@20130828 - Fixes_Request-20130711, ReqNo.17 - START
	@Override
	public void watchlistSelectedEvent(Watchlist watchList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void watchlistUpdatedEvent(String watchlistName, String watchlistId, boolean isNew) {
		// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130604 - Change_Request-20130424, ReqNo.5 - END
		
		try{
			new executeWLCreate().execute(watchlistName);
			
			return;
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void watchlistDeletedEvent(String watchlistId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showKeyboardEvent() {
		((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
	}

	@Override
	public void hideKeyboardEvent(EditText editText) {
		((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}
	// Added by Mary@20130828 - Fixes_Request-20130711, ReqNo.17 - END

	// Added by Diyana, Change_Request_20160624, ReqNo.5 -START
	private class GetArchiveNews extends AsyncTask<View, Void, String> {
		View view;

		@Override
		protected String doInBackground(View... params) {
			view	= params[0];
			String dateFrom	= ArchiveNewsUtil.getOneMonthBeforeDate();
			String dateTo	= ArchiveNewsUtil.getCurrentDate();
			
			String newsList	= ArchiveNewsUtil.getArchiveNews(code, splitTrdExchgCode, dateFrom, dateTo);
			
			return newsList;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			linearProgress.setVisibility(View.GONE);

			//View view = mView.get(6);		//do not hardcode, the order of views will change over time
			newsScrollView = (ScrollView) view.findViewById(R.id.newsScrollView);
			noResultFound = (TextView) mView.get(6).findViewById(R.id.noResultsFoundView);

			if (result != null && result.length() > 0) {

				final ArrayList<StockNews> newsArray = new ArrayList<StockNews>();
				newsArray.addAll(ArchiveNewsUtil.convertArchiveNews(result));
				
				if (newsArray.size() > 0) {
					// newsScrollView.setOnTouchListener(getApplicationContext());
					noResultFound.setVisibility(View.GONE);
					TableLayout tableLayout = (TableLayout) view.findViewById(R.id.newsTable);
					tableLayout.removeAllViews();

					for (s = 0; s < newsArray.size(); s++) {
						LayoutInflater layoutInflate = (LayoutInflater) getApplicationContext()
								.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						
						View archiveView = layoutInflate.inflate(R.layout.news_archive, null);
						
						TextView tvNewsTitle = (TextView) archiveView.findViewById(R.id.tv_news_title);
						String title = newsArray.get(s).getNewsTitle();
						if(title.contains("</Origin>")|| title.contains("</origin>")){
							title	= title.replaceAll("<[^>]*>", "").toUpperCase();
						}
						tvNewsTitle.setText(title);
						
						TextView tvNewsDate = (TextView) archiveView.findViewById(R.id.tv_date);
						tvNewsDate.setText(FormatUtil.formatDateString(newsArray.get(s).getNewsDate(), FormatUtil.NEWSDATE_FORMAT_2));
						
						LinearLayout linearMain = (LinearLayout) archiveView.findViewById(R.id.linear_main);
						linearMain.setOnClickListener(new OnClickListener() {
							int k = s;

							@Override
							public void onClick(View v) {

								//String url = AppConstants.brokerConfigBean.getArchiveNewsUrl() +  + "EN";
								Intent intent = new Intent();
								intent.putExtra(ParamConstants.NEWSID_TAG, newsArray.get(k).getNewsId());
								intent.setClass(StockActivity.this, NewsActivity.class);
								startActivity(intent);

							}
						});
						
						tableLayout.addView(archiveView);
					}

				} else {
					noResultFound.setVisibility(View.VISIBLE);
				}
			} else {
				noResultFound.setVisibility(View.VISIBLE);
			}
		}
	}
	// Added by Diyana, Change_Request_20160624, ReqNo.2 -END
	
	//Change_Request-20170601, ReqNo.2
	private LinearLayout llNewsTitle;
	private LinearLayout llElasticNewsTitle;
	private LinearLayout llElastic;
	private TextView tvNewsTitle;
	private TextView tvReportsTitle;
	private ListView lvElasticNews;
	private String selectedNewsSource = AppConstants.REUTERS_NEWS_SOURCE;		//default TRKD
	
	private class GetElasticNews extends AsyncTask<View, Void, Exception> {
		private View view;
		private Exception e;
		private String response;
		
		private String mTarget 		= "2";
		private String mGuid		= "";
		private String mCategory 	= "";
		private int size;
		private int startIndex 	= 0;
		
		private String mKeyword;
		private String strFromDate;
		private String strToDate;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			noResultFound.setVisibility(View.GONE);
			linearProgress.setVisibility(View.VISIBLE);
			
			String strTodayDate 	= SystemUtil.getTodayDate("yyyy-MM-dd");
			final String FROM_TIME 	= " 00:00:00";
			final String TO_TIME 	= " 23:59:59";
			
			strToDate 	= strTodayDate + TO_TIME;
			strFromDate = SystemUtil.getNDaysFromDate(strTodayDate, -30);
			strFromDate = strFromDate + FROM_TIME;
			
			String[] symbolCodeParts = symbolCode.split("\\.");
			mKeyword 	= symbolCodeParts[0];
		}

		@Override
		protected Exception doInBackground(View... params) {
			view = params[0];
			
			try{
				//TODO: catch exception
				response = QcHttpConnectUtil.getV2News(mKeyword, mTarget, mGuid, mCategory, 
						selectedNewsSource, strFromDate, strToDate, size, startIndex);
				
			}catch(Exception ex){
				ex.printStackTrace();
				
				e = ex;
			}
			return e;
		}
		
		@Override
		protected void onPostExecute(Exception exception) {
			super.onPostExecute(exception);
			
			linearProgress.setVisibility(View.GONE);
			
			if(exception != null){
				
			}else{
				
				ArrayList<StockNews> stockNewsArrayList = QcMessageParser.parseStockNewsV2(response);
				prepareElasticNewsView(view, stockNewsArrayList);
				
			}
		}
	}
	
	private void prepareElasticNewsView(final View mView, final ArrayList<StockNews> stockNewsArrayList){
		
		tvNewsTitle		= (TextView)mView.findViewById(R.id.tv_news_tab);
		tvReportsTitle	= (TextView)mView.findViewById(R.id.tv_research_reports_tab);
		lvElasticNews	= (ListView) mView.findViewById(R.id.lv_elastic_news);
		
		newsScrollView 	= (ScrollView) mView.findViewById(R.id.newsScrollView);
		llNewsTitle		= (LinearLayout)mView.findViewById(R.id.ll_news_title);
		llElasticNewsTitle = (LinearLayout)mView.findViewById(R.id.ll_elastic_news_title);
		llElastic		= (LinearLayout) mView.findViewById(R.id.ll_elastic);
		
		llNewsTitle.setVisibility(View.GONE);
		llElasticNewsTitle.setVisibility(View.VISIBLE);
		
		newsScrollView.setVisibility(View.GONE);
		llElastic.setVisibility(View.VISIBLE);
		
		if(stockNewsArrayList.size() > 0){
			noResultFound.setVisibility(View.GONE);
			
			StockNewsAdapter newsAdapter = new StockNewsAdapter(StockActivity.this, stockNewsArrayList);
			lvElasticNews.setAdapter(newsAdapter);
			lvElasticNews.setVisibility(View.VISIBLE);
		}
		else{
			lvElasticNews.setVisibility(View.GONE);
			noResultFound.setVisibility(View.VISIBLE);
			
			if(selectedNewsSource.equalsIgnoreCase(AppConstants.REUTERS_NEWS_SOURCE))
				noResultFound.setText(getResources().getString(R.string.stock_no_news));
			else
				noResultFound.setText(getResources().getString(R.string.stock_no_research));
			
		}
		
		lvElasticNews.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long arg3) {
				StockNews mNews = stockNewsArrayList.get(position);
				
				Intent intent = new Intent();
				intent.setClass(StockActivity.this, ElasticNewsDetailsActivity.class);
				intent.putExtra(ParamConstants.NEWS_SOURCE_TAG, mNews.getNewsSource());
				intent.putExtra(ParamConstants.NEWSID_TAG, mNews.getNewsId());
				startActivity(intent);
			}
		});
		
		tvNewsTitle.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				tvNewsTitle.setBackgroundResource(LayoutSettings.selectedEdge);
				tvReportsTitle.setBackgroundResource(0);
				selectedNewsSource = AppConstants.REUTERS_NEWS_SOURCE;
				
				lvElasticNews.setVisibility(View.GONE);
				
				new GetElasticNews().execute(mView);
			}
		});
		
		tvReportsTitle.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				tvNewsTitle.setBackgroundResource(0);
				tvReportsTitle.setBackgroundResource(LayoutSettings.selectedEdge);
				
				selectedNewsSource = AppConstants.RESEARCH_REPORTS_SOURCE;
				
				lvElasticNews.setVisibility(View.GONE);
				new GetElasticNews().execute(mView);
			}
		});
	}
}