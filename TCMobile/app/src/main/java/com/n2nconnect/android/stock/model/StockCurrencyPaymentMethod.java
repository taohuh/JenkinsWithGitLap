package com.n2nconnect.android.stock.model;


public class StockCurrencyPaymentMethod {
	
	private String strPaymentMethod;
	private boolean allowedBuyAction;
	private boolean allowedSellAction;
	
	public String getStrPaymentMethod(){
		return this.strPaymentMethod;
	}
	public void setStrPaymentMethod(String strPaymentMethod){
		this.strPaymentMethod	= strPaymentMethod;
	}
	
	public boolean isAllowedBuyAction(){
		return this.allowedBuyAction;
	}
	public void setAllowedBuyAction(boolean isAllow){
		this.allowedBuyAction	= isAllow;
	}
	
	public boolean isAllowedSellAction(){
		return this.allowedSellAction;
	}
	public void setAllowedSellAction(boolean isAllow){
		this.allowedSellAction	= isAllow;
	}
}
