package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.List;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.StockNewsAdapter;
import com.n2nconnect.android.stock.model.NewsCategory;
import com.n2nconnect.android.stock.model.StockNews;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class ElasticNewsSearchActivity extends CustomWindow implements OnDoubleLoginListener{

	private DoubleLoginThread loginThread;

	private ListView lvElasticNews;
	private ProgressDialog dialog;
	private TextView tvSearchResultTitle;
	private TextView tvNewsNoResult;

	private String newsCategoryName;
	private String searchResultText;

	private int intRetryCtr	= 0;
	private int totalNewsSize;

	//search variables
	private final int REPORTS_SIZE = 0;
	private final int REUTER_NEWS_SIZE = 500;
	private int size = 100;
	private int startIndex = 0;
	private String mSource;
	private String mCategory;
	private String mKeyword;
	private String mGuid	= "";
	private String mTarget = "3";	//1 - News Content , 2 - News Header, 3 - News Content & New Header
	private String strFromDate;
	private String strToDate;
	private boolean isFromSearch = false;

	private ArrayList<StockNews> stockNewsArrayList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.title.setText(getResources().getString(R.string.elastic_news_module_title));
		this.icon.setImageResource(R.drawable.selector_back_button);		
		this.icon.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				ElasticNewsSearchActivity.this.finish();
			}
		});
		this.menuButton.setVisibility(View.INVISIBLE);
		
		setContentView(R.layout.activity_elastic_news_search);
		
		Bundle extras 		= this.getIntent().getExtras();
		if(extras != null){

			if(extras.containsKey(ParamConstants.NEWS_SOURCE_TAG))
				mSource	= extras.getString(ParamConstants.NEWS_SOURCE_TAG);
			
			if(extras.containsKey(ParamConstants.NEWS_CATEGORY_TAG))
				mCategory = extras.getString(ParamConstants.NEWS_CATEGORY_TAG);
			
			if(extras.containsKey(ParamConstants.NEWS_CATEGORY_NAME_TAG))
				newsCategoryName = extras.getString(ParamConstants.NEWS_CATEGORY_NAME_TAG);

			isFromSearch 	= extras.getBoolean(ParamConstants.NEWS_IS_SEARCH_TAG);
			if(isFromSearch){
				mKeyword 	= extras.getString(ParamConstants.NEWS_KEYWORD_TAG);
				mTarget 	= extras.getString(ParamConstants.NEWS_TARGET_TAG);
				strFromDate	= extras.getString(ParamConstants.NEWS_FROM_DATE_TAG);
				strToDate	= extras.getString(ParamConstants.NEWS_TO_DATE_TAG);

			}else{

				mKeyword 	= "";
				mTarget 	= "";
				strFromDate = "";
				strToDate 	= "";
			}
		}

		lvElasticNews		= (ListView)findViewById(R.id.lv_elastic_news);
		tvSearchResultTitle = (TextView)findViewById(R.id.tv_search_results_title);
		tvNewsNoResult 		= (TextView)findViewById(R.id.tv_news_noresult);
		dialog				= new ProgressDialog(this);
		stockNewsArrayList  = new ArrayList<StockNews>();

		if(!isFromSearch){
			String todayDateStr = SystemUtil.getTodayDate("yyyy-MM-dd");
			strToDate = todayDateStr + " 23:59:59";


			if(mSource.equalsIgnoreCase(AppConstants.RESEARCH_REPORTS_SOURCE)){
				strFromDate 	= SystemUtil.getNDaysFromDate(todayDateStr, -30) + " 00:00:00";
				size 			= REPORTS_SIZE;
				searchResultText = "Research Reports: " + newsCategoryName;

			}else{
				strFromDate 	= SystemUtil.getNDaysFromDate(todayDateStr, -1) + " 00:00:00";
				size = REUTER_NEWS_SIZE;
				searchResultText = newsCategoryName;
			}
		}else{

			searchResultText = "Search Results";
		}

		tvSearchResultTitle.setText(searchResultText);
		new FetchV2News().execute();
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (idleThread != null && idleThread.isPaused())
			idleThread.resumeRequest(false);
		
		loginThread = ((StockApplication) ElasticNewsSearchActivity.this.getApplication()).getDoubleLoginThread();
		
		if (loginThread != null && loginThread.isPaused()) {
			loginThread.setRegisteredListener(ElasticNewsSearchActivity.this);
			loginThread.resumeRequest();
		}
	}
	
	private class FetchV2News extends AsyncTask<Void, Void, Exception>{
		private Exception ex;
		private String response;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(dialog != null && !ElasticNewsSearchActivity.this.isFinishing()){
				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				dialog.show();
			}

			DefinitionConstants.Debug("debugElastic==source=" + mSource + ", category=" + mCategory +
					", keyword=" + mKeyword + ", target=" + mTarget + ", fromDate=" + strFromDate + ", toDate=" + strToDate);
		}
		
		@Override
		protected Exception doInBackground(Void... arg0) {

			try{
				response = QcHttpConnectUtil.getV2News(mKeyword, mTarget, mGuid, mCategory, 
						mSource, strFromDate, strToDate, size, startIndex);
				
				while(response == null && intRetryCtr < AppConstants.intMaxRetry){
					response = QcHttpConnectUtil.getV2News(mKeyword, mTarget, mGuid, mCategory, 
							mSource, strFromDate, strToDate, size, startIndex);
				}
				
				intRetryCtr = 0;
				
			}catch(Exception e){
				ex = e;
				e.printStackTrace();
			}
			
			return ex;
		}

		@Override
		protected void onPostExecute(Exception exception) {
			super.onPostExecute(exception);
			
			if(dialog != null && dialog.isShowing())
				dialog.dismiss();
			
			if(exception != null){
				
			}else{
				totalNewsSize = QcMessageParser.getTotalNewsSize(response);
				stockNewsArrayList = QcMessageParser.parseStockNewsV2(response);

				if(stockNewsArrayList.size() > 0) {
					tvNewsNoResult.setVisibility(View.GONE);
					lvElasticNews.setVisibility(View.VISIBLE);

					StockNewsAdapter newsAdapter = new StockNewsAdapter(ElasticNewsSearchActivity.this, stockNewsArrayList);
					lvElasticNews.setAdapter(newsAdapter);

					lvElasticNews.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
							StockNews mNews = stockNewsArrayList.get(position);

							Intent intent = new Intent();
							intent.setClass(ElasticNewsSearchActivity.this, ElasticNewsDetailsActivity.class);
							intent.putExtra(ParamConstants.NEWS_SOURCE_TAG, mNews.getNewsSource());
							intent.putExtra(ParamConstants.NEWSID_TAG, mNews.getNewsId());
							startActivity(intent);
						}
					});
				}else{
					tvNewsNoResult.setVisibility(View.VISIBLE);
					lvElasticNews.setVisibility(View.GONE);
				}
			}
		}
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message	= handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				
				if(loginThread != null)
					loginThread.stopRequest();
				
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				if( ElasticNewsSearchActivity.this.mainMenu.isShowing() )
					ElasticNewsSearchActivity.this.mainMenu.hide();
					
				if( !isFinishing() )
					ElasticNewsSearchActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};
	
	@Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/*public class EndlessScrollListener implements OnScrollListener {

	    private int visibleThreshold = 2;
	    private int currentPage = 0;
	    private int previousTotal = 0;
	    private boolean loading = true;

	    public EndlessScrollListener() {
	    }
	    public EndlessScrollListener(int visibleThreshold) {
	        this.visibleThreshold = visibleThreshold;
	    }

	    @Override
	    public void onScroll(AbsListView view, int firstVisibleItem,
	            int visibleItemCount, int totalItemCount) {
	        if (loading) {
	            if (totalItemCount > previousTotal) {
	                loading = false;
	                previousTotal = totalItemCount;
	                currentPage++;
	            }
	        }
	        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
	            
	        	startIndex = startIndex + size;
	        	isFirstLoad = false;
	        	new FetchV2News().execute();
	        	
	            loading = true;
	        }
	    }

	    @Override
	    public void onScrollStateChanged(AbsListView view, int scrollState) {
	    }
	}*/
}