package com.n2nconnect.android.stock.activity;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.StockNews;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.ArchiveNewsUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class NewsItemActivity extends CustomWindow implements OnDoubleLoginListener, OnRefreshStockListener {

	private String categoryName;
	private List<StockNews> newsUpdates;
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private String userParam;
	*/
	/* Mary@20130605 - comment unused variable
	private boolean isEmpty = false;
	*/
	private DoubleLoginThread loginThread;
	private TextView noResult;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr	= 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	//Fixes_Request-20170103, ReqNo.1 - START
	private boolean isDisplayYesterday	= false;
	private ListView newsItemList;
	private ProgressDialog dialog 		= null;
	private boolean isGetArchiveNews	= false;

	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.news_items);
	
			loginThread = ((StockApplication)NewsItemActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) NewsItemActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(this);
	
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(NewsItemActivity.this)
				//.setIcon(android.R.attr.alertDialogStyle)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
						return;
					}
				})
				.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			userParam = sharedPreferences.getString(PreferenceConstants.QC_USER_PARAM, null);
			try {
				if (userParam == null) {
					List<String>data = QcHttpConnectUtil.relogin();
					userParam = data.get(0);
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
					editor.commit();
				}
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
			}
			*/
	
			noResult 	= (TextView)findViewById(R.id.noResultsFoundView);
			dialog		= new ProgressDialog(this);
	
			Bundle extras = this.getIntent().getExtras();
			if (extras != null) {
				categoryName 				= extras.getString(ParamConstants.CATEGORY_NAME_TAG);
				
				//Added by Thinzar, Fixes_Request-20170103, ReqNo.1 - START
				if(categoryName.equalsIgnoreCase("All News") && AppConstants.brokerConfigBean.isArchiveNews()){
					
					isGetArchiveNews	= true;
				}
				//Added by Thinzar, Fixes_Request-20170103, ReqNo.1 - END
				else{
					StockApplication application = (StockApplication) this.getApplication();
					newsUpdates 				 = application.getNewsItems();
					
					noResult.setVisibility( (newsUpdates != null && newsUpdates.size() != 0) ? View.GONE : View.VISIBLE );
				}
			} else {
				categoryName = "News Anouncements";
				try {
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					 * Map parameters = new HashMap();
					parameters.put(ParamConstants.USERPARAM_TAG, userParam);
					parameters.put(ParamConstants.EXCHANGECODE_TAG, AppConstants.DEFAULT_EXCHANGE_CODE);
					
					String response = QcHttpConnectUtil.newsAll(parameters);
					*/ 
					String response = QcHttpConnectUtil.newsAll(AppConstants.DEFAULT_EXCHANGE_CODE);
					while(intRetryCtr < AppConstants.intMaxRetry && response == null){
						intRetryCtr++;
						response = QcHttpConnectUtil.newsAll(AppConstants.DEFAULT_EXCHANGE_CODE);
					}
					intRetryCtr	= 0;
					
					if(response == null)				
						throw new Exception(ErrorCodeConstants.FAIL_GET_NEWS);
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					/* Mary@20121102 - Fixes_Request-20121102, ReqNo.2
					if(response != null)
					*/
					else
						newsUpdates = QcMessageParser.parseNewsUpdate(response);
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				} catch (FailedAuthenicationException e) {
					e.printStackTrace();
				}
				*/
				}catch(Exception ex){
					Log.v("NEWS", "error", ex);
					alertException.setMessage( ex.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
							try{
								dialog.dismiss();
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
							}catch(Exception e){
								e.printStackTrace();
							}
							// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
						}
					});
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! NewsItemActivity.this.isFinishing() )
						alertException.show();	
				}
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END;
	
			}
	
			this.title.setText(categoryName);
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(R.drawable.menuicon_news);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntNewsMenuIconBgImg() );
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	
	public void onResume() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onResume();
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= NewsItemActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
	
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(NewsItemActivity.this);
				loginThread.resumeRequest();
			}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			
			newsItemList = (ListView) this.findViewById(R.id.newsItemList);
	
			/* Mary@20130605 - comment unused variable
			isEmpty = false;
			*/
			if(isGetArchiveNews){
				new GetArchiveNews().execute();
				isGetArchiveNews	= false;
				
			} else{
				NewsArrayAdapter newsAdapter = new NewsArrayAdapter(this, newsUpdates);
				newsItemList.setAdapter(newsAdapter);
				newsItemList.setDivider(null);
				newsItemList.setDividerHeight(0);
		
				newsItemList.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView parent, View view, int position, long id) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						if(newsUpdates!=null){
							/* commented out by Thinzar, Change_Request-20160101, ReqNo.3
							LinearLayout rowLayout	= (LinearLayout) view.findViewById(R.id.newsUpdateLayout);
							rowLayout.setBackgroundDrawable(null);
							rowLayout.setBackgroundColor(Color.CYAN);
							*/
							StockNews news			= newsUpdates.get(position);
							Intent intent 			= new Intent();
							intent.putExtra(ParamConstants.NEWSID_TAG, news.getNewsId());
							intent.setClass(NewsItemActivity.this, NewsActivity.class);
							startActivity(intent);
						}
					}
				});
			}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}

	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	
	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if(this.mainMenu.isShowing()){
				this.mainMenu.hide();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			}else{
				finish();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			}
			// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}

	private class NewsArrayAdapter extends ArrayAdapter<StockNews> {
		private Activity activity;
		private List<StockNews> stockNews;

		public NewsArrayAdapter(Activity activity, List<StockNews> newsUpdates) {
			super(activity, R.layout.news_update_row, newsUpdates);

			this.activity = activity;
			this.stockNews = newsUpdates;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return newsUpdates.size();
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder = null;
			View rowView = convertView;

			if (rowView == null) {
				LayoutInflater inflater = activity.getLayoutInflater();
				rowView = inflater.inflate(R.layout.news_update_row, null, true);
				viewHolder = new ViewHolder();

				viewHolder.titleLabel = (TextView) rowView.findViewById(R.id.newsTitleLabel);
				viewHolder.symbolLabel = (TextView) rowView.findViewById(R.id.newsSymbolLabel);
				viewHolder.dateLabel = (TextView) rowView.findViewById(R.id.newsDateLabel);
				viewHolder.rowLayout = (LinearLayout) rowView.findViewById(R.id.newsUpdateLayout);

				rowView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) rowView.getTag();
			}
			
			StockNews news = stockNews.get(position);
			
			//Added by Thinzar@20150225, Fixes_Request-20150201, ReqNo. 2 - START
			//special case for CIMB_SG, if news title contains <origin Href="quoteref">dbsm.si</origin>, strip the HTML tags and convert the stock symbol to upper case
			//System.out.println("NewsTitle: " + news.getNewsTitle());
			String newsTitle	= news.getNewsTitle();
			if(newsTitle.contains("</origin>")){
				newsTitle	= newsTitle.replaceAll("<[^>]*>", "");//.toUpperCase();	//Don't convert to uppercase
			}
			//Fixes_Request-20150201, ReqNo. 2 - END
			
			viewHolder.titleLabel.setText(newsTitle);
			/* Mary@20130910 - Change_Request-20130724, ReqNo.13
			viewHolder.symbolLabel.setText(news.getSymbolCode());
			*/
			viewHolder.symbolLabel.setText( news.getSymbolName() == null ?  news.getSymbolCode() : news.getSymbolName() );
					
			if(news.getNewsDate() != null)
				viewHolder.dateLabel.setText(FormatUtil.formatDateString(news.getNewsDate(), FormatUtil.NEWSDATE_FORMAT_2));
			
			/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			if ((position+1)%2 != 0) {
				viewHolder.rowLayout.setBackgroundResource(R.drawable.tablerow_dark);
			} else {
				viewHolder.rowLayout.setBackgroundResource(R.drawable.tablerow_light);
			}
			*/
			/* commented out by Thinzar, Change_Request-20160101, ReqNo.3
			viewHolder.rowLayout.setBackgroundResource( ( (position+1)%2 != 0 ) ? R.drawable.tablerow_dark : R.drawable.tablerow_light );
			*/
			return rowView;
		}

		private class ViewHolder {
			public TextView titleLabel;
			public TextView symbolLabel;
			public TextView dateLabel;
			public LinearLayout rowLayout;
		}

	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(NewsItemActivity.this.mainMenu.isShowing()){
					NewsItemActivity.this.mainMenu.hide();
				}

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					NewsItemActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		// TODO Auto-generated method stub

	}
	
	private class GetArchiveNews extends AsyncTask<String, Void, String> {
		String strExchgCode;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			//no archive news return for delay feed, remove "D"
			strExchgCode = AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase(AppConstants.MY_EXCHANGE_CODE) ? "KL" : AppConstants.DEFAULT_EXCHANGE_CODE;
			strExchgCode = (strExchgCode.endsWith("D"))? strExchgCode.substring(0, strExchgCode.length() - 1) : strExchgCode;
			
			try{
				if( dialog != null && !dialog.isShowing() ){
					dialog.setMessage("Loading..");
					
					if( ! NewsItemActivity.this.isFinishing() )
						dialog.show();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		@Override
		protected String doInBackground(String... params) {
			System.out.println("GetArchiveNews: isDisplayYesterday=" + isDisplayYesterday);
		 	String date 			= null ;
		 	String response="";
		 	
			if( isDisplayYesterday==false){
			  	date = ArchiveNewsUtil.getCurrentDate();
			}else{
				date = ArchiveNewsUtil.getYesterdayDate(); 
			}
			
			response = ArchiveNewsUtil.getArchiveNews("", strExchgCode, date, date);
		
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			if( dialog != null && dialog.isShowing() )
					dialog.dismiss();
			
			if (result != null && result.length() > 0) {

				newsUpdates = ArchiveNewsUtil.convertArchiveNews(result);
				
				if(newsUpdates.size()>0){
					NewsArrayAdapter newsAdapter = new NewsArrayAdapter(NewsItemActivity.this, newsUpdates);
					
					newsItemList.setAdapter(newsAdapter);
					newsItemList.setDivider(null);
					newsItemList.setDividerHeight(0);
					newsItemList.setOnItemClickListener(new OnItemClickListener() {
						public void onItemClick(AdapterView parent, View view, int position, long id) {
							StockNews news			= newsUpdates.get(position);
							Intent intent 			= new Intent();
							intent.putExtra(ParamConstants.NEWSID_TAG, news.getNewsId());
							intent.setClass(NewsItemActivity.this, NewsActivity.class);
							startActivity(intent);
						}
					});
					
				}else{
					if(isDisplayYesterday == false)	{			//without this, will end up in endless loop
						isDisplayYesterday=true;
						new GetArchiveNews().execute();
					} else{
						noResult.setVisibility(View.VISIBLE);
					}
				}
			}else{
				noResult.setVisibility( (newsUpdates != null && newsUpdates.size() != 0) ? View.GONE : View.VISIBLE );
			}
		}
	}
}