package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class ListViewItem extends BaseAdapter{
	
	private ArrayList<String> listItem;
	private Context context;
	private LayoutInflater mInflater;
	private ViewHolder holder = null;

	public ListViewItem(Context context,ArrayList<String> list ){
		super();
		this.listItem = list;
		this.context = context;
	}
	
	private class ViewHolder {
		TextView keyItem;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listItem.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView=mInflater.inflate(R.layout.list_item_row,null);
			holder = new ViewHolder();
			holder.keyItem = (TextView)convertView.findViewById(R.id.keyID);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.keyItem.setText(listItem.get(position));
		
		return convertView;
	}
	

}
