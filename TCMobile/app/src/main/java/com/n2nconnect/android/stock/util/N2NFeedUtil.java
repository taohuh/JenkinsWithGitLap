package com.n2nconnect.android.stock.util;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;

import com.n2nconnect.android.stock.FeedObject;

public class N2NFeedUtil {

    public final static String N2NFEED_CHARSET = "ISO-8859-1";

    private static int processKeepAliveTime(String s) {

        int iTmpByte;
        int iDataLength = s.length();
        char cChar;
        StringBuffer sbResult = new StringBuffer();

        for(int i = 0; i < iDataLength; i++) {
            iTmpByte = ~(s.charAt(i));
            cChar = (char)(((iTmpByte >> 4) & 0x0F) + 48);
            if (!Character.isDigit(cChar)) {
                cChar = ' ';
            }
            sbResult.append(cChar);
            cChar = (char)((iTmpByte & 0x0F) + 48);
            if (!Character.isDigit(cChar)) {
                cChar = ' ';
            }
            sbResult.append(cChar);
        }
        if(sbResult.length() > 0) {
            try {
                return Integer.parseInt(formatInteger(sbResult.toString()));
            } catch(NumberFormatException e) {
                return 30;
            }
        } else {
            return 30;
        }
    }

    private static String formatInteger(String value) {
        if (value == null || value.length() == 0)
            return null;

        if(value.length() == 1)
            return value;
        
        String sDigit	= value.substring(value.length()-1, value.length());
        value 			= value.substring(0, value.length()-1);

        int digit 		= 0;
        try {
            digit = Integer.parseInt(sDigit);
        } catch (NumberFormatException e) { }
        
        StringBuffer buf = new StringBuffer(value);
        if(digit > 0){
            if (buf.length() - digit >= 0) {
                buf = buf.insert(buf.length()-digit, ".");
            }else{
                while (buf.length() - digit < 0) {
                    buf.insert(0, '0');
                }
                buf = buf.insert(0, ".");
            }
            // Added by Mary@20130605 - avoid IndexOutOfBoundException
            if(buf.length() > 0){
	            while (buf.charAt(0) == '0') {
	                buf.deleteCharAt(0);
	            }
	            if (buf.charAt(0) == '.') {
	                buf.insert(0, '0');
	            }
            }
            value = buf.toString();
        }else {
        	// Added by Mary@20130605 - avoid IndexOutOfBoundException
            if(buf.length() > 0){
	            while (buf.charAt(0) == '0') {
	                if (buf.length() > 1) {
	                    buf.deleteCharAt(0);
	                }else{
	                    break;
	                }
	            }
            }
            value = buf.toString();
        }
        return value;
    }
    
    public static String decompress(byte[] s) {
		boolean isNegative = false;
		int i;
		int iTmpByte;
		byte bCharByte;
		char cChar;
		String sResult = "";

		for (i = 0; i < s.length; i++) {
			iTmpByte = ~(s[i]);

			cChar = (char) (((iTmpByte >> 4) & 0xf) + 48);

			if (!Character.isDigit(cChar) && (byte) cChar == 58) {
				isNegative = true;
				cChar = ' ';
			} else if (!Character.isDigit(cChar)) {
				cChar = ' ';
				sResult += String.valueOf(cChar);
			} else {
				sResult += String.valueOf(cChar);
			}

			cChar = (char) ((iTmpByte & 0x0F) + 48);


			if (!Character.isDigit(cChar) && (byte) cChar == 58) {
				isNegative = true;
				cChar = ' ';
			} else if (!Character.isDigit(cChar)) {
				cChar = ' ';
				sResult += String.valueOf(cChar);
			} else {
				sResult += String.valueOf(cChar);
			}

		}
		if (isNegative) {
			return ("-" + sResult);
		} else {
			return sResult;
		}
	}

    public static FeedObject processTimeStampData(byte[] abtData, ArrayList alExchgList) throws Exception {

        ArrayList <Byte> alBytes = null;
        ArrayList alField = new ArrayList();
        int iLength = abtData.length;
        FeedObject feedObj = new FeedObject();
        byte[] abtTempByteArray;
        byte[] abtByteArray;
        int iCode;
        boolean bReturned = false;
        int iFieldLength;

        for (int i = 5; i < iLength; i++) {
            int j = (int) abtData[i] & 0xff;

            if (alBytes == null) {
                alBytes = new ArrayList<Byte>();
            }
            if (j == 30 || j == 3) {
                byte[] b = new byte[alBytes.size()];
                for (int k = 0; k < alBytes.size(); k++) {
                    b[k] = alBytes.get(k);
                }
                alField.add(b);
                alBytes.clear();
            } else {
                alBytes.add(abtData[i]);
            }
        }
        iFieldLength = alExchgList == null ? 0 : alExchgList.size();
        for (Object object : alField) {

            abtByteArray = (byte[]) object;
            iCode = abtByteArray[0] & 0xff;
            abtTempByteArray = new byte[abtByteArray.length - 1];
            System.arraycopy(abtByteArray, 1, abtTempByteArray, 0, abtTempByteArray.length);

            switch(iCode) {
                case 131:   //Exchange
                    String sTempStr = new String(abtTempByteArray);
                    if(iFieldLength == 0) {
                        bReturned = true;
                    } else {
                        if(alExchgList.contains(sTempStr) || iFieldLength == 0) {
                            bReturned = true;
                        } else {
                            bReturned = false;
                        }
                    }
                    if(bReturned) {
                        feedObj.setStringData(iCode, new String(abtTempByteArray));
                    }
                    break;
                case 54:    //Time
                    if(bReturned) {
                        feedObj.setNumericWithRoundupData(iCode, SystemUtil.decompress(abtTempByteArray));
                    }
                    break;
                case 53:    //Date
                    if(bReturned) {
                        feedObj.setNumericWithRoundupData(iCode, SystemUtil.decompress(abtTempByteArray));
                    }
                    break;
                case 48:
                    break;
            }
        }
        if(feedObj.getTotalFeedElement() > 1) {
            return feedObj;
        } else {
            return null;
        }
    }

    public static FeedObject[] processScoreBoardData(byte[] abtData, ArrayList alFieldID) throws Exception {

        ArrayList <Byte> alBytes = null;
        ArrayList alField = new ArrayList();
        int iLength = abtData.length;
        FeedObject feedObj = new FeedObject();
        FeedObject feedFullObj = new FeedObject();
        byte[] abtTempByteArray;
        byte[] abtByteArray;
        int iCode;
        boolean bReturned;
        int iFieldLength;

        for (int i = 5; i < iLength; i++) {
            int j = (int) abtData[i] & 0xff;

            if (alBytes == null) {
                alBytes = new ArrayList<Byte>();
            }
            if (j == 30 || j == 3) {
                byte[] b = new byte[alBytes.size()];
                for (int k = 0; k < alBytes.size(); k++) {
                    b[k] = alBytes.get(k);
                }
                alField.add(b);
                alBytes.clear();
            } else {
                alBytes.add(abtData[i]);
            }
        }
        iFieldLength = alFieldID == null ? 0 : alFieldID.size();
        for (Object object : alField) {
            abtByteArray = (byte[]) object;
            iCode = abtByteArray[0] & 0xff;

            if(iFieldLength == 0) {
                bReturned = true;
            } else {
                if(alFieldID.contains(String.valueOf(iCode))) {
                    bReturned = true;
                } else {
                    bReturned = false;
                }
            }
            abtTempByteArray = new byte[abtByteArray.length - 1];
            System.arraycopy(abtByteArray, 1, abtTempByteArray, 0, abtTempByteArray.length);
            switch(iCode) {
                case 131:
                    feedFullObj.setStringData(iCode, new String(abtTempByteArray));
                    if(bReturned) {
                        feedObj.setStringData(iCode, new String(abtTempByteArray));
                    }
                    break;
                case 133:
                    feedFullObj.setNumericData(iCode, SystemUtil.decompress(abtTempByteArray));
                    if(bReturned) {
                        feedObj.setNumericData(iCode, SystemUtil.decompress(abtTempByteArray));
                    }
                    break;
                case 36:
                default:
                    break;
                case 105:   //Scoreboard Up
                case 106:   //Scoreboard Downbut if i
                case 107:   //ScoreBoard Unchange
                case 108:   //ScoreBoard Untrade
                case 109:   //ScoreBoard Total Counter
                case 110:   //ScoreBoard Total Volume
                    feedFullObj.setNumericWithRoundupData(iCode, SystemUtil.decompress(abtTempByteArray));
                    if(bReturned) {
                        feedObj.setNumericWithRoundupData(iCode, SystemUtil.decompress(abtTempByteArray));
                    }
                    break;
                case 111:   //ScoreBoard Total Value
                    feedFullObj.setNumericData(iCode, SystemUtil.decompress(abtTempByteArray));
                    if(bReturned) {
                        feedObj.setNumericData(iCode, SystemUtil.decompress(abtTempByteArray));
                    }
                    break;
            }
        }
        return new FeedObject[] {feedObj, feedFullObj};
    }

    /*
     * 2010-06-02 cytan update mapping of 134 to currency. format string.
     */
    public static FeedObject[] processPushData(byte[] abtData, ArrayList alFieldID) throws Exception {

        ArrayList <Byte> alBytes = null;
        ArrayList alField = new ArrayList();
        int iLength = abtData.length;
        FeedObject feedObj = new FeedObject();
        FeedObject feedFullObj = new FeedObject();
        byte[] abtTempByteArray;
        byte[] abtByteArray;
        int iCode;
        StringBuffer sbBuf = new StringBuffer();
        boolean bReturned;
        int iFieldLength;

        for (int i = 5; i < iLength; i++) {
            int j = (int) abtData[i] & 0xff;

            if (alBytes == null) {
                alBytes = new ArrayList<Byte>();
            }
            if (j == 30 || j == 3) {
                byte[] b = new byte[alBytes.size()];
                for (int k = 0; k < alBytes.size(); k++) {
                    b[k] = alBytes.get(k);
                }
                alField.add(b);
                alBytes.clear();
            } else {
                alBytes.add(abtData[i]);
            }
        }
        iFieldLength = alFieldID == null ? 0 : alFieldID.size();
        if(alField.size() != 0) {
            for (Object object : alField) {
                abtByteArray = (byte[]) object;
                if (abtByteArray.length == 0) {
                    continue;
                }
                
                try {
                    iCode = abtByteArray[0] & 0xff;
                } catch (Exception e) {
                    throw e;
                }

                if(iFieldLength == 0) {
                    bReturned = true;
                } else {
                    if(alFieldID.contains(String.valueOf(iCode))) {
                        bReturned = true;
                    } else {
                        bReturned = false;
                    }
                }
                abtTempByteArray = new byte[abtByteArray.length - 1];
                System.arraycopy(abtByteArray, 1, abtTempByteArray, 0, abtTempByteArray.length);
                switch(iCode) {
                    case 33:    //Stock Code
                        feedFullObj.setStringData(iCode, new String(abtTempByteArray));
                        feedObj.setStringData(iCode, new String(abtTempByteArray));
                        break;
                    //case 134:   //Bid Price
                    case 134:   //Currency
                    case 139:   //Ask Split
                    case 156:   //TP
                    case 48:    //Status
                        feedFullObj.setStringData(iCode, new String(abtTempByteArray));
                        if(bReturned) {
                            feedObj.setStringData(iCode, new String(abtTempByteArray));
                        }
                        break;
                    case 38:    //Symbol
                    case 129:   //Symbol Language Flag
                    case 39:    //Company Name
                    case 157:   //ISIN Code
                    case 104:   //PI
                        sbBuf.setLength(0);
                        for (int i = 0; i < abtTempByteArray.length; i++) {
                            int j = abtTempByteArray[i] & 0xff;
                            sbBuf.append((char) j);
                        }
                        feedFullObj.setStringData(iCode, sbBuf.toString());
                        if(bReturned) {
                            feedObj.setStringData(iCode, sbBuf.toString());
                        }
                        break;
                    case 130:   //Symbol 2
                        try {
                            feedFullObj.setStringData(iCode, new String(abtTempByteArray, "UTF-8"));
                            if(bReturned) {
                                feedObj.setStringData(iCode, new String(abtTempByteArray, "UTF-8"));
                            }
                        } catch(UnsupportedEncodingException e) {
                            }
                        break;
                    case 50:    //Prev
                    case 51:    //Reference/LACP
                    case 55:    //Open
                    case 56:    //High
                    case 57:    //Low
                    case 98:    //Last Done Price
                    case 153:   //TOP
                    case 152:   //Sett
                    case 231:   //Floor
                    case 232:   //Ceiling
                    case 36:    //Sector ID
                    case 35:    //Index ID
                    case 123:   //PAR
                    case 52:    //Open Int
                    case 68:    //Bid Price 1
                    case 88:    //Ask Price 1
                    case 69:    //Bid Price 2
                    case 89:    //Ask Price 2
                    case 70:    //Bid Price 3
                    case 90:    //Ask Price 3
                    case 71:    //Bid Price 4
                    case 91:    //Ask Price 4
                    case 72:    //Bid Price 5
                    case 92:    //Ask Price 5
                        feedFullObj.setNumericData(iCode, SystemUtil.decompress(abtTempByteArray));
                        if(bReturned) {
                            feedObj.setNumericData(iCode, SystemUtil.decompress(abtTempByteArray));
                        }
                        break;
                    case 99:    //Last Done Qty
                    case 100:   //Last Done Val
                    case 101:   //Volume
                    case 102:   //Value
                    case 103:   //Transaction Number
                    case 237:   //Total Buy Trans Vol
                    case 238:   //Total Sell Trans Vol
                    case 239:   //Total Short Volume
                    case 241:   //Foreign Total Buy Value
                    case 242:   //Foreign Total Sell Value
                    case 136:   //Bid Split
                    case 135:   //Bid Qty
                    case 40:    //Lot Size
                    case 41:    //Share Issue
                    case 234:   //Foreign Room
                    case 235:   //Foreign Total Buy Vol
                    case 236:   //Foreign Total Sell Vol
                    case 243:   //Total Short Sell Vol
                    case 244:   //Total Short Buy Vol
                    case 138:   //Ask Qty
                    case 132:   //Total Trade
                    case 58:    //Bid Qty 1
                    case 170:   //Bid Split 1
                    case 78:    //Ask Qty 1
                    case 180:   //Ask Split 1
                    case 59:    //Bid Qty 2
                    case 171:   //Bid Split 2
                    case 79:    //Ask Qty 2
                    case 181:   //Ask Split 2
                    case 60:    //Bid Qty 3
                    case 172:   //Bid Split 3
                    case 80:    //Ask Qty 3
                    case 182:   //Ask Split 3
                    case 61:    //Bid Qty 4
                    case 173:   //Bid Split 4
                    case 81:    //Ask Qty 4
                    case 183:   //Ask Split 4
                    case 62:    //Bid Qty 5
                    case 174:   //Bid Split 5
                    case 82:    //Ask Qty 5
                    case 184:   //Ask Split 5
                        feedFullObj.setNumericWithRoundupData(iCode, SystemUtil.decompress(abtTempByteArray));
                        if(bReturned) {
                            feedObj.setNumericWithRoundupData(iCode, SystemUtil.decompress(abtTempByteArray));
                        }
                        break;
                    case 54:    //Last Done Time
                    case 49:    //Expiry Date
                        feedFullObj.setDateTimeData(iCode, SystemUtil.decompress(abtTempByteArray));
                        if(bReturned) {
                            feedObj.setDateTimeData(iCode, SystemUtil.decompress(abtTempByteArray));
                        }
                        break;
                    case 124:   //PVWAP
                    case 125:   //Last SI
                    case 151:   //Stock Type
                    case 119:   //Text Type
                    case 245:   //?
                    case 246:   //?
                    case 254:   //?
                    case 113:   //Reg. No
                    case 127:   //EPS in sen
                    case 122:   //Tick Trend
                        break;
                }
            }
        }
        return new FeedObject[] {feedObj, feedFullObj};
    }

    public static FeedObject processSortData(byte[] abtData, ArrayList alFieldID) throws Exception {

        ArrayList <Byte> alBytes = null;
        ArrayList alField = new ArrayList();
        int iLength = abtData.length;
        FeedObject feedObj = new FeedObject();
        byte[] abtTempByteArray;
        byte[] abtByteArray;
        int iCode;

        for (int i = 5; i < iLength; i++) {
            int j = (int) abtData[i] & 0xff;

            if (alBytes == null) {
                alBytes = new ArrayList<Byte>();
            }
            if (j == 30 || j == 3) {
                byte[] b = new byte[alBytes.size()];
                for (int k = 0; k < alBytes.size(); k++) {
                    b[k] = alBytes.get(k);
                }
                alField.add(b);
                alBytes.clear();
            } else {
                alBytes.add(abtData[i]);
            }
        }
        for (Object object : alField) {
            abtByteArray = (byte[]) object;
            iCode = abtByteArray[0] & 0xff;

            abtTempByteArray = new byte[abtByteArray.length - 1];
            System.arraycopy(abtByteArray, 1, abtTempByteArray, 0, abtTempByteArray.length);
            switch(iCode) {
                case 33:    //Stock Code
                    feedObj.setStringData(iCode, new String(abtTempByteArray));
                    break;
                /*case 38:    //Symbol
                    break;
                case 130:   //Symbol 2
                    break;*/
                case 50:    //Prev
                case 51:    //Reference/LACP
                case 56:    //High
                case 57:    //Low
                case 98:    //Last Done Price
                case 68:    //Bid Price
                case 88:    //Ask Price
                    feedObj.setNumericData(iCode, SystemUtil.decompress(abtTempByteArray));
                    break;
                case 99:    //Last Done Volume
                case 101:   //Volume
                case 58:    //Bid Qty
                case 78:    //Ask Qty
                    feedObj.setNumericWithRoundupData(iCode, SystemUtil.decompress(abtTempByteArray));
                    break;
            }
        }
        return feedObj;
    }


    public static String byteToString(byte[] buf) {
        String decStr;
        try {
            decStr = new String(buf, N2NFEED_CHARSET);
        } catch (UnsupportedEncodingException ex) {
            decStr = "";
            ex.printStackTrace();
        }
        return decStr;
    }

    public static Charset getFeedCharset() {
        Charset charset = null;
        try {
            charset = Charset.forName(N2NFEED_CHARSET);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return charset;
    }

    public static int getContentLength(String sKey, String sData) {

        if (sKey == null) {
            return 0;
        }
        if (sData == null) {
            return 0;
        }

        int iPos = sData.indexOf(sKey);
        String sTempData;

        if (iPos != -1) {
            sTempData = sData.substring(iPos + sKey.length());
            iPos = sTempData.indexOf("\r");
            if (iPos != -1) {
                sTempData = sTempData.substring(0, iPos);
            }
            try {
                return Integer.parseInt(sTempData.trim());
            } catch (NumberFormatException e) {
                return 0;
            }
        } else {
            return 0;
        }
    }

}
