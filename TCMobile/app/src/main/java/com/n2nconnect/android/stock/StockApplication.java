package com.n2nconnect.android.stock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.n2nconnect.android.stock.model.CurrencyExchangeInfo;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.NewsCategory;
import com.n2nconnect.android.stock.model.StockNews;
import com.n2nconnect.android.stock.model.TradeAccount;

import android.app.Application;

public class StockApplication extends Application {
	public static final String APP_NAME = "StockTrade";
	private List<TradeAccount> tradeAccounts;
	private List<ExchangeInfo> exchangeInfos;
	// Added by Mary@20121228-Fixes_Request-20121102, ReqNo.11
	private List<CurrencyExchangeInfo> currencyExchangeInfos;
    private RefreshStockThread refreshThread;
    private IdleThread idleThread;
    private TradeStatusThread tradeStatusThread;
    private DoubleLoginThread doubleLoginThread;
    private PortfolioThread portfolioThread;
    private ExchangeInfo selectedExchange;
    private TradeAccount selectedAccount;
    private List<StockNews> newsItems;
    private boolean isRememberPin;
    // Added by Mary@20121009 - Fixes_Request-20120724, ReqNo.7
    private boolean isSkipConfirmation;
    private String tradingPin;
    private String selectedExchangeCode;
    private QCAliveThread qcaliveThread;
    //Added by Kw@20130116 - Change_Request20130104, ReqNo.2
    private static OTPvalidationThread otpValidationThread;
    // Added by Mary@20130918 - Change_Request-20130711, ReqNo.14
    private static OneFAValidationThread oneFAValidationThread;
    
    //Added by Thinzar@20140730 - Change_Request-20140701, ReqNo. 9 - START
    private List<String> paymentTypes;
    
    //Change_Request-20170601, ReqNo.2
    private List<NewsCategory> newsCategories;
    
    //Change_Request-20170601, ReqNo.8
    private String externalIntentUri;
    
    // Added by Mary@20121009 - Fixes_Request-20120724, ReqNo.7 - START
    public boolean isSkipConfirmation() {
		return isSkipConfirmation;
	}
	public void setSkipConfirmation(boolean isSkipConfirmation) {
		this.isSkipConfirmation = isSkipConfirmation;
	}
    // Added by Mary@20121009 - Fixes_Request-20120724, ReqNo.7 - END
    
	public boolean isRememberPin() {
		return isRememberPin;
	}
	public void setRememberPin(boolean isRememberPin) {
		this.isRememberPin = isRememberPin;
	}
	public String getTradingPin() {
		return tradingPin;
	}
	public void setTradingPin(String tradingPin) {
		this.tradingPin = tradingPin;
	}
	public List<StockNews> getNewsItems() {
		return newsItems;
	}
	public void setNewsItems(List<StockNews> newsItems) {
		this.newsItems = newsItems;
	}
	public TradeAccount getSelectedAccount() {
		return selectedAccount;
	}
	public void setSelectedAccount(TradeAccount selectedAccount) {
		this.selectedAccount = selectedAccount;
	}
	public ExchangeInfo getSelectedExchange() {
		return selectedExchange;
	}
	public void setSelectedExchange(ExchangeInfo selectedExchange) {
		this.selectedExchange = selectedExchange;
	}
	
	public String getSelectedExchangeCode(){
		return selectedExchangeCode;
	}
	
	public void setSelectedExchangeCode(String exchangeCode){
		this.selectedExchangeCode = exchangeCode;
	}
	
	public RefreshStockThread getRefreshThread() {
		return refreshThread;
	}
	
	public void setPortfolioThread(PortfolioThread portfolioThread) {
		this.portfolioThread = portfolioThread;
	}
	
	public void setRefreshThread(RefreshStockThread refreshThread) {
		this.refreshThread = refreshThread;
	}
	
	public void setQCAliveThread(QCAliveThread aliveThread){
		this.qcaliveThread = aliveThread;
	}
	
	public QCAliveThread getQCAliveThread(){
		return qcaliveThread;
	}
	
	public IdleThread getIdleThread(){
		return idleThread;
	}
	
	public PortfolioThread getPortfolioThread(){
		return portfolioThread;
	}
	
	public void setIdleThread(IdleThread idleThread){
		this.idleThread = idleThread;
	}
	
	public void setDoubleLoginThread(DoubleLoginThread doubleLoginThread){
		this.doubleLoginThread = doubleLoginThread;
	}
	
	public void setTradeStatusThread(TradeStatusThread tradeStatusThread){
		this.tradeStatusThread = tradeStatusThread;
	}
	
	public TradeStatusThread getTradeStatusThread(){
		return tradeStatusThread;
	}
	
	public DoubleLoginThread getDoubleLoginThread(){
		return doubleLoginThread;
	}
	
	public List<TradeAccount> getTradeAccounts() {
		/* Mary@20121018 - Fixes_Request-20121018, ReqNo.4 & 8 - START
		return tradeAccounts;
		 */
		List<TradeAccount> accounts	= new ArrayList<TradeAccount>();
		for (Iterator<TradeAccount> itr=tradeAccounts.iterator(); itr.hasNext(); ) {
			accounts.add( itr.next() );
		}

		return accounts;
		// Mary@20121018 - Fixes_Request-20121018, ReqNo.4 & 8 - END
	}
	
	/* Mary@20130220 - Fixes_Request-20130108, ReqNo.10
	public List getTradeAccounts(String exchangeCode) {
		List exchangeAccounts = new ArrayList();
		for (Iterator itr=tradeAccounts.iterator(); itr.hasNext(); ) {
			TradeAccount account = (TradeAccount) itr.next();
			
			if(account.getSupportedExchange().size()==0){
				if(!exchangeCode.equals("MY")){
					exchangeAccounts.add(account);
				}
			}else{
				if(account.getSupportedExchange().contains(exchangeCode)){
					exchangeAccounts.add(account);
				}
			}
		}
		return exchangeAccounts;
	}
	*/
	
	public void setTradeAccounts(List<TradeAccount> tradeAccounts) {
		this.tradeAccounts = tradeAccounts;
	}
	public List<ExchangeInfo> getExchangeInfos() {
		return exchangeInfos;
	}
	public void setExchangeInfos(List<ExchangeInfo> exchangeInfos) {
		this.exchangeInfos = exchangeInfos;
	}
	
	// Added by Mary@20121228-Fixes_Request-20121102, ReqNo.11 - START
	public List<CurrencyExchangeInfo> getCurrencyExchangeInfos() {
		List<CurrencyExchangeInfo> temp	= new ArrayList<CurrencyExchangeInfo>();
		for(int i=0; i<currencyExchangeInfos.size(); i++){
			temp.add(i, currencyExchangeInfos.get(i) );
		}
		return temp;
	}
	public void setCurrencyExchangeInfos(List<CurrencyExchangeInfo> currencyExchangeInfos) {
		this.currencyExchangeInfos = currencyExchangeInfos;
	}
	// Added by Mary@20121228-Fixes_Request-20121102, ReqNo.11 - END
	
	// Added by Kw@20130116-Change_Request-20130104, ReqNo.2 - START
	public void setOTPvalidationThread(OTPvalidationThread otpValidationThread){
		StockApplication.otpValidationThread = otpValidationThread;
	}
	
	public static OTPvalidationThread getOTPvalidationThread(){
		return otpValidationThread;
	}
	// Added by Kw@20130116-Change_Request-20130104, ReqNo.2 - END
	
	// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14 - START
	public void setOneFAValidationThread(OneFAValidationThread oneFAValidationThread){
		StockApplication.oneFAValidationThread = oneFAValidationThread;
	}
	
	public static OneFAValidationThread getOneFAValidationThread(){
		return oneFAValidationThread;
	}
	// Added by Mary@20130918 - Change_Request-20130711, ReqNo.14 - END
	
	//Added by Thinzar@20140730 - Change_Request-20140701, ReqNo. 9 - START
	public void setPaymentTypes(List<String> pymtTypes){
		this.paymentTypes = pymtTypes;
	}
		
	public List<String> getPaymentTypes(){
		return paymentTypes;
	}
	//Added by Thinzar@20140730 - END
	
	public List<NewsCategory> getNewsCategories() {
		return newsCategories;
	}
	
	public void setNewsCategories(List<NewsCategory> newsCategories) {
		this.newsCategories = newsCategories;
	}
	
	public String getExternalIntentUri() {
		return externalIntentUri;
	}
	
	public void setExternalIntentUri(String externalIntentUri) {
		this.externalIntentUri = externalIntentUri;
	}
}