package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.List;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.NewsCategoryAdapter;
import com.n2nconnect.android.stock.model.NewsCategory;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class ElasticNewsActivity extends CustomWindow implements OnDoubleLoginListener{
	private DoubleLoginThread loginThread;
	
	private ListView lvResearchReportsCategories;
	private ListView lvNewsCategories;
	private Button btnPopupSearch;
	private TextView tvNote;

	private ArrayList<NewsCategory> researchNewsCategories;
	private ArrayList<NewsCategory> newsCategories;
	private StockApplication application;
	private ProgressDialog dialog;

	//search form
	private Spinner spnNewsSource;
	private Spinner spnNewsCategory;
	private EditText etKeyword;
	private CheckBox chkSearchTargetHeadline;
	private CheckBox chkSearchTargetArticle;
	private Button btnCloseSearch;
	private Button btnSearch;
	private RadioGroup rdGroupDuration;

	private ArrayList<NewsCategory> uniqueNewsSources;

	//search variables
	private int size	   = 50;
	private int startIndex = 0;
	private String mSource;
	private String mCategory;
	private String mKeyword;
	private String mGuid;
	private String mTarget = "3";	//1 - News Content , 2 - News Header, 3 - News Content & New Header
	private String strFromDate;
	private String strToDate;
	private int intRetryCtr = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.title.setText(getResources().getString(R.string.elastic_news_module_title));
		this.icon.setImageResource(R.drawable.menuicon_news);
		setContentView(R.layout.activity_elastic_news);

		dialog 					= new ProgressDialog(ElasticNewsActivity.this);
		lvResearchReportsCategories = (ListView)findViewById(R.id.lv_research_reports_categories);
		lvNewsCategories		= (ListView)findViewById(R.id.lv_news_categories);
		btnPopupSearch			= (Button)findViewById(R.id.btn_popup_search);
		tvNote 					= (TextView)findViewById(R.id.tv_note);
		tvNote.setVisibility(AppConstants.brokerConfigBean.isShowResearchReportNote()?View.VISIBLE:View.GONE);

		researchNewsCategories 	= new ArrayList<NewsCategory>();
		newsCategories			= new ArrayList<NewsCategory>();
		application				= (StockApplication) this.getApplication();

		List<NewsCategory> allNewsCategories = application.getNewsCategories();
		if(allNewsCategories == null || allNewsCategories.size() == 0)
			new GetAllNewsCategories().execute();
		else
			initializeView();

	}

	private class GetAllNewsCategories extends AsyncTask<Void, Void, Exception> {

		private String response;
		private Exception mEx = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
			if(dialog != null && !ElasticNewsActivity.this.isFinishing())
				dialog.show();
		}

		@Override
		protected Exception doInBackground(Void... voids) {

			try {
				response = QcHttpConnectUtil.getAllNewsCategories();

				if(response == null && intRetryCtr < AppConstants.intMaxRetry){
					intRetryCtr++;
					response = QcHttpConnectUtil.getAllNewsCategories();
				}

				intRetryCtr = 0;

				if(response == null)
					throw new Exception("Fail to get news categories. Please try again later.");

			} catch (Exception e) {
				e.printStackTrace();
				mEx = e;
			}

			return mEx;
		}

		@Override
		protected void onPostExecute(Exception ex) {
			super.onPostExecute(ex);

			if(dialog.isShowing() && !ElasticNewsActivity.this.isFinishing())
				dialog.dismiss();

			if(ex != null){
				showAlertDialog(ex.getMessage());
			}else{
				if(response != null) {
					List<NewsCategory> mNewsCategories = QcMessageParser.parseNewsCategory(response);
					application.setNewsCategories(mNewsCategories);

					initializeView();
				}
			}
		}
	}

	private void initializeView(){
		btnPopupSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				preparePopupSearchForm();
			}
		});

		researchNewsCategories = NewsCategory.getNewsCategoryBySource(AppConstants.RESEARCH_REPORTS_SOURCE, this, false);
		newsCategories		   = NewsCategory.getNewsCategoryBySource(AppConstants.REUTERS_NEWS_SOURCE, this, false);

		NewsCategoryAdapter researchReportsAdapter = new NewsCategoryAdapter(ElasticNewsActivity.this, researchNewsCategories);
		lvResearchReportsCategories.setAdapter(researchReportsAdapter);

		if(newsCategories.size() == 1){
			NewsCategoryAdapter otherCategoriesAdapter = new NewsCategoryAdapter(ElasticNewsActivity.this, newsCategories);
			lvNewsCategories.setAdapter(otherCategoriesAdapter);
		}else{
			//TODO: group by source for CIMB MY
		}

		lvResearchReportsCategories.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> view, View arg1, int position, long arg3) {
				NewsCategory newsCat = researchNewsCategories.get(position);
				startElasticNewsSearchIntent(newsCat.getSource(), newsCat.getCategory(), newsCat.getName(), null, null, null, null, false);
			}
		});

		lvNewsCategories.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				NewsCategory newsCat = newsCategories.get(position);
				startElasticNewsSearchIntent(newsCat.getSource(), newsCat.getCategory(), newsCat.getName(), null, null, null, null, false);
			}
		});
	}

	private void preparePopupSearchForm(){

		final Dialog searchFormDialog = new Dialog(ElasticNewsActivity.this);
		searchFormDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		searchFormDialog.setContentView(R.layout.elastic_search_dialog);
		searchFormDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		//searchFormDialog.setCancelable(false);	//not enable this for now

		spnNewsSource 	= (Spinner)searchFormDialog.findViewById(R.id.spn_search_news_source);
		spnNewsCategory = (Spinner)searchFormDialog.findViewById(R.id.spn_search_news_category);
		etKeyword 		= (EditText)searchFormDialog.findViewById(R.id.et_search_keyword);
		chkSearchTargetHeadline = (CheckBox)searchFormDialog.findViewById(R.id.chk_search_target_headline);
		chkSearchTargetArticle 	= (CheckBox)searchFormDialog.findViewById(R.id.chk_search_target_article);
		rdGroupDuration		= (RadioGroup)searchFormDialog.findViewById(R.id.rdgroup_duration);
		btnCloseSearch	= (Button)searchFormDialog.findViewById(R.id.btn_cancel_search);
		btnSearch		= (Button)searchFormDialog.findViewById(R.id.btn_search);

		uniqueNewsSources 	   = NewsCategory.getUniqueNewsSources(this, true);

		NewsCategoryAdapter sourceAdapter = new NewsCategoryAdapter(ElasticNewsActivity.this, uniqueNewsSources);
		spnNewsSource.setAdapter(sourceAdapter);

		spnNewsSource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> view, View arg1, int position, long arg3) {
				mSource 	= uniqueNewsSources.get(position).getSource();
				changeCategorySpinner(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		rdGroupDuration.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				String strTodayDate 	= SystemUtil.getTodayDate("yyyy-MM-dd");
				final String FROM_TIME 	= " 00:00:00";
				final String TO_TIME 	= " 23:59:59";
				strToDate 	= strTodayDate + TO_TIME;

				switch(checkedId){
					case R.id.rd_duration_1day:
						strFromDate = strTodayDate + FROM_TIME;
						break;
					case R.id.rd_duration_7days:
						String pastDate = SystemUtil.getNDaysFromDate(strTodayDate, -7);
						strFromDate 	= pastDate + FROM_TIME;
						break;
					case R.id.rd_duration_30days:
						String pastDate2 = SystemUtil.getNDaysFromDate(strTodayDate, -30);
						strFromDate 	= pastDate2 + FROM_TIME;
						break;
				}
			}
		});

		rdGroupDuration.check(R.id.rd_duration_1day); 	//to trigger as default

		btnSearch.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				performNewsSearch();

				if(searchFormDialog != null && searchFormDialog.isShowing())
					searchFormDialog.dismiss();
			}
		});

		btnCloseSearch.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(searchFormDialog != null && searchFormDialog.isShowing())
					searchFormDialog.dismiss();
			}
		});

		searchFormDialog.show();
	}

	public void performNewsSearch(){
		mGuid 		= "";
		mKeyword 	= etKeyword.getText().toString().trim();

		if(chkSearchTargetHeadline.isChecked() && chkSearchTargetArticle.isChecked()){
			mTarget = "3";
		}else if(chkSearchTargetHeadline.isChecked()){
			mTarget = "2";
		}else if(chkSearchTargetArticle.isChecked()){
			mTarget = "1";
		}else{
			//TODO: alert to choose target
		}
		
		startElasticNewsSearchIntent(mSource, mCategory, "", mKeyword, mTarget, strFromDate, strToDate, true);
	}

	public void changeCategorySpinner(int position){
		String selectedSource	= uniqueNewsSources.get(position).getSource();
		final ArrayList<NewsCategory> newsCategories	= NewsCategory.getNewsCategoryBySource(selectedSource, this, true);
		NewsCategoryAdapter categoryAdapter 	= new NewsCategoryAdapter(ElasticNewsActivity.this, newsCategories);
		spnNewsCategory.setAdapter(categoryAdapter);

		spnNewsCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> v, View arg1, int position, long arg3) {
				mCategory = newsCategories.get(position).getCategory();
			}

			@Override
			public void onNothingSelected(AdapterView<?> v) {

			}
		});
	}

	private void startElasticNewsSearchIntent(String source, String category, String categoryName,
					String keyword, String target, String fromDate, String toDate, boolean isPerformSearcch){

		Intent searchNewsIntent = new Intent();
		searchNewsIntent.setClass(ElasticNewsActivity.this, ElasticNewsSearchActivity.class);

		if(source != null)
			searchNewsIntent.putExtra(ParamConstants.NEWS_SOURCE_TAG, source);
		
		if(category != null)
			searchNewsIntent.putExtra(ParamConstants.NEWS_CATEGORY_TAG, category);
		
		if(categoryName != null)
			searchNewsIntent.putExtra(ParamConstants.NEWS_CATEGORY_NAME_TAG, categoryName);

		searchNewsIntent.putExtra(ParamConstants.NEWS_IS_SEARCH_TAG, isPerformSearcch);

		if(isPerformSearcch){
			searchNewsIntent.putExtra(ParamConstants.NEWS_KEYWORD_TAG, keyword);
			searchNewsIntent.putExtra(ParamConstants.NEWS_TARGET_TAG, target);
			searchNewsIntent.putExtra(ParamConstants.NEWS_FROM_DATE_TAG, fromDate);
			searchNewsIntent.putExtra(ParamConstants.NEWS_TO_DATE_TAG, toDate);
		}

		startActivity(searchNewsIntent);
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		if (idleThread != null && idleThread.isPaused())
			idleThread.resumeRequest(false);
		
		loginThread = ((StockApplication) ElasticNewsActivity.this.getApplication()).getDoubleLoginThread();
		
		if (loginThread != null && loginThread.isPaused()) {
			loginThread.setRegisteredListener(ElasticNewsActivity.this);
			loginThread.resumeRequest();
		}
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message	= handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				
				if(loginThread != null)
					loginThread.stopRequest();
				
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				if( ElasticNewsActivity.this.mainMenu.isShowing() )
					ElasticNewsActivity.this.mainMenu.hide();
					
				if( !isFinishing() )
					ElasticNewsActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};
	
	@Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void showAlertDialog(String errorMsg){
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(errorMsg)
					.setCancelable(false)
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							ElasticNewsActivity.this.finish();
						}
					});

			AlertDialog alert = builder.create();
			if( ! this.isFinishing() )
				alert.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}