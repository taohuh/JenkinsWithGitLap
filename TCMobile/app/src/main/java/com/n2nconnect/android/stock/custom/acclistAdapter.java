package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class acclistAdapter extends BaseAdapter implements Filterable {

	private List<TradeAccount> accListOri	= new ArrayList<TradeAccount>();
	private List<TradeAccount> accList;
	private Context context;
	private ViewHolder holder 				= null;
	private LayoutInflater mInflater;
	private final Object mLock 				= new Object();
	private Filter filter;
	private TextView noresultFound;
	private int filterOpt;

	public acclistAdapter(Context context, List<TradeAccount> accList, TextView noresultFound, int selection){
		super();
		this.context = context;
		this.accList = accList;
		cloneItems(accList);
		this.noresultFound = noresultFound;
		this.filterOpt = selection;
	}

	protected void cloneItems(List<TradeAccount> items) {
		for (Iterator<TradeAccount> iterator = items.iterator(); iterator.hasNext();) {
			TradeAccount acc = iterator.next();
			accListOri.add(acc);
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		synchronized(mLock) {
			return accList!=null ? accList.size() : 0;  
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		TradeAccount account = null;
		synchronized(mLock) {
			account = (TradeAccount) (accList!=null ? accList.get(position) : null);
		}
		return account;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Filter getFilter() {
		if (filter == null) {
			filter = new TradeAccountFilter();
		}
		return filter;
	}
	
	public void setFilterOption(int filterOpt){
		this.filterOpt = filterOpt;
	}

	private class TradeAccountFilter extends Filter{

		@Override
		protected FilterResults performFiltering(CharSequence prefix) {
			// TODO Auto-generated method stub
			
			FilterResults results = new FilterResults();
			
			if (prefix == null || prefix.length() == 0) {
                synchronized (mLock) {
                    results.values	= accListOri;
                    results.count	= accListOri.size();
                }
            } else {
                synchronized(mLock) {
                    // Compare lower case strings
                    String prefixString						= prefix.toString().toLowerCase();
                    final List<TradeAccount> filteredItems	= new ArrayList<TradeAccount>();
                    // Local to here so we're not changing actual array
                    final List<TradeAccount> localItems		= new ArrayList<TradeAccount>();
                    localItems.addAll(accListOri);
                    final int count = localItems.size();

                    for (int i = 0; i < count; i++) {
                        final TradeAccount item = localItems.get(i);

                        // First match against the whole, non-split value
                        if(acclistAdapter.this.filterOpt == 0){
                        	 final String itemName = item.getClientName().toString().toLowerCase();
                        	 if (itemName.startsWith(prefixString)) {
                                 filteredItems.add(item);
                             } 
                        }else if(acclistAdapter.this.filterOpt == 1){
                        	final String itemAcc = item.getAccountNo().toString().toLowerCase();
                       	 if (itemAcc.startsWith(prefixString)) {
                                filteredItems.add(item);
                            } 
                        }
                    }

                    // Set and return
                    results.values	= filteredItems;
                    results.count	= filteredItems.size();
                }//end synchronized
            }
            return results;
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			// TODO Auto-generated method stub
            synchronized(mLock) {
                final List<TradeAccount> localItems = (List<TradeAccount>)results.values;
                
                /* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
                if(localItems.size()==0){
                	noresultFound.setVisibility(View.VISIBLE);
                }else{
                	noresultFound.setVisibility(View.GONE);
                }
                */
                noresultFound.setVisibility( (localItems.size()==0) ? View.VISIBLE : View.GONE );
                
                notifyDataSetChanged();
                accList.clear();
                //Add the items back in
                for (Iterator<TradeAccount> iterator = localItems.iterator(); iterator.hasNext();) {
                    TradeAccount acc = iterator.next();
                    accList.add(acc);
                }
            }//end synchronized
		}
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		// TODO Auto-generated method stub

		int[] backgrounds = new int[] {LayoutSettings.tablerow_light, LayoutSettings.tablerow_dark};

		if (convertView == null) {
			mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.trade_account_row,null);
			holder = new ViewHolder();
			holder.accNoLabel = (TextView)convertView.findViewById(R.id.accountNoLabel);
			holder.accNameLabel = (TextView)convertView.findViewById(R.id.clientNameLabel);

			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}

		int bgPos = position % backgrounds.length;

		TradeAccount account  = null;
		synchronized(mLock) {
			account = (TradeAccount) accList.get(position);
		}

		holder.accNoLabel.setText(account.getBranchCode()+" - "+account.getAccountNo());
		holder.accNameLabel.setText(account.getClientName());

		convertView.setBackgroundResource(backgrounds[bgPos]);

		return convertView;
	}

	private class ViewHolder {
		TextView accNoLabel;
		TextView accNameLabel;
	}

}
