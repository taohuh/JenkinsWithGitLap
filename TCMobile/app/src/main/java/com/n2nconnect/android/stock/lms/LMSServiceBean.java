package com.n2nconnect.android.stock.lms;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

public class LMSServiceBean {
    private String sSponsorID;
    private String sProductID;
    private String sCountryCode;
    private String sExchangeID;
    private String sExchangeName;
    private String sExchangeDescription;
    private String sExchangeType;
    private String sExchangeCode;
    private String sCurrency;
    private String sBitMode;
    private String sAppName;
    private String sExpiryDate;
    private String sExpiryDays;
    private String sSubscriptionFlag;
    private String sFeedURL;
    private String sTrdExchgCode;
    /* Mary@20130711 - Change_Request20130104, ReqNo.6
    private Integer sMarketDepthLvl;
    */
    private int sMarketDepthLvl;
    // Added by Mary@20130620 - Change_Request-20130424, ReqNo.9
    private String strFCBitMode;

    public LMSServiceBean() {
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("SponsorID=").append(sSponsorID).append("\r\n");
        buf.append("ProductID=").append(sProductID).append("\r\n");
        buf.append("CountryCode=").append(sCountryCode).append("\r\n");
        buf.append("ExchangeID=").append(sExchangeID).append("\r\n");
        buf.append("ExchangeName=").append(sExchangeName).append("\r\n");
        buf.append("ExchangeDescription=").append(sExchangeDescription).append("\r\n");
        buf.append("ExchangeType=").append(sExchangeType).append("\r\n");
        buf.append("ExchangeCode=").append(sExchangeCode).append("\r\n");
        buf.append("Currency=").append(sCurrency).append("\r\n");
        buf.append("BitMode=").append(sBitMode).append("\r\n");
     // Added by Kw@20130222 - Change_Request20130104, ReqNo.6
        buf.append("MarketDepthLvl=").append(sMarketDepthLvl).append("\r\n");
       // buf.append("AppName=").append(sAppName).append("\r\n");
        buf.append("ExpiryDate=").append(sExpiryDate).append("\r\n");
        buf.append("ExpiryDays=").append(sExpiryDays).append("\r\n");
        buf.append("SubscriptionFlag=").append(sSubscriptionFlag).append("\r\n");
        buf.append("FeedURL=").append(sFeedURL);
        buf.append("TradeExchangeCode=").append(sTrdExchgCode);
        // Added by Mary@20130620 - Change_Request-20130424, ReqNo.9
        buf.append("FCBitMode=").append(strFCBitMode);
        return buf.toString();
    }

    public String getSponsorID() {
        return sSponsorID;
    }

    public void setSponsorID(String sSponsorID) {
        this.sSponsorID = sSponsorID;
    }

    public String getProductID() {
        return sProductID;
    }

    public void setProductID(String sProductID) {
        this.sProductID = sProductID;
    }
    
    public void setTradeExchangeCode(String sTrdExchgCode){
    	this.sTrdExchgCode = sTrdExchgCode;
    }

    public String getCountryCode() {
        return sCountryCode;
    }
    
    public String getTradeExchangeCode() {
        return sTrdExchgCode;
    }

    public void setCountryCode(String sCountryCode) {
        this.sCountryCode = sCountryCode;
    }

    public String getExchangeID() {
        return sExchangeID;
    }

    public void setExchangeID(String sExchangeID) {
        this.sExchangeID = sExchangeID;
    }

    public String getExchangeName() {
        return sExchangeName;
    }

    public void setExchangeName(String sExchangeName) {
        this.sExchangeName = sExchangeName;
    }

    public String getExchangeDescription() {
        return sExchangeDescription;
    }

    public void setExchangeDescription(String sExchangeDescription) {
        this.sExchangeDescription = sExchangeDescription;
    }

    public String getExchangeType() {
        return sExchangeType;
    }

    public void setExchangeType(String sExchangeType) {
        this.sExchangeType = sExchangeType;
    }

    public String getExchangeCode() {
        return sExchangeCode;
    }

    public void setExchangeCode(String sExchangeCode) {
        this.sExchangeCode = sExchangeCode;
    }

    public String getCurrency() {
        return sCurrency;
    }

    public void setCurrency(String sCurrency) {
        this.sCurrency = sCurrency;
    }

    public String getBitMode() {
        return sBitMode;
    }

    public void setBitMode(String sBitMode) {
        this.sBitMode = sBitMode;
    }

    public String getAppName() {
        return sAppName;
    }

    public void setAppName(String sAppName) {
        this.sAppName = sAppName;
    }

    public String getExpiryDate() {
        return sExpiryDate;
    }

    public void setExpiryDate(String sExpiryDate) {
        this.sExpiryDate = sExpiryDate;
    }

    public String getExpiryDays() {
        return sExpiryDays;
    }

    public void setExpiryDays(String sExpiryDays) {
        this.sExpiryDays = sExpiryDays;
    }

    public String getSubscriptionFlag() {
        return sSubscriptionFlag;
    }

    public void setSubscriptionFlag(String sSubscriptionFlag) {
        this.sSubscriptionFlag = sSubscriptionFlag;
    }

    public String getFeedURL() {
        return sFeedURL;
    }

    public void setFeedURL(String sFeedURL) {
        this.sFeedURL = sFeedURL;
    }
 // Added by Kw@20130222 - Change_Request20130104, ReqNo.6 START
    /* Mary@20130711 - Change_Request20130104, ReqNo.6
    public Integer getMarketDepthLvl(){
    */
    public int getMarketDepthLvl(){
    	return sMarketDepthLvl;
    }
    
    /* Mary@20130711 - Change_Request20130104, ReqNo.6
    public void setMarketDepthLvl(Integer sMarketDepthLvl){
    */
    public void setMarketDepthLvl(int sMarketDepthLvl){
    	this.sMarketDepthLvl= sMarketDepthLvl;
    }
 // Added by Kw@20130222 - Change_Request20130104, ReqNo.6 END
    
    // Added by Mary@20130620 - Change_Request-20130424, ReqNo.9 - START
    public String getStrFCBitMode(){
    	return strFCBitMode;
    }
    public void setStrFCBitMode(String strFCBitMode){
    	this.strFCBitMode = strFCBitMode;
    }
    // Added by Mary@20130620 - Change_Request-20130424, ReqNo.9 - END
}
