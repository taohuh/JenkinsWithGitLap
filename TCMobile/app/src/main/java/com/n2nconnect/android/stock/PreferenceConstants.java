package com.n2nconnect.android.stock;


public class PreferenceConstants {
	
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	public static final String QC_USER_PARAM = "QcUserParam";
	*/
	public static final String ATP_USER_PARAM 				= "ATP_USER_PARAM";
	public static final String ATP_PARAM_STRING 			= "ATP_PARAM_STRING";
		
	public static final String SENDER_CODE 					= "SENDER_CODE";
	public static final String BROKER_CODE 					= "BROKER_CODE";
	public static final String USER_NAME 					= "USER_NAME";
	// Added by Mary@20130528 - Change_Request-20130424, ReqNo.4
	public static final String IS_REMEMBER_ME				= "IS_REMEMBER_ME";
	
	public static final String REFRESH_RATE					= "REFRESH_RATE";
	public static final String EXCHANGE_CODE 				= "EXCHANGE_CODE";
	public static final String ACCOUNT_NO 					= "ACCOUNT_NO";
	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
	public static final String BROKER_INIT					= "BROKER_INIT";
	public static final String BROKER_PREFERENCE			= "BROKER_PREFERENCE";
	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	
	// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12 - START
	public static final String IS_AUTOFILL_TRADE_PRICE		= "IS_AUTOFILL_TRADE_PRICE";
	public static final String IS_AUTOFILL_TRADE_QUANTITY	= "IS_AUTOFILL_TRADE_QUANTITY";
	public static final String CUSTOMED_TRADE_QUANTITY		= "CUSTOMED_TRADE_QUANTITY";
	public static final String TRADE_QUANTITY_MEASUREMENT	= "TRADE_QUANTITY_MEASUREMENT";
	// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12 - END
	// Added by Mary@20121112 - Change_Request-20120719, ReqNo.12
	public static final String TRADE_PREFERENCE_EXCHG_CODE	= "TRADE_PREFERENCE_EXCHG_CODE";
	// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
	public static final String TRADE_EXCHANGE_CODE			= "TRADE_EXCHANGE_CODE";
	
	// Added by Mary@20121123 - Changes_Request-20121106, ReqNo.2
	public static final String VIEW_QUANTITY_MEASUREMENT	= "VIEW_QUANTITY_MEASUREMENT";
	
	public static final String BRANCH_CODE 					= "BranchCode";

	public static final String IS_APP_EXIT 					= "IsAppExit";
	
	//Added by Thinzar@20140702 - Change_Request-20140701, ReqNo.1
	public static final String CURRENT_PACKAGE_NAME = "CURRENT_PKG_NAME";
	public static final String HAS_MULTI_LOGIN = "HAS_MULTI_LOGIN";
	public static final String LAST_USED_PKG = "LAST_USED_PKG";
	
	//Added by Thinzar, Fixes_Request-20150601, ReqNo.3
	public static final String WATCHLIST_FAV_ID				= "WATCHLIST_FAV_ID";
	
	public static final String HAS_TUTORIAL_SHOWN			= "hasTutorialShown";
	public static final String APP_LINK_DATA_TAG			= "appLinkData";
	
	//Change_Request_20170119, ReqNo.5
	public static final String DTI_DO_NOT_SHOW_DATE			= "DTI_DO_NOT_SHOW_DATE";

	// Added by Thinzar, Change_Request-20160722, ReqNo.8
	public static final String VALIDATE_PRE_LOGIN_TOKEN = "preLoginToken";
	public static final String IS_PRE_LOGIN_VALIDATED 	= "isPreLoginValidated";

	public static final String KEY_NAME 				= "com.n2nconnect.android.stock";
	public static final String KEY_PASSWORD 			= "EncryptedPassword";
	public static final String KEY_PASSWORD_IV 			= "EncryptedPasswordIV";
	public static final String KEY_USERNAME 			= "TOUCHID_USERNAME";
	
	//for push notifications
	public static final String GCM_PREF_FILE			= "GCM_INFO";
	public static final String GCM_USERNAME				= "GCM_USERNAME";
	public static final String GCM_REG_ID 				= "GCM_REGISTERATION_ID";
	public static final String HAS_DEVICE_REGISTERED	= "HAS_DEVICE_REGISTERED";
	public static final String GCM_REG_APP_VERSION		= "GCM_REG_APP_VERSION";
	public static final String GCM_REG_OS_VERSION		= "GCM_REG_OS_VERSION";
	public static final String DO_NOT_ASK_PUSH_NOTI		= "DO_NOT_ASK_PUSH_NOTI";
}