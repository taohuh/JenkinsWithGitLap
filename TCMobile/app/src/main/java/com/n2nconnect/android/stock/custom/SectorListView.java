package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.model.Sector;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

/**
 *
 * This is the class that manages our menu items and the popup window.
 */
public class SectorListView {

	/**
	 * Some global variables.
	 */
	private List sectors;
	private OnSectorSelectedListener listener;
	private Context context;
	private LayoutInflater layoutInflater;
	private PopupWindow popupWindow;
	private boolean isShowing;
	private boolean hideOnSelect;
	private Sector selectedSector;
	private int selectedRow;
	private int childRow;
	private boolean isChildRefresh;
	private View windowView;
	
	public void positionSector() {
		
		ScrollView parentScroll = (ScrollView) windowView.findViewById(R.id.parentScroll);
		ScrollView childScroll = (ScrollView) windowView.findViewById(R.id.childScroll);
		Sector parentSector = null;
		Sector childSector = null;
		if (selectedSector.getParentSector()==null) {
			parentSector = selectedSector;
		} else {
			parentSector = selectedSector.getParentSector();
			childSector = selectedSector;
		}
		int index = 1;
		for (Iterator itr=sectors.iterator(); itr.hasNext(); ) {
			Sector compare = (Sector) itr.next();
			if (compare.getSectorCode().equals(parentSector.getSectorCode())) {
				break;
			}
			index++;
		}

		int scrollHeight = (index * 35) - 80;
		if (scrollHeight>0) {
			parentScroll.scrollTo(0, scrollHeight);
		}
		TableLayout tableLayout = (TableLayout) windowView.findViewById(R.id.parentTable);
		TableRow tableRow = (TableRow) tableLayout.getChildAt(index-1);
		TextView textLabel = (TextView) tableRow.getChildAt(0);
		textLabel.setBackgroundColor(Color.DKGRAY);
		tableRow.setBackgroundColor(Color.DKGRAY);
		textLabel.setTextColor(Color.WHITE);
  		selectedRow = index - 1;
  		
		if (childSector!=null) {
			index = 1;
			for (Iterator<Sector> itr=parentSector.getChildSectors().iterator(); itr.hasNext(); ) {
				Sector compare = itr.next();
				if (compare.getSectorCode().equals(childSector.getSectorCode())) {
					break;
				}
				index++;
			}
			if(index>7){
				scrollHeight = (index * 70) - 80;
			}else{
				scrollHeight = (index * 35) - 80;
			}
			if (scrollHeight>0) {
				childScroll.scrollTo(0, scrollHeight);
			}
			
			tableLayout = (TableLayout) windowView.findViewById(R.id.childTable);
			tableRow = (TableRow) tableLayout.getChildAt(index-1);
			textLabel = (TextView) tableRow.getChildAt(0);
			textLabel.setBackgroundColor(Color.DKGRAY);
			tableRow.setBackgroundColor(Color.DKGRAY);
			textLabel.setTextColor(Color.WHITE);
			childRow = index - 1;
			isChildRefresh = false;
		}
		
		
	}
	
	public interface OnSectorSelectedListener {
		public void sectorSelectedEvent(Sector selection);
	}
	
	public boolean isShowing() { 
		return isShowing; 
	}
		
	public void setHideOnSelect(boolean doHideOnSelect) { 
		hideOnSelect = doHideOnSelect; 
	} 
	
	public synchronized void setSectorList(List sectors) throws Exception {
		if (isShowing) {
			throw new Exception("Menu list may not be modified while menu is displayed.");
		}
		this.sectors = sectors;
	}
	
	public SectorListView(Context context, OnSectorSelectedListener listener, LayoutInflater inflater) {
		this.listener = listener;
		sectors = new ArrayList();
		this.context = context;
		layoutInflater = inflater;
	}
	
	public synchronized void show(View view, Sector sector) {
		
		this.selectedSector = sector;
		isShowing = true;
		boolean isLandscape = false;
		int itemCount = sectors.size();
		if (itemCount<1) {
			return; 
		}
		if (popupWindow != null) {
			return; 
		}
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		if (display.getWidth() > display.getHeight()) {
			isLandscape = true;
		}
		
//		View windowView = layoutInflater.inflate(R.layout.sector_list_window, null);
		windowView = layoutInflater.inflate(R.layout.sector_table_window, null);

        this.constructTableView();
        if (selectedSector.getParentSector()!=null) {
        	this.constructChildTable(selectedSector.getParentSector().getChildSectors());
        }
//        ListView parentList = (ListView) windowView.findViewById(R.id.parentList);        
//        ArrayAdapter parentAdapter = new ArrayAdapter(context, R.layout.list_item, sectors);
//        
//        parentList.setAdapter(parentAdapter);
//        parentList.setItemsCanFocus(false);
//        parentList.setFocusable(true);
//
//        parentList.setOnItemClickListener(new OnItemClickListener() {
//
//			public void onItemClick(AdapterView<?> parent, View view, int position,
//					long id) {          
//	           String item = ((TextView)view).getText().toString();
//	           Sector sector = (Sector) parent.getAdapter().getItem(position);
//	           Toast.makeText(context, ((TextView) view).getText(),
//	        	          Toast.LENGTH_SHORT).show();
//	        }
//
//		});
        
        Button doneButton = (Button) windowView.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View view) {
        		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(CustomWindow.idleThread != null && ! CustomWindow.isShowingIdleAlert)
					CustomWindow.idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
        		SectorListView.this.hide();
        		listener.sectorSelectedEvent(selectedSector);
        	}
        });
        
		popupWindow = new PopupWindow(windowView, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, false);
        popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
        popupWindow.setWidth(display.getWidth());
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        
        windowView.post(new Runnable() {
        	public void run() {
        		SectorListView.this.positionSector();
        	}
        });
        		
	}
	
	private void constructTableView() {
		
		final TableLayout tableLayout = (TableLayout) windowView.findViewById(R.id.parentTable);
   	 
        int id = 0;
        for (Iterator itr=sectors.iterator(); itr.hasNext(); ) {
        	Sector sector = (Sector) itr.next();
       	 
            final TableRow tableRow = new TableRow(context);   
            tableRow.setId(id);
            tableRow.setLayoutParams(new TableLayout.LayoutParams(
           		 TableLayout.LayoutParams.MATCH_PARENT,
           		 TableLayout.LayoutParams.WRAP_CONTENT));
            
            tableRow.setWeightSum(1);
            
            final TextView textLabel = (TextView) layoutInflater.inflate(R.layout.sector_list_item, null);
            textLabel.setText(sector.getSectorName());
                        
            tableRow.addView(textLabel);
            
            tableRow.setOnClickListener(new OnClickListener() {
           	 
	           	 public void onClick(View view) {
	           		 
	           		 final TableRow previousRow = (TableRow) tableLayout.getChildAt(selectedRow);
	           		 TextView previousLabel = (TextView) previousRow.findViewById(R.id.sectorLabel);
	           		 previousLabel.setBackgroundDrawable(null);
	           		 previousRow.setBackgroundDrawable(null);
	           		 previousLabel.setTextColor(Color.BLACK);
	           		 
	           		 selectedSector = (Sector) sectors.get(tableRow.getId());
	           		 selectedRow = tableRow.getId();
	           		 
	           		 textLabel.setBackgroundColor(Color.DKGRAY);
	           		 tableRow.setBackgroundColor(Color.DKGRAY);
	           		 textLabel.setTextColor(Color.WHITE);
	           		 
	           		 
	           		TableLayout childLayout = (TableLayout) windowView.findViewById(R.id.childTable);
	           		childLayout.removeAllViews();
	           		isChildRefresh = true;
	           		
	           		 if (!selectedSector.getChildSectors().isEmpty()) {
	           			 SectorListView.this.constructChildTable(selectedSector.getChildSectors());
	           		 }
	           		 
	           	 }
            });
            
            // Add the TableRow to the TableLayout
            tableLayout.addView(tableRow, new TableLayout.LayoutParams(
           		 TableRow.LayoutParams.FILL_PARENT,
           		 TableRow.LayoutParams.WRAP_CONTENT));  
            id++;
       }
      
      
	}
	
	private void constructChildTable(final List<Sector> childSectors) {
		final TableLayout tableLayout = (TableLayout) windowView.findViewById(R.id.childTable);
	   	 
        int id = 0;
        for (Iterator<Sector> itr=childSectors.iterator(); itr.hasNext(); ) {
        	Sector sector = itr.next();
       	 
            final TableRow tableRow = new TableRow(context);   
            tableRow.setId(id);
            tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
            tableRow.setWeightSum(1);
            final TextView textLabel = (TextView) layoutInflater.inflate(R.layout.sector_list_item, null);
            textLabel.setText(sector.getSectorName());
            
            tableRow.addView(textLabel);
            
            tableRow.setOnClickListener(new OnClickListener() {
           	 
	           	 public void onClick(View view) {
	           		 if (!isChildRefresh) {
		           		 final TableRow previousRow = (TableRow) tableLayout.getChildAt(childRow);
		           		 TextView previousLabel = (TextView) previousRow.findViewById(R.id.sectorLabel);
		           		 previousRow.setBackgroundDrawable(null);
		           		 previousLabel.setBackgroundDrawable(null);
		           		 previousLabel.setTextColor(Color.BLACK);
	           		 }
	           		 isChildRefresh = false;
	           		 
	           		 selectedSector = (Sector) childSectors.get(tableRow.getId());
	           		 childRow = tableRow.getId();
	           		 tableRow.setBackgroundColor(Color.DKGRAY);
	           		 textLabel.setBackgroundColor(Color.DKGRAY);
	           		 textLabel.setTextColor(Color.WHITE);
	           		 
	           		
	           	 }
            });
            
            // Add the TableRow to the TableLayout
            tableLayout.addView(tableRow, new TableLayout.LayoutParams(
           		 TableRow.LayoutParams.MATCH_PARENT,
           		 TableRow.LayoutParams.WRAP_CONTENT));  
            id++;
       }
	
	}
	
	public synchronized void hide() {
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
		try{
			isShowing = false;
			if(popupWindow != null && popupWindow.isShowing()){
				popupWindow.dismiss();
				popupWindow = null;
			}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
		return;
	}
}
