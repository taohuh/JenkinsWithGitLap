package com.n2nconnect.android.stock;

import java.util.List;
//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 
public class Info2FA {
	private boolean is2FARequired;
	private List<String> deviceList;
	private List<String> icNumberList;
	// Added by Mary@20130918 - Change_Request-20130724, ReqNo.14 - START
	private boolean hasSuccessVerify2FA;
	private boolean isForceUse1FA;
	private boolean is1FAAllowed;
	// Added by Mary@20130918 - Change_Request-20130724, ReqNo.14 - END

	//Change_Request-20170601, ReqNo.10
	private String mobilePhone2FA;
	private int smsOtpInterval;

	public Info2FA(){
		
	}
	
	public void Info2FAFlush(){
		is2FARequired	= false;
		deviceList 		= null;
		icNumberList	= null;
		// Added by Mary@20130918 - Change_Request-20130724, ReqNo.14 - START
		hasSuccessVerify2FA	= false;
		isForceUse1FA	= false;
		is1FAAllowed	= false;
		// Added by Mary@20130918 - Change_Request-20130724, ReqNo.14 - END
	}
	
	public void setIs2FARequired(boolean is2FARequired){
		this.is2FARequired = is2FARequired;
	}
	
	public boolean is2FARequired(){
		return is2FARequired;
	}
	
	public void setDeviceList(List<String> deviceList){
		this.deviceList = deviceList;
		
	}
	
	public List<String> getDeviceList(){
		return deviceList;
	}
	
	public void setICNumberList(List<String> icNumberList){
		this.icNumberList = icNumberList;
	}
	
	public List<String> getICNumberList(){
		return icNumberList;
	}
	
	// Added by Mary@20130918 - Change_Request-20130724, ReqNo.14 - START
	public void setHasSuccessVerify2FA(boolean hasSuccessVerify2FA){
		this.hasSuccessVerify2FA = hasSuccessVerify2FA;
	}
	
	public boolean hasSuccessVerify2FA(){
		return hasSuccessVerify2FA;
	}
	
	public boolean isForceUse1FA() {
		return isForceUse1FA;
	}

	public void setForceUse1FA(boolean isForceUse1FA) {
		this.isForceUse1FA = isForceUse1FA;
	}

	public boolean is1FAAllowed() {
		return is1FAAllowed;
	}

	public void set1FAAllowed(boolean is1FAAllowed) {
		this.is1FAAllowed = is1FAAllowed;
	}
	// Added by Mary@20130918 - Change_Request-20130724, ReqNo.14 - END

	public String getMobilePhone2FA() {
		return mobilePhone2FA;
	}

	public void setMobilePhone2FA(String mobilePhone2FA) {
		this.mobilePhone2FA = mobilePhone2FA;
	}

	public void setSmsOtpInterval(int smsOtpInterval) {
		this.smsOtpInterval = smsOtpInterval;
	}

	public int getSmsOtpInterval() {
		return smsOtpInterval;
	}
}
