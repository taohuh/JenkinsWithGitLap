package com.n2nconnect.android.stock.custom;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.activity.DtiDetailsActivity;
import com.n2nconnect.android.stock.activity.TradeActivity;
import com.n2nconnect.android.stock.model.DtiResult;
import com.n2nconnect.android.stock.model.StockInfo;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class DtiAdapter extends ArrayAdapter<DtiResult>{

	private ArrayList<DtiResult> mDtiResultArrayList;
	private Context ctx;
	private int layoutResourceId;
	private final String TAG	= "DtiAdapter";
	private int intRetryCtr;
	private ProgressDialog mDialog;
	private AlertDialog alertException;

	private class ViewHolder {
		TextView txtInstrumentName;
		TextView txtSymbolCommonExchg;
		TextView txtCountry;
		TextView txtEventTypeName;
		TextView txtDescription;
		TextView txtTradeTypeDescription;
		TextView txtExpectedPercentMove;
		LinearLayout layoutLeft;
		ImageView btnTradeDti;
	}

	public DtiAdapter(Context context, int resource, ArrayList<DtiResult> objects) {
		super(context, resource, objects);

		this.mDtiResultArrayList = objects;
		this.ctx				= context;
		this.layoutResourceId	= resource;
		mDialog					= new ProgressDialog(ctx);
	}

	@Override
	public int getCount() {
		return mDtiResultArrayList.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final DtiResult dti	= mDtiResultArrayList.get(position);

		ViewHolder viewHolder;
		if(convertView == null){
			viewHolder = new ViewHolder();
			LayoutInflater inflater	= LayoutInflater.from(ctx);
			convertView	= inflater.inflate(layoutResourceId, parent, false);

			viewHolder.txtInstrumentName	= (TextView) convertView.findViewById(R.id.txt_instrument_name);
			viewHolder.txtSymbolCommonExchg	= (TextView) convertView.findViewById(R.id.txt_symbol_common_exchange);
			viewHolder.txtCountry			= (TextView) convertView.findViewById(R.id.txt_country);
			viewHolder.txtEventTypeName		= (TextView) convertView.findViewById(R.id.txt_event_type_name);
			viewHolder.txtDescription		= (TextView) convertView.findViewById(R.id.txt_description);
			viewHolder.txtTradeTypeDescription	= (TextView) convertView.findViewById(R.id.txt_trade_type_description);
			viewHolder.txtExpectedPercentMove	= (TextView) convertView.findViewById(R.id.txt_expected_percent_move);
			viewHolder.layoutLeft			= (LinearLayout) convertView.findViewById(R.id.layout_left);
			viewHolder.btnTradeDti			= (ImageView) convertView.findViewById(R.id.btn_trade_dti);

			convertView.setTag(viewHolder);
		}else{
			viewHolder	= (ViewHolder)convertView.getTag();
		}

		viewHolder.txtInstrumentName.setText(dti.getInstrumentName());
		viewHolder.txtSymbolCommonExchg.setText(dti.getInstrumentSymbolCommon() + " (" + dti.getInstrumentExchange() + ")");
		viewHolder.txtCountry.setText(dti.getInfo(ParamConstants.DTI_COUNTRY_TAG).toUpperCase());
		viewHolder.txtCountry.setBackgroundColor(ctx.getResources().getColor(Integer.parseInt(dti.getInfo(ParamConstants.DTI_BG_RESID_TAG))));
		viewHolder.txtEventTypeName.setText(dti.getEventTypeName());
		viewHolder.txtTradeTypeDescription.setText(dti.getTradeTypeDescription());

		if(dti.getExpectedPercentageMove() != null && dti.getExpectedPercentageMove() > 0){
			int percentMove	= (int) Math.round(dti.getExpectedPercentageMove() * 100);

			viewHolder.txtExpectedPercentMove.setVisibility(View.VISIBLE);
			viewHolder.txtExpectedPercentMove.setText(percentMove + "% move possible");
		}else{
			viewHolder.txtExpectedPercentMove.setVisibility(View.GONE);
		}

		Double closePrice	= Double.parseDouble(dti.getPricingClose());
		String shortDescription	= "Trading closed at " + FormatUtil.formatDouble(closePrice).toString() 
				+ " to confirm " + dti.getTradingHorizonDetails() + " "
				+ dti.getTradeTypeShortDescr() + " " + dti.getEventTypeName() + " pattern.";
		viewHolder.txtDescription.setText(shortDescription);

		viewHolder.layoutLeft.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {

				Intent dtiDetailIntent	= new Intent();
				dtiDetailIntent.setClass(ctx, DtiDetailsActivity.class);
				dtiDetailIntent.putExtra(ParamConstants.DTI_ID_TAG, dti.getId());
				dtiDetailIntent.putExtra(ParamConstants.DTI_INSTRUMENT_SYM_SOMMON_TAG, dti.getInstrumentSymbolCommon());
				dtiDetailIntent.putExtra(ParamConstants.DTI_INSTRUMENT_EXCHG_TAG, dti.getInstrumentExchange());
				dtiDetailIntent.putExtra(ParamConstants.DTI_INSTRUMENT_ISIN_TAG, dti.getInstrumentIsin());
				dtiDetailIntent.putExtra(ParamConstants.DTI_TRADE_TYPE_TAG, dti.getTradeType());

				ctx.startActivity(dtiDetailIntent);
			}
		});

		viewHolder.btnTradeDti.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(AppConstants.noTradeClient){
					showAlertDialog("You do not have a trading account", "", false);
				}else{
					String iTradeExchangeCode	= SystemUtil.getItradeExchgCode(dti.getInstrumentExchange());

					if(iTradeExchangeCode == null){
						showAlertDialog(ctx.getResources().getString(R.string.dti_activate_cross_border_account), "", false);
					}else{
						String fullSymbolCode	= dti.getInstrumentSymbolCommon() + "." + iTradeExchangeCode;
						new FetchStockInfo().execute(fullSymbolCode);
					}
				}
			}
		});

		return convertView;
	}		

	private void startTradeActivity(String mSymbolCode, String mLotSize, String mInstrument, boolean mIsPayableWithCPF) {

		Intent intent = new Intent();
		intent.putExtra(ParamConstants.SYMBOLCODE_TAG, mSymbolCode);
		intent.putExtra(ParamConstants.LOT_SIZE_TAG, mLotSize);
		intent.putExtra(ParamConstants.INSTRUMENT_TAG, mInstrument);
		intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, mIsPayableWithCPF);
		intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
		intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
		//intent.putExtra(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG, ParamConstants.PARAM_ORDER_SOURCE_DTI);	

		intent.setClass(ctx, TradeActivity.class);

		ctx.startActivity(intent);
	}

	private void showAlertDialog(String alertMsg, String title, final boolean isExit){
		alertException 	= new AlertDialog.Builder(ctx)
		//.setIcon(android.R.attr.alertDialogStyle)
		.setMessage(alertMsg)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				if(isExit){
					((Activity) ctx).finish();
				}
				return;
			}
		}).create();
		
		if(title.length() > 0)
			alertException.setTitle(title);

		alertException.show();
	}

	private class FetchStockInfo extends AsyncTask<String, Void, String[]> {
		private Exception exception 		= null;
		private StockInfo stockInfo;

		@Override
		protected void onPreExecute() {
			Log.i(TAG, "FetchStockInfo Task");
			super.onPreExecute();

			if(!((Activity) ctx).isFinishing()){
				mDialog.setMessage("fetching StockInfo");
				mDialog.show();
			}
		}

		@Override
		protected String[] doInBackground(String... params) {

			String[] symbolCodes 	= { params[0] };

			String response = null;
			try {
				intRetryCtr = 0;

				response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
				}
				intRetryCtr = 0;

				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);

			} catch (Exception e) {
				exception = e;
			}

			if(response != null)
				stockInfo = QcMessageParser.parseStockDetail(response);

			return symbolCodes;
		}

		@Override
		protected void onPostExecute(String[] result) {
			super.onPostExecute(result);

			if (mDialog != null && mDialog.isShowing())
				mDialog.dismiss();

			if (exception != null) {
				alertException.setMessage(exception.getMessage());

				if (!((Activity) ctx).isFinishing())
					alertException.show();
				return;
			} else{
				String strSymbolCode	= stockInfo.getSymbolCode();

				if(strSymbolCode == null || strSymbolCode.isEmpty())
					showAlertDialog(ctx.getResources().getString(R.string.stock_not_found_dti), 
							ctx.getResources().getString(R.string.stock_not_found_dti_title), false);
				else
					startTradeActivity(stockInfo.getSymbolCode(), Integer.toString(stockInfo.getSharePerLot()), stockInfo.getInstrument(), stockInfo.isPayableWithCPF());

			}
		}
	}
}