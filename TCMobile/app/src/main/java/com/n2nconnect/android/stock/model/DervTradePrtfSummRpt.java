package com.n2nconnect.android.stock.model;

import java.util.ArrayList;

public class DervTradePrtfSummRpt {
	//values get from ATP
	private float buyOptionMktVal;
	private float sellOptionMktVal;
	private float cashBalance;
	private float deposit;
	private float withdrawal;
	private float eligibleCollateral;
	private float initialMargin;
	private float maintenanceMargin;
	private float marginCall;
	private float realisedPL;
	
	//values calculated
	private float currentBalance;
	private float unrealisedPL;
	private float equity;
	private float netLiquidation;
	private float excessOrShortfall;
	private float eligibility;
	private float grossBuy;
	private float grossSell;
	
	
	public float getBuyOptionMktVal() {
		return buyOptionMktVal;
	}
	
	public void setBuyOptionMktVal(float buyOptionMktVal) {
		this.buyOptionMktVal = buyOptionMktVal;
	}

	public float getSellOptionMktVal() {
		return sellOptionMktVal;
	}

	public void setSellOptionMktVal(float sellOptionMktVal) {
		this.sellOptionMktVal = sellOptionMktVal;
	}

	public float getCashBalance() {
		return cashBalance;
	}

	public void setCashBalance(float cashBalance) {
		this.cashBalance = cashBalance;
	}

	public float getDeposit() {
		return deposit;
	}

	public void setDeposit(float deposit) {
		this.deposit = deposit;
	}

	public float getWithdrawal() {
		return withdrawal;
	}

	public void setWithdrawal(float withdrawal) {
		this.withdrawal = withdrawal;
	}

	public float getEligibleCollateral() {
		return eligibleCollateral;
	}

	public void setEligibleCollateral(float eligibleCollateral) {
		this.eligibleCollateral = eligibleCollateral;
	}

	public float getInitialMargin() {
		return initialMargin;
	}

	public void setInitialMargin(float initialMargin) {
		this.initialMargin = initialMargin;
	}

	public float getMaintenanceMargin() {
		return maintenanceMargin;
	}

	public void setMaintenanceMargin(float maintenanceMargin) {
		this.maintenanceMargin = maintenanceMargin;
	}

	public float getMarginCall() {
		return marginCall;
	}

	public void setMarginCall(float marginCall) {
		this.marginCall = marginCall;
	}

	public float getRealisedPL() {
		return realisedPL;
	}

	public void setRealisedPL(float realisedPL) {
		this.realisedPL = realisedPL;
	}

	public float getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(float currentBalance) {
		this.currentBalance = currentBalance;
	}
	
	public void calculateCurrentBalance(){
		this.setCurrentBalance(cashBalance+deposit-withdrawal+realisedPL);
	}

	public float getUnrealisedPL() {
		return unrealisedPL;
	}

	public void setUnrealisedPL(float unrealisedPL) {
		this.unrealisedPL = unrealisedPL;
	}
	
	public void calculateUnrealisedPL(ArrayList<DervTradePrtfSubDetailRpt> dervTradePrtfSubDetailRptList){
		
		float totalUnrealised = 0.0f;
		for(DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt:dervTradePrtfSubDetailRptList){
			//Edited by Thinzar, Fixes_Request-20140929, ReqNo.10 [Added checking for the need of currency conversion]
			if(dervTradePrtfSubDetailRpt.getCurrencySymbol().equalsIgnoreCase(dervTradePrtfSubDetailRpt.getHomeCurrency())){
				totalUnrealised += dervTradePrtfSubDetailRpt.getUnrealisedPL();
			} else {
				totalUnrealised += (dervTradePrtfSubDetailRpt.getUnrealisedPL()*dervTradePrtfSubDetailRpt.getForexExchangeRate());
			}
			
		}
		this.setUnrealisedPL(totalUnrealised);
	}

	public float getEquity() {
		return equity;
	}

	public void setEquity(float equity) {
		this.equity = equity;
	}

	public void calculateEquity(){
		
		this.setEquity(currentBalance + getUnrealisedPL());
	}
	
	public float getNetLiquidation() {
		return netLiquidation;
	}

	public void setNetLiquidation(float netLiquidation) {
		this.netLiquidation = netLiquidation;
	}

	public void calculateNetLiquidation(){
		this.setNetLiquidation(equity+buyOptionMktVal+sellOptionMktVal);
	}
	
	public float getExcessOrShortfall() {
		return excessOrShortfall;
	}

	public void setExcessOrShortfall(float excessOrShortfall) {
		this.excessOrShortfall = excessOrShortfall;
	}
	
	public void calculateExcessOrShortfall(){
		this.setExcessOrShortfall(netLiquidation+eligibleCollateral-initialMargin);
	}

	public float getEligibility() {
		return eligibility;
	}

	public void setEligibility(float eligibility) {
		this.eligibility = eligibility;
	}

	public void calculateEligibility(){
		if(initialMargin>0){
			this.setEligibility(((netLiquidation + eligibleCollateral)/initialMargin)*100);
		}else{
			this.setEligibility(0);
		}
	}
	
	public float getGrossBuy() {
		return grossBuy;
	}

	public void setGrossBuy(float grossBuy) {
		this.grossBuy = grossBuy;
	}

	public void calculateGrossBuy(ArrayList<DervTradePrtfSubDetailRpt> dervTradePrtfSubDetailRptList){
		float totalOpenLongDayLong = 0.0f;
		for(DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt:dervTradePrtfSubDetailRptList){
			totalOpenLongDayLong = totalOpenLongDayLong + dervTradePrtfSubDetailRpt.getBfBuy() + dervTradePrtfSubDetailRpt.getDayBuy();
		}
		this.setGrossBuy(totalOpenLongDayLong);
	}
	
	public float getGrossSell() {
		return grossSell;
	}

	public void setGrossSell(float grossSell) {
		this.grossSell = grossSell;
	}
	
	public void calculateGrossSell(ArrayList<DervTradePrtfSubDetailRpt> dervTradePrtfSubDetailRptList){
		float totalOpenShortDayShort = 0.0f;
		for(DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt:dervTradePrtfSubDetailRptList){
			totalOpenShortDayShort = totalOpenShortDayShort + dervTradePrtfSubDetailRpt.getBfSell() + dervTradePrtfSubDetailRpt.getDaySell();
		}
		this.setGrossSell(totalOpenShortDayShort);
	}
	
}
