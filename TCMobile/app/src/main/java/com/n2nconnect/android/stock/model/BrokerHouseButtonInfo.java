package com.n2nconnect.android.stock.model;

public class BrokerHouseButtonInfo {
	private String strClassName;
	private int intImageResourceID;
	private String strName;
	
	public String getStrClassName() {
		return strClassName;
	}
	public void setStrClassName(String strClassName) {
		this.strClassName = strClassName;
	}
	public int getIntImageResourceID() {
		return intImageResourceID;
	}
	public void setIntImageResourceID(int intImageResourceID) {
		this.intImageResourceID = intImageResourceID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
}