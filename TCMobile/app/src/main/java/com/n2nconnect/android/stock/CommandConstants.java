package com.n2nconnect.android.stock;


public class CommandConstants {
	public final static int IMAGE_SORT_MOVER 			= 0;
	public final static int IMAGE_SORT_GAINER 			= 1;
	public final static int IMAGE_SORT_LOSER 			= 2;	
	public final static int IMAGE_SORT_GAINER_PERCENT 	= 3;
	public final static int IMAGE_SORT_LOSER_PERCENT 	= 4;
	public final static int IMAGE_SORT_TRADE_VALUE 		= 5;
	public final static int IMAGE_SORT_NAME				= 6;		//Added by Thinzar@20141009, Change_Request-20140901,ReqNo.8
	public final static int IMAGE_SORT_DERIVATIVES		= 7;		//Added by Thinzar@20150515, Change_Request-20150401, ReqNo.2
	
	public final static int CHART_TYPE_INTRADAY 		= 0;
	public final static int CHART_TYPE_HISTORICAL 		= 1;
	public final static int CHART_TYPE_INTRADAY_LANDSCAPE 	= 2;
	public final static int CHART_TYPE_HISTORICAL_LANDSCAPE = 3;
	public final static int CHART_TYPE_MARKET_INTRADAY 	= 4;
	
	public final static int IMAGE_QUOTE_REFRESH 		= 0;
	public final static int IMAGE_QUOTE_DETAIL 			= 1;
	public final static int IMAGE_QUOTE_MARKETDEPTH 	= 2;
	public final static int IMAGE_QUOTE_TRADE 			= 3;
	public final static int IMAGE_QUOTE_CURRENCY 		= 4;
	
	private static String SectorCommand = null;
	private static String ExchangeCode=null;
	
	public final static void setCurrentExchgCode(String exchgCode){
		ExchangeCode = exchgCode; 
	}
	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
	//Edited by Thinzar@20141009, Change_Request-20140901,ReqNo.8 (added index 6: sort by name, for derivative feed) 
	//Edited by Thinzar, Change_Request-20150401, ReqNo.2 = index 7: derivatives sort
	public final static String[] IMAGE_SORT_COMMANDS = {
		"Image?[SORT]=0,16,1,%SECTOR,%COUNT,%PAGE,%EXCHANGE,0",
		"Image?[SORT]=0,13,1,%SECTOR,%COUNT,%PAGE,%EXCHANGE,0",
		"Image?[SORT]=0,13,0,%SECTOR,%COUNT,%PAGE,%EXCHANGE,0",
		"Image?[SORT]=0,14,1,%SECTOR,%COUNT,%PAGE,%EXCHANGE,0",
		"Image?[SORT]=0,14,0,%SECTOR,%COUNT,%PAGE,%EXCHANGE,0",
		"Image?[SORT]=0,17,1,%SECTOR,%COUNT,%PAGE,%EXCHANGE,0",
		"Image?[SORT]=0,2,0,%SECTOR,%COUNT,%PAGE,%EXCHANGE,0",
		"Image?[SORT]=0,4,1,%SECTOR,%COUNT,%PAGE,%EXCHANGE,0"
	};
	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
	
	/*public final static String[] IMAGE_SORT_COMMANDS = {
		"Image?[SORT]=0,16,1,%SECTOR,%COUNT,0,%EXCHANGE,0",
		"Image?[SORT]=0,13,1,%SECTOR,%COUNT,0,%EXCHANGE,0",
		"Image?[SORT]=0,13,0,%SECTOR,%COUNT,0,%EXCHANGE,0",
		"Image?[SORT]=0,14,1,%SECTOR,%COUNT,0,%EXCHANGE,0",
		"Image?[SORT]=0,14,0,%SECTOR,%COUNT,0,%EXCHANGE,0",
		"Image?[SORT]=0,17,1,%SECTOR,%COUNT,0,%EXCHANGE,0",
	}; */
	
	public final static String MARKET_SORT_COMMAND = "Image?[SORT]=0,19,1,10,0,0";
	
	//public static final String SECTOR_FAST_COMMAND = null;
	
	public static String getSectorCommand(){
		return "Sector?[FAST]="+ExchangeCode+"&[FIELD]=36,37,35,109,131";
	}
	
	//public static final String SECTOR_FAST_COMMNAD_2 = "Sector?[FAST]=SG&[FIELD]=36,37,35,109,131";
	
	//edited by Thinzar@20141103, Change_Request-20140901, ReqNo. 14
	public static final String[] CHART_IMAGE_COMMANDS = {
		"w=480&h=350&k=",
		"w=480&h=350&k=",
		"t=2&w=700&h=350&k=",
		"w=700&h=350&k=",
		"t=1&w=480&h=350&k=",
	};
	
	/* Mary@20120911 - Change_Request-20120719, ReqNo.12 - START
	public final static String IMAGE_SORT_FIELDS = "33,38,39,50,51,55,56,57,98,101,102,132";
	public final static String MARKET_SORT_FIELDS = "33,38,51,98,56,57";
	*/
	public final static String IMAGE_SORT_FIELDS = "33,38,39,50,51,55,56,57,98,101,102,132,134,40";
	public final static String MARKET_SORT_FIELDS = "33,38,51,98,56,57,40";
	// Mary@20120911 - Change_Request-20120719, ReqNo.12 - END
	
	//Added by Thinzar, Change_Request-20150401, ReqNo. 2
	public final static String IMAGE_SORT_DER_FIELDS = "33,38,39,50,51,55,56,57,98,101,102,132,134,40,58,68,78,88";
														      
	public final static String[] IMAGE_QUOTE_FIELDS = {
		/* Mary@20120911 - Change_Request-20120719, ReqNo.12
		"33,56,57,98,101,102,132,58,68,78,88,50,51,59,69,79,89,60,70,80,90,61,71,81,91,62,72,82,92,139",
		"33,58,68,78,88,59,69,79,89,60,70,80,90,61,71,81,91,62,72,82,92",
		*/
		"33,56,57,98,101,102,132,58,68,78,88,50,51,59,69,79,89,60,70,80,90,61,71,81,91,62,72,82,92,139,40,103,119",
		/* Mary@20121224 - Fixes_Request-20121221, ReqNo.10
		"33,38,39,50,51,55,56,57,98,101,102,132,37,40,41,113,123,127,138,139,231,232",
		*/
		"33,38,39,50,51,55,56,57,98,101,102,132,37,40,41,113,123,127,138,139,231,232,48,237,238,241,242,134,103,239,153",
		"33,58,68,78,88,59,69,79,89,60,70,80,90,61,71,81,91,62,72,82,92,40",
		/*
		 * Added 40 to IMAGE_QUOTE_TRADE, Change_Request-20140901, ReqNo. 13
		 * Added 37 to IMAGE_QUOTE_TRADE, Fixes_Request-20160101, ReqNo.6
		 */
		"33,38,39,50,51,56,57,98,101,102,132,58,68,78,88,59,69,79,89,60,70,80,90,134,139,40,37",
		"33,134",
	};
	
	// if we set 30 days for a month, the data became extra
	public static final int DAYS_CHART_1MONTH = 22;
	public static final int DAYS_CHART_3MONTHS = DAYS_CHART_1MONTH * 3;
	public static final int DAYS_CHART_6MONTHS = DAYS_CHART_1MONTH * 6;
	public static final int DAYS_CHART_12MONTHS = DAYS_CHART_1MONTH * 12;
	public static final int DAYS_CHART_24MONTHS = DAYS_CHART_1MONTH * 24;
}
