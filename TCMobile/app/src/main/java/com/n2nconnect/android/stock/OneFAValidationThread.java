package com.n2nconnect.android.stock;

public class OneFAValidationThread {
	
	private OneFAValidationListener registeredListener;
	
	
	public OneFAValidationListener getRegisteredListener() {
		return registeredListener;
	}
	
	public void setRegisteredListener(OneFAValidationListener registeredListener) {
		this.registeredListener = registeredListener;
	}
	
	public interface OneFAValidationListener {
		public void OneFAValidationEvent(boolean isValid,String message);
	}
}
