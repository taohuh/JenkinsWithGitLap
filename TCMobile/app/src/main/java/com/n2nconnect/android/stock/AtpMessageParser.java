package com.n2nconnect.android.stock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.n2nconnect.android.stock.model.PaymentConfig;
import com.n2nconnect.android.stock.model.CurrencyExchangeInfo;
import com.n2nconnect.android.stock.model.DervTradePrtfDetailRpt;
import com.n2nconnect.android.stock.model.DervTradePrtfSubDetailRpt;
import com.n2nconnect.android.stock.model.DervTradePrtfSummRpt;
import com.n2nconnect.android.stock.model.E2EEInfo;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.OrderControlInfo;
import com.n2nconnect.android.stock.model.RDSInfo;
import com.n2nconnect.android.stock.model.StockCurrencyPaymentMethod;
import com.n2nconnect.android.stock.model.StockHolding;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.model.TradeOrder;
import com.n2nconnect.android.stock.model.Watchlist;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.SystemUtil;

public class AtpMessageParser {
	
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - START
	public static E2EEInfo parseE2EEParams(String message){
		E2EEInfo e2eeInfo			= null;
		List<String> lstHdrFieldIds	= new ArrayList<String>();
		Map<String,Integer> mappings= DefinitionConstants.getTradeE2EEParamMapping();
		
		StringTokenizer tokens = new StringTokenizer(message, "\r\n");
		while( tokens.hasMoreTokens() ){
			String strCurrLine = tokens.nextToken();
			
			if( strCurrLine.startsWith(DefinitionConstants.METADATA_HEADER) ){
				strCurrLine 			= strCurrLine.substring(DefinitionConstants.METADATA_HEADER.length(), strCurrLine.length() - 1);
				StringTokenizer items 	= new StringTokenizer(strCurrLine, "|");
				while (items.hasMoreTokens()) {
					lstHdrFieldIds.add(items.nextToken());
				}
			}else if( strCurrLine.startsWith(DefinitionConstants.RECORD_HEADER) ){
				e2eeInfo		= new E2EEInfo();
				strCurrLine 	= strCurrLine.substring(DefinitionConstants.RECORD_HEADER.length(), strCurrLine.length() - 1);
				String[] items 	= strCurrLine.split("\\|");

				for (int i=0; i<lstHdrFieldIds.size(); i++) {
					String strFieldId	= lstHdrFieldIds.get(i);					
					String strValue		= items[i];

					if( mappings.containsKey(strFieldId) ){
						if( mappings.get(strFieldId) == null)
							continue;
						
						switch( mappings.get(strFieldId).intValue() ){
							case 0 : e2eeInfo.setStrE2EEPublicKey(strValue); break;
							case 1 : e2eeInfo.setStrE2EESessionID(strValue); break;
							case 2 : e2eeInfo.setStrE2EERandomNumber(strValue); break;
							case 3 : e2eeInfo.setStrRSAModulus_Base64(strValue); break;
							case 4 : e2eeInfo.setStrRSAExponent_Base64(strValue); break;
							case 5 : e2eeInfo.setStrRSAPublicKey(strValue); break;
						}
					}
				}
			}
		}
		
		return e2eeInfo;
	}
	// Added by Mary@20130111 - Change_Request-20130104, ReqNo.1 - END
	
	// Added by Mary@20130118 - Change_Request-20130104, ReqNo.3 - START
	public static RDSInfo parseRDSInfo(String message){
		RDSInfo rdsInfo			= new RDSInfo();
		final String[] strArrKey= {"[LocalExchange]", "[RDSVersion]", "[RDSStatus]"};
		int intCanBreak			= 1;
		
		StringTokenizer tokens 	= new StringTokenizer(message, "\r\n");
		while (tokens.hasMoreTokens()) {
			String strCurrLine	= tokens.nextToken();
			String[] columns	= strCurrLine.split("]=");
			String key 			= columns[0] + "]";
			
			if( key.equalsIgnoreCase( strArrKey[0] ) ){
				if(columns.length > 1){
					String[] strArrLET	= columns[1].split(",");
					for(int i=0; i< strArrLET.length; i++){
						rdsInfo.addStrLocalExchangeCode( strArrLET[i] );
					}
				}
				
				if(intCanBreak == strArrKey.length)
					break;
				else
					intCanBreak++;
			}else if( key.equalsIgnoreCase( strArrKey[1] ) ){
				if(columns.length > 1)
					rdsInfo.setStrRDSVersion( columns[1] );
				
				if(intCanBreak == strArrKey.length)
					break;
				else
					intCanBreak++;
			}else if( key.equalsIgnoreCase( strArrKey[2] ) ){
				if(columns.length > 1){
					/*  Mary@20130327 - Change_Request-20130104, ReqNo.3 - START
					String strRDSStatus	= columns[1].trim();
					*/
					String[] strInnerSplit	= columns[1].split("\\|");
					String strRDSStatus		= strInnerSplit[0];
					// Mary@20130327 - Change_Request-20130104, ReqNo.3 - END
					rdsInfo.setIsRDSEnable( strRDSStatus.equalsIgnoreCase("Y") ? false : true);
					rdsInfo.setIsBlockByRDS( strRDSStatus.equalsIgnoreCase("B") ? true : false);
					
					// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - START
					rdsInfo.setStrRDSUrl( strInnerSplit.length > 1 ? strInnerSplit[1] : "");
					strInnerSplit		= null;
					// Added by Mary@20130327 - Change_Request-20130104, ReqNo.3 - END
					strRDSStatus 		= null;
				}
				
				if(intCanBreak == strArrKey.length)
					break;
				else
					intCanBreak++;
			}
		}
		
		return rdsInfo;
	}
	// Added by Mary@20130118 - Change_Request-20130104, ReqNo.3 - END

	// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - START
	public static boolean[] parseResetPwdPin(String message){
		boolean[] hasResetPwdPin= {false, false};
		StringTokenizer tokens 	= new StringTokenizer(message, "\r\n");
		
		while (tokens.hasMoreTokens()) {
			String strCurrLine	= tokens.nextToken();
			String[] columns	= strCurrLine.split("]=");
			String key 			= columns[0] + "]";
			if( key.equalsIgnoreCase("[ResetPwdPin]") ){
				int intVal	= Integer.parseInt( columns[1].trim() );
				switch(intVal){
					case 1 :
						hasResetPwdPin[0] = true;
						hasResetPwdPin[1] = false;
						break;
					case 2 :
						hasResetPwdPin[0] = false;
						hasResetPwdPin[1] = true;
						break;
					case 3 :
						hasResetPwdPin[0] = true;
						hasResetPwdPin[1] = true;
						break;
				}
				break;
			}
		}
		return hasResetPwdPin;
	}
	// Added by Mary@20130110 - Fixes_Request-20121221, ReqNo.6 - END
	
	//Added by Thinzar: Change_Request-20141101, ReqNo 16 - START
	//Note: followed based on the above method parseResetPwdPin
	/* 
	 	0 = Bypass, 
	 	1 = ChgPwd,
		2 = ChgPin,
		3 = 1(ChgPwd) + 2(ChgPin),
		4 = ChgHint&Answer,
		5 = 4(ChgHint&Answer) + 1(ChgPwd),
		6 = 4(ChgHint&Answer) + 2(ChgPin),
		7 = 4(ChgHint&Answer) + 1(ChgPwd) + 2(ChgPin)
	*/
	public static boolean[] parseResetPwdPinHint(String message){
		boolean[] hasResetPwdPin= {false, false, false, false};			//Edited Change_Request-20170119, ReqNo.11
		StringTokenizer tokens 	= new StringTokenizer(message, "\r\n");
		
		while (tokens.hasMoreTokens()) {
			String strCurrLine	= tokens.nextToken();
			String[] columns	= strCurrLine.split("]=");
			String key 			= columns[0] + "]";
			if( key.equalsIgnoreCase("[ResetPwdPin]") ){
				int intVal	= Integer.parseInt( columns[1].trim() );
				switch(intVal){
					case 1 :
						hasResetPwdPin[0] = true;
						hasResetPwdPin[1] = false;
						hasResetPwdPin[2] = false;
						hasResetPwdPin[3] = true;		//Added, Change_Request-20170119, ReqNo.11
						break;
					case 2 :
						hasResetPwdPin[0] = false;
						hasResetPwdPin[1] = true;
						hasResetPwdPin[2] = false;
						hasResetPwdPin[3] = false;
						break;
					case 3 :
						hasResetPwdPin[0] = true;
						hasResetPwdPin[1] = true;
						hasResetPwdPin[2] = false;
						hasResetPwdPin[3] = false;
						break;
					case 4:
						hasResetPwdPin[0] = false;
						hasResetPwdPin[1] = false;
						hasResetPwdPin[2] = true;
						hasResetPwdPin[3] = false;
						break;
					case 5:
						hasResetPwdPin[0] = true;
						hasResetPwdPin[1] = false;
						hasResetPwdPin[2] = true;
						hasResetPwdPin[3] = false;
						break;
					case 6:
						hasResetPwdPin[0] = false;
						hasResetPwdPin[1] = true;
						hasResetPwdPin[2] = true;
						hasResetPwdPin[3] = false;
						break;
					case 7:
						hasResetPwdPin[0] = true;
						hasResetPwdPin[1] = true;
						hasResetPwdPin[2] = true;
						hasResetPwdPin[3] = false;
						break;
				}
				break;
			}
		}
		return hasResetPwdPin;
	}
	//Added by Thinzar: Change_Request-20141101, ReqNo 16 - START
	
	/* Mary@20121228-Fixes_Request-20121102, ReqNo.11
	public static List<ExchangeInfo> parseExchangeInfo(String message) {
	*/
	
	// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6 - START
	public static Map<String,Integer> parseClientLimitOption(String message){
		StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
		Map<String,Integer> limit  = new HashMap<String,Integer>();
		
		while (tokens.hasMoreTokens()) {
			String lineItem	= tokens.nextToken();
			String[] columns= lineItem.split("]=");
			String key 		= columns[0];
			key				= key + "]";
			if(columns.length > 1){
				String value = columns[1];
				if(key.equals("[ClientLimitOption]") && !value.equals("")){
					String[] itemline = value.split(",");
					limit.put(itemline[0],Integer.parseInt(itemline[1]));
					//AppConstants.addClientLimitOption(itemline[0],Integer.parseInt(itemline[1]));
					
				}
			
			}
		}
		
		return limit;
	}
	
	//Added by Thinzar@20140730 - Change_Request-20140701, ReqNo. 9 - START
	public static List<String> parsePaymentType(String message){
		StringTokenizer tokens = new StringTokenizer(message, "\r\n");
		List<String> pymtTypes = new ArrayList<String>();
			
		while(tokens.hasMoreTokens()){
			String lineItem = tokens.nextToken();
			String[] columns = lineItem.split("]=");
			String key = columns[0] + "]";
			if(columns.length > 1){
				String value = columns[1];
				if(key.equals("[MapPaymentCode]") && !value.equals("")){
					pymtTypes.add(value);
				}
			}
		}
			
		return pymtTypes;
	}
	//Added by Thinzar@20140730 - END
	
	//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 START	
	public static Info2FA parse2FAInfo(String message){
		StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
		List<String> deviceList 		= new ArrayList<String>();
		List<String> icNo      			= new ArrayList<String>();
		int count						= 0;
		Info2FA info2fa 				= new Info2FA();
		
		while (tokens.hasMoreTokens()) {
			String lineItem	= tokens.nextToken();
		
			String[] columns= lineItem.split("]=");
			String key 		= columns[0];
			key				= key + "]";
			if(columns.length > 1){
				String value = columns[1];
				/* Mary@20130918 - Change_Request-20130724, ReqNo.14
				if(key.equals("[Is2FARequired]")){
				*/
				if( key.equalsIgnoreCase("[Is2FARequired]") ){
					//System.out.println("2FA aPP sTORE: "+value);
					info2fa.setIs2FARequired( value.equalsIgnoreCase("Y") );
					count++;
				/* Mary@20130918 - Change_Request-20130724, ReqNo.14
				}else if(key.equals("[DeviceList]")){
				*/
				}else if( key.equalsIgnoreCase("[DeviceList]") ){
					StringTokenizer items 	= new StringTokenizer(value, ",");

					while( items.hasMoreTokens() ){
						String item 		= items.nextToken();
						String[] itemline	= item.split("\\|");
					
						deviceList.add(itemline[0]);
						icNo.add(itemline.length>1?itemline[1]:"");
					}
					count++;
				// Added by Mary@20130918 - Change_Request-20130724, ReqNo.14 - START
				}else if( key.equalsIgnoreCase("[2FAbypass]") ){
					info2fa.set1FAAllowed( value.equalsIgnoreCase("YES") );
					info2fa.setForceUse1FA( value.equalsIgnoreCase("ALL") );
					count++;
				// Added by Mary@20130918 - Change_Request-20130724, ReqNo.14 - END
				}
				//Change_Request-20170601, ReqNo.10
				else if(key.equalsIgnoreCase("[2FAMobPhone]")){
					info2fa.setMobilePhone2FA(columns[1]);
					count++;
				}else if(key.equalsIgnoreCase("[2FASMSOTPInterval]")){
					info2fa.setSmsOtpInterval(Integer.parseInt(columns[1]));
					count++;
				}
			}
			
			/* Mary@20130918 - Change_Request-20130724, ReqNo.14
			if(count==2)
			*/
			if(count==5)
				break;
		}
		info2fa.setDeviceList(deviceList);
		info2fa.setICNumberList(icNo);
		
		return info2fa;
	}
	//Added by Kw@20130116 - Change_Request20130104, ReqNo.2 END
	
	public static List<ExchangeInfo> parseExchangeInfo(String message, StringBuffer strExtraBuffer) {
		List<ExchangeInfo> exchangeInfos= new ArrayList<ExchangeInfo>();
		StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
		ExchangeInfo info 				= null;
		boolean hasOverwriteOrderCtrl	= false;
		//System.out.println("2FA:"+message.toString());

		while (tokens.hasMoreTokens()) {
			String lineItem	= tokens.nextToken();
			/* Mary@20121226-Fixes_Request-20121221, ReqNo.24
			String[] columns= lineItem.split("]=");
			*/
			String[] columns= lineItem.split("]=");
			String key 		= columns[0];
			// Added by Mary@20121226-Fixes_Request-20121221, ReqNo.24
			key				= key + "]";
			if(columns.length > 1){
				String value = columns[1];
				if( key.equals("[Action]") ){				// e.g. [Action]=MY,*,Buy,Sell
					if(info != null)						// [Action] is indicator for new exchange info to be create
						exchangeInfos.add(info);
						
					info 					= new ExchangeInfo();
					StringTokenizer items 	= new StringTokenizer(value, ",");
					int index 				= 0;
					while( items.hasMoreTokens() ){
						String item = items.nextToken();
						if(index==0)
							info.setQCExchangeCode(item.trim());
						
						if(index >= 2)
							info.addAction(item);

						index++;
					}
					
					hasOverwriteOrderCtrl 	= false;	//Added by Thinzar, Change_Request-20140801, ReqNo.4
					
				}else if( key.equals("[OrderType]") ){		// e.g. [OrderType]=MY,*,Limit,Market,Stop,StopLimit,MarketToLimit
					StringTokenizer items 	= new StringTokenizer(value, ",");
					int index 				= 0;
					while( items.hasMoreTokens() ){
						String item = items.nextToken();
						if(index >= 2)
							info.addOrderType(item.trim());

						index++;
					}
				}else if( key.equals("[Validity]") ){		// e.g. [Validity]=MY,*,Day,GTC,FAK,FOK,GTD
					StringTokenizer items	= new StringTokenizer(value, ",");
					int index 				= 0;
					while( items.hasMoreTokens() ){
						String item = items.nextToken();
						if(index >= 2)
							info.addValidity(item.trim());

						index++;
					}
				}else if( key.equals("[Payment]") ){		// e.g. [Payment]=SID,*,MCTA=+.Buy.Sell,FCA=+.Sell
					StringTokenizer items	= new StringTokenizer(value, ",");
					int index 				= 0;

					while( items.hasMoreTokens() ){
						String item	= items.nextToken();
						// Mary@20121226-Fixes_Request-20121221, ReqNo.24 
						if(index > 1){
							// Added by Mary@20121121 - Changes_Request-20121106, ReqNo.1
							if( item.indexOf("=+") == -1 ){
								info.addPaymentMethod(item.trim());
							// Added by Mary@20121121 - Changes_Request-20121106, ReqNo.1 - START
							}else{
								String[] strArrTemp	= item.split("=+");
								
								StockCurrencyPaymentMethod stkCurrPymtMthd = new StockCurrencyPaymentMethod();
								stkCurrPymtMthd.setStrPaymentMethod(strArrTemp[0].trim() );
								stkCurrPymtMthd.setAllowedBuyAction( strArrTemp[1].trim().contains("Buy") );
								stkCurrPymtMthd.setAllowedSellAction( strArrTemp[1].trim().contains("Sell") );
								
								info.addStockCurrencyPaymentMethod(stkCurrPymtMthd);
								strArrTemp 			= null;
								stkCurrPymtMthd		= null;
							// Added by Mary@20121121 - Changes_Request-20121106, ReqNo.1 - END
							}
						// Added by Mary@20121226-Fixes_Request-20121221, ReqNo.24
						}

						index++;
					}
				}else if( key.equals("[Currency]") ){		// e.g. [Currency]=
					StringTokenizer items 	= new StringTokenizer(value, ",");
					int index 				= 0;
					while( items.hasMoreTokens() ){
						String item = items.nextToken();
						if(index >= 2){
							/* Mary@20130305 - Fixes_Request-20130227, ReqNo.2 - START
							if( ! item.equals("+") )
								info.addCurrency(item.trim());
							// Added by Mary@20121121 - Changes_Request-20121106, ReqNo.1 - START
							else
								info.setIncludeStockCurrency(true);
							// Added by Mary@20121121 - Changes_Request-20121106, ReqNo.1 - END
							*/
							info.addCurrency(item.trim());
							if( item.equals("+") )
								info.setIncludeStockCurrency(true);
							// Mary@20130305 - Fixes_Request-20130227, ReqNo.2 - END
						}
						index++;
					}
				}else if( key.equals("[Revise]") ){			// e.g. [Revise]=MY,*,Q+-,P+-,O,V,L+-
					StringTokenizer items 	= new StringTokenizer(value, ",");
					int index 				= 0;
					while( items.hasMoreTokens() ){
						String item = items.nextToken();
						if(index >= 2)
							info.addReviseCode(item.trim());
						
						index++;
					}
				}else if( key.equals("[ReviseOT]") ){		// e.g. [ReviseOT]=MY,*,Limit,Market,MarketToLimit
					StringTokenizer items 	= new StringTokenizer(value, ",");
					int index 				= 0;
					while( items.hasMoreTokens() ){
						String item = items.nextToken();
						if(index >= 2)
							info.addReviseOrderType(item.trim());

						index++;
					}
				}else if( key.equals("[ReviseV]") ){		// e.g. [ReviseV]=MY,*,Day,GTC,FAK,GTD
					StringTokenizer items 	= new StringTokenizer(value, ",");
					int index 				= 0;
					while( items.hasMoreTokens() ){
						String item = items.nextToken();
						if(index >= 2)
							info.addReviseValidity(item.trim());

						index++;
					}
				}else if( key.equals("[TNCInfo]") ){		// e.g. [TNCInfo]=
					/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3
					if(value!=null){
						AppConstants.TNCInfo = value;
					}else{
						AppConstants.TNCInfo = value;
					}
					*/
					AppConstants.TNCInfo = value;
				}
				//Added by Thinzar@20140815, Change_Request-20140801, ReqNo.4 - START
				else if(key.equals("[TriggerPriceType]")){		//e.g., [TriggerPriceType]=KL,LastTrade,BestOffer,BestBid
					if(value.length() > 0){
						StringTokenizer items = new StringTokenizer(value, ",");
						int index 				= 0;
						while(items.hasMoreTokens()){
							String item = items.nextToken();
							if(index > 0) 	info.addTriggerPriceType(item);
							index++;
						}
					}
				} else if(key.equals("[TriggerPriceDirection]")){		//e.g., [TriggerPriceDirection]=KL,Up,Down
					if(value.length() > 0){
						StringTokenizer items = new StringTokenizer(value, ",");
						int index	= 0;
						while(items.hasMoreTokens()){
							String item = items.nextToken();
							if(index > 0)	info.addTriggerPriceDirection(item);
							index++;
						}
					}
				}
				//Added by Thinzar@20140815, Change_Request-20140801, ReqNo.4 - END
				else if( key.equals("[OrderCtrl]") ){		// ****** e.g [OrderCtrl]=MY,Limit,Day,GTC,FAK,FOK,GTD;Qty,Price,Min,Disclosed=Day.GTC.GTD
					// Added by Mary@20121121 - Change_Request-20121106, ReqNo. 1
					OrderControlInfo ori	= new OrderControlInfo();
					
					/*  Mary@20121121 - Change_Request-20121106, ReqNo. 1 - START
					if(columns.length == 3)
						value	= value+"="+columns[2];

					String[] items	= value.split("\\;");
					
					arr 			= new String[items.length];
					for(int i=0; i<items.length; i++){
						String item = items[i];
						arr[i] 		= item.trim();
						info.addOrderCtrltoList(arr);
					}
					*/
					
					for(int i=2; i<columns.length; i++){
						value	= value + "=" + columns[i];
					}
					
					String[] items	= value.split("\\;");
					
					/* Get & Set Order Type & Validity */
					StringTokenizer stOrdTypeWithValidity	= new StringTokenizer(items[0], ",");
					int index 				= 0;
					while( stOrdTypeWithValidity.hasMoreTokens() ){
						String item = stOrdTypeWithValidity.nextToken().trim();
						
						if(index == 1)
							ori.setStrOrderType(item);
						else if(index >= 2)
							ori.addStrAvailableValidity(item);

						index++;
					}
					
					if(items.length > 1){
						/* Get & Set input field isEnable flag */
						StringTokenizer stInputField		= new StringTokenizer(items[1], ",");
						
						while( stInputField.hasMoreTokens() ){
							String item = stInputField.nextToken().trim();
							
							if( item.contains("Qty") ){
								ori.setEnableQuantityField(true);
							}
							//Added By Thinzar@20140814 - Change_Request-20140801, ReqNo.4 - START
							/*Needed to add above "Price" checking, because this two strings contains "price"*/
							else if(item.contains("TriggerPriceType")){
								ori.setEnableTriggerType(true);
							
							} else if(item.contains("TriggerPriceDirection")){
								ori.setEnableTriggerDirection(true);
								
							}
							//Added By Thinzar@20140814 - Change_Request-20140801, ReqNo.4 - END
							else if( item.contains("Price") ){
								ori.setEnablePriceField(true);
							}else if( item.contains("Min") ){
								ori.setEnableMinQtyField(true);
								
								/* Get & Set list of validity that enabled minimum input field */
								String[] strArrFieldWithValidity	= item.split("=");
								if( strArrFieldWithValidity.length > 1 ){
									stInputField						= new StringTokenizer(strArrFieldWithValidity[1], "\\.");
									while( stInputField.hasMoreTokens() ){
										String strValidity = stInputField.nextToken().trim();
										ori.addStrValidityThatEnableMinQtyField(strValidity);
									}
								}
							}else if( item.contains("StopLimit") ){
								ori.setEnableStopLimitPriceField(true);
							}else if( item.contains("Disclosed") ){
								ori.setEnableDisclosedQtyField(true);
								
								/* Get & Set list of validity that enabled disclosed input field */
								String[] strArrFieldWithValidity	= item.split("=");
								if( strArrFieldWithValidity.length > 1 ){
									stInputField						= new StringTokenizer(strArrFieldWithValidity[1], "\\.");
									while( stInputField.hasMoreTokens() ){
										String strValidity = stInputField.nextToken().trim();
										ori.addStrValidityThatEnableDisclosedQtyField(strValidity);
									}
								}
							} 
						}
					}
						
					info.addOrderCtrltoList(ori);
					// Mary@20121121 - Change_Request-20121106, ReqNo. 1 - END
				}
				//Added by Thinzar@20140815 - Change_Request-20140810,ReqNo.4 - END
				else if( key.equals("[OrderCtrl2]") ){		// ****** e.g [OrderCtrl2]=KL,Stop,Day,FAK,GTD,IR;Qty,StopLimit,TriggerPriceType,TriggerPriceDirection
					
					OrderControlInfo ori	= new OrderControlInfo();
										
					for(int i=2; i<columns.length; i++){
						value	= value + "=" + columns[i];
					}
					
					String[] items	= value.split("\\;");
					
					/* Get & Set Order Type & Validity */
					StringTokenizer stOrdTypeWithValidity	= new StringTokenizer(items[0], ",");
					int index 				= 0;
					while( stOrdTypeWithValidity.hasMoreTokens() ){
						String item = stOrdTypeWithValidity.nextToken().trim();
						
						if(index == 1)
							ori.setStrOrderType(item);
						else if(index >= 2)
							ori.addStrAvailableValidity(item);

						index++;
					}
					
					if(items.length > 1){
						/* Get & Set input field isEnable flag */
						StringTokenizer stInputField		= new StringTokenizer(items[1], ",");
						
						while( stInputField.hasMoreTokens() ){
							String item = stInputField.nextToken().trim();
							
							if( item.contains("Qty") ){
								ori.setEnableQuantityField(true);
							}
							//Added By Thinzar@20140814 - Change_Request-20140801, ReqNo.4 - START
							/*Needed to add above "Price" checking, because this two strings contains "price"*/
							else if(item.contains("TriggerPriceType")){
								ori.setEnableTriggerType(true);
							
							} else if(item.contains("TriggerPriceDirection")){
								ori.setEnableTriggerDirection(true);
								
							}
							//Added By Thinzar@20140814 - Change_Request-20140801, ReqNo.4 - END
							else if( item.contains("Price") ){
								ori.setEnablePriceField(true);
							}else if( item.contains("Min") ){
								ori.setEnableMinQtyField(true);
								
								/* Get & Set list of validity that enabled minimum input field */
								String[] strArrFieldWithValidity	= item.split("=");
								if( strArrFieldWithValidity.length > 1 ){
									stInputField						= new StringTokenizer(strArrFieldWithValidity[1], "\\.");
									while( stInputField.hasMoreTokens() ){
										String strValidity = stInputField.nextToken().trim();
										ori.addStrValidityThatEnableMinQtyField(strValidity);
									}
								}
							}else if( item.contains("StopLimit") ){
								ori.setEnableStopLimitPriceField(true);
							}else if( item.contains("Disclosed") ){
								ori.setEnableDisclosedQtyField(true);
								
								/* Get & Set list of validity that enabled disclosed input field */
								String[] strArrFieldWithValidity	= item.split("=");
								if( strArrFieldWithValidity.length > 1 ){
									stInputField						= new StringTokenizer(strArrFieldWithValidity[1], "\\.");
									while( stInputField.hasMoreTokens() ){
										String strValidity = stInputField.nextToken().trim();
										ori.addStrValidityThatEnableDisclosedQtyField(strValidity);
									}
								}
							} 
						}
					}
					
					//To replace previous [OrderCtrl]
					if(!hasOverwriteOrderCtrl){
						info.getOrderCtrltoList().clear();
						hasOverwriteOrderCtrl = true;	//set this back to false when the next exchange start
					}
					info.addOrderCtrltoList(ori);
				}
				//Added by Thinzar@20140815 - Change_Request-20140810,ReqNo.4 - END
				
				//Added by Thinzar@20140909 - Change_Request-20140901, ReqNo.1 - START
				else if( key.equals("[ReviseCtrl]") ){		// ****** e.g [ReviseCtrl]=KL,Stop,Day,FAK,GTD,IR;Qty,Price,StopLimit,TriggerPriceType,TriggerPriceDirection
					
					OrderControlInfo reviseOci	= new OrderControlInfo();
										
					for(int i=2; i<columns.length; i++){
						value	= value + "=" + columns[i];
					}
					
					String[] items	= value.split("\\;");
					
					/* Get & Set Order Type & Validity */
					StringTokenizer stOrdTypeWithValidity	= new StringTokenizer(items[0], ",");
					int index 				= 0;
					while( stOrdTypeWithValidity.hasMoreTokens() ){
						String item = stOrdTypeWithValidity.nextToken().trim();
						
						if(index == 1)
							reviseOci.setStrOrderType(item);
						else if(index >= 2)
							reviseOci.addStrAvailableValidity(item);

						index++;
					}
					
					if(items.length > 1){
						/* Get & Set input field isEnable flag */
						StringTokenizer stInputField		= new StringTokenizer(items[1], ",");
						
						while( stInputField.hasMoreTokens() ){
							String item = stInputField.nextToken().trim();
							
							if( item.contains("Qty") ){
								reviseOci.setEnableQuantityField(true);
							}
							/*Needed to add above "Price" checking, because this two strings contains "price"*/
							else if(item.contains("TriggerPriceType")){
								reviseOci.setEnableTriggerType(true);
							
							} else if(item.contains("TriggerPriceDirection")){
								reviseOci.setEnableTriggerDirection(true);
								
							}
							else if( item.contains("Price") ){
								reviseOci.setEnablePriceField(true);
							}else if( item.contains("Min") ){
								reviseOci.setEnableMinQtyField(true);
								
								/* Get & Set list of validity that enabled minimum input field */
								String[] strArrFieldWithValidity	= item.split("=");
								if( strArrFieldWithValidity.length > 1 ){
									stInputField						= new StringTokenizer(strArrFieldWithValidity[1], "\\.");
									while( stInputField.hasMoreTokens() ){
										String strValidity = stInputField.nextToken().trim();
										reviseOci.addStrValidityThatEnableMinQtyField(strValidity);
									}
								}
							}else if( item.contains("StopLimit") ){
								reviseOci.setEnableStopLimitPriceField(true);
							}else if( item.contains("Disclosed") ){
								reviseOci.setEnableDisclosedQtyField(true);
								
								/* Get & Set list of validity that enabled disclosed input field */
								String[] strArrFieldWithValidity	= item.split("=");
								if( strArrFieldWithValidity.length > 1 ){
									stInputField						= new StringTokenizer(strArrFieldWithValidity[1], "\\.");
									while( stInputField.hasMoreTokens() ){
										String strValidity = stInputField.nextToken().trim();
										reviseOci.addStrValidityThatEnableDisclosedQtyField(strValidity);
									}
								}
							} 
						}
					}
					
					info.addReviseOrderCtrltoList(reviseOci);
				}
				//Added by Thinzar@20140909 - Change_Request-20140901, ReqNo.1 - END
				
				//Added by Thinzar, Change_Request-20150401, ReqNo. 5 - START
				else if( key.equals("[CurrencyNotSupported]") ){		// e.g. [CurrencyNotSupported]=HK,*,CNY
					StringTokenizer items 	= new StringTokenizer(value, ",");
					int index 				= 0;
					while( items.hasMoreTokens() ){
						String item = items.nextToken();
						if(index >= 2){
							info.setCurrencyNotSupported(item.trim());
						}
						index++;
					}
				}
				//Added by Thinzar, Change_Request-20150401, ReqNo. 5 - END
				
				// Added by Mary@20121228-Fixes_Request-20121102, ReqNo.11 - START
				else if( key.equals("[CurrencyRate]") ){
					strExtraBuffer.append(lineItem);
				// Added by Mary@20121228-Fixes_Request-20121102, ReqNo.11 - END
				} 
				
			}
		}
		
		// Added by Mary@20121212 - Avoid missing of last component
		if(info != null)
			exchangeInfos.add(info);

		return exchangeInfos;
	}
	
	// Added by Mary@20121228-Fixes_Request-20121102, ReqNo.11 - START
	public static List<CurrencyExchangeInfo> parseCurrencyExchangeInfo(String strMainData) {
		List<CurrencyExchangeInfo> currencyExchangeInfos= new ArrayList<CurrencyExchangeInfo>();
		String[] strMainColumn							= strMainData.split("]="); // e.g. [CurrencyRate]=KL,CNY,MYR,0.000000,48.640000,49.950000,100;KL,AUD,MYR,0.000000,3.208500,3.230500,1;
		
		if(strMainColumn.length <= 1){
			return currencyExchangeInfos;
		}
		
		StringTokenizer tokens 							= new StringTokenizer(strMainColumn[1], ";");
		while (tokens.hasMoreTokens()) {
			String strSubData			= tokens.nextToken(); // e.g. KL,CNY,MYR,0.000000,48.640000,49.950000,100
			String[] strSubColumn		= strSubData.split(",");
			
			CurrencyExchangeInfo info	= new CurrencyExchangeInfo();			
			info.setStrFromStockCurrency(strSubColumn[1]);	// e.g. CNY
			info.setStrToClientCurrency(strSubColumn[2]);	// e.g. MYR
			info.setDblBuyRate( Double.parseDouble( strSubColumn[4] ) ); // e.g. 48.640000
			info.setDblSellRate( Double.parseDouble( strSubColumn[5] ) ); // e.g. 49.950000
			info.setLngDenomination( Long.parseLong( strSubColumn[6] ) ); // e.g. 100
			
			currencyExchangeInfos.add(info);
		}

		return currencyExchangeInfos;
	}
	// Added by Mary@20121228-Fixes_Request-20121102, ReqNo.11 - END
	
	public static String extractTradeClientLimit(String message) {
		StringTokenizer tokens 	= null;
		String creditLimit 		= null;
		try{
			tokens = new StringTokenizer(message, "\r\n");
			int fieldIndex = 0;
			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();
	
				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem = lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items = new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId = items.nextToken();
						if (fieldId.equals(DefinitionConstants.FIELD_CREDIT_LIMIT)) {
							break;
						}
						fieldIndex++;
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
	
					lineItem = lineItem.substring(2, lineItem.length() - 1);
					//				StringTokenizer items = new StringTokenizer(lineItem, "|");
					String[] items = lineItem.split("\\|");
	
	
					for (int i=0; i<items.length; i++) {
	
						creditLimit = items[i];
						if (i==fieldIndex) {
							break;
						}
					}
				}
			}
			
			return creditLimit;
		}catch(Exception e){
			e.printStackTrace();
			
			return null;
		}finally{
			if(tokens != null)
				tokens = null;
			
			creditLimit = null;
		}
	}
	// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
	public static Map<String, String> extractTradeClientLimit2(String message) {
		StringTokenizer tokens 			= null;
		//System.out.println("clientlimit:"+message);
		List<String> fieldIds 			= new ArrayList<String>();
		Map<String, String> limit  		= new HashMap<String,String>();
		
		Map<String, Integer> mappings	= DefinitionConstants.getTradeClientLimitMapping();
		try{
			tokens 	= new StringTokenizer(message, "\r\n");
			while( tokens.hasMoreTokens() ){
				String lineItem = tokens.nextToken();
	
				if( lineItem.startsWith(DefinitionConstants.METADATA_HEADER) ){
					lineItem				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while( items.hasMoreTokens() ){
						String fieldId	= items.nextToken();
						char charId 	= fieldId.length() > 0 ? fieldId.charAt(0) : ' ';
					//	int asciiCode 	= (int) charId;
					//	fieldIds.add(String.valueOf(asciiCode));
						fieldIds.add( String.valueOf(charId) );
					}
				}else if( lineItem.startsWith(DefinitionConstants.RECORD_HEADER) ){
					//System.out.println("lineItemA:"+lineItem);
					lineItem 		= lineItem.substring(2, lineItem.length() - 1);
					//				StringTokenizer items = new StringTokenizer(lineItem, "|");
					String[] items	= lineItem.split("\\|");
	
					for(int i=0; i<items.length; i++){					
						String fieldId	= (String) fieldIds.get(i);	
						//System.out.println("fieldIDS i="+i+" contains:"+ fieldIds.get(i) );
						String column	= items[i];

						if( mappings.containsKey(fieldId) ){
							Integer id = mappings.get(fieldId);
							
							//System.out.println("limitid :"+id);
							if(id == null)
								continue;

							switch( id.intValue() ){
								case 0: limit.put("Trading Limit",column); break;
								case 1: limit.put("Buy Limit",column); break;
								case 2: limit.put("Sell Limit",column); break;
							}
						}
					}
				}	
			}
			return limit;
		}catch(Exception e){
			e.printStackTrace();
			
			return null;
		}finally{
			if(tokens != null)
				tokens = null;
			
			limit = null;
		}
	}
	
	public static List<StockHolding> parseTradePortfolio(String message) {
		try {
			List<StockHolding> stockHoldings= new ArrayList<StockHolding>();
			List<String> fieldIds 			= new ArrayList<String>();

			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getTradePortfolioMapping();

			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();

				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId	= items.nextToken();
						char charId 	= fieldId.length() > 0 ? fieldId.charAt(0) : ' '; 
						int asciiCode 	= (int) charId;
						fieldIds.add(String.valueOf(asciiCode)); 
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					StockHolding counter = new StockHolding();
					lineItem = lineItem.substring(2, lineItem.length() - 1); 
					//	StringTokenizer items = new StringTokenizer(lineItem, "|");
					String[] items = lineItem.split("\\|");
					for (int i=0; i<items.length; i++) {
						String fieldId = (String) fieldIds.get(i);		
						String column = items[i];

						if (mappings.containsKey(fieldId)) {
							Integer id = mappings.get(fieldId);
							if(id == null)
								continue;

							switch (id.intValue()) {
								case 0: counter.setSymbolCode(column); break;
								case 1: counter.setStockCode(column); break;
								case 2: if(column.length()==0) {counter.setMarketValue(0);}else{counter.setMarketValue(Double.parseDouble(column));} break;
								case 3: counter.setGainLossAmount(Double.parseDouble(column)); break;
								case 4: counter.setGainLossPercent(Float.parseFloat(column)/100); break;
								case 5: if(column.length()==0) {counter.setQuantityOnHand(0);} else{counter.setQuantityOnHand(Long.parseLong(column));}break;
								case 6: if(column.length()==0) {counter.setAvgPurchasePrice(0);}else{counter.setAvgPurchasePrice(Float.parseFloat(column));} break;
								case 7: if(column.length()==0){counter.setLastDone(0);} else{counter.setLastDone(Float.parseFloat(column));}break;
								case 8: if(column.length()==0) {counter.setQuantityAvailable(0);} else {counter.setQuantityAvailable(Long.parseLong(column));}break;
								case 9: if(column.length()==0) {counter.setQuantitySellQueued(0);}else {counter.setQuantitySellQueued(Long.parseLong(column));}break;
								
								// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START
								case 10: if(column.length()==0) {counter.setAggrBuyPrice(0);}else{counter.setAggrBuyPrice(Float.parseFloat(column));} break;
								case 12: if(column.length()==0) {counter.setAggrSellPrice(0);} else{counter.setAggrSellPrice(Float.parseFloat(column));}break;
								case 13: if(column.length()==0) {counter.setTotalQtyFromHolding(0);} else{counter.setTotalQtyFromHolding(Long.parseLong(column));}break;
								case 14: if(column.length()==0) {counter.setTotalQtyShort(0);} else{counter.setTotalQtyShort(Long.parseLong(column));}break;
								case 15: 
									if(column.length()==0){
										// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27
										counter.setIntPortfolioType(ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED);
										counter.setTotalQtySold(0);
									}else{
										// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27
										counter.setIntPortfolioType(ParamConstants.INT_PORTFOLIOTYPE_REALIZED);
										counter.setTotalQtySold(Long.parseLong(column));
									}
									break;
								case 16: if(column.length()==0){counter.setTotalBrokerage(0);} else{counter.setTotalBrokerage(Float.parseFloat(column));}break;
								case 17: if(column.length()==0){counter.setRealiseGainLossAmount(0);} else{counter.setRealiseGainLossAmount(Double.parseDouble(column));}break;
								case 18: if(column.length()==0){counter.setRealiseGainLossPercent(0);} else{counter.setRealiseGainLossPercent(Float.parseFloat(column)/100);}break;
								// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END
								// Added by Mary@20120912 - Change_Requets-20120815, ReqNo.12 - START
								case 19 :
									/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
									counter.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "1" : column.trim() ) );
									*/
									counter.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "0" : column.trim() ) );
									break;
								// Added by Mary@20120912 - Change_Requets-20120815, ReqNo.12 - END
									
								//Added by Thinzar@20140805 - Change_Request+20140701, ReqNo. 9 - START
								case 20: 
									if(column.length() == 0)	counter.setSettlementCurrency("");
									else counter.setSettlementCurrency(column);
									break;
								case 21: 
									if(column.length() == 0) 	counter.setInvestmentPortfolio("");
									else counter.setInvestmentPortfolio(column);
									break;
								//Added by Thinzar@20140805 - END
							}
						}
					}
					stockHoldings.add(counter);
				}
			}

			return stockHoldings;	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}


	public static DervTradePrtfSummRpt parseTradePortfolioSummaryRpt(String message) {
		try {
			DervTradePrtfSummRpt derivativeTradePortfolioSummaryRpt = new DervTradePrtfSummRpt();
		
			List<Integer> fieldIds 			= new ArrayList<Integer>();
			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");

			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();

				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId	= items.nextToken();
						char charId 	= fieldId.length() > 0 ? fieldId.charAt(0) : ' ';
						int asciiCode 	= (int) charId;
						fieldIds.add(asciiCode);
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					lineItem = lineItem.substring(2, lineItem.length() - 1);
					String[] items = lineItem.split("\\|");
					for (int i=0; i<items.length; i++) {
						int fieldId = fieldIds.get(i);					
						String column = items[i];
						//Log.i("mel","fieldId "+fieldId+"column"+ column);
						switch(fieldId){
						case 99:
							//openlong
							derivativeTradePortfolioSummaryRpt.setBuyOptionMktVal(Float.parseFloat(column));
							break;
						case 100:
							//openshort
							derivativeTradePortfolioSummaryRpt.setSellOptionMktVal(Float.parseFloat(column));
							break;
						case 103:
							//cashbalance
							derivativeTradePortfolioSummaryRpt.setCashBalance(Float.parseFloat(column));
							break;
						case 104:
							//deposit
							derivativeTradePortfolioSummaryRpt.setDeposit(Float.parseFloat(column));
							break;
						case 105:
							//withdrawal
							derivativeTradePortfolioSummaryRpt.setWithdrawal(Float.parseFloat(column));
							break;
						case 106:
							//eligibleCollateral
							derivativeTradePortfolioSummaryRpt.setEligibleCollateral(Float.parseFloat(column));
							break;
						case 107:
							//initialMargin
							derivativeTradePortfolioSummaryRpt.setInitialMargin(Float.parseFloat(column));
							break;
						case 108:
							//maintenanceMargin
							derivativeTradePortfolioSummaryRpt.setMaintenanceMargin(Float.parseFloat(column));
							break;
						case 109:
							//marginCall
							derivativeTradePortfolioSummaryRpt.setMarginCall(Float.parseFloat(column));
							break;
						//Added by Thinzar@20140904, Fixes_Request-20140820, ReqNo.15 - START
						case 125:
							//realized PL
							derivativeTradePortfolioSummaryRpt.setRealisedPL(Float.parseFloat(column));
							break;
						//Added by Thinzar@20140904, Fixes_Request-20140820, ReqNo.15 - END
						}
					}
				}
			}

			return derivativeTradePortfolioSummaryRpt;	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<DervTradePrtfSubDetailRpt> parseTradePortfolioSubDetailRpt(String message) {
		try {
			
			ArrayList<DervTradePrtfSubDetailRpt> derivativeTradePortfolioSubDetailRpt = new ArrayList<DervTradePrtfSubDetailRpt>();
		
			List<Integer> fieldIds 			= new ArrayList<Integer>();
			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");

			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();
				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId	= items.nextToken();
						char charId 	= fieldId.length() > 0 ? fieldId.charAt(0) : ' ';
						int asciiCode 	= (int) charId;
						fieldIds.add(asciiCode);
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt = new DervTradePrtfSubDetailRpt();
					lineItem = lineItem.substring(2, lineItem.length() - 1);
					String[] items = lineItem.split("\\|");
					for (int i=0; i<items.length; i++) {
						int fieldId = fieldIds.get(i);					
						String column = items[i];
						//Log.i("mel","fieldId "+fieldId+"column"+ column);
						switch(fieldId){
						case 42:
							//sequenceNo
							dervTradePrtfSubDetailRpt.setSequenceNo(Integer.parseInt(column));
							break;
						case 43:
							//stockCode
							dervTradePrtfSubDetailRpt.setStockCode(column);
							break;
						case 44:
							//stockName
							dervTradePrtfSubDetailRpt.setStockName(column);
							break;
						case 78:
							//averagePurchasePrice
							dervTradePrtfSubDetailRpt.setAveragePrice(Float.parseFloat(column));
							break;
						case 82:
							//unrealised gain loss amount
							dervTradePrtfSubDetailRpt.setUnrealisedPL(Float.parseFloat(column));
							break;
						case 95:
							//grossLong
							dervTradePrtfSubDetailRpt.setGrossBuy(Integer.parseInt(column));	//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17
							break;
						case 96:
							//grossShort
							dervTradePrtfSubDetailRpt.setGrossSell(Integer.parseInt(column));	//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17
							break;
						case 97:
							//dayLong
							dervTradePrtfSubDetailRpt.setDayBuy(Integer.parseInt(column));		//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17
							break;
						case 98:
							//dayShort
							dervTradePrtfSubDetailRpt.setDaySell(Integer.parseInt(column));		//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17
							break;
						case 99:
							//openLong
							dervTradePrtfSubDetailRpt.setBfBuy(Integer.parseInt(column));		//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17
							break;
						case 100:
							//openShort
							dervTradePrtfSubDetailRpt.setBfSell(Integer.parseInt(column));		//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17
							break;
						case 111:
							//productCode
							dervTradePrtfSubDetailRpt.setProductCode(column);
							break;
						case 118:
							//contract per val
							dervTradePrtfSubDetailRpt.setMultiplier(Float.parseFloat(column));
							break;
						case 125:
							//realised gain loss amount
							dervTradePrtfSubDetailRpt.setRealisedPL(Float.parseFloat(column));
							break;
						case 153:
							//home currency
							dervTradePrtfSubDetailRpt.setHomeCurrency(column);
							break;
						case 154:
							//forex exchange rate
							dervTradePrtfSubDetailRpt.setForexExchangeRate(Float.parseFloat(column));
							break;
						case 155:
							//currency symbol
							dervTradePrtfSubDetailRpt.setCurrencySymbol(column);
							break;
						}
					}
					derivativeTradePortfolioSubDetailRpt.add(dervTradePrtfSubDetailRpt);
				}
			}

			return derivativeTradePortfolioSubDetailRpt;	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<DervTradePrtfDetailRpt> parseTradePrtfDtlRptRefresh(String message) {
		try {
			
			ArrayList<DervTradePrtfDetailRpt> dervTradePrtfDetailRptList = new ArrayList<DervTradePrtfDetailRpt>();
		
			List<Integer> fieldIds 			= new ArrayList<Integer>();
			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");

			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();
				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId	= items.nextToken();
						char charId 	= fieldId.length() > 0 ? fieldId.charAt(0) : ' ';
						int asciiCode 	= (int) charId;
						fieldIds.add(asciiCode);
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					DervTradePrtfDetailRpt dervTradePrtfDetailRpt = new DervTradePrtfDetailRpt();
					lineItem = lineItem.substring(2, lineItem.length() - 1);
					String[] items = lineItem.split("\\|");
					for (int i=0; i<items.length; i++) {
						int fieldId = fieldIds.get(i);					
						String column = items[i];
						//Log.i("mel","fieldId "+fieldId+"column"+ column);
						switch(fieldId){
						case 42:
							//sequenceNo
							dervTradePrtfDetailRpt.setSequenceNo(Integer.parseInt(column));
							break;
						case 114:
							//side
							dervTradePrtfDetailRpt.setSide(column);
							break;
						case 116:
							//matchedQuantity
							dervTradePrtfDetailRpt.setMatchedQty(Integer.parseInt(column));
							break;
						case 117:
							//matchedPrice
							dervTradePrtfDetailRpt.setMatchedPrice(Float.valueOf(column));
							break;
						case 118:
							//multiplier
							dervTradePrtfDetailRpt.setMultiplier(Float.valueOf(column));
							break;
						case 153:
							//home currency
							dervTradePrtfDetailRpt.setHomeCurrency(column);
							break;
						case 154:
							//forex exchange rate
							dervTradePrtfDetailRpt.setForexExchangeRate(Float.parseFloat(column));
							break;
						case 155:
							//currency symbol
							dervTradePrtfDetailRpt.setCurrencySymbol(column);
							break;
						}
					}
					dervTradePrtfDetailRptList.add(dervTradePrtfDetailRpt);
				}
			}

			return dervTradePrtfDetailRptList;	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public static Map<String, String> extractHintInfo(String message) {
		try {
			List<String> fieldIds 			= new ArrayList<String>();

			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getHintMapping();
			Map<String, String> hints 		= new HashMap<String, String>();

			while( tokens.hasMoreTokens() ){
				String lineItem = tokens.nextToken();

				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while( items.hasMoreTokens() ){
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				}else if( lineItem.startsWith(DefinitionConstants.RECORD_HEADER) ){
					lineItem 		= lineItem.substring(2, lineItem.length() - 1);
					String[] items 	= lineItem.split("\\|");

					for (int i=0; i<items.length; i++) {
						String fieldId = (String) fieldIds.get(i);					
						String column = items[i];


						if (mappings.containsKey(fieldId)) {
							Integer id = mappings.get(fieldId);

							if(id == null)
								continue;

							switch (id.intValue()) {
							case 0: hints.put(DefinitionConstants.HINT_QUESTION, column); break;
							case 1: hints.put(DefinitionConstants.ID_STATUS, column); break;
							case 2: hints.put(DefinitionConstants.EMAIL, column); break;
							}
						}
					}
				}
			}
			return hints;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public static Map<String, String> extractHintInfo2(String message) {
		try {
			List<String> fieldIds 			= new ArrayList<String>();
			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getHintMapping2();
			Map<String, String> hints 		= new HashMap<String, String>();
			
			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();
				
				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					lineItem 		= lineItem.substring(2, lineItem.length() - 1);
					String[] items 	= lineItem.split("\\|");
					
					for (int i=0; i<items.length; i++) {
						String fieldId	= (String) fieldIds.get(i);					
						String column	= items[i];
						
						if( mappings.containsKey(fieldId) ){
							Integer id = mappings.get(fieldId);
							
							if(id == null)
								continue;
							
							switch( id.intValue() ){
								case 0: hints.put(DefinitionConstants.HINT_QUESTION, column); break;
								case 1: hints.put(DefinitionConstants.HINT_ANSWER, column); break;
								case 2: hints.put(DefinitionConstants.HINT_EMAIL, column); break;
								case 3: hints.put(DefinitionConstants.ID_STATUS, column); break;
							}
						}
					}
				}
			}
			return hints;
		}catch (Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public static Map<String, String> extractNewPasswordInfo(String message){
		try {
			List<String> fieldIds 			= new ArrayList<String>();

			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getNewPwdMapping();
			Map<String, String> forgetInfo 	= new HashMap<String, String>();

			while( tokens.hasMoreTokens() ){
				String lineItem	= tokens.nextToken();
				if( lineItem.startsWith(DefinitionConstants.METADATA_HEADER) ){
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while( items.hasMoreTokens() ){
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				}else if( lineItem.startsWith(DefinitionConstants.RECORD_HEADER) ){
					lineItem 		= lineItem.substring(2, lineItem.length() - 1);
					String[] items 	= lineItem.split("\\|");

					for(int i=0; i<items.length; i++){
						String fieldId	= (String) fieldIds.get(i);					
						String column	= items[i];

						if( mappings.containsKey(fieldId) ){
							Integer id = mappings.get(fieldId);

							if(id == null)
								continue;

							switch( id.intValue() ){
								case 0: forgetInfo.put(DefinitionConstants.TRADE_FORGET_PWD_EMAIL, column); break;
								case 1: forgetInfo.put(DefinitionConstants.TRADE_FORGET_PWD, column); break;
							}
						}
					}
				}
			}
			return forgetInfo;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static Map<String, String> extractTradeForgetPwdInfo(String message) {
		try {
			List<String> fieldIds 			= new ArrayList<String>();

			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getTradeForgetPwdMapping();
			Map<String, String> forgetInfo 	= new HashMap<String, String>();

			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();

				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem = lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items = new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					lineItem 		= lineItem.substring(2, lineItem.length() - 1);
					String[] items 	= lineItem.split("\\|");
					for (int i=0; i<items.length; i++) {
						String fieldId	= (String) fieldIds.get(i);					
						String column	= items[i];

						if (mappings.containsKey(fieldId)) {
							Integer id = mappings.get(fieldId);

							if (id == null)
								continue;

							switch( id.intValue() ){
								case 0: forgetInfo.put(DefinitionConstants.ERROR_CODE, column); break;
								case 1: forgetInfo.put(DefinitionConstants.ERROR_MESSAGE, column); break;
							}
						}
					}
				}
			}
			return forgetInfo;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public static Map<String, String> extractNewPinInfo(String message){
		try {
			List<String> fieldIds			= new ArrayList<String>();
			StringTokenizer tokens			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getNewPinMapping();
			Map<String, String> forgetInfo 	= new HashMap<String, String>();

			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();

				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					lineItem 		= lineItem.substring(2, lineItem.length() - 1);
					String[] items 	= lineItem.split("\\|");
					for (int i=0; i<items.length; i++) {
						String fieldId = (String) fieldIds.get(i);					
						String column = items[i];

						if (mappings.containsKey(fieldId)) {
							Integer id = mappings.get(fieldId);

							if(id == null)
								continue;

							switch (id.intValue()) {
							case 0: forgetInfo.put(DefinitionConstants.TRADE_FORGET_PIN_EMAIL, column); break;
							case 1: forgetInfo.put(DefinitionConstants.TRADE_FORGET_PIN, column); break;
							}
						}
					}
				}
			}
			return forgetInfo;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}


	public static Map<String, String> extractTradeForgetPINInfo(String message) {
		try {
			List<String> fieldIds 			= new ArrayList<String>();
			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getTradeForgetPINMapping();
			Map<String, String> forgetInfo 	= new HashMap<String, String>();

			while( tokens.hasMoreTokens() ){
				String lineItem = tokens.nextToken();

				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					lineItem 		= lineItem.substring(2, lineItem.length() - 1);
					String[] items 	= lineItem.split("\\|");
					
					for (int i=0; i<items.length; i++) {
						String fieldId	= (String) fieldIds.get(i);					
						String column	= items[i];

						if (mappings.containsKey(fieldId)) {
							Integer id = mappings.get(fieldId);

							if(id == null)
								continue;

							switch (id.intValue()) {
								case 0: forgetInfo.put(DefinitionConstants.ERROR_CODE, column); break;
								case 1: forgetInfo.put(DefinitionConstants.ERROR_MESSAGE, column); break;
							}
						}
					}
				}
			}
			return forgetInfo;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static String parseTradeTicket(String message){
		System.out.println("debug=== parseTradeTicket:" + message);
		
		String[] data = message.split("\\|");

		for(int i=0; i<data.length; i++){
			if(data[i].indexOf("M=")!=-1){
				String[] stat = data[i].split("\\=", 2);	//Edited by Thinzar, Fixes_Request-20160101, ReqNo.12
				return stat[1];
			}
		}
		return null;
	}

	public static List<String> parseSingleLogonResponse(String message){
		try{
			List<String> parseResponse 		= new ArrayList<String>();
			List<String> fieldIds			= new ArrayList<String>();

			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getDoubleLoginResponseMapping();

			while(tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();

				if(lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()){
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				}else if(lineItem.startsWith(DefinitionConstants.RECORD_HEADER)){ // e.g. )=0240057|The system is unable to proceed with your request. Please try again or alternatively contact our call centre at 1800 538 9889 / +65 6538 9889 (overseas) or email us at clientservices.sg@cimb.com for further assistance.  (ATP 1201)|1201|
					lineItem 		= lineItem.substring(2, lineItem.length() - 1);
					String[] items 	= lineItem.split("\\|");

					for (int i=0; i<items.length; i++) {
						String fieldId	= (String) fieldIds.get(i);					
						String column 	= items[i];

						if (mappings.containsKey(fieldId)) {
							Integer id = mappings.get(fieldId);

							if(id == null)
								continue;

							switch (id.intValue()) {
								case 0:
									parseResponse.add(column);
									break;
								case 1:
									/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
									if(!column.equals("")){
									*/
									if( column.trim().length() > 0 ){
										parseResponse.add(column);
									}else{
										parseResponse.add("Success");
									}
									break;
								case 2:
									parseResponse.add(column);
									break;
							}
						}
					}	
				}
			}
			return parseResponse;

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static List<TradeOrder> parseTradeStatus(String message) {
		try {
			List<TradeOrder> tradeOrders	= new ArrayList<TradeOrder>();
			List<String> fieldIds 			= new ArrayList<String>();

			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getTradeStatusMapping();

			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();

				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					TradeOrder order= new TradeOrder();
					lineItem 		= lineItem.substring(2, lineItem.length() - 1);
					//				StringTokenizer items = new StringTokenizer(lineItem, "|");
					String[] items = lineItem.split("\\|");

					for (int i=0; i<items.length; i++) {
						String fieldId = (String) fieldIds.get(i);					
						String column = items[i];
						if (mappings.containsKey(fieldId)) {
							Integer id = mappings.get(fieldId);

							if(id == null)
								continue;

							switch (id.intValue()) {
								case 0: order.setOrderNo(column); break;
								case 1: order.setAccountNo(column); break;
								case 2: order.setAction(column); break;
								case 3: order.setStatusCode(column); break;
								case 4: order.setStatusText(column); break;
								case 5: order.setQuantity(Long.parseLong(column.replace(",", ""))); break;
								case 6:
									/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3 - START
									if(column.equals("-")){
										order.setPrice(0); 
										break;
									}else{
										order.setPrice(Float.parseFloat(column.replace(",", ""))); 
										break;
									}
									*/
									order.setPrice( ( column.equals("-") || column == null || column.isEmpty() ) ? 0 : Float.parseFloat( column.replace(",", "") ) ); 
									break;
									// Mary@20121005 - Fixes_Request-20120815, ReqNo.3 - END
								case 7: order.setValue(Double.parseDouble(column)); break;
								case 8: order.setSymbolCode(column); break;
								case 9: order.setStockCode(column); break;
								case 10: order.setOrderType(column); break;
								case 11: order.setValidity(column); break;
								case 12:
									/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3
									if (column.length()==0) {
										order.setUnmatchQuantity(0); 
									} else {
										order.setUnmatchQuantity(Long.parseLong(column));
									}
									*/
									order.setUnmatchQuantity( (column.length()==0) ? 0 : Long.parseLong(column) );
									break;
								case 13: 
									/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3
									if (column.length()==0) {
										order.setMatchedQuantity(0);
									} else {
										order.setMatchedQuantity(Long.parseLong(column));
									}
									*/
									order.setMatchedQuantity( (column.length()==0) ? 0 : Long.parseLong(column) );
									break;
								case 14: 
									/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3
									if (column.length()==0) {
										order.setMatchedPrice(0);
									} else {
										order.setMatchedPrice(Float.parseFloat(column));
									}
									*/
									order.setMatchedPrice( (column.length()==0) ? 0 : Float.parseFloat(column) );
									break;
								case 15: 
									/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3
									if (column.length()==0) {
										order.setMatchedValue(0);
									} else {
										order.setMatchedValue(Double.parseDouble(column)); 
									}
									*/
									order.setMatchedValue( (column.length()==0) ? 0 : Double.parseDouble(column) );
									break;
								case 16: order.setOrderSource(column); break;
								case 17: order.setOrderDate(FormatUtil.parseDateString(column, "yyyyMMddhhmmss.S")); break;
								case 18: order.setRemark(column); break;
								case 19: order.setFixedNo(column); break;
								case 20:
									
									if(!column.isEmpty()){
										if(column.length() == 8)
											column.concat("000000.000");	//to cater for dateParseException
										
										order.setExpiryDate(FormatUtil.parseDateString(column, "yyyyMMddhhmmss.S")); break;
									}
								case 21: 
									//Fixes_Request-20170103, ReqNo.9
									if(column.length() > 0 && !column.equals("0"))
										order.setPaymentType(column); 
									
									break;
								case 22:
									order.setStrSettlementCurrency(column); break;
								case 23:
									order.setLastUpdate(column); break;	
								case 24:
									if(!(column.equals(""))){
										order.setStopLimit(Float.parseFloat(column));
									}
									break;
								case 25:
									order.setOrderTicket(column); 
									System.out.println("parseTradeStatus: ticketNumber=" + column);
									break;
								// Added by Mary@20120806 - Fixes_Request, ReqNo.5 - START
								case 26:
									/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3
									if (column.length()==0) {
										order.setCancelledQuantity(0);
									} else {
										order.setCancelledQuantity( Long.parseLong(column) );
									}
									*/
									order.setCancelledQuantity( (column.length()==0) ? 0 : Long.parseLong(column) );
									break;
								// Added by Mary@20120806 - Fixes_Request, ReqNo.5 - END		
								// Added by Mary@20120912 - Change_Requets-20120815, ReqNo.12 - START
								case 27 :
									/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
									order.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "1" : column.trim() ) );
									*/
									order.setIntLotSize( Integer.parseInt(column==null || column!=null && column.trim().length() == 0 ? "0" : column.trim() ) );
									break;
								// Added by Mary@20120912 - Change_Requets-20120815, ReqNo.12 - END
								// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
								case 28 : 
									column	= column.replace(",", "").trim();
									column	= column.length() == 0 || column.equals("null") ? "0" : column;
									order.setMinimumQuantity( Long.parseLong(column) );
									break;
								case 29 : 
									column	= column.replace(",", "").trim();
									column	= column.length() == 0 || column.equals("null") ? "0" : column;
									order.setDisclosedQuantity( Long.parseLong(column) );
									break;
								// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
								// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15 - START
								case 30 :
									order.setStrStockCurrency( column.trim() ); break;
								// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15 - END
									
								//Added by Thinzar@20140819 - Change_Request-20140801, ReqNo.4 - START
								case 31:
									order.setTriggerPriceType(column.trim());
									break;
									
								case 32:
									order.setTriggerPriceDirection(column.trim());
									break;
								//Added by Thinzar@20140819 - Change_Request-20140801, ReqNo.4 - END
							}
						}
					}
					
					// Added by Mary@20120806 - Fixes_Request, ReqNo.5 - END	
					long lngUnmatchQty	= order.getQuantity() - order.getMatchedQuantity() - order.getCancelledQuantity();
					order.setUnmatchQuantity(lngUnmatchQty);
					// Added by Mary@20120806 - Fixes_Request, ReqNo.5 - END

					tradeOrders.add(0,order);
				}
			}

			return tradeOrders;	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static List<TradeAccount> parseTradeClient(String message) {
		try {
			
			List<TradeAccount> tradeAccounts= new ArrayList<TradeAccount>();
			List<String> fieldIds 			= new ArrayList<String>();
			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			//Mappings is to map between the field id to the java object attribute
			Map<String, Integer> mappings 	= DefinitionConstants.getTradeClientMapping();

			while( tokens.hasMoreTokens() ){
				String lineItem = tokens.nextToken();

				if( lineItem.startsWith(DefinitionConstants.METADATA_HEADER) ){
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while( items.hasMoreTokens() ){
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				}else if( lineItem.startsWith(DefinitionConstants.RECORD_HEADER) ){
					TradeAccount account	= new TradeAccount();
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					String[] items 			= lineItem.split("\\|");

					for (int i=0; i<items.length; i++) {
						//From meta field id is mapped to returned record value.
						String fieldId	= (String) fieldIds.get(i);					
						String column	= items[i];
						Integer id 		= mappings.get(fieldId);

						if(id == null)
							continue;

						switch (id.intValue()) {
							case 0: account.setClientCode(column); break;
							case 1: account.setAccountNo(column); break;
							case 2: account.setClientName(column); break;
							case 3: account.setCdsNo(column); break;
							case 4: account.setRmsCode(column); break;
							case 5: account.setBrokerCode(column); break;
							case 6: account.setBranchCode(column); break;
							case 7: account.setCreditLimit(Double.parseDouble(column)); break;
							case 8: account.setBuyLimit(Double.parseDouble(column)); break;
							case 9: account.setSellLimit(Double.parseDouble(column)); break;
							case 10: account.setBrokerage(column); break;
							case 11: account.setAccountStatus(column); break;
							case 12: account.setExchangeCode(column); break;
							case 13: account.setGroupId(column); break;
							case 14: account.setForceOrder(SystemUtil.convertFlag(column)); break;
							case 15: 
								StringTokenizer codes	= new StringTokenizer(column, ",");
								List<String> exchanges 	= new ArrayList<String>();
								while (codes.hasMoreTokens()) {
									exchanges.add(codes.nextToken());
								}
								account.setSupportedExchange(exchanges);
								break;
							case 16: account.setShortSell(SystemUtil.convertFlag(column)); break;
							// Added by Kw@20130411 - Change_Request-20130225, ReqNo.6
							case 17:account.setMarginAccount((Integer.parseInt(column)>0)?true:false); break;
							//	case 17: account.setMarginAccount(SystemUtil.convertFlag(column)); break;
							
							//Added by Thinzar, Change_Request-20150401, ReqNo.1
							case 19: account.setAccountType(column);	break;
							
							//Added by Thinzar, Change_Request-20160101, ReqNo.1
							case 20: account.setSenderCode(column);		break;
						}		
					}
					
					//Change_Request-20170601, ReqNo. 4
					if(!account.getAccountType().equals("D"))
						tradeAccounts.add(account);
				}
			}

			return tradeAccounts;	
		}catch (Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	public static List<Watchlist> parseFavListInfo(String message) {
		try {
			List<Watchlist> watchlists 		= new ArrayList<Watchlist>();
			List<String> fieldIds 			= new ArrayList<String>();
			StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			Map<String, Integer> mappings 	= DefinitionConstants.getFavListInfoMapping();

			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();

				if (lineItem.startsWith(DefinitionConstants.METADATA_HEADER)) {
					lineItem 				= lineItem.substring(2, lineItem.length() - 1);
					StringTokenizer items 	= new StringTokenizer(lineItem, "|");
					while (items.hasMoreTokens()) {
						String fieldId = items.nextToken();
						fieldIds.add(fieldId);
					}
				} else if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) {
					Watchlist watchlist = new Watchlist();
					lineItem 			= lineItem.substring(2, lineItem.length() - 1);
					String[] items 		= lineItem.split("\\|");
					for (int i=0; i<items.length; i++) {
						String fieldId	= (String) fieldIds.get(i);					
						String column 	= items[i];
						Integer id 		= mappings.get(fieldId);

						if(id == null)
							continue;

						switch (id.intValue()) {
							case 0: watchlist.setClientCode(column); break;
							case 1: watchlist.setId(column); break;
							case 2: watchlist.setName(column); break;
							case 3: watchlist.setExchangeCode(column); break;
							case 4: watchlist.setRemark(column); break;
							case 5: watchlist.setFlag(column); break;
							case 6: watchlist.setStatus(column); break;
							/* Mary@20130704 - temp close this as ATP have incorrect date format
							case 7: 
								if (column.length() != 0) {
									Date date = FormatUtil.parseDateString(column, FormatUtil.NEWSDATE_FORMAT_2);
									watchlist.setLastModified(date);
								} else {
									watchlist.setLastModified(null);
								}
								break;
							*/
						}		
					}
					watchlists.add(watchlist);
				}
			}
			return watchlists;	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static String[] parseFavListStockCode(String message) {
		try {
			List<String> stockSymbols	= new ArrayList<String>();
			StringTokenizer tokens		= new StringTokenizer(message, "\r\n");

			while (tokens.hasMoreTokens()) {
				String lineItem = tokens.nextToken();

				if (lineItem.startsWith(DefinitionConstants.RECORD_HEADER)) { // e.g. )=0065|KL|
					lineItem 			= lineItem.substring(2, lineItem.length() - 1);
					String[] items		= lineItem.split("\\|");
					String stockCode	= items[0];
					String exchangeCode = null;
					/* Mary@20130625 - Fixes_Request-20130523, ReqNo.21
					boolean found 		= false;
					*/

					if(items.length>1){
						exchangeCode 				= items[1];
						String symbolCode 			= stockCode + "." + exchangeCode;
						
						/* Mary@20130625 - Fixes_Request-20130523, ReqNo.21 - START
						List<String> LMSExchange	= DefinitionConstants.getValidQCExchangeCodes();
						for(int i=0; i<LMSExchange.size(); i++){
							if( exchangeCode.equals( LMSExchange.get(i) ) ){
								found = true;
								stockSymbols.add(symbolCode);
								break;
							}
						}
						if(!found)
							stockSymbols.add(stockCode + "."+exchangeCode+"D");
						*/
						if( DefinitionConstants.getValidQCExchangeCodes().contains(exchangeCode) ){
							stockSymbols.add(symbolCode);
						}else{
							String strUpdExchgCode	= exchangeCode;
							if( strUpdExchgCode.length() == 1 || ( strUpdExchgCode.length() > 1 && ! strUpdExchgCode.toUpperCase().endsWith("D") ) )
								strUpdExchgCode	= strUpdExchgCode + "D"; 
							else
								strUpdExchgCode	= strUpdExchgCode.toUpperCase().endsWith("D") ? strUpdExchgCode.substring(0, (strUpdExchgCode.length()-1) ) : strUpdExchgCode;
							
							if( DefinitionConstants.getValidQCExchangeCodes().contains(strUpdExchgCode) )
								stockSymbols.add(stockCode + "." + strUpdExchgCode);
							else
								stockSymbols.add(symbolCode);
						}
						// Mary@20130625 - Fixes_Request-20130523, ReqNo.21 - END
					}
				}
			}

			/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3
			if (stockSymbols.size()==0) {
				return new String[0];
			} else {
				return (String[]) stockSymbols.toArray(new String[1]);
			}
			*/
			return ( (stockSymbols.size()==0) ? new String[0] : (String[]) stockSymbols.toArray(new String[1]) );
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return new String[0];
	}
	
	//Added by Thinzar@20140917, Change_Request-20140901, ReqNo.6 - START
	public static Map<String, String> parseTradeGetPKLaodBalance(String message){
		Map<String, String> loadBalanceMap	= new HashMap<String, String>();
		StringTokenizer tokens				= null;
		List<String> fieldIds				= new ArrayList<String>();
		Map<String, Integer> mappings		= DefinitionConstants.getTradeGetPKLoadBalanceMapping();
		
		tokens = new StringTokenizer(message, "\r\n");
		while(tokens.hasMoreTokens()){
			String lineItem	= tokens.nextToken();
			
			if(lineItem.startsWith(DefinitionConstants.METADATA_HEADER)){
				
				lineItem				= lineItem.substring(2, lineItem.length() - 1);
				StringTokenizer items	= new StringTokenizer(lineItem, "|");
				while(items.hasMoreTokens()){
					String fieldId	= items.nextToken();
					char charId		= fieldId.length() > 0 ? fieldId.charAt(0) : ' ';
					fieldIds.add(String.valueOf(charId));
				}
				
			} else if(lineItem.startsWith(DefinitionConstants.RECORD_HEADER)){
				
				lineItem				= lineItem.substring(2, lineItem.length() - 1);
				String[] items			= lineItem.split("\\|");
				for(int i=0; i<items.length;i++){
					String fieldId	= (String)fieldIds.get(i);
					String column	= items[i];
					
					if(mappings.containsKey(fieldId)){
						Integer id	= mappings.get(fieldId);
						if(id == null) continue;
						
						switch(id.intValue()){
						case 0: loadBalanceMap.put(ParamConstants.PUBLIC_KEY_TAG, column); break;
						case 1: loadBalanceMap.put(ParamConstants.PUBLIC_IP_TAG, column); break;
						case 2: loadBalanceMap.put(ParamConstants.PRIVATE_IP_TAG, column); break;
						case 3: loadBalanceMap.put(ParamConstants.SECONDARY_PORT_TAG, column); break;
						}
					}
				}
			}
		}
		
		
		return loadBalanceMap;
	}
	//Added by Thinzar@20140917, Change_Request-20140901, ReqNo.6 - END
	
	//Added by Thinzar, Change_Request-20150401, ReqNo. 1 - START
	/* for example:
	 	[PaymentCfg_Payment]=SG|AccType:B|Payment:CUT|Sett:+
		[PaymentCfg_Payment]=SG|AccType:Def|Payment:Cash,CUT,CPF,SRS

	 */
	public static Map<String, PaymentConfig> parsePaymentConfig(String message){
		Map<String, PaymentConfig> paymentConfigMap	= new HashMap<String, PaymentConfig>();
		
		//System.out.println("Debug: Parsing payment config");
			
		StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
			
		while (tokens.hasMoreTokens()) {
			String lineItem	= tokens.nextToken();
			
			String[] columns= lineItem.split("]=");
			String key 		= columns[0];
			key				= key + "]";
				
			if(columns.length > 1){
				String value = columns[1];
				if( key.equals("[PaymentCfg_Payment]") ){				
					StringTokenizer items			= new StringTokenizer(value, "|");
					int index 						= 0;
					PaymentConfig	paymentConfig	= new PaymentConfig();
					
					while( items.hasMoreTokens() ){
							
						String item	= items.nextToken();
						if( index == 0 ){
							//this is exchange code
							paymentConfig.setExchangeCode(item);
						} else{
							String[] strArrTemp	= item.split(":");
							if(strArrTemp[0].equalsIgnoreCase("AccType")){
								
								paymentConfig.setAccType(strArrTemp[1]);
									
							} else if(strArrTemp[0].equalsIgnoreCase("Payment")){
								
								String paymentString	= strArrTemp[1];
								String[] arrPayments	= paymentString.split(",");
								for(int j=0;j<arrPayments.length;j++){
									paymentConfig.addPaymentMethod(arrPayments[j]);
								}
								
							} else if(strArrTemp[0].equalsIgnoreCase("Sett")){
								
								String currencyString	= strArrTemp[1];
								paymentConfig.setCurrencyString(currencyString);	//delete this later, debug purpose
								
								String[] arrCurrency	= currencyString.split(",");
								for(int j=0;j<arrCurrency.length;j++){
									paymentConfig.addCurrency(arrCurrency[j]);
								}
							}
						}
						index++;
					}
					String mapKey	= paymentConfig.getExchangeCode() + "|" + paymentConfig.getAccType();
					paymentConfigMap.put(mapKey, paymentConfig);
				}
			}
		}//end while hasMoreTokens
			
		return paymentConfigMap;
	}
	
	public static void addPaymentConfigToExchangeInfo(List<PaymentConfig> lstPaymentConfig){
		
	}
	//Added by Thinzar, Change_Request-20150401, ReqNo. 1 - END
	
	//Added by Thinzar, additional params required for stockAlert
	public static void parseAdditionalUserData(String message){
		StringTokenizer tokens 			= new StringTokenizer(message, "\r\n");
		
		while (tokens.hasMoreTokens()) {
			String lineItem	= tokens.nextToken();
		
			String[] columns= lineItem.split("]=");
			String key 		= columns[0];
			key				= key + "]";
			if(columns.length > 1){
				String value = columns[1];
				
				if( key.equalsIgnoreCase("[Email]") ){
					AppConstants.userEmail	= value;
				}else if( key.equalsIgnoreCase("[Mobile]") ){
					AppConstants.userMobile	= value;
				}
				//Change_Request_20170119, ReqNo.5, required for Dti
				else if(key.equalsIgnoreCase("[SenderName]")){
					AppConstants.userSenderName	= value;
				}
			}
		}
	}
	
	//Added by Thinzar, Fixes_Request-20161101, ReqNo.14 - START
	public static Map<String, String> parseTradePasswdPIN(String message){
		Map<String, String> resultMap	= new HashMap<String, String>();
		StringTokenizer tokens	= new StringTokenizer(message, "\r\n");
		List<Integer> fieldIds	= new ArrayList<Integer>();

		while(tokens.hasMoreTokens()){
			String lineItem	= tokens.nextToken();

			if(lineItem.startsWith(DefinitionConstants.METADATA_HEADER)){
				lineItem 				= lineItem.substring(2, lineItem.length() - 1);
				StringTokenizer items 	= new StringTokenizer(lineItem, "|");
				while (items.hasMoreTokens()) {
					String fieldId	= items.nextToken();
					char charId 	= fieldId.length() > 0 ? fieldId.charAt(0) : ' ';
					int asciiCode 	= (int) charId;
					fieldIds.add(asciiCode);
				}
			} else if(lineItem.startsWith(DefinitionConstants.RECORD_HEADER)){
				lineItem = lineItem.substring(2, lineItem.length() - 1);
				String[] items = lineItem.split("\\|");
				for (int i=0; i<items.length; i++) {
					int fieldId = fieldIds.get(i);					
					String column = items[i];

					switch(fieldId){
					case 50:
						resultMap.put(ParamConstants.STATUS_CODE_TAG, column);
						break;
					case 51:
						resultMap.put(ParamConstants.STATUS_MSG_TAG, column);
						break;
					}
				}
			}
		}

		return resultMap;
	}
	//Added by Thinzar, Fixes_Request-20161101, ReqNo.14 - END
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.8 - START
	public static Map<String, String> parseValidatePreLogin(String message){
		Map<String, String> resultMap	= new HashMap<String, String>();
		StringTokenizer tokens	= new StringTokenizer(message, "\r\n");
		List<Integer> fieldIds	= new ArrayList<Integer>();

		while(tokens.hasMoreTokens()){
			String lineItem	= tokens.nextToken();

			if(lineItem.startsWith(DefinitionConstants.METADATA_HEADER)){
				lineItem 				= lineItem.substring(2, lineItem.length() - 1);
				StringTokenizer items 	= new StringTokenizer(lineItem, "|");
				while (items.hasMoreTokens()) {
					String fieldId	= items.nextToken();
					char charId 	= fieldId.length() > 0 ? fieldId.charAt(0) : ' ';
					int asciiCode 	= (int) charId;
					fieldIds.add(asciiCode);
				}
			} else if(lineItem.startsWith(DefinitionConstants.RECORD_HEADER)){
				lineItem = lineItem.substring(2, lineItem.length() - 1);
				String[] items = lineItem.split("\\|");
				for (int i=0; i<items.length; i++) {
					int fieldId = fieldIds.get(i);					
					String column = items[i];

					switch(fieldId){
					case 50:
						resultMap.put(ParamConstants.PRE_LOGIN_TOKEN_TAG, column);
						break;
					case 51:
						resultMap.put(ParamConstants.STATUS_CODE_TAG, column);
						break;
					case 52:
						resultMap.put(ParamConstants.STATUS_MSG_TAG, column);
						break;
					}
				}
			}
		}

		return resultMap;
	}
	//Added by Thinzar, Change_Request-20160722, ReqNo.8 - END

	//Change_Request-20170601, ReqNo.14
	public static Map<String, String> parseTradeGetPK2A(String message){
		Map<String, String> loadBalanceMap	= new HashMap<String, String>();
		StringTokenizer tokens				= null;
		List<Integer> fieldIds				= new ArrayList<Integer>();

		tokens = new StringTokenizer(message, "\r\n");
		while(tokens.hasMoreTokens()){
			String lineItem	= tokens.nextToken();

			if(lineItem.startsWith(DefinitionConstants.METADATA_HEADER)){

				lineItem				= lineItem.substring(2, lineItem.length() - 1);
				StringTokenizer items	= new StringTokenizer(lineItem, "|");
				while (items.hasMoreTokens()) {
					String fieldId	= items.nextToken();
					char charId 	= fieldId.length() > 0 ? fieldId.charAt(0) : ' ';
					int asciiCode 	= (int) charId;
					fieldIds.add(asciiCode);
				}

			} else if(lineItem.startsWith(DefinitionConstants.RECORD_HEADER)){

				lineItem				= lineItem.substring(2, lineItem.length() - 1);
				String[] items			= lineItem.split("\\|");
				for(int i=0; i<items.length;i++){
					int fieldId	= fieldIds.get(i);
					String column	= items[i];

					switch(fieldId){
						case 44: loadBalanceMap.put(ParamConstants.PUBLIC_KEY_TAG, column); break;
						case 45: loadBalanceMap.put(ParamConstants.PUBLIC_IP_TAG, column); break;
						case 46: loadBalanceMap.put(ParamConstants.PRIVATE_IP_TAG, column); break;
						case 47: loadBalanceMap.put(ParamConstants.PRIMARY_PORT_TAG, column); break;
						case 48: loadBalanceMap.put(ParamConstants.SECONDARY_PORT_TAG, column); break;
						case 49: loadBalanceMap.put(ParamConstants.PK_MODULUS_TAG, column); break;
						case 50: loadBalanceMap.put(ParamConstants.PK_EXPONENT_TAG, column); break;
					}
				}
			}
		}

		return loadBalanceMap;
	}
}