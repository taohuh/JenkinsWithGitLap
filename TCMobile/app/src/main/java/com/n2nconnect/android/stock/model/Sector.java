package com.n2nconnect.android.stock.model;

import java.util.ArrayList;
import java.util.List;

public class Sector implements Comparable<Sector> {
	private String sectorName;
	private String sectorCode;
	private String parentCode;
	private Sector parentSector;
	private List<Sector> childSectors;
	private int indexCount;
	//private int totalCount;
	
	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START
/*	
	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	*/
	//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END

	public int getIndexCount() {
		return indexCount;
	}

	public void setIndexCount(int indexCount) {
		this.indexCount = indexCount;
	}

	public String toString() {
        return sectorName;
    }
	
	public Sector() {
		childSectors = new ArrayList<Sector>();
	}
	
	public void addChildSector(Sector sector) {
		childSectors.add(sector);
	}
	
	public String getParentCode() {
		return parentCode;
	}
	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
	public String getSectorName() {
		return sectorName;
	}
	public void setSectorName(String sectorName) {
		this.sectorName = sectorName;
	}
	public String getSectorCode() {
		return sectorCode;
	}
	public void setSectorCode(String sectorCode) {
		this.sectorCode = sectorCode;
	}
	public Sector getParentSector() {
		return parentSector;
	}
	public void setParentSector(Sector parentSector) {
		this.parentSector = parentSector;
	}
	public List<Sector> getChildSectors() {
		return childSectors;
	}
	public void setChildSectors(List<Sector> childSectors) {
		this.childSectors = childSectors;
	}

	@Override
	public int compareTo(Sector compare) {
		if (Integer.parseInt(sectorCode) == Integer.parseInt(compare.getSectorCode())) {
			return 0;
		} else if (Integer.parseInt(sectorCode) > Integer.parseInt(compare.getSectorCode())) {
			return 1;
		} else {
			return -1;
		}
		
	}
	
}
