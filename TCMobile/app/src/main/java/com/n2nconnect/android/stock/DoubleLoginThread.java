package com.n2nconnect.android.stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.n2nconnect.android.stock.util.AtpConnectUtil;

public class DoubleLoginThread extends Thread {
	
	private OnDoubleLoginListener registeredListener;
	private int refreshRate;
	private String userParam;
	private volatile boolean stopRequested; 
	private Thread runThread;
	private String username;
	private byte[] userParamInByte;
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
	private boolean mPaused;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	private int intRetryCtr = 0;
	
	public OnDoubleLoginListener getRegisteredListener() {
		return registeredListener;
	}
	
	public void setRegisteredListener(OnDoubleLoginListener registeredListener) {
		this.registeredListener = registeredListener;
	}
	
	public interface OnDoubleLoginListener {
		public void DoubleLoginEvent(List<String> response);
	}
	
	public DoubleLoginThread(String user, String userParam) {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		this.mPaused		= false;
		this.stopRequested 	= false;
		this.refreshRate 	= AppConstants.CHECK_DOUBLE_LOGIN_RATE;
		this.username 		= user;
		this.userParam 		= userParam;
	}
	
	public void setUserParam(byte[] userParam){
		this.userParamInByte = userParam;
	}
	
	public void run(){
		
		while(!stopRequested){
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			synchronized (this) {
				while (mPaused) {
					try {
						wait();
					} catch (InterruptedException e) {
					}
				}
			}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			
			try{
				runThread = Thread.currentThread();
				
				if (refreshRate > 0) {
					long waitTime = refreshRate * 1000;
					Thread.sleep(waitTime);
				} else if (refreshRate == 0) {
					Thread.sleep(AppConstants.CHECK_DOUBLE_LOGIN_RATE * 1000);
					continue;
				} else {				
					Thread.sleep(AppConstants.CHECK_DOUBLE_LOGIN_RATE * 1000);
				}
				
				Map<String, Object> parameters = new HashMap<String, Object>();
				
				/* Mary@20121005 - Fixes_Request-20120815, ReqNo.3
				if(AppConstants.EnableEncryption ==false){
					parameters.put(ParamConstants.USERPARAM_TAG, userParam);
				}else{
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, userParamInByte);
				}
				*/
				/* Mary@20130111 - Fixes_Request-20130108, ReqNo.5
				if(AppConstants.EnableEncryption)
				*/
				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, userParam);
				
				parameters.put(ParamConstants.USERNAME_TAG, username);


				String response	= AtpConnectUtil.CheckSingleLogon(parameters);
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response	= AtpConnectUtil.CheckSingleLogon(parameters);
				}
				intRetryCtr = 0;
				// NOTE : for double login checking, if after retry still can't have response then just ignore as this checking fail should not effect
				
				if(response != null){
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					List<String> parseResult = AtpMessageParser.parseSingleLogonResponse(response);
					
					if (registeredListener != null) {
						registeredListener.DoubleLoginEvent(parseResult);
					}
				}
			}catch (InterruptedException e) {
				e.printStackTrace();
			} catch (FailedAuthenicationException e) {				
				e.printStackTrace();
				
				// Added by Mary@20130827 - Fixes_Request-20130711, ReqNo.13 - START
				List<String> parseResult = new ArrayList<String>();
				parseResult.add(username);
				parseResult.add( e.getMessage() );
				parseResult.add("-99");
								
				if (registeredListener != null) {
					registeredListener.DoubleLoginEvent(parseResult);
				}
				// Added by Mary@20130827 - Fixes_Request-20130711, ReqNo.13 - END
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public void stopRequest() {
        stopRequested = true;

        if (runThread != null ) {
            runThread.interrupt();
        }
    }
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	public void pauseRequest() {
		synchronized (this){
			mPaused = true;
		}
	}

	public void resumeRequest() {
		synchronized (this){
			mPaused = false;
			notify();
		}
	}
	
	public boolean isPaused(){
		return mPaused;
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
}
