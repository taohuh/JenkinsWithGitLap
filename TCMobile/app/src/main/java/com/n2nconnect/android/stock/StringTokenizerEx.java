package com.n2nconnect.android.stock;

import java.util.Vector;

public class StringTokenizerEx extends Object {
	final String sDEFAULT_SEP_CONST = "|";
	final int iVECTOR_INITIAL_SIZE = 25;
	final int iVECTOR_GROWTH_SIZE = 25;

	String m_sInput;
	String m_sDelim;
	Object m_aoTokens[];
	int m_iNumTokens;
	int m_iCurPos;
	boolean m_bInit = false;

	/** Creates new StringTokenizerEx */
	public StringTokenizerEx(String p_sStr) {
		m_sInput = p_sStr;
		m_sDelim = sDEFAULT_SEP_CONST;
		m_iNumTokens = 0;
		m_iCurPos = -1;
		m_bInit = true;
		tokenizeData();
	}

	/** Creates new StringTokenizerEx */
	public StringTokenizerEx(String p_sStr, String p_sDelim) {
		m_sInput = p_sStr;
		m_sDelim = p_sDelim;
		m_iNumTokens = 0;
		m_iCurPos = -1;
		m_bInit = true;
		tokenizeData();
	}

	public int countTokens() {
		if (!m_bInit) {
		  return 0;
		}

		return m_iNumTokens;
	}

	/*
	boolean hasMoreElements() {
		return true;
	}
	*/

	public boolean hasMoreTokens() {
		if (!m_bInit) {
			return false;
		}

		if ((m_iCurPos +1) < m_iNumTokens) {
			return true;
		}
		return false;
	}

	/*
	Object nextElement() {
		return null;
	}
	*/

	public String nextToken() {
		if (!m_bInit) {
			return "";
		}

		if (++m_iCurPos < m_iNumTokens) {
			return (String) m_aoTokens[m_iCurPos];
		}
		return "";
	}

	/*
	String nextToken(String delim) {
		return "not implemented";
	}
	*/

	private void tokenizeData() {
		Vector<String> vTokens = new Vector<String>(iVECTOR_INITIAL_SIZE, iVECTOR_GROWTH_SIZE);
		int iPos, iPrevPos = 0;
		String sWork;

		if (m_sInput == null) {
			m_iNumTokens = 0;
			return;
		}

		for (iPrevPos=0, iPos=m_sInput.indexOf(m_sDelim, 0); iPos >= 0; iPrevPos=iPos +1, iPos=m_sInput.indexOf(m_sDelim, iPrevPos)) {
			if (iPos == iPrevPos) {
				sWork = "";
			} else {
				sWork = m_sInput.substring(iPrevPos, iPos).trim();
			}
			vTokens.addElement(sWork);
		}
		// add the last token as well
		sWork = m_sInput.substring(iPrevPos, m_sInput.length());
		vTokens.addElement(sWork);

//		m_aoTokens = vTokens.toArray();
		m_aoTokens = new Object[vTokens.size()];
		vTokens.copyInto(m_aoTokens);
		m_iNumTokens = m_aoTokens.length;
	}
}