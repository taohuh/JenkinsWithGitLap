package com.n2nconnect.android.stock.model;

import java.util.ArrayList;

import android.util.Log;

import com.n2nconnect.android.stock.ParamConstants;

public class DerivativeDetail {
	
	private int totalBuyQtyD,totalSellQtyD ;
	private int totalBuyQtyO,totalSellQtyO;
	private float avgBuyPriceD,avgSellPriceD;
	private float avgBuyPriceO,avgSellPriceO;
	private float totalBuyPriceD,totalSellPriceD;
	private float totalBuyPriceO,totalSellPriceO;
	private float totalUnrealised;
	
	private ArrayList<DervTradePrtfDetailRpt> dervTradePrtfDetailRptListO,dervTradePrtfDetailRptListD;

	public ArrayList<DervTradePrtfDetailRpt> getDervTradePrtfDetailRptListO() {
		return dervTradePrtfDetailRptListO;
	}

	public void setDervTradePrtfDetailRptListO(
			ArrayList<DervTradePrtfDetailRpt> dervTradePrtfDetailRptListO) {
		this.dervTradePrtfDetailRptListO = dervTradePrtfDetailRptListO;
	}

	public ArrayList<DervTradePrtfDetailRpt> getDervTradePrtfDetailRptListD() {
		return dervTradePrtfDetailRptListD;
	}

	public void setDervTradePrtfDetailRptListD(
			ArrayList<DervTradePrtfDetailRpt> dervTradePrtfDetailRptListD) {
		this.dervTradePrtfDetailRptListD = dervTradePrtfDetailRptListD;
	}

	
	public void calculate(DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt){

		//day calculation
		totalBuyQtyD = 0; totalSellQtyD = 0; totalBuyPriceD = 0; totalSellPriceD = 0;
		avgBuyPriceD = 0; avgSellPriceD = 0;
		
		for(DervTradePrtfDetailRpt dervTradePrtfDetailRpt:dervTradePrtfDetailRptListD){
			if(dervTradePrtfDetailRpt.getSide().equalsIgnoreCase(ParamConstants.SELL_SIDE)){
				totalSellQtyD =  totalSellQtyD + dervTradePrtfDetailRpt.getMatchedQty();
				totalSellPriceD = totalSellPriceD + (dervTradePrtfDetailRpt.getMatchedPrice()*dervTradePrtfDetailRpt.getMatchedQty());
			}else if(dervTradePrtfDetailRpt.getSide().equalsIgnoreCase(ParamConstants.BUY_SIDE)){
				totalBuyQtyD =  totalBuyQtyD + dervTradePrtfDetailRpt.getMatchedQty();
				totalBuyPriceD = totalBuyPriceD + (dervTradePrtfDetailRpt.getMatchedPrice()*dervTradePrtfDetailRpt.getMatchedQty());
			}
		}
		
		if(totalBuyPriceD!=0 && totalBuyQtyD!=0)
			avgBuyPriceD = totalBuyPriceD/totalBuyQtyD; 
		if(totalSellPriceD!=0 && totalSellQtyD!=0)
			avgSellPriceD = totalSellPriceD/totalSellQtyD;
		
		//overnight calculation
		totalBuyQtyO = 0; totalSellQtyO = 0; totalBuyPriceO = 0; totalSellPriceO = 0;
		avgBuyPriceO = 0; avgSellPriceO = 0; totalUnrealised = 0;
		
		for(DervTradePrtfDetailRpt dervTradePrtfDetailRpt:dervTradePrtfDetailRptListO){
			if(dervTradePrtfDetailRpt.getSide().equalsIgnoreCase(ParamConstants.SELL_SIDE)){
				totalSellQtyO =  totalSellQtyO + dervTradePrtfDetailRpt.getMatchedQty();
				totalSellPriceO = totalSellPriceO + (dervTradePrtfDetailRpt.getMatchedPrice()*dervTradePrtfDetailRpt.getMatchedQty());
			}else if(dervTradePrtfDetailRpt.getSide().equalsIgnoreCase(ParamConstants.BUY_SIDE)){
				totalBuyQtyO =  totalBuyQtyO + dervTradePrtfDetailRpt.getMatchedQty();
				totalBuyPriceO = totalBuyPriceO + (dervTradePrtfDetailRpt.getMatchedPrice()*dervTradePrtfDetailRpt.getMatchedQty());
			}
			dervTradePrtfDetailRpt.calculateUnrealised(dervTradePrtfSubDetailRpt);
			totalUnrealised = totalUnrealised + dervTradePrtfDetailRpt.getUnrealised();
		}

		if(totalBuyPriceO!=0 && totalBuyQtyO!=0)
			avgBuyPriceO = totalBuyPriceO/totalBuyQtyO;
		if(totalSellPriceO!=0 && totalSellQtyO!=0)
			avgSellPriceO = totalSellPriceO/totalSellQtyO;
	}

	public int getTotalBuyQtyD() {
		return totalBuyQtyD;
	}

	public int getTotalSellQtyD() {
		return totalSellQtyD;
	}

	public int getTotalBuyQtyO() {
		return totalBuyQtyO;
	}

	public int getTotalSellQtyO() {
		return totalSellQtyO;
	}

	public float getAvgBuyPriceD() {
		return avgBuyPriceD;
	}

	public float getAvgSellPriceD() {
		return avgSellPriceD;
	}

	public float getAvgBuyPriceO() {
		return avgBuyPriceO;
	}

	public float getAvgSellPriceO() {
		return avgSellPriceO;
	}

	public float getTotalBuyMatchedPriceD() {
		return totalBuyPriceD;
	}

	public float getTotalSellMatchedPriceD() {
		return totalSellPriceD;
	}

	public float getTotalBuyMatchedPriceO() {
		return totalBuyPriceO;
	}

	public float getTotalSellMatchedPriceO() {
		return totalSellPriceO;
	}

	public float getTotalUnrealised() {
		return totalUnrealised;
	}

}
