package com.n2nconnect.android.stock;


public class InvalidPasswordException extends Exception {
	
	private String errorCode;
	
	public InvalidPasswordException(String errorMessage) {
	    super(errorMessage);
	}

	public InvalidPasswordException(String errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
	}
	
}
