package com.n2nconnect.android.stock.activity;

import java.text.Format;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.ExchangeListView;
import com.n2nconnect.android.stock.custom.ExchangeListView.OnExchangListEventListener;
import com.n2nconnect.android.stock.custom.PaymentTypeFilterListView;
import com.n2nconnect.android.stock.custom.PaymentTypeFilterListView.OnPayTypeFilterSelectedListener;
import com.n2nconnect.android.stock.model.Derivative;
import com.n2nconnect.android.stock.model.DervTradePrtfSubDetailRpt;
import com.n2nconnect.android.stock.model.DervTradePrtfSummRpt;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.Sector;
import com.n2nconnect.android.stock.model.StockHolding;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class PortfolioActivity extends CustomWindow implements OnDoubleLoginListener, OnRefreshStockListener
/* Mary@20130715 - Change_Request-20130424, ReqNo.15
, OnPortfolioListener
 */
, OnExchangListEventListener, OnPayTypeFilterSelectedListener  {

	private TradeAccount tradeAccount;
	private List<StockHolding> stockHoldings;
	// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13
	private List<StockHolding> filteredStockHoldings;
	private List<Button> quantityButtons;
	/* Mary@20130604 - comment unused variable
	private ExchangeInfo selectedExchange;
	 */
	private DoubleLoginThread loginThread;
	/* Mary@20130715 - Change_Request-20130424, ReqNo.15
	private PortfolioThread portfolioThread;
	 */
	private boolean isDisplayPercentage = true;  
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private String userParam;
	 */
	private List<StockSymbol> refreshStocks = null;
	private TableLayout tableLayout;
	private ViewPager viewPager, viewPagerDerivative;
	//private ScrollView scrollView;
	private float[] percentPL 				= null;
	private double[] unrealizeGL 			= null;
	/* Mary@20130723 - Fixes_Request-20130711, ReqNo.8
	private double marketValue;
	 */
	private double[] marketValue			= null;
	private HashMap<String, String> details;
	private ArrayList<String> sequences;
	private String atpUserParam;
	private String senderCode;
	private ProgressDialog dialog;
	private TextView accountNoLabel;
	private TextView clientNameLabel;
	private LinearLayout selectedRow;
	private int selectedId;
	private boolean isExpired;
	private StockApplication application;

	private boolean isConstructing;
	private boolean[] expandid;

	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr = 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	// Added by Mary@20120804 - Fixes_Request-20120724, ReqNo.8
	//private TextView headerLabel;
	//private TextView headerlabel2;

	/* Mary@20130604 - comment unused variable
	// Added by Mary@20121018 - Fixes_Request-20121018, ReqNo.8
	private int intMaxCtr	= 100;
	 */

	// Added by Mary@20130122 - Change_Request-20130104, ReqNo.5 - START
	private String strSelectedFilterExchangeType;
	private Button btnExchangeType;
	private ExchangeListView exchgListView;
	private Map<String, Object> selectedExchangeMap;
	private String ALL_TYPE_KEY	= null;
	// Added by Mary@20130122 - Change_Request-20130104, ReqNo.5 - END

	// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27
	private int intPortfolioType	= ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED;

	// Added by Melissa - START
	private ImageButton btnRefresh;

	private RefreshStockThread refreshThread;
	private String[] symbolCodes;
	private Map<String, Integer> dervTradePrtfSubDetailRptMap;
	private Derivative derivative = null;

	//private ImageView leftImage = null;
	//private ImageView rightImage = null;
	private TextView leftTextView, rightTextView;

	private boolean derivativePortfolio = false;
	private PagerAdapter viewPagerAdapter, viewPagerAdapterDerivative;

	private String UNREALIZED 	= "Unrealized G/L";
	private String REALIZED 	= "Realized G/L";
	private String VIRTUALPL 	= "Virtual P/L";
	private String SUMMARY 		= "Summary";

	private String LAST 			= "Last";
	private String AVERAGE_PRICE	= "Avg. Price";

	private int UPDATE_FR_ATP = 123;
	private int UPDATE_FR_FEED = 756;
	private int NEW = 345;

	private String MKT_VAL = "Mkt. Val.";
	private String GL = "G/L";
	private String GLP = "G/L%";

	private DervTradePrtfSummRpt dervTradePrtfSummRpt;
	private ArrayList<DervTradePrtfSubDetailRpt> dervTradePrtfSubDetailRptList;

	private int CASH_BALANCE_TAG = 1;
	private int DEPOSIT_TAG = 2;
	private int WITHDRAWAL_TAG = 3;
	private int REALIZED_TAG = 4;
	private int CURRENT_BALANCE_TAG = 5;
	private int UNREALIZED_TAG = 6;
	private int EQUITY_TAG = 7;
	private int BUY_OPTIONMKTVAL_TAG = 8;
	private int SELL_OPTIONMKTVAL_TAG = 9;
	private int NETT_LIQUIDATION_TAG = 10;
	private int ELIGIBLE_COLLATERAL_TAG = 11;
	private int INITIAL_MARGIN_TAG = 12;
	private int EXCESS_SHORTFALL_TAG = 13;
	private int MARGIN_CALL_TAG = 14;
	private int ELIGIBILITY_TAG = 15;
	private int MAINTENANCE_MARGIN_TAG = 16;
	// Added by Melissa - END

	//added by Thinzar@20140730, Change_Request-20140701, ReqNo.9 - START
	private Button btnPaymentType;
	private List<String> pymtTypes;
	private PaymentTypeFilterListView payFilterListView;
	private String strSelectedFilterPayType;
	//added by Thinzar@20140730 - END

	//Added by Thinzar, Fixes_Request-20140820, ReqNo.33
	private boolean isDifferentClient = false;

	//Change_Request-20160101, ReqNo.9 - variables for portfolio sorting
	private StockHolding mCounter;
	private final int SORT_BY_NAME_ASC	= 1;
	private final int SORT_BY_NAME_DESC	= 2;
	private final int SORT_BY_GL_ASC	= 3;
	private final int SORT_BY_GL_DESC	= 4;
	private int currentUnrealiseSortByMethod	= SORT_BY_NAME_ASC;
	private int currentRealiseSortByMethod		= SORT_BY_NAME_ASC;
	private boolean isSortByUnrealiseGLPercent	= false;	//variable to hide show sort order arrow on changeButtonValue

	public void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);

			setContentView(R.layout.portfolio_view);

			this.title.setText(getResources().getString(R.string.portfolio_module_title));
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntPortfolioMenuIconBgImg() );

			application 				= (StockApplication) this.getApplication();

			// Added by Melissa - START
			btnRefresh = (ImageButton)findViewById(R.id.btnRefresh);
			btnRefresh.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View view) {
					if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) ){
						//edited by Thinzar@20140807 - Change_Request-20140807, ReqNo.10
						if(intPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_REALIZED || intPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED)
							new stockRefresh().execute(intPortfolioType);
						else
							new derivativeTradeRefresh().execute(UPDATE_FR_ATP);
						//edited by Thinzar@20140807 - END
					}
				}
			});

			tableLayout 				=  new TableLayout(PortfolioActivity.this);
			viewPager					= (ViewPager) findViewById(R.id.pager);
			viewPager.setOnPageChangeListener(new OnPageChangeListener(){
				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
				@Override
				public void onPageScrolled(int page, float arg1, int arg2) {

				}
				@Override
				public void onPageSelected(int page) {

					if(page == ParamConstants.INT_PAGE_UNREALIZED){
						unrealiseSelected();
					}else{
						realiseSelected();
					}

				}
			});

			viewPagerDerivative			= (ViewPager) findViewById(R.id.pager_derivative);
			viewPagerDerivative.setOnPageChangeListener(new OnPageChangeListener(){
				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}
				@Override
				public void onPageSelected(int page) {
					if(page == ParamConstants.INT_PAGE_VIRTUALPL){
						virtualPLSelected();
					}else{
						summarySelected();
					}

				}
			});

			accountNoLabel 				= (TextView) this.findViewById(R.id.accountNoLabel);
			clientNameLabel				= (TextView) this.findViewById(R.id.clientNameLabel);

			accountNoLabel.setText("-");
			clientNameLabel.setText("-");

			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(PortfolioActivity.this)
			//.setIcon(android.R.attr.alertDialogStyle)
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,	int id) {
					dialog.dismiss();
				}
			})
			.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			leftTextView = (TextView)findViewById(R.id.leftLabel);
			rightTextView = (TextView)findViewById(R.id.rightLabel);
			LinearLayout leftLayout 	= (LinearLayout)findViewById(R.id.leftLayout);
			LinearLayout rightLayout 	= (LinearLayout)findViewById(R.id.rightLayout);

			// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START
			leftLayout.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View view) {

					if(viewPager.getVisibility() == View.VISIBLE){
						//unrealiseSelected();
						viewPager.setCurrentItem(ParamConstants.INT_PAGE_UNREALIZED);
						viewPager.performClick();		//Edited by Thinzar, to prevent unrealiseSelected() getting called twice
					}else{
						virtualPLSelected();			
						viewPagerDerivative.setCurrentItem(ParamConstants.INT_PAGE_VIRTUALPL);
					}
				}
			});

			rightLayout.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					if(viewPager.getVisibility() == View.VISIBLE){
						//realiseSelected();
						viewPager.setCurrentItem(ParamConstants.INT_PAGE_REALIZED);
						viewPager.performClick();	//Edited by Thinzar, to prevent realiseSelected() getting called twice
					}else{
						summarySelected();
						viewPagerDerivative.setCurrentItem(ParamConstants.INT_PAGE_SUMMARY);
					}
				}

			});

			if(intPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED){

				setSelectedPortfolioHeader(leftTextView);
			}else{

				setSelectedPortfolioHeader(rightTextView);
			}
			// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END

			// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - START
			ALL_TYPE_KEY	= getResources().getString(R.string.all_type_key);
			btnExchangeType = (Button)findViewById(R.id.btnExchangeType);
			btnRefresh = (ImageButton)findViewById(R.id.btnRefresh);

			exchgListView = new ExchangeListView(PortfolioActivity.this, PortfolioActivity.this, getLayoutInflater(), null);
			exchgListView.setHideOnSelect(true);

			btnExchangeType.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					doExchangeList();
				}        	
			});
			// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - END

			//added by Thinzar@20140730, Change_Request-20140701, ReqNo.9 - START
			btnPaymentType			= (Button)findViewById(R.id.btnPaymentType);
			pymtTypes = ((StockApplication)PortfolioActivity.this.getApplication()).getPaymentTypes();

			if(pymtTypes.size() > 0){
				btnPaymentType.setVisibility(View.VISIBLE);

				payFilterListView = new PaymentTypeFilterListView(this, this, getLayoutInflater());
				try{
					payFilterListView.setPayTypeFilterList(pymtTypes);
				} catch(Exception e){
					e.printStackTrace();
				}
				btnPaymentType.setOnClickListener(paymentFilter);

			} else {
				strSelectedFilterPayType = null;
				btnPaymentType.setVisibility(View.GONE);
			}
			//added by Thinzar@20140730 - END 
			
			//initialize variables for localization purpose
			UNREALIZED = getResources().getString(R.string.derivative_unrealized_pl);		//"Unrealized G/L";
			REALIZED 	= getResources().getString(R.string.derivative_realized_pl);		//"Realized G/L";
			VIRTUALPL 	= getResources().getString(R.string.portfolio_param_virtual_pl);	//"Virtual P/L";
			SUMMARY 	= getResources().getString(R.string.portfolio_param_summary);		//"Summary";
			LAST 		= getResources().getString(R.string.title_last);				//"Last";
			AVERAGE_PRICE	= getResources().getString(R.string.derivative_avg_price);		//"Avg. Price";

			MKT_VAL = getResources().getString(R.string.portf_title_mkt_val);	//"Mkt. Val.";
			GL = getResources().getString(R.string.portf_title_gl);			//"G/L";
			GLP = getResources().getString(R.string.portf_title_gl_percent);	//"G/L%";

		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void setSelectedPortfolioHeader(TextView txtLabel){
		leftTextView.setBackgroundResource(R.drawable.selector_grey);
		rightTextView.setBackgroundResource(R.drawable.selector_grey);

		leftTextView.setTextColor(getResources().getColor(R.color.order_row_key_text_color));
		rightTextView.setTextColor(getResources().getColor(R.color.order_row_key_text_color));

		txtLabel.setBackgroundColor(getResources().getColor(R.color.light_grey_bg));
		txtLabel.setTextColor(getResources().getColor(android.R.color.black));
	}

	public void onResume() {
		try{
			super.onResume();

			// Added by Mary@20130718 - Fixes_Request-20130711, ReqNo.4 - START
			loginThread = ( (StockApplication) PortfolioActivity.this.getApplication() ).getDoubleLoginThread();
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );

					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);

					loginThread.start();
					( (StockApplication) PortfolioActivity.this.getApplication() ).setDoubleLoginThread(loginThread);
				}
				// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(PortfolioActivity.this);
				loginThread.resumeRequest();
				// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			}
			loginThread.setRegisteredListener(PortfolioActivity.this);
			// Added by Mary@20130718 - Fixes_Request-20130711, ReqNo.4 - END

			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= PortfolioActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END

			atpUserParam			= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null); 
			senderCode				= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null); 

			dialog					= new ProgressDialog(PortfolioActivity.this);

			AppConstants.showMenu 	= true;

			if(AppConstants.selectedAcc == null){
				tradeAccount 			= (TradeAccount) application.getSelectedAccount(); 
				AppConstants.selectedAcc= tradeAccount;
			}else{
				tradeAccount 			= AppConstants.selectedAcc;
			}

			if( selectedRow!=null && ( filteredStockHoldings != null && filteredStockHoldings.size() > selectedId ) ){

				StockHolding stockholding = filteredStockHoldings.get(selectedId);
				if( stockholding.isExpand() ) {
					int childCount = selectedRow.getChildCount();
					if(childCount!=1)
						selectedRow.removeViewAt(1);

					stockholding.setExpand(false);
				}
			}

			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) ){
				new getClientLimit().execute();
			}

			List<TradeAccount> Accounts	= ((StockApplication) PortfolioActivity.this.getApplication()).getTradeAccounts();
			ImageView detailImage 		= (ImageView) this.findViewById(R.id.detailViewImage);

			if(Accounts.size()<=1)
				detailImage.setVisibility(View.GONE);

			detailImage.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					if( PortfolioActivity.this.mainMenu.isShowing() )
						mainMenu.hide();

					Intent intent = new Intent();
					intent.putExtra(ParamConstants.FILTER_BY_EXCHG, false);
					intent.putExtra(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
					intent.setClass(PortfolioActivity.this, SelectAccountActivity.class);

					PortfolioActivity.this.startActivityForResult(intent, AppConstants.SELECT_ACCOUNT_REQUEST);
				}
			});
			expandid= new boolean[10];


			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
			List<Map<String, Object>> listExchangeMap	= new ArrayList<Map<String, Object>>();
			try{
				// All Exchange
				Map<String, Object> exchgMap			= new HashMap<String, Object>();
				exchgMap.put(ParamConstants.PARAM_STR_TRD_EXCHG_CODE, ALL_TYPE_KEY);
				exchgMap.put(ParamConstants.PARAM_STR_EXCHG_NAME, getResources().getString(R.string.all_exchange_type_label));
				exchgMap.put(ParamConstants.PARAM_INT_EXCHG_IMAGE_ID, null);

				listExchangeMap.add(exchgMap);

				Map<String, Integer> mapExchgImages = DefinitionConstants.getExchangeImageMapping();
				List<String> listQCExchgCode		= DefinitionConstants.getValidQCExchangeCodes();

				for(int i=0; i<listQCExchgCode.size(); i++){
					String strExchgCode		= DefinitionConstants.getTrdExchangeCode().get(i);
					String strQCExchgCode	= listQCExchgCode.get(i);
					String strExchgName		= DefinitionConstants.getExchangeNameMapping().get(strQCExchgCode);

					exchgMap				= new HashMap<String, Object>();
					exchgMap.put(ParamConstants.PARAM_STR_TRD_EXCHG_CODE, strExchgCode);		//Change_Request-20141101, ReqNo 20
					exchgMap.put(ParamConstants.PARAM_STR_EXCHG_NAME, strExchgName);
					exchgMap.put(ParamConstants.PARAM_INT_EXCHG_IMAGE_ID, mapExchgImages.get(strQCExchgCode) == null ? null : ( mapExchgImages.get(strQCExchgCode) ).intValue() );

					listExchangeMap.add(exchgMap);
				}

				exchgListView.setListExchangeMap(listExchangeMap);
			} catch (Exception e) {			
				e.printStackTrace();
			}

			if (selectedExchangeMap == null) {
				for( Iterator<Map<String, Object>> itr=exchgListView.getListExchangeMap().iterator(); itr.hasNext(); ){
					Map<String, Object> mapExchange		= itr.next();
					String strExchgCode = String.valueOf( mapExchange.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
					if( strExchgCode.equalsIgnoreCase(ALL_TYPE_KEY) ){
						selectedExchangeMap = mapExchange;
						break;
					}
				}
			}
			btnExchangeType.setText( String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onPause();

			if(refreshThread != null)
				refreshThread.pauseRequest();

			if(this.mainMenu.isShowing())
				this.mainMenu.hide();

			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private class getClientLimit extends AsyncTask<Void, Void, String>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				PortfolioActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! PortfolioActivity.this.isFinishing() )
					PortfolioActivity.this.dialog.show();
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(Void... params) {
			// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.3
			try{
				Map<String, Object> parameters = new HashMap<String, Object>();

				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
				// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);

				// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.3 - START
				if(tradeAccount == null){
					if(AppConstants.selectedAcc == null){
						tradeAccount 			= (TradeAccount) application.getSelectedAccount();
						AppConstants.selectedAcc= tradeAccount;
					}else{
						tradeAccount 			= AppConstants.selectedAcc;
					}
				}
				// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.3 - END

				parameters.put(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
				parameters.put(ParamConstants.CLIENTCODE_TAG, tradeAccount.getClientCode());
				parameters.put(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
				parameters.put(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
				parameters.put(ParamConstants.EXCHANGECODE_TAG, tradeAccount.getExchangeCode());

				//Edited by Thinzar, Fixes_Request-20170103, ReqNo.2
				if(tradeAccount.getAccountType().equalsIgnoreCase("B")){
					parameters.put(ParamConstants.PAYMENT_TYPE_TAG, "CUT");	
					parameters.put(ParamConstants.PAYMENT_CURRENCY_TAG, AppConstants.getStrBaseCurrencyCode());
				}

				String result = null;

				try {
					result = AtpConnectUtil.tradeClientLimit(parameters);  

					//Added by Thinzar, Change_Request-20150901, ReqNo.1
					AppConstants.connectExceptionCount = 0;
					AppConstants.timeoutExceptionCount = 0;

					while(intRetryCtr < AppConstants.intMaxRetry && result == null){
						intRetryCtr++;
						result = AtpConnectUtil.tradeClientLimit(parameters);
					}
					intRetryCtr	= 0;

					//Edited by Thinzar, Change_Request-20150901, ReqNo.1
					if (result == null){
						if(AppConstants.connectExceptionCount >= AppConstants.intMaxRetry){
							throw new Exception(ErrorCodeConstants.NETWORK_UNAVAILABLE);
						}
						else if(AppConstants.timeoutExceptionCount >= AppConstants.intMaxRetry){
							throw new Exception(ErrorCodeConstants.FAIL_CONNECT_ATP);
						} else
							throw new Exception(ErrorCodeConstants.FAIL_GET_TRADE_LIMIT);
					}
				}catch(FailedAuthenicationException e){        	
					isExpired = true;
					LoginActivity.QCAlive.stopRequest();
					LoginActivity.refreshThread.stopRequest();
				} catch (Exception e) {
					exception	= e;
				}

				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				return result;
				// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.3 - START
			}catch(Exception e){
				e.printStackTrace();
				exception 	= e;
				return null;
			}
			// Added by Mary@20130904 - GoogleCrashReport-20130822, ReqNo.3 - END
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if(isExpired){
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					if(dialog != null && dialog.isShowing() )
						dialog.dismiss();
					PortfolioActivity.this.sessionExpired();
					return;
				}

				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(result == null || exception != null){

					//Added by Thinzar, Change_Request-20150901, ReqNo.1 - START
					boolean isATPConnectionLost = (exception.getMessage().equalsIgnoreCase(ErrorCodeConstants.FAIL_CONNECT_ATP))? true:false;
					prepareAlertException(PortfolioActivity.this.alertException, exception.getMessage(), isATPConnectionLost);

					if( ! PortfolioActivity.this.isFinishing() ){
						PortfolioActivity.this.alertException.setCanceledOnTouchOutside(false);
						alertException.show();
					}
					//Added by Thinzar, Change_Request-20150901, ReqNo.1 - END

					exception	= null;
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

				String balanceText	= AtpMessageParser.extractTradeClientLimit(result);
				double balance		= Double.parseDouble(balanceText);

				if(tradeAccount!=null){

					//Edited by Thinzar, Fixes_Request-20151001, ReqNo. 3 - START
					derivativePortfolio = isDerivatives(tradeAccount.getExchangeCode());
					String strAccountNoLabel	= tradeAccount.getBranchCode()+" - "+tradeAccount.getAccountNo();

					if(!derivativePortfolio)	
						strAccountNoLabel +=  " ("+AppConstants.getStrBaseCurrencyCode() +" "+ FormatUtil.formatDouble(balance) + ")";

					accountNoLabel.setText(strAccountNoLabel);
					clientNameLabel.setText(tradeAccount.getClientName());
					//Edited by Thinzar, Fixes_Request-20151001, ReqNo. 3 - END

				}

				// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5

				if(derivativePortfolio){
					btnExchangeType.setVisibility(View.GONE);
					btnPaymentType.setVisibility(View.GONE);  	//added by Thinzar@20140804 - Change_Request-20140701, ReqNo. 9

					leftTextView.setText(VIRTUALPL);
					rightTextView.setText(SUMMARY);

					viewPagerDerivative.setVisibility(View.VISIBLE);
					viewPager.setVisibility(View.GONE);

					if(viewPagerDerivative.getAdapter()==null){
						viewPagerAdapterDerivative = new PagerAdapter(){	
							@Override
							public int getCount() {
								return 2;
							}

							@Override
							public Object instantiateItem(ViewGroup container, int position) {

								View inflatedView = null;

								if(position==0){
									inflatedView = ((LayoutInflater)PortfolioActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.derivative_virtual_pl, container, false);
									TableLayout tableLayoutKey = (TableLayout)inflatedView.findViewById(R.id.keyTable);
									TableRow keyRow = null;View keyView;TextView keyLabel;

									String[] virtualPLFields = getResources().getStringArray(R.array.virtualpl_field_list);
									for(int i=0; i<virtualPLFields.length ; i++){
										keyRow		= new TableRow(PortfolioActivity.this);
										keyRow.setLayoutParams(new TableLayout.LayoutParams( TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
										keyView 	= PortfolioActivity.this.getLayoutInflater().inflate(R.layout.order_list_item, null);
										keyLabel 	= (TextView) keyView.findViewById(R.id.orderItemLabel);
										keyLabel.setText(virtualPLFields[i]);
										keyRow.addView(keyView);
										tableLayoutKey.addView(keyRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
									}
								}else if(position==1){
									inflatedView = ((LayoutInflater)PortfolioActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.derivative_summary_table, container,false);
								}

								((ViewPager)container).addView(inflatedView);
								return inflatedView;
							}

							@Override
							public boolean isViewFromObject(View view, Object object) {
								return view == ((LinearLayout) object);
							}
						};
						viewPagerDerivative.setAdapter(viewPagerAdapterDerivative);

					}

					//moved this one outside if/else block by Thinzar@20141008, Fixes_Request-20140929, ReqNo.5
					if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
						new derivativeTradeRefresh().execute(NEW);

				}else{

					btnExchangeType.setVisibility(View.VISIBLE);

					leftTextView.setText(UNREALIZED);
					rightTextView.setText(REALIZED);

					viewPager.setVisibility(View.VISIBLE);
					viewPagerDerivative.setVisibility(View.GONE);

					if(viewPager.getAdapter()==null){

						viewPagerAdapter = new PagerAdapter(){	
							@Override
							public int getCount() {
								return 2;
							}

							@Override
							public Object instantiateItem(ViewGroup container, int position) {
								System.out.println("instantiateItem: " + position);
								View inflatedView = ((LayoutInflater)PortfolioActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.portfolio_table, container,
										false);

								if(position == ParamConstants.INT_PAGE_UNREALIZED){
									((TextView)inflatedView.findViewById(R.id.titleSecondColumn)).setText(MKT_VAL);
								}else{
									((TextView)inflatedView.findViewById(R.id.titleSecondColumn)).setText(GL);
								}
								((TextView)inflatedView.findViewById(R.id.titleThirdColumn)).setText(GLP);

								((ViewPager)container).addView(inflatedView);

								togglePortfolioTitleSetUp(position);

								return inflatedView;
							}

							@Override
							public boolean isViewFromObject(View view, Object object) {
								return view == ((LinearLayout) object);
							}
						};	
						viewPager.setAdapter(viewPagerAdapter);
					}

					if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
						new stockRefresh().execute(intPortfolioType);	

				}

				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	private boolean isDerivatives(String exchangeCode){
		for(int i=0; i<AppConstants.getListDerivativesExchgCode().size(); i++){
			if(exchangeCode.equalsIgnoreCase(AppConstants.getListDerivativesExchgCode().get(i))){
				return true;
			}
		}
		return false;
	}

	// Change_Request-20160101, ReqNo.9 = portfolio sorting
	private void togglePortfolioTitleSetUp(final int currentViewPagerId) {

		((LinearLayout) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.portfolioNameLayout))
		.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (currentViewPagerId == ParamConstants.INT_PAGE_UNREALIZED) {
					if (currentUnrealiseSortByMethod == SORT_BY_NAME_ASC) {
						currentUnrealiseSortByMethod = SORT_BY_NAME_DESC;
						/* TODO: uncomment this code block to display sorting arrow
						 * ((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByName)).setImageResource(android.R.drawable.arrow_down_float);
						 */
					} else {
						currentUnrealiseSortByMethod = SORT_BY_NAME_ASC;
						/* TODO: uncomment this code block to display sorting arrow
							((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByName)).setImageResource(android.R.drawable.arrow_up_float);
						 */
					}
					constructUnrealizeTableView(filteredStockHoldings);

				} else {
					if (currentRealiseSortByMethod == SORT_BY_NAME_ASC) {
						currentRealiseSortByMethod = SORT_BY_NAME_DESC;
						/* TODO: uncomment this code block to display sorting arrow
							((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByName)).setImageResource(android.R.drawable.arrow_down_float);
						 */
					} else {
						currentRealiseSortByMethod = SORT_BY_NAME_ASC;
						/* TODO: uncomment this code block to display sorting arrow
							((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByName)).setImageResource(android.R.drawable.arrow_up_float);
						 */
					}
					constructRealizeTableView(filteredStockHoldings);

				}

				((ImageView) viewPager.getChildAt(currentViewPagerId)
						.findViewById(R.id.imgSortPortfolioByName)).setVisibility(View.VISIBLE);
				((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByGL))
				.setVisibility(View.INVISIBLE);
			}
		});

		((LinearLayout) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.portfolioGLLayout))
		.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (currentViewPagerId == ParamConstants.INT_PAGE_UNREALIZED) {
					if (currentUnrealiseSortByMethod == SORT_BY_GL_ASC) {
						currentUnrealiseSortByMethod = SORT_BY_GL_DESC;
						/* TODO: uncomment this code block to display sorting arrow
							((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByGL)).setImageResource(android.R.drawable.arrow_down_float);
						 */
					} else {
						currentUnrealiseSortByMethod = SORT_BY_GL_ASC;
						/* TODO: uncomment this code block to display sorting arrow
							((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByGL)).setImageResource(android.R.drawable.arrow_up_float);
						 */
					}
					constructUnrealizeTableView(filteredStockHoldings);

				} else {
					if (currentRealiseSortByMethod == SORT_BY_GL_ASC) {
						currentRealiseSortByMethod = SORT_BY_GL_DESC;
						/* TODO: uncomment this code block to display sorting arrow
							((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByGL)).setImageResource(android.R.drawable.arrow_down_float);
						 */
					} else {
						currentRealiseSortByMethod = SORT_BY_GL_ASC;
						/* TODO: uncomment this code block to display sorting arrow
							((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByGL)).setImageResource(android.R.drawable.arrow_up_float);
						 */
					}
					constructRealizeTableView(filteredStockHoldings);

				}

				((ImageView) viewPager.getChildAt(currentViewPagerId).findViewById(R.id.imgSortPortfolioByGL))
				.setVisibility(View.VISIBLE);
				((ImageView) viewPager.getChildAt(currentViewPagerId)
						.findViewById(R.id.imgSortPortfolioByName)).setVisibility(View.INVISIBLE);
			}
		});
	}
	// Change_Request-20160101, ReqNo.9 = portfolio sorting - END

	private void unrealiseSelected(){
		System.out.println("unrealiseSelected");

		intPortfolioType				= ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED;

		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();

		setSelectedPortfolioHeader(leftTextView);

		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
		if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
			//commented by Thinzar@20140807, Change_Request-20140701, ReqNo.10 //if(((TableLayout)viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED).findViewById(R.id.portfolioTable)).getChildCount()==0)
			if(!isDifferentClient)	//Added by Thinzar, Fixes_Request-20140820, ReqNo.33	
				new stockRefresh().execute(intPortfolioType);	
	}

	private void realiseSelected(){
		System.out.println("realiseSelected");

		intPortfolioType				= ParamConstants.INT_PORTFOLIOTYPE_REALIZED;

		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

		setSelectedPortfolioHeader(rightTextView);

		intPortfolioType				= ParamConstants.INT_PORTFOLIOTYPE_REALIZED;

		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
		if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
			//commented by Thinzar@20140807, Change_Request-20140701, ReqNo.10 //if(((TableLayout)viewPager.getChildAt(ParamConstants.INT_PAGE_REALIZED).findViewById(R.id.portfolioTable)).getChildCount()==0)
			new stockRefresh().execute(intPortfolioType);
	}

	private void virtualPLSelected(){
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

		setSelectedPortfolioHeader(leftTextView);
	}

	private void summarySelected(){
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

		setSelectedPortfolioHeader(rightTextView);
	}

	private class derivativeTradeRefresh extends AsyncTask<Integer,Void, Derivative>{
		Exception exception	= null; 
		int state;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try{
				PortfolioActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				if( ! PortfolioActivity.this.isFinishing() )
					PortfolioActivity.this.dialog.show();
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		@Override
		protected Derivative doInBackground(Integer... params) {			
			state = params[0]; 
			Map<String, Object> parameters = new HashMap<String, Object>();
			dervTradePrtfSubDetailRptMap = new HashMap<String, Integer>();

			if(tradeAccount!=null){

				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);

				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
				parameters.put(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
				parameters.put(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
				parameters.put(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
				parameters.put(ParamConstants.EXCHANGECODE_TAG, tradeAccount.getExchangeCode());

				try {
					String response = null; derivative = new Derivative();

					try{
						response = AtpConnectUtil.tradePortfolioSubDetailRpt(parameters);
						while(intRetryCtr < AppConstants.intMaxRetry && response == null){
							intRetryCtr++;
							response 	= AtpConnectUtil.tradePortfolioSubDetailRpt(parameters);
						}
						intRetryCtr	= 0;

						if(response == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_PORTFOLIO);
					}catch(Exception e){
						throw e;
					}
					dervTradePrtfSubDetailRptList = AtpMessageParser.parseTradePortfolioSubDetailRpt(response);
					derivative.setDerivativeTradePortfolioSubDetailRptList(dervTradePrtfSubDetailRptList);

					response = null;
					symbolCodes= new String[dervTradePrtfSubDetailRptList.size()];
					for(int i=0; i<dervTradePrtfSubDetailRptList.size(); i++){
						symbolCodes[i] = dervTradePrtfSubDetailRptList.get(i).getStockCode(); 
						//this is to keep track location of stock in dervTradePrtfSubDetailRptList (based on symbolCode)
						dervTradePrtfSubDetailRptMap.put(dervTradePrtfSubDetailRptList.get(i).getStockCode(), i);
					}

					try {

						//if refresh thread is running, temporarily paused it
						if(refreshThread!=null){
							if(refreshThread.isAlive()) refreshThread.pauseRequest();
						}

						response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_REFRESH);
						while(intRetryCtr < AppConstants.intMaxRetry && response == null){
							intRetryCtr++;
							response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_REFRESH);
						}
						intRetryCtr	= 0;

						if(response == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);

					}catch(Exception e){
						throw e;
					}

					//edited by Thinzar@20140717 - Change_Request-20140701, Req #5 - START
					if(response.length() > 0){
						refreshStocks = QcMessageParser.parseStockRefresh(response);

						refreshDervTradePrtfSubDetailRptList(refreshStocks);
					}
					//edited by Thinzar@20140717 - END

					response = null;
					try{
						response = AtpConnectUtil.tradePortfolioSummaryRpt(parameters);
						while(intRetryCtr < AppConstants.intMaxRetry && response == null){
							intRetryCtr++;
							response 	= AtpConnectUtil.tradePortfolioSummaryRpt(parameters);
						}
						intRetryCtr	= 0;

						if(response == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_PORTFOLIO);

					}catch(Exception e){
						throw e;
					}
					dervTradePrtfSummRpt = AtpMessageParser.parseTradePortfolioSummaryRpt(response);

					derivative.setDerivativeTradePortfolioSummaryRpt(dervTradePrtfSummRpt);
					derivative.calculate();

					return derivative;

				} catch (FailedAuthenicationException e) {
					isExpired = true;
					if(LoginActivity.QCAlive != null)
						LoginActivity.QCAlive.stopRequest();
					if(LoginActivity.refreshThread != null)
						LoginActivity.refreshThread.stopRequest();
					exception = e;
				} catch (Exception ex) {
					exception = ex;
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Derivative derivative) {
			super.onPostExecute(derivative);

			if(derivative!=null){
				derivative.setState(state);
				setVirtualPLValues(derivative);
				constructDerivativeSummaryTableView(derivative);
			}

			refreshThread = ((StockApplication) PortfolioActivity.this.getApplication()).getRefreshThread();
			if (refreshThread == null) {
				if(AppConstants.hasLogout){
					finish();
				}else{
					int rate		= sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
					refreshThread 	= new RefreshStockThread(AppConstants.QCdata.getStrUserParam(), symbolCodes, rate);
					refreshThread.start();
					((StockApplication) PortfolioActivity.this.getApplication()).setRefreshThread(refreshThread);
				}
			}
			refreshThread.setRegisteredListener(PortfolioActivity.this);
			refreshThread.setSymbolCodes(symbolCodes);

			if( refreshThread.isPaused() ){
				refreshThread.resumeRequest();
			}

			if(!PortfolioActivity.this.isFinishing()
					&& dialog != null && dialog.isShowing() ) //fixes for crashlytics
				dialog.dismiss();
		}
	}

	private void refreshDervTradePrtfSubDetailRptList(List<StockSymbol> refreshStocks){
		for(int i=0; i<refreshStocks.size(); i++){

			//setRefPrice
			dervTradePrtfSubDetailRptList.get(dervTradePrtfSubDetailRptMap.get(refreshStocks.get(i).getSymbolCode())).setRefPrice(refreshStocks.get(i).getLACP());
			//setLastDone 
			dervTradePrtfSubDetailRptList.get(dervTradePrtfSubDetailRptMap.get(refreshStocks.get(i).getSymbolCode())).setLastDone(refreshStocks.get(i).getLastDone());
			//setDerivativeStockSymbol
			dervTradePrtfSubDetailRptList.get(dervTradePrtfSubDetailRptMap.get(refreshStocks.get(i).getSymbolCode())).setDerivativeStockType(refreshStocks.get(i).getDerivativeStockType());

		}
		derivative.setDerivativeTradePortfolioSubDetailRptList(dervTradePrtfSubDetailRptList);	
	}

	private void setVirtualPLValues(Derivative derivative){
		TableLayout tableLayoutValue = (TableLayout)viewPagerDerivative.getChildAt(ParamConstants.INT_PAGE_VIRTUALPL).findViewById(R.id.valueTable);
		if(derivative.getState() == NEW) tableLayoutValue.removeAllViews();			//edited by Thinzar@20141008, Fixes_Request-20140929, ReqNo.5

		//set B/f Cash Balance
		setVirtualPLRows(derivative.getState(), CASH_BALANCE_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getCashBalance());

		//set Deposit
		setVirtualPLRows(derivative.getState(), DEPOSIT_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getDeposit());	//Edited by Thinzar, Fixes_Request-20151001, ReqNo.3

		//set Withdrawal
		setVirtualPLRows(derivative.getState(), WITHDRAWAL_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getWithdrawal());	//Edited by Thinzar, Fixes_Request-20151001, ReqNo.3

		//set Realised G/L
		setVirtualPLRows(derivative.getState(), REALIZED_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getRealisedPL());

		//set Current Balance
		setVirtualPLRows(derivative.getState(), CURRENT_BALANCE_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getCurrentBalance());

		//set Unrealised G/L
		setVirtualPLRows(derivative.getState(), UNREALIZED_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getUnrealisedPL());

		//set Equity
		setVirtualPLRows(derivative.getState(), EQUITY_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getEquity());

		//set Buy Option Mkt Val
		setVirtualPLRows(derivative.getState(), BUY_OPTIONMKTVAL_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getBuyOptionMktVal());

		//set Sell Option Mkt Val
		setVirtualPLRows(derivative.getState(), SELL_OPTIONMKTVAL_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getSellOptionMktVal());

		//set Nett Liquidation
		setVirtualPLRows(derivative.getState(), NETT_LIQUIDATION_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getNetLiquidation());

		//set Eligible Collateral
		setVirtualPLRows(derivative.getState(), ELIGIBLE_COLLATERAL_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getEligibleCollateral());

		//set Initial Margin
		setVirtualPLRows(derivative.getState(), INITIAL_MARGIN_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getInitialMargin());

		//set Maintenance Margin
		setVirtualPLRows(derivative.getState(), MAINTENANCE_MARGIN_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getMaintenanceMargin());

		//set Excess/Shortfall
		setVirtualPLRows(derivative.getState(), EXCESS_SHORTFALL_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getExcessOrShortfall());

		//set Margin Call
		setVirtualPLRows(derivative.getState(), MARGIN_CALL_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getMarginCall());

		//set Eligibility
		setVirtualPLRows(derivative.getState(), ELIGIBILITY_TAG, tableLayoutValue,derivative.getDerivativeTradePortfolioSummaryRpt().getEligibility());
	}

	private void setVirtualPLRows(int state, int tag, TableLayout tableLayout, float value){
		if (state == NEW){
			TableRow valueRow = null ; View valueView; TextView valueLabel;

			valueRow	= new TableRow(PortfolioActivity.this);
			valueRow.setLayoutParams(new TableLayout.LayoutParams( TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
			valueView 	= PortfolioActivity.this.getLayoutInflater().inflate(R.layout.order_list_item, null);
			valueLabel 	= (TextView) valueView.findViewById(R.id.orderItemLabel);
			valueLabel.setTag(tag);
			valueLabel.setText(FormatUtil.formatFloatToString(value));
			valueLabel.setGravity(Gravity.RIGHT);
			valueRow.addView(valueView);
			tableLayout.addView(valueRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
		}else if (state == UPDATE_FR_ATP){
			// all fields are updated
			((TextView)tableLayout.findViewWithTag(tag)).setText(FormatUtil.formatFloatToString(value));
		}
		else if (state == UPDATE_FR_FEED){
			// only these fields are updated
			if(tag == CURRENT_BALANCE_TAG || tag == UNREALIZED_TAG || tag == EQUITY_TAG || tag == NETT_LIQUIDATION_TAG || tag == EXCESS_SHORTFALL_TAG || tag == ELIGIBILITY_TAG){
				((TextView)tableLayout.findViewWithTag(tag)).setText(FormatUtil.formatFloatToString(value));
			}
		}

	}

	private void constructDerivativeSummaryTableView(Derivative derivative){

		boolean isSelfInitiate	= false;
		final ArrayList<DervTradePrtfSubDetailRpt>  dervTradeprtfSubDetailRptList = derivative.getDerivativeTradePortfolioSubDetailRptList();

		try {
			if( dialog != null && ! dialog.isShowing() ){
				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));

				if( ! PortfolioActivity.this.isFinishing() )
					dialog.show();

				isSelfInitiate	= true;
			}

			this.tableLayout = (TableLayout)viewPagerDerivative.getChildAt(ParamConstants.INT_PAGE_SUMMARY).findViewById(R.id.derivativeSummaryTable);

			if(derivative.getState() == NEW){
				this.tableLayout.removeAllViews();

				((TextView)viewPagerDerivative.getChildAt(ParamConstants.INT_PAGE_SUMMARY).findViewById(R.id.titleThirdColumn)).setText(LAST);

				for(int i=0;i<dervTradeprtfSubDetailRptList.size();i++){

					final DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt = derivative.getDerivativeTradePortfolioSubDetailRptList().get(i);

					final TableRow tableRow		= new TableRow(PortfolioActivity.this);
					tableRow.setTag(i);
					tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
					View rowView 				= getLayoutInflater().inflate(R.layout.derivative_summary_row, null); 

					final LinearLayout rowLayout= (LinearLayout) rowView.findViewById(R.id.derivativeSummaryRowLayout);
					//rowLayout.setBackgroundResource( ( (i+1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);	// Commented, Change_Request-20160101, ReqNo.3

					TextView labelSymbol	= (TextView) rowView.findViewById(R.id.labelSymbol);
					labelSymbol.setText( dervTradePrtfSubDetailRpt.getStockName());

					final TextView labelNettPosition	= (TextView) rowView.findViewById(R.id.labelNettPosition);
					labelNettPosition.setText(String.valueOf( dervTradePrtfSubDetailRpt.getNettPosition()));	//edited by Thinzar, changed from float to int, Fixes_Request-20140929, ReqNo.2
					labelNettPosition.setTag(i);
					labelNettPosition.setOnClickListener(new OnClickListener(){
						public void onClick(View view) {
							Intent intent = new Intent();
							AppConstants.dervTradePrtfSubDetailRpt = dervTradeprtfSubDetailRptList.get((Integer)view.getTag());
							intent.setClass(PortfolioActivity.this, DerivativeDetailActivity.class);
							startActivityForResult(intent,AppConstants.DERIVATIVE_DETAIL_REQUEST);
						}
					});

					Button buttonLastDone = (Button) rowView.findViewById(R.id.buttonLastDone);
					buttonLastDone.setText(FormatUtil.formatFloatToString(  dervTradePrtfSubDetailRpt.getLastDone()));
					buttonLastDone.setOnClickListener(new OnClickListener(){
						public void onClick(View view) {
							changeBetAvgPriceNLastDone(dervTradeprtfSubDetailRptList);
						}
					});

					final View rowExpandView = getLayoutInflater().inflate(R.layout.derivative_summary_expand_row, null);
					rowExpandView.setOnClickListener(new OnClickListener() {
						public void onClick(View view) {
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
							if(idleThread != null && ! isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

							labelNettPosition.performClick();
						}
					});

					populateDerivativeSummaryExpandViewData(derivative.getState(), rowExpandView,  dervTradePrtfSubDetailRpt);

					tableRow.setOnClickListener(new OnClickListener() {
						public void onClick(View view) {
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
							if(idleThread != null && ! isShowingIdleAlert)
								idleThread.resetExpiredTime();
							// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

							if( !dervTradePrtfSubDetailRpt.isExpand() ){
								if(rowLayout.getChildCount() == 1)
									rowLayout.addView(rowExpandView);

								dervTradePrtfSubDetailRpt.setExpand(true);
							}else{
								if(rowLayout.getChildCount() != 1)
									rowLayout.removeViewAt(1);							

								dervTradePrtfSubDetailRpt.setExpand(false);
							}
						}
					});

					tableRow.addView(rowView);
					this.tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
				}
			}else if(derivative.getState() == UPDATE_FR_ATP){
				//update all fields
				for(int i=0;i<dervTradeprtfSubDetailRptList.size();i++){
					DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt = derivative.getDerivativeTradePortfolioSubDetailRptList().get(i);

					((TextView)tableLayout.findViewWithTag(i).findViewById(R.id.labelSymbol)).setText(dervTradePrtfSubDetailRpt.getStockName());
					((TextView)tableLayout.findViewWithTag(i).findViewById(R.id.labelNettPosition)).setText(FormatUtil.formatFloatToString( dervTradePrtfSubDetailRpt.getNettPosition()));
					if(((String)((TextView)viewPagerDerivative.getChildAt(ParamConstants.INT_PAGE_SUMMARY).findViewById(R.id.titleThirdColumn)).getText()).equalsIgnoreCase(LAST)){
						((TextView)tableLayout.findViewWithTag(i).findViewById(R.id.buttonLastDone)).setText(FormatUtil.formatFloatToString(dervTradeprtfSubDetailRptList.get(i).getLastDone()));
					}else{
						((TextView)tableLayout.findViewWithTag(i).findViewById(R.id.buttonLastDone)).setText(FormatUtil.formatFloatToString(dervTradeprtfSubDetailRptList.get(i).getAveragePrice()));
					}

					//update expanded row if it is expanded
					if( dervTradePrtfSubDetailRpt.isExpand() ){
						LinearLayout rowLayout = (LinearLayout)tableLayout.findViewWithTag(i).findViewById(R.id.derivativeSummaryRowLayout);
						populateDerivativeSummaryExpandViewData(derivative.getState(), rowLayout,  dervTradePrtfSubDetailRpt);
					}

				}
			}else if(derivative.getState() == UPDATE_FR_FEED){
				//update only these fields
				for(int i=0;i<dervTradeprtfSubDetailRptList.size();i++){
					DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt = derivative.getDerivativeTradePortfolioSubDetailRptList().get(i);

					if(((String)((TextView)viewPagerDerivative.getChildAt(ParamConstants.INT_PAGE_SUMMARY).findViewById(R.id.titleThirdColumn)).getText()).equalsIgnoreCase(LAST)){
						((TextView)tableLayout.findViewWithTag(i).findViewById(R.id.buttonLastDone)).setText(FormatUtil.formatFloatToString(dervTradeprtfSubDetailRptList.get(i).getLastDone()));
					}

					//update expanded row if it is expanded
					if( dervTradePrtfSubDetailRpt.isExpand() ){
						LinearLayout rowLayout = (LinearLayout)tableLayout.findViewWithTag(i).findViewById(R.id.derivativeSummaryRowLayout);
						populateDerivativeSummaryExpandViewData(derivative.getState(), rowLayout,  dervTradePrtfSubDetailRpt);
					}
				}
			}



		} catch (Exception ex) {
			ex.printStackTrace();
		}

		try{
			if( isSelfInitiate && dialog != null && dialog.isShowing() ){
				dialog.dismiss();
				isSelfInitiate	= false;
			}
		}catch(Exception e){
			e.printStackTrace();
			isSelfInitiate	= false;
		}
		isConstructing=false;
	}


	private void populateDerivativeSummaryExpandViewData(int state, View expandView, DervTradePrtfSubDetailRpt dervTradePrtfSubDetailRpt){
		TextView valueUnrealisedPL, valueRealisedPL, valueGrossBuy, valueGrossSell, valueBfBuy, valueBfSell, valueDayBuy, valueDaySell;
		TextView valueCurr, valueCurrRate;

		if(state == NEW || state == UPDATE_FR_ATP){

			valueUnrealisedPL = (TextView) expandView.findViewById(R.id.valueUnrealisedPL);
			valueUnrealisedPL.setText(FormatUtil.formatFloatToString(dervTradePrtfSubDetailRpt.getUnrealisedPL()));

			valueRealisedPL = (TextView) expandView.findViewById(R.id.valueRealisedPL);
			valueRealisedPL.setText(FormatUtil.formatFloatToString(dervTradePrtfSubDetailRpt.getRealisedPL()));

			valueGrossBuy = (TextView) expandView.findViewById(R.id.valueGrossBuy);
			valueGrossBuy.setText(String.valueOf(dervTradePrtfSubDetailRpt.getGrossBuy()));			//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17

			valueGrossSell = (TextView) expandView.findViewById(R.id.valueGrossSell);
			valueGrossSell.setText(String.valueOf(dervTradePrtfSubDetailRpt.getGrossSell()));		//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17

			valueBfBuy = (TextView) expandView.findViewById(R.id.valueBfBuy);
			valueBfBuy.setText(String.valueOf(dervTradePrtfSubDetailRpt.getBfBuy()));		//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17

			valueBfSell = (TextView) expandView.findViewById(R.id.valueBfSell);
			valueBfSell.setText(String.valueOf(dervTradePrtfSubDetailRpt.getBfSell()));		//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17

			valueDayBuy = (TextView) expandView.findViewById(R.id.valueDayBuy);
			valueDayBuy.setText(String.valueOf(dervTradePrtfSubDetailRpt.getDayBuy()));		//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17

			valueDaySell = (TextView) expandView.findViewById(R.id.valueDaySell);
			valueDaySell.setText(String.valueOf(dervTradePrtfSubDetailRpt.getDaySell()));	//edited by Thinzar, changed from Float to Integer, Fixes_Request-20140820,ReqNo.17

			valueCurr = (TextView) expandView.findViewById(R.id.valueCurr);
			valueCurr.setText(dervTradePrtfSubDetailRpt.getCurrencySymbol());

			valueCurrRate = (TextView) expandView.findViewById(R.id.valueCurrRate);
			valueCurrRate.setText(FormatUtil.formatFloatToString(dervTradePrtfSubDetailRpt.getForexExchangeRate()));

		}else if(state == UPDATE_FR_FEED){
			valueUnrealisedPL = (TextView) expandView.findViewById(R.id.valueUnrealisedPL);
			valueUnrealisedPL.setText(FormatUtil.formatFloatToString(dervTradePrtfSubDetailRpt.getUnrealisedPL()));
		}

	}

	private class stockRefresh extends AsyncTask<Object, Void, List<StockHolding>> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null; 
		int stockRefreshPortfolioType;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				PortfolioActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! PortfolioActivity.this.isFinishing() )
					PortfolioActivity.this.dialog.show();
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected List<StockHolding> doInBackground(Object... params) {
			stockRefreshPortfolioType = (Integer)params[0]; 

			Map<String, Object> parameters = new HashMap<String, Object>();
			if(tradeAccount!=null){

				if( AppConstants.brokerConfigBean.isEnableEncryption() )
					parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
				else
					parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
				// Mary@20121004 - Fixes_Request-20120815, ReqNo.3 - END
				parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
				parameters.put(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
				parameters.put(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
				parameters.put(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
				// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27
				parameters.put(ParamConstants.PARAM_PORTFOLIOTYPE_TAG, intPortfolioType);


				try {

					String response = null;
					try{
						response = AtpConnectUtil.tradePortfolio(parameters);
						while(intRetryCtr < AppConstants.intMaxRetry && response == null){
							intRetryCtr++;
							response 	= AtpConnectUtil.tradePortfolio(parameters);
						}
						intRetryCtr	= 0;

						if(response == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_PORTFOLIO);
					}catch(Exception e){
						throw e;
					}
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					stockHoldings 		= AtpMessageParser.parseTradePortfolio(response);

					//Change_Request-20170601, ReqNo.17
					/* Commented and replaced with following line
					String[] symbolCodes= new String[stockHoldings.size()];

					int index 			= 0;
					for(Iterator<StockHolding> itr=stockHoldings.iterator(); itr.hasNext();){
						StockHolding counter 	= itr.next();
						symbolCodes[index] 		= counter.getSymbolCode(); 

						index++;
					}*/
                    String[] symbolCodes = mapSymbolCodesToSubscribedExchange(stockHoldings);

                    //if refresh thread is running, temporarily paused it
					if(refreshThread!=null){
						if(refreshThread.isAlive()) refreshThread.pauseRequest();
					}

					response = null;
					try {
						response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_REFRESH);
						while(intRetryCtr < AppConstants.intMaxRetry && response == null){
							intRetryCtr++;
							response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_REFRESH);
						}
						intRetryCtr	= 0;

						if(response == null)				
							throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);
						// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

					}catch(Exception e){
						throw e;
					}
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

					if(response!=null){
						refreshStocks = QcMessageParser.parseStockRefresh(response);
					}

					// Added by Mary@20120919 - Change_Request-20120719, ReqNo.12 - START
					for( Iterator<StockHolding> itr = stockHoldings.iterator(); itr.hasNext(); ){
						StockHolding counter	= itr.next();
						String strTemp			= counter.getSymbolCode();
						for( Iterator<StockSymbol> itr2 = refreshStocks.iterator(); itr2.hasNext(); ){
							StockSymbol stock	= itr2.next();
							if( strTemp.equals( stock.getSymbolCode() ) || strTemp.equals(FormatUtil.parseSymbolCodeToRealTimeFeed(stock.getSymbolCode())) ){
								counter.setIntLotSize( stock.getIntLotSize() );
								counter.setLastDone(stock.getLastDone());
								counter.setLacp(stock.getLACP());
								break;
							}
						}
					}
					// Added by Mary@20120919 - Change_Request-20120719, ReqNo.12 - END

					return stockHoldings;

				} catch (FailedAuthenicationException e) {
					isExpired = true;
					if(LoginActivity.QCAlive != null)
						LoginActivity.QCAlive.stopRequest();
					if(LoginActivity.refreshThread != null)
						LoginActivity.refreshThread.stopRequest();
					exception = e;
				} catch (Exception ex) {
					exception = ex;
				}
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			}

			return null;
		}

		@Override
		protected void onPostExecute(List<StockHolding> stockHoldings) {
			super.onPostExecute(stockHoldings);
			isDifferentClient 			= false;	//Added by Thinzar, Fixes_Request-20140820, ReqNo.33

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( dialog != null && dialog.isShowing() )
					dialog.dismiss();

				if(isExpired){
					PortfolioActivity.this.sessionExpired();
					return;
				}

				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							finish();
						}
					});

					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! PortfolioActivity.this.isFinishing() )
						alertException.show();

					exception	= null;
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

				//commented out and Added by Thinzar@20140806, Change_Request-20140701, ReqNo. 9
				//filteredStockHoldings	= getFilteredStockHoldingList(stockHoldings, strSelectedFilterExchangeType, intPortfolioType);
				filteredStockHoldings	= getFilteredStockHoldingList2(stockHoldings, strSelectedFilterExchangeType, intPortfolioType, strSelectedFilterPayType);

				expandid	=  new boolean[stockHoldings.size()];

				loginThread = ((StockApplication)PortfolioActivity.this.getApplication()).getDoubleLoginThread();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
				if(loginThread == null){
					if(AppConstants.hasLogout){
						finish();
					}else{
						loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
						if (AppConstants.brokerConfigBean.isEnableEncryption())
							loginThread.setUserParam(AppConstants.userParamInByte);
						loginThread.start();
						((StockApplication) PortfolioActivity.this.getApplication()).setDoubleLoginThread(loginThread);
					}
				}
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
				loginThread.setRegisteredListener(PortfolioActivity.this);

				if (stockHoldings != null) {
					//Kw@20121024 Fix Request-20121023, ReqNo 8
					//@20121024 FixReq20121023, ReqNo 9 -START
					isConstructing=true;
					//@20121024 FixReq20121023, ReqNo 9 -START
					//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - START

					if(stockRefreshPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED){

						// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
						if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
							PortfolioActivity.this.constructUnrealizeTableView(filteredStockHoldings);

					}else{

						// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
						if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
							PortfolioActivity.this.constructRealizeTableView(filteredStockHoldings);
					}
					//Added by Kw@20120817 - Change Request-20120719, ReqNo.9 - END
				}

				if( dialog != null && dialog.isShowing() )
					dialog.dismiss();
				// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	//Change_Request-20170601, ReqNo. 17
	private String[] mapSymbolCodesToSubscribedExchange(List<StockHolding> lstStockHoldings){
		String[] mappedSymbolCodes = new String[lstStockHoldings.size()];

		int index 	= 0;
		for(Iterator<StockHolding> itr=stockHoldings.iterator(); itr.hasNext();){
			StockHolding counter 	= itr.next();
			String mSymbolCode 		= counter.getSymbolCode();

			mappedSymbolCodes[index++] 	= SystemUtil.convertSymbolMapSubscribeExchg(mSymbolCode);
		}

		return mappedSymbolCodes;
	}

	private class ConstructTableUnrealiseNew extends AsyncTask<Void, Void, Void>{
		private double calculatedMarketValue 	= 0;
		private double calculatedUnrealizeGL	= 0;
		private float calculatedPercentPL		= 0;

		@Override
		protected void onPreExecute() {

			if(dialog != null && !dialog.isShowing()){
				dialog.setMessage(getResources().getString(R.string.dialog_progress_sorting));

				if(!PortfolioActivity.this.isFinishing()){
					dialog.show();
				}
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			//calculate marketValue, 
			for(int pos=0;pos<filteredStockHoldings.size();pos++){
				StockHolding counter	= filteredStockHoldings.get(pos);

				for (Iterator<StockSymbol> itr2=refreshStocks.iterator(); itr2.hasNext(); ){
					StockSymbol stock = itr2.next();

					//Fixes_Request-20170701, ReqNo.19
					String symbolCodeRT = FormatUtil.parseSymbolCodeToRealTimeFeed(stock.getSymbolCode());
					//if(counter.getSymbolCode().equals(stock.getSymbolCode())){
					if(counter.getSymbolCode().equals(symbolCodeRT)){
						//1. Calculate marketValue
						if(counter.getQuantityOnHand() == 0 || (stock.getLastDone() == 0 && stock.getLACP() == 0) )
							calculatedMarketValue = 0;
						else
							calculatedMarketValue = ( stock.getLastDone() == 0 ? stock.getLACP() : stock.getLastDone() ) * counter.getQuantityOnHand();

						//2. CalculatePercentPL
						if(counter.getAvgPurchasePrice()==0 || counter.getQuantityOnHand() == 0 || (stock.getLastDone() == 0 && stock.getLACP() == 0 && counter.getAvgPurchasePrice() == 0) ){
							calculatedPercentPL = 0;
						}else{
							float fltActualPL	= ( (stock.getLastDone() == 0 ? stock.getLACP() : stock.getLastDone() ) - counter.getAvgPurchasePrice() ) * counter.getQuantityOnHand();
							float fltExpectedPL	= counter.getAvgPurchasePrice() * counter.getQuantityOnHand();
							calculatedPercentPL		= fltActualPL / fltExpectedPL;
							fltActualPL			= 0;
							fltExpectedPL		= 0;
						}

						//3. calculateUnrealizeGL
						if(counter.getQuantityOnHand() == 0 || (stock.getLastDone() == 0 && stock.getLACP() == 0 && counter.getAvgPurchasePrice() == 0) ){
							calculatedUnrealizeGL = 0;
						}else{
							calculatedUnrealizeGL = ( (stock.getLastDone() == 0 ? stock.getLACP() : stock.getLastDone() ) - counter.getAvgPurchasePrice() ) * counter.getQuantityOnHand() ;
						}

						//Added by Thinzar, for Unrealize Portfolio sorting, update the calculated values back to the global object 
						filteredStockHoldings.get(pos).setCalculatedMarketValue(calculatedMarketValue);
						filteredStockHoldings.get(pos).setCalculatedPercentPL(calculatedPercentPL);
						filteredStockHoldings.get(pos).setCalculatedUnrealizeGL(calculatedUnrealizeGL);

						break;
					}
				}
			}

			List<StockHolding> sortedStockHoldings	= SortOrders(filteredStockHoldings, currentUnrealiseSortByMethod, ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED);
			filteredStockHoldings	= sortedStockHoldings;

			runOnUiThread(new Runnable(){

				@Override
				public void run() {
					buildTableUnrealise();
				}
			});

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			if( dialog != null && dialog.isShowing() )
				dialog.dismiss();
		}
	}

	// Added by Thinzar, Change_Request-20160101, ReqNo.9 - START
	private List<StockHolding> SortOrders(List<StockHolding> stockHoldings, final int sortByMethod, final int currentPortfolioType){

		switch (sortByMethod) {

		case SORT_BY_NAME_ASC:
			Collections.sort(stockHoldings, new StockHolding.orderByStockCodeASC());
			break;

		case SORT_BY_NAME_DESC:
			Collections.sort(stockHoldings, new StockHolding.orderByStockCodeDESC());
			break;

		case SORT_BY_GL_ASC:
			if(currentPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_REALIZED){
				Collections.sort(stockHoldings, new StockHolding.orderByRealiseGainLossPercentASC());
			} else{
				if(isDisplayPercentage){
					isSortByUnrealiseGLPercent	= true;
					Collections.sort(stockHoldings, new StockHolding.orderByCalculatedPercentPLASC());
				} else {
					isSortByUnrealiseGLPercent	= false;
					Collections.sort(stockHoldings, new StockHolding.orderByCalculatedUnrealizeGLASC());
				}
			}
			break;

		case SORT_BY_GL_DESC:
			if(currentPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_REALIZED){
				Collections.sort(stockHoldings, new StockHolding.orderByRealiseGainLossPercentDESC());
			} else {
				if(isDisplayPercentage){
					isSortByUnrealiseGLPercent	= true;
					Collections.sort(stockHoldings, new StockHolding.orderByCalculatedPercentPLDESC());
				} else {
					isSortByUnrealiseGLPercent	= false;
					Collections.sort(stockHoldings, new StockHolding.orderByCalculatedUnrealizeGLDESC());
				}
			}
			break;
		}

		return stockHoldings;
	}

	private void buildTableUnrealise(){

		for(int id=0;id<filteredStockHoldings.size();id++){
			mCounter	= filteredStockHoldings.get(id);

			final TableRow tableRow		= new TableRow(PortfolioActivity.this);   
			tableRow.setId(id);
			tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
			tableRow.setWeightSum(1);

			View rowView 				= getLayoutInflater().inflate(R.layout.order_row, null);  
			final LinearLayout rowLayout= (LinearLayout) rowView.findViewById(R.id.rowLayout);

			TextView nameLabel 			= (TextView) rowView.findViewById(R.id.nameLabel);
			nameLabel.setText(mCounter.getStockCode());

			final TextView priceLabel 	= (TextView) rowView.findViewById(R.id.priceLabel);

			//String strSymbolExchg		= mCounter.getSymbolCode().substring( (mCounter.getSymbolCode().lastIndexOf(".") + 1), mCounter.getSymbolCode().length() );
			priceLabel.setText(FormatUtil.formatDouble(mCounter.getCalculatedMarketValue(), "-"));

			priceLabel.setOnClickListener(new OnClickListener(){
				public void onClick(View view){
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					selectedRow			= rowLayout;
					selectedId 			= tableRow.getId();
					int id 				= tableRow.getId();
					StockHolding selectedCounter	= filteredStockHoldings.get(tableRow.getId());

					details 			= new HashMap<String, String>();
					sequences 			= new ArrayList<String>();

					if(intPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED)
						PortfolioActivity.this.populateCounterDetail(selectedCounter,id);
					else
						PortfolioActivity.this.populateCounterDetail2(selectedCounter,id);

					Intent intent = new Intent();
					intent.putExtra(ParamConstants.SYMBOLCODE_TAG, selectedCounter.getSymbolCode());
					intent.putExtra(ParamConstants.STOCK_CODE_TAG, selectedCounter.getStockCode());
					intent.putExtra(ParamConstants.ORDER_DETAILS_TAG, details);
					intent.putExtra(ParamConstants.ORDER_DISPLAYSEQ_TAG, sequences);
					intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.PORTFOLIO_DETAIL_TRADE);
					intent.putExtra( ParamConstants.LOT_SIZE_TAG, String.valueOf( selectedCounter.getIntLotSize()));
					intent.setClass(PortfolioActivity.this, OrderDetailActivity.class);
					startActivity(intent);
				}
			});

			final Button quantityButton = (Button) rowView.findViewById(R.id.quantityButton);  

			if (isDisplayPercentage) {
				((TextView)viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED).findViewById(R.id.titleThirdColumn)).setText(GLP);

				quantityButton.setText(FormatUtil.formatPricePercent(mCounter.getCalculatedPercentPL()));								
			} else {
				((TextView)viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED).findViewById(R.id.titleThirdColumn)).setText(GL);

				quantityButton.setText(FormatUtil.formatDouble(mCounter.getCalculatedUnrealizeGL(), null));
			}

			if(quantityButton.getText().toString().indexOf("-")!=-1)
				quantityButton.setBackgroundResource(LayoutSettings.btnRed);
			else
				quantityButton.setBackgroundResource(LayoutSettings.btnGreen);

			quantityButton.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {

					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();

					changeButtonValue();
				}
			});

			quantityButtons.add(quantityButton);

			tableRow.addView(rowView);

			//rowLayout.setBackgroundResource( ( (id+1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);		// Commented, Change_Request-20160101, ReqNo.3
			final View rowExpandView = getLayoutInflater().inflate(R.layout.portfolio_expand_row, null);
			rowExpandView.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					priceLabel.performClick();
				}
			});
			populateUnrealizeExpandViewData(rowExpandView, mCounter);

			tableRow.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					if( !mCounter.isExpand() ){
						// Added by Mary@20120919b - Fixes_Request-20120815, ReqNo.3
						if(rowLayout.getChildCount() == 1)
							rowLayout.addView(rowExpandView);

						// Added by Mary@20130710 - avoid IndexOutOfBoundException
						if( tableRow.getId() < expandid.length)
							expandid[tableRow.getId()] = true;

						mCounter.setExpand(true);
					}else{
						/* Mary@20120919b - Fixes_Request-20120815, ReqNo.3
					int childCount = rowLayout.getChildCount();
						 */
						if(rowLayout.getChildCount() != 1)
							rowLayout.removeViewAt(1);							

						mCounter.setExpand(false);

						// Added by Mary@20130710 - avoid IndexOutOfBoundException
						if( tableRow.getId() < expandid.length)
							expandid[tableRow.getId()]=false;

					}
				}
			});

			if(id < expandid.length && expandid[id]){
				mCounter.setExpand(true);
				rowLayout.addView(rowExpandView); 
			}

			tableLayout.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );
		}
	}
	// Added by Thinzar, Change_Request-20160101, ReqNo.9 - END

	//@20121024 FixReq20121023, ReqNo 9 -START - not used, replaced by constructTableUnrealiseNew
	private class constructTableUnrealise extends AsyncTask<View, Void, String>{

		public constructTableUnrealise(StockHolding counter,int id){
			super();

			final TableRow tableRow		= new TableRow(PortfolioActivity.this);   
			tableRow.setId(id);
			tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
			tableRow.setWeightSum(1);

			View rowView 				= getLayoutInflater().inflate(R.layout.order_row, null);  
			final LinearLayout rowLayout= (LinearLayout) rowView.findViewById(R.id.rowLayout);

			TextView nameLabel 			= (TextView) rowView.findViewById(R.id.nameLabel);
			nameLabel.setText(counter.getStockCode());

			final TextView priceLabel 	= (TextView) rowView.findViewById(R.id.priceLabel);
			/* Mary@20130723 - Fixes_Request-20130711, ReqNo.8
		marketValue = 0;
			 */
			marketValue[id] = 0;
			for (Iterator<StockSymbol> itr2=refreshStocks.iterator(); itr2.hasNext(); ){
				StockSymbol stock = itr2.next();
				if(counter.getSymbolCode().equals(stock.getSymbolCode())){

					if(counter.getQuantityOnHand() == 0 || (stock.getLastDone() == 0 && stock.getLACP() == 0) )

						marketValue[id] = 0;
					else

						marketValue[id] = ( stock.getLastDone() == 0 ? stock.getLACP() : stock.getLastDone() ) * counter.getQuantityOnHand();
					// Mary@20120919 - Fixes_Request-20120815, ReqNo.11 - END
					break;
				}
			}

			String strSymbolExchg		= counter.getSymbolCode().substring( (counter.getSymbolCode().lastIndexOf(".") + 1), counter.getSymbolCode().length() );
			if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )

				priceLabel.setText( FormatUtil.formatDouble2(marketValue[id], null) );
			else

				priceLabel.setText( FormatUtil.formatDouble(marketValue[id], null) );

			priceLabel.setOnClickListener(new OnClickListener(){
				public void onClick(View view){
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					selectedRow			= rowLayout;
					selectedId 			= tableRow.getId();
					int id 				= tableRow.getId();

					/* Mary@20130503 - Fixes_Request-20130419, ReqNo.13
				StockHolding counter= (StockHolding) stockHoldings.get(id);
					 */
					StockHolding counter= (StockHolding) filteredStockHoldings.get(id);
					details 			= new HashMap<String, String>();
					sequences 			= new ArrayList<String>();

					// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START

					if(intPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED)
						PortfolioActivity.this.populateCounterDetail(counter,id);
					else
						PortfolioActivity.this.populateCounterDetail2(counter,id);
					// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END

					Intent intent = new Intent();
					intent.putExtra(ParamConstants.SYMBOLCODE_TAG, counter.getSymbolCode());
					intent.putExtra(ParamConstants.STOCK_CODE_TAG, counter.getStockCode());
					intent.putExtra(ParamConstants.ORDER_DETAILS_TAG, details);
					intent.putExtra(ParamConstants.ORDER_DISPLAYSEQ_TAG, sequences);
					//intent.putExtra(AppConstants.ORDER_TYPE_TAG, actionType);
					intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.PORTFOLIO_DETAIL_TRADE);
					// Added by Mary@20120914 - Change_Request-20120719, ReqNo.12
					intent.putExtra( ParamConstants.LOT_SIZE_TAG, String.valueOf( counter.getIntLotSize()));
					intent.setClass(PortfolioActivity.this, OrderDetailActivity.class);
					startActivity(intent);
				}
			});

			final Button quantityButton = (Button) rowView.findViewById(R.id.quantityButton);  

			// Added by Mary@20120919 - Fixes_Request-20120815, ReqNo.11
			percentPL[id] = 0;
			for (Iterator<StockSymbol> itr2=refreshStocks.iterator(); itr2.hasNext(); ){
				StockSymbol stock = itr2.next();
				if(counter.getSymbolCode().equals(stock.getSymbolCode())){

					if(counter.getAvgPurchasePrice()==0 || counter.getQuantityOnHand() == 0 || (stock.getLastDone() == 0 && stock.getLACP() == 0 && counter.getAvgPurchasePrice() == 0) ){
						percentPL[id] = 0;
					}else{
						float fltActualPL	= ( (stock.getLastDone() == 0 ? stock.getLACP() : stock.getLastDone() ) - counter.getAvgPurchasePrice() ) * counter.getQuantityOnHand();
						float fltExpectedPL	= counter.getAvgPurchasePrice() * counter.getQuantityOnHand();
						percentPL[id]		= fltActualPL / fltExpectedPL;
						fltActualPL			= 0;
						fltExpectedPL		= 0;
					}
					// Mary@20120919 - Fixes_Request-20120815, ReqNo.11 - END
					break;
				}
			}

			// Added by Mary@20120919 - Fixes_Request-20120815, ReqNo.11
			unrealizeGL[id] = 0;
			for (Iterator<StockSymbol> itr2=refreshStocks.iterator(); itr2.hasNext(); ){
				StockSymbol stock = itr2.next();
				if(counter.getSymbolCode().equals(stock.getSymbolCode())){
					/* Mary@20120919 - Fixes_Request-20120815, ReqNo.11 - START
				unrealizeGL[id] = ((stock.getLastDone()-counter.getAvgPurchasePrice()) * counter.getQuantityOnHand());
					 */
					if(counter.getQuantityOnHand() == 0 || (stock.getLastDone() == 0 && stock.getLACP() == 0 && counter.getAvgPurchasePrice() == 0) )
						unrealizeGL[id] = 0;
					else
						unrealizeGL[id] = ( (stock.getLastDone() == 0 ? stock.getLACP() : stock.getLastDone() ) - counter.getAvgPurchasePrice() ) * counter.getQuantityOnHand() ;
					// Mary@20120919 - Fixes_Request-20120815, ReqNo.11 - END
					break;
				}
			}

			if (isDisplayPercentage) {
				// Added by Mary@20120804 - Fixes_Request-20120724, ReqNo.8
				((TextView)viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED).findViewById(R.id.titleThirdColumn)).setText(GLP);
				quantityButton.setText(FormatUtil.formatPricePercent(percentPL[id]));								
			} else {
				// Added by Mary@20120804 - Fixes_Request-20120724, ReqNo.8
				((TextView)viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED).findViewById(R.id.titleThirdColumn)).setText(GL);

				quantityButton.setText(FormatUtil.formatDouble(unrealizeGL[id], null));
			}

			if(quantityButton.getText().toString().indexOf("-")!=-1)
				quantityButton.setBackgroundResource(LayoutSettings.btnRed);
			else
				quantityButton.setBackgroundResource(LayoutSettings.btnGreen);

			quantityButton.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					changeButtonValue();
				}
			});

			quantityButtons.add(quantityButton);

			tableRow.addView(rowView);

			//rowLayout.setBackgroundResource( ( (id+1) % 2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);		// Commented, Change_Request-20160101, ReqNo.3
			final View rowExpandView = getLayoutInflater().inflate(R.layout.portfolio_expand_row, null);
			rowExpandView.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					priceLabel.performClick();
				}
			});
			populateUnrealizeExpandViewData(rowExpandView, counter);

			tableRow.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					StockHolding counter = (StockHolding) filteredStockHoldings.get( tableRow.getId() );
					if( !counter.isExpand() ){
						// Added by Mary@20120919b - Fixes_Request-20120815, ReqNo.3
						if(rowLayout.getChildCount() == 1)
							rowLayout.addView(rowExpandView);

						// Added by Mary@20130710 - avoid IndexOutOfBoundException
						if( tableRow.getId() < expandid.length)
							expandid[tableRow.getId()] = true;

						counter.setExpand(true);
					}else{
						/* Mary@20120919b - Fixes_Request-20120815, ReqNo.3
					int childCount = rowLayout.getChildCount();
						 */
						if(rowLayout.getChildCount() != 1)
							rowLayout.removeViewAt(1);							

						counter.setExpand(false);

						// Added by Mary@20130710 - avoid IndexOutOfBoundException
						if( tableRow.getId() < expandid.length)
							expandid[tableRow.getId()]=false;

					}
				}
			});

			if(id < expandid.length && expandid[id]){
				counter.setExpand(true);
				rowLayout.addView(rowExpandView);
			}

			tableLayout.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );  
		}

		@Override
		protected String doInBackground(View... arg0) {
			// TODO Auto-generated method stub
			return null;
		}

	}

	private class constructTableRealise extends AsyncTask<View,Void,String>{

		public constructTableRealise(StockHolding counter,int id){
			super();
			final TableRow tableRow			= new TableRow(PortfolioActivity.this);
			tableRow.setId(id);
			tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
			tableRow.setWeightSum(1);

			View rowView 					= getLayoutInflater().inflate(R.layout.realise_row, null);  
			final LinearLayout rowLayout	= (LinearLayout) rowView.findViewById(R.id.rowLayout);
			TextView nameLabel 				= (TextView) rowView.findViewById(R.id.nameLabel);
			nameLabel.setText(counter.getStockCode());
			final TextView realisePL 		= (TextView) rowView.findViewById(R.id.realise_pl);
			final TextView realisePLPercent = (TextView) rowView.findViewById(R.id.realise_plPercent);

			realisePLPercent.setText(""+FormatUtil.formatPricePercent(counter.getRealiseGainLossPercent()));
			realisePLPercent.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					selectedRow = rowLayout;
					selectedId = tableRow.getId();

					int id = tableRow.getId();

					StockHolding counter= (StockHolding) filteredStockHoldings.get(id);
					details 			= new HashMap<String, String>();
					sequences 			= new ArrayList<String>();
					// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START

					if(intPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED){
						PortfolioActivity.this.populateCounterDetail(counter,id);
					}else{
						PortfolioActivity.this.populateCounterDetail2(counter,id);
					}
					// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END
					Intent intent = new Intent();
					intent.putExtra(ParamConstants.SYMBOLCODE_TAG, counter.getSymbolCode());
					intent.putExtra(ParamConstants.STOCK_CODE_TAG, counter.getStockCode());
					intent.putExtra(ParamConstants.ORDER_DETAILS_TAG, details);
					intent.putExtra(ParamConstants.ORDER_DISPLAYSEQ_TAG, sequences);
					//  					intent.putExtra(AppConstants.ORDER_TYPE_TAG, actionType);			
					intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.PORTFOLIO_DETAIL_TRADE);
					// Added by Mary@20120914 - Change_Request-20120719, ReqNo.12
					intent.putExtra( ParamConstants.LOT_SIZE_TAG, String.valueOf( counter.getIntLotSize() ) );
					intent.setClass(PortfolioActivity.this, OrderDetailActivity.class);
					startActivity(intent);

				}
			});

			realisePL.setText( "" + FormatUtil.formatDouble(counter.getRealiseGainLossAmount(), null) );
			realisePL.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					selectedRow = rowLayout;
					selectedId = tableRow.getId();

					/* Commented out by Thinzar, Change_Request-20160101, ReqNo.3
				rowLayout.setBackgroundDrawable(null);
				rowLayout.setBackgroundColor(Color.CYAN);
					 */
					int id = tableRow.getId();

					StockHolding counter= (StockHolding) filteredStockHoldings.get(id);
					details 			= new HashMap<String, String>();
					sequences 			= new ArrayList<String>();
					// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START

					if(intPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED){
						PortfolioActivity.this.populateCounterDetail(counter,id);
					}else{
						PortfolioActivity.this.populateCounterDetail2(counter,id);
					}
					// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END
					Intent intent = new Intent();
					intent.putExtra(ParamConstants.SYMBOLCODE_TAG, counter.getSymbolCode());
					intent.putExtra(ParamConstants.STOCK_CODE_TAG, counter.getStockCode());
					intent.putExtra(ParamConstants.ORDER_DETAILS_TAG, details);
					intent.putExtra(ParamConstants.ORDER_DISPLAYSEQ_TAG, sequences);
					//  					intent.putExtra(AppConstants.ORDER_TYPE_TAG, actionType);				
					intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.PORTFOLIO_DETAIL_TRADE);
					// Added by Mary@20120914 - Change_Request-20120719, ReqNo.12
					intent.putExtra( ParamConstants.LOT_SIZE_TAG, String.valueOf( counter.getIntLotSize() ) );
					intent.setClass(PortfolioActivity.this, OrderDetailActivity.class);
					startActivity(intent);
				}
			});

			tableRow.addView(rowView);

			final View rowExpandView = getLayoutInflater().inflate(R.layout.portfolio_expand_row, null);
			rowExpandView.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					realisePL.performClick();
				}
			});
			populateRealizeExpandViewData(rowExpandView, counter);

			tableRow.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

					StockHolding counter = (StockHolding) filteredStockHoldings.get(tableRow.getId());
					if( !counter.isExpand() ){
						// Added by Mary@20120919 - Fixes_Request-20120815, ReqNo.3
						if(rowLayout.getChildCount() == 1)
							rowLayout.addView(rowExpandView);

						// Added by Mary@20130710 - avoid IndexOutOfBoundException
						if( tableRow.getId() < expandid.length)
							expandid[tableRow.getId()] = true;

						counter.setExpand(true);
					}else{

						if(rowLayout.getChildCount() > 1)
							rowLayout.removeViewAt(1);

						// Added by Mary@20130710 - avoid IndexOutOfBoundException
						if( tableRow.getId() < expandid.length)
							expandid[tableRow.getId()]=false;

						counter.setExpand(false);

					}
				}
			});

			/* Mary@20130710 - avoid IndexOutOfBoundException
		if(expandid[id]){
			 */
			if(id < expandid.length && expandid[id]){
				counter.setExpand(true);
				rowLayout.addView(rowExpandView);
			}

			tableLayout.addView( tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT) );  

		}

		@Override
		protected String doInBackground(View... params) {
			// TODO Auto-generated method stub
			return null;

		}
	}

	//@20121024 FixReq20121023, ReqNo 9 -END

	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

			if(this.mainMenu.isShowing()){
				this.mainMenu.hide();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
				 */
				// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
			}else if( this.exchgListView.isShowing() ){
				this.exchgListView.hide();
				// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
			}else{
				/* Mary@20130715 - Change_Request-20130424, ReqNo.15
			portfolioThread.stopRequest();
				 */
				finish();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
				 */
			}
			// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}


	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		isDifferentClient					= true; 	//Added by Thinzar, Fixes_Request-20140820, ReqNo.33

		if (requestCode == AppConstants.SELECT_ACCOUNT_REQUEST) {
			if (resultCode == Activity.RESULT_OK) {
				Bundle bundle				= data.getExtras();
				String accountNo 			= bundle.getString(ParamConstants.ACCOUNTNO_TAG);
				String branchCode 			= bundle.getString(ParamConstants.BRANCHCODE_TAG);

				StockApplication application= (StockApplication) this.getApplication();
				List<TradeAccount> accounts				= application.getTradeAccounts();

				for(Iterator<TradeAccount> itr=accounts.iterator(); itr.hasNext(); ){
					TradeAccount account = itr.next();
					if( account.getAccountNo().equals(accountNo) && account.getBranchCode().equals(branchCode) ){
						tradeAccount 			= account; 
						application.setSelectedAccount(account);
						/* Mary@20130715 - Change_Request-20130424, ReqNo.15
					portfolioThread.setTradeAccount(account);
						 */
						AppConstants.selectedAcc= account;

						String strQCExchgCode	= null;
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
						String strTrdExchgCode	= null;
						for( Iterator<ExchangeInfo> itr2=application.getExchangeInfos().iterator(); itr2.hasNext(); ){
							ExchangeInfo exchgInfo = itr2.next();
							if( exchgInfo.getTrdExchgCode() != null && account.getExchangeCode().equals( exchgInfo.getTrdExchgCode() ) ){
								strQCExchgCode	= exchgInfo.getQCExchangeCode();
								// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
								strTrdExchgCode	= exchgInfo.getTrdExchgCode();
								break;
							}
						}

						// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
						Map<String, String> mapBrokerUserPreference	= new HashMap<String, String>();
						mapBrokerUserPreference.put( PreferenceConstants.EXCHANGE_CODE, strQCExchgCode );
						mapBrokerUserPreference.put( PreferenceConstants.ACCOUNT_NO, account.getAccountNo() );
						mapBrokerUserPreference.put( PreferenceConstants.BRANCH_CODE, account.getBranchCode() );
						// Added by Mary@20121112 - Change_Request-20120719, ReqNo.12
						mapBrokerUserPreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, strTrdExchgCode);
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
						mapBrokerUserPreference.put(PreferenceConstants.TRADE_EXCHANGE_CODE, strTrdExchgCode);
						AppConstants.cacheBrokerUserDetails(PortfolioActivity.this, sharedPreferences, mapBrokerUserPreference);
						mapBrokerUserPreference.clear();
						mapBrokerUserPreference						= null;
						// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END

						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12 - START
						strQCExchgCode 		= null;
						strTrdExchgCode 	= null;
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12 - END

						// Added by Melissa - START
						intPortfolioType= ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED;

						setSelectedPortfolioHeader(leftTextView);

						viewPager.setCurrentItem(ParamConstants.INT_PAGE_UNREALIZED);
						viewPagerDerivative.setCurrentItem(ParamConstants.INT_PAGE_VIRTUALPL);
						// Added by Melissa - END
						break;
					}
				}
			}
		}else if (requestCode == AppConstants.DERIVATIVE_DETAIL_REQUEST){

		}
	}

	// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START
	private void constructRealizeTableView(List<StockHolding> lstFilteredStockHoldings){
		// Added by Mary@20130510 - Fixes_Request-20130424, ReqNo.5
		boolean isSelfInitiate	= false;
		try{
			// Added by Mary@20130510 - Fixes_Request-20130424, ReqNo.5 - START
			if( dialog != null && ! dialog.isShowing() ){
				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));

				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! PortfolioActivity.this.isFinishing() )
					dialog.show();

				isSelfInitiate	= true;
			}
			// Added by Mary@20130510 - Fixes_Request-20130424, ReqNo.5 - END

			this.tableLayout = (TableLayout)viewPager.getChildAt(ParamConstants.INT_PAGE_REALIZED).findViewById(R.id.portfolioTable);
			this.tableLayout.removeAllViews();
			int id 			= 0;

			//Added by Thinzar, Change_Request-20160101, ReqNo.9
			List<StockHolding> sortedStockHoldings	= SortOrders(lstFilteredStockHoldings, currentRealiseSortByMethod, ParamConstants.INT_PORTFOLIOTYPE_REALIZED);
			lstFilteredStockHoldings	= sortedStockHoldings;

			for (Iterator<StockHolding> itr=lstFilteredStockHoldings.iterator(); itr.hasNext(); ) {

				StockHolding stockHolding	= itr.next();

				new constructTableRealise(stockHolding, id++).execute();
			}
		}catch (Exception ex){
			ex.printStackTrace();
		}

		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
		try{
			// Added by Mary@20130510 - Fixes_Request-20130424, ReqNo.5 - START
			if( isSelfInitiate && dialog != null && dialog.isShowing() ){
				dialog.dismiss();
				isSelfInitiate	= false;
			}
			// Added by Mary@20130510 - Fixes_Request-20130424, ReqNo.5 - END
			// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
			isSelfInitiate	= false;
		}
		// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END

		//@20121024 FixReq20121023, ReqNo 9 -START
		isConstructing=false;
		//@20121024 FixReq20121023, ReqNo 9 -START
	}
	// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END

	private void constructUnrealizeTableView(List<StockHolding> lstFilteredStockHolding){
		// Added by Mary@20130510 - Fixes_Request-20130424, ReqNo.5 - START
		boolean isSelfInitiate	= false;

		try {

			this.tableLayout = (TableLayout)viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED).findViewById(R.id.portfolioTable);
			this.tableLayout.removeAllViews();

			// Added by Mary@20130723 - Fixes_Request-20130711, ReqNo.8
			marketValue		= new double[lstFilteredStockHolding.size()];

			quantityButtons = new ArrayList<Button>();

			new ConstructTableUnrealiseNew().execute();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		//@20121024 FixReq20121023, ReqNo 9 -START
		isConstructing=false;
	}

	private void changeButtonValue(){
		int index = 0;

		for (Iterator<Button> itr=quantityButtons.iterator(); itr.hasNext(); ) {
			Button quantityButton= itr.next();
			StockHolding mCounter	= filteredStockHoldings.get(index);
			//String strSymbolExchg		= mCounter.getSymbolCode().substring( (mCounter.getSymbolCode().lastIndexOf(".") + 1), mCounter.getSymbolCode().length() );

			if (isDisplayPercentage) {

				((TextView)viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED).findViewById(R.id.titleThirdColumn)).setText(GL);

				quantityButton.setText(FormatUtil.formatDouble(mCounter.getCalculatedUnrealizeGL(), null));

			} else {
				// Added by Mary@20120804 - Fixes_Request-20120724, ReqNo.8
				//headerLabel.setText("G/L%");
				((TextView)viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED).findViewById(R.id.titleThirdColumn)).setText(GLP);
				quantityButton.setText(FormatUtil.formatPricePercent(mCounter.getCalculatedPercentPL()));
			}

			//Added by Thinzar, Change_Request-20160101, ReqNo.9 - to hide/show sortOrder arrow accordingly			
			if(currentUnrealiseSortByMethod == SORT_BY_GL_ASC || currentUnrealiseSortByMethod == SORT_BY_GL_DESC){
				if((isSortByUnrealiseGLPercent && !isDisplayPercentage) || 
						(!isSortByUnrealiseGLPercent && isDisplayPercentage)){
					((ImageView) viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED)
							.findViewById(R.id.imgSortPortfolioByGL)).setVisibility(View.VISIBLE);
				} else{
					((ImageView) viewPager.getChildAt(ParamConstants.INT_PAGE_UNREALIZED)
							.findViewById(R.id.imgSortPortfolioByGL)).setVisibility(View.INVISIBLE);
				}
			}
			//Added by Thinzar, Change_Request-20160101, ReqNo.9 - END

			index++;
		}

		isDisplayPercentage	= !isDisplayPercentage;
	}

	private void changeBetAvgPriceNLastDone(ArrayList<DervTradePrtfSubDetailRpt> dervTradeprtfSubDetailRptList){
		TextView titleThirdColumn = (TextView) viewPagerDerivative.getChildAt(ParamConstants.INT_PAGE_SUMMARY).findViewById(R.id.titleThirdColumn);
		TableLayout tableLayout = (TableLayout) viewPagerDerivative.getChildAt(ParamConstants.INT_PAGE_SUMMARY).findViewById(R.id.derivativeSummaryTable);

		if(((String)titleThirdColumn.getText()).equalsIgnoreCase(LAST)){
			titleThirdColumn.setText(AVERAGE_PRICE);
			for(int i=0;i<dervTradeprtfSubDetailRptList.size();i++){
				((Button)tableLayout.getChildAt(i).findViewById(R.id.buttonLastDone)).setText(FormatUtil.formatFloatToString(dervTradeprtfSubDetailRptList.get(i).getAveragePrice()));
			}
		}else{
			titleThirdColumn.setText(LAST);
			for(int i=0;i<dervTradeprtfSubDetailRptList.size();i++){
				((Button)tableLayout.getChildAt(i).findViewById(R.id.buttonLastDone)).setText(FormatUtil.formatFloatToString(dervTradeprtfSubDetailRptList.get(i).getLastDone()));
			}
		}	
	}

	private void populateCounterDetail(StockHolding counter, int id) {
		String strExchgCode	= (counter.getSymbolCode().split("\\."))[1];

		final String PARAM_QTY_ON_HAND	= getResources().getString(R.string.portfolio_param_qty_on_hand);			//"Qty on Hand(Unit)";
		final String PARAM_QTY_AVAIL	= getResources().getString(R.string.portfolio_param_qty_available);			//"Qty Available(Unit)";
		final String PARAM_QTY_QUE_SELL	= getResources().getString(R.string.portfolio_param_qty_queued_sell);		//"Qty Queued Sell(Unit)";
		final String PARAM_AVG_BUY_PRC	= getResources().getString(R.string.portfolio_param_avg_buy_price);			//"Average Buy Price";
		final String PARAM_MKT_VAL		= getResources().getString(R.string.portfolio_param_mkt_value);				//"Market Value";
		final String PARAM_UNREALIZE_GL	= getResources().getString(R.string.portfolio_param_unrealized_gl);			//"Unrealized G/L";
		final String PARAM_PL_PERCENTAGE= getResources().getString(R.string.portfolio_param_unrealized_gl_percent);	//"Unrealized G/L %";
		final String PARAM_LOT_SIZE		= getResources().getString(R.string.order_param_lot_size);					//"Lot Size";

		details.put( PARAM_QTY_ON_HAND, FormatUtil.formatQty(counter.getQuantityOnHand()));	//Edited, Fixes_Request-20160101,ReqNo.5
		sequences.add(PARAM_QTY_ON_HAND);

		details.put( PARAM_QTY_AVAIL, FormatUtil.formatQty(counter.getQuantityAvailable())); //Edited, Fixes_Request-20160101,ReqNo.5
		sequences.add(PARAM_QTY_AVAIL);

		details.put( PARAM_QTY_QUE_SELL, FormatUtil.formatQty(counter.getQuantitySellQueued()));	//Edited, Fixes_Request-20160101,ReqNo.5
		sequences.add(PARAM_QTY_QUE_SELL);

		details.put( PARAM_AVG_BUY_PRC, FormatUtil.formatPrice(counter.getAvgPurchasePrice(), null, strExchgCode));
		sequences.add(PARAM_AVG_BUY_PRC);

		details.put( PARAM_MKT_VAL, FormatUtil.formatDouble(counter.getCalculatedMarketValue(), null) );	//Edited, based on changes for portfolio sorting
		sequences.add(PARAM_MKT_VAL);

		details.put( PARAM_UNREALIZE_GL, FormatUtil.formatDouble( counter.getCalculatedUnrealizeGL(), null) );	//Edited, based on changes for portfolio sorting
		sequences.add(PARAM_UNREALIZE_GL);

		details.put( PARAM_PL_PERCENTAGE, FormatUtil.formatPricePercent( counter.getCalculatedPercentPL() ) );	//details.put( PARAM_PL_PERCENTAGE, FormatUtil.formatPricePercent( percentPL[id] ) );
		sequences.add(PARAM_PL_PERCENTAGE);

		details.put( PARAM_LOT_SIZE, FormatUtil.formatInteger( counter.getIntLotSize(), null ) );
		sequences.add(PARAM_LOT_SIZE);
		// Mary@20120918 - Fixes_Request-20120815, ReqNo.3 & 15 - END

		//Added by Thinzar@20140805 - Change_Request-20140701 - ReqNo. 9 - START
		if(pymtTypes.size() > 0){
			final String PARAM_SETT_CURRENCY = getResources().getString(R.string.portfolio_param_currency);		//"Currency";
			final String PARAM_INV_PORTFOLIO = getResources().getString(R.string.portfolio_param_inv_portfolio);//"Investment Portfolio";

			details.put(PARAM_SETT_CURRENCY, counter.getSettlementCurrency());
			sequences.add(PARAM_SETT_CURRENCY);

			details.put(PARAM_INV_PORTFOLIO, counter.getInvestmentPortfolio());
			sequences.add(PARAM_INV_PORTFOLIO);
		}
		//Added by Thinzar@20140805 - END
	}

	// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START
	private void populateCounterDetail2(StockHolding counter, int id){
		String strExchgCode	= (counter.getSymbolCode().split("\\."))[1];

		final String PARAM_AGGR_BUY_PRC			= getResources().getString(R.string.portfolio_param_aggr_buy_price);	//"Aggregated Buy Price";		//edited term, Fixes_Request-20140929,ReqNo.6
		final String PARAM_AGGR_SELL_PRC		= getResources().getString(R.string.portfolio_param_aggr_sell_price);	//"Aggregated Sell Price";
		final String PARAM_TTL_QTY_FRM_HOLDING	= getResources().getString(R.string.portfolio_param_qty_from_holding_unit);//"Qty From Holding (Unit)";
		final String PARAM_TTL_QTY_SHORT		= getResources().getString(R.string.portfolio_param_qty_short_unit);		//"Qty Short (Unit)";
		final String PARAM_TTL_QTY_SOLD			= getResources().getString(R.string.portfolio_param_qty_sold_unit);		//"Qty Sold (Unit)";
		final String PARAM_TTL_BROKERAGE		= getResources().getString(R.string.portfolio_param_brokerage);		//"Brokerage";
		final String PARAM_REALIZE_PL			= getResources().getString(R.string.portfolio_param_realized_gl);		//"Realized G/L";
		final String PARAM_REALIZE_PL_PERCENTAGE= getResources().getString(R.string.portfolio_param_realized_gl_percent);	//"Realized G/L %";
		final String PARAM_LOT_SIZE				= getResources().getString(R.string.order_param_lot_size);	//"Lot Size";

		details.put( PARAM_AGGR_BUY_PRC, FormatUtil.formatPrice(counter.getAggrBuyPrice(), null, strExchgCode));
		sequences.add(PARAM_AGGR_BUY_PRC);

		details.put( PARAM_AGGR_SELL_PRC, FormatUtil.formatPrice(counter.getAggrSellPrice(), null, strExchgCode));
		sequences.add(PARAM_AGGR_SELL_PRC);

		details.put(PARAM_TTL_QTY_FRM_HOLDING, FormatUtil.formatQty(counter.getTotalQtyFromHolding()));//Edited, Fixes_Request-20160101,ReqNo.5
		sequences.add(PARAM_TTL_QTY_FRM_HOLDING);

		details.put(PARAM_TTL_QTY_SHORT,FormatUtil.formatQty(counter.getTotalQtyShort()));	//Edited, Fixes_Request-20160101,ReqNo.5
		sequences.add(PARAM_TTL_QTY_SHORT);

		details.put(PARAM_TTL_QTY_SOLD, FormatUtil.formatQty(counter.getTotalQtySold()));	//Edited, Fixes_Request-20160101,ReqNo.5
		sequences.add(PARAM_TTL_QTY_SOLD);

		details.put(PARAM_TTL_BROKERAGE, FormatUtil.formatFloat(counter.getTotalBrokerage(), null));
		sequences.add(PARAM_TTL_BROKERAGE);

		details.put(PARAM_REALIZE_PL, FormatUtil.formatDouble(counter.getRealiseGainLossAmount(), null));
		sequences.add(PARAM_REALIZE_PL);

		details.put(PARAM_REALIZE_PL_PERCENTAGE, FormatUtil.formatPricePercent(counter.getRealiseGainLossPercent()));
		sequences.add(PARAM_REALIZE_PL_PERCENTAGE);

		details.put( PARAM_LOT_SIZE, FormatUtil.formatInteger( counter.getIntLotSize(), null ) );
		sequences.add(PARAM_LOT_SIZE);
		// Mary@20120918 - Fixes_Request-20120815, ReqNo.3 - END

		//Added by Thinzar@20140805 - Change_Request-20140701 - ReqNo. 9 - START
		if(pymtTypes.size() > 0){
			final String PARAM_SETT_CURRENCY = getResources().getString(R.string.portfolio_param_currency);			//"Currency";
			final String PARAM_INV_PORTFOLIO = getResources().getString(R.string.portfolio_param_inv_portfolio);	//"Investment Portfolio";

			details.put(PARAM_SETT_CURRENCY, counter.getSettlementCurrency());
			sequences.add(PARAM_SETT_CURRENCY);

			details.put(PARAM_INV_PORTFOLIO, counter.getInvestmentPortfolio());
			sequences.add(PARAM_INV_PORTFOLIO);
		}
		//Added by Thinzar@20140805 - END
	}

	// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END

	private void populateUnrealizeExpandViewData(View expandView, StockHolding counter) {
		String strExchgCode	= (counter.getSymbolCode().split("\\."))[1];

		TextView quantityLabel	= (TextView) expandView.findViewById(R.id.quantityLabel);

		int intLotSize			= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? counter.getIntLotSize() : 1;

		//quantityLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (counter.getQuantityOnHand() / intLotSize), null ) );
		quantityLabel.setText(FormatUtil.formatQty(counter.getQuantityOnHand()));		//commented above line and Edited by Thinzar, Change_Request-20150601, ReqNo.1

		TextView purchaseLabel 	= (TextView) expandView.findViewById(R.id.purchaseLabel);

		purchaseLabel.setText(FormatUtil.formatPrice(counter.getAvgPurchasePrice(), null, strExchgCode));

		TextView lastDoneLabel = (TextView) expandView.findViewById(R.id.lastDoneLabel);

		// Fixes_Request-20160101, ReqNo.8
		if (counter.getLastDone() == 0) {
			lastDoneLabel.setText(FormatUtil.formatPrice(counter.getLacp(), null, strExchgCode));
		} else {
			lastDoneLabel.setText(FormatUtil.formatPrice(counter.getLastDone(), null, strExchgCode));
		}

		//Added by Thinzar@20140805 - Change_Request-20140701, ReqNo. 9 - START
		TableRow tblRowPaymentType = (TableRow)expandView.findViewById(R.id.tblRowPaymentType);
		TextView currencyLabel = (TextView)expandView.findViewById(R.id.currencyLabel);
		TextView invPortfolioLabel = (TextView)expandView.findViewById(R.id.invPortfolioLabel);

		if(pymtTypes.size() > 0){
			tblRowPaymentType.setVisibility(View.VISIBLE);
			currencyLabel.setText(counter.getSettlementCurrency());
			invPortfolioLabel.setText(counter.getInvestmentPortfolio());
		} else {
			tblRowPaymentType.setVisibility(View.GONE);
		}
		//Added by Thinzar@20140805 - END
	}

	// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-START
	private void populateRealizeExpandViewData(View expandView, StockHolding counter) {
		String strExchgCode	= (counter.getSymbolCode().split("\\."))[1];

		TextView topLeftLabel	= (TextView)expandView.findViewById(R.id.lbl_topleft);
		topLeftLabel.setText(getResources().getString(R.string.portfolio_param_aggr_buy_price) + ":");

		TextView quantityLabel	= (TextView) expandView.findViewById(R.id.quantityLabel);

		quantityLabel.setText( FormatUtil.formatPrice( counter.getAggrBuyPrice(), null, strExchgCode) );


		TextView bottomLeftLabel= (TextView)expandView.findViewById(R.id.lbl_bottomleft);
		bottomLeftLabel.setText(getResources().getString(R.string.portfolio_param_aggr_sell_price) + ":");

		TextView purchaseLabel 	= (TextView) expandView.findViewById(R.id.purchaseLabel);

		purchaseLabel.setText(FormatUtil.formatPrice(counter.getAggrSellPrice(), null, strExchgCode));

		TextView bottomRightLabel= (TextView)expandView.findViewById(R.id.lbl_bottomright);
		bottomRightLabel.setText(getResources().getString(R.string.portfolio_param_qty_sold) + ":");

		TextView lastDoneLabel	= (TextView) expandView.findViewById(R.id.lastDoneLabel);

		int intLotSize			= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? counter.getIntLotSize() : 1;

		//lastDoneLabel.setText( FormatUtil.formatLong( intLotSize == 0 ? 0 : (counter.getTotalQtySold() / intLotSize), null ) );
		lastDoneLabel.setText(FormatUtil.formatQty(counter.getTotalQtySold()));	//commented above and Edited by Thinzar, Change_Request-20150601, ReqNo.1

		//Added by Thinzar@20140805 - Change_Request-20140701, ReqNo. 9 - START
		TableRow tblRowPaymentType = (TableRow)expandView.findViewById(R.id.tblRowPaymentType);
		TextView currencyLabel = (TextView)expandView.findViewById(R.id.currencyLabel);
		TextView invPortfolioLabel = (TextView)expandView.findViewById(R.id.invPortfolioLabel);

		if(pymtTypes.size() > 0){
			tblRowPaymentType.setVisibility(View.VISIBLE);
			currencyLabel.setText(counter.getSettlementCurrency());
			invPortfolioLabel.setText(counter.getInvestmentPortfolio());
		} else {
			tblRowPaymentType.setVisibility(View.GONE);
		}
		//Added by Thinzar@20140805 - END
	}
	// Added by KW@20120827 - Change_Request-20120827, ReqNo.11-END

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message = handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;

			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if( PortfolioActivity.this.mainMenu.isShowing() )
					PortfolioActivity.this.mainMenu.hide();

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.refreshThread != null)
					LoginActivity.refreshThread.stopRequest();

				if(loginThread != null)
					loginThread.stopRequest();

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();

				if( !isFinishing() )
					PortfolioActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};


	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {

		if(derivativePortfolio){
			refreshStocks = changedSymbols;
			refreshDervTradePrtfSubDetailRptList(refreshStocks);
			derivative.calculate();

			Message message = refreshStockSymbolHandler.obtainMessage();
			message.obj 	= changedSymbols;
			refreshStockSymbolHandler.sendMessage(message);
		}else{

		}

	}

	final Handler refreshStockSymbolHandler = new Handler() {
		public void handleMessage(Message message) {
			derivative.setState(UPDATE_FR_FEED);
			setVirtualPLValues(derivative);
			constructDerivativeSummaryTableView(derivative);
		}
	};

	// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
	private void doExchangeList() {
		if( exchgListView.isShowing() ){
			exchgListView.hide();
		}else{
			if( mainMenu.isShowing() )
				mainMenu.hide();

			exchgListView.show(findViewById(R.id.rl_pv_TopNavBar), selectedExchangeMap);
		}
	}

	public void exchangeListSelectedEvent(Map<String, Object> exchangeMap, Sector sector) {
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

		selectedExchangeMap				= exchangeMap;

		strSelectedFilterExchangeType	= String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
		btnExchangeType.setText( String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );

		//Commented out and added by Thinzar@20140806, Change_Request-20140701, ReqNo.9
		//this.filteredStockHoldings	= getFilteredStockHoldingList(this.stockHoldings, this.strSelectedFilterExchangeType, this.intPortfolioType);
		this.filteredStockHoldings	= getFilteredStockHoldingList2(this.stockHoldings, this.strSelectedFilterExchangeType, this.intPortfolioType, this.strSelectedFilterPayType);

		isConstructing=true;

		if(this.intPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED){

			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				constructUnrealizeTableView(filteredStockHoldings);
		}else{

			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				constructRealizeTableView(filteredStockHoldings);
		}
	}
	// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END

	// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - START
	private List<StockHolding> getFilteredStockHoldingList(List<StockHolding> lstStockHolding, String strSelectedFilterExchgType, int intPortfolioType){
		List<StockHolding> filteredList	= new ArrayList<StockHolding>();

		for (Iterator<StockHolding> itr=lstStockHolding.iterator(); itr.hasNext(); ) {
			StockHolding stockHolding	= itr.next();
			StockHolding tempSH			= null;
			if(strSelectedFilterExchgType == null){
				tempSH	= stockHolding;
			}else{
				if( strSelectedFilterExchgType.equalsIgnoreCase(ALL_TYPE_KEY) ){
					tempSH	= stockHolding;
				}else{
					/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
				String strCurrSymbolCode		= stockHolding.getSymbolCode();
				String strCurrTradeExchgCode	= strCurrSymbolCode.split("\\.")[1];
					 */
					String strCurrTradeExchgCode	= stockHolding.getSymbolCode().substring( (stockHolding.getSymbolCode().lastIndexOf(".") + 1), stockHolding.getSymbolCode().length() );
					strCurrTradeExchgCode			= ( strCurrTradeExchgCode.length() > 1 && strCurrTradeExchgCode.toUpperCase().endsWith("D") ) ? strCurrTradeExchgCode.substring(0, strCurrTradeExchgCode.length() - 1) : strCurrTradeExchgCode;
					if( strSelectedFilterExchgType.equalsIgnoreCase(strCurrTradeExchgCode) )
						tempSH = stockHolding;
				}
			}

			if(tempSH != null){
				// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27
				if(tempSH.getIntPortfolioType() == intPortfolioType)
					filteredList.add(tempSH);
			}
		}

		return filteredList;
	}
	// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - END

	//Added by Thinzar@20140805 - Change_Request-20140701, ReqNo.9 - START
	//This is basically an extension from the above method
	private List<StockHolding> getFilteredStockHoldingList2(List<StockHolding> lstStockHolding, String strSelectedFilterExchgType, int intPortfolioType, String strSelectedFilterPayType){
		List<StockHolding> filteredList	= new ArrayList<StockHolding>();
		for (Iterator<StockHolding> itr=lstStockHolding.iterator(); itr.hasNext(); ) {
			StockHolding stockHolding	= itr.next();
			StockHolding tempSH			= null;
			int condition = 0;

			if(strSelectedFilterExchangeType == null || strSelectedFilterExchangeType.equalsIgnoreCase(ALL_TYPE_KEY))
				condition++;
			else{
				String strCurrTradeExchgCode	= stockHolding.getSymbolCode().substring( (stockHolding.getSymbolCode().lastIndexOf(".") + 1), stockHolding.getSymbolCode().length() );
				strCurrTradeExchgCode			= ( strCurrTradeExchgCode.length() > 1 && strCurrTradeExchgCode.toUpperCase().endsWith("D") ) ? strCurrTradeExchgCode.substring(0, strCurrTradeExchgCode.length() - 1) : strCurrTradeExchgCode;

				if(strSelectedFilterExchangeType.equalsIgnoreCase(strCurrTradeExchgCode))
					condition++;
			}

			if(strSelectedFilterPayType == null || strSelectedFilterPayType.equalsIgnoreCase(getResources().getString(R.string.all_pymt_type_label)))
				condition++;
			else{
				if(strSelectedFilterPayType.equalsIgnoreCase(stockHolding.getInvestmentPortfolio()))	condition++;
			}

			if(condition == 2)	tempSH = stockHolding;
			else tempSH = null;

			if(tempSH != null){
				// Added by Mary@20130704 - Fixes_Request-20130523, ReqNo.27
				if(tempSH.getIntPortfolioType() == intPortfolioType)
					filteredList.add(tempSH);
			}
		}

		return filteredList;
	}

	View.OnClickListener paymentFilter = new OnClickListener(){

		@Override
		public void onClick(View v) {
			if(idleThread != null && !isShowingIdleAlert)
				idleThread.resetExpiredTime();

			PortfolioActivity.this.doFilterPayment();
		}

	};

	private void doFilterPayment(){
		if(payFilterListView.isShowing()){
			payFilterListView.hide();
		} else{
			if(mainMenu.isShowing()){
				mainMenu.hide();
			}
			payFilterListView.show(findViewById(R.id.linearLayoutContainer), strSelectedFilterPayType);
		}
	}

	@Override
	public void payTypeFilterSelectedEvent(String payTypeFilter) {
		this.strSelectedFilterPayType = payTypeFilter;
		if(payTypeFilter != null){
			btnPaymentType.setText(payTypeFilter);
		}

		filteredStockHoldings	= getFilteredStockHoldingList2(stockHoldings, strSelectedFilterExchangeType, intPortfolioType, strSelectedFilterPayType);

		if(this.intPortfolioType == ParamConstants.INT_PORTFOLIOTYPE_UNREALIZED){

			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				constructUnrealizeTableView(filteredStockHoldings);	

		}else{
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				constructRealizeTableView(filteredStockHoldings);

		}		
	}
	//Added by Thinzar@20140731 - Change_Request-20140701, ReqNo.9 - END
}