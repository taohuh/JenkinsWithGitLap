package com.n2nconnect.android.stock.gcm;

import java.security.interfaces.RSAPrivateKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jose4j.base64url.Base64;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ParseException;

public class N2NJwt {
	
	//generate JWT Token
	public static Map<String, String> generateJWT(String pubKey, RSAPrivateKey privKey, String keyID, 
			String issuer, String audience, int expireTime, int nbf, String subject, String ev){
		
		Map<String, String> result = new HashMap<String, String>();

		try {	 
			RsaJsonWebKey eStatementJWK =  (RsaJsonWebKey)PublicJsonWebKey.Factory.newPublicJwk(pubKey);//pubKeyCache.getIfPresent(bhCode)
			eStatementJWK.setKeyId("rk1");

			JwtClaims claims = new JwtClaims();
			claims.setIssuer(issuer);  // who creates the token and signs it
			claims.setAudience(audience); // to whom the token is intended to be sent
			claims.setExpirationTimeMinutesInTheFuture(expireTime); // time when the token will expire (10 minutes from now)
			claims.setGeneratedJwtId(); // a unique identifier for the token
			claims.setIssuedAtToNow();  // when the token was issued/created (now)				  
			claims.setNotBeforeMinutesInThePast(nbf); // time before which the token is not yet valid (10 minutes ago)
			claims.setSubject(subject); // the subject/principal is whom the token is about
			claims.setClaim("ev",ev); // additional claims/attributes about the subject can be added

			// A JWT is a JWS and/or a JWE with JSON claims as the payload.
			// In this example it is a JWS nested inside a JWE
			// So we first create a JsonWebSignature object.
			JsonWebSignature jws = new JsonWebSignature();

			//The payload of the JWS is JSON content of the JWT Claims
			jws.setPayload(claims.toJson());
			jws.setKey(privKey);
			jws.setKeyIdHeaderValue("sk1");

			// Set the signature algorithm on the JWT/JWS that will integrity protect the claims
			jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

			// Sign the JWS and produce the compact serialization, which will be the inner JWT/JWS
			// representation, which is a string consisting of three dot ('.') separated
			// base64url-encoded parts in the form Header.Payload.Signature
			String innerJwt = jws.getCompactSerialization();

			// The outer JWT is a JWE
			JsonWebEncryption jwe = new JsonWebEncryption();

			// The output of the ECDH-ES key agreement will encrypt a randomly generated content encryption key
			jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.RSA_OAEP);

			// The content encryption key is used to encrypt the payload
			// with a composite AES-CBC / HMAC SHA2 encryption algorithm
			String encAlg = ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256;
			jwe.setEncryptionMethodHeaderParameter(encAlg);

			// We encrypt to the receiver using their public key
			jwe.setKey(eStatementJWK.getPublicKey());
			jwe.setKeyIdHeaderValue(eStatementJWK.getKeyId());	  
			jwe.setHeader("keyID",keyID);

			// A nested JWT requires that the cty (Content Type) header be set to "JWT" in the outer JWT
			jwe.setContentTypeHeaderValue("JWT");

			// The inner JWT is the payload of the outer JWT
			jwe.setPayload(innerJwt);

			// Produce the JWE compact serialization, which is the complete JWT/JWE representation,
			// which is a string consisting of five dot ('.') separated
			// base64url-encoded parts in the form Header.EncryptedKey.IV.Ciphertext.AuthenticationTag
			String jwt = jwe.getCompactSerialization();

			// Now you can do something with the JWT. Like send it to some other party
			// over the clouds and through the interwebs.
			System.out.println("JWT="+jwt);

			result.put("s", "200");
			result.put("v", jwt);
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			result.put("s", "500");
			result.put("v", e.getMessage());
		}

		return result;
	}

	public static boolean generateKey(Context context){

		boolean isSuccess = false;
		
		JsonWebKeySet jwks = new JsonWebKeySet(); //To store JsonWebKey
		RsaJsonWebKey rsaJsonWebKey;
		
		try {
			rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
			rsaJsonWebKey.setKeyId(getMiliseconds());
			rsaJsonWebKey.setAlgorithm("RS256");
			jwks.addJsonWebKey(rsaJsonWebKey);
			String privateKey =  Base64.encode(rsaJsonWebKey.getPrivateKey().getEncoded());
			String pubKey =  jwks.toJson();
			
			saveKey(context, pubKey, privateKey);
			
			isSuccess = true;
		} catch (JoseException e) {
			e.printStackTrace();
			isSuccess = false;
		}
		
		return isSuccess;
	}

	public static String getMiliseconds(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date todayDate = new Date();
		String thisDate = sdf.format(todayDate);
		try
		{
			Date mDate = sdf.parse(thisDate);
			long timeInMilliseconds = mDate.getTime();
			System.out.println("Date in milli :: " + timeInMilliseconds);
			return String.valueOf(timeInMilliseconds);
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}

		return "";
	}

	public static String getPrivateKeyFromSharedPref(Context context){
		SharedPreferences sharedpreferences = context.getSharedPreferences("keyPref", Context.MODE_PRIVATE);
		
		return sharedpreferences.getString("PrivateKey", null);
	}

	private static void saveKey(Context context,String pubKeyStr,String privKeyStr){
		SharedPreferences sharedpreferences = context.getSharedPreferences("keyPref", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedpreferences.edit();
		editor.putString("PublicKey", pubKeyStr);
		editor.putString("PrivateKey", privKeyStr);   
		editor.commit();
	}
}