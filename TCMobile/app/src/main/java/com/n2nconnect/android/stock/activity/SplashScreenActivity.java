package com.n2nconnect.android.stock.activity;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.net.URLDecoder;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.activity.CheckPlistTask.CheckPlistTaskFinishedListener;
import com.n2nconnect.android.stock.model.BrokerPlistConfigBean;
import com.n2nconnect.android.stock.util.PlistFileUtil;
import com.n2nconnect.android.stock.util.RootCheckUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class SplashScreenActivity extends Activity implements CheckPlistTaskFinishedListener {
	
	private ProgressDialog dialog;
	private String appDataPath;
	private String plistFileName;
	public static String PACKAGE_NAME;
	public static String plistPath;
	public static String externalImgPath;
	public static String currentVersion;
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;
	private boolean hasTutorialShown	= false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		System.out.println("SplashScreenActivity onCreate()");
		setContentView(R.layout.splash_screen);

		sharedPreferences 						= this.getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		SharedPreferences.Editor editor 		= sharedPreferences.edit();
		
		StockApplication stockApplication = (StockApplication) this.getApplication();
		
		//Added by Thinzar, Change_Request-20160722, ReqNo.8 - START
		if(this.getIntent() != null){
			Intent i = getIntent();
		
		    String action = i.getAction();
		    Uri data = i.getData();
		    
		    DefinitionConstants.Debug("Received implicit intent: action=" + action);
		    
		    if(data != null){
		    	
		    	try {
		    		String dataString		= URLDecoder.decode(data.toString(), AppConstants.STRING_ENCODING_FORMAT);
		    		DefinitionConstants.Debug("data=" + data.toString());
		    		
		    		//Change_Request-20170601, ReqNo.8
		    		if(dataString.indexOf("sgxtrademobile/buysell") != -1){
		    			DefinitionConstants.Debug("received intent from SGX");
		    			stockApplication.setExternalIntentUri(dataString);
		    		}else{
		    			//decrypted intent from iBillionaire
			    		String[] arrDataString	= dataString.split(Pattern.quote("://"));
						String decryptedData	= TheScreenerActivity.decrypt(AppConstants.iBillionaireSecretkey, arrDataString[1]);
						
						DefinitionConstants.Debug("iBillionaire decrypted data: " + decryptedData);
						
						stockApplication.setExternalIntentUri(decryptedData);
		    		}
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }
		}
		//Added by Thinzar, Change_Request-20160722, ReqNo.8 - END
		
		dialog				= new ProgressDialog(this);
		PACKAGE_NAME 		= getApplicationContext().getPackageName();
		appDataPath 		= getFilesDir().getPath();		// data/data/PACKAGE_NAME/files
		plistPath 			= appDataPath + "/" + PlistFileUtil.PLIST_FOLDER_NAME;
		externalImgPath 	= appDataPath + "/" + PlistFileUtil.ICONS_FOLDER_NAME;
		plistFileName 		= PlistFileUtil.getPlistFileNameFormat(PACKAGE_NAME); // returns config_com.n2nconnect.iMSL.stock.plist
		
		try {
			currentVersion 		= getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		// prepare plist and icons folders in external directory
		plistPath = appDataPath + "/" + PlistFileUtil.PLIST_FOLDER_NAME;
		File plistPathFile = new File(plistPath);
		if (!plistPathFile.exists())
			plistPathFile.mkdirs();

		externalImgPath = appDataPath + "/" + PlistFileUtil.ICONS_FOLDER_NAME;
		File extImgPathFile = new File(externalImgPath);
		if (!extImgPathFile.exists())
			extImgPathFile.mkdirs();
		
		
		boolean isAppExit = sharedPreferences.getBoolean(PreferenceConstants.IS_APP_EXIT, true);
		
		if (!isAppExit) {
			
			if (stockApplication.getExchangeInfos() == null) {
				editor = sharedPreferences.edit();
				editor.putBoolean(PreferenceConstants.IS_APP_EXIT, true);
				editor.commit();
				
				startCheckPlistTask(SplashScreenActivity.this);
			} else{
				Intent intent = new Intent();
				intent.putExtra(ParamConstants.FROM_LOGIN_TAG, true);
				intent.setClass(this, QuoteActivity.class);
				this.startActivity(intent);
				finish();
			}
		} else{
			startCheckPlistTask(SplashScreenActivity.this);
		}
	}
	
	public void startCheckPlistTask(Activity activity){
		String[] parameters = {PACKAGE_NAME, this.getPlistFileVersion(PACKAGE_NAME), ParamConstants.SPLASH_ACTIVITY_TAG};
		new CheckPlistTask(activity, dialog, this).execute(parameters);
	}
	
	// This is the callback for when async task has finished
    @Override
	public void onTaskFinished() {
    
    	sharedPreferences 						= getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		SharedPreferences.Editor editor 		= sharedPreferences.edit();
    	
		String result	= PlistFileUtil.checkMinVersionAndIsMultiLogin(SplashScreenActivity.this, plistPath, plistFileName, currentVersion);
		
		hasTutorialShown = sharedPreferences.getBoolean(PreferenceConstants.HAS_TUTORIAL_SHOWN, false);
		
		prepareBrokerConfig(PACKAGE_NAME);

		//Change_Request-20170601, ReqNo.20
		if(AppConstants.brokerConfigBean.isBlockRootedDevices()) {
			RootCheckUtil rootCheckUtil = new RootCheckUtil(SplashScreenActivity.this);
			boolean isDeviceRooted = rootCheckUtil.isRootedWithoutBusyBoxCheck();    //onePlus and MotoE devices have busybox binary present on the stock rom
			if (isDeviceRooted) {
				//logRootedDevice(rootCheckUtil.getErrorMsg());
				showAlertAndExitApp(ErrorCodeConstants.ROOT_DETECTED + " " + rootCheckUtil.getErrorMsg() + ".");
				return;
			}
		}
	
		if(result.equals(ParamConstants.MIN_VERSION_NOT_MET_TAG)){
			displayMinVersionMsg();
			
		} else {
			if(AppConstants.brokerConfigBean.isTutorialAvailable()==true && !hasTutorialShown){

				editor.putBoolean(PreferenceConstants.HAS_TUTORIAL_SHOWN, true);
				editor.commit();

				Intent i = new Intent();
				i.putExtra(ParamConstants.LOGIN_TYPE_TAG, result);
				i.setClass(SplashScreenActivity.this, TutorialActivity.class);
				startActivity(i);
				finish();
				
			} else{
			
				if(result.equals(ParamConstants.MULTI_LOGIN_TAG)){
					AppConstants.isAppMultilogin	= true;	//Change_Request-20160722, ReqNo. 6
					
					editor.putBoolean(PreferenceConstants.HAS_MULTI_LOGIN, true);
					editor.commit();
					showBrokerHouseListScreen(PACKAGE_NAME);
						
					finish();
						
				} else if(result.equals(ParamConstants.SINGLE_LOGIN_TAG)){
					AppConstants.isAppMultilogin	= false;	//Change_Request-20160722, ReqNo. 6
					
					AppConstants.CURRENT_PKG_NAME	= PACKAGE_NAME;
					showLoginScreen(PACKAGE_NAME);
							
					finish();
				}
			}
		}
    }
        
    public void showBrokerHouseListScreen(String mPackageName){
		
		Intent intent = new Intent();
		intent.putExtra(ParamConstants.PARAM_PACKAGE_NAME, mPackageName);
		intent.setClass(this, SelectBrokerHouseActivity.class);
		startActivity(intent);
		finish();
	}
    
    public void showLoginScreen(String mPackageName){
    	Intent intent = new Intent();
		intent.putExtra(ParamConstants.PARAM_PACKAGE_NAME, mPackageName);
		intent.setClass(this, LoginActivity.class);
		startActivity(intent);
		finish();
    }
    
    private String getPlistFileVersion(String pkgName){
		String mVersion;
		sharedPreferences 						= getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
		mVersion = sharedPreferences.getString(pkgName, "0");
		
		//to prevent file from being deleted by user
		if(!mVersion.equals("0")){
			String filename = PlistFileUtil.getPlistFileNameFormat(pkgName);
			File file = new File(plistPath, filename);
			
			if(!file.exists())	mVersion = "0";
		}
		
		return mVersion;
	}
    
    //Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 9 - START
    public void displayMinVersionMsg(){

    	//alert and close application
    	new AlertDialog.Builder(this)
	    	.setTitle(getResources().getString(R.string.splash_version_outdate_title))
	    	.setMessage(getResources().getString(R.string.splash_version_outdate_msg))
	    	.setPositiveButton(getResources().getString(R.string.splash_update_btn),
	    			new DialogInterface.OnClickListener() {
	    		public void onClick(DialogInterface dialog,
	    				int which) {
	    			final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
	    			try {
	    				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
	    			} catch (android.content.ActivityNotFoundException anfe) {
	    				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
	    			}
	    			SplashScreenActivity.this.finish();
	    		}
	    	})
	    	.setCancelable(false)
	    	.setNegativeButton(getResources().getString(R.string.splash_close_btn),
	    			new DialogInterface.OnClickListener() {
	    		public void onClick(DialogInterface dialog,
	    				int which) {
	    			finish();
	    		}
	    	}
	    	).create().show();
    } 
  
    public void prepareBrokerConfig(String mPackageName) {
    	SharedPreferences sharedPreferences 	= getSharedPreferences(AppConstants.DATA_STORE, MODE_PRIVATE);
    	SharedPreferences.Editor editor 		= sharedPreferences.edit();
    	editor.putString(PreferenceConstants.CURRENT_PACKAGE_NAME, mPackageName);
    	editor.commit();

    	//TODO further research what does the following do, i'm just following executeBrokerConfig
    	AppConstants.setEncryptedKey(null);
    	AppConstants.userParamInByte	= null;
    	AppConstants.userParam			= null;

    	try{

    		if (AppConstants.brokerConfigBean != null) {
    			AppConstants.brokerConfigBean = null;
    		} 

    		AppConstants.brokerConfigBean = BrokerPlistConfigBean.class.newInstance();
    		AppConstants.brokerConfigBean.initialize(mPackageName, this.getApplicationContext());

    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    //Added by Thinzar@20141023, Change_Request-20140901, ReqNo. 9 - END

	public void showAlertAndExitApp(String errorMsg){
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(errorMsg)
					.setCancelable(false)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							SplashScreenActivity.this.finish();
						}
					});

			AlertDialog alert = builder.create();
			if( ! this.isFinishing() )
				alert.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void logRootedDevice(String status){
		Answers.getInstance().logCustom(new CustomEvent("RootedDevice")
				.putCustomAttribute("status", status));
	}
}