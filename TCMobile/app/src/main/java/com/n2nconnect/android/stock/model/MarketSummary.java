package com.n2nconnect.android.stock.model;

public class MarketSummary extends StockSymbol {
	private String exchangeCode;
	private int upCounter;
	private int downCounter;
	private int unchangeCounter;
	private int untradedCounter;
	private int totalCounter;
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
	private int intLotSize;
	
	
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12 - START
	public int getIntLotSize() {
		return intLotSize;
	}

	public void setIntLotSize(int intLotSize) {
		/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
		this.intLotSize = intLotSize == 0 ? 1 : intLotSize;
		*/
		this.intLotSize = intLotSize;
	}
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12 - END
	
	
	public String getExchangeCode() {
		return exchangeCode;
	}

	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}

	public int getUpCounter() {
		return upCounter;
	}

	public void setUpCounter(int upCounter) {
		this.upCounter = upCounter;
	}

	public int getDownCounter() {
		return downCounter;
	}

	public void setDownCounter(int downCounter) {
		this.downCounter = downCounter;
	}

	public int getUnchangeCounter() {
		return unchangeCounter;
	}

	public void setUnchangeCounter(int unchangeCounter) {
		this.unchangeCounter = unchangeCounter;
	}

	public int getUntradedCounter() {
		return untradedCounter;
	}

	public void setUntradedCounter(int untradedCounter) {
		this.untradedCounter = untradedCounter;
	}

	public int getTotalCounter() {
		return totalCounter;
	}

	public void setTotalCounter(int totalCounter) {
		this.totalCounter = totalCounter;
	}

	
}
