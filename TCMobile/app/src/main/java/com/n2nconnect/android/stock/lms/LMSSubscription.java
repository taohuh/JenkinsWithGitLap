package com.n2nconnect.android.stock.lms;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLHandshakeException;

import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.util.SystemUtil;

public class LMSSubscription {

	private ArrayList<LMSServiceBean> alSubscriptionServices;
	private boolean bSubSuccess = false;
	private String sSubAuthFailMsg = "";
	
	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
	private String strServer 	= "http://beacon-uat.getmsl.com/lmstemp/websub/authen";
	private String strSponsor	= "9";
	private String strProduct	= "TCiPhone";
	// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END

	
	/* Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	public LMSSubscription() {
	 */
	public LMSSubscription(String strServer, String strSponsor, String strProduct){
		alSubscriptionServices 	= new ArrayList<LMSServiceBean>();
		// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
		this.strServer			= strServer;
		this.strSponsor			= strSponsor;
		this.strProduct 		= strProduct;
		// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
	}
	
	/* Mary@20130823 - Fixes_Request-20130711, ReqNo.16 - START
	public String authenLMSServices(String sUsername) throws FailedAuthenicationException {
	*/
	public String authenLMSServices(String sUsername) throws Exception{
		URL url;
		HttpURLConnection urlConn;
		InputStream is = null;
		InputStreamReader isr = null;
		OutputStreamWriter os = null;
		int ch;
		int iResponseCode;
		String sResponseMsg;
		StringBuffer sbData = new StringBuffer();

		/* Mary@20120814 - Change_Request-20120719, ReqNo.4
		String sServer = "http://beacon-uat.getmsl.com/lmstemp/websub/authen";
		*/
//		String sServer = "http://lms.asiaebroker.com/lms/websub/authen";
		//Previous
//		String sServer = "http://lms-cimbuat.asiaebroker.com/lmscimbtemp/websub/authen";
		
		// sponsor -> [CIMB = 9; TA = 23; Apex = 2; Hwang DB = 12; InterPac = 13; AmSec = 5;]
		/* Mary@20120814 - Change_Request-20120719, ReqNo.4
		String sSponsor = "9"; 
		*/
//		String sSponsor = "36";
//		String sSponsor = "25";
//		non Trading
//		String sSponsor = "43";
//		String sSponsor = "13";
		
		// product -> [ CIMB = TCiPhone; TA, Apex, Hwang DB, InterPac = TCAndroid; ]
		/* Mary@20120814 - Change_Request-20120719, ReqNo.4
		String sProduct = "TCiPhone";
		*/ 
		//strProduct = "TCiPhone";	//TODO: for PSE debugging, remove later 
		String sParams = "identifier="+sUsername+"&sponsor="+strSponsor+"&prod="+strProduct;
		DefinitionConstants.Debug("LoginLMS(): " + strServer + sParams);
		
		try {
			url = new URL(strServer);
			urlConn = (HttpURLConnection) url.openConnection();
			urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			urlConn.setUseCaches(false);
			urlConn.setDoOutput(true);
			urlConn.setDoInput(true);
			urlConn.setRequestMethod("POST");
			os = new OutputStreamWriter(urlConn.getOutputStream());
			os.write(sParams);
			os.flush();
			iResponseCode = urlConn.getResponseCode();
			sResponseMsg = urlConn.getResponseMessage();
			if (iResponseCode == 200) {
				is = urlConn.getInputStream();
				isr = new InputStreamReader(is);
				while ((ch = isr.read()) != -1) {
					sbData.append((char) ch);
				}
				
				DefinitionConstants.Debug("sbData: "+sbData.toString());
				
				return sbData.toString();
			} else if(iResponseCode == 401) {
				return "invalid client";
			} else {
				//throw new Exception("sendHttpByPost:HTTP code:" + iResponseCode + " Msg:" + sResponseMsg);
				throw new Exception(ErrorCodeConstants.FAIL_PROCESS_LMS);	//Change_Request-20160101, ReqNo.2
			}
		/* Mary@20130823 - Fixes_Request-20130711, ReqNo.16 - START
		}catch (Exception e){
			e.printStackTrace();
			throw new FailedAuthenicationException("Network is unreachable");
		*/
		}catch(NullPointerException npe){
			npe.printStackTrace();
			return sbData.toString();
		}catch(EOFException eof){
			eof.printStackTrace();
			throw new EOFException(ErrorCodeConstants.FAIL_COMMUNICATION_EOF);
		}catch(SSLHandshakeException she){
			she.printStackTrace();
			if( ErrorCodeConstants.isExtCertPathValidatorException( she.getMessage() ) )
				throw new Exception(ErrorCodeConstants.OS_CAUSE_INVALID_CERT);
			else
				throw she;
		}catch (IOException ioe){
			ioe.printStackTrace();
			throw new IOException(ErrorCodeConstants.FAIL_COMMUNICATION_IOE);
		}catch (Exception e){
			e.printStackTrace();
			throw e;
		// Mary@20130823 - Fixes_Request-20130711, ReqNo.16 - END
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				is = null;	
			}
			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				isr = null;
			}
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				os = null;	
			}
		}
    }
	
    public boolean isAuthenSuccess(String sData) {
        boolean bSuccess = false;
        
		String BeginData = "--_BeginData_";
		String EndData = "--_EndData_";

        int startIdx = -1;
        int endIdx = -1;
        try {
            startIdx = sData.indexOf(BeginData) + BeginData.length();
            endIdx = sData.indexOf(EndData);
        } catch (Exception e) {
        }

        if (startIdx != -1 && endIdx != -1) {
            String sSubData = sData.substring(startIdx, endIdx);
            sSubData = sSubData.trim();
            String[] aStatus = sSubData.split("\\|");
            if (aStatus.length > 0) {
                if ("0".equalsIgnoreCase(aStatus[0])) {
                    bSuccess = true;
                } else {
                    bSuccess = false;
                    if (aStatus.length > 1) sSubAuthFailMsg = aStatus[1];
                }
            }
        }

        return bSuccess;
    }
    
    //Added by Thinzar, Change_Request-20140801, ReqNo. 3 - START
    /*
     * sample features string
      	--_BeginFeatures_
		DLD-KL|Bw==|
		DLD-JK|Bw==|
		DLD-SG|Bw==|
		PLC-KL|Bw==|
		PLC-SG|Bw==|
		PLC-JK|Bw==|
		TCProFeatures|AAAAAAgAPA==|
		MD-KL-05|Bw==|
		MD-KL-10|Bw==|
		--_EndFeatures_
     */
    public HashMap<String, Integer> processSubFeaturesGetMktDepth(String sData){
    	String strBeginFeatures = "--_BeginFeatures_";
    	String strEndFeatures = "--_EndFeatures_";
    	HashMap<String, Integer> marketDepthInfos = new HashMap<String, Integer>();
    	
    	int startIdx = -1;
    	int endIdx = -1;
    	
    	try{
    		startIdx = sData.indexOf(strBeginFeatures) + strBeginFeatures.length();
    		endIdx = sData.indexOf(strEndFeatures);
    	} catch(Exception ex){}
    	
    	if(startIdx != -1 && endIdx != -1){
    		String sSubData = sData.substring(startIdx, endIdx);
    		sSubData = sSubData.trim();
    		if(sSubData.length()>0){
	    		String[] subs = sSubData.split("\r\n");		//e.g., MD-KL-10|AQ==|
	    		
	    		for(String str : subs){
		    		String[] es = str.split("\\|");
		    		if(es.length > 0){	
		    			String data = es[0];					//e.g., MD-KL-10
		    			String accessBitBase64 = es[1];			//e.g., AQ==
		    			
		    			try {
							if(SystemUtil.isBitOn_Base64(accessBitBase64, 0)){	//check again why bitIdx is zero? 
								if(data.startsWith("MD")){
									String[] dataArray = data.split("-");
									String exchg = dataArray[1];
									Integer depth = new Integer(dataArray[2]);
									Integer oldDepth = marketDepthInfos.get(exchg);
									
									if(oldDepth == null || depth > oldDepth){
										marketDepthInfos.put(exchg, depth);
									} 
								}
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
		    		}
	    		}
    		}
    	}
    	
    	return marketDepthInfos;
    }
    //Added by Thinzar, Change_Request-20140801, ReqNo. 3 - END
    
    public void processSubServices(String sData) {
    	//System.out.println("LMS:"+sData);
		String BeginAllService = "--_BeginAllService_";
		String EndAllService = "--_EndAllService_";
    	int startIdx = -1;
        int endIdx = -1;
        try {
            startIdx = sData.indexOf(BeginAllService) + BeginAllService.length();
            endIdx = sData.indexOf(EndAllService);
        } catch (Exception e) {}

        if (startIdx != -1 && endIdx != -1) {
            String sSubData = sData.substring(startIdx, endIdx);
            sSubData = sSubData.trim();

            String[] subs = sSubData.split("\r\n");
            for (String string : subs) {
            	/* Mary@20130711 - Fixes_Request-20130711, ReqNo.1
                LMSServiceBean oService = new LMSServiceBean();
                */
            	LMSServiceBean oService = null;
            	
                String[] es = string.split("\\|");
               //System.out.println("LMS LENGTH:"+es.length);//18// e.g. P64|60|02|M'sia Derivatives|Bursa Derivative|R|MY|RM|1|5|-|-|-|-|0|1|2:218.100.22.117:20000:80:|MY|
                for (int i = 0; i < es.length; i++) {
                    String value = es[i];
                    
                    // Added by Mary@20130711 - Fixes_Request-20130711, ReqNo.1 - START
                	if(oService == null){
                		if(i==0 && value != null && value.trim().length() > 0)
                			oService = new LMSServiceBean();
                		else
                			break;
                	}
                	// Added by Mary@20130711 - Fixes_Request-20130711, ReqNo.1 - END
                	
                	// Added by Mary@20130729 - GooglePlayReport-20130729, CrashNo.1
                	value = value != null ? value.trim() : value;
                	
                    switch(i) {
                        case 0: oService.setProductID(value);break;
                        case 1: oService.setCountryCode(value);break;
                        case 2: oService.setExchangeID(value);break;
                        case 3: oService.setExchangeName(value);break;
                        case 4: oService.setExchangeDescription(value);break;
                    	// Added by Kw@20130222 - Change_Request20130104, ReqNo.6
                        case 5: oService.setExchangeType(value);break;
                        case 6: oService.setExchangeCode(value);break;
                        case 7: oService.setCurrency(value);break;
                        case 8: oService.setBitMode(value);break;
                        /* Mary@20130729 - GooglePlayReport-20130729, CrashNo.1 - START
                        case 9: oService.setMarketDepthLvl(Integer.parseInt(value.trim()));break;//oService.setAppName(value);
                        */
                        case 9: 
                        	try{
	                        	oService.setMarketDepthLvl( Integer.parseInt(value) );
                        	}catch(Exception e){
                        		oService.setMarketDepthLvl(1);	//edited by Thinzar, set default as 1, if market depth returns null
                        	}
                        	break;//oService.setAppName(value);
                        // Mary@20130729 - GooglePlayReport-20130729, CrashNo.1 - END
                        case 10: oService.setExpiryDate(value);break;
                        case 11: oService.setExpiryDays(value);break;
                        case 12: oService.setSubscriptionFlag(value);break;
                        case 16: oService.setFeedURL(value);break;
                        case 17: oService.setTradeExchangeCode(value);break;
                        // Added by Mary@20130620 - Change_Request-20130424, ReqNo.9
                        // P16|60|01|M'sia Equities|Bursa Equity|R|KL|RM|1|5|-|-|-|-|0|1|0:49.236.201.87:20000:80:|KL|Pw==|
                        case 18 : oService.setStrFCBitMode(value == null ? "" : value); break;
                        default: ;
                    }
                }

                // Added by Mary@20130711 - Fixes_Request-20130711, ReqNo.1
                if(oService != null)
                	alSubscriptionServices.add(oService);
            }
        }
    }

    public ArrayList<LMSServiceBean> getSubscriptions() {
        return alSubscriptionServices;
    }

    public LMSServiceBean getServiceByExchange(String sExchange) {
        LMSServiceBean oService = null;
        for (LMSServiceBean lmsServiceBean : alSubscriptionServices) {
            if(sExchange.equalsIgnoreCase(lmsServiceBean.getExchangeCode())) {
                oService = lmsServiceBean;
                break;
            }
        }
        return oService;
    }

    public String getSubsAuthFailMsg() {
        return sSubAuthFailMsg;
    }

}
