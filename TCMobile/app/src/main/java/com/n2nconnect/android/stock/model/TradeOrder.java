package com.n2nconnect.android.stock.model;

import java.util.Comparator;
import java.util.Date;

public class TradeOrder {
	
	private String orderNo;
	private String fixedNo;
	private String action;
	private String statusCode;
	private String statusText;
	private long quantity;
	private float price;
	private float stopLimit;
	private double value;
	private String orderType;
	private String validity;
	private long unmatchQuantity;
	private long matchedQuantity;
	// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.5
	private long cancelledQuantity;
	private float matchedPrice;
	private double matchedValue;
	private String orderSource;
	private Date orderDate;
	private String accountNo;
	private String remark;
	private String symbolCode;
	private String stockCode;
	private boolean isExpand; 
	private Date expiryDate;
	private String paymentType;
	private String strSettlementCurrency;
	private String lastUpdateTimeStamp;
	private String orderTicket;
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12
	private int intLotSize;
	
	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
	private long lngDisclosedQty;
	private long lngMinQty;
	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
	
	// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15
	private String strStockCurrency;
	
	//Added by Thinzar@20140819, Change_Request-20140801, ReqNo4
	private String triggerPriceType;
	private String triggerPriceDirection;
	
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12 - START
	public int getIntLotSize() {
		return intLotSize;
	}

	public void setIntLotSize(int intLotSize) {
		/* Mary@20130314 - Fixes_Request-20130314, ReqNo.2
		this.intLotSize = intLotSize == 0 ? 1 : intLotSize;
		*/
		this.intLotSize = intLotSize;
	}
	// Added by Mary@20120911 - Change_Request-20120719, ReqNo.12 - END
	
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getFixedNo() {
		return fixedNo;
	}
	public void setFixedNo(String fixedNo) {
		this.fixedNo = fixedNo;
	}
	public boolean isExpand() {
		return isExpand;
	}
	public void setExpand(boolean isExpand) {
		this.isExpand = isExpand;
	}
	public String getSymbolCode() {
		return symbolCode;
	}
	public void setSymbolCode(String symbolCode) {
		this.symbolCode = symbolCode;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusText() {
		return statusText;
	}
	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	
	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
	public long getDisclosedQuantity() {
		return lngDisclosedQty;
	}
	public void setDisclosedQuantity(long lngDisclosedQty) {
		this.lngDisclosedQty = lngDisclosedQty;
	}
	
	public long getMinimumQuantity() {
		return lngMinQty;
	}
	public void setMinimumQuantity(long lngMinQty) {
		this.lngMinQty = lngMinQty;
	}
	// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
	
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public long getUnmatchQuantity() {
		return unmatchQuantity;
	}
	public void setUnmatchQuantity(long unmatchQuantity) {
		this.unmatchQuantity = unmatchQuantity;
	}
	public long getMatchedQuantity() {
		return matchedQuantity;
	}
	public void setMatchedQuantity(long matchedQuantity) {
		this.matchedQuantity = matchedQuantity;
	}
	// Added by Mary@20120806 - Fixes_Request, ReqNo.5 - START
	public long getCancelledQuantity() {
		return cancelledQuantity;
	}
	public void setCancelledQuantity(long cancelledQuantity) {
		this.cancelledQuantity = cancelledQuantity;
	}
	// Added by Mary@20120806 - Fixes_Request, ReqNo.5 - END
	public float getMatchedPrice() {
		return matchedPrice;
	}
	public void setMatchedPrice(float matchedPrice) {
		this.matchedPrice = matchedPrice;
	}
	public double getMatchedValue() {
		return matchedValue;
	}
	public void setMatchedValue(double matchedValue) {
		this.matchedValue = matchedValue;
	}
	public String getOrderSource() {
		return orderSource;
	}
	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}
	public String getPaymentType(){
		return paymentType;
	}
	public void setPaymentType(String paymentType){
		this.paymentType = paymentType;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public void setLastUpdate(String timeStamp){
		this.lastUpdateTimeStamp = timeStamp;
	}
	public String getLastUpdate(){
		return lastUpdateTimeStamp;
	}
	public void setStopLimit(float stopLimit){
		this.stopLimit = stopLimit;
	}
	public float getStopLimit(){
		return stopLimit;
	}
	public void setOrderTicket(String orderTicket){
		this.orderTicket = orderTicket;
	}
	public String getOrderTicket(){
		return orderTicket;
	}
	
	// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15 - START
	public String getStrSettlementCurrency() {
		return strSettlementCurrency;
	}
	public void setStrSettlementCurrency(String strSettlementCurrency) {
		this.strSettlementCurrency = strSettlementCurrency;
	}

	public String getStrStockCurrency() {
		return strStockCurrency;
	}
	public void setStrStockCurrency(String strStockCurrency) {
		this.strStockCurrency = strStockCurrency;
	}
	// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15 - END
	
	//Added by Thinzar@20140819, Change_Request-20140801, ReqNo4 - START
	public String getTriggerPriceType() {
		return triggerPriceType;
	}

	public void setTriggerPriceType(String triggerPriceType) {
		this.triggerPriceType = triggerPriceType;
	}

	public String getTriggerPriceDirection() {
		return triggerPriceDirection;
	}

	public void setTriggerPriceDirection(String triggerPriceDirection) {
		this.triggerPriceDirection = triggerPriceDirection;
	}
	//Added by Thinzar@20140819, Change_Request-20140801, ReqNo4 - END
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.5 - START
	public static class orderByStockCodeASC implements Comparator<TradeOrder> {

        @Override
        public int compare(TradeOrder o1, TradeOrder o2) {
            return o1.stockCode.compareTo(o2.stockCode);
        }
    }
	
	public static class orderByStockCodeDESC implements Comparator<TradeOrder> {

        @Override
        public int compare(TradeOrder o1, TradeOrder o2) {
            return o2.stockCode.compareTo(o1.stockCode);
        }
    }
	
	public static class orderByLastUpdateDESC implements Comparator<TradeOrder> {

        @Override
        public int compare(TradeOrder o1, TradeOrder o2) {
        	
        	long lastUpdate1	= parseTimeStampToLong(o1.lastUpdateTimeStamp);
        	long lastUpdate2	= parseTimeStampToLong(o2.lastUpdateTimeStamp);
        	
            return lastUpdate2 > lastUpdate1 ? 1 : (lastUpdate2 < lastUpdate1 ? -1 : 0);
        }
    }
	
	public static class orderByLastUpdateASC implements Comparator<TradeOrder> {

        @Override
        public int compare(TradeOrder o1, TradeOrder o2) {
        	
        	long lastUpdate1	= parseTimeStampToLong(o1.lastUpdateTimeStamp);
        	long lastUpdate2	= parseTimeStampToLong(o2.lastUpdateTimeStamp);
        	
            return lastUpdate1 > lastUpdate2 ? 1 : (lastUpdate1 < lastUpdate2 ? -1 : 0);
        }
    }
	
	public static long parseTimeStampToLong(String strTimeStamp){
		String arr[] 		= strTimeStamp.split("\\.");
		String newTimeStamp = arr[0] + arr[1];
		return Long.parseLong(newTimeStamp);
	}
	//Added by Thinzar, Change_Request-20160101, ReqNo.5 - END
}
