package com.n2nconnect.android.stock.activity;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.util.FileDownloader;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class ElasticNewsDetailsActivity extends CustomWindow implements OnDoubleLoginListener{
	private DoubleLoginThread loginThread;
	
	private WebView newsWebView;
	private ProgressDialog dialog;
	
	private String newsSource 	= "";
	private String newsId		= "";
	private String url;

	private final String REPORTS_DIR = "CIMBResearchReports";
	private final int pdfIntentRequestCode = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.title.setText(getResources().getString(R.string.elastic_news_module_title));
		this.icon.setImageResource(R.drawable.selector_back_button);		
		this.icon.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				ElasticNewsDetailsActivity.this.finish();
			}
		});
		this.menuButton.setVisibility(View.INVISIBLE);

		setContentView(R.layout.news_web_view);
		
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			newsSource 	= extras.getString(ParamConstants.NEWS_SOURCE_TAG);
			newsId 		= extras.getString(ParamConstants.NEWSID_TAG);
		}
		
		newsWebView = (WebView)findViewById(R.id.newsWebView);
		dialog		= new ProgressDialog(this);
		
		if(newsSource!= null && newsSource.equalsIgnoreCase(AppConstants.RESEARCH_REPORTS_SOURCE)){
			//TODO: check for run time permission here if required in the future

			url = getResearchReportUrl(newsId);
			String fileName = newsId + ".pdf";

			DefinitionConstants.Debug("Research report URL: " + url);
			new DownloadFile().execute(url, fileName);

		}else{
			String brokerCode	= sharedPreferences.getString(PreferenceConstants.BROKER_CODE, "");
			url = getElasticNewsUrl(newsId, newsSource, brokerCode);

			DefinitionConstants.Debug("newsSource=" + newsSource + ", Elastic News Url=" + url);

			newsWebView.getSettings().setJavaScriptEnabled(true);
			newsWebView.getSettings().setDomStorageEnabled(true);	//this news page using Dom Storage specifically
			newsWebView.setWebViewClient(new WebViewController());

			newsWebView.setWebChromeClient(new WebChromeClient(){

				@Override
				public void onProgressChanged(WebView view, int newProgress) {
					if( ! ElasticNewsDetailsActivity.this.isFinishing() ){
						dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
						dialog.show();
					}

					if(newProgress == 100 && dialog.isShowing())
						dialog.dismiss();
				}
			});

			newsWebView.loadUrl(url);
		}
	}
	
	public class WebViewController extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, final String url) {

			//NOTE: this is to open pdf external links
			if (url.contains("pdf.reuters.com") || url.contains("play.google.com")) {
				try {
					AlertDialog dialog = new AlertDialog.Builder(ElasticNewsDetailsActivity.this)
							.setMessage(getResources().getString(R.string.news_external_web_warning))
							.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener(){

								@Override
								public void onClick(DialogInterface dialog, int which) {
									Uri uriUrl = Uri.parse(url);
									Intent intentUrl = new Intent(Intent.ACTION_VIEW, uriUrl);
									startActivity(intentUrl);
								}
							})
							.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									//do nothing
								}
							})
							.create();
					dialog.show();

					return true;
				} catch (Exception e) {
					System.out.println(e);
					Toast.makeText(ElasticNewsDetailsActivity.this, getResources().getString(R.string.no_pdf_viewer_found), Toast.LENGTH_LONG).show();
				}
			} else{
				view.loadUrl(url);
			}

			return true;
        }
		
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

	    }
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (idleThread != null && idleThread.isPaused())
			idleThread.resumeRequest(false);
		
		loginThread = ((StockApplication) ElasticNewsDetailsActivity.this.getApplication()).getDoubleLoginThread();
		
		if (loginThread != null && loginThread.isPaused()) {
			loginThread.setRegisteredListener(ElasticNewsDetailsActivity.this);
			loginThread.resumeRequest();
		}
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message	= handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				
				if(loginThread != null)
					loginThread.stopRequest();
				
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				if( ElasticNewsDetailsActivity.this.mainMenu.isShowing() )
					ElasticNewsDetailsActivity.this.mainMenu.hide();
					
				if( !isFinishing() )
					ElasticNewsDetailsActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};
	
	@Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public String getResearchReportUrl(String reportId){
		//String researchReportUrl = "https://sgnewsstg.itradecimb.com/gcNEWS/srvs/displayNews?" + "id=" + reportId;
		String researchReportUrl = AppConstants.brokerConfigBean.getElasticResearchReportsUrl() + "id=" + reportId;

		/* note: google viewer has bandwidth limit + file size, we decide to change approach to download and view in external pdf viewer
		String googleDocsPdfViewer = "https://docs.google.com/gview?embedded=true&url=";
		return googleDocsPdfViewer + researchReportUrl;
		*/

		return researchReportUrl;
	}
	
	public String getElasticNewsUrl(String newsId, String newsSource, String brokerCode){
		String url;
		
		//https://sgnewsstg.itradecimb.com/gcNEWS/web/html/NewsDetails.html?t=7&nid=HK_urn_newsml_reuters.com_20170619_nHKS6vslQ6&ns=TRKD&ex=SG&spc=017&bh=065&appId=LP&rdt=P&ts=
		String baseUrl = AppConstants.brokerConfigBean.getElasticReuterNewsUrl();	//TODO: move this to plist
		url = baseUrl + "t=7&nid=" + newsId
			+ "&ns=" + newsSource
			+ "&ex=&spc="+ AppConstants.brokerConfigBean.getStrLMSSponsor()
			+ "&bh=" + brokerCode + "&appId=LP&rdt=P"
			+ "&ts=" + Long.toString(System.currentTimeMillis());
		
		return url;
	}

	public void viewFile(String filename){
		File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + REPORTS_DIR + "/" + filename);
		Uri path = Uri.fromFile(pdfFile);
		Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
		pdfIntent.setDataAndType(path, "application/pdf");
		pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		try{
			startActivityForResult(pdfIntent, pdfIntentRequestCode);
		}catch(ActivityNotFoundException e){
			Toast.makeText(ElasticNewsDetailsActivity.this, "No application to view pdf file", Toast.LENGTH_LONG).show();
		}
	}

	private class DownloadFile extends AsyncTask<String, Void, Exception> {
		private Exception ex = null;
		private String mFileUrl;
		private String mFileName;
		private boolean isFolderCreateSuccess 	= true;
		private boolean isFileCreateSuccess 	= true;
		private File pdfFile;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if(dialog != null && !ElasticNewsDetailsActivity.this.isFinishing()){
				dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				dialog.show();
			}
		}

		@Override
		protected Exception doInBackground(String... params) {
			mFileUrl = params[0];
			mFileName = params[1];

			String externalStorageDir = Environment.getExternalStorageDirectory().toString();
			File folder = new File(externalStorageDir, REPORTS_DIR);
			try{
				if(!folder.exists())
					isFolderCreateSuccess = folder.mkdir();

				if(isFolderCreateSuccess) {
					pdfFile = new File(folder, mFileName);

					if (!pdfFile.exists()) {
						//Delete all files in the folder whenever the count reaches 10
						if(folder.listFiles().length > 10){
							String[] children = folder.list();
							for (int i = 0; i < children.length; i++)
							{
								new File(folder, children[i]).delete();
							}
						}

						isFileCreateSuccess = pdfFile.createNewFile();
						FileDownloader.downloadFile(mFileUrl, pdfFile);
					}
				}

				if(!isFolderCreateSuccess || !isFileCreateSuccess)
					ex = new Exception(ErrorCodeConstants.FAIL_WRITE_EXTERNAL_STORAGE);

			}catch(IOException e){
				e.printStackTrace();
				ex = new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_IOE);
			}catch(Exception e){
				e.printStackTrace();
				ex = new Exception(ErrorCodeConstants.FAIL_COMMUNICATION_E);
			}

			return ex;
		}

		@Override
		protected void onPostExecute(Exception exception) {
			super.onPostExecute(exception);

			if(dialog.isShowing() && !ElasticNewsDetailsActivity.this.isFinishing())
				dialog.dismiss();

			if(exception == null){
				viewFile(mFileName);
			}else{
				//delete the empty file if exception raised
				if (pdfFile.exists())
					pdfFile.delete();

				SystemUtil.showExceptionAlert(exception, ElasticNewsDetailsActivity.this);
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		System.out.println("onActivityResult: requestCode=" + requestCode + ", result=" + resultCode);
		if(requestCode == pdfIntentRequestCode){
			ElasticNewsDetailsActivity.this.finish();
		}
	}
}