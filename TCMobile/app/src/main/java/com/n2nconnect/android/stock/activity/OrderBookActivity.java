package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.FailedAuthenicationException;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.TradeStatusThread;
import com.n2nconnect.android.stock.TradeStatusThread.onRefreshTradeStatusListener;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.ExchangeListView;
import com.n2nconnect.android.stock.custom.ExchangeListView.OnExchangListEventListener;
import com.n2nconnect.android.stock.custom.OrderFilterListView;
import com.n2nconnect.android.stock.custom.OrderFilterListView.OnOrderFilterSelectedListener;
import com.n2nconnect.android.stock.custom.PaymentTypeFilterListView;
import com.n2nconnect.android.stock.custom.PaymentTypeFilterListView.OnPayTypeFilterSelectedListener;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.Sector;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.model.TradeOrder;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class OrderBookActivity extends CustomWindow implements OnOrderFilterSelectedListener, OnRefreshStockListener, OnPayTypeFilterSelectedListener,
onRefreshTradeStatusListener, OnDoubleLoginListener, OnExchangListEventListener {

	private TradeAccount tradeAccount;
	private List<Button> quantityButtons;
	private boolean isDisplayStatus;
	private TextView headerLabel;

	private Button FilterOrder;
	private OrderFilterListView filterListView;
	private String strSelectedFilterOrderType;

	// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5
	private String strSelectedFilterExchangeType;
	private List<String> filter;

	private TradeStatusThread statusThread;
	private DoubleLoginThread loginThread;
	private String atpUserParam;
	private String senderCode;
	private ProgressDialog dialog;
	private boolean isExpired = false;
	private TextView clientNameLabel;
	private TextView accountNoLabel;
	private LinearLayout selectedRow;
	private int selectedId;
	private static boolean[] expandid;
	boolean isConstructing;

	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr = 0;
	private AlertDialog alertException;

	private boolean onStopThread;
	private static final String VALIDITY_GTD = "GTD";

	// Added by Mary@20130912 - Change_Request-20130424, ReqNo.12
	private static final String VALIDITY_GTM 						= "GTM";
	
	// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - START
	private Button btnExchangeType;
	private ExchangeListView exchgListView;
	private Map<String, Object> selectedExchangeMap;
	private String ALL_TYPE_KEY	= null;

	// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13
	private List<TradeOrder> filteredTradeOrders;
	
	//Added by Thinzar@20140730
	private Button btnPaymentType;
	private PaymentTypeFilterListView payFilterListView;
	private String strSelectedFilterPayType;
	private List<String> pymtTypes;
	
	//Added by Thinzar, Change_Request-20160101, ReqNo.5
	private ImageView imgSortName;
	private ImageView imgSortLastUpdated;
	private LinearLayout layoutName;
	private LinearLayout layoutLastUpdated;
	private ImageButton btnOrderHistory;
	
	private final int SORT_BY_NAME_ASC			= 1;
	private final int SORT_BY_NAME_DESC			= 2;
	private final int SORT_BY_LAST_UPDATED_ASC	= 3;
	private final int SORT_BY_LAST_UPDATED_DESC	= 4;
	private int currentSortByMethod		= SORT_BY_LAST_UPDATED_DESC;		//set default value
	
	//Change_Request-20160101, ReqNo.6 & 7
	private boolean isOrderHistory	= false;
	private String dateFrom;
	private String dateTo;
	private TextView txtOrderHistoryDetails;
	
	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.order_book);
			this.title.setText(getResources().getString(R.string.orderbook_module_title));
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntOrdBookMenuIconBgImg() );
			
			dialog = new ProgressDialog(OrderBookActivity.this);
			
			// Added by Mary@20130122 - Change_Request-20130104, ReqNo.5
			ALL_TYPE_KEY	= getResources().getString(R.string.all_type_key);
			
			FilterOrder		= (Button)findViewById(R.id.orderButton);

			if(DefinitionConstants.tradeOrders!=null)
				DefinitionConstants.tradeOrders=null;
	
			clientNameLabel	= (TextView) OrderBookActivity.this.findViewById(R.id.clientNameLabel);
			clientNameLabel.setText("-");
			
			accountNoLabel	= (TextView) OrderBookActivity.this.findViewById(R.id.accountNoLabel);
			accountNoLabel.setText("-");

			filterListView = new OrderFilterListView(this, this, getLayoutInflater());
	
			filter 			= new ArrayList<String>();
			filter.add(getResources().getString(R.string.orderbook_filter_all));
			filter.add(getResources().getString(R.string.orderbook_filter_open));
			filter.add(getResources().getString(R.string.orderbook_filter_filled));
			filter.add(getResources().getString(R.string.orderbook_filter_active));
			filter.add(getResources().getString(R.string.orderbook_filter_inactive));
	
			try {
				filterListView.setOrderFilterList(filter);
			} catch (Exception e) {
				e.printStackTrace();
			}
	
			FilterOrder.setOnClickListener(orderFilter);
			
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(OrderBookActivity.this)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130802 - Fixes_Request-20130523, ReqNo.6 - END
					}
				})
				.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - START
			btnExchangeType 		= (Button)findViewById(R.id.btnExchangeType);

			exchgListView 			= new ExchangeListView( OrderBookActivity.this, OrderBookActivity.this, getLayoutInflater(), null);
			exchgListView.setHideOnSelect(true);
			
			btnExchangeType.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					doExchangeList();
				}        	
			});
			// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - END
			
			//added by Thinzar@20140730, Change_Request-20140701, ReqNo.9 - START
			btnPaymentType			= (Button)findViewById(R.id.btnPaymentType);
			pymtTypes = ((StockApplication)OrderBookActivity.this.getApplication()).getPaymentTypes();
			if(pymtTypes.size() > 0){
				btnPaymentType.setVisibility(View.VISIBLE);

				payFilterListView = new PaymentTypeFilterListView(this, this, getLayoutInflater());
				try{
					payFilterListView.setPayTypeFilterList(pymtTypes);
				} catch(Exception e){
					e.printStackTrace();
				}
				btnPaymentType.setOnClickListener(paymentFilter);

			} else {
				strSelectedFilterPayType = null;
				btnPaymentType.setVisibility(View.GONE);
			}
			//added by Thinzar@20140730 - END (not finished modifying yet, continue from here)
			
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
		
		//Added by Thinzar, Change_Request-20160101, ReqNo.5
		imgSortName			= (ImageView)findViewById(R.id.imgSortName);
		imgSortLastUpdated	= (ImageView)findViewById(R.id.imgSortLastUpdated);
		layoutName			= (LinearLayout)findViewById(R.id.layoutName);
		layoutLastUpdated	= (LinearLayout)findViewById(R.id.layoutLastUpdated);
		btnOrderHistory		= (ImageButton)findViewById(R.id.imgBtnOrderHistory);
		txtOrderHistoryDetails	= (TextView) findViewById(R.id.txtOrderHistoryDetails);
		
		imgSortName.setVisibility(View.INVISIBLE); 		//default sort by last updated time
		txtOrderHistoryDetails.setVisibility(View.GONE); 	//hide the textView by default
	}
	
	@Override
	public void onResume() {
		DefinitionConstants.Debug("tradeDebug==OrderBookActivity, onResume()");
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onResume();
			if(!alertException.isShowing()){
				// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
				currContext	= OrderBookActivity.this;
				if( idleThread != null && idleThread.isPaused() )
					idleThread.resumeRequest(false);
				// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END

				loginThread = ((StockApplication)OrderBookActivity.this.getApplication()).getDoubleLoginThread();
				DefinitionConstants.Debug("onresume(), loginThread state:" + loginThread.getState());
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
				if(loginThread == null){
					if(AppConstants.hasLogout){
						finish();
					}else{
						loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
						if (AppConstants.brokerConfigBean.isEnableEncryption())
							loginThread.setUserParam(AppConstants.userParamInByte);
						loginThread.start();
						((StockApplication) OrderBookActivity.this.getApplication()).setDoubleLoginThread(loginThread);
					}
				// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
				}else if( loginThread.isPaused() ){
					loginThread.setRegisteredListener(OrderBookActivity.this);
					loginThread.resumeRequest();
				// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
				}
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
				loginThread.setRegisteredListener(OrderBookActivity.this);
				
				//Edited by Thinzar, Change_Request-20160101, ReqNo.7 - START
				if(!isOrderHistory){
					// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
					statusThread 	= ((StockApplication) OrderBookActivity.this.getApplication()).getTradeStatusThread();
					if( statusThread != null && statusThread.isPaused() ){
						
						statusThread.setRegisteredListener(OrderBookActivity.this);
						statusThread.resumeRequest();
					}
					
					// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
					if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
						new getClientLimit().execute();
					
					txtOrderHistoryDetails.setVisibility(View.GONE);
				} else{
					new GetTradeHistory().execute();
				}

				if(selectedRow != null && (filteredTradeOrders != null && filteredTradeOrders.size() > selectedId ) ){

					TradeOrder tradeOrder = filteredTradeOrders.get(selectedId);
					if( tradeOrder.isExpand() ){
						int childCount = selectedRow.getChildCount();
						if(childCount!=1)
							selectedRow.removeViewAt(1);
						
						tradeOrder.setExpand(false);
					}
				}
		
				isConstructing				= false;	//edited by Thinzar, Fixes_Request-20140820,ReqNo.27
				
				List<TradeAccount> Accounts = ((StockApplication) OrderBookActivity.this.getApplication()).getTradeAccounts();
				ImageView detailImage		= (ImageView) this.findViewById(R.id.detailViewImage);
				if(Accounts.size() <= 1)
					detailImage.setVisibility(View.GONE);
				
				detailImage.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

						if(OrderBookActivity.this.mainMenu.isShowing()){
							mainMenu.hide();
						}
		
						Intent intent = new Intent();
						intent.putExtra(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
						intent.putExtra(ParamConstants.FILTER_BY_EXCHG, false);
						intent.setClass(OrderBookActivity.this, SelectAccountActivity.class);
						OrderBookActivity.this.startActivityForResult(intent, AppConstants.SELECT_ACCOUNT_REQUEST);
		
					}
		
				});
				
				// Added by Mary@20130121 - Change_Request-20130104, ReqNo.5 - START
				List<Map<String, Object>> listExchangeMap	= new ArrayList<Map<String, Object>>();
				try{
					// All Exchange
					Map<String, Object> exchgMap		= new HashMap<String, Object>();
					exchgMap.put(ParamConstants.PARAM_STR_TRD_EXCHG_CODE, ALL_TYPE_KEY);
					exchgMap.put(ParamConstants.PARAM_STR_EXCHG_NAME, getResources().getString(R.string.all_exchange_type_label));
					exchgMap.put(ParamConstants.PARAM_INT_EXCHG_IMAGE_ID, null);
					
					listExchangeMap.add(exchgMap);
					
					Map<String, Integer> mapExchgImages = DefinitionConstants.getExchangeImageMapping();
					List<String> listQCExchgCode		= DefinitionConstants.getValidQCExchangeCodes();
		
					for(int i=0; i<listQCExchgCode.size(); i++){
						String strExchgCode		= DefinitionConstants.getTrdExchangeCode().get(i);
						String strQCExchgCode	= listQCExchgCode.get(i);
						String strExchgName		= DefinitionConstants.getExchangeNameMapping().get(strQCExchgCode);
						
						exchgMap				= new HashMap<String, Object>();
						exchgMap.put(ParamConstants.PARAM_STR_TRD_EXCHG_CODE, strExchgCode);		//Change_Request-20141101, ReqNo 20
						exchgMap.put(ParamConstants.PARAM_STR_EXCHG_NAME, strExchgName);	
						exchgMap.put(ParamConstants.PARAM_INT_EXCHG_IMAGE_ID, mapExchgImages.get(strQCExchgCode) == null ? null : ( mapExchgImages.get(strQCExchgCode) ).intValue() );
						
						listExchangeMap.add(exchgMap);
					}
					
					exchgListView.setListExchangeMap(listExchangeMap);
				} catch (Exception e) {			
					e.printStackTrace();
				}
				
				if (selectedExchangeMap == null) {
					for( Iterator<Map<String, Object>> itr=exchgListView.getListExchangeMap().iterator(); itr.hasNext(); ){
						Map<String, Object> mapExchange		= itr.next();
						String strExchgCode = String.valueOf( mapExchange.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
						if( strExchgCode.equalsIgnoreCase(ALL_TYPE_KEY) ){
							selectedExchangeMap = mapExchange;
							break;
						}
					}
				}
				btnExchangeType.setText( String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );
				// Added by Mary@20130121 - Change_Request-20130104, ReqNo.5 - END

			} //Added by Thinzar, Change_Request-20150901, ReqNo.1, run again only if alertException is not showing
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//Added by Thinzar, Change_Request-20160101, ReqNo.5 - START
		layoutName.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				//note: default ascending
				if(currentSortByMethod == SORT_BY_NAME_ASC){
					currentSortByMethod	= SORT_BY_NAME_DESC;
					
					//imgSortName.setImageResource(android.R.drawable.arrow_down_float);	//TODO:uncomment this back when CIMB SG agrees to put back sorting arrow
				} else {
					currentSortByMethod	= SORT_BY_NAME_ASC;
					//imgSortName.setImageResource(android.R.drawable.arrow_up_float);		//TODO:uncomment this back when CIMB SG agrees to put back sorting arrow
				}
				imgSortName.setVisibility(View.VISIBLE);
				imgSortLastUpdated.setVisibility(View.INVISIBLE);
				
				List<TradeOrder> orders 		= SortOrders(DefinitionConstants.tradeOrders); 	
				DefinitionConstants.tradeOrders = orders;
				filteredTradeOrders	= getFilteredTradeOrderList2(DefinitionConstants.tradeOrders, strSelectedFilterOrderType, strSelectedFilterExchangeType, strSelectedFilterPayType);
				OrderBookActivity.this.constructTableView(filteredTradeOrders);
			}
		});
		
		layoutLastUpdated.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View v){
				//note: default descending
				if(currentSortByMethod == SORT_BY_LAST_UPDATED_DESC){
					currentSortByMethod	= SORT_BY_LAST_UPDATED_ASC;
					//imgSortLastUpdated.setImageResource(android.R.drawable.arrow_up_float);	//TODO:uncomment this back when CIMB SG agrees to put back sorting arrow
				} else {
					currentSortByMethod	= SORT_BY_LAST_UPDATED_DESC;
					//imgSortLastUpdated.setImageResource(android.R.drawable.arrow_down_float);	//TODO:uncomment this back when CIMB SG agrees to put back sorting arrow
				}
				
				imgSortLastUpdated.setVisibility(View.VISIBLE);
				imgSortName.setVisibility(View.INVISIBLE);
				
				List<TradeOrder> orders 		= SortOrders(DefinitionConstants.tradeOrders); 	
				DefinitionConstants.tradeOrders = orders;
				filteredTradeOrders	= getFilteredTradeOrderList2(DefinitionConstants.tradeOrders, strSelectedFilterOrderType, strSelectedFilterExchangeType, strSelectedFilterPayType);
				OrderBookActivity.this.constructTableView(filteredTradeOrders);
			}
		});
		//Added by Thinzar, Change_Request-20160101, ReqNo.5 - END
		
		//Added by Thinzar, Change_Request-20160101, ReqNo.6 - START
		btnOrderHistory.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(OrderBookActivity.this, OrderHistoryDatePickerActivity.class);
				OrderBookActivity.this.startActivityForResult(intent, AppConstants.SELECT_ORDER_HISTORY_REQUEST);
			}
		});
		
		//Added by Thinzar, Change_Request-20160101, ReqNo.6 - END
	}
	
	@Override
	protected void onPause() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		
		try{
			super.onPause();
			
			onStopThread = true;
			
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
			
			if(statusThread!=null)
				statusThread.pauseRequest();
			
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) { 

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
			if(this.mainMenu.isShowing()){
				this.mainMenu.hide();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
			}else if( this.exchgListView.isShowing() ){
				this.exchgListView.hide();
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
			}else{
				// Added by Mary@20120920 - avoid nullpointerexception
				if(statusThread != null){
					statusThread.stopRequest();
					
					//Added by Thinzar@20140924, Fixes_Request-20140820,ReqNo.27
					statusThread = null;
					((StockApplication) OrderBookActivity.this.getApplication()).setTradeStatusThread(null);
				}
					
				finish();
			}
			// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}

	protected void onMenuShownEvent() {
		if (filterListView.isShowing()) {
			filterListView.hide();
		} 
	}

	View.OnClickListener orderFilter = new OnClickListener(){
		@Override
		public void onClick(View v) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			OrderBookActivity.this.doFilter();
		}

	};

	@Override
	public void orderFilterSelectedEvent(String OrderFilter) {

		this.strSelectedFilterOrderType = OrderFilter;
		if(OrderFilter!=null){
			FilterOrder.setText(OrderFilter);
		}

		isConstructing		= true;			//added by Thinzar, Fixes_Request-20140820,ReqNo.27
		filteredTradeOrders	= getFilteredTradeOrderList2(DefinitionConstants.tradeOrders, strSelectedFilterOrderType, strSelectedFilterExchangeType, strSelectedFilterPayType);
		constructTableView(filteredTradeOrders);
		// Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - END
	}

	private void doFilter() {
		if (filterListView.isShowing()) {
			filterListView.hide();
		} else {
			if (mainMenu.isShowing()) {
				mainMenu.hide();
			}
			filterListView.show(findViewById(R.id.RelativeLayoutContainer), strSelectedFilterOrderType);
		}

	}
	
	private class getClientLimit extends AsyncTask<Void, Void, String>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				
				OrderBookActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));

				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! OrderBookActivity.this.isFinishing() && !OrderBookActivity.this.dialog.isShowing() )
					OrderBookActivity.this.dialog.show();

			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		
		@Override
		protected String doInBackground(Void... params) {

			Map<String, Object> parameters	= new HashMap<String, Object>();
			
			atpUserParam 					= sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, null);
			senderCode 						= sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);

			StockApplication application	= (StockApplication) OrderBookActivity.this.getApplication();
				
			if(AppConstants.selectedAcc == null){
				tradeAccount 			= application.getSelectedAccount();
				AppConstants.selectedAcc= tradeAccount;
			}else{
				tradeAccount 			= AppConstants.selectedAcc;
			}

			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);

			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			parameters.put(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
			parameters.put(ParamConstants.CLIENTCODE_TAG, tradeAccount.getClientCode());
			parameters.put(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
			parameters.put(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
			parameters.put(ParamConstants.EXCHANGECODE_TAG, tradeAccount.getExchangeCode());
			
			//Edited by Thinzar, Fixes_Request-20170103, ReqNo.2
			if(tradeAccount.getAccountType().equalsIgnoreCase("B")){
				parameters.put(ParamConstants.PAYMENT_TYPE_TAG, "CUT");	
				parameters.put(ParamConstants.PAYMENT_CURRENCY_TAG, AppConstants.getStrBaseCurrencyCode());
			}

			String result 					= null;

			try {
				result	= AtpConnectUtil.tradeClientLimit(parameters);
				//Added by Thinzar, Change_Request-20150901, ReqNo.1
				AppConstants.connectExceptionCount = 0;
				AppConstants.timeoutExceptionCount = 0;
				
				while(intRetryCtr < AppConstants.intMaxRetry && result == null){
					intRetryCtr++;
					result = AtpConnectUtil.tradeClientLimit(parameters);
				}
				intRetryCtr	= 0;

				//Edited by Thinzar, Change_Request-20150901, ReqNo.1
				if (result == null){
					if(AppConstants.connectExceptionCount >= AppConstants.intMaxRetry){
						throw new Exception(ErrorCodeConstants.NETWORK_UNAVAILABLE);
					}
					else if(AppConstants.timeoutExceptionCount >= AppConstants.intMaxRetry){
						throw new Exception(ErrorCodeConstants.FAIL_CONNECT_ATP);
					} else
						throw new Exception(ErrorCodeConstants.FAIL_GET_TRADE_LIMIT);
				}
				
			}catch(FailedAuthenicationException e){        	
				e.printStackTrace();
				isExpired = true;
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				if(LoginActivity.refreshThread != null)
					LoginActivity.refreshThread.stopRequest();
			} catch (Exception e) {
				exception	= e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if(isExpired){
					if( dialog != null && dialog.isShowing() )
						dialog.dismiss();
					
					OrderBookActivity.this.sessionExpired();
					return;
				}
				
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(result == null || exception != null){
					
					//Added by Thinzar, Change_Request-20150901, ReqNo.1 - START
					boolean isATPConnectionLost = (exception.getMessage().equalsIgnoreCase(ErrorCodeConstants.FAIL_CONNECT_ATP))? true:false;
					prepareAlertException(OrderBookActivity.this.alertException, exception.getMessage(), isATPConnectionLost);
					//Added by Thinzar, Change_Request-20150901, ReqNo.1 - END
					
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! OrderBookActivity.this.isFinishing() ){
						OrderBookActivity.this.alertException.setCanceledOnTouchOutside(false);
						alertException.show();
					}
					
					exception	= null;
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				String balanceText 	= AtpMessageParser.extractTradeClientLimit(result);
				double balance		= Double.parseDouble(balanceText);
				quantityButtons 	= new ArrayList<Button>();
				headerLabel 		= (TextView) OrderBookActivity.this.findViewById(R.id.headerLabel);
	
				if(tradeAccount!=null){

					String strAccountDetails	= tradeAccount.getBranchCode()+" - "+tradeAccount.getAccountNo();
					if(!SystemUtil.isDerivative(tradeAccount.getExchangeCode())){
						strAccountDetails += " (" + AppConstants.getStrBaseCurrencyCode() + " " + FormatUtil.formatDoubleFull(balance, "0.00") + ")";
					}
					accountNoLabel.setText(strAccountDetails);
					clientNameLabel.setText(tradeAccount.getClientName());
					//Edited by Thinzar, Fixes_Request-20151001, ReqNo.5 - START
				}
				
				new stockRefresh().execute();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	//Change_Request-20160101, ReqNo. 7 - START
	private class GetTradeHistory extends AsyncTask<Void, Void, Void>{
		Exception exception	= null;
		
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			intRetryCtr	= 0;
			
			OrderBookActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
		
			if( ! OrderBookActivity.this.isFinishing() && !OrderBookActivity.this.dialog.isShowing() ) {
				OrderBookActivity.this.dialog.show();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			if(tradeAccount!=null){
				parameters.put(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
				parameters.put(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
				
				parameters.put(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
			}else{
				parameters.put(ParamConstants.ACCOUNTNO_TAG, "");
				parameters.put(ParamConstants.BRANCHCODE_TAG, "");
			}
			
			parameters.put(ParamConstants.EXCHANGECODE_TAG, tradeAccount.getExchangeCode());
			parameters.put(ParamConstants.DATE_FROM_TAG, dateFrom);
			parameters.put(ParamConstants.DATE_TO_TAG, dateTo);
			
			try {
				String response = AtpConnectUtil.tradeHistory(parameters);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response	= AtpConnectUtil.tradeHistory(parameters);
				}
					
				intRetryCtr	= 0;						
				
				if(response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_TRADE_HISTORY);
				
				//use the same parser as tradeStatus
				DefinitionConstants.tradeOrders	= AtpMessageParser.parseTradeStatus(response);
				List<TradeOrder> orders 		= SortOrders(DefinitionConstants.tradeOrders); 	
				DefinitionConstants.tradeOrders = orders;
				
				filteredTradeOrders	= getFilteredTradeOrderList2(DefinitionConstants.tradeOrders, strSelectedFilterOrderType, strSelectedFilterExchangeType, strSelectedFilterPayType);
				
			} catch (FailedAuthenicationException e) {
				isExpired = true;
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				if(LoginActivity.refreshThread != null)
					LoginActivity.refreshThread.stopRequest();
				exception = e;
			} catch (Exception ex) {
				exception	= ex;
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			if(!OrderBookActivity.this.isFinishing() && dialog != null && dialog.isShowing() )
				dialog.dismiss();
					
			if(isExpired){
				OrderBookActivity.this.sessionExpired();
				return;
			}
			
			if(exception != null){
				alertException.setMessage( exception.getMessage() );
				alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						dialog.dismiss();
						finish();
					}
				});
				
				if( ! OrderBookActivity.this.isFinishing() ){
					if(exception.getMessage() != null){
						alertException.show();	 
					} 
					else{
						if(OrderBookActivity.this.dialog.isShowing()) dialog.dismiss();
					}
				}
						
				exception	= null;
				return;
			}
			
			txtOrderHistoryDetails.setText(getResources().getString(R.string.order_history_result_title)
					+ ": " + FormatUtil.customFormatDate(dateFrom, "yyyyMMdd", "dd-MMM-yyyy")
					+ " " + getResources().getString(R.string.order_history_result_to)
					+ " " + FormatUtil.customFormatDate(dateTo, "yyyyMMdd", "dd-MMM-yyyy"));
			txtOrderHistoryDetails.setVisibility(View.VISIBLE);
			
			OrderBookActivity.this.constructTableView(filteredTradeOrders);
		}
	}
	//Change_Request-20160101, ReqNo. 7 - END

	private class stockRefresh extends AsyncTask<View, Void, String> {
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			DefinitionConstants.Debug("tradeDebug==OrderBookActivity, stockRefresh()");
		}

		@Override
		protected String doInBackground(View... params) {
			
			Map<String, Object> parameters	= new HashMap<String, Object>();

			parameters.put(ParamConstants.TIMESTAMP_TAG, "");

			if( AppConstants.brokerConfigBean.isEnableEncryption() )
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
			else
				parameters.put(ParamConstants.USERPARAM_TAG, atpUserParam);
			
			parameters.put(ParamConstants.SENDERCODE_TAG, senderCode);
			if(tradeAccount!=null){
				parameters.put(ParamConstants.ACCOUNTNO_TAG, tradeAccount.getAccountNo());
				parameters.put(ParamConstants.BRANCHCODE_TAG, tradeAccount.getBranchCode());
				
				//Added by Thinzar, Fixes_Request-20140820, ReqNo.1
				parameters.put(ParamConstants.BROKERCODE_TAG, tradeAccount.getBrokerCode());
			}else{
				parameters.put(ParamConstants.ACCOUNTNO_TAG, "");
				parameters.put(ParamConstants.BRANCHCODE_TAG, "");
			}
			
			try {
				if(DefinitionConstants.tradeOrders==null){
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					String response = AtpConnectUtil.tradeStatus(parameters);
					*/
					String response = null;
					try{
						response = AtpConnectUtil.tradeStatus(parameters);
						while(intRetryCtr < AppConstants.intMaxRetry && response == null){
							intRetryCtr++;
							response = AtpConnectUtil.tradeStatus(parameters);
						}
						intRetryCtr	= 0;						
						
						if(response == null)
							throw new Exception(ErrorCodeConstants.FAIL_GET_TRADE_STATUS);
					}catch(Exception e){
						throw e;
					}
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					
					DefinitionConstants.tradeOrders = AtpMessageParser.parseTradeStatus(response);
					List<TradeOrder> orders 		= SortOrders(DefinitionConstants.tradeOrders); 	//SortArrayListByDate(DefinitionConstants.tradeOrders);	//Edited by Thinzar, Change_Request-20160101, ReqNo.5
					DefinitionConstants.tradeOrders = orders;

					//Added by Melissa - START
					//To check if there are redundant orders, if there are, take order with latest update (L Ascii 76) and remove the rest
					//Check using TicketNo (* Ascii 42), unique data to represent each order
					
					List<String> ticketNos = new ArrayList<String>();
					List<TradeOrder> tempOrders = new ArrayList<TradeOrder>();
					
					for(int i=0;i<orders.size();i++){
						if(i==0){
							ticketNos.add(orders.get(0).getOrderTicket());
							tempOrders.add(orders.get(0));
						}
						else{
							if (!ticketNos.contains(orders.get(i).getOrderTicket())) {
								ticketNos.add(orders.get(i).getOrderTicket());
								tempOrders.add(orders.get(i));
							}
						}
					}
					DefinitionConstants.tradeOrders = tempOrders;
					
					//Added by Melissa - END
					
					if (DefinitionConstants.tradeOrders != null) {
						for (Iterator<TradeOrder> itr=DefinitionConstants.tradeOrders.iterator(); itr.hasNext(); ) {
							TradeOrder order 		= itr.next();
							String symbolName = OrderBookActivity.this.getSymbolName(order.getSymbolCode());

							if (symbolName != null) {
								order.setStockCode(symbolName);
							}
						}
					}
					
					// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - START
					filteredTradeOrders	= getFilteredTradeOrderList2(DefinitionConstants.tradeOrders, strSelectedFilterOrderType, strSelectedFilterExchangeType, strSelectedFilterPayType);
					return strSelectedFilterOrderType;
					// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - END
				}else{
					if(DefinitionConstants.tradeOrders.size()!=0){
					
						long latestTimeStamp = 0;
						for (Iterator<TradeOrder> itr=DefinitionConstants.tradeOrders.iterator(); itr.hasNext(); ) {
							TradeOrder order	= itr.next();
							String arr[] 		= order.getLastUpdate().split("\\.");
							String newTimeStamp = arr[0] + arr[1];
							long update 		= Long.parseLong(newTimeStamp);

							if(update > latestTimeStamp)
								latestTimeStamp = update;
						}

						String timestamp			= Long.toString(latestTimeStamp);
						parameters.put( ParamConstants.TIMESTAMP_TAG, FormatUtil.formatTimeStamp(timestamp) );

						String response				= AtpConnectUtil.tradeStatus(parameters);
						List<TradeOrder> tradeOrders= AtpMessageParser.parseTradeStatus(response);
						
						if(tradeOrders!=null){
							List<String> orderTicket2 = new ArrayList<String>();
							for(Iterator<TradeOrder> itr=DefinitionConstants.tradeOrders.iterator(); itr.hasNext(); ){
								TradeOrder Updateorder	= itr.next();
								String order2			= Updateorder.getOrderTicket().substring(1,Updateorder.getOrderTicket().length()-1);
								orderTicket2.add(order2);
							}
							updateGlobalTradeOrders(tradeOrders, orderTicket2);
						}

					}else{
						String response = AtpConnectUtil.tradeStatus(parameters);
						DefinitionConstants.tradeOrders	= AtpMessageParser.parseTradeStatus(response);
						List<TradeOrder> orders 		= SortOrders(DefinitionConstants.tradeOrders); 	//SortArrayListByDate(DefinitionConstants.tradeOrders);	//Edited by Thinzar, Change_Request-20160101, ReqNo.5SortArrayListByDate(DefinitionConstants.tradeOrders);
						DefinitionConstants.tradeOrders = orders;

					}
					
					// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - START
					filteredTradeOrders	= getFilteredTradeOrderList2(DefinitionConstants.tradeOrders, strSelectedFilterOrderType, strSelectedFilterExchangeType, strSelectedFilterPayType);
					return strSelectedFilterOrderType;
					// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - END
				}
			} catch (FailedAuthenicationException e) {
				isExpired = true;
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				if(LoginActivity.refreshThread != null)
					LoginActivity.refreshThread.stopRequest();
				exception = e;
			} catch (Exception ex) {
				exception	= ex;
				ex.printStackTrace();
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			return null;
		}

		@Override
		protected void onPostExecute(String SelectedFilter) {
			super.onPostExecute(SelectedFilter);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			try{
				if(dialog != null && dialog.isShowing() )
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
					dialog.dismiss();
				
				if(isExpired){
					OrderBookActivity.this.sessionExpired();
					return;
				}
	
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							finish();
						}
					});
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! OrderBookActivity.this.isFinishing() ){
						DefinitionConstants.Debug("Debug: caught an exception");	//java.util.ConcurrentModificationException
						if(exception.getMessage() != null){
							alertException.show();	//Edited by Thinzar, Fixes_Request-20151001, ReqNo.2 
						} 
						//Added by Thinzar, Fixes_Request-20151001, ReqNo.2
						else{
							if(OrderBookActivity.this.dialog.isShowing()) dialog.dismiss();
						}
					}
					
					exception	= null;
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				if(dialog != null && dialog.isShowing() )
					dialog.dismiss();
				
				expandid		= new boolean[DefinitionConstants.tradeOrders.size()];

				OrderBookActivity.this.constructTableView(filteredTradeOrders);
	
				int rate 		= sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
				
				statusThread 	= ((StockApplication) OrderBookActivity.this.getApplication()).getTradeStatusThread();
				if(statusThread == null){
					
					statusThread = new TradeStatusThread(atpUserParam, rate, tradeAccount, senderCode);
					statusThread.start();	
					statusThread.setTradeAccount(tradeAccount);
					statusThread.setRegisteredListener(OrderBookActivity.this);
					((StockApplication) OrderBookActivity.this.getApplication()).setTradeStatusThread(statusThread);
				
				}else{
					
					statusThread.resumeRequest();
					statusThread.setRegisteredListener(OrderBookActivity.this);
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

	}
	
	private List<TradeOrder> SortOrders(List<TradeOrder> orderlist){
		DefinitionConstants.Debug("tradeDebug==OrderBookActivity, SortOrders()");
		switch(currentSortByMethod){
		
		case SORT_BY_NAME_ASC:
			Collections.sort(orderlist, new TradeOrder.orderByStockCodeASC());
			break;
			
		case SORT_BY_NAME_DESC:
			Collections.sort(orderlist, new TradeOrder.orderByStockCodeDESC());
			break;
			
		case SORT_BY_LAST_UPDATED_ASC:
			Collections.sort(orderlist, new TradeOrder.orderByLastUpdateASC());
			break;
			
		case SORT_BY_LAST_UPDATED_DESC:
			Collections.sort(orderlist, new TradeOrder.orderByLastUpdateDESC());
			break;
		}
		
		return orderlist;
	}

	private String getSymbolName(String symbolCode) {
		try {
			String[] symbolCodes = {symbolCode};

			String response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_TRADE);
			while(intRetryCtr < AppConstants.intMaxRetry && response == null){
				intRetryCtr++;
				response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_TRADE);
			}
			intRetryCtr	= 0;
			
			if(response == null)				
				throw new Exception(ErrorCodeConstants.FAIL_GET_STOCK_DETAIL);
			// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			StockSymbol stockSymbol = QcMessageParser.parseStockTrade(response);

			return stockSymbol.getStockCode();

		}catch(Exception e){
			e.printStackTrace(); // NOTE : this function is executed in a loop so no error alert shown
		}
		return null;
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == AppConstants.SELECT_ACCOUNT_REQUEST) {
			if (resultCode == Activity.RESULT_OK) {
				Bundle bundle 				= data.getExtras();
				String accountNo 			= bundle.getString(ParamConstants.ACCOUNTNO_TAG);
				String branchCode 			= bundle.getString(ParamConstants.BRANCHCODE_TAG);
				
				StockApplication application= (StockApplication) OrderBookActivity.this.getApplication();
				List<TradeAccount> accounts = application.getTradeAccounts();
				
				for (Iterator<TradeAccount> itr=accounts.iterator(); itr.hasNext(); ) {
					TradeAccount account	= itr.next();
					if( account.getAccountNo().equals(accountNo) && account.getBranchCode().equals(branchCode) ){
						tradeAccount 			= account;             			
						application.setSelectedAccount(account);
						AppConstants.selectedAcc= account;
						statusThread.setTradeAccount(account);
						
						DefinitionConstants.tradeOrders.clear();
						// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13
						filteredTradeOrders.clear();
						
						String strQCExchgCode	= null; 
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
						String strTrdExchgCode	= null;
						for( Iterator<ExchangeInfo> itr2=application.getExchangeInfos().iterator(); itr2.hasNext(); ){
							ExchangeInfo exchgInfo = itr2.next();
							if( exchgInfo.getTrdExchgCode() != null && account.getExchangeCode().equals( exchgInfo.getTrdExchgCode() ) ){
								strQCExchgCode	= exchgInfo.getQCExchangeCode();
								// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
								strTrdExchgCode	= exchgInfo.getTrdExchgCode();
								break;
							}
						}
						
						// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
						Map<String, String> mapBrokerUserPreference	= new HashMap<String, String>();
						mapBrokerUserPreference.put( PreferenceConstants.EXCHANGE_CODE,  strQCExchgCode );
						mapBrokerUserPreference.put( PreferenceConstants.ACCOUNT_NO, account.getAccountNo() );
						mapBrokerUserPreference.put( PreferenceConstants.BRANCH_CODE, account.getBranchCode() );

						// Added by Mary@20121112 - Change_Request-20120719, ReqNo.12
						mapBrokerUserPreference.put(PreferenceConstants.TRADE_PREFERENCE_EXCHG_CODE, strTrdExchgCode);
						mapBrokerUserPreference.put(PreferenceConstants.TRADE_EXCHANGE_CODE, strTrdExchgCode);
						AppConstants.cacheBrokerUserDetails(OrderBookActivity.this, sharedPreferences, mapBrokerUserPreference);
						mapBrokerUserPreference.clear();

						mapBrokerUserPreference						= null;
						// Added by Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
						
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12 - START
						strQCExchgCode 		= null;
						strTrdExchgCode 	= null;
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12 - END
						break;
					}
				}
				
				//Added by Thinzar, Change_Request-20160101, ReqNo.7
				isOrderHistory	= false;
				dateFrom		= null;
				dateTo			= null;
			}
		} else if(requestCode == AppConstants.SELECT_ORDER_HISTORY_REQUEST){
			isOrderHistory	= false;	//reset the boolean
			
			if(resultCode == Activity.RESULT_OK){
			
				Bundle bundle	= data.getExtras();
				dateFrom		= bundle.getString(ParamConstants.DATE_FROM_TAG);
				dateTo			= bundle.getString(ParamConstants.DATE_TO_TAG);
				
				DefinitionConstants.tradeOrders.clear();
				filteredTradeOrders.clear();
				
				if(dateFrom != null && !dateFrom.isEmpty()){
					if(dateTo != null && !dateTo.isEmpty()){
						isOrderHistory	= true;
					} 
				}
			}
		}
	}

	private void constructTableView(List<TradeOrder> lstFilteredTradeOrder) {
		DefinitionConstants.Debug("tradeDebug==OrderBookActivity, constructTableView()");
		try {
			TableLayout tableLayout				= (TableLayout) findViewById(R.id.orderTable);
			tableLayout.removeAllViews();

			quantityButtons.clear();

			int id 								= 0;

			for(Iterator<TradeOrder> itr=lstFilteredTradeOrder.iterator(); itr.hasNext(); ){
				TradeOrder order		= itr.next();
				//Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - START

				if(order != null)
					TableView(id++, order, tableLayout);
				// Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - END
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	
		isConstructing=false;
	}
	
	private void TableView(int id, TradeOrder order, TableLayout tableLayout){
		DefinitionConstants.Debug("tradeDebug==OrderBookActivity, TableView()");

		final TableRow tableRow 	= new TableRow(this);   
		tableRow.setId(id);
		tableRow.setLayoutParams( new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT) );
		tableRow.setWeightSum(1);

		View rowView				= this.getLayoutInflater().inflate(R.layout.order_book_row, null);
		final LinearLayout rowLayout= (LinearLayout) rowView.findViewById(R.id.rowLayout);

		TextView nameLabel 			= (TextView) rowView.findViewById(R.id.nameLabel);
		nameLabel.setText(order.getStockCode());
		
		TextView lastUpdatedLabel	= (TextView) rowView.findViewById(R.id.lastUpdatedLabel);
		lastUpdatedLabel.setText(FormatUtil.formatLastUpdateDate(order.getLastUpdate()));

		String strSymbolExchg		= order.getSymbolCode().substring( (order.getSymbolCode().lastIndexOf(".") + 1), order.getSymbolCode().length() );
		
		final TextView priceLabel 	= (TextView) rowView.findViewById(R.id.priceLabel);
		
		//Edited by Thinzar, Change_Request-20170119, ReqNo.14
		String priceText = "";
		if((order.getOrderType().equalsIgnoreCase("Market") 
				|| order.getOrderType().equalsIgnoreCase("MarketToLimit"))
				&& order.getPrice() == 0f){
			priceText = "-";
		}else{
			if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
				priceText 	= FormatUtil.formatPrice2(order.getPrice(), null);
			}else{
				priceText 	=  FormatUtil.formatPrice(order.getPrice(), null, strSymbolExchg); 
			}
		}
		priceLabel.setText(priceText);
		
		priceLabel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				selectedRow						= rowLayout;
				selectedId						= tableRow.getId();

				int id 							= tableRow.getId();

				//Fixes_Request-20170701, ReqNo. 6
				if(filteredTradeOrders != null && filteredTradeOrders.size() > id) {

					TradeOrder order = filteredTradeOrders.get(id);
					HashMap<String, String> details = new HashMap<String, String>();
					ArrayList<String> sequences = new ArrayList<String>();
					OrderBookActivity.this.populateOrderDetail(order, details, sequences);
					Intent intent = new Intent();
					intent.putExtra(ParamConstants.SYMBOLCODE_TAG, order.getSymbolCode());
					intent.putExtra(ParamConstants.ORDER_DETAILS_TAG, details);
					intent.putExtra(ParamConstants.ORDER_DISPLAYSEQ_TAG, sequences);
					intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_REVISE);
					intent.putExtra(ParamConstants.ORDER_NO_TAG, order.getOrderNo());
					intent.putExtra(ParamConstants.ORDER_FIXEDNO_TAG, order.getFixedNo());
					intent.putExtra(ParamConstants.ORDER_PRICE_TAG, order.getPrice());
					intent.putExtra(ParamConstants.ORDER_QUANTITY_TAG, order.getQuantity());
					intent.putExtra(ParamConstants.ORDER_MATCHEDQTY_TAG, order.getMatchedQuantity());

					// Added by Mary@20120806 - Fixes_Request-20120724, ReqNo.5
					intent.putExtra(ParamConstants.ORDER_UNMATCHQTY_TAG, order.getUnmatchQuantity());
					// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
					intent.putExtra(ParamConstants.DISCLOSED_QUANTITY_TAG, order.getDisclosedQuantity());
					intent.putExtra(ParamConstants.MINIMUM_QUANTITY_TAG, order.getMinimumQuantity());

					// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
					intent.putExtra(ParamConstants.ORDER_STATUS_TAG, order.getStatusCode());
					intent.putExtra(ParamConstants.PAYMENT_TYPE_TAG, order.getPaymentType());
					intent.putExtra(ParamConstants.ORDER_VALIDITY_TAG, order.getValidity());
					intent.putExtra(ParamConstants.ORDER_TYPE_TAG, order.getOrderType());

					intent.putExtra(ParamConstants.STOCK_CODE_TAG, order.getStockCode());
					intent.putExtra(ParamConstants.ORDER_STOPLIMIT_TAG, order.getStopLimit());
					if (order.getExpiryDate() != null) {

						intent.putExtra(ParamConstants.GO_TO_DATE_TAG, FormatUtil.formatDateString(order.getExpiryDate(), AppConstants.PARAM_DATE_FORMAT));
					} else {
						intent.putExtra(ParamConstants.GO_TO_DATE_TAG, "");
					}

					intent.putExtra(ParamConstants.PAYMENT_CURRENCY_TAG, order.getStrSettlementCurrency());

					// Added by Mary@20120914 - Change_Request-20120719, ReqNo.12
					intent.putExtra(ParamConstants.LOT_SIZE_TAG, order.getIntLotSize());

					// Added by Mary@20130108 - Fixes_Request-20121102, ReqNo.17
					intent.putExtra(ParamConstants.ORI_ACTION_TYPE, order.getAction());

					//Added by Thinzar@20140819 - Change_Request-2140801, ReqNo.4
					intent.putExtra(ParamConstants.TRIGGER_PRICE_TYPE_TAG, order.getTriggerPriceType());
					intent.putExtra(ParamConstants.TRIGGER_PRICE_DIRECTION_TAG, order.getTriggerPriceDirection());

					//Fixes_Request-20170701, ReqNo.16
					intent.putExtra(ParamConstants.TICKETNO_TAG, order.getOrderTicket());

					intent.setClass(OrderBookActivity.this, OrderDetailActivity.class);
					startActivity(intent);
				}
			}

		});

		final Button quantityButton 			= (Button) rowView.findViewById(R.id.changeButton);

		toggleQuantityButton(quantityButton, order);
		
		//Change_Request-20150901, ReqNo.6 - START
		if(order.getAction().equalsIgnoreCase("Buy"))
			quantityButton.setBackgroundResource(LayoutSettings.btnGreen);
		else 
			quantityButton.setBackgroundResource(LayoutSettings.btnRed);
		//Change_Request-20150901, ReqNo.6 - END
		
		quantityButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				OrderBookActivity.this.changeButtonStatus();
			}
		});
		quantityButtons.add(quantityButton);

		tableRow.addView(rowView);

		final View rowExpandView 				= this.getLayoutInflater().inflate(R.layout.order_expand_row, null);
		rowExpandView.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				priceLabel.performClick();
			}
		});
		populateExpandViewData(rowExpandView, order);

		tableRow.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END

				TradeOrder order = filteredTradeOrders.get(tableRow.getId());
				if (!order.isExpand()) {
					rowLayout.addView(rowExpandView); 

					// Added by Mary@20130710 - avoid IndexOutOfBoundException
					if( tableRow.getId() < expandid.length)
						expandid[tableRow.getId()]=true;
					
					//rowLayout.setBackgroundResource( ( (tableRow.getId()+1)%2 != 0 ) ? LayoutSettings.tablerow_dark_expand : LayoutSettings.tablerow_light_expand );	Commented out, Change_Request-20160101, ReqNo.3

					order.setExpand(true);
				}else{
					int childCount = rowLayout.getChildCount();
					if(childCount!=1)
						rowLayout.removeViewAt(1);
					
					// Added by Mary@20130710 - avoid IndexOutOfBoundException
					if( tableRow.getId() < expandid.length)
						expandid[tableRow.getId()]=false;

					order.setExpand(false);
				}
			}
		});
		
		if(id < expandid.length && expandid[id]){
			order.setExpand(true);
			rowLayout.addView(rowExpandView); 

		}  
	

		tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)); 
	}

	private void populateOrderDetail(TradeOrder order, Map<String, String> details, List<String> sequences) {
		DefinitionConstants.Debug("tradeDebug==OrderBookActivity, populateOrderDetail()");

		String strSymbolExchg			= order.getSymbolCode().substring( (order.getSymbolCode().lastIndexOf(".") + 1), order.getSymbolCode().length() );

		final String PARAM_ACTION		= getResources().getString(R.string.order_param_action); //"Action";
		final String PARAM_STATUS		= getResources().getString(R.string.order_param_status); //"Status";
		//final String PARAM_ORD_QTY	= "Order Qty (Unit)";	//Commented out by Thinzar, Fixes_Request-20140820, ReqNo.13
		final String PARAM_ORD_PRC		= getResources().getString(R.string.order_param_order_price); //"Order Price";
		final String PARAM_ORD_VAL		= getResources().getString(R.string.order_param_order_value); //"Order Value";
		final String PARAM_ORD_TYPE		= getResources().getString(R.string.order_param_order_type); //"Order Type";
		final String PARAM_VALIDITY		= getResources().getString(R.string.order_param_validity); //"Validity";

		// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15 - START
		final String PARAM_STLMNT_CURR	= getResources().getString(R.string.order_param_sett_currency); //"Settlement Currency";
		final String PARAM_PYMT			= getResources().getString(R.string.orderpad_payment); //"Payment";

		//Added by Thinzar@20140819 - Change_Request-20140801, ReqNo4 - START
		final String PARAM_TRIGGER_PRICE	= getResources().getString(R.string.orderpad_trigger_price); //"Trigger Price";
		final String PARAM_TRIGGER_TYPE		= getResources().getString(R.string.orderpad_trigger); //"Trigger Price Type";
		final String PARAM_TRIGGER_DIRECTION	= getResources().getString(R.string.orderpad_trigger_direction); //"Trigger Price Direction";

		//final String PARAM_UNMCH_QTY	= "Unmatch Qty (Unit)";			//Commented out by Thinzar, Fixes_Request-20140820, ReqNo.13
		//final String PARAM_MCH_QTY	= "Matched Qty (Unit)";			//Commented out by Thinzar, Fixes_Request-20140820, ReqNo.13
		final String PARAM_MCH_PRC		= getResources().getString(R.string.order_param_matched_price); //"Matched Price";
		final String PARAM_MCH_VAL		= getResources().getString(R.string.order_param_matched_value); //"Matched Value";
		final String PARAM_ORD_SRC		= getResources().getString(R.string.order_param_ord_src); //"Order Source";
		final String PARAM_ORD_NO		= getResources().getString(R.string.order_param_ord_num); //"Order No";
		final String PARAM_ORD_DATE		= getResources().getString(R.string.order_param_ord_date); //"Order Date";
		final String PARAM_ORD_TIME		= getResources().getString(R.string.order_param_ord_time); //"Order Time";
		final String PARAM_ACC_NO		= getResources().getString(R.string.order_param_acc_num); //"Account Number";
		final String PARAM_REMARK		= getResources().getString(R.string.order_param_remarks); //"Remarks";

		// Added by Mary@20120914 - Change_Request-20120719, ReqNo.12
		final String PARAM_LOT_SIZE		= getResources().getString(R.string.order_param_lot_size); //"Lot Size";
		
		//Added by Thinzar@20140903 - Fixes_Request-20140820, ReqNo.13 - START
		String PARAM_ORD_QTY		= getResources().getString(R.string.order_param_ord_qty_unit); 		//"Order Qty (Unit)";
		String PARAM_DISCLOSE_QTY	= getResources().getString(R.string.order_param_disclosed_qty_unit); 	//"Disclosed Qty (Unit)";
		String PARAM_MIN_QTY		= getResources().getString(R.string.order_param_min_qty_unit); 	//"Minimum Qty (Unit)";
		String PARAM_UNMCH_QTY		= getResources().getString(R.string.order_param_unmatched_qty_unit); 	//"Unmatched Qty (Unit)";
		String PARAM_MCH_QTY		= getResources().getString(R.string.order_param_matched_qty_unit); 	//"Matched Qty (Unit)";
		
		if(SystemUtil.isDerivative(strSymbolExchg)){
			PARAM_ORD_QTY		= getResources().getString(R.string.order_param_contract); //"Contract";
			PARAM_DISCLOSE_QTY	= getResources().getString(R.string.order_param_disclosed_qty); //"Disclosed Qty";
			PARAM_MIN_QTY		= getResources().getString(R.string.order_param_min_qty); //"Minimum Qty";	
			PARAM_UNMCH_QTY		= getResources().getString(R.string.order_param_unmatched_qty); //"Unmatched Qty";
			PARAM_MCH_QTY		= getResources().getString(R.string.order_param_matched_qty); //"Matched Qty";	
		}
		//Added by Thinzar@20140903 - Fixes_Request-20140820, ReqNo.13 - END
		
		details.put( PARAM_ACTION, order.getAction() );
		sequences.add(PARAM_ACTION);
		
		details.put( PARAM_STATUS, order.getStatusText() );
		sequences.add(PARAM_STATUS);

		details.put( PARAM_ORD_QTY, FormatUtil.formatLong(order.getQuantity(), null) );
		sequences.add(PARAM_ORD_QTY);
		
		//Edited by Thinzar, Change_Request-20170119, ReqNo.14
		String priceText = "";
		if((order.getOrderType().equalsIgnoreCase("Market") 
				|| order.getOrderType().equalsIgnoreCase("MarketToLimit"))
				&& order.getPrice() == 0f){
			priceText = "-";
		}else{
			if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
				priceText = FormatUtil.formatPrice2(order.getPrice(), null);
			else
				priceText = FormatUtil.formatPrice(order.getPrice(), null, strSymbolExchg);
		}
		details.put( PARAM_ORD_PRC, order.getStrStockCurrency() + " " +  priceText);
		sequences.add(PARAM_ORD_PRC);

		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
			details.put( PARAM_ORD_VAL, order.getStrStockCurrency() + " " + FormatUtil.formatDouble2(order.getValue(), null) );
		else
			details.put( PARAM_ORD_VAL, order.getStrStockCurrency() + " " + FormatUtil.formatDoubleByExchange(order.getValue(), null, strSymbolExchg));
		sequences.add(PARAM_ORD_VAL);
		
		details.put( PARAM_ORD_TYPE, order.getOrderType() );
		sequences.add(PARAM_ORD_TYPE);

		if( order.getValidity().equalsIgnoreCase(VALIDITY_GTD) || order.getValidity().equalsIgnoreCase(VALIDITY_GTM) )
			details.put( PARAM_VALIDITY, order.getValidity() + " (" + FormatUtil.formatDateString(order.getExpiryDate(), "dd/MM/yyyy") + ")" );	        
		else
			details.put( PARAM_VALIDITY, order.getValidity() );
		sequences.add(PARAM_VALIDITY);
		
		// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15 - START
		details.put( PARAM_STLMNT_CURR, order.getStrSettlementCurrency() );
		sequences.add(PARAM_STLMNT_CURR);

		String strPaymentType	= order.getPaymentType() == null ? "" : order.getPaymentType().trim();
		details.put( PARAM_PYMT, strPaymentType);
		sequences.add(PARAM_PYMT);
		// Added by Mary@20130102 - Fixes_Request-20121102, ReqNo.15 - END
		
		//Added by Thinzar@20140819 - Change_Request-20140801, ReqNo4 - START
		if(order.getOrderType().equalsIgnoreCase("Stop") || order.getOrderType().equalsIgnoreCase("StopLimit")){
			sequences.add(PARAM_TRIGGER_PRICE);
			details.put(PARAM_TRIGGER_PRICE, Float.toString(order.getStopLimit()));
			
			String triggerType = order.getTriggerPriceType();
			if(triggerType != null && triggerType.length()>0){
				sequences.add(PARAM_TRIGGER_TYPE);
				details.put(PARAM_TRIGGER_TYPE, triggerType);
			}
			
			String triggerDirection = order.getTriggerPriceDirection();
			if(triggerDirection != null && triggerDirection.length()>0){
				sequences.add(PARAM_TRIGGER_DIRECTION);
				details.put(PARAM_TRIGGER_DIRECTION, triggerDirection);
			}
		}
		//Added by Thinzar@20140819 - Change_Request-20140801, ReqNo4 - END
		
		// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
		details.put( PARAM_DISCLOSE_QTY, FormatUtil.formatLong(order.getDisclosedQuantity(), null) );
		sequences.add(PARAM_DISCLOSE_QTY);
		
		//Mary@20130315 - Fixes_Request-20120815, ReqNo.15

		details.put( PARAM_MIN_QTY, FormatUtil.formatLong(order.getMinimumQuantity(), null) );
		sequences.add(PARAM_MIN_QTY);

		details.put( PARAM_UNMCH_QTY, FormatUtil.formatLong(order.getUnmatchQuantity(), null) );
		sequences.add(PARAM_UNMCH_QTY);

		details.put( PARAM_MCH_QTY, FormatUtil.formatLong(order.getMatchedQuantity(), null) );
		sequences.add(PARAM_MCH_QTY);

		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
			details.put( PARAM_MCH_PRC, order.getStrStockCurrency() + " " + FormatUtil.formatPrice2(order.getMatchedPrice(), null) );
		else
			details.put( PARAM_MCH_PRC, order.getStrStockCurrency() + " " + FormatUtil.formatPrice(order.getMatchedPrice(), null, strSymbolExchg));
		
		sequences.add(PARAM_MCH_PRC);

		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") )
			details.put( PARAM_MCH_VAL, order.getStrStockCurrency() + " " + FormatUtil.formatDouble2(order.getMatchedValue(), null) );
		else
			details.put( PARAM_MCH_VAL, order.getStrStockCurrency() + " " + FormatUtil.formatDoubleByExchange(order.getMatchedValue(), null, strSymbolExchg));
		
		sequences.add(PARAM_MCH_VAL);

		//Edited by Thinzar, Fixes_Request-20140929, ReqNo.4
		if(!SystemUtil.isDerivative(strSymbolExchg)){
			details.put( PARAM_LOT_SIZE, FormatUtil.formatInteger(order.getIntLotSize(), null) );
			sequences.add(PARAM_LOT_SIZE);
		}
		// Added by Mary@20120914 - Change_Request-20120719, ReqNo.12 - END
		
		details.put( PARAM_ORD_SRC, DefinitionConstants.getOrderSourceDefinitions().get( order.getOrderSource() ) );
		sequences.add(PARAM_ORD_SRC);
		
		details.put( PARAM_ORD_NO, order.getOrderNo() );
		sequences.add(PARAM_ORD_NO);
		
		details.put( PARAM_ORD_DATE, FormatUtil.formatDateString(order.getOrderDate(), "yyyy-MM-dd") );
		sequences.add(PARAM_ORD_DATE);
		
		details.put( PARAM_ORD_TIME, FormatUtil.formatDateString(order.getOrderDate(), "hh:mm:ss") );
		sequences.add(PARAM_ORD_TIME);
		
		details.put( PARAM_ACC_NO, order.getAccountNo() );
		sequences.add(PARAM_ACC_NO);
		
		details.put( PARAM_REMARK, order.getRemark() );
		sequences.add(PARAM_REMARK);
		// Mary@20120914 - Change_Request-20120815, ReqNo.3 & 15 - END
	}

	private void populateExpandViewData(View expandView, TradeOrder order) {
		DefinitionConstants.Debug("tradeDebug==OrderBookActivity, populateExpandViewData()");

		String strSymbolExchg			= order.getSymbolCode().substring( (order.getSymbolCode().lastIndexOf(".") + 1), order.getSymbolCode().length() );
		
		TextView priceLabel = (TextView) expandView.findViewById(R.id.priceLabel);

		if( strSymbolExchg.equalsIgnoreCase("JK") || strSymbolExchg.equalsIgnoreCase("JKD") ){
			priceLabel.setText( FormatUtil.formatPrice2(order.getMatchedPrice(), null) );
		}else{
			priceLabel.setText( FormatUtil.formatPrice(order.getMatchedPrice(), null, strSymbolExchg));
		}

		TextView quantityLabel = (TextView) expandView.findViewById(R.id.quantityLabel);

		int intLotSize	= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? order.getIntLotSize() : 1;

		quantityLabel.setText( FormatUtil.formatLong(order.getMatchedQuantity() , null) );	//edited by Thinzar, Fixes_Request-20150601, ReqNo.1
		
		TextView statusLabel = (TextView) expandView.findViewById(R.id.statusLabel);
		statusLabel.setText(order.getStatusText());

		TextView actionLabel = (TextView) expandView.findViewById(R.id.actionLabel);
		actionLabel.setText(order.getAction());
		
		//Added by Thinzar@20140801 - Change_Request-20140701, ReqNo. 9 - START
		String settCurrency = order.getStrSettlementCurrency();
		String settMode = order.getPaymentType();
		
		TableRow tblRowSettlement = (TableRow)expandView.findViewById(R.id.tblRowSettlement);
		if(pymtTypes.size() > 0){
			tblRowSettlement.setVisibility(View.VISIBLE);
		} else{
			tblRowSettlement.setVisibility(View.GONE);
		}
		
		TextView settCurrencyLabel = (TextView)expandView.findViewById(R.id.settCurrencyLabel);
		settCurrencyLabel.setText(settCurrency);
		TextView settModeLabel = (TextView)expandView.findViewById(R.id.settModeLabel);
		settModeLabel.setText(settMode);
		//Change_Request-20140701, ReqNo. 9 - END
	}

	private void changeButtonStatus() {
		int index = 0;
		
		// Mary@20121018 - Fixes_Request-20121018, ReqNo.5
		isDisplayStatus	= !isDisplayStatus;
				
		for (Iterator<Button> itr=quantityButtons.iterator(); itr.hasNext(); ) {
			Button button	= itr.next();

			TradeOrder order= filteredTradeOrders.get(index);

			toggleQuantityButton(button, order);
			
			index++;
		}
	}
	
	// Added by Mary@20121018 - Fixes_Request-20121018, ReqNo.5 - START
	public void toggleQuantityButton(Button button, TradeOrder order){
		// Mary@20121123 - Change_Request-20121106, ReqNo.2
		int intLotSize	= AppConstants.brokerConfigBean.getIntViewQtyMeasurement() == AppConstants.INT_TRADE_QTY_IN_LOT ? order.getIntLotSize() : 1;
		
		if (isDisplayStatus) {
			String statusText	= (String) DefinitionConstants.getOrderStatusDefinitions().get(order.getStatusCode());
			if( order.getAction().equalsIgnoreCase("Buy") )
				statusText	= "B-" + statusText;
			else if( order.getAction().equalsIgnoreCase("Sell") )
				statusText	= "S-" + statusText;
			
			button.setText(statusText);
			headerLabel.setText(getResources().getString(R.string.ord_title_order_status));
		} else {
			//Added by Thinzar, Fixes_Request-20150601, ReqNo.1
			button.setText( FormatUtil.formatLong( order.getQuantity(), "-" ) );
			headerLabel.setText(getResources().getString(R.string.ord_title_order_qty));
		}
	}
	// Added by Mary@20121018 - Fixes_Request-20121018, ReqNo.5 - END
	
	public void updateGlobalTradeOrders(List<TradeOrder> tradeOrders, List<String> orderTicket2){
		DefinitionConstants.Debug("tradeDebug==OrderBookActivity, updateGlobalTradeOrders()");
		List<String> orderTicket1	= new ArrayList<String>();
		//if(tradeOrders!=null){
		
		try{
			for (Iterator<TradeOrder> itr=tradeOrders.iterator(); itr.hasNext(); ) {
				TradeOrder order= itr.next();
				orderTicket1.add( order.getOrderTicket().substring(1,order.getOrderTicket().length() - 1) );
			}
	
			for(int i=0; i<orderTicket1.size(); i++){
				boolean isExists	= false;
				
				for(int j=0; j<orderTicket2.size(); j++){
					
					if (orderTicket1.get(i).equals( orderTicket2.get(j) ) ){
						isExists		= true;
						TradeOrder order= tradeOrders.get(i);
						/*Edited by Thinzar, Change_Request-20140801, ReqNo.2 - START
						 * The root cause seems like, an order was removed and append at the position zero of the list, but when next time compare, the ordering got messed up
						 * After confirming with Grace, I will add back the order at the index it was removed.
						*/

						TradeOrder Globalorder	= DefinitionConstants.tradeOrders.get(j);
						Globalorder.setOrderNo(order.getOrderNo());
						Globalorder.setAccountNo(order.getAccountNo());
						Globalorder.setAction(order.getAction());
						Globalorder.setStatusCode(order.getStatusCode());
						Globalorder.setStatusText(order.getStatusText());
						Globalorder.setQuantity(order.getQuantity());
						// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - START
						Globalorder.setDisclosedQuantity( order.getDisclosedQuantity() );
						Globalorder.setMinimumQuantity( order.getMinimumQuantity() );
						// Added by Mary@20121129 - Change_Request-20121106, ReqNo.1 - END
						Globalorder.setPrice(order.getPrice());
						Globalorder.setValue(order.getValue());
						Globalorder.setSymbolCode(order.getSymbolCode());
						Globalorder.setStockCode(order.getStockCode());
						Globalorder.setOrderType(order.getOrderType());
						Globalorder.setValidity(order.getValidity());
						Globalorder.setMatchedQuantity(order.getMatchedQuantity());
						Globalorder.setUnmatchQuantity(order.getUnmatchQuantity());
						Globalorder.setMatchedPrice(order.getMatchedPrice());
						Globalorder.setMatchedValue(order.getMatchedValue());
						Globalorder.setOrderSource(order.getOrderSource());
						Globalorder.setOrderDate(order.getOrderDate());
						Globalorder.setRemark(order.getRemark());
						Globalorder.setFixedNo(order.getFixedNo());
						Globalorder.setExpiryDate(order.getExpiryDate());
						Globalorder.setPaymentType(order.getPaymentType());
						Globalorder.setStrSettlementCurrency(order.getStrSettlementCurrency());
						Globalorder.setLastUpdate(order.getLastUpdate());
						Globalorder.setStopLimit(order.getStopLimit());
						Globalorder.setOrderTicket(order.getOrderTicket());

						break;
					}
				}
	
				if( ! isExists )
					DefinitionConstants.tradeOrders.add( tradeOrders.get(i) );
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//Added by Thinzar@20140926, Fixes_Request-20140820, ReqNo. 28 - START
		List<TradeOrder> orders 		= SortOrders(DefinitionConstants.tradeOrders); 	//SortArrayListByDate(DefinitionConstants.tradeOrders);	//Edited by Thinzar, Change_Request-20160101, ReqNo.5
		DefinitionConstants.tradeOrders = orders;
		//Added by Thinzar@20140926, Fixes_Request-20140820, ReqNo. 28 - END
	}

	@Override
	public void OnTradeRefreshEvent(List<TradeOrder> tradeorders) {
		DefinitionConstants.Debug("tradeDebug==OrderBookActivity, OnTradeRefreshEvent()");

		Message message	= handler2.obtainMessage();
		message.obj		= tradeorders;
		
		if(isConstructing || onStopThread){
		//if(k>0){
			handler2.removeMessages(1);
			//handler2.removeCallbacksAndMessages(1);
			
		}else{
		handler2.sendMessage(message);
		message.what = 1;
		}
	}

	final Handler handler2 = new Handler(){
		public void handleMessage(Message message){
			List<String> orderTicket2 = new ArrayList<String>();

			for(Iterator<TradeOrder> itr=DefinitionConstants.tradeOrders.iterator(); itr.hasNext(); ){
				TradeOrder Updateorder = itr.next();
				orderTicket2.add( Updateorder.getOrderTicket().substring(1, Updateorder.getOrderTicket().length() - 1) );
			}

			updateGlobalTradeOrders((List<TradeOrder>)message.obj, orderTicket2);
			isConstructing		=	true;			//edited by Thinzar, Fixes_Request-20140820,ReqNo.27

			// Mary@20130503 - Fixes_Request-20130419, ReqNo.13
			filteredTradeOrders	= getFilteredTradeOrderList2(DefinitionConstants.tradeOrders, strSelectedFilterOrderType, strSelectedFilterExchangeType, strSelectedFilterPayType);
			OrderBookActivity.this.constructTableView(filteredTradeOrders);
		}
	};

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;

			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(OrderBookActivity.this.mainMenu.isShowing()){
					OrderBookActivity.this.mainMenu.hide();
				}

				if(loginThread!=null){
					loginThread.stopRequest();
				}
				if(statusThread!=null){
					statusThread.stopRequest();
				}
				LoginActivity.refreshThread.stopRequest();
				LoginActivity.QCAlive.stopRequest();
				if(filterListView.isShowing()){
					filterListView.hide();
				}
				if(!isFinishing()){
					OrderBookActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {

	}
	
	// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - START
	private void doExchangeList() {
		if( exchgListView.isShowing() ){
			exchgListView.hide();
		}else{
			if( mainMenu.isShowing() )
				mainMenu.hide();
			
			exchgListView.show(findViewById(R.id.RelativeLayoutContainer), selectedExchangeMap);
		}
	}
	
	public void exchangeListSelectedEvent(Map<String, Object> exchangeMap, Sector sector) {
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
		selectedExchangeMap				= exchangeMap;
	
		strSelectedFilterExchangeType	= String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
		btnExchangeType.setText( String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );
		
		isConstructing=true;

		filteredTradeOrders	= getFilteredTradeOrderList2(DefinitionConstants.tradeOrders, strSelectedFilterOrderType, strSelectedFilterExchangeType,strSelectedFilterPayType);
		OrderBookActivity.this.constructTableView(filteredTradeOrders);
	}
	// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - END
	
	// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - START
	private List<TradeOrder> getFilteredTradeOrderList(List<TradeOrder> lstTradeOrder, String strSelectedFilterOrderType, String strSelectedFilterExchgType){
		List<TradeOrder> filteredList	= new ArrayList<TradeOrder>();
		
		// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - START
		for(Iterator<TradeOrder> itr=DefinitionConstants.tradeOrders.iterator(); itr.hasNext(); ){
			TradeOrder order		= itr.next();
			List<String> StatusCode = DefinitionConstants.getStatusCodeMapping().get(strSelectedFilterOrderType);

			TradeOrder tempTO		= null;
			if(strSelectedFilterOrderType == null && strSelectedFilterExchangeType == null){
				tempTO	= order;
			}else{
				if(strSelectedFilterOrderType != null){
					if( strSelectedFilterOrderType.equals( getResources().getString(R.string.all_order_type_label) ) ){
						tempTO	= order;
					}else{
						
						for(int i=0; i<StatusCode.size(); i++){
							if( order.getStatusCode().equals( StatusCode.get(i) ) ){
								tempTO = order;
								break;
							}
						}
					}
				}
				
				if(strSelectedFilterExchangeType != null){
					if( strSelectedFilterExchangeType.equalsIgnoreCase(ALL_TYPE_KEY) ){
						if(tempTO == null && strSelectedFilterOrderType == null)
							tempTO = order;
					}else{

						String strCurrTradeExchgCode	= order.getSymbolCode().substring( (order.getSymbolCode().lastIndexOf(".") + 1), order.getSymbolCode().length() );
						strCurrTradeExchgCode			= ( strCurrTradeExchgCode.length() > 1 && strCurrTradeExchgCode.toUpperCase().endsWith("D") ) ? strCurrTradeExchgCode.substring(0, strCurrTradeExchgCode.length() - 1) : strCurrTradeExchgCode;
						if( strSelectedFilterExchangeType.equalsIgnoreCase(strCurrTradeExchgCode) ){
							if(tempTO == null && strSelectedFilterOrderType == null)
								tempTO = order;
						}else{
							if(tempTO != null)
								tempTO = null;
						}
					}
				}
			}
			
			if(tempTO != null)
				filteredList.add(tempTO);

		}
		// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - END
		
		return filteredList;
	}
	// Added by Mary@20130503 - Fixes_Request-20130419, ReqNo.13 - END
	
	//Added by Thinzar@20140731 0 Change_Request-20140701, ReqNo. 9 - START
	//This function is an extended version of the above one, getFilteredTradeOrderList after paymentType is added to filter
	private List<TradeOrder> getFilteredTradeOrderList2(List<TradeOrder> lstTradeOrder, String strSelectedFilterOrderType, String strSelectedFilterExchgType, String strSelectedFilterPayType){
		DefinitionConstants.Debug("tradeDebug==OrderBookActivity, getFilteredTradeOrderList2()");

		List<TradeOrder> filteredList	= new ArrayList<TradeOrder>();

		if(lstTradeOrder != null) {	//Added to fix crashlytics
			for (Iterator<TradeOrder> itr = lstTradeOrder.iterator(); itr.hasNext(); ) {
				TradeOrder order = itr.next();
				List<String> StatusCode = DefinitionConstants.getStatusCodeMapping().get(strSelectedFilterOrderType);

				TradeOrder tempTO = null;
				int condition = 0;

				//1. checked for order status
				if (strSelectedFilterOrderType == null || strSelectedFilterOrderType.equalsIgnoreCase(getResources().getString(R.string.all_order_type_label)))
					condition++;
				else {
					if (StatusCode.contains(order.getStatusCode())) {
						condition++;
					}
					//Fixes_Request-20170701, ReqNo.17 - START
					else{
						if(strSelectedFilterOrderType.equalsIgnoreCase(getResources().getString(R.string.orderbook_filter_filled))){
							DefinitionConstants.Debug("debug==filter by filled.. checking for cancelled and expired orders.");
							if(order.getStatusCode().equals("C") || order.getStatusCode().equals("4")){
								if(order.getMatchedQuantity() > 0)
									condition++;
							}
						}
					}
					//Fixes_Request-20170701, ReqNo.17 - END
				}

				//2. filter exchange
				if (strSelectedFilterExchangeType == null || strSelectedFilterExchangeType.equalsIgnoreCase(ALL_TYPE_KEY))
					condition++;
				else {
					String strCurrTradeExchgCode = order.getSymbolCode().substring((order.getSymbolCode().lastIndexOf(".") + 1), order.getSymbolCode().length());
					strCurrTradeExchgCode = (strCurrTradeExchgCode.length() > 1 && strCurrTradeExchgCode.toUpperCase().endsWith("D")) ? strCurrTradeExchgCode.substring(0, strCurrTradeExchgCode.length() - 1) : strCurrTradeExchgCode;

					if (strSelectedFilterExchangeType.equalsIgnoreCase(strCurrTradeExchgCode))
						condition++;
				}

				//3. filter payment type
				if (strSelectedFilterPayType == null || strSelectedFilterPayType.equalsIgnoreCase(getResources().getString(R.string.all_pymt_type_label)))
					condition++;
				else {
					if (strSelectedFilterPayType.equalsIgnoreCase(order.getPaymentType()))
						condition++;
				}

				if (condition == 3) {
					tempTO = order;
				} else {
					tempTO = null;
				}

				if (tempTO != null)
					filteredList.add(tempTO);
			}
		}
		
		return filteredList;
	}
	//Added by Thinzar@20140731 0 Change_Request-20140701, ReqNo. 9 - END

	//Added by Thinzar@20140731 - Change_Request-20140701, ReqNo.9 - START
	View.OnClickListener paymentFilter = new OnClickListener(){

		@Override
		public void onClick(View v) {
			if(idleThread != null && !isShowingIdleAlert)
				idleThread.resetExpiredTime();
			
			OrderBookActivity.this.doFilterPayment();
		}
		
	};
	
	private void doFilterPayment(){
		
		if(payFilterListView.isShowing()){
			payFilterListView.hide();
		} else{
			if(mainMenu.isShowing()){
				mainMenu.hide();
			}
			payFilterListView.show(findViewById(R.id.RelativeLayoutContainer), strSelectedFilterPayType);
		}
	}
	
	@Override
	public void payTypeFilterSelectedEvent(String payTypeFilter) {
		this.strSelectedFilterPayType = payTypeFilter;
		if(payTypeFilter != null){
			btnPaymentType.setText(payTypeFilter);
		}
		isConstructing		= true;		//added by Thinzar, Fixes_Request-20140820,ReqNo.27
		filteredTradeOrders	= getFilteredTradeOrderList2(DefinitionConstants.tradeOrders, strSelectedFilterOrderType, strSelectedFilterExchangeType, strSelectedFilterPayType);
		constructTableView(filteredTradeOrders);
	}
	//Added by Thinzar@20140731 - Change_Request-20140701, ReqNo.9 - END
}