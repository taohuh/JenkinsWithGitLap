package com.n2nconnect.android.stock.activity;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.AtpMessageParser;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.util.AtpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class ForgetPasswordActivity extends CustomWindow {

	private TextView hintTitleLabel;
	private TextView hintAnswerLabel;
	private TextView hintLabel;
	private EditText hintField;
	private EditText usernameField;
	private Button submitButton;
	private String hintQuestion;
	private String idStatus;
	private String hintEmail;
	private String userEmail;
	/* Mary@20130605 - comment unused variable
	private String hintAnswer;
	private String encryptedKey = null;
	private byte[] userParamKeyInByte = null;
	*/
	private Button retrieveButton;
	private ScrollView passwordScroll;
	private String tradeForgetNewPwd;
	private ProgressDialog dialog;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr = 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.forget_password);
	
			dialog = new ProgressDialog(this);
	
			AppConstants.showMenu = false;
			this.menuButton.setVisibility(View.GONE);
			this.title.setText(getResources().getString(R.string.forgot_password_module_title));
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(R.drawable.menuicon_acc_setting);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntAccSettingMenuIconBgImg() );
			usernameField = (EditText) this.findViewById(R.id.usernameField);
			usernameField.setSingleLine();
			hintTitleLabel = (TextView) this.findViewById(R.id.hintTitleLabel);
			hintTitleLabel.setText("");
			hintAnswerLabel = (TextView) this.findViewById(R.id.hintAnswerLabel);
			hintAnswerLabel.setText("");
			hintLabel = (TextView) this.findViewById(R.id.hintLabel);
			hintLabel.setText("");
			hintField = (EditText) this.findViewById(R.id.hintField);
			hintField.setVisibility(View.GONE);
			submitButton = (Button) this.findViewById(R.id.submitButton);
			submitButton.setVisibility(View.GONE);
			
			// Added by Mary@20130104 - additional information to ease user process - START
			if(this.getIntent() != null){
				Bundle bundle 		= this.getIntent().getExtras();
				usernameField.setText( bundle.getString(ParamConstants.USERNAME_TAG) );
			}
			// Added by Mary@20130104 - additional information to ease user process - END
			
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(ForgetPasswordActivity.this)
			//.setIcon(android.R.attr.alertDialogStyle)
			.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,	int id) {
					dialog.dismiss();
				}
			})
			.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			retrieveButton = (Button) this.findViewById(R.id.retrieveButton);
			retrieveButton.setOnClickListener(new OnClickListener() {
	
				public void onClick(View view) {
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
					try{
						//ForgetPasswordActivity.this.handleRetrieveClickEvent();
						if (usernameField.getText().toString().trim().equals("")) {
							AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
							builder.setMessage(getResources().getString(R.string.keyin_username))
									.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											dialog.dismiss();
										}
									});
							AlertDialog alert = builder.create();
							// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
							if( ! ForgetPasswordActivity.this.isFinishing() )
								alert.show();
							
							return;
						}
		
						ForgetPasswordActivity.this.handleRetrieveClickEvent2();
						hintField.requestFocus();
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(hintField, InputMethodManager.SHOW_IMPLICIT);
						Handler keyboardHandler = new Handler();
						keyboardHandler.postDelayed(new Runnable() {
		
							public void run() {
		
								passwordScroll.scrollBy(0, 63);
		
							}
						}, 500);
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
					}catch(Exception e){
						e.printStackTrace();
					}
					// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
				}
			});
	
			passwordScroll = (ScrollView) ForgetPasswordActivity.this.findViewById(R.id.passwordScroll);
			passwordScroll.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View view, MotionEvent event) {
					return true;
				}
			});
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	/*
	private void handleRetrieveClickEvent() {

		String username = usernameField.getText().toString().trim();
		if (username.length() == 0) {
			return;
		}

		String senderCode = sharedPreferences.getString(PreferenceConstants.SENDER_CODE, null);
		Map parameters = new HashMap();
		parameters.put(ParamConstants.USERNAME_TAG, username);
		parameters.put(ParamConstants.ADMINKEY_TAG, AppConstants.ADMIN_KEY);

		try {
			if (AppConstants.EnableEncryption == true) {
				PublicKey RSAPublicKey = AtpConnectUtil.tradeGetPK();
				// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				while (intRetryCtr < AppConstants.intMaxRetry && RSAPublicKey == null) {
					intRetryCtr++;
					RSAPublicKey = AtpConnectUtil.tradeGetPK();
				}
				intRetryCtr = 0;
				if(RSAPublicKey == null){
					alertException.setMessage(ErrorCodeConstants.FAIL_GET_PUBLIC_KEY);
					alertException.show();
					return;
				}
				// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				String[] AESKey = AtpConnectUtil.tradeGetKey2(RSAPublicKey);
				// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				while (intRetryCtr < AppConstants.intMaxRetry && AESKey == null) {
					intRetryCtr++;
					AESKey 	= AtpConnectUtil.tradeGetKey2(RSAPublicKey);
				}
				intRetryCtr = 0;

				if (AESKey != null) {
					encryptedKey = AESKey[0];
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					userParamKeyInByte = AtpConnectUtil.tradeGetSession2(AESKey);
					while (intRetryCtr < AppConstants.intMaxRetry && userParamKeyInByte == null) {
						intRetryCtr++;
						userParamKeyInByte = AtpConnectUtil.tradeGetSession2(AESKey);
					}
					intRetryCtr = 0;

					if (userParamKeyInByte == null) {
						alertException.setMessage(ErrorCodeConstants.FAIL_GET_ENCRYPTED_SESSION);
						alertException.show();
						return;
					}
				} else {
					alertException.setMessage(ErrorCodeConstants.FAIL_GET_ENCRYPTED_KEY);
					alertException.show();
					return;
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}

				if (encryptedKey != null) {
					AppConstants.setEncryptedKey(encryptedKey);
				}
				parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, userParamKeyInByte);
			}

			String response = AtpConnectUtil.getHintInfo(parameters);

			Map hints = AtpMessageParser.extractHintInfo(response);
			if (hints != null) {
				hintQuestion = (String) hints.get(DefinitionConstants.HINT_QUESTION);
				idStatus = (String) hints.get(DefinitionConstants.ID_STATUS);
				userEmail = (String) hints.get(DefinitionConstants.EMAIL);
			}

			if (idStatus.equals("S")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(
						"Login ID suspended! Please contact System Administrator to activate your account.")
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.dismiss();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
				return;
			}

			if (idStatus.equals("P")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(
						"Our system shows that you have not activate your account. Please activate your Account via the URL Link that was sent to your registered email "
								+ userEmail + ".").setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
				return;
			}

			if (idStatus.equals("X")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(
						"Please contact our System Administrator for more information.")
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.dismiss();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
				return;
			}

			if (idStatus.equals("C")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(
						"Your account had been Closed. Please contact our System Administrator.")
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.dismiss();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
				return;
			}

			hintTitleLabel.setText("Hint to Password");
			hintLabel.setText((hintQuestion.indexOf("?") == -1) ? hintQuestion
					+ "?" : hintQuestion);
			hintAnswerLabel.setText("Answer to Hint");
			hintField.setVisibility(View.VISIBLE);
			hintField.setSingleLine();
			submitButton.setVisibility(View.VISIBLE);

			usernameField.setEnabled(false);
			retrieveButton.setEnabled(false);

			submitButton.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					ForgetPasswordActivity.this.handleSubmitClickEvent();
				}
			});

		} catch (FailedAuthenicationException e) {
			e.printStackTrace();
			// this.sessionExpired();
		}
	}
	*/

	/* Mary@20130103 - Fixes_Request-20121102, ReqNo.12
	private void handleSubmitClickEvent() {
		EditText hintField = (EditText) this.findViewById(R.id.hintField);
		String hintText = hintField.getText().toString();
		// String hintEmail = null;
		if (hintText.equals("")) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Please enter Password hint").setPositiveButton(
					"OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		}

		String username = usernameField.getText().toString().trim();

		Map parameters = new HashMap();
		parameters.put(ParamConstants.USERNAME_TAG, username);
		parameters.put(ParamConstants.ADMINKEY_TAG, AppConstants.ADMIN_KEY);
		parameters.put(ParamConstants.HINT_QUESTION_TAG, hintQuestion);
		parameters.put(ParamConstants.HINT_ANSWER_TAG, hintText);

		// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - START
		if(AppConstants.EnableEncryption)
			parameters.put(ParamConstants.USERPARAM_INBYTE_TAG, AppConstants.userParamInByte);
		else
			parameters.put(ParamConstants.USERPARAM_TAG, AppConstants.userParam);
		// Added by Mary@20121226 - Fixes_Request-20121102,ReqNo.8 - END
		
		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		try {
			String response = AtpConnectUtil.tradeForgetPasswd2(parameters,	AppConstants.CHANGE_PASSWORD_TYPE);

			if (response.indexOf("Error") == -1) {

				Map tradeForget = AtpMessageParser.extractTradeForgetPwdInfo(response);

				String errorCode = (String) tradeForget.get(DefinitionConstants.ERROR_CODE);
				String errorMessage = (String) tradeForget.get(DefinitionConstants.ERROR_MESSAGE);

				if (!errorCode.equals("0")) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(errorMessage).setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,	int id) {
									dialog.dismiss();
								}
							});
					AlertDialog alert = builder.create();
					alert.show();
				} else {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(
							"Password reset successfully. Please check your email.")
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											dialog.dismiss();
											Intent intent = new Intent();
											intent.setClass(
													ForgetPasswordActivity.this,
													LoginActivity.class);
											ForgetPasswordActivity.this
													.startActivity(intent);
										}
									});
					AlertDialog alert = builder.create();
					alert.show();
				}
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(response).setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}

		} catch (FailedAuthenicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// this.sessionExpired();
		}
		/
		try {
			String response = AtpConnectUtil.tradeForgetPasswd2(parameters,	AppConstants.CHANGE_PASSWORD_TYPE);
			
			while(intRetryCtr < AppConstants.intMaxRetry && response == null){
				intRetryCtr++;
				response = AtpConnectUtil.tradeForgetPasswd2(parameters, AppConstants.CHANGE_PASSWORD_TYPE);
			}
			intRetryCtr	= 0;
			
			if(response == null){						
				throw new Exception(ErrorCodeConstants.FAIL_CHANGE_PASSWORD_2);
			}
			
			if (response.toUpperCase().indexOf("ERROR") == -1){
				Map tradeForget 	= AtpMessageParser.extractTradeForgetPwdInfo(response);
				String errorCode 	= (String) tradeForget.get(DefinitionConstants.ERROR_CODE);
				String errorMessage = (String) tradeForget.get(DefinitionConstants.ERROR_MESSAGE);

				if ( ! errorCode.equals("0") ) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(errorMessage).setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
				} else {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("Password reset successfully. Please check your email.")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							Intent intent = new Intent();
							intent.setClass(ForgetPasswordActivity.this, LoginActivity.class);
							ForgetPasswordActivity.this.startActivity(intent);
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
			}else{
				throw new Exception(response);
			}
		}catch (Exception e){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage( e.getMessage() )
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}
	*/

	private void handleRetrieveClickEvent2() {
		new getAccountInfo().execute();
	}

	private class getAccountInfo extends AsyncTask<Void, Void, String> {

		private AsyncTask<Void, Void, String> updateTask;
		private boolean isException	= false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				updateTask = this;
				dialog.setMessage(getResources().getString(R.string.retrieving_account));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ForgetPasswordActivity.this.isFinishing() )
					dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String username = usernameField.getText().toString().trim();
			if (username.length() == 0) {
				return null;
			}

			Map<String, Object> parameters 		= new HashMap<String, Object>();
			parameters.put(ParamConstants.USERNAME_TAG, username);
			parameters.put(ParamConstants.ADMINKEY_TAG, AppConstants.ADMIN_KEY);

			
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			try {
				String response = null; = AtpConnectUtil.getAccountInfo(parameters);
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				updateTask.cancel(true);
				new getAccountInfo().execute();
				e.printStackTrace();
			}
			*/
			String response = null;
			try {
				response = AtpConnectUtil.getAccountInfo(parameters);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response = AtpConnectUtil.getAccountInfo(parameters);
				}
				intRetryCtr	= 0;
				if(response == null){						
					throw new Exception(ErrorCodeConstants.FAIL_RETRIEVE_ACCINFO);
				}
			} catch (Exception e) {
				isException = true;
				response	= e.getMessage();
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			return response;
		}

		@Override
		protected void onPostExecute(String response) {
			super.onPostExecute(response);

			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			try{
				if(dialog != null && dialog.isShowing() )
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
					dialog.dismiss();
				if (response != null) {
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2
					// if (response.indexOf("Error") != -1) {
					if (response.toUpperCase().indexOf("ERROR") != -1 || isException) {
						AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
						builder.setMessage(response).setPositiveButton(getResources().getString(R.string.dialog_button_ok),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
									}
								});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ForgetPasswordActivity.this.isFinishing() )
							alert.show();
						
						return;
					}
					Map<String, String> hints = AtpMessageParser.extractHintInfo2(response);
					if (hints.size() != 0) {
						hintQuestion= hints.get(DefinitionConstants.HINT_QUESTION);
						/* Mary@20130605 - comment unused variable
						hintAnswer	= (String) hints.get(DefinitionConstants.HINT_ANSWER);
						*/
						hintEmail	= hints.get(DefinitionConstants.HINT_EMAIL);
						idStatus	= hints.get(DefinitionConstants.ID_STATUS);
	
						if (idStatus.equals("S")) {
							AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
							builder.setMessage(getResources().getString(R.string.status_s_login_suspended))
									.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog, int id) {
													dialog.dismiss();
												}
											});
							AlertDialog alert = builder.create();
							// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
							if( ! ForgetPasswordActivity.this.isFinishing() )
								alert.show();
							
							return;
						}
	
						if (idStatus.equals("P")) {
							AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
							builder.setMessage(getResources().getString(R.string.status_p_account_not_activated)
											+ userEmail + ".").setPositiveButton(
									getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											dialog.dismiss();
										}
									});
							AlertDialog alert = builder.create();
							// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
							if( ! ForgetPasswordActivity.this.isFinishing() )
								alert.show();
							
							return;
						}
	
						if (idStatus.equals("X")) {
							AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
							builder.setMessage(
									getResources().getString(R.string.status_x_contact_admin))
									.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
											new DialogInterface.OnClickListener() {
												public void onClick( DialogInterface dialog, int id) {
													dialog.dismiss();
												}
											});
							AlertDialog alert = builder.create();
							// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
							if( ! ForgetPasswordActivity.this.isFinishing() )
								alert.show();
							
							return;
						}
	
						if (idStatus.equals("C")) {
							AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
							builder.setMessage(
									getResources().getString(R.string.status_c_account_close))
									.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
											new DialogInterface.OnClickListener() {
												public void onClick( DialogInterface dialog, int id) {
													dialog.dismiss();
												}
											});
							AlertDialog alert = builder.create();
							// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
							if( ! ForgetPasswordActivity.this.isFinishing() )
								alert.show();
							
							return;
						}
	
						hintTitleLabel.setText(getResources().getString(R.string.hint_to_password));
						hintLabel.setText((hintQuestion.indexOf("?") == -1) ? hintQuestion + "?" : hintQuestion);
						hintAnswerLabel.setText("Answer to Hint");
						hintField.setVisibility(View.VISIBLE);
						hintField.requestFocus();
						hintField.setSingleLine();
						submitButton.setVisibility(View.VISIBLE);
	
						usernameField.setEnabled(false);
						retrieveButton.setEnabled(false);
	
					} else {
						AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
						builder.setMessage(getResources().getString(R.string.invalid_username))
								.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
										public void onClick( DialogInterface dialog, int id) {
											dialog.dismiss();
										}
									});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ForgetPasswordActivity.this.isFinishing() )
							alert.show();
						
						return;
					}
	
					submitButton.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							ForgetPasswordActivity.this.handleSubmitClickEvent2();
						}
					});
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}

	/* Mary@20130528 - Fixes_Request-20130523, ReqNo.2
	private void RegisterMailEvent() {
	*/
	private void RegisterMailEvent() throws Exception {
		// String mailType =
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(ParamConstants.HINT_EMAIL_TAG, hintEmail);
		// parameters.put(ParamConstants.HINT_EMAIL_TAG, "yee.ase3@gmail.com");
		parameters.put(ParamConstants.TRADEFORGET_NEWPWD_TAG, tradeForgetNewPwd);

		/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
		try {
			String response = AtpConnectUtil.RegisterMail(parameters, AppConstants.CHANGE_PASSWORD_TYPE);
		} catch (FailedAuthenicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// this.sessionExpired();
		}
		*/
		try {
			String response = AtpConnectUtil.RegisterMail(parameters, AppConstants.CHANGE_PASSWORD_TYPE);
			while(intRetryCtr < AppConstants.intMaxRetry && response == null){
				intRetryCtr++;
				response = AtpConnectUtil.RegisterMail(parameters, AppConstants.CHANGE_PASSWORD_TYPE);
			}
			intRetryCtr	= 0;
			
			if(response == null)				
				throw new Exception(ErrorCodeConstants.FAIL_REGISTER_MAIL);
			
		}catch(Exception e){
			/* Mary@20130528 - Fixes_Request-20130523, ReqNo.2
			alertException.setMessage( e.getMessage() );
			alertException.show();
			return;
			*/
			throw e;
		}
		// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	}

	private void handleSubmitClickEvent2() {
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
		try{
			EditText hintField 	= (EditText) this.findViewById(R.id.hintField);
			String hintText		= hintField.getText().toString();
	
			if (hintText.trim().equals("")) {
				AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
				builder.setMessage(getResources().getString(R.string.keyin_hint_answer))
						.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
									}
								});
				AlertDialog alert = builder.create();
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ForgetPasswordActivity.this.isFinishing() )
					alert.show();
	
				return;
			}
	
			new processRequest().execute();
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	}

	private class processRequest extends AsyncTask<Void, Void, String> {

		private AsyncTask<Void, Void, String> updateTask;
		// Added by Mary@20130103 - Fixes_Request-20121102, ReqNo.12
		private boolean isException = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				dialog.setMessage(getResources().getString(R.string.processing_request));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ForgetPasswordActivity.this.isFinishing() )	
					dialog.show();
				
				updateTask = this;
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Map<String, Object> parameters = new HashMap<String, Object>();
			String username = usernameField.getText().toString().trim();
			parameters.put(ParamConstants.USERNAME_TAG, username);
			parameters.put(ParamConstants.ADMINKEY_TAG, AppConstants.ADMIN_KEY);
			parameters.put(ParamConstants.HINT_QUESTION_TAG, hintQuestion);
			parameters.put(ParamConstants.HINT_ANSWER_TAG, hintField.getText().toString().trim());
			// parameters.put(ParamConstants.HINT_EMAIL_TAG,
			// "yee.ase3@gmail.com");
			parameters.put(ParamConstants.HINT_EMAIL_TAG, hintEmail);

			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			try {
				String response = AtpConnectUtil.tradeForgetPasswd(parameters, AppConstants.CHANGE_PASSWORD_TYPE);

				DefinitionConstants.Debug("response: " + response);

				return response;
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
			*/
			try {
				String response = AtpConnectUtil.tradeForgetPasswd(parameters, AppConstants.CHANGE_PASSWORD_TYPE);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					response = AtpConnectUtil.tradeForgetPasswd(parameters, AppConstants.CHANGE_PASSWORD_TYPE);
				}
				intRetryCtr	= 0;

				if(response == null)						
					throw new Exception(ErrorCodeConstants.FAIL_FORGET_PASSWORD_REQ);
				
				return response;
			}catch(Exception e){
				// Added by Mary@20130103 - Fixes_Request-20121102, ReqNo.12
				isException 	= true;
				return e.getMessage();
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		}

		@Override
		protected void onPostExecute(String response) {
			super.onPostExecute(response);

			try{
				if(dialog != null && dialog.isShowing() )	// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6			
					dialog.dismiss();
	
				if (response == null) {
					return;
				}
				
				//Fixes_Request-20161101, ReqNo.14
				Map<String, String> responseMap	= AtpMessageParser.parseTradePasswdPIN(response);
				String statusCode		= responseMap.get(ParamConstants.STATUS_CODE_TAG);
				String statusMsg		= responseMap.get(ParamConstants.STATUS_MSG_TAG);
				
				if(statusCode.equals("0")){
					showAlert(getResources().getString(R.string.password_reset_success_msg));
				} else{
					showAlert(statusMsg);
				}
				
				/* Commented by Thinzar, Fixes_Request-20161101, ReqNo.14
				if(response.toUpperCase().indexOf("ERROR") != -1 || response.toUpperCase().indexOf("INVALID") != -1 || isException) {
					AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
					builder.setMessage(response).setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.dismiss();
								}
							});
					AlertDialog alert = builder.create();
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! ForgetPasswordActivity.this.isFinishing() )
						alert.show();
				} else {
					Map<String, String> newPasswordInfo = AtpMessageParser.extractNewPasswordInfo(response);
					tradeForgetNewPwd 					= newPasswordInfo.get(DefinitionConstants.TRADE_FORGET_PWD);
					
					if( ! AppConstants.brokerConfigBean.isEnableEncryption() && ( tradeForgetNewPwd == null || ( tradeForgetNewPwd != null && tradeForgetNewPwd.trim().equals("0") ) ) ){
						AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
						builder.setMessage("Password hint not match")
								.setPositiveButton("OK", new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											dialog.dismiss();
										}
									});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ForgetPasswordActivity.this.isFinishing() )
							alert.show();
						
						return;
					}
	
					// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.2
					try{
						//RegisterMailEvent();		//Commented out by Thinzar@20141009, Fixes_Request-20140929,ReqNo.8
		
						AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPasswordActivity.this);
						builder.setMessage("Password reset successfully. Please check your email.")
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int id) {
												dialog.dismiss();
												Intent intent = new Intent();
												intent.setClass(ForgetPasswordActivity.this, LoginActivity.class);
												ForgetPasswordActivity.this.startActivity(intent);
											}
										});
						AlertDialog alert = builder.create();
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ForgetPasswordActivity.this.isFinishing() )
							alert.show();
					// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.2 - START
					}catch(Exception e){
						alertException.setMessage( e.getMessage() );
						// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
						if( ! ForgetPasswordActivity.this.isFinishing() )
							alertException.show();
					}
					// Added by Mary@20130528 - Fixes_Request-20130523, ReqNo.2 - END
				}
				//Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
				*/
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
	}
	
	private void showAlert(String message){
		AlertDialog.Builder builder	= new AlertDialog.Builder(this);
		builder.setMessage(message)
		.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				dialog.dismiss();
			}
		});
		
		builder.show();
	}
}
