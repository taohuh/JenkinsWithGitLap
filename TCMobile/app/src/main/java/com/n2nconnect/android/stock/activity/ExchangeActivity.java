package com.n2nconnect.android.stock.activity;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QCAliveThread;
import com.n2nconnect.android.stock.QCAliveThread.OnQCAliveListener;
import com.n2nconnect.android.stock.QCBean;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.ExchangeInfo;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.model.TradeAccount;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class ExchangeActivity extends CustomWindow implements OnDoubleLoginListener, OnQCAliveListener, OnRefreshStockListener {

	private List<ExchangeInfo> exchangeInfos;
	private ExchangeInfo selectedTradeExchange;
	private RadioButton selectedRadio;
	private Map<String, ExchangeInfo> exchangeSectors;
	private QCAliveThread QCAliveThread;
	private RefreshStockThread refreshThread;
	private DoubleLoginThread loginThread;
	private String selectedTradeExchangeCode;
	private String selectedQCExchangeCode;
	private ProgressDialog dialog;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	/* Mary@20130521 - Fixes_Request-20130424, ReqNo.10
	private int intRetryCtr	= 0;
	*/
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	// Added by Mary@20130521 - Fixes_Request-20130424, ReqNo.10
	private TableRow selectedTableRow;
	
	
	public void onCreate(Bundle savedInstanceState) {
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
		try{
			super.onCreate(savedInstanceState);
			
			setContentView(R.layout.stock_exchange);
	
			this.title.setText(getResources().getString(R.string.exchange_module_title));
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(R.drawable.menuicon_exchg);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntExchgMenuIconBgImg() );
	
			StockApplication application	= (StockApplication) this.getApplication();
			selectedTradeExchange 			= application.getSelectedExchange();
			exchangeInfos 					= application.getExchangeInfos();
			
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(ExchangeActivity.this)
				//.setIcon(android.R.attr.alertDialogStyle)
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6
						try{
							dialog.dismiss();
						// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - START
						}catch(Exception e){
							e.printStackTrace();
						}
						// Added by Mary@20130801 - Fixes_Request-20130523, ReqNo.6 - END
						return;
					}
				})
				.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
	}

	public void onResume() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onResume();
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= ExchangeActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
				
			// Added by Mary@20121116 - Change_Request-20120719, ReqNo.12
			AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, null);
			
			dialog 									= new ProgressDialog(this);
			
			TableLayout tableLayout 				= (TableLayout) this.findViewById(R.id.exchangeTable);
			tableLayout.removeAllViews();
	
			Map<String, String> exchangeNames 		= DefinitionConstants.getExchangeNameMapping();
			Map<String, Integer> exchangeImages 	= DefinitionConstants.getExchangeImageMapping();
			final List<String> QCExchangeCodes 		= DefinitionConstants.getValidQCExchangeCodes();
			final List<String> tradeExchangeCodes	= DefinitionConstants.getTrdExchangeCode();
	
			refreshThread 							= ((StockApplication)this.getApplication()).getRefreshThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(refreshThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					int rate 		= sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
					refreshThread 	= new RefreshStockThread(AppConstants.QCdata.getStrUserParam(), new String[0], rate);
					refreshThread.start();
					((StockApplication) ExchangeActivity.this.getApplication()).setRefreshThread(refreshThread);
				}
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			refreshThread.setRegisteredListener(this);
			// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29
			refreshThread.setSymbolCodes(new String[0]);
			
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if( refreshThread.isPaused() ){
				refreshThread.resumeRequest();
			}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			
			loginThread 							= ((StockApplication)this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) ExchangeActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(ExchangeActivity.this);
				loginThread.resumeRequest();
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			}
			
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(this);
			
			QCAliveThread 						= ((StockApplication)this.getApplication()).getQCAliveThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(QCAliveThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					QCAliveThread = new QCAliveThread(AppConstants.QCdata.getStrUserParam(), AppConstants.QC_KEEP_ALIVE_RATE);
					QCAliveThread.start();
					( (StockApplication) ExchangeActivity.this.getApplication() ).setQCAliveThread(QCAliveThread);
				}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			}else if( QCAliveThread.isPaused() ){
				QCAliveThread.setRegisteredListener(ExchangeActivity.this);
				QCAliveThread.resumeRequest();
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			QCAliveThread.setRegisteredListener(this);
			
			final StockApplication application	= (StockApplication) ExchangeActivity.this.getApplication();
	
			int id 								= 0;
			for (int i=0; i<QCExchangeCodes.size(); i++){
				final TableRow tableRow			= new TableRow(this);   
				tableRow.setId(id);
				tableRow.setLayoutParams( new TableLayout.LayoutParams( TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT) );
				tableRow.setWeightSum(1);
				
				View rowView					= this.getLayoutInflater().inflate(R.layout.exchange_row, null);
				TextView exchangeLabel 			= (TextView) rowView.findViewById(R.id.exchangeLabel);

				//Edited, Change_Request-20170119, ReqNo.7 - START
				/* Commented by Thinzar on 20170427, LMS will provide the new exchange names
				String newExchgeName	= DefinitionConstants.getNewExchangeName(QCExchangeCodes.get(i));
				if(newExchgeName != null)
					exchangeLabel.setText(newExchgeName);
				else
					exchangeLabel.setText( exchangeNames.get( QCExchangeCodes.get(i) ) );
					*/
				exchangeLabel.setText( exchangeNames.get( QCExchangeCodes.get(i) ) );
				//Change_Request-20170119, ReqNo.7 - START
				
				/* Removed exchange Flag in TcMobile 2.0, commented by Thinzar, Change_Request-20160101, ReqNo.3
				//ImageView exchangeImage 		= (ImageView) rowView.findViewById(R.id.exchangeImage);	
				Integer resourceId 				= exchangeImages.get(QCExchangeCodes.get(i));
				
				// Added by Mary@20121101 - avoid NullPointerException
				if(resourceId != null)
					exchangeImage.setImageResource(resourceId.intValue());
				// Added by Mary@20130110 - handle null image source - START
				else
					exchangeImage.setImageResource(android.R.color.transparent);
				// Added by Mary@20130110 - handle null image source - END
				*/
				final RadioButton exchangeRadio = (RadioButton) rowView.findViewById(R.id.exchangeRadio);
				if( QCExchangeCodes.get(i).equals(AppConstants.DEFAULT_EXCHANGE_CODE) ) {
					exchangeRadio.setChecked(true);
					selectedRadio 		= exchangeRadio;
					// Added by Mary@20130521 - Fixes_Request-20130424, ReqNo.10
					selectedTableRow	= tableRow;
				}else{
					exchangeRadio.setChecked(false);
				}
	
				tableRow.addView(rowView);
	
				final LinearLayout rowLayout 	= (LinearLayout) rowView.findViewById(R.id.rowLayout);
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				if( ( ( id+1 ) % 2 ) != 0 )
					rowLayout.setBackgroundResource(R.drawable.tablerow_dark);
				else
					rowLayout.setBackgroundResource(R.drawable.tablerow_light);
				*/
				//rowLayout.setBackgroundResource( ( ( ( id+1 ) % 2 ) != 0 ) ? R.drawable.tablerow_dark : R.drawable.tablerow_light);	//commented, Change_Request-20160101, ReqNo.3
	
				exchangeRadio.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						int rowId						= tableRow.getId();
						selectedRadio.setChecked(false);
						
						// Added by Mary@20130521 - Fixes_Request-20130424, ReqNo.10 - START
						LinearLayout selectedRowLayout 	= (LinearLayout) ( (View)selectedTableRow.getChildAt(0) ).findViewById(R.id.rowLayout);
						//selectedRowLayout.setBackgroundResource( ( ( ( selectedTableRow.getId()+1 ) % 2 ) != 0 ) ? R.drawable.tablerow_dark : R.drawable.tablerow_light);	//commented, Change_Request-20160101, ReqNo.3
						
						selectedTableRow				= tableRow;
						// Added by Mary@20130521 - Fixes_Request-20130424, ReqNo.10 - END
						selectedRadio 					= exchangeRadio;
						exchangeRadio.setChecked(true);
	
						selectedTradeExchangeCode		= tradeExchangeCodes.get(rowId);
						selectedQCExchangeCode 			= QCExchangeCodes.get(rowId);
	
						for(Iterator<ExchangeInfo> itr = exchangeInfos.iterator(); itr.hasNext();){
							ExchangeInfo Info	= itr.next();
							if( Info.getTrdExchgCode() != null && Info.getTrdExchgCode().equals(selectedTradeExchangeCode) )
								selectedTradeExchange = Info;
						}
	
						if(selectedTradeExchange!=null){
							application.setSelectedExchange(selectedTradeExchange);
							application.setSelectedExchangeCode(selectedTradeExchangeCode);
						}
	
						// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2
						AppConstants.noTradeClient		= false;
						TradeAccount account 			= application.getSelectedAccount();
						/* Mary@20130220 - Fixes_Request-20130108, ReqNo.10 - START
						if(selectedTradeExchange.getTrdExchgCode()!=null){
							if(account != null){
								if( ! account.getExchangeCode().equals( selectedTradeExchange.getTrdExchgCode() ) ){
									/* Mary@20130220 - Fixes_Request-20130108, ReqNo.10
									List tradeAccounts = application.getTradeAccounts(selectedTradeExchange.getTrdExchgCode());
									/
									List tradeAccounts	= application.getTradeAccounts();
									if (tradeAccounts.size() > 0)
										application.setSelectedAccount( (TradeAccount) tradeAccounts.get(0) );
									// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2 - START
									else
										AppConstants.noTradeClient	= true;
									// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2 - END
								} 
							}else{
								/* Mary@20130220 - Fixes_Request-20130108, ReqNo.10
								List tradeAccounts = application.getTradeAccounts(selectedTradeExchange.getTrdExchgCode());
								/
								List<TradeAccount> tradeAccounts= application.getTradeAccounts();
								if(tradeAccounts.size() > 0)
									application.setSelectedAccount( tradeAccounts.get(0) );
								// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2 - START
								else
									AppConstants.noTradeClient	= true;
								// Added by Mary@20120815 - Fixes_Request-20120815, ReqNo.2 - END
							}
						}
						*/
						if(account == null){
							List<TradeAccount> tradeAccounts= application.getTradeAccounts();
							if(tradeAccounts.size() > 0)
								application.setSelectedAccount( tradeAccounts.get(0) );
							else
								AppConstants.noTradeClient	= true;
						}
						// Mary@20130220 - Fixes_Request-20130108, ReqNo.10 - END
						
						/* Mary@20120814 - Change_Request-20120719, ReqNo.4 - START
						SharedPreferences.Editor editor = sharedPreferences.edit();
						editor.putString(PreferenceConstants.EXCHANGE_CODE, selectedExchangeCode);
						if(application.getSelectedAccount() != null)
							editor.putString( PreferenceConstants.ACCOUNT_NO, application.getSelectedAccount().getAccountNo() );
						editor.commit();
						*/
						Map<String, String> mapBrokerUserPreference	= new HashMap<String, String>();
						mapBrokerUserPreference.put( PreferenceConstants.EXCHANGE_CODE,  selectedQCExchangeCode );
						/* Mary@20130225 - Change_Request-20130225, ReqNo.1
						if(application.getSelectedAccount() != null || !AppConstants.noTradeClient){
						*/
						if( application.getSelectedAccount() != null && !AppConstants.noTradeClient && AppConstants.brokerConfigBean.enableTrade() ){
							mapBrokerUserPreference.put( PreferenceConstants.ACCOUNT_NO, application.getSelectedAccount().getAccountNo() );
							mapBrokerUserPreference.put( PreferenceConstants.BRANCH_CODE, application.getSelectedAccount().getBranchCode() );
						}else{
							mapBrokerUserPreference.put(PreferenceConstants.ACCOUNT_NO, null);
							mapBrokerUserPreference.put(PreferenceConstants.BRANCH_CODE, null);
						}
						// Added by Mary@20121122 - Change_Request-20120719, ReqNo.12
						mapBrokerUserPreference.put(PreferenceConstants.TRADE_EXCHANGE_CODE, selectedTradeExchangeCode);
						AppConstants.cacheBrokerUserDetails(ExchangeActivity.this, sharedPreferences, mapBrokerUserPreference);
						mapBrokerUserPreference.clear();
						mapBrokerUserPreference						= null;
						// Mary@20120814 - Change_Request-20120719, ReqNo.4 - END
						
						/*commented, Change_Request-20160101, ReqNo.3
						rowLayout.setBackgroundDrawable(null);
						rowLayout.setBackgroundColor(Color.CYAN);
						*/
						AppConstants.setDefaultExchangeCode(selectedQCExchangeCode);
						ExchangeActivity.this.switchFeedServer(selectedQCExchangeCode);
						
						// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12
						AppConstants.updateTradePreferenceSharedPreference(ExchangeActivity.this, sharedPreferences, selectedTradeExchangeCode);
						
						//Added by Thinzar, Change_Request-20150901, ReqNo.5, reset selected board
						AppConstants.SELECTED_FEED_MARKET_CODE	= AppConstants.DEFAULT_MARKET_CODE;
					}
				});
	
				tableRow.setOnClickListener(new OnClickListener() {
					public void onClick(View view) {
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
						if(idleThread != null && ! isShowingIdleAlert)
							idleThread.resetExpiredTime();
						// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
						exchangeRadio.performClick();
					}
				});
	
				tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));  
				id++;
			}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	@Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
			
			if(refreshThread != null)
				refreshThread.pauseRequest();
			
			if(QCAliveThread != null)
				QCAliveThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END

	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
			if(this.mainMenu.isShowing()){
				this.mainMenu.hide();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			}else{
				finish();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			}
			// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}

	private void switchFeedServer(String exchangeCode) {
		new switchExchg().execute(exchangeCode);
	}
	
	private class switchExchg extends AsyncTask<String, Void, String>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		@Override
		protected void onPreExecute(){
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				ExchangeActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
				// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
				if( ! ExchangeActivity.this.isFinishing() )
					ExchangeActivity.this.dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}
		
		@Override
		protected String doInBackground(String... strArrExchgCode) {
			// TODO Auto-generated method stub
			String strExchangeCode 			= strArrExchgCode[0];
			// Added by Mary@20130401 - Fixes_Request-20130314, ReqNo.9
			String strPrevQCSvrIP			= AppConstants.QC_SERVER_IP;
			AppConstants.QC_SERVER_IP		= AppConstants.getExchangeServerMapping().get(strExchangeCode);
			// Added by Mary@20130801 - Change_Request-20130724, ReqNo.3
			AppConstants.QC_SERVER_PORT		= AppConstants.getExchangeServerPortMapping().get(strExchangeCode);
			
			// Added by Mary@20130401 - Fixes_Request-20130314, ReqNo.9
			if( strPrevQCSvrIP == null || ( strPrevQCSvrIP != null && ! strPrevQCSvrIP.equalsIgnoreCase(AppConstants.QC_SERVER_IP) ) ){
				try {
					/* Mary@20130517 - Fixes_Request-20130424, ReqNo.9
					String userParamKey = QcHttpConnectUtil.getKey();
					*/
					String userParamKey = AppConstants.brokerConfigBean.isGetNewKey() ? QcHttpConnectUtil.getNewKey() : QcHttpConnectUtil.getKey();
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					/* Mary@20130521 - Fixes_Request-20130424, ReqNo.10
					while(intRetryCtr < AppConstants.intMaxRetry && userParamKey == null){
						intRetryCtr++;
						userParamKey = QcHttpConnectUtil.getKey();
					}
					intRetryCtr	= 0;
					*/
					
					if(userParamKey == null)				
						throw new Exception(ErrorCodeConstants.FAIL_GET_QC_KEY);
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					Map parameters = new HashMap();
					parameters.put(ParamConstants.USERPARAM_KEY_TAG, userParamKey);
				
					StringBuffer response = new StringBuffer(25600);
	
					List<String>data =  QcHttpConnectUtil.login(parameters, response);
					*/
					
					StringBuffer response = new StringBuffer(25600);
					
					QCBean data	= QcHttpConnectUtil.login(userParamKey, response);
					/* Mary@20130521 - Fixes_Request-20130424, ReqNo.10
					while(intRetryCtr < AppConstants.intMaxRetry && data == null){
						intRetryCtr++;
						data	= QcHttpConnectUtil.login(userParamKey, response);
					}
					intRetryCtr	= 0;
					*/
					
					if(data == null)				
						throw new Exception(ErrorCodeConstants.FAIL_LOGIN_QC);
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
					/* Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					String userParam = data.get(1);
					String aliveTimeOut = data.get(2);
					double QCAliverate = Double.parseDouble(aliveTimeOut);
					*/
					AppConstants.QCdata	= data;
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					
					double QCAliverate = AppConstants.QCdata.getDblAliveTimeOut();
					QCAliverate = Math.floor(QCAliverate*0.75);
					
					AppConstants.QC_KEEP_ALIVE_RATE = (int)QCAliverate;
					
					/* Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					QCAliveThread.setUserParam(userParam);
					refreshThread.setUserParam(userParam);
	
					SharedPreferences.Editor editor= sharedPreferences.edit();
					editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
					editor.commit();
					*/
					QCAliveThread.setUserParam( AppConstants.QCdata.getStrUserParam() );
					refreshThread.setUserParam( AppConstants.QCdata.getStrUserParam() );
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END 
					
	
					/* Mary@20130605 - comment unused variable
					String result = response.toString();
					*/
	
					exchangeSectors = QcMessageParser.parseExchangeSector( response.toString() );
	
					ExchangeActivity.this.populateExchangeSector();
	
					StockApplication application = (StockApplication) ExchangeActivity.this.getApplication();
					application.setExchangeInfos(exchangeInfos);
				
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				} catch (FailedAuthenicationException e) {
					// TODO Auto-generated catch block
					ExchangeActivity.this.switchFeedServer(exchangeCode);
					e.printStackTrace();
				}catch (Exception ex) {
					ex.printStackTrace();
				}
				*/
				}catch(Exception e){
					exception = e;
				}
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(String response){
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			try{
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
				if(ExchangeActivity.this.dialog != null && ExchangeActivity.this.dialog.isShowing() )
					ExchangeActivity.this.dialog.dismiss();
				
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
						}
					});
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! ExchangeActivity.this.isFinishing() )
						alertException.show();	
					exception = null;
					
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				Intent intent = new Intent();
				intent.putExtra(ParamConstants.FROM_EXCHANGE_TAG, true);		//Added by Thinzar, Fixes_Request-20161101, ReqNo.1
				intent.setClass(ExchangeActivity.this, QuoteActivity.class);
				startActivity(intent);
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
			super.onPostExecute(response);
		}
		
	}

	private void populateExchangeSector() {
		
		for (Iterator<ExchangeInfo> itr=exchangeInfos.iterator(); itr.hasNext(); ) {
			ExchangeInfo exchange		= itr.next();
			/* Mary@20130220 - Fixes_Request-20130108, ReqNo.13
			String exchangeCode			= selectedQCExchangeCode;
			*/
			String exchangeCode			= exchange.getQCExchangeCode();
			ExchangeInfo sectorExchange = (ExchangeInfo) exchangeSectors.get(exchangeCode);
			if (sectorExchange != null) {
				exchange.setSectors(sectorExchange.getSectors());			
				exchange.setIndiceSymbol(sectorExchange.getIndiceSymbol());
			}
		}
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(ExchangeActivity.this.mainMenu.isShowing()){
					ExchangeActivity.this.mainMenu.hide();
				}
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
				if(refreshThread != null)
					refreshThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
				if(loginThread != null)
					loginThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				if(!isFinishing()){
					ExchangeActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};

	@Override
	public void QCAliveEvent(boolean isResume) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		// TODO Auto-generated method stub
		
	}

}
