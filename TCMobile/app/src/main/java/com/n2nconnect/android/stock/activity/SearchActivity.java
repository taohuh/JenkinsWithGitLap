package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.BadTokenException;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.custom.ExchangeListView;
import com.n2nconnect.android.stock.custom.ExchangeListView.OnExchangListEventListener;
import com.n2nconnect.android.stock.model.Sector;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class SearchActivity extends CustomWindow implements OnDoubleLoginListener, OnExchangListEventListener{

	private List<StockSymbol> stockSymbols;
	/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private String userParam;
	*/
	private EditText searchField;
	private ImageButton searchButton;
	private ImageButton clearButton;
	private ProgressDialog dialog;
	private DoubleLoginThread loginThread;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr	= 0;

	// Added by Mary@20130110 - avoid unnecessary reinitialization
	private TableLayout tableLayout;
	
	// Added by Mary@20130122 - Change_Request-20130104, ReqNo.5 - START
	private String strSelectedFilterExchangeType;
	private Button btnExchangeType;
	private ExchangeListView exchgListView;
	private Map<String, Object> selectedExchangeMap;
	private Sector selectedSector;
	private String ALL_TYPE_KEY	= null;
	// Added by Mary@20130122 - Change_Request-20130104, ReqNo.5 - END

	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState); 
			setContentView(R.layout.search_symbol);
			
			this.title.setText(getResources().getString(R.string.search_module_title));
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(LayoutSettings.menu_search_icn);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntSearchMenuIconBgImg() );
	
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			userParam = sharedPreferences.getString(PreferenceConstants.QC_USER_PARAM, null);
			try {
				if (userParam == null) {
					List<String>data = QcHttpConnectUtil.relogin();
					userParam = data.get(0);
					SharedPreferences.Editor editor = sharedPreferences.edit();
					editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
					editor.commit();
				}
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
	
			searchField		= (EditText) SearchActivity.this.findViewById(R.id.searchField);
			searchField.setSingleLine();
	
			searchButton	= (ImageButton) this.findViewById(R.id.searchButton);
	
			clearButton 	= (ImageButton) this.findViewById(R.id.clearButton);
			clearButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					EditText searchField = (EditText) SearchActivity.this.findViewById(R.id.searchField);
					searchField.setText("");
					
					//for debugging, to simulate ATP down, delete this code block later
					//AppConstants.brokerConfigBean.setStrAtpIP("218.100.22.216");
					//end for debugging
				}
			});
	
			dialog 			= new ProgressDialog(this);
			
			// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - START
			ALL_TYPE_KEY	= getResources().getString(R.string.all_type_key);
			btnExchangeType = (Button)findViewById(R.id.btnExchangeType);
			
			if(! AppConstants.hasMultiLogin ){
				//( (RelativeLayout)findViewById(R.id.rl_ss_TopNavBar) ).setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBgImg() );	////Change_Request-20160101, ReqNo.3
				//btnExchangeType.setBackgroundResource( AppConstants.brokerConfigBean.getIntTopNavBarBtnBgImg() );	////Change_Request-20160101, ReqNo.3
			}
			
			List<String> lstExchgHaveSector	= new ArrayList<String>();
			/* Mary@20130312 - Fixes_Request-20130108, ReqNo.19
			lstExchgHaveSector.add("MY");
			*/
			lstExchgHaveSector.add(AppConstants.MY_EXCHANGE_CODE);
			
			exchgListView 					= new ExchangeListView(SearchActivity.this, SearchActivity.this, getLayoutInflater(), lstExchgHaveSector);
			exchgListView.setHideOnSelect(true);
			
			btnExchangeType.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					doExchangeList();
				}        	
			});
			// Added by Mary@20130118 - Change_Request-20130104, ReqNo.5 - END
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void onResume() {
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.17
		try{
			super.onResume();
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= SearchActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
						
			// Added by Mary@20121116 - Change_Request-20120719, ReqNo.12
			AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, null);
			
			AppConstants.showMenu = true;
	
			/* Mary@20130222 - Change_Request-20130104, ReqNo.5
			loginThread						= ( (StockApplication) SearchActivity.this.getApplication() ).getDoubleLoginThread();
			*/
			StockApplication application 	= (StockApplication) SearchActivity.this.getApplication();
			loginThread						= application.getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) SearchActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(SearchActivity.this);
				loginThread.resumeRequest();
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(this);
			
			searchField.requestFocus();
	
			searchField.postDelayed(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					InputMethodManager keyboard = (InputMethodManager)
					getSystemService(Context.INPUT_METHOD_SERVICE);
					keyboard.showSoftInput(searchField, 0);
				}
			},200);
	
			searchButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					SearchActivity.this.handleSearchEvent();
	
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(searchField.getWindowToken(), 0);
				}
			});
	
			tableLayout = (TableLayout) findViewById(R.id.searchTable);
	
			for (int i=0; i<tableLayout.getChildCount(); i++) {
				TableRow tableRow = (TableRow) tableLayout.getChildAt(i);
				LinearLayout rowLayout = (LinearLayout) tableRow.findViewById(R.id.rowLayout);
	
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				if ((i+1)%2 != 0) {
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
				} else {
					rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
				}
				*/
				rowLayout.setBackgroundResource( ( (i+1)%2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light );	//Change_Request-20160101, ReqNo.3
			}
			
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
			List<Map<String, Object>> listExchangeMap		= new ArrayList<Map<String, Object>>();
			try{
				// All Exchange
				Map<String, Object> exchgMap		= new HashMap<String, Object>();
				exchgMap.put(ParamConstants.PARAM_STR_TRD_EXCHG_CODE, ALL_TYPE_KEY);
				exchgMap.put(ParamConstants.PARAM_STR_EXCHG_NAME, getResources().getString(R.string.all_exchange_type_label));
				exchgMap.put(ParamConstants.PARAM_INT_EXCHG_IMAGE_ID, null);
				
				listExchangeMap.add(exchgMap);
				
				Map<String, Integer> mapExchgImages = DefinitionConstants.getExchangeImageMapping();
				List<String> listQCExchgCode		= DefinitionConstants.getValidQCExchangeCodes();
		
				for(int i=0; i<listQCExchgCode.size(); i++){
					String strExchgCode		= DefinitionConstants.getTrdExchangeCode().get(i);
					String strQCExchgCode	= listQCExchgCode.get(i);
					String strExchgName		= DefinitionConstants.getExchangeNameMapping().get(strQCExchgCode);
					
					exchgMap				= new HashMap<String, Object>();
					exchgMap.put(ParamConstants.PARAM_STR_TRD_EXCHG_CODE, strExchgCode);
					exchgMap.put(ParamConstants.PARAM_STR_EXCHG_NAME, strExchgName);
					exchgMap.put(ParamConstants.PARAM_INT_EXCHG_IMAGE_ID, mapExchgImages.get(strQCExchgCode) == null ? null : ( mapExchgImages.get(strQCExchgCode) ).intValue() );
					
					listExchangeMap.add(exchgMap);
				}
				
				exchgListView.setListExchangeMap(listExchangeMap);
				exchgListView.setListExchangeInfo( application.getExchangeInfos() );
			} catch (Exception e) {			
				e.printStackTrace();
			}
			
			if (selectedExchangeMap == null) {
				for( Iterator<Map<String, Object>> itr=exchgListView.getListExchangeMap().iterator(); itr.hasNext(); ){
					Map<String, Object> mapExchange		= itr.next();
					String strExchgCode = String.valueOf( mapExchange.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
					if( strExchgCode.equalsIgnoreCase(ALL_TYPE_KEY) ){
						selectedExchangeMap = mapExchange;
						break;
					}
				}
			}
			btnExchangeType.setText( String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) ) );
			strSelectedFilterExchangeType	= String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.17 - START
		}catch(Exception e){
			e.printStackTrace();
			finish();
		}
		// Added by Mary@20130905 - GoogleCrashReport-20130822, ReqNo.17 - END
	}
	
	// Added by Mary@20121105 - Fixes_Request-20121023, ReqNo.4 - START
	@Override
	protected void onPause() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onPause();
			
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
			
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END

	}
	// Added by Mary@20121105 - Fixes_Request-20121023, ReqNo.4 - END
	
	/* Mary@20130604 - comment unneccessary process
	private class IMMResult extends ResultReceiver {
		public int result = -1;
		public IMMResult() {
			super(null);
		}

		public void onReceiveResult(int r, Bundle data) {
			result = r;
		}

		// poll result value for up to 500 milliseconds
		public int getResult() {
			try {
				int sleep = 0;
				while (result == -1 && sleep < 500) {
					Thread.sleep(100);
					sleep += 100;
				}
			} catch (InterruptedException e) {
				Log.e("IMMResult", e.getMessage());
			}
			return result;
		}
	}
	*/

	private void handleSearchEvent() {
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
		if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) ){
			if(searchField.getText().toString().trim().length() > 0)						
				new stockSearch().execute();
		}
	}

	private class stockSearch extends AsyncTask <View, Void, Boolean>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    		try{
				// Added by Mary@20130110 - avoid unnecessary reinitialization
				// Added by Mary@20121224-Fixes_request-20121102, ReqNo.7
				if(SearchActivity.this.tableLayout == null)
					SearchActivity.this.tableLayout = (TableLayout) findViewById(R.id.searchTable);
				SearchActivity.this.tableLayout.removeAllViews();
	
				// Added by Mary@20130110 - avoid unnecessary reinitialization - START
				if(SearchActivity.this.dialog != null){
					if( SearchActivity.this.dialog.isShowing() )
						SearchActivity.this.dialog.dismiss();
				// Added by Mary@20130110 - avoid unnecessary reinitialization - END
					
					SearchActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! SearchActivity.this.isFinishing() )
						SearchActivity.this.dialog.show();
				}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

		@Override
		protected Boolean doInBackground(View... arg0) {
			// TODO Auto-generated method stub

			String searchText = searchField.getText().toString();
			Map<String, String> parameters = new HashMap<String, String>();
			
			/* Mary@20130108 - Fixes_Request-20121102, ReqNo.18
			parameters.put(ParamConstants.SEARCH_TEXT_TAG, searchText);
			*/
			parameters.put(ParamConstants.SEARCH_TEXT_TAG, searchText.trim() );
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			parameters.put(ParamConstants.USERPARAM_TAG, userParam);
			 */
			
			/* Mary@20130604 - comment unused variable
			StockApplication application = (StockApplication) SearchActivity.this.getApplication();
			*/
			/* Mary@20130123 - Change_Request-20130104, ReqNo.5
			parameters.put(ParamConstants.EXCHANGECODE_TAG, AppConstants.DEFAULT_EXCHANGE_CODE);
			*/
			/* Mary@20130607 - Change_Request-20130424, ReqNo.7 - START
			parameters.put( ParamConstants.EXCHANGECODE_TAG, strSelectedFilterExchangeType.equalsIgnoreCase(ALL_TYPE_KEY) ? "" : strSelectedFilterExchangeType );
			*/
			String strFilterExchangeType	= "";
			if( strSelectedFilterExchangeType.equalsIgnoreCase(ALL_TYPE_KEY) ){
			/* Mary@20130722 - Change_Request-20130424, ReqNo.7 - START
				for( Iterator<Map<String, Object>> itr=exchgListView.getListExchangeMap().iterator(); itr.hasNext(); ){
					Map<String, Object> mapExchange		= itr.next();
					String strExchgCode 				= String.valueOf( mapExchange.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
					if( ! strExchgCode.equalsIgnoreCase(ALL_TYPE_KEY) )
						strFilterExchangeType	+= (strFilterExchangeType.length() > 0 ? "," : "") + strExchgCode;
				}
			}else{
				strFilterExchangeType	= strSelectedFilterExchangeType;
				*/
				for( Iterator<String> itr=DefinitionConstants.getValidQCExchangeCodes().iterator(); itr.hasNext(); ){
					strFilterExchangeType	+= (strFilterExchangeType.length() > 0 ? "," : "") + itr.next();
				}
			}else{
				for(int i=0; i < DefinitionConstants.getTrdExchangeCode().size(); i++ ){
					if( strSelectedFilterExchangeType.equalsIgnoreCase( DefinitionConstants.getTrdExchangeCode().get(i) ) ){
						strFilterExchangeType	= DefinitionConstants.getValidQCExchangeCodes().get(i);
					}
				}
			// Mary@20130722 - Change_Request-20130424, ReqNo.7 - END
			}
			parameters.put( ParamConstants.EXCHANGECODE_TAG, strFilterExchangeType);
			// Mary@20130607 - Change_Request-20130424, ReqNo.7 - END
			
			/* Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
			List sectors = application.getSelectedExchange().getSectors();
			if (sectors.size()>0) {
				parameters.put(ParamConstants.SECTORCODE_TAG, ((Sector) sectors.get(0)).getSectorCode()); 
			}
			*/
			if(selectedSector != null)
				parameters.put(ParamConstants.SECTORCODE_TAG, selectedSector.getSectorCode());
						
			//  Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
			
			try {
				/* Mary@20130123 - Change_Request-20130104, ReqNo.5
				String response = QcHttpConnectUtil.imageSearch(parameters);
				*/
				/* Mary@20130620 - Fixes_Request-20130523, ReqNo.13
				String response = selectedSector != null ? QcHttpConnectUtil.imageSearch(parameters) : QcHttpConnectUtil.imageMSearch(parameters);
				*/
				String response = ( selectedSector != null || !AppConstants.brokerConfigBean.isConnectJavaQC() ) ? QcHttpConnectUtil.imageSearch(parameters) : QcHttpConnectUtil.imageMSearch(parameters);
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					/* Mary@20130123 - Change_Request-20130104, ReqNo.5
					response = QcHttpConnectUtil.imageSearch(parameters);
					*/
					/* Mary@20130620 - Fixes_Request-20130523, ReqNo.13
					response = selectedSector != null ? QcHttpConnectUtil.imageSearch(parameters) : QcHttpConnectUtil.imageMSearch(parameters);
					*/
					response = ( selectedSector != null || !AppConstants.brokerConfigBean.isConnectJavaQC() ) ? QcHttpConnectUtil.imageSearch(parameters) : QcHttpConnectUtil.imageMSearch(parameters);
				}
				intRetryCtr	= 0;
				// NOTE : after retry still null then ignore as existing code already have code that handle null value
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				if (response == null) {
					if(stockSymbols!=null){
						stockSymbols.clear();
					}
				} else {
					stockSymbols = QcMessageParser.parseSymbolSearch(response);
				}
				return true;
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			} catch (FailedAuthenicationException e) {
				e.printStackTrace();
				SearchActivity.this.handleSearchEvent(); // might caused infinite loop if FailedAuthenicationException continuously thrown
			}
			*/
			} catch (Exception e) {
				e.printStackTrace();
				exception	= e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			return false;
		}

		@Override
		protected void onPostExecute(Boolean success){
			super.onPostExecute(success);
			
			// Added by Mary@20130109 - Fixes_Request-20130108, ReqNo.2
			try{
				if(SearchActivity.this.dialog != null && SearchActivity.this.dialog.isShowing())
					SearchActivity.this.dialog.dismiss();
	
				 if( stockSymbols == null || ( stockSymbols != null &&  stockSymbols.isEmpty() )	){
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					String strMsg	= exception != null ? exception.getMessage() : getResources().getString(R.string.search_no_result);
					exception		= null;
					// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					
					AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
					builder.setMessage(strMsg)
					.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
					
					AlertDialog alert = builder.create();
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! SearchActivity.this.isFinishing() )
						alert.show();
					
					return;
				}else if(stockSymbols!=null){
					/* Mary@20130123 - Change_Request-20130104, ReqNo.5
					SearchActivity.this.constructTableView();
					*/
					SearchActivity.this.constructTableView(strSelectedFilterExchangeType);
				}
			// Added by Mary@20130109 - Fixes_Request-20130108, ReqNo.2 - START
			}catch(BadTokenException bte){
				bte.printStackTrace();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
			}
			// Added by Mary@20130109 - Fixes_Request-20130108, ReqNo.2 - END
		}

	}

	/* Mary@20130123 - Change_Request-20130104, ReqNo.5
	private void constructTableView() {
	*/
	private void constructTableView(String strSelectedFilterExchangeType){
		try {
			/* Mary@20130110 - avoid unnecessary reinitialization
			TableLayout tableLayout = (TableLayout) findViewById(R.id.searchTable);
			*/
			this.tableLayout.removeAllViews();

			ScrollView scroll = (ScrollView)findViewById(R.id.scrollSearch);

			scroll.fullScroll(ScrollView.FOCUS_UP);

			int id = 0;

			if(stockSymbols!=null){
				for (Iterator<StockSymbol> itr=stockSymbols.iterator(); itr.hasNext(); ) {
					StockSymbol symbol = itr.next();
					/* Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
					tableView(symbol, id);
					id++;
					*/
					StockSymbol tempSS			= null;
					if(strSelectedFilterExchangeType == null){
						tempSS	= symbol;
					}else{
						if( strSelectedFilterExchangeType.equalsIgnoreCase(ALL_TYPE_KEY) ){
							tempSS	= symbol;
						}else{
							/* Mary@20130626 - Fixes_Request-20130523, ReqNo.20
							String strCurrSymbolCode		= symbol.getSymbolCode();
							String strCurrTradeExchgCode	= strCurrSymbolCode.split("\\.")[1];
							*/
							String strCurrTradeExchgCode	= symbol.getSymbolCode().substring( (symbol.getSymbolCode().lastIndexOf(".") + 1), symbol.getSymbolCode().length() );
							strCurrTradeExchgCode			= ( strCurrTradeExchgCode.length() > 1 && strCurrTradeExchgCode.toUpperCase().endsWith("D") ) ? strCurrTradeExchgCode.substring(0, strCurrTradeExchgCode.length() - 1) : strCurrTradeExchgCode;
							if( strSelectedFilterExchangeType.equalsIgnoreCase(strCurrTradeExchgCode) )
								tempSS = symbol;
						}
					}
					
					if(tempSS != null){
						tableView(symbol, id);
						id++;
					}
					
					// Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void tableView(StockSymbol stockSymbol, int intID){
		final TableRow tableRow			= new TableRow(this);   
		tableRow.setId(intID);
		tableRow.setLayoutParams(new TableLayout.LayoutParams( TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
		tableRow.setWeightSum(1);
		
		View rowView 					= this.getLayoutInflater().inflate(R.layout.search_row, null);  
		final LinearLayout rowLayout 	= (LinearLayout) rowView.findViewById(R.id.rowLayout);
		
		TextView nameLabel 				= (TextView) rowView.findViewById(R.id.nameLabel);
		TextView codeLabel 				= (TextView) rowView.findViewById(R.id.codeLabel);
		nameLabel.setText(stockSymbol.getStockCode());
		codeLabel.setText(stockSymbol.getSymbolCode());

		tableRow.addView(rowView);
		tableRow.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
				if(idleThread != null && ! isShowingIdleAlert)
					idleThread.resetExpiredTime();
				// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				
				//rowLayout.setBackgroundDrawable(null);		//Change_Request-20160101, ReqNo.3
				//rowLayout.setBackgroundColor(Color.CYAN);		//Change_Request-20160101, ReqNo.3

				Intent intent 		= new Intent();
				StockSymbol symbol 	= stockSymbols.get(tableRow.getId());
				intent.putExtra(ParamConstants.SYMBOLCODE_TAG, symbol.getSymbolCode());
				// Added by Mary - Fixes_Request-20121023, ReqNo.4
				intent.putExtra(ParamConstants.PARAM_STR_RESTART_ACTIVITY, true);
				intent.setClass(SearchActivity.this, StockActivity.class);
				startActivity(intent);
			}
		});

		/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
		if ((intID+1)%2 != 0) {
			rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
		} else {
			rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
		}
		*/
		//rowLayout.setBackgroundResource( ( (intID+1)%2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);	//Change_Request-20160101, ReqNo.3

		tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));  
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) { 

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
			if(this.mainMenu.isShowing()){
				this.mainMenu.hide();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
			}else if( this.exchgListView.isShowing() ){
				this.exchgListView.hide();
			// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
			}else{
				finish();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			}
			// Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
		}

		return super.onKeyDown(keyCode, event); 
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		// TODO Auto-generated method stub
		Message message = handler3.obtainMessage();
		message.obj = response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if(response.get(2).equals("1102")){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if(SearchActivity.this.mainMenu.isShowing()){
					SearchActivity.this.mainMenu.hide();
				}
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				if(!isFinishing()){
					SearchActivity.this.doubleLoginMessage((String)response.get(1));
				}
			}
		}
	};
	
	// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - START
	private void doExchangeList() {
		if( exchgListView.isShowing() ){
			exchgListView.hide();
		}else{
			if( mainMenu.isShowing() )
				mainMenu.hide();
			
			exchgListView.show(findViewById(R.id.rl_ss_TopNavBar), selectedExchangeMap);
		}
	}
		
	public void exchangeListSelectedEvent(Map<String, Object> exchangeMap, Sector sector) {
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		if(idleThread != null && ! isShowingIdleAlert)
			idleThread.resetExpiredTime();
		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
		selectedExchangeMap				= exchangeMap;
		selectedSector					= sector;
		strSelectedFilterExchangeType	= String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_TRD_EXCHG_CODE) );
		String strBtnName				= String.valueOf( selectedExchangeMap.get(ParamConstants.PARAM_STR_EXCHG_NAME) );
		strBtnName						= sector == null ? strBtnName : strBtnName.concat("-").concat( sector.getSectorName() );
		btnExchangeType.setText(strBtnName);
		
		// constructTableView(strSelectedFilterExchangeType);
		SearchActivity.this.handleSearchEvent();
	}
	// Added by Mary@20130123 - Change_Request-20130104, ReqNo.5 - END
}