package com.n2nconnect.android.stock;

import java.util.HashMap;
import java.util.Map;

import com.n2nconnect.android.stock.util.AtpConnectUtil;

public class TradeAliveThread extends Thread {
	
	private OnTradeAliveListener registeredListener;
	private int refreshRate;
	private String userParam;
	private volatile boolean stopRequested; 
	private Thread runThread;
	
	public OnTradeAliveListener getRegisteredListener() {
		return registeredListener;
	}
	
	public void setRegisteredListener(OnTradeAliveListener registeredListener) {
		this.registeredListener = registeredListener;
	}

	public String getUserParam() {
		return userParam;
	}

	public void setUserParam(String userParam) {
		this.userParam = userParam;
	}
	
	public interface OnTradeAliveListener {
		public void TradeAliveEvent(boolean isResume);
	}
	
	public TradeAliveThread(String userParam, int refreshRate) {
		this.userParam = userParam;
		this.refreshRate = refreshRate;
		this.stopRequested = false;
	}

	public void run(){
		
		while(!stopRequested){
			try{
				
				runThread = Thread.currentThread();
				
				if (refreshRate > 0) {
					long waitTime = refreshRate * 1000;
					Thread.sleep(waitTime);
				} else if (refreshRate == 0) {
					Thread.sleep(AppConstants.TRADE_KEEP_ALIVE_RATE * 1000);
					continue;
				} else {				
					Thread.sleep(AppConstants.TRADE_KEEP_ALIVE_RATE * 1000);
				}
				
				Map<String, String> parameters	= new HashMap<String, String>();
				parameters.put(ParamConstants.USERPARAM_TAG, userParam);

				/* Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START  - NOTE : not perform retry for thread
				AtpConnectUtil.tradeAlive(parameters);
				*/
				AtpConnectUtil.tradeAlive(parameters);
				
				if (registeredListener != null) {
					registeredListener.TradeAliveEvent(true);
				}

			}catch (InterruptedException e) {
				e.printStackTrace();
			} catch (FailedAuthenicationException e) {				
				e.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public void stopRequest() {
        stopRequested = true;

        if (runThread != null ) {
            runThread.interrupt();
        }
    }
	
}
