package com.n2nconnect.android.stock.activity;

import java.io.IOException;
import java.util.List;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.CommandConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.StockInfo;
import com.n2nconnect.android.stock.util.HttpHelper;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class DtiDetailsActivity extends CustomWindow implements OnDoubleLoginListener {
	private final String TAG	= "DtiDetailsActivity";
	private WebView webViewDtiDetails;
	private Button btnTradeNow;
	private AlertDialog alertException;
	private ProgressDialog dialog;
	private DoubleLoginThread loginThread;
	
	private String url;
	
	private String eid;
	private String symbol;
	private String dtiExchgCode;
	private String isin;
	private char tradeType;
	private int intRetryCtr	= 0;
	private StockInfo stockInfo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.title.setText(getResources().getString(R.string.dti_module_title));
		setContentView(R.layout.activity_dti_details);
		
		Bundle extras	= this.getIntent().getExtras();
		if(extras != null){
			eid			= extras.getString(ParamConstants.DTI_ID_TAG);
			symbol		= extras.getString(ParamConstants.DTI_INSTRUMENT_SYM_SOMMON_TAG);
			dtiExchgCode	= extras.getString(ParamConstants.DTI_INSTRUMENT_EXCHG_TAG);
			isin		= extras.getString(ParamConstants.DTI_INSTRUMENT_ISIN_TAG);
			tradeType	= extras.getChar(ParamConstants.DTI_TRADE_TYPE_TAG);
		}
		
		//Change_Request-20170601, ReqNo.7
		this.icon.setImageResource(R.drawable.selector_back_button);
		this.icon.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				DtiDetailsActivity.this.finish();
			}
		});
	}
	
	private String getDtiDetailsUrl(){
		String mUrl;
		
		mUrl	= AppConstants.brokerConfigBean.getDtiUrl() + "page=event&name=stocks"
				+ "&eid=" + eid
				+ "&symbol=" + symbol
				+ "&exchange=" + dtiExchgCode
				+ "&isin=" + isin
				+ "&trade_type=" + tradeType
				+ "&lang=en"
				+ "&delivery=mobile";
		
		return mUrl;
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
		if (idleThread != null && idleThread.isPaused())
			idleThread.resumeRequest(false);
		// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END

		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		loginThread = ((StockApplication) DtiDetailsActivity.this.getApplication()).getDoubleLoginThread();
		if (loginThread != null && loginThread.isPaused()) {
			loginThread.setRegisteredListener(DtiDetailsActivity.this);
			loginThread.resumeRequest();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
				
		if(!AppConstants.hasDtiTermsShown){
			Intent dtiTermsIntent	= new Intent();
			dtiTermsIntent.putExtra(ParamConstants.IS_FROM_DTI_TAG, true);
			dtiTermsIntent.setClass(DtiDetailsActivity.this, DtiTermsActivity.class);
			
			startActivity(dtiTermsIntent);
		}else{
		
			url = getDtiDetailsUrl();
			DefinitionConstants.Debug(TAG + " Url: " + url);
			
			dialog	= new ProgressDialog(DtiDetailsActivity.this);
			webViewDtiDetails	= (WebView)findViewById(R.id.web_view_dti_details);
			btnTradeNow			= (Button)findViewById(R.id.btn_trade_now);
			webViewDtiDetails.getSettings().setJavaScriptEnabled(true);
			
			//to enhance the performance with hardware acceleration, android 19 has Chromium engine for WebView
			if (Build.VERSION.SDK_INT >= 19) {
				webViewDtiDetails.setLayerType(View.LAYER_TYPE_HARDWARE, null);
			}       
			else {
				webViewDtiDetails.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}
			
			webViewDtiDetails.loadUrl(url);
			
			webViewDtiDetails.setWebChromeClient(new WebChromeClient(){
	
				@Override
				public void onProgressChanged(WebView view, int newProgress) {
					if( ! DtiDetailsActivity.this.isFinishing() ){	
	    				dialog.setMessage("Loading");
	    				dialog.show();
					}
					
					if(newProgress == 100 && dialog.isShowing())
						dialog.dismiss();
				}
	
			});
			webViewDtiDetails.setWebViewClient(new WebViewController());
			
			//Change_Request-20170601, ReqNo.7 - START
			this.menuButton.setVisibility(View.GONE);
			this.Register.setVisibility(View.VISIBLE);
			this.Register.setText(getResources().getString(R.string.dti_trade_button));
			
			this.Register.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					tradeNowButtonClickedEvent();
				}
			});
			
			this.btnTradeNow.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					tradeNowButtonClickedEvent();
				}
			});
			//Change_Request-20170601, ReqNo.7 - END
			
			new LogDtiTask().execute();
		}
	}
	
	public void tradeNowButtonClickedEvent(){
		if(AppConstants.noTradeClient){
			showAlertDialog("You do not have a trading account", false);
		}else{
			String iTradeExchangeCode	= SystemUtil.getItradeExchgCode(dtiExchgCode);
			
			if(iTradeExchangeCode == null){
				showAlertDialog(getResources().getString(R.string.dti_activate_cross_border_account), false);
			}else{
				String fullSymbolCode	= symbol + "." + iTradeExchangeCode;
				new FetchStockInfo().execute(fullSymbolCode);
			}
		}
	}
	
	public class WebViewController extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			System.out.println("[DtiDetailsActivity] ShouldOverrideUrlLoading:" + url);
			
			//itradecimbdti://stockcode:LPTX/exchange:NASDAQ
        	if(url.startsWith("itradecimbdti:")){

        		if(AppConstants.noTradeClient){
        			showAlertDialog("You do not have a trading account", false);
        		}else{
        			String iTradeExchangeCode	= SystemUtil.getItradeExchgCode(dtiExchgCode);
        			
        			if(iTradeExchangeCode == null){
        				showAlertDialog(getResources().getString(R.string.dti_activate_cross_border_account), false);
        			}else{
        				String fullSymbolCode	= symbol + "." + iTradeExchangeCode;
        				new FetchStockInfo().execute(fullSymbolCode);
        			}
        		}
        		
        		return true;
        	} 
        	else if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
                view.reload();
                
                return true;
            }
        	else {
        		view.loadUrl(url);
        		
        		return false;
        	}
        }
		
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			showAlertDialog(getResources().getString(R.string.dti_not_available), true);
	    }
	}
	
	private void showAlertDialog(String alertMsg, final boolean isExit){
		alertException 	= new AlertDialog.Builder(DtiDetailsActivity.this)
		//.setIcon(android.R.attr.alertDialogStyle)
		.setMessage(alertMsg)
		.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				if(isExit){
					DtiDetailsActivity.this.finish();
				}
				return;
			}
		}).create();
		
		alertException.show();
	}
	
	private void showStockCodeNotFoundMessage(){
		alertException 	= new AlertDialog.Builder(DtiDetailsActivity.this)
				//.setIcon(android.R.attr.alertDialogStyle)
				.setTitle(getResources().getString(R.string.stock_not_found_dti_title))
				.setMessage(getResources().getString(R.string.stock_not_found_dti))
				.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
						return;
					}
				}).create();

		alertException.show();
	}

	private void startTradeActivity(String mSymbolCode, String mLotSize, String mInstrument, boolean mIsPayableWithCPF) {

		Intent intent = new Intent();
		intent.putExtra(ParamConstants.SYMBOLCODE_TAG, mSymbolCode);
		intent.putExtra(ParamConstants.LOT_SIZE_TAG, mLotSize);
		intent.putExtra(ParamConstants.INSTRUMENT_TAG, mInstrument);
		intent.putExtra(ParamConstants.IS_PAYABLE_WITH_CPF, mIsPayableWithCPF);
		intent.putExtra(ParamConstants.ORDERPAD_TYPE_TAG, AppConstants.ORDER_PAD_TRADE);
		intent.putExtra(ParamConstants.ORDER_DETAILTYPE_TAG, AppConstants.ORDER_DETAIL_SUBMIT);
		intent.putExtra(ParamConstants.EXTERNAL_ORDER_SOURCE_TAG, ParamConstants.PARAM_ORDER_SOURCE_DTI);	
		
		intent.setClass(DtiDetailsActivity.this, TradeActivity.class);

		startActivity(intent);
	}
	
	private class LogDtiTask extends AsyncTask<Void, Void, Void>{
		private String dtiLogUrl;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			dtiLogUrl	= "https://api.recognia.com/api/logusage?cid=207&uid=admin&pwd=tradeCONTROL&tuid=" 
					+ AppConstants.userSenderName + "&page=dti_detail_mobile";
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			//DTI logging purpose
			HttpHelper httpHelper	= new HttpHelper();
			String logDetails;
			try {
				logDetails = httpHelper.doGet(dtiLogUrl);
				//System.out.println("dtiLogUrl=" + dtiLogUrl);
				//System.out.println("dtiLog=" + logDetails);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
	}
	
	private class FetchStockInfo extends AsyncTask<String, Void, String[]> {
		private Exception exception 		= null;
		private int nextTaskId;
		
		@Override
		protected void onPreExecute() {
			Log.i(TAG, "FetchStockInfo Task");
			super.onPreExecute();
			
			if(!DtiDetailsActivity.this.isFinishing()){
				dialog.setMessage("fetching StockInfo");
				dialog.show();
			}
		}

		@Override
		protected String[] doInBackground(String... params) {
			
			String[] symbolCodes 	= { params[0] };
			
			String response = null;
			try {
				intRetryCtr = 0;
				
				response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
				while (intRetryCtr < AppConstants.intMaxRetry && response == null) {
					intRetryCtr++;
					response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_DETAIL);
				}
				intRetryCtr = 0;

				if (response == null)
					throw new Exception(ErrorCodeConstants.FAIL_GET_QUOTE_DETAIL);

			} catch (Exception e) {
				exception = e;
			}

			if(response != null)
				stockInfo = QcMessageParser.parseStockDetail(response);

			return symbolCodes;
		}

		@Override
		protected void onPostExecute(String[] result) {
			super.onPostExecute(result);

			if (dialog != null && dialog.isShowing())
				dialog.dismiss();

			if (exception != null) {
				alertException.setMessage(exception.getMessage());

				if (!DtiDetailsActivity.this.isFinishing())
					alertException.show();
				return;
			} else{
				String strSymbolCode	= stockInfo.getSymbolCode();
				
				if(strSymbolCode == null || strSymbolCode.isEmpty()){
					showStockCodeNotFoundMessage();
				}
				else
					startTradeActivity(stockInfo.getSymbolCode(), Integer.toString(stockInfo.getSharePerLot()), stockInfo.getInstrument(), stockInfo.isPayableWithCPF());
			}
		}
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message	= handler3.obtainMessage();
		message.obj 	= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				
				if(loginThread != null)
					loginThread.stopRequest();
				
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				
				if( DtiDetailsActivity.this.mainMenu.isShowing() )
					DtiDetailsActivity.this.mainMenu.hide();
					
				if( !isFinishing() )
					DtiDetailsActivity.this.doubleLoginMessage((String)response.get(1));
			}
		}
	};
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (webViewDtiDetails.canGoBack() && (keyCode == KeyEvent.KEYCODE_BACK)) {
	    	// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if(this.mainMenu.isShowing())
	    		this.mainMenu.hide();
			
			webViewDtiDetails.goBack();
	    	finish();
	    	
	        return true;
	    }else if(keyCode==KeyEvent.KEYCODE_BACK){
	    	// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
    @Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
    // Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
}