package com.n2nconnect.android.stock.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.LayoutSettings;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.QcMessageParser;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.FormatUtil;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class IndicesActivity extends CustomWindow 
implements OnRefreshStockListener, OnDoubleLoginListener{

	// Mary@20120724 - Fixes_Request-20120724, ReqNo.2
	// private String userParam;
	private List<StockSymbol> stockSymbols;
	private boolean isDisplayPercentage;    
	private List<TextView> nameLabels;
	private List<TextView> priceLabels;
	private List<Button> changeButtons;
	/* Mary@20130718 - Change_Request-20130424, ReqNo.13
	// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13
	private List<TextView> exchgLabels;
	*/
	private List<TextView> highLabels;
	private List<TextView> lowLabels;
	private List<TextView> openLabels;
	private List<TextView> closeLabels;		//Added by Thinzar, Change_Request-20141101, ReqNo 7
	
	private RefreshStockThread refreshThread;
	private DoubleLoginThread loginThread;
	private ProgressDialog dialog;
	
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
	private int intRetryCtr	= 0;
	private AlertDialog alertException;
	// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
	
	private String strExchgCode;

	final Handler handler = new Handler() {
		public void handleMessage(Message message) {
			List<StockSymbol> changedSymbols = (List<StockSymbol>) message.obj;
			IndicesActivity.this.refreshStockData(changedSymbols);
		}
	};
	
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);

			setContentView(R.layout.indices_view);

			this.title.setText(getResources().getString(R.string.indices_module_title));
			/* Mary@20120814 - Change_Request-20120719, ReqNo.4
			this.icon.setImageResource(LayoutSettings.menu_indices_icn);
			*/
			this.icon.setImageResource( AppConstants.brokerConfigBean.getIntIndiciesMenuIconBgImg() );

			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
			userParam = sharedPreferences.getString(PreferenceConstants.QC_USER_PARAM, null);
			if (userParam == null) {
				
				List<String>data = QcHttpConnectUtil.relogin();
				userParam = data.get(0);
				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.putString(PreferenceConstants.QC_USER_PARAM, userParam);
				editor.commit();
			}
			*/

			changeButtons 	= new ArrayList<Button>();
			nameLabels 		= new ArrayList<TextView>();
			priceLabels 	= new ArrayList<TextView>();
			/* Mary@20130718 - Change_Request-20130424, ReqNo.13
			// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13
			exchgLabels		= new ArrayList<TextView>();
			*/
			highLabels 		= new ArrayList<TextView>();
			lowLabels 		= new ArrayList<TextView>();
			openLabels 		= new ArrayList<TextView>();
			closeLabels		= new ArrayList<TextView>();	//Added by Thinzar, Change_Request-20141101, ReqNo 7

			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			alertException = new AlertDialog.Builder(IndicesActivity.this)
				//.setIcon(android.R.attr.alertDialogStyle)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						dialog.dismiss();
						return;
					}
				})
				.create();
			// Added Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
			
			strExchgCode	= AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase(AppConstants.MY_EXCHANGE_CODE) ? "KL" : AppConstants.DEFAULT_EXCHANGE_CODE;
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void onResume() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onResume();
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
			currContext	= IndicesActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
			
			// Added by Mary@20121116 - Change_Request-20120719, ReqNo.12
			AppConstants.updateTradePreferenceSharedPreference(this, sharedPreferences, null);
			
			loginThread = ((StockApplication)IndicesActivity.this.getApplication()).getDoubleLoginThread();
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
			if(loginThread == null){
				if(AppConstants.hasLogout){
					finish();
				}else{
					loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
					if (AppConstants.brokerConfigBean.isEnableEncryption())
						loginThread.setUserParam(AppConstants.userParamInByte);
					loginThread.start();
					((StockApplication) IndicesActivity.this.getApplication()).setDoubleLoginThread(loginThread);
				}
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
			}else if( loginThread.isPaused() ){
				loginThread.setRegisteredListener(IndicesActivity.this);
				loginThread.resumeRequest();
			// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
			}
			// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(IndicesActivity.this);
					
			dialog		= new ProgressDialog(this);
			
			// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5
			if(idleThread == null || ( idleThread != null && ! idleThread.isExpired() ) )
				new stockRefresh().execute();
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END
	}
	
	@Override
	protected void onPause() {
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
		try{
			super.onPause();
			
			if(this.mainMenu.isShowing())
				this.mainMenu.hide();
		
			if(refreshThread!=null)
				refreshThread.pauseRequest();
			
		// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
		Message message	= handler.obtainMessage();
		message.obj 	= changedSymbols;
		handler.sendMessage(message);

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
			
			if(this.mainMenu.isShowing()){
				this.mainMenu.hide();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			}else{
				finish();
				/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
				return true;
				*/
			}
			// Added by Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}

	private void refreshStockData(List<StockSymbol> changedSymbols) {
		try {
			int id = 0;
			for (Iterator<StockSymbol> itr=changedSymbols.iterator(); itr.hasNext(); ) {
				final StockSymbol symbol= itr.next();
				StockSymbol current 	= stockSymbols.get(id);

				if (symbol.getSymbolCode().equals(current.getSymbolCode())) {

					if (symbol.checkDiscrepancy(current)) {

						if (symbol.getLastDone() != current.getLastDone() || symbol.getTotalTrade() != current.getTotalTrade()) {
							final TextView priceLabel 	= priceLabels.get(id);
							float gap 					= symbol.getLastDone()-current.getLastDone();

							current.setLastDone(symbol.getLastDone());
							/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
							priceLabel.setText(FormatUtil.formatDouble(symbol.getLastDone()));
							*/
							//priceLabel.setText(FormatUtil.formatDouble(symbol.getLastDone(), "-"));
							priceLabel.setText(FormatUtil.formatDoubleByExchange(symbol.getLastDone(), "-", strExchgCode));		//Edited by Thinzar, Change_Request-20141101, ReqNo. 8

							if (gap > 0) {
								/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
								priceLabel.setBackgroundColor(LayoutSettings.colorGreen);
								priceLabel.setTextColor(Color.BLACK);
								*/
								priceLabel.setBackgroundColor( getResources().getColor(R.color.UpStatus_BgColor) );
								priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_FgColor) );
							} else if (gap < 0) {
								/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
								priceLabel.setBackgroundColor(LayoutSettings.colorRed);
								priceLabel.setTextColor(Color.WHITE);
								*/
								priceLabel.setBackgroundColor( getResources().getColor(R.color.DownStatus_BgColor) );
								priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_FgColor) );
							/* Mary@20120905 - Fixes_Request-20120815, ReqNo.8
							} else if (gap == 0 && symbol.getTotalTrade() != current.getTotalTrade()) {
							*/
							}else if( gap == 0 && symbol.getVolume() != current.getVolume() ){
								/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
								priceLabel.setBackgroundColor(LayoutSettings.colorYellow);
								priceLabel.setTextColor(Color.BLACK);
								*/
								priceLabel.setBackgroundColor( getResources().getColor(R.color.DiffTtlStatus_BgColor) );
								priceLabel.setTextColor( getResources().getColor(R.color.DiffTtlStatus_FgColor) );
							}

							priceLabel.postDelayed(new Runnable() {
								public void run() {
									priceLabel.setBackgroundColor(Color.TRANSPARENT);
									if(symbol.getPriceChange() > 0)
										/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
										priceLabel.setTextColor(LayoutSettings.colorGreen);
										*/
										priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
									else if(symbol.getPriceChange() < 0)
										/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
										priceLabel.setTextColor(LayoutSettings.colorRed);
										*/
										priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
									else
										/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO' status
										priceLabel.setTextColor(Color.BLACK);
										*/
										priceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );
								}
							}, 1500);


							Button changeButton = changeButtons.get(id);
							if(isDisplayPercentage){
								changeButton.setText(FormatUtil.formatPricePercent(current.getPriceChange()));
							}else{
								changeButton.setText(FormatUtil.formatIndicesChangeByExchange(current.getPriceChange(), strExchgCode));
							}
							/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
							if (current.getPriceChange()<0) {
								changeButton.setBackgroundResource(LayoutSettings.btnRed);
							} else {
								changeButton.setBackgroundResource(LayoutSettings.btnGreen);
							}
							*/
							changeButton.setBackgroundResource( (current.getPriceChange()<0) ? LayoutSettings.btnRed : LayoutSettings.btnGreen);
						}

						if (symbol.getHigh()!=current.getHigh()) {
							current.setHigh(symbol.getHigh());
							TextView highLabel = highLabels.get(id);
							/*  Mary@20121001 - Fixes_Request-20120815, ReqNo.15
							highLabel.setText(FormatUtil.formatFloat(symbol.getHigh()));
							*/
							//highLabel.setText( FormatUtil.formatFloat(symbol.getHigh(), "-") );
							highLabel.setText(FormatUtil.formatDoubleByExchange(symbol.getHigh(), "-", strExchgCode));	//Edited by Thinzar, Change_Request-20141101, ReqNo. 8
						}
						if (symbol.getLow()!=current.getLow()) {
							current.setLow(symbol.getLow());
							TextView lowLabel = lowLabels.get(id);
							/*  Mary@20121001 - Fixes_Request-20120815, ReqNo.15
							lowLabel.setText(FormatUtil.formatFloat(symbol.getLow()));
							*/
							//lowLabel.setText(FormatUtil.formatFloat(symbol.getLow(), "-"));
							lowLabel.setText(FormatUtil.formatDoubleByExchange(symbol.getLow(), "-", strExchgCode));	//Edited by Thinzar, Change_Request-20141101, ReqNo. 8
						}
					}  
				}
				id++;
			}

		}catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private class stockRefresh extends AsyncTask<View, Void, List<StockSymbol>>{
		// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2
		Exception exception	= null;
		
		private AsyncTask<View, Void, List<StockSymbol>> updateTask = null;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				updateTask = this;
		        IndicesActivity.this.dialog.setMessage(getResources().getString(R.string.dialog_progress_loading));
		        // Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
	 			if( ! IndicesActivity.this.isFinishing() )
	 				IndicesActivity.this.dialog.show();
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
	 		// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
	    }
		
		@Override
		protected List<StockSymbol> doInBackground(View... params) {
			// TODO Auto-generated method stub
			String response = null;
			/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
			Map parameters = new HashMap();
			parameters.put(ParamConstants.USERPARAM_TAG, userParam);
			parameters.put(ParamConstants.EXCHANGECODE_TAG, AppConstants.DEFAULT_EXCHANGE_CODE);
			
			try {
				response = QcHttpConnectUtil.getIndices(parameters);
			} catch (FailedAuthenicationException e) {
				// TODO Auto-generated catch block
				updateTask.cancel(true);
				new stockRefresh().execute();
				e.printStackTrace();
			}
			*/
			try{
				/* Mary@20130312 - Fixes_Request-20130108, ReqNo.19
				// Added by Mary@20130110 - Fixes_Request-20130108, ReqNo.4
				String strExchgCode	= AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase("MY") ? "KL" : AppConstants.DEFAULT_EXCHANGE_CODE;
				*/
				String strExchgCode	= AppConstants.DEFAULT_EXCHANGE_CODE.equalsIgnoreCase(AppConstants.MY_EXCHANGE_CODE) ? "KL" : AppConstants.DEFAULT_EXCHANGE_CODE;
				
				/* Mary@20130110 - Fixes_Request-20130108, ReqNo.4
				response = QcHttpConnectUtil.getIndices(AppConstants.DEFAULT_EXCHANGE_CODE);
				*/
				response 			= QcHttpConnectUtil.getIndices(strExchgCode);
				while(intRetryCtr < AppConstants.intMaxRetry && response == null){
					intRetryCtr++;
					/* Mary@20130110 - Fixes_Request-20130108, ReqNo.4
					response = QcHttpConnectUtil.getIndices(AppConstants.DEFAULT_EXCHANGE_CODE);
					*/
					response = QcHttpConnectUtil.getIndices(strExchgCode);
				}
				intRetryCtr			= 0;
				
				if(response == null)				
					throw new Exception(ErrorCodeConstants.FAIL_GET_INDICES);
			}catch(Exception e){
				exception = e;
			}
			// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END

			if(response!=null)
				stockSymbols = QcMessageParser.parseIndices(response);
			
			return stockSymbols;
		}
		
		@Override
		protected void onPostExecute(List<StockSymbol> stockSymbols){
			super.onPostExecute(stockSymbols);
			
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
			try{
				if( dialog != null && dialog.isShowing() )
					dialog.dismiss();
				
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
				if(exception != null){
					alertException.setMessage( exception.getMessage() );
					alertException.setButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {
							dialog.dismiss();
							finish();
						}
					});
					// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
		 			if( ! IndicesActivity.this.isFinishing() )
		 				alertException.show();	
	
					exception = null;
					
					return;
				}
				// Added by Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				
				if(stockSymbols != null)
					IndicesActivity.this.constructTableView();
	
				String[] symbolCodes= SystemUtil.getSymbolCodes(stockSymbols);
	
				refreshThread 		= ((StockApplication) IndicesActivity.this.getApplication()).getRefreshThread();
	
				if(refreshThread == null){
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
					if(AppConstants.hasLogout){
						finish();
					}else{
					// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
						int rate 		= sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
						refreshThread 	= new RefreshStockThread(AppConstants.QCdata.getStrUserParam(), symbolCodes, rate);
						refreshThread.start();
						((StockApplication) IndicesActivity.this.getApplication()).setRefreshThread(refreshThread);
					}
				/* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
				/* Mary@20130924 - Fixes_Request-20130711, ReqNo.22
				}else{
				/
				}else if( refreshThread.isPaused() ){
					refreshThread.resumeRequest();
					refreshThread.setSymbolCodes(symbolCodes);
				*/
				}
				refreshThread.setRegisteredListener(IndicesActivity.this);
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29
				refreshThread.setSymbolCodes(symbolCodes);
				
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - START
				if( refreshThread.isPaused() ){
					/* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
					refreshThread.setSymbolCodes(symbolCodes);
					*/
					refreshThread.resumeRequest();
				}
				// Added by Mary@20130930 - Fixes_Request-20130711, ReqNo.29 - END
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
			}catch(Exception e){
				e.printStackTrace();
			}
			// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
		}

	}

	private void constructTableView() {
		TableLayout tableLayout = (TableLayout) findViewById(R.id.symbolTable);
		tableLayout.removeAllViews();
		
		int id = 0;
		for (Iterator<StockSymbol> itr=stockSymbols.iterator(); itr.hasNext(); ) {
			StockSymbol symbol 			= itr.next();

			final TableRow tableRow 	= new TableRow(this);   
			tableRow.setId(id);
			tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
			tableRow.setWeightSum(1);
			View rowView 				= this.getLayoutInflater().inflate(R.layout.symbol_row, null);  
			final LinearLayout rowLayout= (LinearLayout) rowView.findViewById(R.id.rowLayout);

			TextView nameLabel 			= (TextView) rowView.findViewById(R.id.nameLabel);
			/* Mary@20130718 - Change_Request-20130424, ReqNo.13
			nameLabel.setText(symbol.getStockName());
			*/
			nameLabel.setText( symbol.getStockName(true) );
			nameLabels.add(nameLabel);

			final TextView priceLabel 	= (TextView) rowView.findViewById(R.id.priceLabel);
			/*  Mary@20121001 - Fixes_Request-20120815, ReqNo.15
			priceLabel.setText(FormatUtil.formatDouble(symbol.getLastDone()));
			*/
			priceLabel.setText(FormatUtil.formatDoubleByExchange(symbol.getLastDone(), "-", strExchgCode));

			if(symbol.getPriceChange() > 0)
				/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO'  status
				priceLabel.setTextColor(LayoutSettings.colorGreen);
				*/
				priceLabel.setTextColor( getResources().getColor(R.color.UpStatus_BgColor) );
			else if(symbol.getPriceChange() < 0)
				/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO'  status
				priceLabel.setTextColor(LayoutSettings.colorRed);
				*/
				priceLabel.setTextColor( getResources().getColor(R.color.DownStatus_BgColor) );
			else
				/* Mary@20120815 - standarized status color for 'UP', 'DOWN' and 'NO'  status
				priceLabel.setTextColor(Color.BLACK);
				*/
				priceLabel.setTextColor( getResources().getColor(R.color.NoStatus_FgColor) );

			priceLabels.add(priceLabel);

			final Button changeButton 	= (Button) rowView.findViewById(R.id.changeButton);             
			changeButton.setText(FormatUtil.formatIndicesChangeByExchange(symbol.getPriceChange(), strExchgCode));
			/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			if(symbol.getPriceChange()<0)
				changeButton.setBackgroundResource(LayoutSettings.btnRed);
			else
				changeButton.setBackgroundResource(LayoutSettings.btnGreen);
			*/
			changeButton.setBackgroundResource( (symbol.getPriceChange()<0) ? LayoutSettings.btnRed : LayoutSettings.btnGreen );
			changeButton.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					IndicesActivity.this.changeButtonPercentage();
				}
			});

			changeButtons.add(changeButton);

			tableRow.addView(rowView);

			/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
			if ((id+1)%2 != 0) {
				rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
			} else {
				rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
			}
			*/
			//rowLayout.setBackgroundResource( ( (id+1)%2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);		//Change_Request-20160101, ReqNo.3
			final View rowExpandView	= IndicesActivity.this.getLayoutInflater().inflate(R.layout.indices_expand_row, null);
			populateExpandViewData(rowExpandView, symbol);

			tableRow.setOnClickListener(new OnClickListener() {
				public void onClick(View view) {
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
					
					StockSymbol symbol = stockSymbols.get(tableRow.getId());
					if (!symbol.isExpand()) {

						rowLayout.addView(rowExpandView); 
						/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
						if ((tableRow.getId()+1)%2 != 0) {
							rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark_expand);
						} else {
							rowLayout.setBackgroundResource(LayoutSettings.tablerow_light_expand);
						}
						*/
						rowLayout.setBackgroundResource( ( (tableRow.getId()+1)%2 != 0 ) ? LayoutSettings.tablerow_dark_expand : LayoutSettings.tablerow_light_expand );

						symbol.setExpand(true);
					} else {
						/* Mary@20130605 - comment unused variable
						int childCount = rowLayout.getChildCount();
						*/
						rowLayout.removeViewAt(1);
						symbol.setExpand(false);
						/* Mary@20121004 - Fixes_Request-20120815, ReqNo.3
						if ((tableRow.getId()+1)%2 != 0) { 
							rowLayout.setBackgroundResource(LayoutSettings.tablerow_dark);
						} else {
							rowLayout.setBackgroundResource(LayoutSettings.tablerow_light);
						}
						*/
						rowLayout.setBackgroundResource( ( (tableRow.getId()+1)%2 != 0 ) ? LayoutSettings.tablerow_dark : LayoutSettings.tablerow_light);
						
					}
				}
			});
	
			// Add the TableRow to the TableLayout
			tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));  
			
			id++;
		}

	}

	private void changeButtonPercentage() {
		int index			= 0;
		for (Iterator<Button> itr=changeButtons.iterator(); itr.hasNext(); ) {
			Button changeButton = itr.next();
			StockSymbol symbol	= stockSymbols.get(index);
			if(isDisplayPercentage)
				changeButton.setText(FormatUtil.formatIndicesChangeByExchange(symbol.getPriceChange(), strExchgCode));				
			else
				changeButton.setText(FormatUtil.formatPricePercent(symbol.getChangePercent()));
			
			index++;
		}
		/* Mary@20120924 - Fixes_Request-20120815, ReqNo.3
		if (isDisplayPercentage) {
			isDisplayPercentage = false;
		} else {
			isDisplayPercentage = true;
		}
		*/
		isDisplayPercentage= !isDisplayPercentage;
	}

	private void populateExpandViewData(View expandView, StockSymbol symbol) {
		/* Mary@20130718 - Change_Request-20130424, ReqNo.13
		// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - START
		TextView exchgLabel 		= (TextView) expandView.findViewById(R.id.tvFieldExchgType);
		exchgLabel.setText(AppConstants.DEFAULT_EXCHANGE_CODE);
		exchgLabels.add(exchgLabel);
		// Added by Mary@20130715 - Change_Request-20130424, ReqNo.13 - END
		*/
				
		TextView openLabel	= (TextView) expandView.findViewById(R.id.openLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		openLabel.setText(FormatUtil.formatDouble(symbol.getOpen()));
		*/
		openLabel.setText(FormatUtil.formatDoubleByExchange(symbol.getOpen(), "-", strExchgCode));	//Edited by Thinzar, Change_Request-20141101, ReqNo. 8
		openLabels.add(openLabel);
		
		//Added by Thinzar, Change_Request-20141101, ReqNo 7 - START
		TextView closeLabel	= (TextView) expandView.findViewById(R.id.closeLabel);
		closeLabel.setText(FormatUtil.formatDoubleByExchange(symbol.getPreviousClose(), "-", strExchgCode));
		closeLabels.add(closeLabel);
		//Added by Thinzar, Change_Request-20141101, ReqNo 7 - END
		
		TextView highLabel	= (TextView) expandView.findViewById(R.id.highLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		highLabel.setText(FormatUtil.formatDouble(symbol.getHigh()));
		*/
		highLabel.setText(FormatUtil.formatDoubleByExchange(symbol.getHigh(), "-", strExchgCode));	//Edited by Thinzar, Change_Request-20141101, ReqNo. 8
		highLabels.add(highLabel);
		
		TextView lowLabel	= (TextView) expandView.findViewById(R.id.lowLabel);
		/* Mary@20121001 - Fixes_Request-20120815, ReqNo.15
		lowLabel.setText(FormatUtil.formatDouble(symbol.getLow()));
		*/
		lowLabel.setText(FormatUtil.formatDoubleByExchange(symbol.getLow(), "-", strExchgCode));	//Edited by Thinzar, Change_Request-20141101, ReqNo. 8
		lowLabels.add(lowLabel);
	}
	
	public void showMainThreadAlert(Exception ex) {
		Message message = handler.obtainMessage();
		message.obj 	= ex;
		handlerError.sendMessage(message);
	}


	final Handler handlerError = new Handler() {
		public void handleMessage(Message message) {
			Exception ex = (Exception) message.obj;
			SystemUtil.showExceptionAlert(ex, IndicesActivity.this);
		}
	};

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message = handler3.obtainMessage();
		message.obj		= response;
		handler3.sendMessage(message);
	}

	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if( response.get(2).equals("1102") ){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if( IndicesActivity.this.mainMenu.isShowing() )
					IndicesActivity.this.mainMenu.hide();

				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(refreshThread != null)
					refreshThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(LoginActivity.QCAlive != null)
					LoginActivity.QCAlive.stopRequest();
				if( ! isFinishing() )
					IndicesActivity.this.doubleLoginMessage( (String)response.get(1) );
			}
		}
	};
}