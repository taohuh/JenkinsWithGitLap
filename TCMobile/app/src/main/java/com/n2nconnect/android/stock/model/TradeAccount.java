package com.n2nconnect.android.stock.model;

import java.util.ArrayList;
import java.util.List;

public class TradeAccount {
	
	private String clientCode;
	private String accountNo;
	private String clientName;
	private String cdsNo;
	private String rmsCode;
	private String brokerCode;
	private String branchCode;
	private double creditLimit;
	private double buyLimit;
	private double sellLimit;
	private String brokerage;
	private String accountStatus;
	private String exchangeCode;
	private double accountBalance;
	private String groupId;
	private boolean forceOrder;
	private List<String> supportedExchange;
	private boolean shortSell;
	private boolean marginAccount;
	
	//Added by Thinzar, Change_Request-20150401, ReqNo.1
	private String accountType;
	
	//Added by Thinzar, Change_Request-20160101, ReqNo. 1
	private String senderCode;

	public TradeAccount() {
		supportedExchange = new ArrayList<String>();
	}
	
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public boolean isForceOrder() {
		return forceOrder;
	}
	public void setForceOrder(boolean forceOrder) {
		this.forceOrder = forceOrder;
	}
	public List<String> getSupportedExchange() {
		return supportedExchange;
	}
	public void setSupportedExchange(List<String> supportedExchange) {
		this.supportedExchange = supportedExchange;
	}
	public boolean isShortSell() {
		return shortSell;
	}
	public void setShortSell(boolean shortSell) {
		this.shortSell = shortSell;
	}
	public boolean isMarginAccount() {
		return marginAccount;
	}
	public void setMarginAccount(boolean marginAccount) {
		this.marginAccount = marginAccount;
	}
	public double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getCdsNo() {
		return cdsNo;
	}
	public void setCdsNo(String cdsNo) {
		this.cdsNo = cdsNo;
	}
	public String getRmsCode() {
		return rmsCode;
	}
	public void setRmsCode(String rmsCode) {
		this.rmsCode = rmsCode;
	}
	public String getBrokerCode() {
		return brokerCode;
	}
	public void setBrokerCode(String brokerCode) {
		this.brokerCode = brokerCode;
	}
	public double getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}
	public double getBuyLimit() {
		return buyLimit;
	}
	public void setBuyLimit(double buyLimit) {
		this.buyLimit = buyLimit;
	}
	public double getSellLimit() {
		return sellLimit;
	}
	public void setSellLimit(double sellLimit) {
		this.sellLimit = sellLimit;
	}
	public String getBrokerage() {
		return brokerage;
	}
	public void setBrokerage(String brokerage) {
		this.brokerage = brokerage;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getExchangeCode() {
		return exchangeCode;
	}
	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}
	
	//Added by Thinzar, Change_Request-20150401, ReqNo.1 - START
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	//Added by Thinzar, Change_Request-20150401, ReqNo.1 - END
	
	//Added by Thinzar, Change_Request-20160101, ReqNo. 1 - START
	public String getSenderCode() {
		return senderCode;
	}

	public void setSenderCode(String senderCode) {
		this.senderCode = senderCode;
	}
	//Added by Thinzar, Change_Request-20160101, ReqNo. 1 - END
}
