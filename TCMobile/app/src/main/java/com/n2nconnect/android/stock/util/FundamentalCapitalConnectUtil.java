package com.n2nconnect.android.stock.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;

public class FundamentalCapitalConnectUtil {

	private static final String strProtocol	= "http";
	private static final String strHost		= "plc.asiaebroker.com";
	private static final int intPort		= 80;

	public static String getFundamentalInfoUrl(Map<String, String> mapInput){
		URL url			= null;
		String strPath	= null;
		try {
			String strSourceCode	= mapInput.get(ParamConstants.PARAM_FC_SOURCE_CODE);
			String strCategoryType	= mapInput.get(ParamConstants.PARAM_FC_CATEGORY);
			String strExchCode		= mapInput.get(ParamConstants.EXCHANGECODE_TAG);
			String strStockCode		= mapInput.get(ParamConstants.SYMBOLCODE_TAG);
			
			//Edited by Thinzar, Change_Request-20150401, ReqNo. 3 - START
			String strLMSSponsor	= AppConstants.brokerConfigBean.getStrLMSSponsor();
			String strBrokerCode	= mapInput.get(PreferenceConstants.BROKER_CODE);
			
			/*
			strPath					= "/plc/stkCompInfom.jsp?SourceCode=" + strSourceCode + "&ExchCode=" + strExchCode + "&StkCode=" + strStockCode + "&Category=" + strCategoryType;
			url						= new URL(strProtocol, strHost, intPort, strPath);
			*/
			
			//e.g., https://plc.asiaebroker.com/gcPLC/stkCompInfom.jsp?SourceCode=FCS&ExchCode=KL&StkCode=0020&Category=Info&Sponsor=4&BHCode=065
			strPath					= "stkCompInfom.jsp?SourceCode=" + strSourceCode + "&ExchCode=" + strExchCode + "&StkCode=" + strStockCode + "&Category=" + strCategoryType
										+ "&Sponsor=" + strLMSSponsor + "&BHCode=" + strBrokerCode; 
			url						= new URL(AppConstants.brokerConfigBean.getFundamentalNewsServer() + strPath);
			//Edited by Thinzar, Change_Request-20150401, ReqNo. 3 - END
			
			DefinitionConstants.Debug("[FundamentalCapitalConnectUtil][getFundamentalInfoUrl]Url address: " + url.toString() );

			return url.toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}finally{
			url 	= null;
			strPath	= null;
		}

		return null;
	}
	
	public static String getFinancialReportUrl(Map<String, String> mapInput){
		URL url			= null;
		String strPath	= null;
		try {
			String strSourceCode	= mapInput.get(ParamConstants.PARAM_FC_SOURCE_CODE);
			String strExchCode		= mapInput.get(ParamConstants.EXCHANGECODE_TAG);
			String strStockCode		= mapInput.get(ParamConstants.SYMBOLCODE_TAG);
			String strReportType	= mapInput.get(ParamConstants.PARAM_FC_REPORT_TYPE);
    		//String strReportPeriod	= mapInput.get(ParamConstants.PARAM_FC_REPORT_PERIOD);
			
    		//Edited by Thinzar, Change_Request-20150401, ReqNo. 3 - START
			String strReportPeriod	= AppConstants.brokerConfigBean.getFundamentalDefaultReportCode();
			String strLMSSponsor	= AppConstants.brokerConfigBean.getStrLMSSponsor();
			String strBrokerCode	= mapInput.get(PreferenceConstants.BROKER_CODE);
			/*
			strPath					= "/plc/stkFincInfom.jsp?SourceCode=" + strSourceCode + "&ExchCode=" + strExchCode + "&StkCode=" + strStockCode + "&Report=" + strReportType + "&DataItemType=" + strReportPeriod;
			url						= new URL(strProtocol, strHost, intPort, strPath);
			*/
			strPath					= "stkFincInfom.jsp?SourceCode=" + strSourceCode + "&ExchCode=" + strExchCode + "&StkCode=" + strStockCode + "&Report=" + strReportType + "&DataItemType=" + strReportPeriod
									  + "&Sponsor=" + strLMSSponsor + "&BHCode=" + strBrokerCode;
			url						= new URL(AppConstants.brokerConfigBean.getFundamentalNewsServer() + strPath);
			//Edited by Thinzar, Change_Request-20150401, ReqNo. 3 - END
			
			DefinitionConstants.Debug("[FundamentalCapitalConnectUtil][getFinancialReportUrl]Url address: " + url.toString() );

			return url.toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}finally{
			url 	= null;
			strPath	= null;
		}

		return null;
	}
	
	public static String getNewFundamentalReportUrl(String symbolCode, String brokerCode, String userName, String packageId){
		/*
		 * https://plc.asiaebroker.com/gcPLC/rw.jsp?StkCode=7054.KL&SourceCode=FCS
		 * &Sponsor=59&bhCode=086&loginID=n2nuser&prodId=TCMOBILE&Type=5&key=%01%05%03%00%06%01&PackageID=1&Menu=N
		 */
		String strFullUrl	= null;
		String strBaseUrl	= AppConstants.brokerConfigBean.getNewFundamentalNewsServer();
		String sourceCode	= AppConstants.brokerConfigBean.getNewFundamentalSourceCode();
		String sponsor		= AppConstants.brokerConfigBean.getStrLMSSponsor();
		String prodId		= "TCMOBILE";
		int type			= 5;
		String key			= ArchiveNewsUtil.getEncryptedTime();
		
		strFullUrl	= strBaseUrl + "?StkCode=" + symbolCode
				+ "&SourceCode=" + sourceCode
				+ "&Sponsor=" + sponsor
				+ "&bhCode=" + brokerCode
				+ "&loginID=" + userName
				+ "&prodId=" + prodId
				+ "&Type=" + type
				+ "&key=" + key
				+ "&PackageID=" + packageId
				+ "&Menu=N";
		
		return strFullUrl;
	}
}
