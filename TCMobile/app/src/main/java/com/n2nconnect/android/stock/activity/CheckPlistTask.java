package com.n2nconnect.android.stock.activity;
/*
 * Moved out AsyncTask from LoginActivity, Change_Request-20141101, ReqNo.1 
 * This AsyncTask will hit JSP, check pListFile Version, will either download or copy plist from Assets 
 * HttpResponseCodes:
 * 200 - save to data/data/PACKAGE_NAME/plist 
 * 304 - go check if plist exists, if none, copy from asset 
 * 404 - copy from Asset
 */
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.ErrorCodeConstants;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.util.PlistFileUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class CheckPlistTask extends AsyncTask<String, Void, Exception> {
	public interface CheckPlistTaskFinishedListener {
		void onTaskFinished(); // If you want to pass something back to the listener add a param to this method
	}
	
	private Context mContext;
	private String plistFileName;
	private String mPackageName;
	private String mVersion;
	private String fromActivityTag;
	private ProgressDialog mDialog;
	private final CheckPlistTaskFinishedListener mfinishedListener;
	private String appDataPath;
	private String plistPath;	
	private final String TAG	= "CheckPlistTask";
	private static final String device = "A";
	
	public CheckPlistTask(Context c, ProgressDialog progressDialog, CheckPlistTaskFinishedListener finishedListener) {
		this.mContext 			= c;
		this.mDialog			= progressDialog;
		this.mfinishedListener	= finishedListener;
		this.appDataPath 		= c.getFilesDir().getPath();		// data/data/PACKAGE_NAME/files
		this.plistPath 			= appDataPath + "/" + PlistFileUtil.PLIST_FOLDER_NAME;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mDialog.setMessage(mContext.getResources().getString(R.string.splash_fetch_config));
		mDialog.setCancelable(false);
		mDialog.show();
	}
	
	@Override
	protected Exception doInBackground(String... values) {
		URL fullJspURL				= null;
		HttpURLConnection conn		= null;
		Integer responseCode 		= null;
		InputStream inputStream 	= null;
		
		boolean isPlistSaved = false;
		mPackageName = values[0];
		mVersion = values[1];
		fromActivityTag	= values[2];
		
		plistFileName = PlistFileUtil.getPlistFileNameFormat(mPackageName); // returns config_com.n2nconnect.iMSL.stock.plist
		String fullPath = AppConstants.LMS_JSP_PATH + "?id=" + mPackageName + "&ver=" + mVersion + "&device=" + device;	//TODO get version from SharedPref
		Log.d(TAG, "HitJspCheckPlist: " + fullPath);
		
		try {
			if(fromActivityTag.equalsIgnoreCase(ParamConstants.SPLASH_ACTIVITY_TAG)){
				//to display the splash screen a bit longer
				Thread.sleep(1000*2);
			}
			
			if(!isNetworkAvailable()){
				if(!checkPlistFileInStorage(mPackageName)) {
					throw new Exception(ErrorCodeConstants.FAIL_GET_CONFIG_FILE);
				} 
			} else {
				
				fullJspURL = new URL(fullPath);
				conn = (HttpURLConnection) fullJspURL.openConnection();
				conn.connect();

				responseCode = conn.getResponseCode();
				Log.d("Login", "ResponseCode : " + responseCode);

				if (responseCode == 200) {
					inputStream		= conn.getInputStream();
					isPlistSaved 	= PlistFileUtil.writeToExternalStorage(inputStream, plistPath, plistFileName);
				}

				if (responseCode == 404 || responseCode == 304 || isPlistSaved == false) {
					
					if(!checkPlistFileInStorage(mPackageName)) {
						throw new Exception(ErrorCodeConstants.FAIL_GET_CONFIG_FILE);
					}
				}
			}

		}
		//Added by Thinzar@20141027, to fix unknownHostException - START
		catch(IOException ioe){
			System.out.println("IOException!");
			if(!checkPlistFileInStorage(mPackageName)) {
				ioe.printStackTrace();
			}
		} 
		//Added by Thinzar@20141027, to fix unknownHostException - END
		catch (Exception ex) {
			ex.printStackTrace();
			return ex;
		}
		finally{
			if(inputStream != null){
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				inputStream = null;
			}
			if(conn != null){
				conn.disconnect();
				conn	= null;
			}
		}

		return null;
	}
	
	@Override
	protected void onPostExecute(Exception ex) {
		super.onPostExecute(ex);
		
		try{
			if(mDialog != null && mDialog.isShowing())
				mDialog.dismiss();
		} catch (Exception e){
			e.printStackTrace();
		}
		
		if (ex != null) {	
			showAlertAndExitApp(ErrorCodeConstants.FAIL_GET_CONFIG_FILE);
			return;
		}
		
		mfinishedListener.onTaskFinished(); 	// Tell whoever was listening we have finished
	}

	private boolean checkPlistFileInStorage(String mPkgName){
		System.out.println("checkPlistFileInStorage: " + mPkgName);
		String plistFileName = PlistFileUtil.getPlistFileNameFormat(mPkgName);
		File plistFile = new File(plistPath, plistFileName);
			
		if (!plistFile.exists()) {

			try {
				if (Arrays.asList(mContext.getResources().getAssets().list(PlistFileUtil.ASSETS_PLIST_FOLDER_NAME))
						.contains(plistFileName)) {
					PlistFileUtil.copyAssets(mContext, PlistFileUtil.ASSETS_PLIST_FOLDER_NAME + "/" + plistFileName, plistPath + "/" + plistFileName);
					return true;
				} else {
					return false;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		} else{
			return true;
		}
		
	}
	
	private boolean isNetworkAvailable() {
		ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	private void showAlertAndExitApp(String errorMsg){
		try{
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setMessage(errorMsg)
			.setCancelable(false)
			.setPositiveButton(mContext.getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					((Activity) mContext).finish();
				}
			});
			
			AlertDialog alert = builder.create();
			if( ! ((Activity) mContext).isFinishing() )
				alert.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
