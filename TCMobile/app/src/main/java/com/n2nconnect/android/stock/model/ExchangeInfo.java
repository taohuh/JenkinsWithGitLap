package com.n2nconnect.android.stock.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.n2nconnect.android.stock.AppConstants;
//import com.sun.istack.internal.NotNull;
import android.support.annotation.NonNull;

public class ExchangeInfo {
	
	private String QCExchangeCode;
	private String exchnageName;
	private String trdexchangeCode;
	private List<String> actions;
	private List<String> orderTypes;
	private List<String> validities;
	private List<String> paymentMethods;
	private List<String> currencies;
	private List<String> reviseCodes;
	private List<String> reviseOrderTypes;
	private List<String> reviseValidities;
	private String indiceSymbol;
	private List<Sector> sectors;
	
	// Mary@20121121 - Change_Request-20121106, ReqNo.1
	private List<OrderControlInfo> lstOrderControlInfo;
	
	// Added by Mary@20121121 - Change_Request-20121106, ReqNo.1 - START
	private boolean includeStockCurrency;
	private List<StockCurrencyPaymentMethod> lstStockCurrPymtMthd;
	// Added by Mary@20121121 - Change_Request-20121106, ReqNo.1 - END
	
	//Added by Thinzar@20140815 - Change_Request-20140810,ReqNo.4
	private List<String> triggerPriceTypes;
	private List<String> triggerPriceDirections;
	
	//Added by Thinzar@20140909 - Change_Request-20140901, ReqNo.1
	private List<OrderControlInfo> lstReviseOrderControlInfo;
	
	//Added by Thinzar@20141027, Change_Request-20140901, ReqNo. 12
	private List<LotMarket> lotMarkets;
	
	//Added by Thinzar, Change_Request-20150401, ReqNo. 1 - START
	private Map<String, PaymentConfig> paymentConfigMap;
	
	//Added by Thinzar, Change_Request-20150401, ReqNo. 5 - START
	private String currencyNotSupported;
	
	public ExchangeInfo() {
		actions 			= new ArrayList<String>();
		orderTypes 			= new ArrayList<String>();
		validities 			= new ArrayList<String>();
		paymentMethods 		= new ArrayList<String>();
		currencies 			= new ArrayList<String>();
		reviseCodes 		= new ArrayList<String>();
		reviseValidities 	= new ArrayList<String>();
		reviseOrderTypes 	= new ArrayList<String>();
		sectors 			= new ArrayList<Sector>();
		lstOrderControlInfo = new ArrayList<OrderControlInfo>();
		// Added by Mary@20121121 - Change_Request-20121106, ReqNo.1
		lstStockCurrPymtMthd = new ArrayList<StockCurrencyPaymentMethod>();
		
		//Added by Thinzar@20140815 - Change_Request-20140810,ReqNo.4
		triggerPriceTypes 		= new ArrayList<String>();
		triggerPriceDirections 	= new ArrayList<String>();
		
		//Added by Thinzar@20140909 - Change_Request-20140901, ReqNo.1
		lstReviseOrderControlInfo	= new ArrayList<OrderControlInfo>();
		
		//Added by Thinzar@20141027, Change_Request-20140901, ReqNo. 12
		lotMarkets 				= new ArrayList<LotMarket>();
		
		//Added by Thinzar, Change_Request-20150401, ReqNo. 1
		paymentConfigMap		= new HashMap<String, PaymentConfig>();
		
	}
	
	public List<Sector> getSectors() {
		return sectors;
	}
	public void setSectors(List<Sector> sectors) {
		this.sectors = sectors;
	}
	public void addSector(Sector sector) {
		this.sectors.add(sector);
	}
	
	public String getIndiceSymbol() {
		return indiceSymbol;
	}
	public void setIndiceSymbol(String indiceSymbol) {
		this.indiceSymbol = indiceSymbol;
	}
	
	public void addReviseOrderType(String reviseOrderType) {
		reviseOrderTypes.add(reviseOrderType);
	}
	public List<String> getReviseOrderTypes() {
		return reviseOrderTypes;
	}
	public void setReviseOrderTypes(List<String> reviseOrderTypes) {
		this.reviseOrderTypes = reviseOrderTypes;
	}
	
	public String getQCExchangeCode() {
		return QCExchangeCode;
	}
	public void setQCExchangeCode(String exchangeCode) {
		this.QCExchangeCode = exchangeCode;
	}
	
	public String getTrdExchgCode(){
		return trdexchangeCode;
	}
	public void setTrdExchangeCode(String exchangeCode) {
		this.trdexchangeCode = exchangeCode;
	}
	
	public String getExchnageName() {
		return exchnageName;
	}
	public void setExchnageName(String exchnageName) {
		this.exchnageName = exchnageName;
	}
	
	public void addAction(String action) {
		actions.add(action);
	}
	public List<String> getActions() {
		return actions;
	}
	public void setActions(List<String> actions) {
		this.actions = actions;
	}
	
	public void addOrderType(String orderType) {
		orderTypes.add(orderType);
	}
	public List<String> getOrderTypes() {
		return orderTypes;
	}
	public void setOrderTypes(List<String> orderTypes) {
		this.orderTypes = orderTypes;
	}
	
	public void addValidity(String validity) {
		validities.add(validity);
	}
	public List<String> getValidities() {
		return validities;
	}
	public void setValidities(List<String> validities) {
		this.validities = validities;
	}
	
	public void addPaymentMethod(String paymentMethod) {
		paymentMethods.add(paymentMethod);
	}
	public List<String> getPaymentMethods() {
		return paymentMethods;
	}
	public void setPaymentMethods(@NonNull List<String> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	
	public void addCurrency(String currency) {
		currencies.add(currency);
	}
	
	public List<String> getCurrencies() {
		return currencies;
	}
	public void setCurrencies(@NonNull List<String> currencies) {
		this.currencies = currencies;
	}
	
	public void addReviseCode(String reviseCode) {
		reviseCodes.add(reviseCode);
	}
	public List<String> getReviseCodes() {
		return reviseCodes;
	}
	
	public void setReviseCodes(List<String> reviseCodes) {
		this.reviseCodes = reviseCodes;
	}
	public void addReviseValidity(String reviseValidity) {
		reviseValidities.add(reviseValidity);
	}
	
	public List<String> getReviseValidities() {
		return reviseValidities;
	}
	public void setReviseValidities(List<String> reviseValidities) {
		this.reviseValidities = reviseValidities;
	}
	
	// Mary@20121121 - Changes_Request-20121106, ReqNo.1 - START
	public List<OrderControlInfo> getOrderCtrltoList(){
		return lstOrderControlInfo;
	}
	
	public void addOrderCtrltoList(OrderControlInfo ori){
		lstOrderControlInfo.add(ori);
	}
	// Mary@20121121 - Changes_Request-20121106, ReqNo.1 - END
	
	// Added by Mary@20121121 - Changes_Request-20121106, ReqNo.1 - START
	public boolean isIncludeStockCurrency(){
		return includeStockCurrency;
	}
	public void setIncludeStockCurrency(boolean isInclude){
		this.includeStockCurrency	= isInclude;
	}
	
	public List<StockCurrencyPaymentMethod> getLstStockCurrencyPaymentMethods(){
		return this.lstStockCurrPymtMthd;
	}
	public void addStockCurrencyPaymentMethod(StockCurrencyPaymentMethod stkCurrPymtMthd){
		this.lstStockCurrPymtMthd.add(stkCurrPymtMthd);
	}
	// Added by Mary@20121121 - Changes_Request-20121106, ReqNo.1 - END
	
	//Added by Thinzar@20140815 - Change_Request-20140810,ReqNo.4 - START
	public void addTriggerPriceType(String strTriggerPriceType) {
		this.triggerPriceTypes.add(strTriggerPriceType);
	}
	
	public List<String> getTriggerPriceTypes() {
		return triggerPriceTypes;
	}
		
	public void setTriggerPriceTypes(List<String> lstTriggerPriceType) {
		this.triggerPriceTypes = lstTriggerPriceType;
	}
		
	public void addTriggerPriceDirection(String strTriggerPriceDirection){
		this.triggerPriceDirections.add(strTriggerPriceDirection);
	}
		
	public List<String> getTriggerPriceDirections(){
		return this.triggerPriceDirections;
	}
		
	public void setTriggerPirceDirections(List<String> lstTriggerPriceDirections){
		this.triggerPriceDirections = lstTriggerPriceDirections;
	}
	//Added by Thinzar@20140815 - Change_Request-20140810,ReqNo.4 - END
	
	//Added by Thinzar@20140909 - Change_Request-20140901, ReqNo.1 - START
	public List<OrderControlInfo> getReviseOrderCtrltoList(){
		return lstReviseOrderControlInfo;
	}
	public void addReviseOrderCtrltoList(OrderControlInfo reviseOci){
		lstReviseOrderControlInfo.add(reviseOci);
	}
	//Added by Thinzar@20140909 - Change_Request-20140901, ReqNo.1 - END
	
	//Added by Thinzar@20141027, Change_Request-20140901, ReqNo. 12 - START
	public List<LotMarket> getLotMarkets() {
		return lotMarkets;
	}

	public void setLotMarkets(List<LotMarket> lotMarkets) {
		this.lotMarkets = lotMarkets;
	}
	
	public void addLotMarket(LotMarket market){
		this.lotMarkets.add(market);
	}
	
	public List<LotMarket> processAndGetMarketInfo(){
		
		this.lotMarkets.clear();
		ArrayList<String> filterShowMarketArray	= AppConstants.brokerConfigBean.getFilterShowMarketArray();
		List<LotMarket> lotMarketList = new ArrayList<LotMarket>();
		
		for(int i=0;i<this.sectors.size();i++){
			
			Sector sector	= sectors.get(i);
		
			if(Integer.parseInt(sector.getSectorCode()) >= 1000){
				//ignore
				continue;
			} else { 
				LotMarket lm	= new LotMarket();
				
				lm.setCode(sector.getSectorCode());
				
				if(sector.getSectorCode().equals("10")){
				
					lm.setName("Normal Board Lot");
					lm.setSymbol("");
					
				} else if(sector.getSectorCode().equals("15")){
				
					if(this.QCExchangeCode.equalsIgnoreCase("PH") || this.QCExchangeCode.equalsIgnoreCase("PHD"))
						lm.setName("Odd Lot");
					else
						lm.setName("Normal Odd Lot");
					
					lm.setSymbol("_");
				} else if(sector.getSectorCode().equals("16")){
					
					lm.setName(sector.getSectorName());
					lm.setSymbol("$");
					
				} else if(sector.getSectorCode().equals("17")){
					
					lm.setName("Buy In Board Lot");
					lm.setSymbol("!");
					
				} else if(sector.getSectorCode().equals("18")){
					
					lm.setName("Buy In Odd Lot");
					lm.setSymbol("^");
					
				} else if(sector.getSectorCode().equals("19")){
					
					lm.setName("Off Market");
					lm.setSymbol("#");
					
				}  else {
					//for other possibilities
					lm.setName(sector.getSectorName());
					lm.setSymbol("");
				}
				
				if(filterShowMarketArray != null && filterShowMarketArray.size() > 0){
					if(filterShowMarketArray.contains(sector.getSectorCode())){
						lotMarketList.add(lm);
					}
				} else {
					lotMarketList.add(lm);
				}
				
			}
		}//end for loop
		
		return lotMarketList;
	}
	//Added by Thinzar@20141027, Change_Request-20140901, ReqNo. 12 - END

	//Added by Thinzar, Change_Request-20150401, ReqNo. 1 - START
	public Map<String, PaymentConfig> getPaymentConfigMap() {
		return paymentConfigMap;
	}
	
	public void addPaymentConfigMap(String key, PaymentConfig config){
		this.paymentConfigMap.put(key, config);
	}
	
	public void setPaymentConfigMap(Map<String, PaymentConfig> paymentConfigMap) {
		this.paymentConfigMap = paymentConfigMap;
	}
	//Added by Thinzar, Change_Request-20150401, ReqNo. 1 - END

	//Added by Thinzar, Change_Request-20150401, ReqNo. 5 - START
	public void setCurrencyNotSupported(String currency){
		this.currencyNotSupported	= currency;
	}
	
	public String getCurrencyNotSupported(){
		return currencyNotSupported;
	}
	//Added by Thinzar, Change_Request-20150401, ReqNo. 5 - END
	
}
