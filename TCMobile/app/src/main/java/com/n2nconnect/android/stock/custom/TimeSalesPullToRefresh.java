package com.n2nconnect.android.stock.custom;


import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.TextView;

import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

public class TimeSalesPullToRefresh extends LinearLayout {

	private static final int TAP_TO_REFRESH = 1; 
	private static final int PULL_TO_REFRESH = 2; 
	private static final int RELEASE_TO_REFRESH = 3; 
	private static final int REFRESHING = 4; 
	public int mRefreshState;
	public int mfooterRefreshState;
	public Scroller scroller;
	public ScrollView sv;
	private View refreshView;
	public View mfooterView;
	public TextView mfooterViewText;
	private ImageView refreshIndicatorView;
	public ImageView refreshIndicatorView2;
	public ImageView information;
	public int refreshTargetTop;
	public int refreshFooter;
	private ProgressBar bar;
	public ProgressBar bar2;
	private TextView downTextView;
	private boolean isvalidate = false;
	private RefreshListener refreshListener;
	private int lastY;

	private RotateAnimation mFlipAnimation;
	private RotateAnimation mReverseFlipAnimation;
	public int nowpull = -1;

	private boolean isRecord;
	private Context mContext;
	
	public TimeSalesPullToRefresh(Context context) {
		super(context);
		mContext = context;

	}

	public TimeSalesPullToRefresh(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		init();

	}
	
	//return the right topmargin value base on the density of the display	
	/*
	public static int getTopMargin(TimeSalesPullToRefresh tryRefreshableView){
		int i = 0;
		 Resources resources = tryRefreshableView.getResources();
		 DisplayMetrics metrics = resources.getDisplayMetrics();
		 
		 if(metrics.densityDpi<200)
			 return -25;
		 else if(metrics.densityDpi<240)
			 return -46;
		 else if(metrics.densityDpi<280)
			 return -50;
		 else if(metrics.densityDpi>=280)
			 return -62;
		 else
			 return 0;
	}
	*/
	
	// Replace above function by Thinzar@20140710 - Change_Request-20140701 - START
	public static int getTopMargin(TimeSalesPullToRefresh tryRefreshableView){
		int heightInPixel;
		Resources resources = tryRefreshableView.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
			 
		//Let's say the topMargin is 30dp, convert that to pixel according to screen density
		int assumedHeightInDp = 30;
		heightInPixel = (-1) * Math.round(assumedHeightInDp * (metrics.densityDpi / 160f));	//convert dp to pixel
			
		return heightInPixel;
	}

	private void init() {
		mFlipAnimation = new RotateAnimation(0, -180, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		mFlipAnimation.setInterpolator(new LinearInterpolator());
		mFlipAnimation.setDuration(250);
		mFlipAnimation.setFillAfter(true);

		mReverseFlipAnimation = new RotateAnimation(-180, 0, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		mReverseFlipAnimation.setInterpolator(new LinearInterpolator());
		mReverseFlipAnimation.setDuration(250);
		mReverseFlipAnimation.setFillAfter(true);
		scroller = new Scroller(mContext);

		refreshView = LayoutInflater.from(mContext).inflate(R.layout.pull_to_refresh_timesales_header, null);
		refreshIndicatorView = (ImageView) refreshView.findViewById(R.id.head_arrowImageView);
		//bar = (ProgressBar) refreshView.findViewById(R.id.head_progressBar);
		information = (ImageView)refreshView.findViewById(R.id.information_imageview);
		downTextView = (TextView) refreshView.findViewById(R.id.head_tipsTextView);
	
		refreshTargetTop = getTopMargin(this);
		refreshView.setMinimumHeight(30);
		
		LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		lp.topMargin = refreshTargetTop;
		lp.gravity = Gravity.CENTER;
		addView(refreshView, lp);
		
		isRecord = false;
		mRefreshState = TAP_TO_REFRESH;
		mfooterRefreshState = TAP_TO_REFRESH;
	}

	public boolean onTouchEvent(MotionEvent event) {

		int y = (int) event.getRawY();

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if ( ! isRecord ) {
				lastY = y;
				isRecord = true;
			}
			break;

		case MotionEvent.ACTION_MOVE:
			int m = y - lastY;
			if(-10<m && m <10){
				return false;
			}
			
			doMovement(m);
			lastY = y;

			break;

		case MotionEvent.ACTION_UP:
			fling();
			mfooterView.setVisibility(GONE);

			isRecord = false;
			break;
		}
		return true;
	}

	private void fling() {
		if(nowpull == 0 && mRefreshState != REFRESHING) {
			LinearLayout.LayoutParams lp = (LayoutParams) refreshView
					.getLayoutParams();
			if(lp.topMargin > 0) {
				refresh();
				
			} else{
				returnInitState();
			}
		}else if(nowpull == 1 && mfooterRefreshState != REFRESHING) {

			if (refreshFooter >= 40	&& mfooterRefreshState == RELEASE_TO_REFRESH) {
				mfooterRefreshState = REFRESHING;
				FooterPrepareForRefresh(); 
				onRefresh();
			}else {
				if(refreshFooter>=0)
					resetFooterPadding();
				else{
					resetFooterPadding();
					mfooterRefreshState = TAP_TO_REFRESH;
					TryPullToRefreshScrollView.ScrollToPoint(sv, sv.getChildAt(0),-refreshFooter);
				}
			}
		}
	}

	public void onRefresh() {
		if(refreshListener != null) {
			refreshListener.onRefresh();
		}
	}

	private void returnInitState() {
		// TODO Auto-generated method stub
		mRefreshState = TAP_TO_REFRESH;
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
		int i = lp.topMargin;
		scroller.startScroll(0, i, 0, refreshTargetTop);

		invalidate();
	}

	private void refresh() {
		// TODO Auto-generated method stub
		mRefreshState = REFRESHING;
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
		int i = lp.topMargin;
		refreshIndicatorView.setVisibility(View.GONE);
		refreshIndicatorView.setImageDrawable(null);
		information.setVisibility(View.VISIBLE);
		downTextView.setText("Refreshing...");
		scroller.startScroll(0, i, 0, (0 - i)+10);
		invalidate();


		if (refreshListener != null) {
			refreshListener.onRefresh();
		}
	}

	private void resetFooterPadding() {
		LayoutParams rf = (LayoutParams) mfooterView.getLayoutParams();
		rf.bottomMargin = rf.bottomMargin/2;
		
		mfooterView.setVisibility(GONE);
		mfooterView.setLayoutParams(rf);
		TryPullToRefreshScrollView.ScrollToPoint(sv, sv.getChildAt(0),0);
	}

	public void FooterPrepareForRefresh() {
		resetFooterPadding();
		refreshIndicatorView2.setVisibility(View.GONE);
		refreshIndicatorView2.setImageDrawable(null);
		bar2.setVisibility(View.VISIBLE);
		mfooterViewText.setText("Loading...");
		mfooterRefreshState = REFRESHING;
	}

	@Override
	public void computeScroll() {
		// TODO Auto-generated method stub
		if (scroller.computeScrollOffset()) {
			int i = this.scroller.getCurrY();
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
			int k = Math.max(i, refreshTargetTop);
			lp.topMargin = k;
			this.refreshView.setLayoutParams(lp);
			this.refreshView.invalidate();
			invalidate();
		}
	}
	
	public void doMovement(int moveY) {
		if(isvalidate){
			returnInitState();
			isvalidate = false;
		}
		
		LinearLayout.LayoutParams lp = (LayoutParams) refreshView.getLayoutParams();
		if(sv.getScrollY() == 0 && moveY > 0 && refreshFooter<=0){
			nowpull = 0;
		}
		if(sv.getChildAt(0).getMeasuredHeight() - sv.getMeasuredHeight() == sv.getScrollY() && moveY < 0){
			nowpull = 1;
		}
		
		if (nowpull == 0 && mRefreshState != REFRESHING && AppConstants.MAXRECORDNO > AppConstants.FINAL_LASTRECORDNO) {
			float f1 = lp.topMargin;
			float f2 = f1 + moveY * 0.3F;
			int i = (int) f2;
			lp.topMargin = i;
			if(lp.topMargin>=refreshTargetTop)
				refreshView.setLayoutParams(lp);
			
			refreshView.invalidate();
			invalidate();
			
			downTextView.setVisibility(View.VISIBLE);

			refreshIndicatorView.setVisibility(View.VISIBLE);
			information.setVisibility(View.GONE);
			
			if (lp.topMargin > 20 && mRefreshState != RELEASE_TO_REFRESH){
				downTextView.setText("Release to refresh... ");
				
				refreshIndicatorView.clearAnimation();
				refreshIndicatorView.startAnimation(mFlipAnimation);
				mRefreshState = RELEASE_TO_REFRESH;
			}else if (lp.topMargin <= 20 && mRefreshState != PULL_TO_REFRESH){
				downTextView.setText("Pull down to get latest records.");
				if (mRefreshState != TAP_TO_REFRESH) {
					refreshIndicatorView.clearAnimation();
					refreshIndicatorView.startAnimation(mReverseFlipAnimation);
				}
				mRefreshState = PULL_TO_REFRESH;
			}
		}else if (nowpull == 1 && mfooterRefreshState != REFRESHING && AppConstants.MINRECORDNO > 0) {
			
			//LayoutParams svlp = (LayoutParams) sv.getLayoutParams();
			LayoutParams rf = (LayoutParams)mfooterView.getLayoutParams();
			
			mfooterView.setVisibility(VISIBLE);
			rf.bottomMargin += (int)(-moveY*0.2F);
			refreshFooter=rf.bottomMargin;
			mfooterView.setLayoutParams(rf);
			TryPullToRefreshScrollView.ScrollToPoint(sv, sv.getChildAt(0),0);
			
			refreshIndicatorView.setVisibility(View.VISIBLE);
			
			if (rf.bottomMargin >= 50 && mfooterRefreshState != RELEASE_TO_REFRESH){
				refreshIndicatorView2.clearAnimation();
				refreshIndicatorView2.startAnimation(mFlipAnimation);
				
				mfooterViewText.setText("Release to refresh... ");
				
				mfooterRefreshState = RELEASE_TO_REFRESH;
				
			}else if (rf.bottomMargin < 50 && mfooterRefreshState != PULL_TO_REFRESH){
				refreshIndicatorView2.clearAnimation();
				refreshIndicatorView2.startAnimation(mReverseFlipAnimation);
				mfooterViewText.setText("Pull up to refresh");
				mfooterRefreshState = PULL_TO_REFRESH;
			}
		}
	}

	public void setRefreshListener(RefreshListener listener) {
		this.refreshListener = listener;
	}
	
	public void validate(){
		if (mRefreshState != TAP_TO_REFRESH) {
			mRefreshState = TAP_TO_REFRESH; 
			isvalidate =true;
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
			int i = lp.topMargin;
			refreshIndicatorView.setImageResource(R.drawable.pull_down);
			refreshIndicatorView.clearAnimation();
			information.setVisibility(View.GONE);
		}
	}
	
	public void finishRefresh() {
		if (mRefreshState != TAP_TO_REFRESH) {
			mRefreshState = TAP_TO_REFRESH; 
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) this.refreshView.getLayoutParams();
			int i = lp.topMargin;
			refreshIndicatorView.setImageResource(R.drawable.pull_down);
			refreshIndicatorView.clearAnimation();
			information.setVisibility(View.GONE);
			
			scroller.startScroll(0, 0, 0, refreshTargetTop*2);
			invalidate();
		}
		if (mfooterRefreshState != TAP_TO_REFRESH) {
			resetFooter();
		}
	}

	public void resetFooter() {
		bar2.setVisibility(View.GONE);
		refreshIndicatorView2.setVisibility(View.VISIBLE);
		mfooterRefreshState = TAP_TO_REFRESH;
		resetFooterPadding();
		refreshIndicatorView2.setImageResource(R.drawable.pull_up);
		refreshIndicatorView2.clearAnimation();
		TryPullToRefreshScrollView.ScrollToInitialPoint(sv);
		LayoutParams rf = (LayoutParams)mfooterView.getLayoutParams();
		rf.bottomMargin = 0;
		mfooterView.setLayoutParams(rf);
	}

	public void HideFooter() {
		LayoutParams mfvlp = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		mfvlp.bottomMargin = 0;
		mfooterView.setLayoutParams(mfvlp);
		mfooterRefreshState = TAP_TO_REFRESH;
	}
	
	/*
	 * @see
	 * android.view.ViewGroup#onInterceptTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onInterceptTouchEvent(MotionEvent e) {
		// TODO Auto-generated method stub
		int action = e.getAction();
		int y = (int) e.getRawY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			if (isRecord == false) {
				lastY = y;
				isRecord = true;
			}
			break;

		case MotionEvent.ACTION_MOVE:
			int m = y - lastY;
			if (canScroll(m)) {
				return true;
			}
			break;
		case MotionEvent.ACTION_UP:
	
			isRecord = false;
			break;

		case MotionEvent.ACTION_CANCEL:

			break;
		}
		return false;
	} 

	private boolean canScroll(int diff) {
		// TODO Auto-generated method stub
		LinearLayout.LayoutParams lp = (LayoutParams) refreshView.getLayoutParams();
		View childView;
		childView = this.getChildAt(1);
	
		if(-10<diff && diff <10){
			return false;
		}
	
		if(isvalidate){
			returnInitState();
			isvalidate =false;
		}

		if (mRefreshState == REFRESHING || mfooterRefreshState == REFRESHING) {
			return true;
		}
		
		if (getChildCount() > 1) {
			childView = this.getChildAt(1);
			if(childView instanceof ScrollView){
				if(((ScrollView) childView).getScrollY() == 0 && diff > 0){
					nowpull = 0;
					return true;
				}else if(((ScrollView) childView).getChildAt(0).getMeasuredHeight() - sv.getMeasuredHeight() == sv.getScrollY()){
					if(AppConstants.MINRECORDNO == 0)
						return false;
					nowpull = 1;
					return true;
				}else{
					return false;
				}
			}
		}
		return false;
	}

	public interface RefreshListener {
		public void onRefresh();
	}
}

