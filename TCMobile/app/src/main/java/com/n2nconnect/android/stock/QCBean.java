package com.n2nconnect.android.stock;

public class QCBean {
	private String strRawParam		= null;
	private String strUserParam		= null;
	private String strUserParamKey	= null;
	private double dblAliveTimeOut	= 0.00;
	
	
	public String getStrUserParam() {
		return strUserParam;
	}
	public void setStrUserParam(String strUserParam) {
		this.strUserParam = strUserParam;
	}
	public String getStrUserParamKey() {
		return strUserParamKey;
	}
	public void setStrUserParamKey(String strUserParamKey) {
		this.strUserParamKey = strUserParamKey;
	}
	public double getDblAliveTimeOut() {
		return dblAliveTimeOut;
	}
	public void setDblAliveTimeOut(double dblAliveTimeOut) {
		this.dblAliveTimeOut = dblAliveTimeOut;
	}
	public String getStrRawParam() {
		return strRawParam;
	}
	public void setStrRawParam(String strUnknown) {
		this.strRawParam = strUnknown;
	}
	
	// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12 - START
	public void flushBean(){
		strRawParam		= null;
		strUserParam	= null;
		strUserParamKey	= null;
		dblAliveTimeOut	= 0.00;
	}
	// Added by Mary@20120827 - Change_Request-20120719, ReqNo.12 - END
	
	
}
