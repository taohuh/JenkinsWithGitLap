package com.n2nconnect.android.stock;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.QcHttpConnectUtil;

public class RefreshStockThread extends Thread{

	private OnRefreshStockListener registeredListener;
	private String[] symbolCodes;
	private String userParam;
	private int refreshRate;
	private volatile boolean stopRequested; 
	private Thread runThread;
	private boolean mPaused;

	public int getRefreshRate() {
		return refreshRate;
	}

	public void setRefreshRate(int refreshRate) {
		this.refreshRate = refreshRate;
	}

	public OnRefreshStockListener getRegisteredListener() {
		return registeredListener;
	}

	public void setRegisteredListener(OnRefreshStockListener registeredListener) {
		this.registeredListener = registeredListener;
	}

	public String getUserParam() {
		return userParam;
	}

	public void setUserParam(String userParam) {
		this.userParam = userParam;
	}
	
	public interface OnRefreshStockListener {
		public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols);
	}

	public RefreshStockThread(String userParam, String[] symbolCodes, int refreshRate) {
		mPaused = false;
		this.userParam = userParam;
		this.symbolCodes = symbolCodes;
		this.refreshRate = refreshRate;
		this.stopRequested = false;
	}

	public void run() {
		while (!stopRequested) {

			synchronized (this) {
				while (mPaused) {
					try {
						wait();
					} catch (InterruptedException e) {
					}
				}
			}

			try {
				runThread = Thread.currentThread();

				if (refreshRate > 0) {
					long waitTime = refreshRate * 1000;
					Thread.sleep(waitTime);
				} else if (refreshRate == 0) {
					Thread.sleep(AppConstants.DEFAULT_REFRESH_RATE * 1000);
					continue;
				} else {				
					Thread.sleep(AppConstants.DEFAULT_REFRESH_RATE * 1000);
				}

				List<StockSymbol> refreshStocks = null;
				
				/* Mary@20130930 - Fixes_Request-20130711, ReqNo.29
				if (symbolCodes.length != 0) {
				*/
				if(symbolCodes != null && symbolCodes.length != 0){
					/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - START
					Map parameters = new HashMap();
					parameters.put(ParamConstants.USERPARAM_TAG, userParam);
					parameters.put(ParamConstants.SYMBOLCODES_TAG, symbolCodes);

					String response = QcHttpConnectUtil.imageQuote(parameters, CommandConstants.IMAGE_QUOTE_REFRESH);
					*/
					String response = QcHttpConnectUtil.imageQuote(symbolCodes, CommandConstants.IMAGE_QUOTE_REFRESH); // NOTE : no retry for function executed in thread
					// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
					
					if(response!=null){
						refreshStocks = QcMessageParser.parseStockRefresh(response);
					}

				} else {
					refreshStocks = new ArrayList<StockSymbol>();
				}
				
				if(registeredListener != null)
					registeredListener.refreshStockSymbolEvent(refreshStocks);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (FailedAuthenicationException e) {				
				e.printStackTrace();
				try {
				/* Mary@20120724 - Fixes_Request-20120724, ReqNo.2
				 	List<String> data = QcHttpConnectUtil.relogin();
					if(data!=null){
						userParam = data.get(0);
					}
				} catch (FailedAuthenicationException e2) {
				*/
					AppConstants.QCdata	= QcHttpConnectUtil.relogin();
					userParam	= AppConstants.QCdata.getStrUserParam();
				// Mary@20120724 - Fixes_Request-20120724, ReqNo.2 - END
				}catch(Exception e2){
					e2.printStackTrace();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
	}

	public void stopRequest() {
		stopRequested = true;

		if (runThread != null ) {
			runThread.interrupt();
		}
	}

	public void pauseRequest() {
		synchronized (this){
			mPaused = true;
		}
	}

	public void resumeRequest() {
		synchronized (this){
			mPaused = false;
			notify();
		}
	}
	
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - START
	public boolean isPaused(){
		return mPaused;
	}
	// Added by Mary@20130920 - Change_Request-20130724, ReqNo.15 - END

	public String[] getSymbolCodes() {
		return symbolCodes;
	}

	public void setSymbolCodes(String[] symbolCodes) {
		this.symbolCodes = symbolCodes;
	}
}
