package com.n2nconnect.android.stock.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

//import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.n2nconnect.android.stock.AppConstants;
import com.n2nconnect.android.stock.DefinitionConstants;
import com.n2nconnect.android.stock.DoubleLoginThread;
import com.n2nconnect.android.stock.DoubleLoginThread.OnDoubleLoginListener;
import com.n2nconnect.android.stock.ParamConstants;
import com.n2nconnect.android.stock.PreferenceConstants;
import com.n2nconnect.android.stock.RefreshStockThread;
import com.n2nconnect.android.stock.RefreshStockThread.OnRefreshStockListener;
import com.n2nconnect.android.stock.StockApplication;
import com.n2nconnect.android.stock.custom.CustomWindow;
import com.n2nconnect.android.stock.gcm.StockAlertPubKey;
import com.n2nconnect.android.stock.gcm.StockAlertUtil;
import com.n2nconnect.android.stock.model.StockSymbol;
import com.n2nconnect.android.stock.util.SystemUtil;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends CustomWindow implements OnDoubleLoginListener, OnRefreshStockListener {
	
	public static final int[] REFRESH_RATES					= { 0, 5, 30, 60 };
	public static final int ACTION_CHANGE_PASSWORD 			= 0;
	public static final int ACTION_CHANGE_PIN 				= 1;
	public static final int ACTION_FORGOT_PIN 				= 2;
	public static final int ACTION_REFRESH_RATE 			= 3;
	public static final int ACTION_ABOUT 					= 4;
	public static final int ACTION_TERM_SERVICE 			= 5;
	public static final int ACTION_PRIVACY_POLICY 			= 6;
	public static final int ACTION_DISCLAIMER_WARRANTY		= 7;
	// Added by Mary@20120824 - Change_Request-20120719, ReqNo.12
	public static final int ACTION_TRADE_PREFERENCE			= 8;
	// Added by Mary@20121123 - Change_Request-20121106, ReqNo.2
	public static final int ACTION_VIEW_PREFERENCE			= 9;
	// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - START
	public static final int ACTION_SUBSCRIPTION_SERVICES	= 10;
	public static final int ACTION_SUBSCRIPTION_STATUS		= 11;
	// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - END
	
	//Added by Thinzar, Change_Request-20160722, ReqNo.1
	public static final int ACTION_QRCODE_ACTIVATION		= 12;
	
	//Change_Request-20170119, ReqNo.12
	public static final int ACTION_MANAGE_PUSH_NOTI		= 13;

	//Change_Request-20170119, ReqNo.16
	public static final int ACTION_MANAGE_TOUCHID			=14;

	private DoubleLoginThread loginThread;
	// Added by Mary@20120824 - define as constant to have easier maintenance - START
	private static final String STR_PARAM_TITLE				= "title";
	private static final String STR_PARAM_SUBTITLE			= "subtitle";
	private static final String STR_PARAM_ICON_IMAGE		= "iconImage";	
	// Added by Mary@20120824 - define as constant to have easier maintenance - END
	
	private String[] PASSWORD_TITLES;
	private String[] PASSWORD_SUBTITLES;
	private String[] REFRESH_TITLES;
	private String[] REFRESH_SUBTITLES;
	
	private String[] INFO_TITLES;
	private String[] INFO_SUBTITLES;
	// Mary@20120705 - exclude unused titles - END
	
	// Added by Mary@20120824 - Change_Request-20120719, ReqNo.12 - START
	private String[] TRADE_PREFERENCE_TITLES;
	private String[] TRADE_PREFERENCE_SUBTITLES;
	
	private int intLayoutIDSettingListItem			= R.layout.settings_list_item;
	// Added by Mary@20120824 - Change_Request-20120719, ReqNo.12 - END
	
	// Added by Mary@20121123 - Change_Request-20121106, ReqNo.2 - START
	private String[] VIEW_PREFERENCE_TITLES;
	private String[] VIEW_PREFERENCE_SUBTITLES;
	// Added by Mary@20121123 - Change_Request-20121106, ReqNo.2 - END
	
	//Change_Request-20170119, ReqNo.13
	private String[] PUSH_NOTIFICATION_TITLES;
	private String[] PUSH_NOTIFICATION_SUBTITLES;

	//Change_Request-20170119, ReqNo.16
	private String[] TOUCHID_TITLES;
	private String[] TOUCHID_SUBTITLES;
	private LinearLayout llTouchId;


	public void onCreate(Bundle savedInstanceState) {
        // Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
        try{
        	super.onCreate(savedInstanceState);
        	
	        AppConstants.showMenu	= true;
	        
	        //Added by Thinzar@20140709 - Change_Request-20170701 - Req #2 - START
	        int listItemHeightInPixel = Math.round(SystemUtil.convertDpToPixel(58, SettingsActivity.this));	//58 - because the layout height in xml is set to 58dp
	        //Added by Thinzar@20140709 - Change_Request-20170701 - Req #2 - END
	        
	        /*Commented by Thinzar
	        setContentView( AppConstants.noTradeClient || ! AppConstants.brokerConfigBean.enableTrade() ? R.layout.account_settings_old : R.layout.account_settings );
	        */
	        setContentView(R.layout.account_settings_2);
	        
	        loginThread				= ((StockApplication) SettingsActivity.this.getApplication()).getDoubleLoginThread();
	        // Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - START
	        if(loginThread == null){
	        	if(AppConstants.hasLogout){
	        		finish();
	        	}else{
	        		loginThread 	= new DoubleLoginThread( sharedPreferences.getString(PreferenceConstants.USER_NAME, ""), sharedPreferences.getString(PreferenceConstants.ATP_USER_PARAM, "") );
	        		if (AppConstants.brokerConfigBean.isEnableEncryption())
	        			loginThread.setUserParam(AppConstants.userParamInByte);
	        		loginThread.start();
	        		((StockApplication) SettingsActivity.this.getApplication()).setDoubleLoginThread(loginThread);
	        	}
	        }
	        // Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3 - END
			loginThread.setRegisteredListener(this);
	 
	        this.title.setText(getResources().getString(R.string.settings_module_title));
	        /* Mary@20120814 - Change_Request-20120719, ReqNo.4
	        this.icon.setImageResource(R.drawable.menuicon_acc_setting);
	        */
	        this.icon.setImageResource( AppConstants.brokerConfigBean.getIntAccSettingMenuIconBgImg() );
	        
	        //Added by Thinzar, Change_Request-20170119, ReqNo.2 - START 
	    	PASSWORD_TITLES 			= new String[]{ 
	    			getResources().getString(R.string.settings_change_passwd_title),
	    			getResources().getString(R.string.settings_change_pin_title),
	    			getResources().getString(R.string.settings_forgot_pin_title)
	    			};
	    	PASSWORD_SUBTITLES 		= new String[]{ 
	    			getResources().getString(R.string.settings_change_passwd_sub_title),
	    			getResources().getString(R.string.settings_change_pin_sub_title),
	    			getResources().getString(R.string.settings_forgot_pin_sub_title)
	    	};
	    	REFRESH_TITLES 			= new String[]{getResources().getString(R.string.settings_refresh_rate_title)};
	    	REFRESH_SUBTITLES 		= new String[]{getResources().getString(R.string.settings_refresh_rate_sub_title)};

	    	INFO_TITLES 			= new String[]{ 
	    			getResources().getString(R.string.settings_info_about),
	    			getResources().getString(R.string.settings_info_terms), 
	    			getResources().getString(R.string.settings_info_privacy),
	    			getResources().getString(R.string.settings_info_disclaimer),
	    			getResources().getString(R.string.settings_info_subcr_services),
	    			getResources().getString(R.string.settings_info_subcr_status)
	    	};
	    	INFO_SUBTITLES 			= new String[]{ 
	    			getResources().getString(R.string.settings_info_about_sub),
	    			getResources().getString(R.string.settings_info_terms_sub), 
	    			getResources().getString(R.string.settings_info_privacy_sub),
	    			getResources().getString(R.string.settings_info_disclaimer_sub),
	    			getResources().getString(R.string.settings_info_subcr_services_sub),
	    			getResources().getString(R.string.settings_info_subcr_status_sub)
	    	};

	    	TRADE_PREFERENCE_TITLES 	= new String[]{ getResources().getString(R.string.settings_trade_pref_title) };
	    	TRADE_PREFERENCE_SUBTITLES	= new String[]{ getResources().getString(R.string.settings_trade_pref_sub_title) };

	    	VIEW_PREFERENCE_TITLES 		= new String[]{ getResources().getString(R.string.settings_view_pref_title) };
	    	VIEW_PREFERENCE_SUBTITLES	= new String[]{ getResources().getString(R.string.settings_view_pref_sub_title) };
	    	
	    	//Change_Request-20170119, ReqNo.13
	    	PUSH_NOTIFICATION_TITLES	= new String[]{
	    			getResources().getString(R.string.settings_push_noti_register_title)};
	    	PUSH_NOTIFICATION_SUBTITLES	= new String[]{
	    			getResources().getString(R.string.settings_push_noti_register_subtitle)
	    			};
	    	//Added by Thinzar, Change_Request-20170119, ReqNo.2 - END

			//Change_Request-20170119, ReqNo.16
			TOUCHID_TITLES		= new String[]{getResources().getString(R.string.settings_deactivate_touchid_title)};
			TOUCHID_SUBTITLES	= new String[]{getResources().getString(R.string.settings_deactivate_touchid_sub_title)};

	        String[] from			= { STR_PARAM_TITLE, STR_PARAM_SUBTITLE };
	        int[] to 				= { R.id.titleLabel, R.id.subtitleLabel};
	        ListView passwordList 	= (ListView) this.findViewById(R.id.passwordList);
	        
	        List<Map<String, Object>> fillList 	= new ArrayList<Map<String, Object>>();
	        for (int i=0; i<PASSWORD_TITLES.length; i++) {
	        	Map<String, Object> map	= new HashMap<String, Object>();
	        	// Added by Mary@20130116-Fixes_Request-20130108, ReqNo.6 - START
	        	String strPasswordTitle	= PASSWORD_TITLES[i];
	        	if( strPasswordTitle.toUpperCase().endsWith(" PASSWORD") && ( ! AppConstants.brokerConfigBean.isEnablePasswordFeatures() ) )
		        	continue;
	        	else if( strPasswordTitle.toUpperCase().endsWith(" PIN") && ( ! AppConstants.brokerConfigBean.isEnablePinFeatures() || AppConstants.noTradeClient ) )
		        	continue;
	        	// Added by Mary@20130116-Fixes_Request-20130108, ReqNo.6 - END
	        	
	        	map.put(STR_PARAM_TITLE, strPasswordTitle);
	        	map.put(STR_PARAM_SUBTITLE, PASSWORD_SUBTITLES[i]);
	        	fillList.add(map);
	        }
	        
	        // Added by Mary@20130116-Fixes_Request-20130108, ReqNo.6 - START
	        ( (TextView)this.findViewById(R.id.tvPwdPinTitle) ).setVisibility( fillList.size() == 0 ? View.GONE : View.VISIBLE);
	        passwordList.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, ( listItemHeightInPixel * fillList.size() ) ) );
	        // Added by Mary@20130116-Fixes_Request-20130108, ReqNo.6 - END
	        SimpleAdapter adapter	= new SimpleAdapter(this, fillList, intLayoutIDSettingListItem, from, to);
	        passwordList.setAdapter(adapter);
	        passwordList.setOnItemClickListener(new OnItemClickListener() {
	        	public void onItemClick(AdapterView parent, View view, int position, long id) {
	        		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
	        		boolean isValidSelection	= false;
		        	switch(position){
		        		case 0 :
		        			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
		        			isValidSelection = true;
		        			
		        			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_CHANGE_PASSWORD);
		        			break;
		        		case 1 :
		        			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
		        			isValidSelection = true;
		        			
		        			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_CHANGE_PIN);
		        			break;
		        		case 2 :
		        			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
		        			isValidSelection = true;
		        			
		        			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_FORGOT_PIN);
		        			break;
		        	}
		        	
		        	// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
					if(isValidSelection && idleThread != null && ! isShowingIdleAlert)
						idleThread.resetExpiredTime();
					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
				}
	        });
	        
	        //Change_Request-20170119, ReqNo.12 - START
	        LinearLayout llPushNotification	= (LinearLayout)this.findViewById(R.id.ll_push_notification);
	     
	        if (!AppConstants.isAppMultilogin && AppConstants.brokerConfigBean.isEnableStockAlert()) {
	        	llPushNotification.setVisibility(View.VISIBLE);
	        	ListView lvPushNotification		= (ListView)this.findViewById(R.id.lv_push_notification);
	        	
	        	fillList = new ArrayList<Map<String, Object>>();
				for (int i = 0; i < PUSH_NOTIFICATION_TITLES.length; i++) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put(STR_PARAM_TITLE, PUSH_NOTIFICATION_TITLES[i]);
					map.put(STR_PARAM_SUBTITLE, PUSH_NOTIFICATION_SUBTITLES[i]);
					fillList.add(map);
				}

				lvPushNotification.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
						(listItemHeightInPixel * fillList.size())));
				adapter = new SimpleAdapter(this, fillList, intLayoutIDSettingListItem, from, to);
				lvPushNotification.setAdapter(adapter);
				lvPushNotification.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView parent, View view, int position, long id) {

						if (idleThread != null && !isShowingIdleAlert)
							idleThread.resetExpiredTime();
						switch(position){
						case 0:
							SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_MANAGE_PUSH_NOTI);
							break;
						}
					}
				});
	        }else{
	        	llPushNotification.setVisibility(View.GONE);
	        }
	        //Change_Request-20170119, ReqNo.12 - END

			//Change_Request-20170119, ReqNo.16 - START
			boolean isPreLoginValidated = sharedPreferences.getBoolean(PreferenceConstants.IS_PRE_LOGIN_VALIDATED, false);
			llTouchId = (LinearLayout)this.findViewById(R.id.ll_touchid);
			if(AppConstants.brokerConfigBean.isTouchIDEnabled() && isPreLoginValidated){
				ListView lvTouchId = (ListView)this.findViewById(R.id.lv_touchid);
				fillList = new ArrayList<Map<String, Object>>();

				for (int i = 0; i < TOUCHID_TITLES.length; i++) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put(STR_PARAM_TITLE, TOUCHID_TITLES[i]);
					map.put(STR_PARAM_SUBTITLE, TOUCHID_SUBTITLES[i]);
					fillList.add(map);
				}

				lvTouchId.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
						(listItemHeightInPixel * fillList.size())));
				adapter = new SimpleAdapter(this, fillList, intLayoutIDSettingListItem, from, to);
				lvTouchId.setAdapter(adapter);
				lvTouchId.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView parent, View view, int position, long id) {

						if (idleThread != null && !isShowingIdleAlert)
							idleThread.resetExpiredTime();
						switch(position){
							case 0:
								SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_MANAGE_TOUCHID);
								break;
						}
					}
				});
			}else{
				llTouchId.setVisibility(View.GONE);
			}
			//Change_Request-20170119, ReqNo.16 - END
	        
	        String[] iconFrom		= { STR_PARAM_TITLE, STR_PARAM_SUBTITLE, STR_PARAM_ICON_IMAGE };
	        int[] iconTo			= { R.id.titleLabel, R.id.subtitleLabel, R.id.iconImage };
	        ListView refreshList	= (ListView) this.findViewById(R.id.refreshList);
	        fillList 				= new ArrayList<Map<String, Object>>();
	        for (int i=0; i<REFRESH_TITLES.length; i++) {
	        	Map<String, Object> map	= new HashMap<String, Object>();
	        	map.put(STR_PARAM_TITLE, REFRESH_TITLES[i]);
	        	map.put(STR_PARAM_SUBTITLE, REFRESH_SUBTITLES[i]);
	        	map.put(STR_PARAM_ICON_IMAGE, R.drawable.dropdown_icon);
	        	fillList.add(map);
	        }
	        
	        // Added by Mary@20121128 - Fixes_Request-20121102, ReqNo.6
	        refreshList.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, ( listItemHeightInPixel * fillList.size() ) ) );
	        adapter					= new SimpleAdapter(this, fillList, intLayoutIDSettingListItem, iconFrom, iconTo);
	        refreshList.setAdapter(adapter);
	        refreshList.setOnItemClickListener(new OnItemClickListener() {
	        	public void onItemClick(AdapterView parent, View view, int position, long id) {
	        		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
	    			if(idleThread != null && ! isShowingIdleAlert)
	    				idleThread.resetExpiredTime();
	    			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
	    			
	        		SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_REFRESH_RATE);
				}
	        });
	        
	        ListView infoList		= (ListView) this.findViewById(R.id.infoList);
	        fillList 				= new ArrayList<Map<String, Object>>();
	        for (int i=0; i<INFO_TITLES.length; i++) {
	        	
	        	boolean showOption	= true;
	        	switch(i){
	        		case 1 :
	        			if(AppConstants.brokerConfigBean.getALTermsOfService().size() == 0
	        				&& ( AppConstants.brokerConfigBean.getStrTermsOfServiceURL() == null 
	    							|| (AppConstants.brokerConfigBean.getStrTermsOfServiceURL() != null && AppConstants.brokerConfigBean.getStrTermsOfServiceURL().trim().length() == 0) 
	    						)
	        			)
	        				showOption	= false;
	        			break;
	        		case 2 :
	        			if(AppConstants.brokerConfigBean.getALPrivacyPolicy().size() == 0
	        				&& ( AppConstants.brokerConfigBean.getStrPrivacyPolicyURL() == null 
	    							|| (AppConstants.brokerConfigBean.getStrPrivacyPolicyURL() != null && AppConstants.brokerConfigBean.getStrPrivacyPolicyURL().trim().length() == 0) 
	    						)
	        			)
	        				showOption	= false;
	        			break;
	        		case 3 :
	        			if(AppConstants.brokerConfigBean.getALDisclaimerOfWarranty().size() == 0
	        				&& ( AppConstants.brokerConfigBean.getStrDisclaimerOfWarrantyURL() == null 
	    							|| (AppConstants.brokerConfigBean.getStrDisclaimerOfWarrantyURL() != null && AppConstants.brokerConfigBean.getStrDisclaimerOfWarrantyURL().trim().length() == 0) 
	    						)
	        			)
	        				showOption	= false;
	        			break;
	        		// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - START
	        		case 4 :
	        			if(AppConstants.brokerConfigBean.getStrLMSSubscriptionUrl() == null
	        				|| ( AppConstants.brokerConfigBean.getStrLMSSubscriptionUrl() != null && AppConstants.brokerConfigBean.getStrLMSSubscriptionUrl().trim().length() == 0 )
	        			)
	        				showOption = false;
	        			break;
	        		case 5 :
	        			if(AppConstants.brokerConfigBean.getStrLMSSubscriptionStatusUrl() == null
	            				|| ( AppConstants.brokerConfigBean.getStrLMSSubscriptionStatusUrl() != null && AppConstants.brokerConfigBean.getStrLMSSubscriptionStatusUrl().trim().length() == 0 )
	        			)
	        				showOption = false;
	        			break;
	        		// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - END
	        	}
	        	
	        	if( ! showOption )
	        	// Mary@20130606 - Change_Request-20130424, ReqNo.8 - END
	        		/* Mary@20130729 - Change_Request-20130724, ReqNo.2
	        		break;
	        		*/
	        		continue;
	        	// Added by Mary@20121102 - Fixes_Request-20121102, ReqNo.3 - END
	        	
	        	Map<String, Object> map	= new HashMap<String, Object>();
	        	map.put(STR_PARAM_TITLE,INFO_TITLES[i]);
	        	map.put(STR_PARAM_SUBTITLE, INFO_SUBTITLES[i]);
	        	fillList.add(map);
	        }
	        adapter								= new SimpleAdapter(this, fillList, intLayoutIDSettingListItem, from, to);
	        
	        // Added by Mary@20130729 - Change_Request-20130724, ReqNo.2 - START
	        final List<Map<String, Object>> finalList	= new ArrayList<Map<String, Object>>();
	        finalList.addAll(fillList);
	        // Added by Mary@20130729 - Change_Request-20130724, ReqNo.2 - END
	        		
	        // Added by Mary@20121128 - Fixes_Request-20121102, ReqNo.6
	        infoList.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, ( listItemHeightInPixel * fillList.size() ) ) );
	        infoList.setAdapter(adapter);
	        infoList.setOnItemClickListener(new OnItemClickListener() {
	        	public void onItemClick(AdapterView parent, View view, int position, long id) {
	        		// Added by Mary@20130729 - Change_Request-20130724, ReqNo.2
	        		try{
		        		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5
		        		boolean isValidSelection	= false;
		        		
	        			if( INFO_TITLES[0].equalsIgnoreCase( String.valueOf( finalList.get(position).get(STR_PARAM_TITLE) ) ) ){
	                   		isValidSelection	= true;
	                		
	        				SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_ABOUT);
	    				}else if( INFO_TITLES[1].equalsIgnoreCase( String.valueOf( finalList.get(position).get(STR_PARAM_TITLE) ) ) ){
	                		isValidSelection	= true;
	                		
		        			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_TERM_SERVICE);
	    				}else if( INFO_TITLES[2].equalsIgnoreCase( String.valueOf( finalList.get(position).get(STR_PARAM_TITLE) ) ) ){
	                		isValidSelection	= true;
	                		
		        			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_PRIVACY_POLICY);
	    				}else if( INFO_TITLES[3].equalsIgnoreCase( String.valueOf( finalList.get(position).get(STR_PARAM_TITLE) ) ) ){
	                		isValidSelection	= true;
	                		
		        			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_DISCLAIMER_WARRANTY);
	    				}else if( INFO_TITLES[4].equalsIgnoreCase( String.valueOf( finalList.get(position).get(STR_PARAM_TITLE) ) ) ){
	                		isValidSelection	= true;
	                		
		        			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_SUBSCRIPTION_SERVICES);
	    				}else if( INFO_TITLES[5].equalsIgnoreCase( String.valueOf( finalList.get(position).get(STR_PARAM_TITLE) ) ) ){
	                		isValidSelection	= true;
	                		
		        			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_SUBSCRIPTION_STATUS);
	    				}
	        			// Mary@20130729 - Change_Request-20130724, ReqNo.2 - END
		        		
		        		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		    			if(isValidSelection && idleThread != null && ! isShowingIdleAlert)
		    				idleThread.resetExpiredTime();
		    			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
		    		// Added by Mary@20130729 - Change_Request-20130724, ReqNo.2 - START
	        		}catch(Exception e){ 
	        			e.printStackTrace();
	        		}
	        		// Added by Mary@20130729 - Change_Request-20130724, ReqNo.2 - END
				}
	        });
	        
	        LinearLayout llTradePreference	= (LinearLayout) this.findViewById(R.id.ll_trade_preference);
	        
	        if( ! AppConstants.noTradeClient && AppConstants.brokerConfigBean.enableTrade() ){
	        	llTradePreference.setVisibility(View.VISIBLE);
	        	ListView tradePreferenceList= (ListView) this.findViewById(R.id.tradePreferenceList);
		        fillList 					= new ArrayList<Map<String, Object>>();
		        for (int i=0; i<TRADE_PREFERENCE_TITLES.length; i++) {
		        	Map<String, Object> map	= new HashMap<String, Object>();
		        	map.put( STR_PARAM_TITLE,TRADE_PREFERENCE_TITLES[i] );
		        	map.put( STR_PARAM_SUBTITLE, TRADE_PREFERENCE_SUBTITLES[i] );
		        	fillList.add(map);
		        }
		        // Added by Mary@20121128 - Fixes_Request-20121102, ReqNo.6
		        tradePreferenceList.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, ( listItemHeightInPixel * fillList.size() ) ) );
		        adapter						= new SimpleAdapter(this, fillList, intLayoutIDSettingListItem, from, to);
		        tradePreferenceList.setAdapter(adapter);
		        tradePreferenceList.setOnItemClickListener(new OnItemClickListener(){
		        	public void onItemClick(AdapterView parent, View view, int position, long id){
		        		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		    			if(idleThread != null && ! isShowingIdleAlert)
		    				idleThread.resetExpiredTime();
		    			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
		    			
		    			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_TRADE_PREFERENCE);
					}
		        });
	        }else{
	        	llTradePreference.setVisibility(View.GONE);
	        }
		    // Added by Mary@20120824 - Change_Request-20120719, ReqNo.12 - END
	        
	        // Added by Mary@20121123 - Change_Request-20121106, ReqNo.2 - START
	        
	        if(AppConstants.hasEquityExchange){	//If checking added by Thinzarr@20140903 - Fixes_Request-20140820, ReqNo.14
		        ListView viewPreferenceList= (ListView) this.findViewById(R.id.viewPreferenceList);
		        fillList 					= new ArrayList<Map<String, Object>>();
		        for (int i=0; i<VIEW_PREFERENCE_TITLES.length; i++) {
		        	Map<String, Object> map	= new HashMap<String, Object>();
		        	map.put( STR_PARAM_TITLE, VIEW_PREFERENCE_TITLES[i] );
		        	map.put( STR_PARAM_SUBTITLE, VIEW_PREFERENCE_SUBTITLES[i] );
		        	fillList.add(map);
		        }
		        // Added by Mary@20121128 - Fixes_Request-20121102, ReqNo.6
		        viewPreferenceList.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, ( listItemHeightInPixel * fillList.size() ) ) );
		        adapter						= new SimpleAdapter(this, fillList, intLayoutIDSettingListItem, from, to);
		        viewPreferenceList.setAdapter(adapter);
		        viewPreferenceList.setOnItemClickListener(new OnItemClickListener(){
		        	public void onItemClick(AdapterView parent, View view, int position, long id){
		        		// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
		    			if(idleThread != null && ! isShowingIdleAlert)
		    				idleThread.resetExpiredTime();
		    			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
		    			
		    			SettingsActivity.this.handleListClickEvent(SettingsActivity.ACTION_VIEW_PREFERENCE);
					}
		        });
	        } else {
	        	//added by Thinzarr@20140903 - Fixes_Request-20140820, ReqNo.14 - START
	        	LinearLayout ll_viewpref = (LinearLayout)findViewById(R.id.ll_view_preference);
	        	ll_viewpref.setVisibility(View.GONE);
	        	//added by Thinzarr@20140903 - Fixes_Request-20140820, ReqNo.14 - END
	        }
	        // Added by Mary@20121123 - Change_Request-20121106, ReqNo.2 - END
	        
	    // Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
        }catch(Exception e){
        	e.printStackTrace();
        }
     // Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
    }
    
    // Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - START
    @Override
    public void onResume(){
    	// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15
    	try{
    		super.onResume();
    		
			currContext	= SettingsActivity.this;
			if( idleThread != null && idleThread.isPaused() )
				idleThread.resumeRequest(false);
			
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
			if( loginThread != null && loginThread.isPaused() ){
				loginThread.setRegisteredListener(SettingsActivity.this);
				loginThread.resumeRequest();
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
		// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
    }
	// Added by Mary@20130531 - Change_Request-20130424, ReqNo.5 - END
    
    // Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - START
    @Override
	public void onPause(){
		try{
			super.onPause();
			
			if(loginThread != null)
				loginThread.pauseRequest();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Added by Mary@20130923 - Change_Request-20130724, ReqNo.15 - END
    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
			if(idleThread != null && ! isShowingIdleAlert)
				idleThread.resetExpiredTime();
			// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
						
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
			else
				finish();

			return true;
		}
		return super.onKeyDown(keyCode, event); 
	}
    
    private void handleListClickEvent(int actionType) {
    	// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6
    	try{
	    	Intent intent = new Intent();
	    	
	    	switch (actionType) {
	    		case ACTION_CHANGE_PASSWORD:
	    			intent.putExtra(ParamConstants.PASSWORD_TYPE_TAG, AppConstants.CHANGE_PASSWORD_TYPE);
	    			intent.setClass(SettingsActivity.this, ChangePasswordActivity.class);
					startActivity(intent);
	    			break;
	    		case ACTION_CHANGE_PIN:
	    			intent.putExtra(ParamConstants.PASSWORD_TYPE_TAG, AppConstants.CHANGE_PIN_TYPE);
	    			intent.setClass(SettingsActivity.this, ChangePasswordActivity.class);
					startActivity(intent);
	    			break;
	    		case ACTION_FORGOT_PIN:
	    			intent.setClass(SettingsActivity.this, ForgetPinActivity.class);
	    			startActivity(intent);
	    			break;
	    		case ACTION_REFRESH_RATE:
	    			final String[] items		= {
	    					getResources().getString(R.string.settings_refresh_rate_no),
	    					getResources().getString(R.string.settings_refresh_rate_5sec),
	    					getResources().getString(R.string.settings_refresh_rate_30sec),
	    					getResources().getString(R.string.settings_refresh_rate_60sec)
	    			};
	
	    			AlertDialog.Builder builder	= new AlertDialog.Builder(this);
	    			builder.setTitle(getResources().getString(R.string.settings_refresh_rate_dialog_title));
	    			
	    			int rate 					= sharedPreferences.getInt(PreferenceConstants.REFRESH_RATE, AppConstants.DEFAULT_REFRESH_RATE);
	    			int index 					= -1;
	    			for (int i=0; i<REFRESH_RATES.length; i++) {
	    				if (rate == REFRESH_RATES[i]) {
	    					index = i;
	    					break;
	    				}
	    			}
	    			
	    			builder.setSingleChoiceItems(items, index, new DialogInterface.OnClickListener() {
	    			    public void onClick(DialogInterface dialog, int item) {
	    			    	// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - START
	    					if(idleThread != null && ! isShowingIdleAlert)
	    						idleThread.resetExpiredTime();
	    					// Added by Mary@20130605 - Change_Request-20130424, ReqNo.5 - END
	    					
	    			    	SharedPreferences.Editor editor	= sharedPreferences.edit();
	    		        	editor.putInt( PreferenceConstants.REFRESH_RATE, REFRESH_RATES[item] );
	    		        	editor.commit();
	    		        	
	    		        	dialog.dismiss();
	    		        	
	    		        	RefreshStockThread refreshThread= ( (StockApplication) SettingsActivity.this.getApplication() ).getRefreshThread();
	    		        	if(refreshThread != null)
	    		        		refreshThread.setRefreshRate( REFRESH_RATES[item] );
	    			    }
	    			});
	    			
	    			AlertDialog alert			= builder.create();
	    			// Added by Mary@20130618 - Fixes_Request-20130523, ReqNo.6
					if( ! SettingsActivity.this.isFinishing() )
						alert.show();
	    			break;
	    		case ACTION_ABOUT:
	    			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - START
	    			if(AppConstants.brokerConfigBean.getStrAboutURL() != null && AppConstants.brokerConfigBean.getStrAboutURL().trim().length() > 0){
	    				openInBrowser( AppConstants.brokerConfigBean.getStrAboutURL() );
	    			}else{
	    			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - END
		    			intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_ABOUT_TYPE);
		    			intent.setClass(SettingsActivity.this, ShowInfoActivity.class);
						startActivity(intent);
	    			}
	    			break;
	    		case ACTION_TERM_SERVICE:
	    			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - START
	    			if(AppConstants.brokerConfigBean.getStrTermsOfServiceURL() != null && AppConstants.brokerConfigBean.getStrTermsOfServiceURL().trim().length() > 0){
	    				openInBrowser( AppConstants.brokerConfigBean.getStrTermsOfServiceURL() );
	    			}else{
	    			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - END
	    				/* Mary@20130606 - Change_Request-20130424, ReqNo.8
		    			intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_TERM_TYPE);
		    			*/
	    				intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_SERVICE_TYPE);
		    			intent.setClass(SettingsActivity.this, ShowInfoActivity.class);
						startActivity(intent);
	    			}
	    			break;
	    		case ACTION_PRIVACY_POLICY:
	    			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - START
	    			if(AppConstants.brokerConfigBean.getStrPrivacyPolicyURL() != null && AppConstants.brokerConfigBean.getStrPrivacyPolicyURL().trim().length() > 0){
	    				openInBrowser( AppConstants.brokerConfigBean.getStrPrivacyPolicyURL() );
	    			}else{
	    			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - END
		    			intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_PRIVACY_TYPE);
		    			intent.setClass(SettingsActivity.this, ShowInfoActivity.class);
						startActivity(intent);
	    			}
	    			break;
	    		case ACTION_DISCLAIMER_WARRANTY:
	    			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - START
	    			if(AppConstants.brokerConfigBean.getStrDisclaimerOfWarrantyURL() != null && AppConstants.brokerConfigBean.getStrDisclaimerOfWarrantyURL().trim().length() > 0){
	    				openInBrowser( AppConstants.brokerConfigBean.getStrDisclaimerOfWarrantyURL() );
	    			}else{
	    			// Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - END
		    			intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_WARRANTY_TYPE);
		    			intent.setClass(SettingsActivity.this, ShowInfoActivity.class);
						startActivity(intent);
	    			}
	    			break;
	   			// Added by Mary@20120824 - Change_Request-20120719, ReqNo.12 - START
	    		case ACTION_TRADE_PREFERENCE :
	    			intent.setClass(SettingsActivity.this, TradePreferenceActivity.class);
					startActivity(intent);
	    			break;
	   			// Added by Mary@20120824 - Change_Request-20120719, ReqNo.12 - END
				// Added by Mary@20121123 - Change_Request-20121106, ReqNo.2 - START
	    		case ACTION_VIEW_PREFERENCE :
	    			intent.setClass(SettingsActivity.this, ViewPreferenceActivity.class);
					startActivity(intent);
	    			break;
	   			// Added by Mary@20121123 - Change_Request-20121106, ReqNo.2 - END
	    		// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - START
	    		case ACTION_SUBSCRIPTION_SERVICES:
	    			intent.putExtra("PreviousClass", "SettingsActivity");
	    			intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_SUBSCRIBE_SERVICE_TYPE);
	    			intent.setClass(SettingsActivity.this, ShowInfoActivity.class);
					startActivity(intent);
	    			break;
	    		case ACTION_SUBSCRIPTION_STATUS:
	    			intent.putExtra(ParamConstants.INFO_TYPE_TAG, AppConstants.INFO_SUBSCRIBE_STATUS_TYPE);
	    			intent.setClass(SettingsActivity.this, ShowInfoActivity.class);
					startActivity(intent);
	    			break;
	    		// Added by Mary@20130728 - Change_Request-20130724, ReqNo.2 - END
	    		
	    		//Change_Request-20170119, ReqNo13
	    		case ACTION_MANAGE_PUSH_NOTI:
	    			
	    			Intent pushNotiSettingsIntent = new Intent();
	    			pushNotiSettingsIntent.setClass(SettingsActivity.this, SettingsPushNotiActivity.class);
	    			startActivity(pushNotiSettingsIntent);
	    			break;

				//Change_Request-20170119, ReqNo.16
				case ACTION_MANAGE_TOUCHID:
					AlertDialog.Builder touchIdAlertBuilder = new AlertDialog.Builder(SettingsActivity.this);
					touchIdAlertBuilder.setMessage(getResources().getString(R.string.deactivate_touchid_confirmation))
							.setPositiveButton(getResources().getString(R.string.dialog_button_yes), new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									//TODO: clear sharedPref, hide menu
									SharedPreferences.Editor editor	= sharedPreferences.edit();
									editor.remove(PreferenceConstants.IS_PRE_LOGIN_VALIDATED);
									editor.remove(PreferenceConstants.KEY_PASSWORD);
									editor.remove(PreferenceConstants.KEY_PASSWORD_IV);
									editor.remove(PreferenceConstants.KEY_USERNAME);
									editor.commit();

									llTouchId.setVisibility(View.GONE);
									Toast.makeText(SettingsActivity.this, getResources().getString(R.string.deactivate_touchid_success_msg), Toast.LENGTH_LONG).show();
								}
							})
							.setNegativeButton("No", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int arg1) {
									dialog.dismiss();
								}
							});

					AlertDialog touchIdAlert		= touchIdAlertBuilder.create();
					if( ! SettingsActivity.this.isFinishing() )
						touchIdAlert.show();

					break;
	    	}
    	// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - START
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	// Added by Mary@20130701 - Fixes_Request-20130523, ReqNo.6 - END
    }
    
    // Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - START
    private void openInBrowser(String strURL){
    	if (!strURL.toLowerCase().startsWith("http://") && !strURL.toLowerCase().startsWith("https://"))
    		strURL = "http://" + strURL;
    	
    	Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse(strURL) );
    	startActivity(browserIntent);
    }
    // Added by Mary@20130606 - Change_Request-20130424, ReqNo.8 - END
    
  	@Override
	protected void onDestroy() {
  		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23
  		try{
			super.onDestroy();
			
			if( this.mainMenu.isShowing() )
				this.mainMenu.hide();
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - START
		}catch(Exception e){
			e.printStackTrace();
		}
		// Added by Mary@20130924 - Fixes_Request-20130711, ReqNo.23 - END
	}

	@Override
	public void DoubleLoginEvent(List<String> response) {
		Message message	= handler3.obtainMessage();
		message.obj		= response;
		handler3.sendMessage(message);
	}
	
	final Handler handler3 = new Handler(){
		public void handleMessage(Message message){
			List<String> response = (List<String>) message.obj;
			/* Mary@20130606 - Fixes_Request-20130523, ReqNo.7
			if( response.get(2).equals("1102") ){
			*/
			/* Mary@20130717 - Fixes_Request-20130711, ReqNo.4
			if(response != null && response.size() > 2 && response.get(2).equals("1102") ){
			*/
			if(response != null && response.size() > 2 && !response.get(2).equals("0") ){
				if( SettingsActivity.this.mainMenu.isShowing() )
					SettingsActivity.this.mainMenu.hide();
				
				// Added by Mary@20130702 - GooglePlayReport-20130702, CrashNo.3
				if(loginThread != null)
					loginThread.stopRequest();
				DefinitionConstants.stopAllThread();
				if( ! isFinishing() )
					SettingsActivity.this.doubleLoginMessage( (String)response.get(1) );
			}
		}
	};

	@Override
	public void refreshStockSymbolEvent(List<StockSymbol> changedSymbols) {
	
	}
}