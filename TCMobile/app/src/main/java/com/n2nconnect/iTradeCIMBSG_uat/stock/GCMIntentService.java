package com.n2nconnect.iTradeCIMBSG_uat.stock;

import java.util.Timer;
import java.util.TimerTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.n2nconnect.android.stock.activity.WidgetDialogActivity;
import com.n2nconnect.iTradeCIMBSG_uat.stock.R;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GCMIntentService extends IntentService {
	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	public GCMIntentService() {
		super("GcmIntentService");
	}
	public static final String TAG = "GCM TcMobile";

	@Override
	protected void onHandleIntent(Intent intent) {
		if(intent != null){
			Bundle extras = intent.getExtras();
			GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
	
			// The getMessageType() intent parameter must be the intent you received
			// in your BroadcastReceiver.
			String messageType = gcm.getMessageType(intent);
	
			if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
				/*
				 * Filter messages based on message type. Since it is likely that GCM will be
				 * extended in the future with new message types, just ignore any message types you're
				 * not interested in, or that you don't recognize.
				 */
				if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
					//  sendNotification("Send error: " + extras.toString());
				} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
					//   sendNotification("Deleted messages on server: " + extras.toString());
					// If it's a regular GCM message, do some work.
				} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
					// This loop represents the service doing some work.
					for (int i = 0; i < 5; i++) {
						Log.i(TAG, "Working... " + (i + 1)
								+ "/5 @ " + SystemClock.elapsedRealtime());
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
						}
					}
					Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
					// Post notification of received message.
					sendNotification( extras);
					Log.i(TAG, "Received: " + extras.toString());
				}
			}
			// Release the wake lock provided by the WakefulBroadcastReceiver.
			GcmBroadcastReceiver.completeWakefulIntent(intent);
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		System.out.println("test");
		return START_STICKY;
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with a GCM message.

	private void sendNotification(Bundle extra){

		String message	= "";
		String title	= "";
		String msg_id	= "";
		String appName;

		if(extra!=null){
			if(extra.containsKey("gcm.notification.text")){
				message = extra.getString("gcm.notification.text");
			}
			
			if(extra.containsKey("gcm.notification.title")){
				title = extra.getString("gcm.notification.title");
			}
			
			if(extra.containsKey("google.message_id")){
				msg_id	= extra.getString("google.message_id");
			}

			Intent intent = new Intent(this,WidgetDialogActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			intent.putExtra("message", message);
			intent.putExtra("title", title);
			intent.setAction(msg_id + Long.toString(System.currentTimeMillis()));		//Edited by Thinzar, without this, PendingIntent works correctly for the first notification but incorrectly for the rest

			int icon = R.drawable.app_icon;
			appName = getApplicationContext().getResources().getString(R.string.app_name);


			PendingIntent resultPendingIntent = PendingIntent.getActivity(  getApplicationContext(),
					0, intent, PendingIntent.FLAG_UPDATE_CURRENT );

			Notification.Builder mBuilder = new Notification.Builder( getApplicationContext());
			Notification notification = mBuilder		
					.setSmallIcon(icon)
					.setTicker(title).setWhen(0)
					.setAutoCancel(true)
					.setContentTitle(appName)
					.setStyle(new Notification.BigTextStyle().bigText(message))
					.setContentIntent(resultPendingIntent)
					.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
					//.setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),icon))		//commented by Thinzar
					.setContentText(title+ " " + message).build();

			NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
			int id = (int) System.currentTimeMillis();
			notificationManager.notify(id, notification);

			// Wake Android Device when notification received
			PowerManager pm = (PowerManager) getApplicationContext()
					.getSystemService(Context.POWER_SERVICE);
			final PowerManager.WakeLock mWakelock = pm.newWakeLock(
					PowerManager.FULL_WAKE_LOCK
					| PowerManager.ACQUIRE_CAUSES_WAKEUP, "GCM_PUSH");
			mWakelock.acquire();

			// Timer before putting Android Device to sleep mode.
			Timer timer = new Timer();
			TimerTask task = new TimerTask() {
				public void run() {
					mWakelock.release();
				}
			};
			timer.schedule(task, 50000);
		}
	}
}